<?php
class Creport extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('wip/mreport');
  }
  
  // 15-03-2014
  function viewsohasiljahit(){
     $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
    
    $data['isi'] = 'wip/vformviewsohasiljahit';
	$gudang 	= $this->input->post('gudang', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);  
	
	if ($gudang == '' && $bulan=='' && $tahun=='') {
		$gudang 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	if ($gudang == '')
		$gudang = 0;

    $jum_total = $this->mreport->get_sowiptanpalimit($gudang, $bulan, $tahun);

			$config['base_url'] = base_url().'index.php/wip/creport/viewsohasiljahit/'.$gudang.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mreport->get_sowip($config['per_page'],$this->uri->segment(7), $gudang, $bulan, $tahun);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;

	$data['list_gudang'] = $this->mreport->get_gudang();
	$data['cgudang'] = $gudang;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  // 18-03-2014. 02-11-2015 cek2
  function editsohasiljahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cgudang 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cgudang'] = $cgudang;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mreport->get_sohasiljahit($id_so); 
	$data['querybrgbaru'] = $this->mreport->get_detail_stokbrgbaru_hasiljahit($id_so); 
	
	//$data['jum_total'] = count($data['query']);
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	if (is_array($data['querybrgbaru']))
		$data['jum_total'] += count($data['querybrgbaru']);
		
	$data['isi'] = 'wip/veditsohasiljahit';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  function updatesohasiljahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cgudang = $this->input->post('cgudang', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  $id_gudang = $this->input->post('id_gudang', TRUE);
	  $tgl = date("Y-m-d H:i:s"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_hasil_jahit SET tgl_so = '$tgl_so', jenis_perhitungan_stok='$jenis_perhitungan_stok',
							tgl_update = '$tgl', status_approve='t' 
							where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mreport->savesohasiljahit($id_so, $tgl_so, $jenis_perhitungan_stok, $id_gudang,
			 $this->input->post('id_'.$i, TRUE),
			 $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 );
		 }
			  
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "wip/creport/viewsohasiljahit/".$cgudang."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
  // 06-11-2015
  function viewsounitjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
    $data['isi'] = 'wip/vformviewsounitjahit';
	$id_unit 	= $this->input->post('id_unit', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);
	$s_approve  = $this->input->post('s_approve', TRUE);
	
	if ($id_unit == '' && $bulan=='' && $tahun=='') {
		$id_unit 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	if ($id_unit == '')
		$id_unit = '0';

    $jum_total = $this->mreport->get_sowipunittanpalimit($id_unit, $bulan, $tahun, $s_approve);

			$config['base_url'] = base_url().'index.php/wip/creport/viewsounitjahit/'.$id_unit.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mreport->get_sowipunit($config['per_page'],$this->uri->segment(7), $id_unit, $bulan, $tahun, $s_approve);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['cunit'] = $id_unit;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  // 17-03-2014
  function editsounitjahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cunit 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cunit'] = $cunit;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mreport->get_sounitjahit($id_so); 
	$data['querybrgbaru'] = $this->mreport->get_detail_stokbrgbaru_unitjahit($id_so); 
	
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	if (is_array($data['querybrgbaru']))
		$data['jum_total'] += count($data['querybrgbaru']);
	//echo $data['jum_total']; die();
	
	$data['isi'] = 'wip/veditsounitjahit';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  function updatesounitjahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $id_unit = $this->input->post('id_unit', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cunit = $this->input->post('cunit', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  $tgl = date("Y-m-d H:i:s"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_unit_jahit SET tgl_so = '$tgl_so', 
						jenis_perhitungan_stok='$jenis_perhitungan_stok', tgl_update = '$tgl', status_approve='t' 
						where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mreport->savesounitjahit($id_so, $tgl_so, $id_unit, $jenis_perhitungan_stok,
			 $this->input->post('id_'.$i, TRUE),
			 $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 );
		 }
		//die();
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "wip/creport/viewsounitjahit/".$cunit."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
  // 09-11-2015
  function viewsounitpacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
    $data['isi'] = 'wip/vformviewsounitpacking';
	$id_unit 	= $this->input->post('id_unit', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);  
	
	if ($id_unit == '' && $bulan=='' && $tahun=='') {
		$id_unit 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	if ($id_unit == '')
		$id_unit = '0';

    $jum_total = $this->mreport->get_sowipunitpackingtanpalimit($id_unit, $bulan, $tahun);

			$config['base_url'] = base_url().'index.php/wip/creport/viewsounitpacking/'.$id_unit.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mreport->get_sowipunitpacking($config['per_page'],$this->uri->segment(7), $id_unit, $bulan, $tahun);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['list_unit'] = $this->mreport->get_unit_packing();
	$data['cunit'] = $id_unit;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  function editsounitpacking(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cunit 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cunit'] = $cunit;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mreport->get_sounitpacking($id_so); 
	$data['querybrgbaru'] = $this->mreport->get_detail_stokbrgbaru_unitpacking($id_so); 
	
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	if (is_array($data['querybrgbaru']))
		$data['jum_total'] += count($data['querybrgbaru']);
	//echo $data['jum_total']; die();
	
	$data['isi'] = 'wip/veditsounitpacking';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  // 13-02-2016 lanjutan desta
  // contek dari unit jahit
  function updatesounitpacking() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $id_unit = $this->input->post('id_unit', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cunit = $this->input->post('cunit', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  $tgl = date("Y-m-d H:i:s"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_unit_packing SET tgl_so = '$tgl_so', 
						jenis_perhitungan_stok='$jenis_perhitungan_stok', tgl_update = '$tgl', status_approve='t' 
						where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mreport->savesounitpacking($id_so, $tgl_so, $id_unit, $jenis_perhitungan_stok,
			 $this->input->post('id_'.$i, TRUE),
			 $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 );
		 }
		//die();
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "wip/creport/viewsounitpacking/".$cunit."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
  // 12-11-2015
  // ++++++++++++++++++ mutasi brg unit WIP ++++++++++++++++++++++++++++
  function mutasiunit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'wip/vformmutasiunit';
	$this->load->view('template',$data);
  }
  
  function viewmutasiunit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewmutasiunit';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	
	$data['query'] = $this->mreport->get_mutasi_unit($date_from, $date_to, $unit_jahit);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['unit_jahit'] = $unit_jahit;
	
	if ($unit_jahit != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_jahit'] = $unit_jahit;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
	$this->load->view('template',$data);
  }
  
  // 14-11-2015
  // ++++++++++++++++++ mutasi brg unit packing ++++++++++++++++++++++++++++
  function mutasiunitpacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_packing();
	$data['isi'] = 'wip/vformmutasiunitpacking';
	$this->load->view('template',$data);
  }
  
  function viewmutasiunitpacking(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewmutasiunitpacking';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$unit_packing = $this->input->post('unit_packing', TRUE);  
	
	$data['query'] = $this->mreport->get_mutasi_unit_packing($date_from, $date_to, $unit_packing);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['unit_packing'] = $unit_packing;
	
	if ($unit_packing != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_packing'] = $unit_packing;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
	$this->load->view('template',$data);
  }
  
  // ++++++++++++++++++ laporan transaksi barang hasil jahit di gudang QC ++++++++++++++++++++++++++++
  function transaksihasiljahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mreport->get_gudang();
	$data['isi'] = 'wip/vformlaptransaksihasiljahit';
	$this->load->view('template',$data);
  }
  
  function viewtransaksihasiljahit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlaptransaksihasiljahit';
	$id_gudang = $this->input->post('id_gudang', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	
	$data['query'] = $this->mreport->get_transaksi_hasil_jahit($id_gudang, $bulan, $tahun, $id_brg_wip);
	$data['jum_total'] = count($data['query']);
	
	if ($id_gudang != '0') {
		$query3	= $this->db->query(" SELECT a.nama as nama_lokasi, b.kode_gudang, b.nama as nama_gudang FROM tm_lokasi_gudang a 
				INNER JOIN tm_gudang b ON a.id = b.id_lokasi 
				WHERE b.id = '$id_gudang' ");
		$hasilrow = $query3->row();
		$nama_lokasi	= $hasilrow->nama_lokasi;
		$nama_gudang	= $hasilrow->nama_gudang;
		$kode_gudang	= $hasilrow->kode_gudang;
		$gudangnya = "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang;
	}
	else {
		//$gudangnya = "Semua";
		$kode_gudang = "";
		$nama_gudang = "";
	}
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip;
	
	$data['id_gudang'] = $id_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
  
  function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil kode, nama
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = '';
			$nama_brg_wip = '';
		}
		
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['id_brg_wip'] = $id_brg_wip;
		$this->load->view('wip/vinfobrgwip2', $data); 
		return true;
  }
  
  // 04-12-2015
  // ++++++++++++++++++ laporan transaksi barang unit jahit ++++++++++++++++++++++++++++
  function transaksiunitjahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'wip/vformlaptransaksiunitjahit';
	$this->load->view('template',$data);
  }
  
  // 05-12-2015
  function viewtransaksiunitjahit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlaptransaksiunitjahit';
	$unit_jahit = $this->input->post('unit_jahit', TRUE);
	//$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	//$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	//$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	
	// 14-01-2016 diganti jadi tanggal
	//$bulan = $this->input->post('bulan', TRUE);
	//$tahun = $this->input->post('tahun', TRUE);
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);
	
	// 12-01-2016
	$jumbrg = $this->input->post('jumbrg', TRUE);  
	$list_id_brg_wip = "";
	for ($i=1; $i<=$jumbrg; $i++) {
		$list_id_brg_wip.= $this->input->post('id_brg_wip_'.$i, TRUE).";";
	}
	
	//$data['query'] = $this->mreport->get_transaksi_unit_jahit($unit_jahit, $bulan, $tahun, $list_id_brg_wip);
	$data['query'] = $this->mreport->get_transaksi_unit_jahit($unit_jahit, $date_from, $date_to, $list_id_brg_wip);
	//print_r($data['query']); die();
	$data['jum_total'] = count($data['query']);
	
	if ($unit_jahit != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_jahit'] = $unit_jahit;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
		
	/*if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember"; */
		
	/*$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip; */
	$data['list_id_brg_wip'] = $list_id_brg_wip;
	$data['jumbrg'] = $jumbrg;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	//$data['bulan'] = $bulan;
	//$data['tahun'] = $tahun;
	//$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
  
  // 08-12-2015
  // ++++++++++++++++++ laporan transaksi barang unit packing ++++++++++++++++++++++++++++
  function transaksiunitpacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_packing();
	$data['isi'] = 'wip/vformlaptransaksiunitpacking';
	$this->load->view('template',$data);
  }
  
  function viewtransaksiunitpacking(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlaptransaksiunitpacking';
	$unit_packing = $this->input->post('unit_packing', TRUE);
	$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	$data['query'] = $this->mreport->get_transaksi_unit_packing($unit_packing, $bulan, $tahun, $id_brg_wip);
	//print_r($data['query']); die();
	$data['jum_total'] = count($data['query']);
	
	if ($unit_packing != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_packing'] = $unit_packing;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
		
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
	$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
  
  // 12-12-2015
  function export_excel_mutasiunit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_jahit = $this->input->post('unit_jahit', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$nama_unit = $this->input->post('nama_unit', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_mutasi_unit($date_from, $date_to, $unit_jahit);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='18' align='center'>LAPORAN MUTASI UNIT JAHIT</th>
				 </tr>
				 <tr>
					<th colspan='18' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr></table><br>";
		if (is_array($query)) {
			for($a=0;$a<count($query);$a++){
				$html_data.= "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b><br>";
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th width='3%' rowspan='2'>No</th>
					 <th width='15%' rowspan='2'>Kode</th>
					 <th width='25%' rowspan='2'>Nama Brg WIP</th>
					 <th width='8%' rowspan='2'>HPP</th>
					<th rowspan='2'>Saldo Awal</th>
					<th colspan='4'>Masuk</th>
					<th colspan='6'>Keluar</th>
					<th rowspan='2'>Saldo Akhir</th>
					<th rowspan='2'>Stok Opname</th>
					<th width='3%' rowspan='2'>Selisih</th>
				 </tr>
				 <tr>
					 <th width='3%'>Bgs</th>
					 <th width='3%'>Retur Gdg QC</th>
					 <th width='3%'>Lain2</th>
					 <th width='3%'>Total</th>
					 <th width='3%'>Bgs Gdg QC</th>
					 <th width='3%'>Bgs Gdg Jadi</th>
					 <th width='3%'>Hsl Perbaikan</th>
					 <th width='3%'>Retur Bhn Baku</th>
					 <th width='3%'>Lain2</th>
					 <th width='3%'>Total</th>
				 </tr>
				</thead>
				<tbody>";
				
				$detail_stok = $query[$a]['data_stok'];
				if (is_array($detail_stok)) {
					for($j=0;$j<count($detail_stok);$j++){
						$html_data.=" 
						<tr>
							<td align='center'>".($j+1)."</td>
							<td>".$detail_stok[$j]['kode_brg_wip']."</td>
							<td>".$detail_stok[$j]['nama_brg_wip']."</td>
							<td align='right'>0</td>
							<td align='right'>".$detail_stok[$j]['jum_saldo_awal']."</td>
							<td align='right'>".$detail_stok[$j]['masuk_bgs']."</td>
							<td align='right'>".$detail_stok[$j]['masuk_returbrgjadi']."</td>
							<td align='right'>".$detail_stok[$j]['masuk_lain']."</td>
							<td align='right'>".$detail_stok[$j]['jum_masuk']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_bgs']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_gudangjadi']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_perbaikan']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_retur_bhnbaku']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_lain']."</td>
							<td align='right'>".$detail_stok[$j]['jum_keluar']."</td>
							<td align='right'>".$detail_stok[$j]['jum_saldo_akhir']."</td>
							<td align='right'>".$detail_stok[$j]['jum_so']."</td>
							<td align='right'>".$detail_stok[$j]['selisih']."</td>
						</tr>
						";
					} //endfor2
				} // endif2
/*				$html_data.= "<tr>
						<td colspan='21' align='center'>TOTAL</td>
						<td align='right'>".$query[$a]['total_so_rupiah']."</td>
						<td colspan='4'></td>
						<td>".$query[$a]['total_sisa_stok_rupiah']."</td>
				</tr>"; */
				$html_data.="</tbody></table><br><br>";
			} // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_mutasi_unit_jahit";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../simdsg/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // 15-12-2015
  function export_excel_mutasiunitpacking() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_packing = $this->input->post('unit_packing', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$nama_unit = $this->input->post('nama_unit', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_mutasi_unit_packing($date_from, $date_to, $unit_packing);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='15' align='center'>LAPORAN MUTASI UNIT PACKING</th>
				 </tr>
				 <tr>
					<th colspan='15' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr></table><br>";
		if (is_array($query)) {
			for($a=0;$a<count($query);$a++){
				$html_data.= "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b><br>";
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th width='3%' rowspan='2'>No</th>
					 <th width='15%' rowspan='2'>Kode</th>
					 <th width='25%' rowspan='2'>Nama Brg WIP</th>
					 <th width='8%' rowspan='2'>HPP</th>
					<th rowspan='2'>Saldo Awal</th>
					<th colspan='3'>Masuk</th>
					<th colspan='4'>Keluar</th>
					<th rowspan='2'>Saldo Akhir</th>
					<th rowspan='2'>Stok Opname</th>
					<th width='3%' rowspan='2'>Selisih</th>
				 </tr>
				 <tr>
					 <th width='3%'>Bgs</th>
					 <th width='3%'>Lain2</th>
					 <th width='3%'>Total</th>
					 <th width='3%'>Bgs</th>
					 <th width='3%'>Retur</th>
					 <th width='3%'>Lain2</th>
					 <th width='3%'>Total</th>
				 </tr>
				</thead>
				<tbody>";
				
				$detail_stok = $query[$a]['data_stok'];
				if (is_array($detail_stok)) {
					for($j=0;$j<count($detail_stok);$j++){
						$html_data.=" 
						<tr>
							<td align='center'>".($j+1)."</td>
							<td>".$detail_stok[$j]['kode_brg_wip']."</td>
							<td>".$detail_stok[$j]['nama_brg_wip']."</td>
							<td align='right'>0</td>
							<td align='right'>".$detail_stok[$j]['jum_saldo_awal']."</td>
							<td align='right'>".$detail_stok[$j]['masuk_bgs']."</td>";
							
							$masuk_lain = $detail_stok[$j]['masuk_lain']+$detail_stok[$j]['masuk_other'];
							$html_data.= "
							<td align='right'>".$masuk_lain."</td>
							
							<td align='right'>".$detail_stok[$j]['jum_masuk']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_bgs']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_retur']."</td>";
							
							$keluar_lain = $detail_stok[$j]['keluar_lain']+$detail_stok[$j]['keluar_other'];
							$html_data.= "
							<td align='right'>".$keluar_lain."</td>
							
							<td align='right'>".$detail_stok[$j]['jum_keluar']."</td>
							<td align='right'>".$detail_stok[$j]['jum_saldo_akhir']."</td>
							<td align='right'>".$detail_stok[$j]['jum_so']."</td>
							<td align='right'>".$detail_stok[$j]['selisih']."</td>
						</tr>
						";
					} //endfor2
				} // endif2
/*				$html_data.= "<tr>
						<td colspan='21' align='center'>TOTAL</td>
						<td align='right'>".$query[$a]['total_so_rupiah']."</td>
						<td colspan='4'></td>
						<td>".$query[$a]['total_sisa_stok_rupiah']."</td>
				</tr>"; */
				$html_data.="</tbody></table><br><br>";
			} // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_mutasi_unit_packing";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../simdsg/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // 12-01-2016
  function caribrgwip_laptransaksi(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil kode, nama
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = '';
			$nama_brg_wip = '';
		}
		
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['id_brg_wip'] = $id_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('wip/vinfobrgwip2_laptransaksi', $data); 
		return true;
  }
  
  // 13-02-2016 lanjutan desta
  // ++++++++++++++++++ laporan transaksi barang di gudang jadi ++++++++++++++++++++++++++++
  // contek dari lap transaksi unit jahit
  function transaksigudangjadi(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'wip/vformlaptransaksigudangjadi';
	$this->load->view('template',$data);
  }
  
  function viewtransaksigudangjadi(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlaptransaksigudangjadi';
	$unit_jahit = $this->input->post('unit_jahit', TRUE);
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);
	
	$jumbrg = $this->input->post('jumbrg', TRUE);  
	$list_id_brg_wip = "";
	for ($i=1; $i<=$jumbrg; $i++) {
		$list_id_brg_wip.= $this->input->post('id_brg_wip_'.$i, TRUE).";";
	}
	
	$data['query'] = $this->mreport->get_transaksi_gudang_jadi($date_from, $date_to, $list_id_brg_wip);
	$data['jum_total'] = count($data['query']);
	
	$data['list_id_brg_wip'] = $list_id_brg_wip;
	$data['jumbrg'] = $jumbrg;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$this->load->view('template',$data);
  }
  
   // start 12-03-2014
 /*  function addpresentasikerjaunit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$submit = $this->input->post('submit', TRUE);
	
	if ($submit == "Proses") {
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$unit_jahit = $this->input->post('unit_jahit', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
		
		$data['unit_jahit'] = $unit_jahit;
		$data['nama_unit'] = $nama_unit;
			
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		
		$cek_data = $this->mreport->cek_presentasikerjaunit($unit_jahit, $bulan, $tahun); 
			
		if ($cek_data['idnya'] == '' ) { 
			// ambil nama2 brg dari tabel tm_stok_unit_jahit
			$data['query'] = $this->mreport->get_brgjadi_unit_jahit($unit_jahit);
			
			$data['is_new'] = '1';
			if (is_array($data['query']) )
				$data['jum_total'] = count($data['query']);
			else
				$data['jum_total'] = 0;
			
			$data['list_grup_jahit'] = $this->mreport->get_grup_jahit($unit_jahit);
			$data['isi'] = 'wip/vform2presentasikerjaunit';
			$this->load->view('template',$data);
		}
		else {
				// get data dari tabel tm_presentasi_kerja_unit
				$data['query'] = $this->mreport->get_presentasi_kerja_unit($unit_jahit, $bulan, $tahun);
				$data['is_new'] = '0';
				$data['jum_total'] = count($data['query']);
				$data['list_grup_jahit'] = $this->mreport->get_grup_jahit($unit_jahit);
				$data['isi'] = 'wip/vform2presentasikerjaunit';
				$this->load->view('template',$data);
		} // end else
	}
	else {
		$data['isi'] = 'wip/vformpresentasikerjaunit';
		$data['msg'] = '';
		//$data['bulan_skrg'] = date("m");
		$data['list_unit'] = $this->mreport->get_unit_jahit();
		$this->load->view('template',$data);
	}
  }
  
  function submitpresentasikerjaunit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_jahit = $this->input->post('unit_jahit', TRUE);  
	  $jum_total = $this->input->post('jum_total', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $total_rata = $this->input->post('gtotal', TRUE);
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
	  $hasilrow = $query3->row();
	  $nama_unit	= $hasilrow->nama;
	  
	  if ($is_new == '1') {
	      // insert ke tabel tm_presentasi_kerja_unit
	        $seq	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$id_presentasi	= $seqrow->id+1;
			}else{
				$id_presentasi	= 1;
			}
	      $data_header = array(
			  'id'=>$id_presentasi,
			  'kode_unit'=>$unit_jahit,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'total_rata'=>$total_rata
			);
		  $this->db->insert('tm_presentasi_kerja_unit',$data_header);
		  
	      for ($i=1;$i<=$jum_total;$i++)
		  {
			  // save ke tabel detail
				$seq	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit_detail ORDER BY id DESC LIMIT 1 ");
				if($seq->num_rows() > 0) {
					$seqrow	= $seq->row();
					$iddetailx	= $seqrow->id+1;
				}else{
					$iddetailx	= 1;
				}
					  
				  $data_detail = array(
							'id'=>$iddetailx,
							'id_presentasi_kerja_unit'=>$id_presentasi,
							'kode_brg_jadi'=>$this->input->post('kode_brg_jadi_'.$i, TRUE)
						);
				$this->db->insert('tm_presentasi_kerja_unit_detail',$data_detail); 
			  
				$jum_data = $this->input->post('jum_data_'.$i, TRUE); //echo $jum_data."<br>";
				
				$this->db->query(" DELETE FROM tm_presentasi_kerja_unit_detail_grupjahit WHERE id_presentasi_kerja_unit_detail='$iddetailx' ");
				for ($j=1;$j<=$jum_data-1;$j++) {
					// save ke tabel detail_grupjahit
					$this->mreport->savepresentasikerjaunit($iddetailx,
						 $this->input->post('hari_'.$i.'_'.$j, TRUE),
						 $this->input->post('kapasitas_'.$i.'_'.$j, TRUE),
						 $this->input->post('minggu1_'.$i.'_'.$j, TRUE),
						 $this->input->post('minggu2_'.$i.'_'.$j, TRUE),
						 $this->input->post('minggu3_'.$i.'_'.$j, TRUE),
						 $this->input->post('minggu4_'.$i.'_'.$j, TRUE),
						 $this->input->post('total_'.$i.'_'.$j, TRUE),
						 $this->input->post('persentase_'.$i.'_'.$j, TRUE),
						 $this->input->post('nama_grup_'.$i.'_'.$j, TRUE)
					 );
				} // end for detail
		  } // end for
		  
		$data['msg'] = "Input presentasi kerja unit untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil disimpan";
		$data['list_unit'] = $this->mreport->get_unit_jahit();
		$data['isi'] = 'wip/vformpresentasikerjaunit';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
			  // ambil data id dari tabel tm_presentasi_kerja_unit
			 $query2	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit WHERE kode_unit = '$unit_jahit'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_presentasi	= $hasilrow->id; 
			 
			 // 19-04-2014
			 $query2	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit_detail 
							WHERE id_presentasi_kerja_unit = '$id_presentasi' ");
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->result();
				foreach ($hasil2 as $row2) {
					$this->db->query(" delete from tm_presentasi_kerja_unit_detail_grupjahit 
									where id_presentasi_kerja_unit_detail = '".$row2->id."' ");
				}
			}
			// --------------------------------------------------------------------------------
			 
			$this->db->query(" delete from tm_presentasi_kerja_unit_detail where id_presentasi_kerja_unit = '$id_presentasi' ");
			$this->db->query(" delete from tm_presentasi_kerja_unit where id = '$id_presentasi' ");
			
			$data['msg'] = "Input presentasi kerja unit untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil dihapus";
			$data['list_unit'] = $this->mreport->get_unit_jahit();
			$data['isi'] = 'wip/vformpresentasikerjaunit';
			$this->load->view('template',$data);
		  }
		  else { // function update
			  // update ke tabel tm_presentasi_kerja_unit
				 $query2	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit WHERE kode_unit = '$unit_jahit' 
								AND bulan = '$bulan' AND tahun = '$tahun' ");
				 $hasilrow = $query2->row();
				 $id_presentasi	= $hasilrow->id; 
				 
				 $this->db->query(" UPDATE tm_presentasi_kerja_unit SET total_rata = '$total_rata', tgl_update = '$tgl' 
							where id = '$id_presentasi' ");
				 
				 for ($i=1;$i<=$jum_total;$i++)
				  {
						$jum_data = $this->input->post('jum_data_'.$i, TRUE);
						$iddetailx = $this->input->post('iddetail_'.$i, TRUE);
						
						if ($iddetailx == '0') {
							// save ke tabel detail utk data brg jadi yg blm pernah disave
							$seq	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit_detail ORDER BY id DESC LIMIT 1 ");
							if($seq->num_rows() > 0) {
								$seqrow	= $seq->row();
								$iddetailx	= $seqrow->id+1;
							}else{
								$iddetailx	= 1;
							}
								  
							  $data_detail = array(
										'id'=>$iddetailx,
										'id_presentasi_kerja_unit'=>$id_presentasi,
										'kode_brg_jadi'=>$this->input->post('kode_brg_jadi_'.$i, TRUE)
									);
							$this->db->insert('tm_presentasi_kerja_unit_detail',$data_detail); 
						} // end if
						
						$this->db->query(" DELETE FROM tm_presentasi_kerja_unit_detail_grupjahit WHERE id_presentasi_kerja_unit_detail='$iddetailx' ");
						for ($j=1;$j<=$jum_data-1;$j++) {
							// save ke tabel detail_grupjahit
							$this->mreport->savepresentasikerjaunit($iddetailx,
								 $this->input->post('hari_'.$i.'_'.$j, TRUE),
								 $this->input->post('kapasitas_'.$i.'_'.$j, TRUE),
								 $this->input->post('minggu1_'.$i.'_'.$j, TRUE),
								 $this->input->post('minggu2_'.$i.'_'.$j, TRUE),
								 $this->input->post('minggu3_'.$i.'_'.$j, TRUE),
								 $this->input->post('minggu4_'.$i.'_'.$j, TRUE),
								 $this->input->post('total_'.$i.'_'.$j, TRUE),
								 $this->input->post('persentase_'.$i.'_'.$j, TRUE),
								 $this->input->post('nama_grup_'.$i.'_'.$j, TRUE)
							 );
						} // end for detail
				  } // end for
		  
			  
			$data['msg'] = "Input presentasi kerja unit untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil diupdate";
			$data['list_unit'] = $this->mreport->get_unit_jahit();
			$data['isi'] = 'wip/vformpresentasikerjaunit';
			$this->load->view('template',$data);
		}
      }
  } // 
  
  // ==============================================================================================================
  
  function addforecast(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$submit = $this->input->post('submit', TRUE);
	
	if ($submit == "Proses") {
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		
		$cek_data = $this->mreport->cek_forecast($bulan, $tahun); 
			
		if ($cek_data['idnya'] == '' ) { 
			// ambil nama2 brg dari tabel master brg jadi (tr_product_motif)
			//$data['query'] = $this->mreport->get_brgjadi();
			$data['query'] = '';
			
			$data['is_new'] = '1';
			if (is_array($data['query']) )
				$data['jum_total'] = count($data['query']);
			else
				$data['jum_total'] = 0;
				
			$data['isi'] = 'wip/vform2forecast';
			$this->load->view('template',$data);
		}
		else {
				// get data dari tabel tm_forecast_wip
				$data['query'] = $this->mreport->get_forecast($bulan, $tahun);
				$data['is_new'] = '0';
				$data['jum_total'] = count($data['query']);
				$data['isi'] = 'wip/vform2forecast';
				$this->load->view('template',$data);
		} // end else
	}
	else {
		$data['isi'] = 'wip/vformforecast';
		$data['msg'] = '';
		//$data['bulan_skrg'] = date("m");
		$this->load->view('template',$data);
	}
  }
  
  function submitforecast() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE)-1;  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit3 = $this->input->post('submit3', TRUE);
	  //echo $bulan." ".$tahun." ".$submit3; die();
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  	  
	  if ($is_new == '1') {
	      // insert ke tabel tm_forecast_wip
	        $seq	= $this->db->query(" SELECT id FROM tm_forecast_wip ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$id_fc	= $seqrow->id+1;
			}else{
				$id_fc	= 1;
			}
	      $data_header = array(
			  'id'=>$id_fc,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
		  $this->db->insert('tm_forecast_wip',$data_header);
		  	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mreport->saveforecast($is_new, $id_fc, $this->input->post('iddetail_'.$i, TRUE),
			 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
			 $this->input->post('fc_'.$i, TRUE),
			 $this->input->post('ket_'.$i, TRUE)
			 );
		  }
		  
		$data['msg'] = "Input forecast untuk bulan ".$nama_bln." ".$tahun." sudah berhasil disimpan";
		$data['isi'] = 'wip/vformforecast';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit3 != '') { // function hapus
			  // ambil data id dari tabel tm_forecast_wip
			 $query2	= $this->db->query(" SELECT id FROM tm_forecast_wip WHERE bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_fc	= $hasilrow->id; 
			 
			 
			$this->db->query(" delete from tm_forecast_wip_detail where id_forecast_wip = '$id_fc' ");
			$this->db->query(" delete from tm_forecast_wip where id = '$id_fc' ");
			
			$data['msg'] = "Input forecast untuk bulan ".$nama_bln." ".$tahun." sudah berhasil dihapus";
			$data['isi'] = 'wip/vformforecast';
			$this->load->view('template',$data);
		  }
		  else { // function update
			  // update ke tabel tm_forecast_wip
			   // ambil data terakhir di tabel tm_forecast_wip
				 $query2	= $this->db->query(" SELECT id FROM tm_forecast_wip WHERE bulan = '$bulan' AND tahun = '$tahun' ");
				 $hasilrow = $query2->row();
				 $id_fc	= $hasilrow->id; 
				 
				 $this->db->query(" UPDATE tm_forecast_wip SET tgl_update = '$tgl' 
							where id = '$id_fc' ");
				 $this->db->query(" DELETE FROM tm_forecast_wip_detail WHERE id_forecast_wip='$id_fc' ");
			  for ($i=1;$i<=$jum_data;$i++)
			  {
				 $this->mreport->saveforecast($is_new, $id_fc, $this->input->post('iddetail_'.$i, TRUE),
				 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
				 $this->input->post('fc_'.$i, TRUE),
				 $this->input->post('ket_'.$i, TRUE)
				 );
			  }
			  
			$data['msg'] = "Input forecast untuk bulan ".$nama_bln." ".$tahun." sudah berhasil diupdate";
			$data['isi'] = 'wip/vformforecast';
			$this->load->view('template',$data);
		}
      }
  } // 
  
  // 02-04-2014
  // ++++++++++++++++++ laporan stok mingguan unit ++++++++++++++++++++++++++++
  function stokmingguanunit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'wip/vformstokmingguanunit';
	$this->load->view('template',$data);
  }
  
  function viewstokmingguanunit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewstokmingguanunit';
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	
	$data['query'] = $this->mreport->get_stokmingguan_unit($unit_jahit);
	$data['jum_total'] = count($data['query']);
	$data['unit_jahit'] = $unit_jahit;
	
	if ($unit_jahit != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$nama_unit = "Semua";
	}
		
	$data['unit_jahit'] = $unit_jahit;
	$data['nama_unit'] = $nama_unit;
	$this->load->view('template',$data);
  }
  
  // 03-04-2014
  function export_excel_stokmingguanunit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_jahit = $this->input->post('unit_jahit', TRUE);
		$nama_unit = $this->input->post('nama_unit', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_stokmingguan_unit($unit_jahit);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='5' align='center'>DATA STOCK</th>
				 </tr>
				</table><br>";
		if (is_array($query)) {
			for($a=0;$a<count($query);$a++){
				$html_data.= "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b><br>";
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='60%'>
	<thead>
	 <tr>
		 <th width='3%'>No</th>
		 <th width='15%'>Kode</th>
		 <th width='35%'>Nama Brg</th>
		<th width='5%'>Stok Bagus</th>
		<th width='5%'>Stok Perbaikan</th>
	 </tr>
	</thead>
	<tbody>";
				
				$detail_stok = $query[$a]['data_stok'];
				if (is_array($detail_stok)) {
					for($j=0;$j<count($detail_stok);$j++){
						$html_data.=" 
						<tr>
							<td align='center'>".($j+1)."</td>
							<td>".$detail_stok[$j]['kode_brg']."</td>
							<td>".$detail_stok[$j]['nama_brg']."</td>
							<td align='right'>".$detail_stok[$j]['stok_bagus']."</td>
							<td align='right'>".$detail_stok[$j]['stok_perbaikan']."</td>
						</tr>
						";
					} //endfor2
				} // endif2
				
				$html_data.="</tbody></table><br><br>";
			} // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_stok_mingguan";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  function bhnbakuunit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'wip/vformbhnbakuunit';
	$this->load->view('template',$data);
  }
  
  function viewbhnbakuunit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewbhnbakuunit';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	$data['query'] = $this->mreport->get_bhnbaku_unit($date_from, $date_to);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }
  
  function export_excel_bhnbakuunit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_bhnbaku_unit($date_from, $date_to);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='50%'>
				 <tr>
					<th colspan='2' align='center'>DATA BAHAN BAKU <br> Periode Tanggal SJ $date_from s.d $date_to</th>
				 </tr>
				</table><br><br>";
		if (is_array($query)) {
			for($a=0;$a<count($query);$a++){
				$html_data.="<table border='1' cellpadding= '1' cellspacing = '1' width='50%'>
				<thead>
				 <tr>
					 <th colspan='2'><b>".$query[$a]['kode_brg_jadi']." - ".$query[$a]['nama_brg_jadi']."<br> (Forecast ".$query[$a]['bln_forecast']." ".$query[$a]['thn_forecast'].": ".$query[$a]['qty_forecast'].") </b></th>
				 </tr>
				</thead>
				<tbody>";
				
				$detail_stok = $query[$a]['data_stok'];
				if (is_array($detail_stok)) {
					for($j=0;$j<count($detail_stok);$j++){
						$html_data.= "<tr>
							<td width='50%'>".$detail_stok[$j]['kode_unit']." - ".$detail_stok[$j]['nama_unit']."</td>
							<td width='50%' align='right'>".$detail_stok[$j]['jum']."&nbsp;</td>
						</tr>";
					} //endfor2
				} // endif2
								
				$html_data.="</tbody></table><br><br>";
			} // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_bhn_baku";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // 19-04-2014
  // ++++++++++++++++++ laporan presentasi kerja unit per unit jahit ++++++++++++++++++++++++++++
  function presentasikerjaunit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'wip/vformlappresentasikerjaunit';
	$this->load->view('template',$data);
  }
  
  function viewpresentasikerjaunit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlappresentasikerjaunit';
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	$data['query'] = $this->mreport->get_rekap_presentasi_kerja_unit($bulan, $tahun, $unit_jahit);
	//print_r($data['query']); die();
	$data['jum_total'] = count($data['query']);
	
	if ($unit_jahit != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$nama_unit = "Semua";
	}
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
	$data['unit_jahit'] = $unit_jahit;
	$data['nama_unit'] = $nama_unit;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
  
  // 21-04-2014
  function export_excel_presentasikerjaunit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_jahit = $this->input->post('unit_jahit', TRUE);  
		$nama_unit = $this->input->post('nama_unit', TRUE);
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$nama_bulan = $this->input->post('nama_bulan', TRUE);
		
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_rekap_presentasi_kerja_unit($bulan, $tahun, $unit_jahit);
		$nomor = 1;
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='90%'>
				 <tr>
					<th colspan='10' align='center'>Laporan Presentasi Schedule Unit Bulan $nama_bulan</th>
				 </tr>
				 ";
		
		if (is_array($query)) {
		  for($a=0;$a<count($query);$a++){
			$html_data.=" 
				<tr>
					<th colspan='10' align='center'><b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b></th>
				 </tr> 
				 <tr>
					<th width='3%'>No</th>
					 <th width='5%'>Hari</th>
					 <th width='20%'>Nama Barang</th>
					 <th width='5%'>Kapasitas</th>
					 <th width='5%'>Minggu 1</th>
					 <th width='5%'>Minggu 2</th>
					 <th width='5%'>Minggu 3</th>
					 <th width='5%'>Minggu 4</th>
					 <th width='8%'>Total/Bulan</th>
					 <th width='8%'>Persentase</th>
				 </tr>
				 ";
			$detail_grup = $query[$a]['data_grup_jahit'];
			if (is_array($detail_grup)) {
				for($j=0;$j<count($detail_grup);$j++){
					$html_data.=" <tr>
				<td colspan='10' align='center'>".$detail_grup[$j]['nama_grup_jahit']."</td>
			</tr> ";
					
					$detail_item = $detail_grup[$j]['data_item_brg'];
					if (is_array($detail_item)) {
						for($z=0;$z<count($detail_item);$z++){
							$html_data.="<tr>
								<td align='center'>$nomor</td>
								<td align='right'>".$detail_item[$z]['hari']."&nbsp;</td>
								<td>&nbsp;".$detail_item[$z]['kode_brg_jadi']." - ".$detail_item[$z]['nama_brg_jadi']."</td>
								<td align='right'>".$detail_item[$z]['kapasitas']."&nbsp;</td>
								<td align='right'>".$detail_item[$z]['minggu1'] ."&nbsp;</td>
								<td align='right'>".$detail_item[$z]['minggu2'] ."&nbsp;</td>
								<td align='right'>".$detail_item[$z]['minggu3'] ."&nbsp;</td>
								<td align='right'>".$detail_item[$z]['minggu4'] ."&nbsp;</td>
								<td align='right'>".$detail_item[$z]['total'] ."&nbsp;</td>
								<td align='right'>".$detail_item[$z]['persentase'] ."&nbsp;</td>
							</tr>";
							$nomor++;
						}
					}
				}
				
			}
			$html_data.="<tr>
			<td colspan='9' align='center'>Rata-Rata Presentasi Schedule / Bulan</td>
			<td align='right'>".$query[$a]['total_rata']."&nbsp;</td>
			</tr></tbody>
			</table><br><br>";
		 } // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	
		$nama_file = "laporan_presentasi_kerja_unit";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // ++++++++++++++++++ rekap presentasi kerja unit tahunan ++++++++++++++++++++++++++++
  function rekappresentasikerjaunit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'wip/vformrekappresentasikerjaunit';
	$this->load->view('template',$data);
  }
  
  function viewrekappresentasikerjaunit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewrekappresentasikerjaunit';
	//$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	//$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	$data['queryutama'] = $this->mreport->get_rekap_presentasi_kerja_unit_tahunan($tahun);
	//$data['bulan'] = $bulan;
	//print_r($data['queryutama']); die();
	$data['tahun'] = $tahun;
	//$data['unit_jahit'] = $unit_jahit;
		
	$data['tahun'] = $tahun;
	$this->load->view('template',$data);
  }
  
  // ===== 30-04-2014 ===============
  function forecastvsschedule(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'wip/vformlapforecastvsschedule';
	$this->load->view('template',$data);
  }
  
  function viewforecastvsschedule(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewforecastvsschedule';
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		$data['nama_bulan'] = $nama_bln;
	
	$data['query'] = $this->mreport->get_forecastvsschedule($bulan, $tahun);
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$this->load->view('template',$data);
  }
  
  function export_excel_forecastvsschedule() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$nama_bulan = $this->input->post('nama_bulan', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_forecastvsschedule($bulan, $tahun);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='10' align='center'>PT TIRAI PELANGI NUSANTARA</th>
				 </tr>
				 <tr>
					<th colspan='10' align='center'>LAPORAN FORECAST BULAN ".strtoupper($nama_bulan)." ".$tahun."</th>
				 </tr>
				 <tr>
					<th width='3%'>No </th>
					 <th width='10%'>Kode</th>
					 <th width='15%'>Nama Barang Jadi</th>
					 <th width='5%'>FC Produksi</th>
					 <th width='5%'>Schedule</th>
					 <th width='5%'>Saldo Awal</th>
					 <th width='5%'>Barang Masuk</th>
					 <th width='5%'>%Produksi <br>vs Schedule</th>
					 <th>%Brg Masuk <br>vs Schedule+<br>Saldo Awal</th>
					 <th width='20%'>Keterangan</th>
				 </tr>";
				 // <th width='5%'>%Produksi <br>vs Forecast</th>
		if (is_array($query)) {
			$no=1;
			$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumschedule=0; $sumsaldo_awal_bgs=0; $sumbrgmasuk=0; 
				$sumpersenschedule=0; $sumpersenforecast2=0;
				
			for($a=0;$a<count($query);$a++){
				if ($temp_kodekel != $query[$a]['kode_kel']) {
					 $temp_kodekel = $query[$a]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='10'>&nbsp;<b>".$query[$a]['kode_kel']." - ".$query[$a]['nama_kel']."</b></td>
					</tr>";
				 }
				 
				$html_data.= "<tr>
				<td align='center'>".$no."</td>
				<td>".$query[$a]['kode_brg_jadi']."</td>
				<td>".$query[$a]['nama_brg_jadi']."</td>
				<td>".$query[$a]['fc']."</td>
				<td>".$query[$a]['schedule']."</td>
				<td>".$query[$a]['saldo_awal_bgs']."</td>
				<td>".$query[$a]['brgmasuk']."</td>
				<td>".$query[$a]['persenschedule']."</td>
				<td>".$query[$a]['persenforecast2']."</td>
				<td>".$query[$a]['keterangan']."</td>
				</tr>";
				// <td>".$query[$a]['persenforecast']."</td>
				
					$sumfc+=$query[$a]['fc'];
					$sumschedule+=$query[$a]['schedule'];
					$sumsaldo_awal_bgs+=$query[$a]['saldo_awal_bgs'];
					$sumbrgmasuk+=$query[$a]['brgmasuk'];
					$sumpersenschedule+=$query[$a]['persenschedule'];
					$sumpersenforecast2+=$query[$a]['persenforecast2'];
					$cdata++;
					
					if (isset($query[$a+1]['kode_kel']) && ($query[$a]['kode_kel'] != $query[$a+1]['kode_kel'])) {	
						$rataschedule= $sumpersenschedule/$cdata;
						$rataforecast2= $sumpersenforecast2/$cdata;
						
						$html_data.="<tr>
							<td colspan='3' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumschedule."</b></td>
							<td align='right'><b>".$sumsaldo_awal_bgs."</b></td>
							<td align='right'><b>".$sumbrgmasuk."</b></td>
							<td align='right'><b>".$rataschedule."</b></td>
							<td align='right'><b>".$rataforecast2."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumschedule=0; $sumsaldo_awal_bgs=0; $sumbrgmasuk=0; 
						$sumpersenschedule=0; $sumpersenforecast2=0;
					}
					else if (!isset($query[$a+1]['kode_kel'])) {
						$rataschedule= $sumpersenschedule/$cdata;
						$rataforecast2= $sumpersenforecast2/$cdata;
						
						$html_data.="<tr>
							<td colspan='3' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumschedule."</b></td>
							<td align='right'><b>".$sumsaldo_awal_bgs."</b></td>
							<td align='right'><b>".$sumbrgmasuk."</b></td>
							<td align='right'><b>".$rataschedule."</b></td>
							<td align='right'><b>".$rataforecast2."</b></td>
							<td>&nbsp;</td>
						</tr>";
					}
					
				$no++;
			}
				$html_data.="</tbody></table><br><br>";
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_forecastvsschedule";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  //=================================
  
  // 23-05-2014 STOK SCHEDULE
  function addstokschedule(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$submit = $this->input->post('submit', TRUE);
	
	if ($submit == "Proses") {
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$unit_jahit = $this->input->post('unit_jahit', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
		
		$data['unit_jahit'] = $unit_jahit;
		$data['nama_unit'] = $nama_unit;
			
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		
		// cek_presentasikerjaunit
		$cek_data = $this->mreport->cek_stokschedule($unit_jahit, $bulan, $tahun); 
			
		if ($cek_data['idnya'] == '' ) { 
			// ambil nama2 brg dari query mutasi unit
			$data['query'] = $this->mreport->get_mutasi_unit_stokschedule($unit_jahit, $bulan, $tahun);
			
			$data['is_new'] = '1';
			if (is_array($data['query']) )
				$data['jum_total'] = count($data['query']);
			else
				$data['jum_total'] = 0;
			
			$data['isi'] = 'wip/vform2stokschedule';
			$this->load->view('template',$data);
		}
		else {
				// get data dari tabel tm_stok_schedule_wip
				$data['query'] = $this->mreport->get_stok_schedule($unit_jahit, $bulan, $tahun);
				$data['is_new'] = '0';
				$data['jum_total'] = count($data['query']);
				$data['isi'] = 'wip/vform2stokschedule';
				$this->load->view('template',$data);
		} // end else
	}
	else {
		$data['isi'] = 'wip/vformstokschedule';
		$data['msg'] = '';
		$data['list_unit'] = $this->mreport->get_unit_jahit();
		$this->load->view('template',$data);
	}
  }
  
  function submitstokschedule() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_jahit = $this->input->post('unit_jahit', TRUE);  
	  $jum_total = $this->input->post('jum_total', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
	  $hasilrow = $query3->row();
	  $nama_unit	= $hasilrow->nama;
	  
	  if ($is_new == '1') {		  
	      for ($i=1;$i<=$jum_total;$i++)
		  {
			  // save ke tabel detail
				$seq	= $this->db->query(" SELECT id FROM tm_stok_schedule_wip ORDER BY id DESC LIMIT 1 ");
				if($seq->num_rows() > 0) {
					$seqrow	= $seq->row();
					$id_schedule	= $seqrow->id+1;
				}else{
					$id_schedule	= 1;
				}
				
				$kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
				$iddetailx = $this->input->post('iddetail_'.$i, TRUE); // ini 0 karena new
				$qty = $this->input->post('qty_'.$i, TRUE);
				
				  $data_detail = array(
							  'id'=>$id_schedule,
							  'kode_unit'=>$unit_jahit,
							  'bulan'=>$bulan,
							  'tahun'=>$tahun,
							  'kode_brg_jadi'=>$kode_brg_jadi,
							  'qty'=>$qty,
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl
						);
				$this->db->insert('tm_stok_schedule_wip',$data_detail); 
		  } // end for
		  
		$data['msg'] = "Input stok schedule untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil disimpan";
		$data['list_unit'] = $this->mreport->get_unit_jahit();
		$data['isi'] = 'wip/vformstokschedule';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
				$this->db->query(" DELETE FROM tm_stok_schedule_wip
									WHERE kode_unit = '$unit_jahit' AND bulan = '$bulan' AND tahun = '$tahun' ");
			
			$data['msg'] = "Input stok schedule untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil dihapus";
			$data['list_unit'] = $this->mreport->get_unit_jahit();
			$data['isi'] = 'wip/vformstokschedule';
			$this->load->view('template',$data);
		  }
		  else { // function update				 
				 for ($i=1;$i<=$jum_total;$i++)
				  {
						$iddetailx = $this->input->post('iddetail_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$this->db->query(" UPDATE tm_stok_schedule_wip SET qty='$qty' WHERE id='$iddetailx' ");								
				  } // end for
		  			  
			$data['msg'] = "Input stok schedule untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil diupdate";
			$data['list_unit'] = $this->mreport->get_unit_jahit();
			$data['isi'] = 'wip/vformstokschedule';
			$this->load->view('template',$data);
		}
      }
  }
  
  // 13-06-2014
  function export_excel_rekappresentasikerjaunit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$tahun = $this->input->post('tahun', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$queryutama= $this->mreport->get_rekap_presentasi_kerja_unit_tahunan($tahun);
		
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='16' align='center'>PT TIRAI PELANGI NUSANTARA</th>
				 </tr>
				 <tr>
					<th colspan='16' align='center'>LAPORAN PRESENTASI KAPASITAS UNIT JAHIT TAHUN ".$tahun."</th>
				 </tr>";
		
		$nomor = 0; $temp_unit = ""; $totalperunit=0; $jumgrupperunit=0;
		$tjanuari=0; $tfebruari=0; $tmaret=0; $tapril=0; $tmei=0; $tjuni=0; $tjuli=0; $tagustus=0; $tseptember=0;
		$toktober=0; $tnovember=0; $tdesember=0;
		$cjanuari=0; $cfebruari=0; $cmaret=0; $capril=0; $cmei=0; $cjuni=0; $cjuli=0; $cagustus=0; $cseptember=0;
		$coktober=0; $cnovember=0; $cdesember=0;
		$jum1=0; $cunit1=0; $temp_kel_unit = "";
	
		if (is_array($queryutama)) {
			for($aa=0;$aa<count($queryutama);$aa++){
				if ($queryutama[$aa]['kode'] != $temp_kel_unit) {
					$temp_kel_unit = $queryutama[$aa]['kode'];
					
					$html_data.="<tr>
						<td colspan='16'><b>".$queryutama[$aa]['kode']." - ".$queryutama[$aa]['nama']."</b></td>
						</tr>
						<tr>
					 <th width='3%' rowspan='2'>No</th>
					 <th width='10%' rowspan='2'>Unit</th>
					 <th width='10%' rowspan='2'>Grup</th>
					 <th colspan='12'>Bulan</th>
					 <th width='10%' rowspan='2'>Rata-Rata</th>
				 </tr>
				 <tr>
					<th width='10%'>Januari</th>
					<th width='10%'>Februari</th>
					<th width='10%'>Maret</th>
					<th width='10%'>April</th>
					<th width='10%'>Mei</th>
					<th width='10%'>Juni</th>
					<th width='10%'>Juli</th>
					<th width='10%'>Agustus</th>
					<th width='10%'>September</th>
					<th width='10%'>Oktober</th>
					<th width='10%'>November</th>
					<th width='10%'>Desember</th>
				 </tr>";
				 
				 $query = $queryutama[$aa]['data_rekap'];
		
				if (is_array($query)) {
					for($a=0;$a<count($query);$a++){
						$html_data.="<tr>";
						
						if ($temp_unit != $query[$a]['kode_unit']) {
							$temp_unit = $query[$a]['kode_unit'];
							$nomor++;
							$html_data.= "<td align='center'>".$nomor."</td>";
							$html_data.= "<td>&nbsp;".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</td>";
						}
						else {
							$html_data.= "<td align='center'>&nbsp;</td>";
							$html_data.= "<td>&nbsp;</td>";
						}
						
						$html_data.="<td>".$query[$a]['nama_grup_jahit']."</td>";
			
						$listratabulan = $query[$a]['listratabulan'];
							
						$bulan=1; $total = 0; $cbulan=0;
						if (is_array($listratabulan)) {
							for($j=0;$j<count($listratabulan);$j++){
								//$total+=$listratabulan[$j][$bulan];
									// 06-08-2014
									if ($listratabulan[$j][$bulan] != 0) {
										$total+=$listratabulan[$j][$bulan];
										$cbulan++;
									}
									
									if ($bulan == 1) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tjanuari+=$listratabulan[$j][$bulan];
											$cjanuari++;
										}
									}
									else if ($bulan == 2) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tfebruari+=$listratabulan[$j][$bulan];
											$cfebruari++;
										}
									}
									else if ($bulan == 3) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tmaret+=$listratabulan[$j][$bulan];
											$cmaret++;
										}
									}
									else if ($bulan == 4) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tapril+=$listratabulan[$j][$bulan];
											$capril++;
										}
									}
									else if ($bulan == 5) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tmei+=$listratabulan[$j][$bulan];
											$cmei++;
										}
									}
									else if ($bulan == 6) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tjuni+=$listratabulan[$j][$bulan];
											$cjuni++;
										}
									}
									else if ($bulan == 7) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tjuli+=$listratabulan[$j][$bulan];
											$cjuli++;
										}
									}
									else if ($bulan == 8) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tagustus+=$listratabulan[$j][$bulan];
											$cagustus++;
										}
									}
									else if ($bulan == 9) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tseptember+=$listratabulan[$j][$bulan];
											$cseptember++;
										}
									}
									else if ($bulan == 10) {
										if ($listratabulan[$j][$bulan] != 0) {
											$toktober+=$listratabulan[$j][$bulan];
											$coktober++;
										}
									}
									else if ($bulan == 11) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tnovember+=$listratabulan[$j][$bulan];
											$cnovember++;
										}
									}
									else if ($bulan == 12) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tdesember+=$listratabulan[$j][$bulan];
											$cdesember++;
										}
									}
						
								$html_data.="<td align='right'>".$listratabulan[$j][$bulan]."</td>";
								$bulan++;
							}
							//$totrata = $total/12;
							//$totalperunit+=$totrata;
							$totrata = $total/$cbulan;
							$totalperunit+=$totrata;
							$jumgrupperunit++;
						}
						
						$html_data.="<td align='right'>".$totrata."</td></tr>";
						
						if ( isset($query[$a+1]['kode_unit']) && ($query[$a]['kode_unit'] != $query[$a+1]['kode_unit']) ) {
							//06-08-2014
							if ($jumgrupperunit != 0)
								$rataperunit = $totalperunit/$jumgrupperunit;
							else
								$rataperunit = 0;
							
							$jum1+= $rataperunit;
							$cunit1++;
							
							$html_data.="<tr>
								<td colspan='15' align='right'><b>Rata-Rata Unit ".$query[$a]['nama_unit']."</b></td>
								<td align='right'><b>".$rataperunit."</b></td>
							</tr>";
							
							$totalperunit = 0; $jumgrupperunit = 0;
						}
						else if ( !isset($query[$a+1]['kode_unit'])) {
							//06-08-2014
							if ($jumgrupperunit != 0)
								$rataperunit = $totalperunit/$jumgrupperunit;
							else
								$rataperunit = 0;
							
							$jum1+= $rataperunit;
							$cunit1++;
							
							$html_data.="<tr>
								<td colspan='15' align='right'><b>Rata-Rata Unit ".$query[$a]['nama_unit']."</b></td>
								<td align='right'><b>".$rataperunit."</b></td>
								</tr>";
					
							$totalperunit = 0; $jumgrupperunit = 0;
						}
						
					} //end for query
				}
				if (isset($queryutama[$aa+1]['kode']) && ($queryutama[$aa]['kode'] != $queryutama[$aa+1]['kode'])) {
					$rataperkel = $jum1/$cunit1;
					
					$html_data.="<tr>
						<th colspan='15'>TOTAL RATA-RATA</th>
						<th align='right'><b>".$rataperkel."</b></th>
						</tr>";
					$jum1=0; $cunit1=0;
				}
				else if ( !isset($queryutama[$aa+1]['kode'])) {
					$rataperkel = $jum1/$cunit1;
					
					$html_data.="<tr>
						<th colspan='15'>TOTAL RATA-RATA</th>
						<th align='right'><b>".$rataperkel."</b></th>
						</tr>";
					$jum1=0; $cunit1=0;
				}
			}
			else {
				// ini tanpa header
				$query = $queryutama[$aa]['data_rekap'];
		
				if (is_array($query)) {
					for($a=0;$a<count($query);$a++){
						$html_data.="<tr>";
						
						if ($temp_unit != $query[$a]['kode_unit']) {
							$temp_unit = $query[$a]['kode_unit'];
							$nomor++;
							$html_data.= "<td align='center'>".$nomor."</td>";
							$html_data.= "<td>&nbsp;".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</td>";
						}
						else {
							$html_data.= "<td align='center'>&nbsp;</td>";
							$html_data.= "<td>&nbsp;</td>";
						}
						
						$html_data.="<td>".$query[$a]['nama_grup_jahit']."</td>";
			
						$listratabulan = $query[$a]['listratabulan'];
							
						$bulan=1; $total = 0; $cbulan=0;
						if (is_array($listratabulan)) {
							for($j=0;$j<count($listratabulan);$j++){
								//$total+=$listratabulan[$j][$bulan];
									// 06-08-2014
									if ($listratabulan[$j][$bulan] != 0) {
										$total+=$listratabulan[$j][$bulan];
										$cbulan++;
									}
									
									if ($bulan == 1) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tjanuari+=$listratabulan[$j][$bulan];
											$cjanuari++;
										}
									}
									else if ($bulan == 2) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tfebruari+=$listratabulan[$j][$bulan];
											$cfebruari++;
										}
									}
									else if ($bulan == 3) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tmaret+=$listratabulan[$j][$bulan];
											$cmaret++;
										}
									}
									else if ($bulan == 4) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tapril+=$listratabulan[$j][$bulan];
											$capril++;
										}
									}
									else if ($bulan == 5) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tmei+=$listratabulan[$j][$bulan];
											$cmei++;
										}
									}
									else if ($bulan == 6) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tjuni+=$listratabulan[$j][$bulan];
											$cjuni++;
										}
									}
									else if ($bulan == 7) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tjuli+=$listratabulan[$j][$bulan];
											$cjuli++;
										}
									}
									else if ($bulan == 8) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tagustus+=$listratabulan[$j][$bulan];
											$cagustus++;
										}
									}
									else if ($bulan == 9) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tseptember+=$listratabulan[$j][$bulan];
											$cseptember++;
										}
									}
									else if ($bulan == 10) {
										if ($listratabulan[$j][$bulan] != 0) {
											$toktober+=$listratabulan[$j][$bulan];
											$coktober++;
										}
									}
									else if ($bulan == 11) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tnovember+=$listratabulan[$j][$bulan];
											$cnovember++;
										}
									}
									else if ($bulan == 12) {
										if ($listratabulan[$j][$bulan] != 0) {
											$tdesember+=$listratabulan[$j][$bulan];
											$cdesember++;
										}
									}
						
								$html_data.="<td align='right'>".$listratabulan[$j][$bulan]."</td>";
								$bulan++;
							}
							//$totrata = $total/12;
							//$totalperunit+=$totrata;
							$totrata = $total/$cbulan;
							$totalperunit+=$totrata;
							$jumgrupperunit++;
						}
						
						$html_data.="<td align='right'>".$totrata."</td></tr>";
						
						if ( isset($query[$a+1]['kode_unit']) && ($query[$a]['kode_unit'] != $query[$a+1]['kode_unit']) ) {
							//06-08-2014
							if ($jumgrupperunit != 0)
								$rataperunit = $totalperunit/$jumgrupperunit;
							else
								$rataperunit = 0;
							
							$jum1+= $rataperunit;
							$cunit1++;
							
							$html_data.="<tr>
								<td colspan='15' align='right'><b>Rata-Rata Unit ".$query[$a]['nama_unit']."</b></td>
								<td align='right'><b>".$rataperunit."</b></td>
							</tr>";
							
							$totalperunit = 0; $jumgrupperunit = 0;
						}
						else if ( !isset($query[$a+1]['kode_unit'])) {
							//06-08-2014
							if ($jumgrupperunit != 0)
								$rataperunit = $totalperunit/$jumgrupperunit;
							else
								$rataperunit = 0;
							
							$jum1+= $rataperunit;
							$cunit1++;
							
							$html_data.="<tr>
								<td colspan='15' align='right'><b>Rata-Rata Unit ".$query[$a]['nama_unit']."</b></td>
								<td align='right'><b>".$rataperunit."</b></td>
								</tr>";
					
							$totalperunit = 0; $jumgrupperunit = 0;
						}
						
					} //end for query
				}
				if (isset($queryutama[$aa+1]['kode']) && ($queryutama[$aa]['kode'] != $queryutama[$aa+1]['kode'])) {
					$rataperkel = $jum1/$cunit1;
					
					$html_data.="<tr>
						<th colspan='15'>TOTAL RATA-RATA</th>
						<th align='right'><b>".$rataperkel."</b></th>
						</tr>";
					$jum1=0; $cunit1=0;
				}
				else if ( !isset($queryutama[$aa+1]['kode'])) {
					$rataperkel = $jum1/$cunit1;
					
					$html_data.="<tr>
						<th colspan='15'>TOTAL RATA-RATA</th>
						<th align='right'><b>".$rataperkel."</b></th>
						</tr>";
					$jum1=0; $cunit1=0;
				}
			} // end if header
		}
	}
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		 
	$html_data.="<tr>
		<td>&nbsp;</td>
		<td>Rata-Rata</td>
		<td>&nbsp;</td>";
		if($cjanuari!=0) $ratajanuari = $tjanuari/$cjanuari; else $ratajanuari = 0;
		if($cfebruari!=0) $ratafebruari = $tfebruari/$cfebruari; else $ratafebruari = 0;
		if($cmaret!=0) $ratamaret = $tmaret/$cmaret; else $ratamaret = 0;
		if($capril!=0) $rataapril = $tapril/$capril; else $rataapril = 0;
		if($cmei!=0) $ratamei = $tmei/$cmei; else $ratamei = 0;
		if($cjuni!=0) $ratajuni = $tjuni/$cjuni; else $ratajuni = 0;
		if($cjuli!=0) $ratajuli = $tjuli/$cjuli; else $ratajuli = 0;
		if($cagustus!=0) $rataagustus = $tagustus/$cagustus; else $rataagustus = 0;
		if($cseptember!=0) $rataseptember = $tseptember/$cseptember; else $rataseptember = 0;
		if($coktober!=0) $rataoktober = $toktober/$coktober; else $rataoktober = 0;
		if($cnovember!=0) $ratanovember = $tnovember/$cnovember; else $ratanovember = 0;
		if($cdesember!=0) $ratadesember = $tdesember/$cdesember; else $ratadesember = 0;
		
		$html_data.="<td align='right'><b>".$ratajanuari."</b></td>";
		$html_data.="<td align='right'><b>".$ratafebruari."</b></td>";
		$html_data.="<td align='right'><b>".$ratamaret."</b></td>";
		$html_data.="<td align='right'><b>".$rataapril."</b></td>";
		$html_data.="<td align='right'><b>".$ratamei."</b></td>";
		$html_data.="<td align='right'><b>".$ratajuni."</b></td>";
		$html_data.="<td align='right'><b>".$ratajuli."</b></td>";
		$html_data.="<td align='right'><b>".$rataagustus."</b></td>";
		$html_data.="<td align='right'><b>".$rataseptember."</b></td>";
		$html_data.="<td align='right'><b>".$rataoktober."</b></td>";
		$html_data.="<td align='right'><b>".$ratanovember."</b></td>";
		$html_data.="<td align='right'><b>".$ratadesember."</b></td>";
		$html_data.="<td>&nbsp;</td>";
	$html_data.="</tr>";
	$html_data.="</tbody></table>";		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_rekap_presentasi_unit_tahunan_newversion";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  //=================================
  
  // 22-05-2014, update persentase supaya dikali 100
  function updatepersentase220514() {
	  $this->db->query(" update tm_presentasi_kerja_unit set total_rata= (total_rata*100) ");
	  $this->db->query(" update tm_presentasi_kerja_unit_detail_grupjahit set persentase= (persentase*100) ");
	  
	  echo "sukses update persentase dikali 100";
  }
  
  // 29-08-2014
  // ++++++++++++++++++ laporan transaksi barang unit jahit ++++++++++++++++++++++++++++
  function qcunitjahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'wip/vformlapqcunitjahit';
	$this->load->view('template',$data);
  }
  
  function viewqcunitjahit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlapqcunitjahit';
	//$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	$hasil = $this->mreport->get_mutasi_unit_qc($bulan, $tahun);
	//print_r($hasil['datautamabagus']); die();
	$data['query'] = $hasil['datautamabagus'];
	$data['query2'] = $hasil['datautamaperbaikan'];
	$data['jum_total'] = count($data['query']);
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";

	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
  
  // 30-08-2014 INPUT RETUR QC
  function addreturqc(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$submit = $this->input->post('submit', TRUE);
	
	if ($submit == "Proses") {
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$jenis = $this->input->post('jenis', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['jenis'] = $jenis;
		
		// cek apakah udh ada datanya
		$cek_data = $this->mreport->cek_returqc($bulan, $tahun, $jenis); 
			
		if ($cek_data['idnya'] == '' ) { 
			// ambil nama2 brg dari query laporan qc unit
			$hasil = $this->mreport->get_mutasi_unit_qc($bulan, $tahun);
			if ($jenis == "BAGUS")
				$data['query'] = $hasil['data_stok_bagus'];
			else
				$data['query'] = $hasil['data_stok_perbaikan'];
			
			$data['is_new'] = '1';
			if (is_array($data['query']) )
				$data['jum_total'] = count($data['query']);
			else
				$data['jum_total'] = 0;
			
			$data['isi'] = 'wip/vform2returqc';
			$this->load->view('template',$data);
		}
		else {
				// get data dari tabel tm_retur_qc_wip
				$data['query'] = $this->mreport->get_retur_qc($bulan, $tahun, $jenis);
				$data['is_new'] = '0';
				$data['jum_total'] = count($data['query']);
				$data['isi'] = 'wip/vform2returqc';
				$this->load->view('template',$data);
		} // end else
	}
	else {
		$data['isi'] = 'wip/vformreturqc';
		$data['msg'] = '';
		$this->load->view('template',$data);
	}
  }
  
  function submitreturqc() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $jenis = $this->input->post('jenis', TRUE);  
	  $jum_total = $this->input->post('jum_total', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  	  
	  if ($is_new == '1') {		  
	      for ($i=1;$i<=$jum_total;$i++)
		  {
			  // save ke tabel detail
				$seq	= $this->db->query(" SELECT id FROM tm_retur_qc_wip ORDER BY id DESC LIMIT 1 ");
				if($seq->num_rows() > 0) {
					$seqrow	= $seq->row();
					$id_retur	= $seqrow->id+1;
				}else{
					$id_retur	= 1;
				}
				
				$kode_unit = $this->input->post('kode_unit_'.$i, TRUE);
				$kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
				$iddetailx = $this->input->post('iddetail_'.$i, TRUE); // ini 0 karena new
				$qty = $this->input->post('qty_'.$i, TRUE);
				
				  $data_detail = array(
							  'id'=>$id_retur,
							  'kode_unit'=>$kode_unit,
							  'bulan'=>$bulan,
							  'tahun'=>$tahun,
							  'kode_brg_jadi'=>$kode_brg_jadi,
							  'qty'=>$qty,
							  'jenis'=>$jenis,
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl
						);
				$this->db->insert('tm_retur_qc_wip',$data_detail); 
		  } // end for
		  
		$data['msg'] = "Input retur QC untuk bulan ".$nama_bln." ".$tahun." jenis ".$jenis." sudah berhasil disimpan";
		$data['isi'] = 'wip/vformreturqc';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
				$this->db->query(" DELETE FROM tm_retur_qc_wip
									WHERE jenis = '$jenis' AND bulan = '$bulan' AND tahun = '$tahun' ");
			
			$data['msg'] = "Input retur QC untuk bulan ".$nama_bln." ".$tahun." jenis ".$jenis." sudah berhasil dihapus";
			$data['isi'] = 'wip/vformreturqc';
			$this->load->view('template',$data);
		  }
		  else { // function update				 
				 for ($i=1;$i<=$jum_total;$i++)
				  {
						$iddetailx = $this->input->post('iddetail_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$this->db->query(" UPDATE tm_retur_qc_wip SET qty='$qty' WHERE id='$iddetailx' ");
				  } // end for
		  			  
			$data['msg'] = "Input retur QC untuk bulan ".$nama_bln." ".$tahun." jenis ".$jenis." sudah berhasil diupdate";
			$data['isi'] = 'wip/vformreturqc';
			$this->load->view('template',$data);
		}
      }
  }
  
  // export excel mutasi QC
  function export_excel_qcunitjahit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$nama_bulan = $this->input->post('nama_bulan', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$hasil = $this->mreport->get_mutasi_unit_qc($bulan, $tahun);
		
		$query = $hasil['datautamabagus']; 
		$query2 = $hasil['datautamaperbaikan']; 
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
				
		$html_data = "<table border='0' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='10' align='center'>LAPORAN QC UNIT JAHIT</th>
				 </tr>
				 <tr>
					<th colspan='10' align='center'>Periode: $nama_bulan $tahun</th>
				 </tr></table><br>";
		$html_data.= "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					<th colspan='10' align='center'>BARANG BAGUS</th>
				 </tr>";
				 
		if (is_array($query)) {
			$jum1=0; $jum2=0; $cunit1=0; $temp_kel_unit = "";
			
			$i = 0;
			$jumlah=0; $cbrg=0; $jumlah2=0;
			$temp_kode_unit = "";
			
			for($a=0;$a<count($query);$a++){
				if ($query[$a]['kode'] != $temp_kel_unit) {
					$temp_kel_unit = $query[$a]['kode'];
					
					$html_data.="<tr class='judulnya'>
								<td colspan='10'><b>".$query[$a]['kode']." - ".$query[$a]['nama']."</b></td>
							</tr>
							<tr>
							 <th>No </th>
							 <th>Unit Jahit</th>
							 <th>Kode</th>
							 <th>Nama Barang Jadi</th>
							<th>Qty</th>
							<th>Retur</th>
							<th>Grade A</th>
							<th>% Grade A</th>
							<th>% Retur</th>
							<th>Keterangan</th>
						 </tr>
					</thead>";
					
					$databagus = $query[$a]['data_stok'];
						
					if (is_array($databagus)) {
						for($j=0;$j<count($databagus);$j++){					
							 $html_data.= "<tr class=\"record\">";
							 
							 if ($databagus[$j]['kode_unit_jahit'] != $temp_kode_unit) {
								$temp_kode_unit = $databagus[$j]['kode_unit_jahit'];
								$i++;
								$html_data.= "<td align='center'>$i</td>";
								$html_data.= "<td>".$databagus[$j]['kode_unit_jahit']." - ".$databagus[$j]['nama_unit_jahit']."</td>";
							 }
							 else {
								 $html_data.= "<td align='center'>&nbsp;</td>";
								 $html_data.= "<td>&nbsp;</td>";
							 }
							 
							 $jumlah+= $databagus[$j]['persen_retur'];
							 $jumlah2+= $databagus[$j]['persena'];
							 $cbrg++;
							 
							 $html_data.="<td>".$databagus[$j]['kode_brg_jadi']."</td>
							<td>".$databagus[$j]['nama_brg_jadi']."</td>
							 <td>".$databagus[$j]['qty']."</td>
							 <td>".$databagus[$j]['retur']."</td>
							 <td>".$databagus[$j]['gradea']."</td>
							 <td>".$databagus[$j]['persena']."</td>
							 <td>".$databagus[$j]['persen_retur']."</td>
							 <td>&nbsp;</td>
							</tr>";
							
							if (isset($databagus[$j+1]['kode_unit_jahit']) && ($databagus[$j]['kode_unit_jahit'] != $databagus[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
								
								$html_data.="<tr>
									<td colspan='7' align='right'><b>Rata-Rata Unit ".$databagus[$j]['nama_unit_jahit']."</b></td>
									<td><b>".$ratagradea."</b></td>
									<td><b>".$rataperunit."</b></td>
									<td>&nbsp;</td>
								</tr>";
							$jumlah=0; $jumlah2=0; $cbrg=0;
							}
							else if ( !isset($databagus[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
								
								$html_data.="<tr>
								<td colspan='7' align='right'><b>Rata-Rata Unit ".$databagus[$j]['nama_unit_jahit']."</b></td>
								<td><b>".$ratagradea."</b></td>
								<td><b>".$rataperunit."</b></td>
								<td>&nbsp;</td>
								</tr>";
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
						}
					}
					
					if (isset($query[$a+1]['kode']) && ($query[$a]['kode'] != $query[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
						
						$html_data.="<tr>
							<th colspan='7'>TOTAL RATA-RATA</th>
							<th><b>".$ratagradeaperkel."</b></th>
							<th><b>".$ratareturperkel."</b></th>
							<th>&nbsp;</th>
						</tr>";
						
						$jum1=0; $jum2=0; $cunit1=0;
					}
					else if ( !isset($query[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
						
						$html_data.="<tr>
							<th colspan='7'>TOTAL RATA-RATA</th>
							<th><b>".$ratagradeaperkel."</b></th>
							<th><b>".$ratareturperkel."</b></th>
							<th>&nbsp;</th>
						</tr>";
						$jum1=0; $jum2=0; $cunit1=0;
					}
				}
				else {
					$databagus = $query[$a]['data_stok'];
					
					//$i = 0;
					//$jumlah=0; $cbrg=0; $jumlah2=0;
					$temp_kode_unit = "";
						
					if (is_array($databagus)) {
						for($j=0;$j<count($databagus);$j++){					
							 $html_data.= "<tr class=\"record\">";
							 
							 if ($databagus[$j]['kode_unit_jahit'] != $temp_kode_unit) {
								$temp_kode_unit = $databagus[$j]['kode_unit_jahit'];
								$i++;
								$html_data.= "<td align='center'>".$i."</td>";
								$html_data.= "<td>".$databagus[$j]['kode_unit_jahit']." - ".$databagus[$j]['nama_unit_jahit']."</td>";
							 }
							 else {
								 $html_data.= "<td align='center'>&nbsp;</td>";
								 $html_data.= "<td>&nbsp;</td>";
							 }
							 
							 $jumlah+= $databagus[$j]['persen_retur'];
							 $jumlah2+= $databagus[$j]['persena'];
							 $cbrg++;
							
							$html_data.="<td>".$databagus[$j]['kode_brg_jadi']."</td>
							<td>".$databagus[$j]['nama_brg_jadi']."</td>
							 <td>".$databagus[$j]['qty']."</td>
							 <td>".$databagus[$j]['retur']."</td>
							 <td>".$databagus[$j]['gradea']."</td>
							 <td>".$databagus[$j]['persena']."</td>
							 <td>".$databagus[$j]['persen_retur']."</td>
							 <td>&nbsp;</td>
							</tr>";
							
							if (isset($databagus[$j+1]['kode_unit_jahit']) && ($databagus[$j]['kode_unit_jahit'] != $databagus[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
								
								$html_data.="<tr>
									<td colspan='7' align='right'><b>Rata-Rata Unit ".$databagus[$j]['nama_unit_jahit']."</b></td>
									<td><b>".$ratagradea."</b></td>
									<td><b>".$rataperunit."</b></td>
									<td>&nbsp;</td>
								</tr>";
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
							else if ( !isset($databagus[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
								
								$html_data.="<tr>
								<td colspan='7' align='right'><b>Rata-Rata Unit ".$databagus[$j]['nama_unit_jahit']."</b></td>
								<td><b>".$ratagradea."</b></td>
								<td><b>".$rataperunit."</b></td>
								<td>&nbsp;</td>
								</tr>";
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
						}
					}
					
					if (isset($query[$a+1]['kode']) && ($query[$a]['kode'] != $query[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
						
						$html_data.="<tr>
							<th colspan='7'>TOTAL RATA-RATA</th>
							<th><b>".$ratagradeaperkel."</b></th>
							<th><b>".$ratareturperkel."</b></th>
							<th>&nbsp;</th>
						</tr>";
						
						$jum1=0; $jum2=0; $cunit1=0;
					}
					else if ( !isset($query[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
						
						$html_data.="<tr>
							<th colspan='7'>TOTAL RATA-RATA</th>
							<th><b>".$ratagradeaperkel."</b></th>
							<th><b>".$ratareturperkel."</b></th>
							<th>&nbsp;</th>
						</tr>";
						$jum1=0; $jum2=0; $cunit1=0;
					}
				} // end if header
			}
		} // end if brg bagus
		
		$html_data.="</tbody>
					</table><br><br>";
		
		$html_data.= "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					<th colspan='10' align='center'>BARANG PERBAIKAN</th>
				 </tr>";
		
		if (is_array($query2)) {
			$jum1=0; $jum2=0; $cunit1=0; $temp_kel_unit = "";
			
			$i = 0;
			$jumlah=0; $cbrg=0; $jumlah2=0;
			$temp_kode_unit = "";
			
			for($a=0;$a<count($query2);$a++){
				if ($query2[$a]['kode'] != $temp_kel_unit) {
					$temp_kel_unit = $query2[$a]['kode'];
					
					$html_data.="<tr class='judulnya'>
								<td colspan='10'><b>".$query2[$a]['kode']." - ".$query2[$a]['nama']."</b></td>
							</tr>
							<tr>
							 <th>No </th>
							 <th>Unit Jahit</th>
							 <th>Kode</th>
							 <th>Nama Barang Jadi</th>
							<th>Qty</th>
							<th>Retur</th>
							<th>Grade A</th>
							<th>% Grade A</th>
							<th>% Retur</th>
							<th>Keterangan</th>
						 </tr>
					</thead>";
					
					$dataperbaikan = $query2[$a]['data_stok2'];
						
					if (is_array($dataperbaikan)) {
						for($j=0;$j<count($dataperbaikan);$j++){					
							 $html_data.= "<tr class=\"record\">";
							 
							 if ($dataperbaikan[$j]['kode_unit_jahit'] != $temp_kode_unit) {
								$temp_kode_unit = $dataperbaikan[$j]['kode_unit_jahit'];
								$i++;
								$html_data.= "<td align='center'>$i</td>";
								$html_data.= "<td>".$dataperbaikan[$j]['kode_unit_jahit']." - ".$dataperbaikan[$j]['nama_unit_jahit']."</td>";
							 }
							 else {
								 $html_data.= "<td align='center'>&nbsp;</td>";
								 $html_data.= "<td>&nbsp;</td>";
							 }
							 
							 $jumlah+= $dataperbaikan[$j]['persen_retur'];
							 $jumlah2+= $dataperbaikan[$j]['persena'];
							 $cbrg++;
							 
							 $html_data.="<td>".$dataperbaikan[$j]['kode_brg_jadi']."</td>
							<td>".$dataperbaikan[$j]['nama_brg_jadi']."</td>
							 <td>".$dataperbaikan[$j]['qty']."</td>
							 <td>".$dataperbaikan[$j]['retur']."</td>
							 <td>".$dataperbaikan[$j]['gradea']."</td>
							 <td>".$dataperbaikan[$j]['persena']."</td>
							 <td>".$dataperbaikan[$j]['persen_retur']."</td>
							 <td>&nbsp;</td>
							</tr>";
							
							if (isset($dataperbaikan[$j+1]['kode_unit_jahit']) && ($dataperbaikan[$j]['kode_unit_jahit'] != $dataperbaikan[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
								
								$html_data.="<tr>
									<td colspan='7' align='right'><b>Rata-Rata Unit ".$dataperbaikan[$j]['nama_unit_jahit']."</b></td>
									<td><b>".$ratagradea."</b></td>
									<td><b>".$rataperunit."</b></td>
									<td>&nbsp;</td>
								</tr>";
							$jumlah=0; $jumlah2=0; $cbrg=0;
							}
							else if ( !isset($dataperbaikan[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
								
								$html_data.="<tr>
								<td colspan='7' align='right'><b>Rata-Rata Unit ".$dataperbaikan[$j]['nama_unit_jahit']."</b></td>
								<td><b>".$ratagradea."</b></td>
								<td><b>".$rataperunit."</b></td>
								<td>&nbsp;</td>
								</tr>";
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
						}
					}
					
					if (isset($query2[$a+1]['kode']) && ($query2[$a]['kode'] != $query2[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
						
						$html_data.="<tr>
							<th colspan='7'>TOTAL RATA-RATA</th>
							<th><b>".$ratagradeaperkel."</b></th>
							<th><b>".$ratareturperkel."</b></th>
							<th>&nbsp;</th>
						</tr>";
						
						$jum1=0; $jum2=0; $cunit1=0;
					}
					else if ( !isset($query2[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
						
						$html_data.="<tr>
							<th colspan='7'>TOTAL RATA-RATA</th>
							<th><b>".$ratagradeaperkel."</b></th>
							<th><b>".$ratareturperkel."</b></th>
							<th>&nbsp;</th>
						</tr>";
						$jum1=0; $jum2=0; $cunit1=0;
					}
				}
				else {
					$dataperbaikan = $query2[$a]['data_stok2'];
					
					//$i = 0;
					//$jumlah=0; $cbrg=0; $jumlah2=0;
					$temp_kode_unit = "";
						
					if (is_array($dataperbaikan)) {
						for($j=0;$j<count($dataperbaikan);$j++){					
							 $html_data.= "<tr class=\"record\">";
							 
							 if ($dataperbaikan[$j]['kode_unit_jahit'] != $temp_kode_unit) {
								$temp_kode_unit = $dataperbaikan[$j]['kode_unit_jahit'];
								$i++;
								$html_data.= "<td align='center'>".$i."</td>";
								$html_data.= "<td>".$dataperbaikan[$j]['kode_unit_jahit']." - ".$dataperbaikan[$j]['nama_unit_jahit']."</td>";
							 }
							 else {
								 $html_data.= "<td align='center'>&nbsp;</td>";
								 $html_data.= "<td>&nbsp;</td>";
							 }
							 
							 $jumlah+= $dataperbaikan[$j]['persen_retur'];
							 $jumlah2+= $dataperbaikan[$j]['persena'];
							 $cbrg++;
							
							$html_data.="<td>".$dataperbaikan[$j]['kode_brg_jadi']."</td>
							<td>".$dataperbaikan[$j]['nama_brg_jadi']."</td>
							 <td>".$dataperbaikan[$j]['qty']."</td>
							 <td>".$dataperbaikan[$j]['retur']."</td>
							 <td>".$dataperbaikan[$j]['gradea']."</td>
							 <td>".$dataperbaikan[$j]['persena']."</td>
							 <td>".$dataperbaikan[$j]['persen_retur']."</td>
							 <td>&nbsp;</td>
							</tr>";
							
							if (isset($dataperbaikan[$j+1]['kode_unit_jahit']) && ($dataperbaikan[$j]['kode_unit_jahit'] != $dataperbaikan[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
								
								$html_data.="<tr>
									<td colspan='7' align='right'><b>Rata-Rata Unit ".$dataperbaikan[$j]['nama_unit_jahit']."</b></td>
									<td><b>".$ratagradea."</b></td>
									<td><b>".$rataperunit."</b></td>
									<td>&nbsp;</td>
								</tr>";
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
							else if ( !isset($dataperbaikan[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
								
								$html_data.="<tr>
								<td colspan='7' align='right'><b>Rata-Rata Unit ".$dataperbaikan[$j]['nama_unit_jahit']."</b></td>
								<td><b>".$ratagradea."</b></td>
								<td><b>".$rataperunit."</b></td>
								<td>&nbsp;</td>
								</tr>";
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
						}
					}
					
					if (isset($query2[$a+1]['kode']) && ($query2[$a]['kode'] != $query2[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
						
						$html_data.="<tr>
							<th colspan='7'>TOTAL RATA-RATA</th>
							<th><b>".$ratagradeaperkel."</b></th>
							<th><b>".$ratareturperkel."</b></th>
							<th>&nbsp;</th>
						</tr>";
						
						$jum1=0; $jum2=0; $cunit1=0;
					}
					else if ( !isset($query2[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
						
						$html_data.="<tr>
							<th colspan='7'>TOTAL RATA-RATA</th>
							<th><b>".$ratagradeaperkel."</b></th>
							<th><b>".$ratareturperkel."</b></th>
							<th>&nbsp;</th>
						</tr>";
						$jum1=0; $jum2=0; $cunit1=0;
					}
				} // end if header
			}
		} // end if brg perbaikan
		
		$html_data.="</tbody>
					</table><br><br>";		
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_qc_unit_newversion";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // 23-09-2014, forecast berdasarkan kelompok unit jahit
  function addforecastunit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$submit = $this->input->post('submit', TRUE);
	
	if ($submit == "Proses") {
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$id_kel_unit = $this->input->post('id_kel_unit', TRUE);
		
		// 30-09-2014, ambil nama kel unit
		$query3	= $this->db->query(" SELECT kode, nama FROM tm_kel_forecast_wip_unit WHERE id = '$id_kel_unit' ");
		$hasilrow = $query3->row();
		$kodekel	= $hasilrow->kode;
		$namakel	= $hasilrow->nama;

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		
		
		// 30-09-2014
		// ambil list unit jahit di tabel kel_forecast_wip_unit_listunit
		$query3	= $this->db->query(" SELECT a.kode_unit, b.nama FROM tm_kel_forecast_wip_unit_listunit a 
							INNER JOIN tm_unit_jahit b ON a.kode_unit = b.kode_unit 
							WHERE a.id_kel_forecast_wip_unit = '".$id_kel_unit."' ORDER BY a.kode_unit ");
		$namaunit=$kodekel."-".$namakel." ( Unit: ";
		if ($query3->num_rows() > 0){
			$hasil3 = $query3->result();
				
			foreach ($hasil3 as $row3) {
				$namaunit	.= $row3->kode_unit."-".$row3->nama." ";
			}
		}
		$namaunit.=")";
		
		$cek_data = $this->mreport->cek_forecast_unit($bulan, $tahun, $id_kel_unit); 
			
		if ($cek_data['idnya'] == '' ) { 
			$data['query'] = '';
			
			$data['is_new'] = '1';
			$data['jum_total'] = 0;
			
			$data['id_kel_unit'] = $id_kel_unit;
			$data['namaunit'] = $namaunit;
			$data['isi'] = 'wip/vform2forecastunit';
			$this->load->view('template',$data);
		}
		else {
				// get data dari tabel tm_forecast_wip_unit
				$data['query'] = $this->mreport->get_forecast_unit($bulan, $tahun, $id_kel_unit);
				$data['is_new'] = '0';
				$data['jum_total'] = count($data['query']);
				$data['id_kel_unit'] = $id_kel_unit;
				$data['namaunit'] = $namaunit;
				$data['isi'] = 'wip/vform2forecastunit';
				$this->load->view('template',$data);
		} // end else
	}
	else {
		$data['isi'] = 'wip/vformforecastunit';
		$data['msg'] = '';
		//$data['bulan_skrg'] = date("m");
		$data['list_kel_unit'] = $this->mreport->get_kel_unit_jahit();
		$this->load->view('template',$data);
	}
  }
  
  function submitforecastunit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $id_kel_unit = $this->input->post('id_kel_unit', TRUE);
	  $namaunit = $this->input->post('namaunit', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE)-1;  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit3 = $this->input->post('submit3', TRUE);
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  // 30-09-2014, ambil nama kel unit
	  $query3	= $this->db->query(" SELECT kode, nama FROM tm_kel_forecast_wip_unit WHERE id = '$id_kel_unit' ");
	  $hasilrow = $query3->row();
	  $kodekel	= $hasilrow->kode;
	  $namakel	= $hasilrow->nama;
	  	  
	  if ($is_new == '1') {
	      // insert ke tabel tm_forecast_wip_unit
	        $seq	= $this->db->query(" SELECT id FROM tm_forecast_wip_unit ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$id_fc	= $seqrow->id+1;
			}else{
				$id_fc	= 1;
			}
	      $data_header = array(
			  'id'=>$id_fc,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'id_kel_forecast_wip_unit'=>$id_kel_unit,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
		  $this->db->insert('tm_forecast_wip_unit',$data_header);
		  	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mreport->saveforecastunit($is_new, $id_fc, $this->input->post('iddetail_'.$i, TRUE),
			 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
			 $this->input->post('fc_'.$i, TRUE),
			 $this->input->post('ket_'.$i, TRUE)
			 );
		  }
		  
		$data['msg'] = "Input forecast kelompok unit jahit ".$kodekel."-".$namakel." untuk bulan ".$nama_bln." ".$tahun." sudah berhasil disimpan";
		$data['isi'] = 'wip/vformforecastunit';
		$data['list_kel_unit'] = $this->mreport->get_kel_unit_jahit();
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit3 != '') { // function hapus
			  // ambil data id dari tabel tm_forecast_wip_unit
			//  $jumstring = strlen($kodeunit);
			//  $new_kodeunit = substr($kodeunit, 0, $jumstring-1);
			  
			  $sql = " SELECT id FROM tm_forecast_wip_unit WHERE bulan = '$bulan' AND tahun = '$tahun'
						AND id_kel_forecast_wip_unit = '$id_kel_unit' ";
			  $query2	= $this->db->query($sql);
			  $hasilrow = $query2->row();
			  $id_fc	= $hasilrow->id; 
			 
			 
			$this->db->query(" delete from tm_forecast_wip_unit_detail where id_forecast_wip_unit = '$id_fc' ");
			$this->db->query(" delete from tm_forecast_wip_unit where id = '$id_fc' ");
			
			$data['msg'] = "Input forecast kelompok unit jahit ".$kodekel."-".$namakel." untuk bulan ".$nama_bln." ".$tahun." sudah berhasil dihapus";
			$data['isi'] = 'wip/vformforecastunit';
			$data['list_kel_unit'] = $this->mreport->get_kel_unit_jahit();
			$this->load->view('template',$data);
		  }
		  else { // function update
			  //echo $jum_data; die();
			  // update ke tabel tm_forecast_wip_unit
			   // ambil data terakhir di tabel tm_forecast_wip_unit
				 $query2	= $this->db->query(" SELECT id FROM tm_forecast_wip_unit WHERE bulan = '$bulan' AND tahun = '$tahun'
							AND id_kel_forecast_wip_unit = '$id_kel_unit' ");
				 $hasilrow = $query2->row();
				 $id_fc	= $hasilrow->id;
				 
				 $this->db->query(" UPDATE tm_forecast_wip_unit SET tgl_update = '$tgl' 
							where id = '$id_fc' ");
				 $this->db->query(" DELETE FROM tm_forecast_wip_unit_detail WHERE id_forecast_wip_unit='$id_fc' ");
			  for ($i=1;$i<=$jum_data;$i++)
			  {
				 $this->mreport->saveforecastunit($is_new, $id_fc, $this->input->post('iddetail_'.$i, TRUE),
				 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
				 $this->input->post('fc_'.$i, TRUE),
				 $this->input->post('ket_'.$i, TRUE)
				 );
			  }
			  
			$data['msg'] = "Input forecast kelompok unit jahit ".$kodekel."-".$namakel." untuk bulan ".$nama_bln." ".$tahun." sudah berhasil diupdate";
			$data['isi'] = 'wip/vformforecastunit';
			$data['list_kel_unit'] = $this->mreport->get_kel_unit_jahit();
			$this->load->view('template',$data);
		}
      }
  } // 
  
  function forecastvsscheduleunit(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'wip/vformlapforecastvsscheduleunit';
	$data['list_kel_unit'] = $this->mreport->get_kel_unit_jahit();
	$this->load->view('template',$data);
  }
  
  function viewforecastvsscheduleunit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewforecastvsscheduleunit';
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$id_kel_unit = $this->input->post('id_kel_unit', TRUE);
	
	// 30-09-2014, ambil nama kel unit
	  $query3	= $this->db->query(" SELECT kode, nama FROM tm_kel_forecast_wip_unit WHERE id = '$id_kel_unit' ");
	  $hasilrow = $query3->row();
	  $kodekel	= $hasilrow->kode;
	  $namakel	= $hasilrow->nama;
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		$data['nama_bulan'] = $nama_bln;
	
	
	$data['query'] = $this->mreport->get_forecastvsscheduleunit($bulan, $tahun, $id_kel_unit);
	
	// 30-09-2014
		// ambil list unit jahit di tabel kel_forecast_wip_unit_listunit
		$query3	= $this->db->query(" SELECT a.kode_unit, b.nama FROM tm_kel_forecast_wip_unit_listunit a 
							INNER JOIN tm_unit_jahit b ON a.kode_unit = b.kode_unit 
							WHERE a.id_kel_forecast_wip_unit = '".$id_kel_unit."' ORDER BY a.kode_unit ");
		$namaunit=$kodekel."-".$namakel." ( Unit: ";
		if ($query3->num_rows() > 0){
			$hasil3 = $query3->result();
				
			foreach ($hasil3 as $row3) {
				$namaunit	.= $row3->kode_unit."-".$row3->nama." ";
			}
		}
		$namaunit.=")";
	
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['id_kel_unit'] = $id_kel_unit;
	$data['namaunit'] = $namaunit;
	$this->load->view('template',$data);
  }
  
  function export_excel_forecastvsscheduleunit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$nama_bulan = $this->input->post('nama_bulan', TRUE);
		$namaunit = $this->input->post('namaunit', TRUE);
		$id_kel_unit = $this->input->post('id_kel_unit', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_forecastvsscheduleunit($bulan, $tahun, $id_kel_unit);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='10' align='center'>PT TIRAI PELANGI NUSANTARA</th>
				 </tr>
				 <tr>
					<th colspan='10' align='center'>LAPORAN FORECAST BULAN ".strtoupper($nama_bulan)." ".$tahun."</th>
				 </tr>
				 <tr>
					<th colspan='10' align='center'>Kelompok Unit Jahit: ".$namaunit."</th>
				 </tr>
				 <tr>
					<th width='3%'>No </th>
					 <th width='10%'>Kode</th>
					 <th width='15%'>Nama Barang Jadi</th>
					 <th width='5%'>FC Produksi</th>
					 <th width='5%'>Schedule</th>
					 <th width='5%'>Saldo Awal</th>
					 <th width='5%'>Barang Masuk</th>
					 <th width='5%'>%Produksi <br>vs Schedule</th>
					 <th>%Brg Masuk <br>vs Schedule+<br>Saldo Awal</th>
					 <th width='20%'>Keterangan</th>
				 </tr>";
				 // <th width='5%'>%Produksi <br>vs Forecast</th>
		if (is_array($query)) {
			$no=1;
			$temp_kodekel = "";
			for($a=0;$a<count($query);$a++){
				if ($temp_kodekel != $query[$a]['kode_kel']) {
					 $temp_kodekel = $query[$a]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='10'>&nbsp;<b>".$query[$a]['kode_kel']." - ".$query[$a]['nama_kel']."</b></td>
					</tr>";
				 }
				 
				$html_data.= "<tr>
				<td align='center'>".$no."</td>
				<td>".$query[$a]['kode_brg_jadi']."</td>
				<td>".$query[$a]['nama_brg_jadi']."</td>
				<td>".$query[$a]['fc']."</td>
				<td>".$query[$a]['schedule']."</td>
				<td>".$query[$a]['saldo_awal_bgs']."</td>
				<td>".$query[$a]['brgmasuk']."</td>
				<td>".$query[$a]['persenschedule']."</td>
				<td>".$query[$a]['persenforecast2']."</td>
				<td>".$query[$a]['keterangan']."</td>
				</tr>";
				// <td>".$query[$a]['persenforecast']."</td>
				$no++;
			}
				$html_data.="</tbody></table><br><br>";
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_forecastvsscheduleunit";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // 20-10-2014, joko dilantik
  function laptransaksiglobal(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'wip/vformlaptransaksiglobal';
	$this->load->view('template',$data);
  }
  
  function viewlaptransaksiglobal(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlaptransaksiglobal';
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		$data['nama_bulan'] = $nama_bln;
	
	$data['query'] = $this->mreport->get_transaksiglobal($bulan, $tahun);
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$this->load->view('template',$data);
  }
  
  // 21-10-2014
  function export_excel_laptransaksiglobal() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$nama_bulan = $this->input->post('nama_bulan', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_transaksiglobal($bulan, $tahun);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='5' align='center'>PT TIRAI PELANGI NUSANTARA</th>
				 </tr>
				 <tr>
					<th colspan='5' align='center'>LAPORAN KELUAR MASUK BARANG GLOBAL</th>
				 </tr>
				 <tr>
					<th colspan='5' align='center'>PERIODE ".strtoupper($nama_bulan)." ".$tahun."</th>
				 </tr>
				 <tr>
					 <th width='10%'>Minggu Ke</th>
					 <th width='20%' style='white-space:nowrap;'>Produksi Ke Unit Jahit</th>
					 <th width='20%' style='white-space:nowrap;'>Unit Jahit Ke WIP</th>
					 <th width='20%' style='white-space:nowrap;'>WIP Ke Packing</th>
					 <th width='20%' style='white-space:nowrap;'>Gudang Ke Distributor</th>
				 </tr>";
		if (is_array($query)) {
			for($j=0;$j<count($query);$j++){
				$pisah1 = explode("-", $query[$j]['awal']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$awal = $tgl1."-".$bln1."-".$thn1;
							
				$pisah1 = explode("-", $query[$j]['akhir']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$akhir = $tgl1."-".$bln1."-".$thn1;
				
				$html_data.=" <tr>
					<td align='center'>".$query[$j]['minggu']." (".$awal." s/d ".$akhir.")</td>
					<td align='right'>".$query[$j]['produksi2unit1']."</td>
					<td align='right'>".$query[$j]['jahit2wip1']."</td>
					<td align='right'>".$query[$j]['wip2packing1']."</td>
					<td align='right'>".$query[$j]['gdjadi2dist1']."</td>
				</tr>";
			}
		}
		
		$html_data.="</table><br><br>";
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_transaksiglobal";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // 14-01-2015
  function export_excel_transaksiunitjahit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$nama_bulan = $this->input->post('nama_bulan', TRUE);
		
		$unit_jahit = $this->input->post('unit_jahit', TRUE);
		$nama_unit = $this->input->post('nama_unit', TRUE);
		
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_transaksi_unit_jahit($bulan, $tahun, $unit_jahit);
		
	// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
	$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='9' align='center'>LAPORAN KELUAR MASUK BARANG DI UNIT JAHIT ".$nama_unit."</th>
				 </tr>
				 <tr>
					<th colspan='9' align='center'>PERIODE ".strtoupper($nama_bulan)." ".$tahun."</th>
				 </tr></table><br>";
				 
	$nomor = 1;
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			$html_data.= "<b>".$query[$a]['kode_brg_jadi']." - ".$query[$a]['nama_brg_jadi']."</b>"."<br>";
			
		$html_data.= "<table border='1' cellpadding= '1' cellspacing = '1' width='70%'>
		<tr>
			<td colspan='9' align='center'><b>BARANG BAGUS</b></td>
		</tr>
	 <tr>
		 <th width='5%' rowspan='2'>Tgl</th>
		 <th width='5%' rowspan='2'>Ket</th>
		 <th width='10%' colspan='2'>No Bukti</th>
		 <th width='10%' colspan='2'>Brg Jadi</th>
		 <th width='10%' colspan='2'>Retur Bhn Baku</th>
		 <th width='10%' rowspan='2'>Saldo</th>
	 </tr>
	 <tr>
		<th>Masuk</th>
		<th>Keluar</th>
		<th>Masuk</th>
		<th>Keluar</th>
		<th>Masuk</th>
		<th>Keluar</th>
	</tr>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>";
			
			$data_so_warna = $query[$a]['data_so_warna'];
			if (is_array($data_so_warna)) {
				for($zz=0;$zz<count($data_so_warna);$zz++){ 
					$html_data.= "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['jum_bagus']."<br>";
				}
			}
		$html_data.="</td></tr>";
		
		$data_tabel1 = $query[$a]['data_tabel1'];
		if (is_array($data_tabel1)) {
			for($j=0;$j<count($data_tabel1);$j++){
				$html_data.="<tr>
				<td>".$data_tabel1[$j]['tgl_sj']."</td>
				
				<td>";
				if ($data_tabel1[$j]['masuk'] == "ya") $html_data.= "Masuk"; else $html_data.= "Keluar";
				$html_data.="</td>";
		$html_data.="<td>";
		if ($data_tabel1[$j]['masuk'] == "ya") $html_data.= "&nbsp;".$data_tabel1[$j]['no_sj']; else $html_data.= "&nbsp;";
		$html_data.="</td>";
		
		$html_data.="<td>";
		if ($data_tabel1[$j]['keluar'] == "ya") $html_data.= "&nbsp;".$data_tabel1[$j]['no_sj']; else $html_data.= "&nbsp;";
		$html_data.="</td>";
		$html_data.="<td>";
		if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['is_bonmkeluarcutting'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						$html_data.= "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 
		$html_data.="</td>";
			
		$html_data.="<td>"; if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['is_masukwip'] == '1') {
			$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
			if (is_array($data_tabel1_perwarna)) {
				for($z=0;$z<count($data_tabel1_perwarna);$z++){
					$html_data.= "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
				}
			}
		}
		 
		$html_data.="</td>";
		
		$html_data.="<td>"; if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['is_bonmkeluarcutting2'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						$html_data.= "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 
		 $html_data.="</td>";
		 $html_data.="<td>"; if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['is_sjmasukbhnbakupic'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						$html_data.= "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 $html_data.="</td>";
		 
		 $html_data.="<td>";
				$data_tabel1_saldo_perwarna = $data_tabel1[$j]['data_tabel1_saldo_perwarna'];
				if (is_array($data_tabel1_saldo_perwarna)) {
					for($z=0;$z<count($data_tabel1_saldo_perwarna);$z++){
						$html_data. "&nbsp;".$data_tabel1_saldo_perwarna[$z]['nama_warna'].": ".$data_tabel1_saldo_perwarna[$z]['saldo']."<br>";
					}
				}
		 $html_data.="</td>";
		$html_data.="</tr>";
			}
		}
		
		$html_data.="<tr>
			<td colspan='2' align='center'><b>Total/Bulan</b></td>
			<td colspan='2' align='center'><b>Total</b></td>
			<td>"; 
		$data_warna = $query[$a]['data_warna'];
		if (is_array($data_warna)) {
			for($zz=0;$zz<count($data_warna);$zz++){ 
				$html_data.= "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk1']."<br>";
			}
		}
		$html_data.="</td>";
		$html_data.="<td>"; 
		$data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					$html_data.= "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar1']."<br>";
				}
			}
		$html_data.="</td>";
		
		$html_data.="<td>";
		$data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					$html_data.= "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_retur1']."<br>";
				}
			}
		$html_data.="</td>";
		$html_data.="<td>"; 
		$data_warna = $query[$a]['data_warna'];
		if (is_array($data_warna)) {
			for($zz=0;$zz<count($data_warna);$zz++){ 
				$html_data.= "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_retur1']."<br>";
			}
		}
		$html_data.="</td>";
		$html_data.="<td>"; 
		$data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					$html_data.= "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['saldo']."<br>";
				}
			}
		$html_data.="</td></tr>
	</table>";
	
	$html_data.=" <table border='1' cellpadding= '1' cellspacing = '1' width='70%'>
		<tr>
			<td colspan='7' align='center'><b>BARANG PERBAIKAN</b></td>
		</tr>
	 <tr>
		 <th width='5%' rowspan='2'>Tgl</th>
		 <th width='5%' rowspan='2'>Ket</th>
		 <th width='10%' colspan='2'>No Bukti</th>
		 <th width='10%' colspan='2'>Retur Brg Jadi</th>
		 <th width='10%' rowspan='2'>Saldo</th>
	 </tr>
	 <tr>
		<th>Masuk</th>
		<th>Keluar</th>
		<th>Masuk</th>
		<th>Keluar</th>

	</tr>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>";
			$data_so_warna = $query[$a]['data_so_warna'];
			if (is_array($data_so_warna)) {
				for($zz=0;$zz<count($data_so_warna);$zz++){ 
					$html_data.= "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['jum_perbaikan']."<br>";
				}
			}
		
			$html_data.="</td></tr>";
			
	
		$data_tabel2 = $query[$a]['data_tabel2'];
		if (is_array($data_tabel2)) {
			for($j=0;$j<count($data_tabel2);$j++){

	$html_data.="<tr>
		<td>".$data_tabel2[$j]['tgl_sj']."</td>
		<td>"; if ($data_tabel2[$j]['masuk'] == "ya") $html_data.= "&nbsp;Masuk"; else $html_data.= "&nbsp;Keluar"; 
		$html_data.="</td>
		<td>"; if ($data_tabel2[$j]['masuk'] == "ya") $html_data.= "&nbsp;".$data_tabel2[$j]['no_sj']; else $html_data.= "&nbsp;";
		$html_data.="</td>
		<td>"; if ($data_tabel2[$j]['keluar'] == "ya") $html_data.= "&nbsp;".$data_tabel2[$j]['no_sj']; else $html_data.= "&nbsp;";
		$html_data.="</td>
		<td>"; if ($data_tabel2[$j]['masuk'] == "ya" && $data_tabel2[$j]['is_keluarwip'] == '1') {
				$data_tabel2_perwarna = $data_tabel2[$j]['data_tabel2_perwarna'];
				if (is_array($data_tabel2_perwarna)) {
					for($z=0;$z<count($data_tabel2_perwarna);$z++){
						$html_data.= "&nbsp;".$data_tabel2_perwarna[$z]['nama_warna'].": ".$data_tabel2_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 
		 $html_data.="</td>
		 <td>"; if ($data_tabel2[$j]['keluar'] == "ya" && $data_tabel2[$j]['is_masukwip'] == '1') {
				$data_tabel2_perwarna = $data_tabel2[$j]['data_tabel2_perwarna'];
				if (is_array($data_tabel2_perwarna)) {
					for($z=0;$z<count($data_tabel2_perwarna);$z++){
						$html_data.= "&nbsp;".$data_tabel2_perwarna[$z]['nama_warna'].": ".$data_tabel2_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 
		 $html_data.="</td>
		 
		 <td>";
				$data_tabel2_saldo_perwarna = $data_tabel2[$j]['data_tabel2_saldo_perwarna'];
				if (is_array($data_tabel2_saldo_perwarna)) {
					for($z=0;$z<count($data_tabel2_saldo_perwarna);$z++){
						$html_data.= "&nbsp;".$data_tabel2_saldo_perwarna[$z]['nama_warna'].": ".$data_tabel2_saldo_perwarna[$z]['saldo']."<br>";
					}
				}

		 $html_data.="</td>	 
	</tr>";
	
			}
		}
	
	$html_data.="<tr>
			<td colspan='2' align='center'><b>Total/Bulan</b></td>
			<td colspan='2' align='center'><b>Total</b></td>
			<td>"; $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					$html_data.= "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_retur2']."<br>";
				}
			}
		$html_data.="</td>
		<td>"; $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					$html_data.= "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_retur2']."<br>";
				}
			}
		$html_data.="</td>
		<td>"; $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					$html_data.= "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['saldo_retur']."<br>";
				}
			}
		$html_data.="</td>
		</tr>
	</table>
	";
	  }
	}
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_transaksiunitjahit";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  function create_tabel_returqc() {
	  // skrip create tabel retur_qc_wip
	  $sql = " 
		CREATE TABLE tm_retur_qc_wip
		(
		  id integer NOT NULL,
		  kode_unit character varying(3),
		  bulan character varying(2),
		  tahun integer,
		  kode_brg_jadi character varying(20) NOT NULL,
		  qty numeric(10,0),
		  jenis character varying(10), -- STATIS, hanya 2:...
		  tgl_input timestamp without time zone,
		  tgl_update timestamp without time zone,
		  CONSTRAINT pk_retur_qc PRIMARY KEY (id)
		)
		WITH (
		  OIDS=FALSE
		);
		ALTER TABLE tm_retur_qc_wip
		  OWNER TO postgres;
		COMMENT ON COLUMN tm_retur_qc_wip.jenis IS 'STATIS, hanya 2:
		BAGUS
		PERBAIKAN';
		 ";
		
		$this->db->query($sql);
		echo "sukses bikin tabel";
	}
	
	function cekdatabonmkeluarcutting_nostokunit() {
		for ($kode_unit = 1; $kode_unit <= 39; $kode_unit++) {
			if ($kode_unit < 10)
				$kode_unit = "0".$kode_unit;
			
			
			$queryxx = $this->db->query(" select distinct a.id from tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting a 
					where a.id = b.id_bonmkeluarcutting AND 
					a.kode_unit='$kode_unit' AND b.kode_brg_jadi NOT IN (select kode_brg_jadi from tm_stok_unit_jahit where kode_unit='$kode_unit')
					ORDER BY a.id ASC ");
			
			if($queryxx->num_rows() > 0) {
				$hasil	= $queryxx->row();
				$id_bonm	= $hasil->id;
				//echo $kode_unit." ".$no_bonm." ".$tgl_bonm." ".$jenis_keluar." ".$kode_brg_jadi."<br>";
				
				$queryxx2 = $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail WHERE id_bonmkeluarcutting='$id_bonm' ");
				if($queryxx2->num_rows() > 0) {
					$hasilxx2 = $queryxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						$id_detail = $rowxx2->id;
						$this->db->query(" DELETE FROM tm_bonmkeluarcutting_detail_warna WHERE id_bonmkeluarcutting_detail='$id_detail' ");
					}
				}
				$this->db->query(" DELETE FROM tm_bonmkeluarcutting_detail WHERE id_bonmkeluarcutting='$id_bonm' ");
				$this->db->query(" DELETE FROM tm_bonmkeluarcutting WHERE id='$id_bonm' ");
			}
			
		} // end for
		echo "sukses hapus";
	}
	
	// 26-09-2014
	function create_tabel_forecastunit() {
		$sql = " 
DROP TABLE tm_forecast_wip_unit;
DROP TABLE tm_forecast_wip_unit_detail;
DROP TABLE tm_forecast_wip_unit_listunit;

CREATE TABLE tm_kel_forecast_wip_unit
(
  id serial NOT NULL,
  kode character varying(2) NOT NULL,
  nama character varying(30),
  keterangan character varying(50),
  tgl_input timestamp without time zone,
  tgl_update timestamp without time zone,
  CONSTRAINT pk_kel_forecast_wip_unit PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tm_kel_forecast_wip_unit
  OWNER TO postgres;
  
  CREATE TABLE tm_kel_forecast_wip_unit_listunit
(
  id serial NOT NULL,
  id_kel_forecast_wip_unit integer,
  kode_unit character varying(3),
  CONSTRAINT pk_tm_kel_forecast_wip_unit_listunit PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tm_kel_forecast_wip_unit_listunit
  OWNER TO postgres;

CREATE TABLE tm_forecast_wip_unit
(
  id integer NOT NULL,
  bulan character varying(2),
  tahun integer,
  id_kel_forecast_wip_unit integer,
  tgl_input timestamp without time zone,
  tgl_update timestamp without time zone,
  CONSTRAINT pk_tm_forecast_wip_unit PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tm_forecast_wip_unit
  OWNER TO postgres;
  

CREATE TABLE tm_forecast_wip_unit_detail
(
  id integer NOT NULL,
  id_forecast_wip_unit integer,
  kode_brg_jadi character varying(20),
  qty integer,
  keterangan character varying(30),
  CONSTRAINT pk_tm_forecast_wip_unit_detail PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tm_forecast_wip_unit_detail
  OWNER TO postgres;
  		 ";
		 $this->db->query($sql);
		 echo "sukses create tabel kelompok unit jahit dan forecast unit";
	} */
  
}
