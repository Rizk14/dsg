<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
			
			$data['form_title_detail_pendding']	= $this->lang->line('form_title_detail_pendding');
			$data['page_title_pendding']	= $this->lang->line('page_title_pendding');
			$data['list_pend_tgl_mulai_op']	= $this->lang->line('list_pend_tgl_mulai_op');
			$data['list_pend_sort_brg']	= $this->lang->line('list_pend_sort_brg');
			$data['list_pend_sort_dftr_brg']	= $this->lang->line('list_pend_sort_dftr_brg');
			$data['list_pend_tgl_op']	= $this->lang->line('list_pend_tgl_op');
			$data['list_pend_no_op']	= $this->lang->line('list_pend_no_op');
			$data['list_pend_cabang']	= $this->lang->line('list_pend_cabang');
			$data['list_pend_kd_brg']	= $this->lang->line('list_pend_kd_brg');
			$data['list_pend_nm_brg']	= $this->lang->line('list_pend_nm_brg');
			$data['list_pend_do']	= $this->lang->line('list_pend_do');
			$data['list_pend_sisa']	= $this->lang->line('list_pend_sisa');
			$data['list_pend_amount']	= $this->lang->line('list_pend_amount');
			$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$this->load->model('listbrgterkirim/mclass');
			$data['isi']	= 'listbrgterkirim/vmainform';	
			$this->load->view('template',$data);		
		
		
	}
	
	function carilistpendding() {		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
	$db2=$this->load->database('db_external', TRUE);
		$data['form_title_detail_pendding']	= $this->lang->line('form_title_detail_pendding');
		$data['page_title_pendding']	= $this->lang->line('page_title_pendding');
		$data['list_pend_tgl_mulai_op']	= $this->lang->line('list_pend_tgl_mulai_op');
		$data['list_pend_sort_brg']	= $this->lang->line('list_pend_sort_brg');
		$data['list_pend_sort_dftr_brg']	= $this->lang->line('list_pend_sort_dftr_brg');
		$data['list_pend_tgl_op']	= $this->lang->line('list_pend_tgl_op');
		$data['list_pend_no_op']	= $this->lang->line('list_pend_no_op');
		$data['list_pend_cabang']	= $this->lang->line('list_pend_cabang');
		$data['list_pend_kd_brg']	= $this->lang->line('list_pend_kd_brg');
		$data['list_pend_nm_brg']	= $this->lang->line('list_pend_nm_brg');
		$data['list_pend_do']	= $this->lang->line('list_pend_do');
		$data['list_pend_sisa']	= $this->lang->line('list_pend_sisa');
		$data['list_pend_amount']	= $this->lang->line('list_pend_amount');
		$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lbrgpendding']	= "";
		
		if($this->input->post('d_op_first') || 
			$this->input->post('d_op_last') || 
			$this->input->post('f_stop_produksi') || 
			$this->input->post('iop') || 
			$this->input->post('imotif') || 
			$this->input->post('ido'))
		{
			$d_op_first		= $this->input->post('d_op_first');
			$d_op_last		= $this->input->post('d_op_last');
			$iop	= $this->input->post('iop');
			$ido	= $this->input->post('ido');
			$imotif	= $this->input->post('imotif');
			$fstopproduksi = $this->input->post('fstopproduksi');
			
			if($iop=='')
				$iop	= "kosong";
			
			if($ido=='')
				$ido	= "kosong";
			
			if($imotif=='')
				$imotif	= "kosong";
			
			if($d_op_first=='')
				$d_op_first	= "kosong";
			
			if($d_op_last=='')
				$d_op_last	= "kosong";
			
			if($iop!='kosong'){
				$iorder	= " AND a.i_op_code='".$this->input->post('iop')."' ";
			}else{
				$iorder	= "";
			}
			
			if($imotif!='kosong'){
				$ordermotif	= " AND b.i_product='".$this->input->post('imotif')."' ";
			}else{
				$ordermotif	= "";
			}

			if($ido!='kosong'){
				$idonya	= " AND e.i_do_code='".$this->input->post('ido')."' ";
			}else{
				$idonya	= "";
			}
						
			if(substr($d_op_first,2,1)=="/" && $d_op_first!="kosong"){ // dd/mm/YYYY
				$tgl_awal	= substr($d_op_first,6,4).'-'.substr($d_op_first,3,2).'-'.substr($d_op_first,0,2);
				$data['tglmulai']	= $d_op_first;
			}else{
				$tgl_awal	= date("Y").'-'.date("m").'-'.'01';
				$data['tglmulai']	= '';
			}
			
			if(substr($d_op_last,2,1)=="/" && $d_op_last!="kosong"){
				$tgl_akhir	= substr($d_op_last,6,4).'-'.substr($d_op_last,3,2).'-'.substr($d_op_last,0,2);
				$data['tglakhir']	= $d_op_last;
			}else{
				$tgl_akhir	= date("Y").'-'.date("m").'-'.date("d");
				$data['tglakhir']	= '';
			}
		}
		
		if(!$this->input->post('d_op_first') && 
		   !$this->input->post('d_op_last') &&
		   !$this->input->post('iop') &&
		   !$this->input->post('ido') && 
		   !$this->input->post('imotif') && 
		   !$this->input->post('fstopproduksi')) {

			$d_op_first	= $this->uri->segment(4);
			$d_op_last	= $this->uri->segment(5);
			$iop	= $this->uri->segment(6);
			$imotif	= $this->uri->segment(7);
			$fstopproduksi = $this->uri->segment(8);
			$ido 	= $this->uri->segment(9);
			
			if($iop!='kosong')
				$iorder	= " AND a.i_op_code='".$iop."' ";
			else
				$iorder	= "";
			
			if($imotif!='kosong')
				$ordermotif	= " AND b.i_product='".$imotif."' ";
			else
				$ordermotif	= "";

			if($ido!='kosong')
				$idonya	= " AND e.i_do_code='".$ido."' ";
			else
				$idonya	= "";
								
			if(substr($d_op_first,4,1)=='-' && $d_op_first!='kosong'){ // YYYY-mm-dd
				$tgl_awal	= $d_op_first;
				$data['tglmulai']	= substr($d_op_first,8,2).'/'.substr($d_op_first,5,2).'/'.substr($d_op_first,0,4);
			}else{
				$tgl_awal	= date("Y").'-'.date("m").'-'.'01';
				$data['tglmulai']	= '';
			}
			
			if(substr($d_op_last,4,1)=='-' && $d_op_last!='kosong'){ // YYYY-mm-dd
				$tgl_akhir	= $d_op_last;
				$data['tglakhir']	= substr($d_op_last,8,2).'/'.substr($d_op_last,5,2).'/'.substr($d_op_last,0,4);
			}else{
				$tgl_akhir	= date("Y").'-'.date("m").'-'.date("d");
				$data['tglakhir']	= '';
			}
		}

		if($d_op_first!='kosong' && $d_op_last!='kosong') {
			$fdate = " (a.d_op BETWEEN '$tgl_awal' AND '$tgl_akhir') AND ";
		}else{
			$fdate = " ";
		}
					
		$uriorder	= $iop;
		$urimotif	= $imotif;
		$uritglawal	= $tgl_awal;
		$uritglakhir= $tgl_akhir;
		$urifstopproduksi = $fstopproduksi;
		$urido		= $ido;
		
		$data['stp']			= $fstopproduksi=='t'?' checked ':'';
		$data['orderpembelian']	= $uriorder=='kosong'?'':$uriorder;
		$data['productmotif']	= $urimotif=='kosong'?'':$urimotif;
		$data['ido']			= $urido=='kosong'?'':$urido;
		
		/*	
		$query	= $db2->query(" SELECT  a.d_op AS dop,
			a.i_op_code AS iopcode,
			a.i_branch AS ibranch,
			b.i_product AS iproduct,
			c.e_product_motifname AS motifname,
			(b.n_count-b.n_residual) AS dobarang,
			b.n_residual AS sisa,
			d.v_unitprice AS hjp,
			(b.n_residual*d.v_unitprice) AS amount,
			a.d_op FROM tm_op_item b 
			
			LEFT JOIN tm_op a ON a.i_op=b.i_op
			INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
			WHERE ".$fdate." a.f_op_cancel='f' AND b.f_do_created='t' AND d.f_stop_produksi='$fstopproduksi' ".$iorder." ".$ordermotif." ");
		
		*/
		
		$query	= $db2->query(" SELECT  a.d_op AS dop,
			a.i_op_code AS iopcode,
			a.i_branch AS ibranch,
			b.i_product AS iproduct,
			c.e_product_motifname AS motifname,
			(b.n_count-b.n_residual) AS dobarang,
			b.n_residual AS sisa,
			d.v_unitprice AS hjp,
			(b.n_residual*d.v_unitprice) AS amount,
			a.d_op FROM tm_op_item b
			
			LEFT JOIN tm_op a ON a.i_op=b.i_op
			INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
			
			WHERE ".$fdate." a.f_op_cancel='f' AND (b.f_do_created='t' 
OR (b.f_do_created='f' AND (b.n_count-b.n_residual) > 0) AND b.n_residual > 0) AND d.f_stop_produksi='$fstopproduksi' ".$iorder." ".$ordermotif."
			
			ORDER BY a.i_op_code DESC, b.i_product ASC ");
		
		$query2	= $db2->query(" SELECT a.i_op, a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount
				
				FROM tm_op_item b
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do_item f ON f.i_op=a.i_op
				INNER JOIN tm_do e ON e.i_do=f.i_do
				
				WHERE ".$fdate." a.f_op_cancel='f' AND (b.f_do_created='t' 
OR (b.f_do_created='f' AND (b.n_count-b.n_residual) > 0) AND b.n_residual > 0) AND d.f_stop_produksi='$fstopproduksi' ".$iorder." ".$ordermotif." ".$idonya."
				
				GROUP BY a.i_op, a.d_op, a.i_op_code, a.i_branch, b.i_product, c.e_product_motifname, b.n_count, b.n_residual, d.v_unitprice
				
				ORDER BY a.i_op_code DESC, b.i_product ASC ");
		
		if($urido!='kosong')	
			$jml	= $query2->num_rows();
		else
			$jml	= $query->num_rows();
			
			
		$total = 0;
		
		if($urido=='kosong') {
			foreach($isi=$query->result() as $row) {
				$total+=$row->amount;
			}
		}else{
			foreach($isi=$query2->result() as $row) {
				$total+=$row->amount;
			}
		}
		
		
		$data['totalnilainya']	= $total;
		
		$pagination['base_url'] = base_url().'/index.php/listbrgterkirim/cform/carilistpendding/'.$uritglawal.'/'.$uritglakhir.'/'.$uriorder.'/'.$urimotif.'/'.$urifstopproduksi.'/'.$urido.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(10,0);
		$this->pagination->initialize($pagination);
		
		$data['create_link']	= $this->pagination->create_links();
					
		$this->load->model('listbrgterkirim/mclass');
		
		
		if($urido=='kosong')
			$data['query']	= $this->mclass->clistbrgpendding($fdate,$tgl_awal,$tgl_akhir,$fstopproduksi,$iorder,$ordermotif,$pagination['per_page'],$pagination['cur_page']);
		else
			$data['query']	= $this->mclass->clistbrgpendding2($fdate,$tgl_awal,$tgl_akhir,$fstopproduksi,$iorder,$idonya,$ordermotif,$pagination['per_page'],$pagination['cur_page']);
			
		$data['isi']	= 'listbrgterkirim/vlistform';	
		$this->load->view('template',$data);	
		
	}
	
	function showdo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$iop = $this->uri->segment(4);
		$iproduct =	$this->uri->segment(5);
		$iopcode = $this->uri->segment(6);
		
		$data['iop'] = $iop;
		$data['iproduct'] = $iproduct;
		$data['iopcode'] = $iopcode;
		
		$this->load->model('listbrgterkirim/mclass');
		
		$data['isi'] = $this->mclass->detaildo($iop,$iproduct);
		
		$this->load->view('listbrgterkirim/vformdetaildo',$data);
		
	}
	
}
?>
