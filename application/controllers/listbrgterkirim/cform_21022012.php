<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0) 
		{	
			
			$data['form_title_detail_pendding']	= $this->lang->line('form_title_detail_pendding');
			$data['page_title_pendding']	= $this->lang->line('page_title_pendding');
			$data['list_pend_tgl_mulai_op']	= $this->lang->line('list_pend_tgl_mulai_op');
			$data['list_pend_sort_brg']	= $this->lang->line('list_pend_sort_brg');
			$data['list_pend_sort_dftr_brg']	= $this->lang->line('list_pend_sort_dftr_brg');
			$data['list_pend_tgl_op']	= $this->lang->line('list_pend_tgl_op');
			$data['list_pend_no_op']	= $this->lang->line('list_pend_no_op');
			$data['list_pend_cabang']	= $this->lang->line('list_pend_cabang');
			$data['list_pend_kd_brg']	= $this->lang->line('list_pend_kd_brg');
			$data['list_pend_nm_brg']	= $this->lang->line('list_pend_nm_brg');
			$data['list_pend_do']	= $this->lang->line('list_pend_do');
			$data['list_pend_sisa']	= $this->lang->line('list_pend_sisa');
			$data['list_pend_amount']	= $this->lang->line('list_pend_amount');
			$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$this->load->model('listbrgterkirim/mclass');
			
			$this->load->view('listbrgterkirim/vmainform',$data);
		}	
	}
	
	function carilistpendding() {		
		
		$data['form_title_detail_pendding']	= $this->lang->line('form_title_detail_pendding');
		$data['page_title_pendding']	= $this->lang->line('page_title_pendding');
		$data['list_pend_tgl_mulai_op']	= $this->lang->line('list_pend_tgl_mulai_op');
		$data['list_pend_sort_brg']	= $this->lang->line('list_pend_sort_brg');
		$data['list_pend_sort_dftr_brg']	= $this->lang->line('list_pend_sort_dftr_brg');
		$data['list_pend_tgl_op']	= $this->lang->line('list_pend_tgl_op');
		$data['list_pend_no_op']	= $this->lang->line('list_pend_no_op');
		$data['list_pend_cabang']	= $this->lang->line('list_pend_cabang');
		$data['list_pend_kd_brg']	= $this->lang->line('list_pend_kd_brg');
		$data['list_pend_nm_brg']	= $this->lang->line('list_pend_nm_brg');
		$data['list_pend_do']	= $this->lang->line('list_pend_do');
		$data['list_pend_sisa']	= $this->lang->line('list_pend_sisa');
		$data['list_pend_amount']	= $this->lang->line('list_pend_amount');
		$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lbrgpendding']	= "";
		
		if($this->input->post('d_op_first') || 
			$this->input->post('d_op_last') || 
			$this->input->post('f_stop_produksi') || 
			$this->input->post('iop') || 
			$this->input->post('imotif'))
		{
			$d_op_first		= $this->input->post('d_op_first');
			$d_op_last		= $this->input->post('d_op_last');
			$iop	= $this->input->post('iop');
			$imotif	= $this->input->post('imotif');
			$fstopproduksi = $this->input->post('fstopproduksi');
			
			if($iop=='')
				$iop	= "kosong";
			
			if($imotif=='')
				$imotif	= "kosong";
			
			if($d_op_first=='')
				$d_op_first	= "kosong";
			
			if($d_op_last=='')
				$d_op_last	= "kosong";
			
			if($iop!='kosong'){
				$iorder	= " AND a.i_op_code='".$this->input->post('iop')."' ";
			}else{
				$iorder	= "";
			}
			
			if($imotif!='kosong'){
				$ordermotif	= " AND b.i_product='".$this->input->post('imotif')."' ";
			}else{
				$ordermotif	= "";
			}
			
			if(substr($d_op_first,2,1)=="/" && $d_op_first!="kosong"){ // dd/mm/YYYY
				$tgl_awal	= substr($d_op_first,6,4).'-'.substr($d_op_first,3,2).'-'.substr($d_op_first,0,2);
				$data['tglmulai']	= $d_op_first;
			}else{
				$tgl_awal	= date("Y").'-'.date("m").'-'.'01';
				$data['tglmulai']	= '';
			}
			
			if(substr($d_op_last,2,1)=="/" && $d_op_last!="kosong"){
				$tgl_akhir	= substr($d_op_last,6,4).'-'.substr($d_op_last,3,2).'-'.substr($d_op_last,0,2);
				$data['tglakhir']	= $d_op_last;
			}else{
				$tgl_akhir	= date("Y").'-'.date("m").'-'.date("d");
				$data['tglakhir']	= '';
			}
		}
		
		if(!$this->input->post('d_op_first') && 
		   !$this->input->post('d_op_last') &&
		   !$this->input->post('iop') && 
		   !$this->input->post('imotif') && 
		   !$this->input->post('fstopproduksi')) {

			$d_op_first	= $this->uri->segment(4);
			$d_op_last	= $this->uri->segment(5);
			$iop	= $this->uri->segment(6);
			$imotif	= $this->uri->segment(7);
			$fstopproduksi = $this->uri->segment(8);
				
			if($iop!='kosong')
				$iorder	= " AND a.i_op_code='".$iop."' ";
			else
				$iorder	= "";
			
			if($imotif!='kosong')
				$ordermotif	= " AND b.i_product='".$imotif."' ";
			else
				$ordermotif	= "";

					
			if(substr($d_op_first,4,1)=='-' && $d_op_first!='kosong'){ // YYYY-mm-dd
				$tgl_awal	= $d_op_first;
				$data['tglmulai']	= substr($d_op_first,8,2).'/'.substr($d_op_first,5,2).'/'.substr($d_op_first,0,4);
			}else{
				$tgl_awal	= date("Y").'-'.date("m").'-'.'01';
				$data['tglmulai']	= '';
			}
			
			if(substr($d_op_last,4,1)=='-' && $d_op_last!='kosong'){ // YYYY-mm-dd
				$tgl_akhir	= $d_op_last;
				$data['tglakhir']	= substr($d_op_last,8,2).'/'.substr($d_op_last,5,2).'/'.substr($d_op_last,0,4);
			}else{
				$tgl_akhir	= date("Y").'-'.date("m").'-'.date("d");
				$data['tglakhir']	= '';
			}
		}

		if($d_op_first!='kosong' && $d_op_last!='kosong') {
			$fdate = " (a.d_op BETWEEN '$tgl_awal' AND '$tgl_akhir') AND ";
		}else{
			$fdate = " ";
		}
					
		$uriorder	= $iop;
		$urimotif	= $imotif;
		$uritglawal	= $tgl_awal;
		$uritglakhir= $tgl_akhir;
		$urifstopproduksi = $fstopproduksi;
		
		$data['stp']			= $fstopproduksi=='t'?' checked ':'';
		$data['orderpembelian']	= $uriorder=='kosong'?'':$uriorder;
		$data['productmotif']	= $urimotif=='kosong'?'':$urimotif;
		
		/*	
		$query	= $this->db->query(" SELECT  a.d_op AS dop,
			a.i_op_code AS iopcode,
			a.i_branch AS ibranch,
			b.i_product AS iproduct,
			c.e_product_motifname AS motifname,
			(b.n_count-b.n_residual) AS dobarang,
			b.n_residual AS sisa,
			d.v_unitprice AS hjp,
			(b.n_residual*d.v_unitprice) AS amount,
			a.d_op FROM tm_op_item b 
			
			LEFT JOIN tm_op a ON a.i_op=b.i_op
			INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
			WHERE ".$fdate." a.f_op_cancel='f' AND b.f_do_created='t' AND d.f_stop_produksi='$fstopproduksi' ".$iorder." ".$ordermotif." ");
		
		*/
		
		$query	= $this->db->query(" SELECT  a.d_op AS dop,
			a.i_op_code AS iopcode,
			a.i_branch AS ibranch,
			b.i_product AS iproduct,
			c.e_product_motifname AS motifname,
			(b.n_count-b.n_residual) AS dobarang,
			b.n_residual AS sisa,
			d.v_unitprice AS hjp,
			(b.n_residual*d.v_unitprice) AS amount,
			a.d_op FROM tm_op_item b
			
			LEFT JOIN tm_op a ON a.i_op=b.i_op
			INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
			
			WHERE ".$fdate." a.f_op_cancel='f' AND a.f_do_created='t' AND d.f_stop_produksi='$fstopproduksi' ".$iorder." ".$ordermotif." ");
			
		$jml	= $query->num_rows();
		
		$total = 0;
		
		foreach($isi=$query->result() as $row) {
			$total+=$row->amount;
		}
		
		$data['totalnilainya']	= $total;
		
		$pagination['base_url'] = '/listbrgterkirim/cform/carilistpendding/'.$uritglawal.'/'.$uritglakhir.'/'.$uriorder.'/'.$urimotif.'/'.$urifstopproduksi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		
		$data['create_link']	= $this->pagination->create_links();
					
		$this->load->model('listbrgterkirim/mclass');
		
		$data['isi']	= $this->mclass->clistbrgpendding($fdate,$tgl_awal,$tgl_akhir,$fstopproduksi,$iorder,$ordermotif,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('listbrgterkirim/vlistform',$data);
	}
	
	function showdo() {
		
		$iop = $this->uri->segment(4);
		$iproduct =	$this->uri->segment(5);
		$iopcode = $this->uri->segment(6);
		
		$data['iop'] = $iop;
		$data['iproduct'] = $iproduct;
		$data['iopcode'] = $iopcode;
		
		$this->load->model('listbrgterkirim/mclass');
		
		$data['isi'] = $this->mclass->detaildo($iop,$iproduct);
		
		$this->load->view('listbrgterkirim/vformdetaildo',$data);
		
	}
	
}
?>
