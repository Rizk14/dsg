<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('importdata/mmaster');
  }

  function importbhnbaku(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importdata/vmainformimportbhnbaku';
	$this->load->view('template',$data);
  }
  
  function doimportbhnbaku(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	//ini_set("display_errors", 1);
		
	//$jenistabel	= $this->input->post('jenistabel');
	//$jenisfile	= $this->input->post('jenisfile');
	//$opsi	= $this->input->post('opsi');
	$filenya = $_FILES['userfile']['name'];
		
	$config['upload_path'] = 'files/';
	$config['allowed_types'] = 'xls';
	$config['max_size']	= '0';
	$config['overwrite']	= 'TRUE';
	                
    $this->load->library('upload', $config);

     if ( !$this->upload->do_upload()) {
            //$data = array('error' => $this->upload->display_errors());
           // $this->session->set_flashdata('msg_excel', 'Insert failed. Please check your file, only .xls file allowed.');
        $hasilnya = array('error' => $this->upload->display_errors());
        $data['isi'] = 'importdata/vbhnbakuuploadgagal';
		//print_r($hasilnya);
		$data['msg'] = $hasilnya['error']." Upload file excel bahan baku/pembantu gagal. Silahkan dicoba lagi. 1 ";
		$this->load->view('template',$data);
     }
     else {
		$hasilnya = array('upload_data' => $this->upload->data());
		
		$file= 'files/'.$filenya;
		$prosesexcel = $this->mmaster->proses_excel_bhnbaku($file);
		
		if ($prosesexcel) {
			$data['isi'] = 'importdata/vbhnbakuuploadsukses';
			$data['msg'] = "Data berhasil diupload dan diimport ke database. ";
			$this->load->view('template',$data);	
		}
		else {
			$data['isi'] = 'importdata/vbhnbakuuploadgagal';
			$data['msg'] = "Import data excel bahan baku/pembantu gagal. Silahkan dicoba lagi. 2 ";
			$this->load->view('template',$data);
		}
	 }
  }
	 
	 // 29-06-2015
  function importsupplier(){
	// =======================
		// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importdata/vmainformimportsupplier';
	$this->load->view('template',$data);
  }
  
  function doimportsupplier(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	//ini_set("display_errors", 1);
		
	//$jenistabel	= $this->input->post('jenistabel');
	//$jenisfile	= $this->input->post('jenisfile');
	//$opsi	= $this->input->post('opsi');
	$filenya = $_FILES['userfile']['name'];
		
	$config['upload_path'] = 'files/';
	$config['allowed_types'] = 'xls';
	$config['max_size']	= '0';
	$config['overwrite']	= 'TRUE';
	                
    $this->load->library('upload', $config);

     if ( !$this->upload->do_upload()) {
            //$data = array('error' => $this->upload->display_errors());
           // $this->session->set_flashdata('msg_excel', 'Insert failed. Please check your file, only .xls file allowed.');
        $hasilnya = array('error' => $this->upload->display_errors());
        $data['isi'] = 'importdata/vsupplieruploadgagal';
		
		$data['msg'] = $hasilnya['error']." Upload file excel supplier gagal. Silahkan dicoba lagi. 1 ";
		$this->load->view('template',$data);
     }
     else {
		$hasilnya = array('upload_data' => $this->upload->data());
		
		$file= 'files/'.$filenya;
		$prosesexcel = $this->mmaster->proses_excel_supplier($file);
		
		if ($prosesexcel) {
			$data['isi'] = 'importdata/vsupplieruploadsukses';
			$data['msg'] = "Data berhasil diupload dan diimport ke database. ";
			$this->load->view('template',$data);	
		}
		else {
			$data['isi'] = 'importdata/vsupplieruploadgagal';
			$data['msg'] = "Import data excel supplier gagal. Silahkan dicoba lagi. 2 ";
			$this->load->view('template',$data);
		}
	 }
  }
  
  function importhargasupplier(){
	// =======================
		// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importdata/vmainformimporthargasupplier';
	$this->load->view('template',$data);
  }
  
  function doimporthargasupplier(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	//ini_set("display_errors", 1);
		
	$filenya = $_FILES['userfile']['name'];
		
	$config['upload_path'] = 'files/';
	$config['allowed_types'] = 'xls';
	$config['max_size']	= '0';
	$config['overwrite']	= 'TRUE';
	                
    $this->load->library('upload', $config);

     if ( !$this->upload->do_upload()) {
        $hasilnya = array('error' => $this->upload->display_errors());
        $data['isi'] = 'importdata/vhargasupplieruploadgagal';
		
		$data['msg'] = $hasilnya['error']." Upload file excel harga berdasarkan supplier gagal. Silahkan dicoba lagi. 1 ";
		$this->load->view('template',$data);
     }
     else {
		$hasilnya = array('upload_data' => $this->upload->data());
		
		$file= 'files/'.$filenya;
		$prosesexcel = $this->mmaster->proses_excel_hargasupplier($file);
		
		if ($prosesexcel) {
			$data['isi'] = 'importdata/vhargasupplieruploadsukses';
			$data['msg'] = "Data berhasil diupload dan diimport ke database. ";
			$this->load->view('template',$data);	
		}
		else {
			$data['isi'] = 'importdata/vhargasupplieruploadgagal';
			$data['msg'] = "Import data excel harga berdasarkan supplier gagal. Silahkan dicoba lagi. 2 ";
			$this->load->view('template',$data);
		}
	 }
  }
  //====================================================================================================
  
  // IMPORT UTK TASKHUB. 28-09-2015
  function importtrxpembelian(){
	// =======================
		// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importdata/vmainformimporttrxpembelian';
	$this->load->view('template',$data);
  }
  
  function doimporttrxpembelian(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	//ini_set("display_errors", 1);
		
	//$jenistabel	= $this->input->post('jenistabel');
	//$jenisfile	= $this->input->post('jenisfile');
	//$opsi	= $this->input->post('opsi');
	$filenya = $_FILES['userfile']['name'];
		
	$config['upload_path'] = 'files/';
	$config['allowed_types'] = '*';
	$config['max_size']	= '0';
	$config['overwrite']	= 'TRUE';
	                
    $this->load->library('upload', $config);

     if ( !$this->upload->do_upload()) {
            //$data = array('error' => $this->upload->display_errors());
           // $this->session->set_flashdata('msg_excel', 'Insert failed. Please check your file, only .xls file allowed.');
        $hasilnya = array('error' => $this->upload->display_errors());
        $data['isi'] = 'importdata/vtrxpembelianuploadgagal';
		
		$data['msg'] = $hasilnya['error']." Upload file excel laporan pembelian gagal. Silahkan dicoba lagi.1 ";
		$this->load->view('template',$data);
     }
     else {
		$hasilnya = array('upload_data' => $this->upload->data());
		
		$file= 'files/'.$filenya;
		$prosesexcel = $this->mmaster->proses_excel_trxpembelian($file);
		
		if ($prosesexcel) {
			$data['isi'] = 'importdata/vtrxpembelianuploadsukses';
			$data['msg'] = "Data laporan pembelian berhasil diupload dan diimport ke database. ";
			$this->load->view('template',$data);	
		}
		else {
			$data['isi'] = 'importdata/vtrxpembelianuploadgagal';
			$data['msg'] = "Import data excel laporan pembelian gagal. Silahkan dicoba lagi.2";
			$this->load->view('template',$data);
		}
	 }
  }
  
  // 29-09-2015 import data supplier yg udh di select distinct dari data tm_temp_transaksi_pembelian
  function importtrxsupplier(){
	// =======================
		// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importdata/vmainformimporttrxsupplier';
	$this->load->view('template',$data);
  }
  
  function doimporttrxsupplier(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	//ini_set("display_errors", 1);
		
	$filenya = $_FILES['userfile']['name'];
		
	$config['upload_path'] = 'files/';
	$config['allowed_types'] = '*';
	$config['max_size']	= '0';
	$config['overwrite']	= 'TRUE';
	                
    $this->load->library('upload', $config);

     if ( !$this->upload->do_upload()) {
            //$data = array('error' => $this->upload->display_errors());
           // $this->session->set_flashdata('msg_excel', 'Insert failed. Please check your file, only .xls file allowed.');
        $hasilnya = array('error' => $this->upload->display_errors());
        $data['isi'] = 'importdata/vtrxsupplieruploadgagal';
		
		$data['msg'] = $hasilnya['error']." Upload file excel supplier gagal. Silahkan dicoba lagi.1 ";
		$this->load->view('template',$data);
     }
     else {
		$hasilnya = array('upload_data' => $this->upload->data());
		
		$file= 'files/'.$filenya;
		$prosesexcel = $this->mmaster->proses_excel_trxsupplier($file);
		
		if ($prosesexcel) {
			$data['isi'] = 'importdata/vtrxsupplieruploadsukses';
			$data['msg'] = "Data supplier berhasil diupload dan diimport ke database. ";
			$this->load->view('template',$data);	
		}
		else {
			$data['isi'] = 'importdata/vtrxsupplieruploadgagal';
			$data['msg'] = "Import data excel supplier gagal. Silahkan dicoba lagi.2";
			$this->load->view('template',$data);
		}
	 }
  }
  
  // PENJUALAN. AMBIL DATANYA DARI tm_faktur_do_t, tm_faktur, dan tm_faktur_bhnbaku
  function importtrxpenjualan(){
	// =======================
		// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importdata/vmainformimporttrxpenjualan';
	$this->load->view('template',$data);
  }
  
  function doimporttrxpenjualan(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");

	$prosesimport = $this->mmaster->proses_import_trxpenjualan();
		
	if ($prosesimport) {
		$data['isi'] = 'importdata/vtrxpenjualansukses';
		$data['msg'] = "Data laporan penjualan berhasil dimigrasi ";
		$this->load->view('template',$data);	
	}
	else {
		$data['isi'] = 'importdata/vtrxpenjualangagal';
		$data['msg'] = "Data laporan penjualan gagal dimigrasi. Silahkan dicoba lagi.2";
		$this->load->view('template',$data);
	}
  }
  
  // 29-09-2015
  function exporttrxpenjualan(){
	// =======================
		// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importdata/vmainformexporttrxpenjualan';
	$this->load->view('template',$data);
  }
  
  function doexporttrxpenjualan(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$query = $this->mmaster->get_temp_transaksi_penjualan();
	$nama_file = "transaksi_penjualan_jan_aug_2015.xls";
	
	$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		<tr>
				 <th>company</th>
				 <th>ou_code</th>
				 <th>doc_no</th>
				 <th>doc_date</th>
				 <th>customer_code</th>
				 <th>due_date</th>
				 <th>tax_no</th>
				 <th>tax_date</th>
				 <th>main_acc</th>
				 <th>sub_acc</th>
				 <th>gov_tax_amount</th>
				 <th>curr_code</th>
				 <th>nett_amount</th>
				 <th>remark</th>
			 </tr>
		</thead>
		<tbody>";
				
		//for($j=0;$j<count($query);$j++){
		foreach ($query as $row1) {				 
			$pisah1 = explode("-", $row1->doc_date);
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			$doc_date = $thn1.$bln1.$tgl1;
			
			$pisah1 = explode("-", $row1->due_date);
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			$due_date = $thn1.$bln1.$tgl1;
			
			$pisah1 = explode("-", $row1->tax_date);
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			$tax_date = $thn1.$bln1.$tgl1;
			
			 $html_data.= "<tr>
			 <td>".$row1->company."</td>
			 <td>".$row1->ou_code."</td>
			 <td>".$row1->doc_no."</td>
			 <td>".$doc_date."</td>
			 <td>".$row1->customer_code."</td>
			 <td>".$due_date."</td>
			 <td>".$row1->tax_no."</td>
			 <td>".$tax_date."</td>
			 <td>".$row1->main_acc."</td>
			 <td>".$row1->sub_acc."</td>
			 <td>".$row1->gov_tax_amount."</td>
			 <td>".$row1->curr_code."</td>
			 <td>".$row1->nett_amount."</td>
			 <td>".$row1->remark."</td>
			 ";
			$html_data.=  "</tr>";			
	 	}
		 $html_data.= "</tbody>
		</table>";
		$export_excel1 = '1';
		/*if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods"; */
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  // 30-09-2015
  function exporttrxpembelian(){
	// =======================
		// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importdata/vmainformexporttrxpembelian';
	$this->load->view('template',$data);
  }
  
  function doexporttrxpembelian(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$query = $this->mmaster->get_temp_transaksi_pembelian();
	$nama_file = "transaksi_pembelian_jan_aug_2015.xls";
	
	$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		<tr>
				 <th>company</th>
				 <th>ou_code</th>
				 <th>doc_no</th>
				 <th>doc_date</th>
				 <th>supplier_code</th>
				 <th>due_date</th>
				 <th>supplier_doc_no</th>
				 <th>supplier_doc_date</th>
				 <th>tax_no</th>
				 <th>tax_date</th>
				 <th>main_acc</th>
				 <th>sub_acc</th>
				 <th>gov_tax_amount</th>
				 <th>curr_code</th>
				 <th>nett_amount</th>
				 <th>remark</th>
			 </tr>
		</thead>
		<tbody>";
				
		//for($j=0;$j<count($query);$j++){
		foreach ($query as $row1) {				 
			$pisah1 = explode("-", $row1->doc_date);
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			$doc_date = $thn1.$bln1.$tgl1;
			
			$pisah1 = explode("-", $row1->due_date);
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			$due_date = $thn1.$bln1.$tgl1;
			
			if ($row1->tax_date != '') {
				$pisah1 = explode("-", $row1->tax_date);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$tax_date = $thn1.$bln1.$tgl1;
			}
			else
				$tax_date = '';
				
			 $html_data.= "<tr>
			 <td>".$row1->company."</td>
			 <td>".$row1->ou_code."</td>
			 <td>'".$row1->doc_no."</td>
			 <td>".$doc_date."</td>
			 <td>".$row1->supplier_code."</td>
			 <td>".$due_date."</td>
			 <td>'".$row1->supplier_doc_no."</td>
			 <td>".$doc_date."</td>
			 <td>".$row1->tax_no."</td>
			 <td>".$tax_date."</td>
			 <td>".$row1->main_acc."</td>
			 <td>".$row1->sub_acc."</td>
			 <td>".$row1->gov_tax_amount."</td>
			 <td>".$row1->curr_code."</td>
			 <td>".$row1->nett_amount."</td>
			 <td>".$row1->remark."</td>
			 ";
			$html_data.=  "</tr>";			
	 	}
		 $html_data.= "</tbody>
		</table>";
		$export_excel1 = '1';
		/*if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods"; */
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }

}
