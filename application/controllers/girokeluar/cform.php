<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$data['page_title'] = $this->lang->line('master_girokeluar');
			$this->load->model('girokeluar/mmaster');
			$data['igiro']='';
			$data['query']=$this->mmaster->bacasemua();
			$data['isi']='girokeluar/vmainform';
			$this->load->view('template', $data);
		
	}
	function insert_fail()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$data['page_title'] = $this->lang->line('master_girodgu');
			$this->load->view('girokeluar/vinsert_fail',$data);
		
	}
	function edit()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$data['page_title'] = $this->lang->line('master_girodgu')." update";
			if($this->uri->segment(4)){
				$igiro = $this->uri->segment(4);
				$ipv = $this->uri->segment(5);
				$isupplier = $this->uri->segment(6);
				$dfrom = $this->uri->segment(7);
				$dto 	 = $this->uri->segment(8);
				$data['igiro'] 		= $igiro;
				$data['isupplier']= $isupplier;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$data['ipv'] 			= $ipv;
				$this->load->model('girokeluar/mmaster');
				$data['isi']=$this->mmaster->baca($igiro,$ipv);
		 		$this->load->view('girokeluar/vformupdate',$data);
			}else{
				$this->load->view('girokeluar/vinsert_fail',$data);
			}
		
	}
	function update()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$igiro 			= $this->input->post('igiro', TRUE);
			$ipv 			= $this->input->post('ipv', TRUE);
			$isupplier		= $this->input->post('isupplier', TRUE);
			$dgiro			= $this->input->post('dgiro', TRUE);
			if($dgiro!=''){
				$tmp=explode("-",$dgiro);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiro=$th."-".$bl."-".$hr;
			}
			$dpv			= $this->input->post('dpv', TRUE);
			if($dpv!=''){
				$tmp=explode("-",$dpv);
				$rvth=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpv=$rvth."-".$bl."-".$hr;
			}
			$dgiroduedate	= $this->input->post('dgiroduedate', TRUE);
			if($dgiroduedate!=''){
				$tmp=explode("-",$dgiroduedate);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiroduedate=$th."-".$bl."-".$hr;
			}
			$dgirocair	= $this->input->post('dgirocair', TRUE);
			if($dgirocair!=''){
				$fgirocair		= 't';
				$tmp=explode("-",$dgirocair);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgirocair=$th."-".$bl."-".$hr;
			}else{
				$fgirocair	= 'f';
				$dgirocair	= null;
			}
			$egirodescription= $this->input->post('egirodescription', TRUE);
			$egirobank		= $this->input->post('egirobank', TRUE);
			if($this->input->post('fgirobatal')!=''){
				$fgirobatal		= 't';
			}else{
				$fgirobatal		= 'f';
			}
			$dgirotolak	= $this->input->post('dgirotolak', TRUE);
			if($dgirotolak!=''){
				$fgirotolak		= 't';
				$tmp=explode("-",$dgirotolak);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgirotolak=$th."-".$bl."-".$hr;
			}else{
				$fgirotolak	= 'f';
				$dgirotolak	= null;
			}
			$vjumlah		= $this->input->post('vjumlah', TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$vsisa			= $vjumlah;

			if (
				(isset($igiro) && $igiro != '') && 
				(isset($isupplier) && $isupplier != '') && 
				(isset($dgiro) && $dgiro != '') && 
				(isset($dpv) && $dpv != '') && 
				(isset($dgiroduedate) && $dgiroduedate != '') && 
				(isset($vjumlah) && $vjumlah != '')
			   )
			{
				$this->db->trans_begin();
				$this->load->model('girokeluar/mmaster');
//				$irv = $this->mmaster->runningnumberrv($rvth);
				$this->mmaster->update($igiro,$isupplier,$ipv,$dgiro,$dpv,$dgiroduedate,$dgirocair,$egirodescription,$egirobank,
									   $fgirotolak,$fgirocair,$vjumlah,$vsisa,$dgirotolak,$fgirobatal);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Update girokeluar Supplier'.$isupplier.' No:'.$igiro;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );  

					$data['sukses']			= true;
					$data['inomor']			= $igiro;
					$this->load->view('nomor',$data);
				}
			}
		
	}
	function delete()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$igiro		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$this->load->model('girokeluar/mmaster');
			$this->mmaster->delete($igiro,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus girokeluar No'.$igiro.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$data['page_title'] = $this->lang->line('master_girodgu');
			$data['isi']=$this->mmaster->bacasemua();
			$data['igiro']='';
			$this->load->view('girokeluar/vmainform', $data);
		
	}
	function page()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$arr = array('vformlist','vform');
		$page=$this->uri->segment(4);
		if(!in_array($page,$arr)){ return false; }
		$config['base_url'] = base_url().'index.php/girokeluar/cform/paging/';
		$cari 				= $this->input->post('cari', TRUE);
		$cari				= strtoupper($cari);
		$query				= $this->db->query("select * from tm_giro
												where upper(i_giro) like '%$cari%' 
												or upper(i_customer) like '%$cari%'",false);
		$config['total_rows'] = $query->num_rows();
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(4);
		$this->pagination->initialize($config);
		$data['page_title'] = $this->lang->line('master_girodgu');
		$data['igiro']='';
		$this->load->model('girokeluar/mmaster');
		$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
		$this->load->view('girokeluar/'.$page, $data);
	}
	function supplier()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/girokeluar/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tm_supplier",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('girokeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('girokeluar/vlistsupplier', $data);
		
	}
	function carisupplier()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$config['base_url'] = base_url().'index.php/girokeluar/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier where (upper(i_supplier) like '%$cari%' 
							      	   or upper(e_supplier_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('girokeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('girokeluar/vlistsupplier', $data);
		
	}
	function simpan()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$igiro 			= $this->input->post('igiro', TRUE);
			$dgiro			= $this->input->post('dgiro', TRUE);
			if($dgiro!=''){
				$tmp=explode("-",$dgiro);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiro=$th."-".$bl."-".$hr;
			}
#			$iarea 			= $this->input->post('iarea', TRUE);
			$isupplier		= $this->input->post('isupplier', TRUE);
			$dpv			= $this->input->post('dpv', TRUE);
			if($dpv!=''){
				$tmp=explode("-",$dpv);
				$pvth=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpv=$pvth."-".$bl."-".$hr;
			}
			$dgiroduedate	= $this->input->post('dgiroduedate', TRUE);
			if($dgiroduedate!=''){
				$tmp=explode("-",$dgiroduedate);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiroduedate=$th."-".$bl."-".$hr;
			}
			$egirodescription= $this->input->post('egirodescription', TRUE);
			$egirobank		= $this->input->post('egirobank', TRUE);
			$vjumlah		= $this->input->post('vjumlah', TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$vsisa			= $vjumlah;

			if (
				(isset($igiro) && $igiro != '') && 
##				(isset($iarea) && $iarea != '') &&
				(isset($isupplier) && $isupplier != '') && 
				(isset($dgiro) && $dgiro != '') && 
				(isset($dpv) && $dpv != '') && 
				(isset($dgiroduedate) && $dgiroduedate != '') && 
				(isset($vjumlah) && $vjumlah != '')
			   )
			{
				$this->load->model('girokeluar/mmaster');
				$ipv = $this->mmaster->runningnumberpv($pvth);
				$this->db->trans_begin();
				$this->mmaster->insert($igiro,$isupplier,$ipv,$dgiro,$dpv,$dgiroduedate,$egirodescription,$egirobank,
									   $vjumlah,$vsisa);
			}
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
		print "<script>alert(\"Sukses no Giro ".$igiro." supplier:".$isupplier." \");window.open(\"index\", \"_self\");</script>";
			  //~ $sess=$this->session->userdata('session_id');
			  //~ $id=$this->session->userdata('user_id');
			  //~ $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  //~ $rs		= pg_query($sql);
			  //~ if(pg_num_rows($rs)>0){
				  //~ while($row=pg_fetch_assoc($rs)){
					  //~ $ip_address	  = $row['ip_address'];
					  //~ break;
				  //~ }
			  //~ }else{
				  //~ $ip_address='kosong';
			  //~ }
			  //~ $query 	= pg_query("SELECT current_timestamp as c");
	      //~ while($row=pg_fetch_assoc($query)){
	      	//~ $now	  = $row['c'];
			  //~ }
			  //~ $pesan='Input Giro DGU No'.$igiro.' Supplier:'.$isupplier;
			  //~ $this->load->model('logger');
			  //~ $this->logger->write($id, $ip_address, $now , $pesan );  

				//~ $data['sukses']			= true;
				//~ $data['inomor']			= $igiro;
				//~ $this->load->view('nomor',$data);
			}
		
	}
}
?>
