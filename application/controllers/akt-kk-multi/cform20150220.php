<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kkmulti');
			$data['iarea']='';
			$data['iperiode']='';
			$data['ikk']='';
			$data['periode']='';
			$query=$this->db->query("	select i_periode from tm_periode ",false);
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $rw){
          $data['periode']=$rw->i_periode;
		    }
	    }
			$area1 = $this->session->userdata('i_area');
			$query = $this->db->query("select e_area_name from tr_area where i_area = '$area1'",false);
			foreach($query->result() as $tmp){
				$nama=$tmp->e_area_name;
			}
    	$data['lepel']= $area1;
    	$data['area1']= $area1;
    	$data['nama'] = $nama;      
     	$this->load->view('akt-kk-multi/vmainform', $data);
    }else{
			$this->load->view('awal/index.php');
    }
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kkmulti');
			$this->load->view('akt-kk-multi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kkmulti')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$ikk 		= $this->uri->segment(4);
				$iperiode	= $this->uri->segment(5);
				$iarea		= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto		= $this->uri->segment(8);
				$lepel		= $this->uri->segment(9);
				$this->load->model("akt-kk-multi/mmaster");
				$data['isi']=$this->mmaster->baca($ikk,$iperiode,$iarea);
				$data['iarea']  = $iarea;
				$data['dfrom']  = $dfrom;
				$data['dto']    = $dto;
				$data['lepel']	= $lepel;
 		 		$this->load->view('akt-kk-multi/vformupdate',$data);
			}else{
				$this->load->view('akt-kk-multi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikk		= $this->input->post('ikk', TRUE);
			$iarea		= $this->input->post('iarea', TRUE);
			$iperiode	= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$tah		= substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul		= $this->input->post('iperiodebl', TRUE);
			$vkk		= $this->input->post('vkk', TRUE);
			$vkk		= str_replace(',','',$vkk);
			$dkk		= $this->input->post('dkk', TRUE);
			$dbukti		= $this->input->post('dbukti', TRUE);
			$icoa		= $this->input->post('icoa', TRUE);
			$ecoaname	= $this->input->post('ecoaname', TRUE);
			$enamatoko	= $this->input->post('enamatoko', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			$ibukti		= $this->input->post('ibukti', TRUE);
			if($edescription=="") $edescription=null;

			if($dkk!=''){
				$tmp=explode("-",$dkk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkk=$th."-".$bl."-".$hr;
			}
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$fdebet='t';
			if (
				(isset($ikk) && $ikk != '') &&
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($vkk) && (($vkk != 0) || ($vkk != ''))) &&
				(isset($dkk) && $dkk != '') &&
				(isset($icoa) && $icoa != '')
			   )
			{
				$this->load->model('akt-kk-multi/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($iarea,$ikk,$iperiode,$icoa,$vkk,$dkk,$ecoaname,
						                   $edescription,$fdebet,$dbukti,
						                   $enamatoko,$ibukti);
				$nomor=$ikk;
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update Kas kecil No:'.$ikk.' Periode:'.$iperiode.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-kk-multi/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-kk-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-kk-multi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/akt-kk-multi/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('akt-kk-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-kk-multi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area	= $this->session->userdata('i_area');
      $baris=$this->uri->segment(4);			
      $config['base_url'] = base_url().'index.php/akt-kk-multi/cform/coa/'.$baris.'/';
			if($area!='00'){
#			  $query = $this->db->query("select * from tr_coa where (i_coa like '6%' or i_coa like '800.000') and f_coa_cabang='t'",false);
			  $query = $this->db->query("select * from tr_coa where (i_coa like '6%' or i_coa like '800.000')",false);
      }else{
			  $query = $this->db->query("select * from tr_coa where (i_coa like '6%' or i_coa like '800.000')",false);
			}			  
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
      $data['baris']=$this->uri->segment(4);
			$this->load->model('akt-kk-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('akt-kk-multi/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){           
      $area	= $this->session->userdata('i_area');
      $baris 	= $this->input->post('xbaris', FALSE);
      $config['base_url'] = base_url().'index.php/akt-kk-multi/cform/coa/index/'.$baris.'';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area!='00'){
#			  $query 	= $this->db->query("select * from tr_coa
#						      									where (upper(i_coa) like '800.000' or upper(i_coa) like '6%$cari%' or (upper(e_coa_name) like '%$cari%' 
#																	  and upper(i_coa) like '6%')) and f_coa_cabang='t'",false);
			  $query 	= $this->db->query("select * from tr_coa
						      									where (upper(i_coa) like '800.000' or upper(i_coa) like '6%$cari%' or (upper(e_coa_name) like '%$cari%' 
																	  and upper(i_coa) like '6%'))",false);
      }else{
			  $query 	= $this->db->query("select * from tr_coa
						      									where (upper(i_coa) like '800.000' or upper(i_coa) like '6%$cari%' or (upper(e_coa_name) like '%$cari%' 
																	  and upper(i_coa) like '6%'))",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
  
			$this->load->model('akt-kk-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(6),$area);
 $data['baris']=$baris;
			$this->load->view('akt-kk-multi/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){		
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$area	=$this->input->post('area', TRUE);
      $baris 	= $this->input->post('xbaris', FALSE);
			$periode=$this->input->post('periode', TRUE);
      if($area=='') $area	= $this->uri->segment(4);
			if($periode=='') $periode = $this->uri->segment(5);
      if($baris=='') $baris = $this->uri->segment(6);
      if($cari=='') $cari = $this->uri->segment(7);
      if($cari=='') $cari='sikasep';
			$config['base_url'] = base_url().'index.php/akt-kk-multi/cform/kendaraan/'.$area.'/'.$periode.'/'.$baris.'/'.$cari.'/';
      if($cari=='sikasep'){
			  $query = $this->db->query("	select i_kendaraan from tr_kendaraan a
							                      inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							                      inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							                      where a.i_area='$area' and a.i_periode='$periode'",false);
      }else{
			  $query = $this->db->query("	select i_kendaraan from tr_kendaraan a
							                      inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							                      inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							                      where a.i_area='$area' and a.i_periode='$periode'
                                    and (upper(i_kendaraan) like '%$cari%' or upper(e_pengguna) like '%$cari%') ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
      $data['baris']=$this->uri->segment(6);
			$this->load->model('akt-kk-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['area']	=$area;
			$data['periode']=$periode;
			$data['isi']=$this->mmaster->bacakendaraan($cari,$area,$periode,$config['per_page'],$this->uri->segment(8));
			$this->load->view('akt-kk-multi/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	=$this->input->post('area', TRUE);
            $baris 	= $this->input->post('xbaris', FALSE);
			$periode=$this->input->post('periode', TRUE);
			$config['base_url'] = base_url().'index.php/akt-kk-multi/cform/kendaraan/'.$area.'/'.$periode.'/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("	select * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
							and a.i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
        
			$this->pagination->initialize($config);
			$this->load->model('akt-kk-multi/mmaster');
			$data['area']=$area;
            $data['baris']=$baris;
			$data['periode']=$periode;
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['isi']=$this->mmaster->carikendaraan($area,$periode,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-kk-multi/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea		= $this->input->post('iarea', TRUE);
			$ipvtype  = $this->input->post('ipvtype', TRUE);
			$iperiode	= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$tah		  = substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul		  = $this->input->post('iperiodebl', TRUE);
			$dkk		  = $this->input->post('dkk', TRUE);
      $jml      = $this->input->post('jml', TRUE);
			if($dkk!=''){
				$tmp=explode("-",$dkk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkk=$th."-".$bl."-".$hr;
				$dpv=$th."-".$bl."-".$hr;
			}
			$fdebet='t';
      $nomor=array();
			if (
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($dkk) && $dkk != '') &&
				(isset($jml) && $jml != '' && $jml != '0')

			   )
			{
				$this->load->model('akt-kk-multi/mmaster');
				$this->db->trans_begin();
				$tot=0;
        $ipv=$this->mmaster->runningnumberpv($tah,$bul,$iarea,$ipvtype);
        for ($a=1;$a<=$jml;$a++) {
          $icoa         = $this->input->post('icoa'.$a, TRUE);
          $ecoaname     = $this->input->post('ecoaname'.$a, TRUE);
          $ibukti       = $this->input->post('ibukti'.$a, TRUE);
          $dbukti	      = $this->input->post('dbukti'.$a, TRUE);
          $eremark      = null;
	        if($dbukti!=''){
		        $tmp=explode("-",$dbukti);
		        $th=$tmp[2];
		        $bl=$tmp[1];
		        $hr=$tmp[0];
		        $dbukti=$th."-".$bl."-".$hr;
	        }
          $vkk          = $this->input->post('vkk'.$a, TRUE);
          $vkk	        = str_replace(',','',$vkk);
          $tot=$tot+$vkk;
          $edescription = $this->input->post('edescription'.$a, TRUE);
          if($edescription=="") $edescription=null;
          $ikk=$this->mmaster->runningnumberkk($tah,$bul,$iarea);
          $this->mmaster->insert( $iarea,$ikk,$iperiode,$icoa,$vkk,$dkk,$ecoaname,
              						        $edescription,$fdebet,$dbukti,$ibukti);
          $nomor[]=$ikk;
###########posting##########
          $eremark		= $edescription;
          $fclose			= 'f';
			    $this->mmaster->inserttransheader($ikk,$iarea,$eremark,$fclose,$dkk);
			    if($fdebet=='t'){
				    $accdebet		= $icoa;
				    $namadebet		= $ecoaname;
				    $acckredit		= KasKecil.$iarea;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
			    }else{
				    $accdebet		  = KasKecil.$iarea;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
			    }
			    $this->mmaster->inserttransitemdebet($accdebet,$ikk,$namadebet,'t','t',$iarea,$eremark,$vkk,$dkk,$iarea);
			    $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vkk);
			    $this->mmaster->inserttransitemkredit($acckredit,$ikk,$namakredit,'f','t',$iarea,$eremark,$vkk,$dkk,$iarea);
			    $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vkk);
			    $this->mmaster->insertgldebet($accdebet,$ikk,$namadebet,'t',$iarea,$vkk,$dkk,$eremark);
			    $this->mmaster->insertglkredit($acckredit,$ikk,$namakredit,'f',$iarea,$vkk,$dkk,$eremark);
###########end of posting##########
          $this->mmaster->insertpvitem( $ipv,$iarea,$icoa,$ecoaname,$vkk,$edescription,$ikk,$ipvtype);
        }
				$icoa=KasKecil.$iarea;
				$this->mmaster->insertpv( $ipv,$iarea,$iperiode,$icoa,$dpv,$tot,$eremark,$ipvtype);
        $nomor[]=$ikk;
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
#          $this->db->trans_rollback();					
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Input Kas kecil No:'.$ikk.' Periode:'.$iperiode.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomorarray',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function start()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('iarea', TRUE);
      if($area=='') $area=$this->uri->segment(4);
			$periode= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
      if($periode=='') $periode=$this->uri->segment(5);
			$tanggal= $this->input->post('dkk', TRUE);
      if($tanggal=='') $tanggal=$this->uri->segment(6);
			if($tanggal!=''){
				$tmp=explode("-",$tanggal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$tgl=$th."-".$bl."-".$hr;
			}
			$this->load->model('akt-kk-multi/mmaster');

			$tmp = explode("-", $tgl);
			$det	= $tmp[2];
			$mon	= $tmp[1];
			$yir 	= $tmp[0];
			$dsaldo	= $yir."/".$mon."/".$det;
			$dtos	= $this->mmaster->dateAdd("d",1,$dsaldo);
			$tmp 	= explode("-", $dtos);
			$det1	= $tmp[2];
			$mon1	= $tmp[1];
			$yir1 	= $tmp[0];
			$dtos	= $yir1."-".$mon1."-".$det1;
		
			$data['saldo']	  = $this->mmaster->bacasaldo($area,$dtos);
			$data['page_title'] = $this->lang->line('kkmulti');
			$data['iarea']	  = $area;
			$data['eareaname']= $this->mmaster->area($area);
			$data['iperiode'] = $periode;
			$data['tanggal']  = $tanggal;
			$data['ikk']	  = '';
			$data['lepel']	  = $this->session->userdata('i_area');
			$this->load->view('akt-kk-multi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	
	
	function kkgroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
            $cari=strtoupper($this->input->post("cari"));
			$config['base_url'] = base_url().'index.php/akt-kk-multi/cform/kkgroup/';
			$query = $this->db->query("	select * from tr_kk_group where upper(i_kk_group) like '%$cari%' or upper(e_kk_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('akt-kk-multi/mmaster');
			$data['page_title'] = $this->lang->line('listkkgroup');
			$data['isi']=$this->mmaster->bacakkgroup($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('akt-kk-multi/vlistkkgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea	= $this->uri->segment(5);
			$ipv	  = $this->uri->segment(4);
			$this->load->model('akt-kk-multi/mmaster');
      $data['iarea']= $iarea;
			$data['ipv']  = $ipv;
			$data['page_title'] = $this->lang->line('printpv');
			$data['isi']=$this->mmaster->bacapv($ipv,$iarea);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$sql	= "select e_area_name from tr_area where i_area='$iarea'";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
			     $data['eareaname']=$tes->e_area_name;
			  }
			}
			$pesan='Cetak PV Area:'.$iarea.' No:'.$ipv;
			$this->load->model('logger');
#			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('akt-kk-multi/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pv()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){		
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$area	=$this->input->post('area', TRUE);
			$periode=$this->input->post('periode', TRUE);
      if($area=='') $area	= $this->uri->segment(4);
			if($periode=='') $periode = $this->uri->segment(5);
      if($cari=='') $cari = $this->uri->segment(6);
      if($cari=='') $cari='sikasep';
			$config['base_url'] = base_url().'index.php/akt-kk-multi/cform/pv/'.$area.'/'.$periode.'/'.$cari.'/';
      if($cari=='sikasep'){
			  $query = $this->db->query("	select i_pv, v_pv from tm_pv
							                      where i_area='$area' and i_periode='$periode' and i_pv_type='00'",false);
      }else{
			  $query = $this->db->query("	select i_pv, v_pv from tm_pv
							                      where i_area='$area' and i_periode='$periode' and i_pv_type='00'
                                    and (upper(i_pv) like '%$cari%') ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
      $data['baris']=$this->uri->segment(7);
			$this->load->model('akt-kk-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_pv');
			$data['area']	=$area;
			$data['periode']=$periode;
			$data['isi']=$this->mmaster->bacapvprint($cari,$area,$periode,$config['per_page'],$this->uri->segment(7));
			$this->load->view('akt-kk-multi/vlistpv', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripv()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu231')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	=$this->input->post('area', TRUE);
            $baris 	= $this->input->post('xbaris', FALSE);
			$periode=$this->input->post('periode', TRUE);
			$config['base_url'] = base_url().'index.php/akt-kk-multi/cform/kendaraan/'.$area.'/'.$periode.'/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("	select * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)

							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
							and a.i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
        
			$this->pagination->initialize($config);
			$this->load->model('akt-kk-multi/mmaster');
			$data['area']=$area;
            $data['baris']=$baris;
			$data['periode']=$periode;
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['isi']=$this->mmaster->carikendaraan($area,$periode,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-kk-multi/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
