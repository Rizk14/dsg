<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
			$data['page_title_addprinter']	= $this->lang->line('page_title_addprinter');
			$data['form_tbl_printer_name']	= $this->lang->line('form_tbl_printer_name');
			$data['form_tbl_printer_uri']	= $this->lang->line('form_tbl_printer_uri');
			$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
			$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
			$data['form_name_printer']	= $this->lang->line('form_name_printer');
			$data['form_tbl_printer_ip']	= $this->lang->line('form_tbl_printer_ip');
			$data['form_ip_printer']	= $this->lang->line('form_ip_printer');
			$data['form_uri_printer']	= $this->lang->line('form_uri_printer');
			$data['form_level_user_printer']	= $this->lang->line('form_level_user_printer');
			$data['form_pilihan_level_printer']	= $this->lang->line('form_pilihan_level_printer');
			$data['form_pilihan_user_printer']	= $this->lang->line('form_pilihan_user_printer');
			$data['link_aksi']		= $this->lang->line('link_aksi');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['llevel']	= "";
			$this->load->model('printer/mclass');
			$data['query']	= $this->mclass->view();
			$data['opt_level']	= $this->mclass->LevelUser();
			$data['isi']		= 'printer/vmainform';
			$this->load->view('template',$data);
	
		}	
	
function tambah() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
			$data['page_title_addprinter']	= $this->lang->line('page_title_addprinter');
			$data['form_tbl_printer_name']	= $this->lang->line('form_tbl_printer_name');
			$data['form_tbl_printer_uri']	= $this->lang->line('form_tbl_printer_uri');
			$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
			$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
			$data['form_name_printer']	= $this->lang->line('form_name_printer');
			$data['form_tbl_printer_ip']	= $this->lang->line('form_tbl_printer_ip');
			$data['form_ip_printer']	= $this->lang->line('form_ip_printer');
			$data['form_uri_printer']	= $this->lang->line('form_uri_printer');
			$data['form_level_user_printer']	= $this->lang->line('form_level_user_printer');
			$data['form_pilihan_level_printer']	= $this->lang->line('form_pilihan_level_printer');
			$data['form_pilihan_user_printer']	= $this->lang->line('form_pilihan_user_printer');
			$data['link_aksi']		= $this->lang->line('link_aksi');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['llevel']	= "";
			$this->load->model('printer/mclass');
			$data['query']	= $this->mclass->view();
			$data['opt_level']	= $this->mclass->LevelUser();
			$data['isi']		= 'printer/vmainformadd';
			$this->load->view('template',$data);
	
		}	
	function cari_user() {
		/*
		incabcab(nilai)
		*/
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$ilevel	= $this->input->post('ilepel')?$this->input->post('ilepel'):$this->input->get_post('ilepel');
		$this->load->model('printer/mclass');
		
		$query	= $this->mclass->getlepel($ilevel);
		
		if($query->num_rows()>0) {
		
			$c	= "";
			
			$Luser	= $query->result();

			foreach($Luser as $row) {
				$c.="<option value=".$row->i_user_id." >".$row->e_user_name."</option>";
			}
			echo "<select name=\"iuser\" id=\"iuser\" >
					<option value=\"\">[Pilih User]</option>".$c."</select>";
		}
	}
		
	function simpan() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$eprintername	= $this->input->post('e_printer_name')?trim($this->input->post('e_printer_name')):trim($this->input->get_post('e_printer_name'));
		$euri	= $this->input->post('e_uri')?$this->input->post('e_uri'):$this->input->get_post('e_uri');
		$ip	= $this->input->post('ip')?$this->input->post('ip'):$this->input->get_post('ip');
		$ilevel	= $this->input->post('ilevel')?$this->input->post('ilevel'):$this->input->get_post('ilevel');
		$iuserid	= $this->input->post('iuser')?$this->input->post('iuser'):$this->input->get_post('iuser');
		$destination_ip	= $ip;
		if( !empty($eprintername) && !empty($euri) && !empty($ip) && !empty($iuserid) ) {
			
			$this->load->model('printer/mclass');
			
			$qry_cekprinter = $this->mclass->CekPrinter($destination_ip);
			
			if($qry_cekprinter->num_rows()>0) {
				$data['page_title_addprinter']	= $this->lang->line('page_title_addprinter');
				$data['form_tbl_printer_name']	= $this->lang->line('form_tbl_printer_name');
				$data['form_tbl_printer_uri']	= $this->lang->line('form_tbl_printer_uri');
				$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
				$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
				$data['form_name_printer']	= $this->lang->line('form_name_printer');
				$data['form_tbl_printer_ip']	= $this->lang->line('form_tbl_printer_ip');
				$data['form_ip_printer']	= $this->lang->line('form_ip_printer');
				$data['form_uri_printer']	= $this->lang->line('form_uri_printer');
				$data['form_level_user_printer']	= $this->lang->line('form_level_user_printer');
				$data['form_pilihan_level_printer']	= $this->lang->line('form_pilihan_level_printer');
				$data['form_pilihan_user_printer']	= $this->lang->line('form_pilihan_user_printer');
				$data['link_aksi']		= $this->lang->line('link_aksi');
				$data['button_simpan']	= $this->lang->line('button_simpan');
				$data['button_batal']	= $this->lang->line('button_batal');
				$data['detail']		= "";
				$data['list']		= "";
				$data['not_defined']	= "Maaf, Printer yg anda masukan telah tersedia. Terimakasih.";
				$data['limages']	= base_url();
				$data['llevel']	= "";
				$this->load->model('printer/mclass');
				$data['isi']	= $this->mclass->view();
				$data['opt_level']	= $this->mclass->LevelUser();
				$this->load->view('printer/vmainform',$data);
			} else {
				$this->mclass->msimpan($eprintername,$euri,$ip,$ilevel,$iuserid);			
				
			}
		} else {
			$data['page_title_addprinter']	= $this->lang->line('page_title_addprinter');
			$data['form_tbl_printer_name']	= $this->lang->line('form_tbl_printer_name');
			$data['form_tbl_printer_uri']	= $this->lang->line('form_tbl_printer_uri');
			$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
			$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
			$data['form_name_printer']	= $this->lang->line('form_name_printer');
			$data['form_tbl_printer_ip']	= $this->lang->line('form_tbl_printer_ip');
			$data['form_ip_printer']	= $this->lang->line('form_ip_printer');
			$data['form_uri_printer']	= $this->lang->line('form_uri_printer');
			$data['form_level_user_printer']	= $this->lang->line('form_level_user_printer');
			$data['form_pilihan_level_printer']	= $this->lang->line('form_pilihan_level_printer');
			$data['form_pilihan_user_printer']	= $this->lang->line('form_pilihan_user_printer');
			$data['link_aksi']		= $this->lang->line('link_aksi');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih.";
			$data['llevel']	= "";
			$this->load->model('printer/mclass');
			$data['query']	= $this->mclass->view();
			$data['opt_level']	= $this->mclass->LevelUser();
			$data['isi']		= 'printer/vmainform';
			$this->load->view('template',$data);
			redirect('printer/cform');
		}
	}
	
	function actdelete() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$id = $this->uri->segment(4);

		$this->load->model('printer/mclass');
		$this->mclass->delete($id);
	}
	
	function edit() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$iprinter	= $this->uri->segment('4');

		$data['page_title_addprinter']	= $this->lang->line('page_title_addprinter');
		$data['form_tbl_printer_name']	= $this->lang->line('form_tbl_printer_name');
		$data['form_tbl_printer_uri']	= $this->lang->line('form_tbl_printer_uri');
		$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
		$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
		$data['form_name_printer']	= $this->lang->line('form_name_printer');
		$data['form_tbl_printer_ip']	= $this->lang->line('form_tbl_printer_ip');
		$data['form_ip_printer']	= $this->lang->line('form_ip_printer');
		$data['form_uri_printer']	= $this->lang->line('form_uri_printer');
		$data['form_level_user_printer']	= $this->lang->line('form_level_user_printer');
		$data['form_pilihan_level_printer']	= $this->lang->line('form_pilihan_level_printer');
		$data['form_pilihan_user_printer']	= $this->lang->line('form_pilihan_user_printer');
		$data['link_aksi']		= $this->lang->line('link_aksi');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$data['llevel']	= "";
		
		$this->load->model('printer/mclass');
		$data['opt_level']	= $this->mclass->LevelUser();
		$query	= $this->mclass->get($iprinter);
		
		$data['iprinter']  = $query['i_printer'];   
		$data['eprintername']= $query['e_printer_name'];
		$data['euri']	= $query['e_uri'];
		$data['ip']	= $query['ip'];
		$data['iuserid']	= $query['i_user_id'];
		$data['isi']		= 'printer/veditform';
			$this->load->view('template',$data);
		
	}
	
	function actedit() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$iprinter	= $this->input->post('i_printer_id')?trim($this->input->post('i_printer_id')):trim($this->input->get_post('i_printer_id'));
		$eprintername	= $this->input->post('e_printer_name')?trim($this->input->post('e_printer_name')):trim($this->input->get_post('e_printer_name'));
		$euri	= $this->input->post('e_uri')?$this->input->post('e_uri'):$this->input->get_post('e_uri');
		$ip	= $this->input->post('ip')?$this->input->post('ip'):$this->input->get_post('ip');
		
		$this->load->model('printer/mclass');
		if(!empty($eprintername) && !empty($euri) && !empty($ip) ) {
			$this->mclass->update($eprintername,$euri,$ip,$iprinter);	
		} else {
			$data['page_title_addprinter']	= $this->lang->line('page_title_addprinter');
			$data['form_tbl_printer_name']	= $this->lang->line('form_tbl_printer_name');
			$data['form_tbl_printer_uri']	= $this->lang->line('form_tbl_printer_uri');
			$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
			$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
			$data['form_name_printer']	= $this->lang->line('form_name_printer');
			$data['form_tbl_printer_ip']	= $this->lang->line('form_tbl_printer_ip');
			$data['form_ip_printer']	= $this->lang->line('form_ip_printer');
			$data['form_uri_printer']	= $this->lang->line('form_uri_printer');
			$data['form_level_user_printer']	= $this->lang->line('form_level_user_printer');
			$data['form_pilihan_level_printer']	= $this->lang->line('form_pilihan_level_printer');
			$data['form_pilihan_user_printer']	= $this->lang->line('form_pilihan_user_printer');
			$data['link_aksi']		= $this->lang->line('link_aksi');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Maaf, Printer tsb gagal di update, terimakasih";
			$data['limages']	= base_url();
			$data['llevel']	= "";
			$this->load->model('printer/mclass');
			$data['isi']	= $this->mclass->view();
			$data['opt_level']	= $this->mclass->LevelUser();
			$this->load->view('printer/vmainform',$data);			
		}
	}
}
?>
