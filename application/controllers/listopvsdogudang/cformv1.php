<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('listopvsdogudang/mclass');
	}
	
	function index() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0 ) 
		{	
			$data['page_title_opvsdo']	= $this->lang->line('page_title_opvsdo');
			$data['form_title_detail_opvsdo']	= $this->lang->line('form_title_detail_opvsdo');
			$data['list_opvsdo_tgl_mulai_op']	= $this->lang->line('list_opvsdo_tgl_mulai_op');
			$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
			$data['list_opvsdo_kd_brg']	= $this->lang->line('list_opvsdo_kd_brg');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['disabled']	= 'f';
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$data['customer']	= $this->mclass->lcustomer();
			
			$this->load->view('listopvsdogudang/vmainform',$data);
		}	
	}
	
	function carilistopvsdo() {
		$data['page_title_opvsdo']	= $this->lang->line('page_title_opvsdo');
		$data['form_title_detail_opvsdo']	= $this->lang->line('form_title_detail_opvsdo');
		$data['list_opvsdo_tgl_mulai_op']	= $this->lang->line('list_opvsdo_tgl_mulai_op');
		$data['list_opvsdo_t_permintaan']	= $this->lang->line('list_opvsdo_t_permintaan');
		$data['list_opvsdo_t_pengiriman']	= $this->lang->line('list_opvsdo_t_pengiriman');
		$data['list_opvsdo_n_permintaan']	= $this->lang->line('list_opvsdo_n_permintaan');
		$data['list_opvsdo_n_penjualan']	= $this->lang->line('list_opvsdo_n_penjualan');
		$data['list_opvsdo_selisih']	= $this->lang->line('list_opvsdo_selisih');
		$data['list_opvsdo_t_n_selisih']	= $this->lang->line('list_opvsdo_t_n_selisih');
		$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
		$data['list_opvsdo_kd_brg']	= $this->lang->line('list_opvsdo_kd_brg');
		$data['list_opvsdo_nm_brg']	= $this->lang->line('list_opvsdo_nm_brg');
		$data['list_opvsdo_unit_price']	= $this->lang->line('list_opvsdo_unit_price');
		$data['list_opvsdo_op']	= $this->lang->line('list_opvsdo_op');
		$data['list_opvsdo_n_op']	= $this->lang->line('list_opvsdo_n_op');
		$data['list_opvsdo_do']	= $this->lang->line('list_opvsdo_do');
		$data['list_opvsdo_n_do']	= $this->lang->line('list_opvsdo_n_do');
		$data['list_opvsdo_selisih_opdo']	= $this->lang->line('list_opvsdo_selisih_opdo');
		$data['list_opvsdo_n_selisih']	= $this->lang->line('list_opvsdo_n_selisih');
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$i_product	= $this->input->post('i_product');
		$iproduct	= !empty($i_product)?$i_product:"kosong";
		$d_op_first	= $this->input->post('d_op_first');
		$d_op_last	= $this->input->post('d_op_last');
		$f_stop_produksi	= $this->input->post('f_stop_produksi');
		$stopproduct		= $f_stop_produksi=='1'?"TRUE":"FALSE";
		$icustomer	= $this->input->post('customer');
		$fdropforcast	= $this->input->post('fdropforcast');
		$is_grosir	= $this->input->post('is_grosir');
		
		$data['tglopmulai']	= $d_op_first;
		$data['tglopakhir']	= $d_op_last;
		$data['kproduksi']	= $i_product;
		$data['sproduksi']	= $f_stop_produksi==1?" checked ":"";
		$data['fdropforcast']	= $fdropforcast;
		$data['is_grosir']	= $is_grosir;
		
		$e_d_do_first	= explode("/",$d_op_first,strlen($d_op_first));
		$e_d_do_last	= explode("/",$d_op_last,strlen($d_op_last));
		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";

		$data['var_iproduct']	= $iproduct;
		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;
		$data['var_stopproduct']= $stopproduct;
		$data['icustomer']		= $icustomer;
		
		$data['customer']	= $this->mclass->lcustomer();
		
		$data['isi']	= $this->mclass->clistopvsdo_new($icustomer, $i_product,$n_d_do_first,$n_d_do_last,$stopproduct,$fdropforcast,$is_grosir);
		$data['isixx']	= $this->mclass->clistopvsdo_dokosong($icustomer, $i_product,$n_d_do_first,$n_d_do_last,$stopproduct,$fdropforcast,$is_grosir);
			
		$this->load->view('listopvsdogudang/vlistform',$data);
	}
	
	function listbarangjadi() {
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listopvsdogudang/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('listopvsdogudang/vlistformbrgjadi',$data);	
	}

	function listbarangjadinext() {
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listopvsdogudang/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('listopvsdogudang/vlistformbrgjadi',$data);	
	}	
	
	function flistbarangjadi() {
		
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		if(!empty($key)) {
			$query	= $this->mclass->flbarangjadi($key);
			$jml	= $query->num_rows();
		} else {
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->iproduct."</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->imotif."</a></td>
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->motifname."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}
	
	function refreshpemenuhan() {
		
		$data['page_title_opvsdo']	= $this->lang->line('page_title_opvsdo');
		$data['form_title_detail_opvsdo']	= $this->lang->line('form_title_detail_opvsdo');
		$data['list_opvsdo_tgl_mulai_op']	= $this->lang->line('list_opvsdo_tgl_mulai_op');
		$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
		$data['list_opvsdo_kd_brg']	= $this->lang->line('list_opvsdo_kd_brg');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');
		$data['disabled']	= 't';
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$jml=1;
		
		$qorder	= $this->db->query(" SELECT * FROM tm_op WHERE f_op_cancel='f' AND f_op_close='f' AND f_do_created='t' AND (i_customer!='0' AND i_customer!='') ");
		if($qorder->num_rows()>0){
			foreach($qorder->result() as $rorder){
				$qorderitem	= $this->db->query(" SELECT i_op_item, i_op, i_product, n_count, n_residual, f_do_created FROM tm_op_item WHERE i_op='$rorder->i_op' AND length(e_product_name) > 9 AND length(i_product) > 8 AND f_koreksi_order='f' ORDER BY i_op_item ASC ");
	
				if($qorderitem->num_rows()>0){
					foreach($qorderitem->result() as $row){

						$qdeliveritem	= $this->db->query(" SELECT i_do, i_op, i_product, n_deliver FROM tm_do_item WHERE i_op='$rorder->i_op' AND i_product='$row->i_product' AND length(e_product_name) > 9 AND length(cast(i_op AS character varying)) > 6 ORDER BY i_do_item ASC ");
						
						$sisa=0;
						$antar=0;
						
						foreach($qdeliveritem->result() as $rdeliveritem){
							$antar	= $antar+$rdeliveritem->n_deliver;
						}
						
						$sisa	= (($row->n_count)-$antar);
						if($sisa<0 || $sisa==0){
							$status	= 't';
						}else{
							$status	= 'f';
						}						
						
						$this->db->query(" UPDATE tm_op_item SET n_residual='$sisa', f_do_created='$status', f_koreksi_order='t' WHERE i_op_item='$row->i_op_item' AND f_koreksi_order='f' ");
						$jml+=1;
													
					}
				}
			}	
		}
		
		print "<script>alert(\"Jml Order terupdate: '\"+$jml+\"' \");show(\"listopvsdo/cform\",\"#content\");</script>";

	}
}
?>
