<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-pembelian/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['isi'] = 'info-pembelian/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-pembelian/vformview';
    $jenis_beli = $this->input->post('jenis_beli', TRUE);
    $supplier = $this->input->post('supplier', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
    $jum_total = $this->mmaster->get_all_pembeliantanpalimit($jenis_beli, $date_from, $date_to, $supplier);
						/*	$config['base_url'] = base_url().'index.php/info-pembelian/cform/view/index/'.$date_from.'/'.$date_to.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						*/
	//$data['query'] = $this->mmaster->get_all_pembelian($config['per_page'],$this->uri->segment(7), $jenis_beli, $date_from, $date_to);						
	$data['query'] = $this->mmaster->get_all_pembelian($jenis_beli, $date_from, $date_to, $supplier);
	$data['jum_total'] = count($jum_total);
	//$data['cari'] = $keywordcari;
	//$data['list_supplier'] = $this->mmaster->get_supplier();
	//$data['csupplier'] = $csupplier;
	
	// ambil data nama supplier
	$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$supplier' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_supplier	= $hasilrow->nama;
	}
	else
		$nama_supplier = '';
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$data['supplier'] = $supplier;
	$data['nama_supplier'] = $nama_supplier;
	$this->load->view('template',$data);
  }
  
  //03-04-2012, ini utk cek sj pembelian yg tidak sinkron antara total di detail dgn di header
  function cek_sj_nonsinkron(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$data['query'] = $this->mmaster->get_sj_nonsinkron();
		$data['jum_total'] = count($data['query']);
		$data['isi'] = 'info-pembelian/vformviewsjnonsinkron';
		$this->load->view('template',$data);
  }

}
