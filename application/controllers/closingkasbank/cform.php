<?php
class Cform extends Controller {

	public $title  = 'Closing Kas dan Bank';
	public $folder = 'closingkasbank';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->folder.'/mclass');
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}

	public function index(){
		if ($this->session->userdata('ses_level')!='' || $this->session->userdata('ses_level')!=0) {
			$data = array(
				'folder' 	=> $this->folder,
				'title'  	=> $this->title,
				'dclosing' 	=> '04-'.date('m-Y'),
				'dopen'		=> '01-'.date('m-Y'),
			);
			$this->logger->write('Membuka Menu '.$this->title);
			$this->load->view($this->folder.'/vform', $data);
		}else{
			$this->load->view('index.php');
		}
	}
	
	public function closing(){
		if ($this->session->userdata('ses_level')!='' || $this->session->userdata('ses_level')!=0) {
			$dclosing = $this->input->post('dclosing', TRUE);
			$dopen 	  = $this->input->post('dopen', TRUE);
			$fkb 	  = $this->input->post('f_kb', TRUE);
			$fkbin 	  = $this->input->post('f_kbin', TRUE);
			$fkbank   = $this->input->post('f_kbank', TRUE);
			$fkbankin = $this->input->post('f_kbankin', TRUE);
			$fkk 	  = $this->input->post('f_kk', TRUE);
			$fkkin 	  = $this->input->post('f_kkin', TRUE);
			/*$judul 	  = $this->title;*/
			if ($dclosing!='' && $dopen!='') {
				$dateclosing = date('Y-m-d', strtotime($dclosing));
				$dateopen 	 = date('Y-m-d', strtotime($dopen));
				$this->db->trans_begin();
				$query = $this->mclass->cekclosing($dateclosing,$dateopen);
				if ($query->num_rows()>0) {
					if ($fkb=='on') {
						$this->mclass->closingkb($dateclosing,$dateopen);
						/*$judul = 'Kas Besar (Keluar)';*/
					}
					if ($fkbin=='on') {
						$this->mclass->closingkbin($dateclosing,$dateopen);
						/*$judul = 'Kas Besar (Masuk)';*/
					}
					if ($fkbank=='on') {
						$this->mclass->closingkbank($dateclosing,$dateopen);
						/*$judul = 'Kas Bank (Keluar)';*/
					}
					if ($fkbankin=='on') {
						$this->mclass->closingkbankin($dateclosing,$dateopen);
						/*$judul = 'Kas Bank (Masuk)';*/
					}
					if ($fkk=='on') {
						$this->mclass->closingkk($dateclosing,$dateopen);
						/*$judul = 'Kas Kecil (Keluar)';*/
					}
					if ($fkkin=='on') {
						$this->mclass->closingkkin($dateclosing,$dateopen);
						/*$judul = 'Kas Kecil (Masuk)';*/
					}
				}else{
					$this->mclass->closing($dateclosing,$dateopen);
				}
				if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$this->logger->write('Update Closing '.$this->title.', Tanggal Closing : '.$dclosing.', Tanggal Open : '.$dopen);
					print "<script>alert(\"Closing Berhasil. Terimakasih.\");show(\"".$this->folder."/cform\",\"#content\");</script>";
				}
			}else{
				print "<script>alert(\"Maaf, closing gagal disimpan. Terimakasih.\");show(\"".$this->folder."/cform\",\"#content\");</script>";
			}
		}else{
			$this->load->view('index.php');
		}
	}
}
?>
