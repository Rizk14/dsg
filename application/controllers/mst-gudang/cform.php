<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-gudang/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode_gudang = $row->kode_gudang;
			$enama = $row->nama;
			$eid_lokasi = $row->id_lokasi;
			$ekode_lokasi = $row->kode_lokasi;
			$ejenis = $row->jenis;
		}
	}
	else {
			$eid = '';
			$ekode_gudang = '';
			$enama = '';
			$eid_lokasi = '';
			$ekode_lokasi = '';
			$ejenis = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode_gudang'] = $ekode_gudang;
	$data['enama'] = $enama;
	$data['eid_lokasi'] = $eid_lokasi;
	$data['ekode_lokasi'] = $ekode_lokasi;
	$data['ejenis'] = $ejenis;
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	$data['query'] = $this->mmaster->getAll();
	$data['list_jenis'] = $this->mmaster->get_jenis();
	$data['lokasi'] = $this->mmaster->get_lokasi();
    $data['isi'] = 'mst-gudang/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		//if ($goedit == 1) {
			$this->form_validation->set_rules('kode_gudang', 'Kode Gudang', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		//}
		
		if ($this->form_validation->run() == FALSE)
		{
			//redirect('mst-gudang/cform');
			$data['isi'] = 'mst-gudang/vmainform';
			$data['msg'] = 'Kode Gudang dan Nama harus diisi..!';
			
			$eid = '';
			$ekode_gudang = '';
			$enama = '';
			$eid_lokasi = '';
			$ejenis = '';
			$edit = '';
			$data['eid'] = $eid;
			$data['ekode_gudang'] = $ekode_gudang;
			$data['enama'] = $enama;
			$data['eid_lokasi'] = $eid_lokasi;
			$data['ejenis'] = $ejenis;
			$data['edit'] = $edit;
			$data['query'] = $this->mmaster->getAll();
			$data['lokasi'] = $this->mmaster->get_lokasi();
			
			$this->load->view('template',$data);
		}
		else
		{
			$id_lokasi 	= $this->input->post('id_lokasi', TRUE);
			$kode_gudang 	= $this->input->post('kode_gudang', TRUE);
			$kodeeditlok 	= $this->input->post('kodeeditlok', TRUE); 
			$kodeeditgud 	= $this->input->post('kodeeditgud', TRUE); 
			$id_gudang	 	= $this->input->post('id_gudang', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$jenis 	= $this->input->post('jenis', TRUE);
			
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($id_lokasi, $kode_gudang);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-gudang/vmainform';
					$data['msg'] = 'Data sudah ada..!';
					
					$eid = '';
					$ekode_gudang = '';
					$enama = '';
					$eid_lokasi = '';
					$ejenis = '';
					$edit = '';
					$data['eid'] = $eid;
					$data['ekode_gudang'] = $ekode_gudang;
					$data['enama'] = $enama;
					$data['eid_lokasi'] = $eid_lokasi;
					$data['ejenis'] = $ejenis;
					$data['edit'] = $edit;
					$data['query'] = $this->mmaster->getAll();
					$data['lokasi'] = $this->mmaster->get_lokasi();

					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($id_lokasi, $kode_gudang, $id_gudang, $nama, $jenis, $goedit);
					/*$data['isi'] = 'mst-gudang/vmainform';
					$data['msg'] = 'Simpan data berhasil';
					$this->load->view('template',$data); */
					redirect('mst-gudang/cform');
				}
			} // end if goedit == ''
			else {
					$cek_data = $this->mmaster->cek_data($id_lokasi, $kode_gudang);
				if (($id_lokasi != $kodeeditlok) && ($kode_gudang != $kodeeditgud)) {
					if (count($cek_data) == 0) {
						$this->mmaster->save($id_lokasi, $kode_gudang, $id_gudang, $nama, $jenis, $goedit);
						redirect('mst-gudang/cform');
					}
					else {
						$data['isi'] = 'mst-gudang/vmainform';
						$data['msg'] = 'Data sudah ada..!';
						
						$edit = '1';
						$data['eid'] = $id_gudang;
						$data['ekode_gudang'] = $kode_gudang;
						$data['enama'] = $nama;
						$data['eid_lokasi'] = $id_lokasi;
						$data['ejenis'] = $jenis;
						$data['edit'] = $edit;
						$data['query'] = $this->mmaster->getAll();
						$data['lokasi'] = $this->mmaster->get_lokasi();

						$this->load->view('template',$data);
					}
				}
				else {
					$this->mmaster->save($id_lokasi, $kode_gudang, $id_gudang, $nama, $jenis, $goedit);
					redirect('mst-gudang/cform');
				}
			}
		}
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-gudang/cform');
  }
  
  
}
