<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-unit-jahit/mmaster');
  }

  function index(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->kode_unit;
			$elokasi = $row->lokasi;
			$enama = $row->nama;
		}
	}
	else {
			$ekode = '';
			$elokasi = '';
			$enama = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['elokasi'] = $elokasi;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-unit-jahit/vmainform';
    
	$this->load->view('template',$data);
 //   $this->load->view('mst-kel-barang/input',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode_unit', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode_unit', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			//$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('mst-unit-jahit/cform');
		}
		else
		{
			$kode 	= $this->input->post('kode_unit', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$lokasi = $this->input->post('lokasi', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			
			$this->mmaster->save($kode, $kodeedit, $lokasi, $nama, $goedit);
			redirect('mst-unit-jahit/cform');
		}
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-unit-jahit/cform');
  }
  
  
}
