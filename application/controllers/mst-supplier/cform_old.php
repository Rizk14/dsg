<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-supplier/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->kode_supplier;
			$enama = $row->nama;
			$ealamat = $row->alamat;
			$ekota = $row->kota;
			$ekontak_person = $row->kontak_person;
			$etelp = $row->telp;
			$efax = $row->fax;
			$etop = $row->top;
			$epkp = $row->pkp;
			$enpwp = $row->npwp;
			$enama_npwp = $row->nama_npwp;
			$etipe_pajak = $row->tipe_pajak;
			$ekategori = $row->kategori;
		}
	}
	else {
			$ekode = '';
			$enama = '';
			$ealamat = '';
			$ekota = '';
			$ekontak_person = '';
			$etelp = '';
			$efax = '';
			$etop = '';
			$epkp = '';
			$enpwp = '';
			$enama_npwp = '';
			$etipe_pajak = '';
			$ekategori = '1';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['ealamat'] = $ealamat;
	$data['ekota'] = $ekota;
	$data['ekontak_person'] = $ekontak_person;
	$data['etelp'] = $etelp;
	$data['efax'] = $efax;
	$data['etop'] = $etop;
	$data['epkp'] = $epkp;
	$data['enpwp'] = $enpwp;
	$data['enama_npwp'] = $enama_npwp;
	$data['etipe_pajak'] = $etipe_pajak;
	$data['ekategori'] = $ekategori;
	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	//$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-supplier/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	/*$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode Supplier', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required');
			$this->form_validation->set_rules('telp', 'Telepon', 'required');
			$this->form_validation->set_rules('top', 'TOP', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode Supplier', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required');
			$this->form_validation->set_rules('telp', 'Telepon', 'required');
			$this->form_validation->set_rules('top', 'TOP', 'required');
		} */
		$goedit 	= $this->input->post('goedit', TRUE);
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$alamat 	= $this->input->post('alamat', TRUE);
			$kota 	= $this->input->post('kota', TRUE);
			$kontak_person 	= $this->input->post('kontak_person', TRUE);
			$telp 	= $this->input->post('telp', TRUE);
			$fax 	= $this->input->post('fax', TRUE);
			$top 	= $this->input->post('top', TRUE);
			$pkp 	= $this->input->post('pkp', TRUE);
			$npwp 	= $this->input->post('npwp', TRUE);
			$nama_npwp 	= $this->input->post('nama_npwp', TRUE);
			$tipe_pajak = $this->input->post('tipe_pajak', TRUE);
			$kategori = $this->input->post('kategori', TRUE);
			
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-supplier/vmainform';
					$data['msg'] = 'Data sudah ada..!';
					$ekode = '';
					$enama = '';
					$ealamat = '';
					$ekota = '';
					$ekontak_person = '';
					$etelp = '';
					$efax = '';
					$etop = '';
					$epkp = '';
					$enpwp = '';
					$enama_npwp = '';
					$etipe_pajak = '';
					$ekategori = '1';
					$edit = '';
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;
					$data['ealamat'] = $ealamat;
					$data['ekota'] = $ekota;
					$data['ekontak_person'] = $ekontak_person;
					$data['etelp'] = $etelp;
					$data['efax'] = $efax;
					$data['etop'] = $etop;
					$data['epkp'] = $epkp;
					$data['enpwp'] = $enpwp;
					$data['enama_npwp'] = $enama_npwp;
					$data['etipe_pajak'] = $etipe_pajak;
					$data['ekategori'] = $ekategori;
					
					$data['edit'] = $edit;
					
					//$data['query'] = $this->mmaster->getAll();
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($kode,$nama, $alamat,$kota, $kontak_person, $telp, $fax, $top, $pkp, $npwp, $nama_npwp, $tipe_pajak, $kategori, $kodeedit, $goedit);
					redirect('mst-supplier/cform/view');
				}
			} // end if goedit == ''
			else {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'mst-supplier/vmainform';
						$data['msg'] = "Data kode ".$kode." sudah ada..!";
						$data['ekode'] = $kode;
						$data['enama'] = $nama;
						$data['ealamat'] = $alamat;
						$data['ekota'] = $kota;
						$data['ekontak_person'] = $kontak_person;
						$data['etelp'] = $telp;
						$data['efax'] = $fax;
						$data['etop'] = $top;
						$data['epkp'] = $pkp;
						$data['enpwp'] = $npwp;
						$data['enama_npwp'] = $nama_npwp;
						$data['etipe_pajak'] = $tipe_pajak;
						$data['ekategori'] = $ekategori;
						$edit = '1';
						$data['edit'] = $edit;
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kode,$nama, $alamat,$kota, $kontak_person, $telp, $fax, $top, $pkp, $npwp, $nama_npwp, $tipe_pajak, $kategori, $kodeedit, $goedit);
						redirect('mst-supplier/cform/view');
					}
				}
				else {
					$this->mmaster->save($kode,$nama, $alamat,$kota, $kontak_person, $telp, $fax, $top, $pkp, $npwp, $nama_npwp, $tipe_pajak, $kategori, $kodeedit, $goedit);
					redirect('mst-supplier/cform/view');
				}
			}
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $keywordcari = $this->input->post('cari', TRUE);
    //echo $keywordcari; die();
    if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
    
    if ($keywordcari == '')
		$keywordcari 	= "all";
		
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);	
						$config['base_url'] = base_url().'index.php/mst-supplier/cform/view/index/'.$keywordcari.'/';
						//$config['total_rows'] = $query->num_rows(); 
						$config['total_rows'] = count($jum_total); 
						$config['per_page'] = '10';
						$config['first_link'] = 'Awal';
						$config['last_link'] = 'Akhir';
						$config['next_link'] = 'Selanjutnya';
						$config['prev_link'] = 'Sebelumnya';
						$config['cur_page'] = $this->uri->segment(6);
						$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $keywordcari);	
	$data['jum_total'] = count($jum_total);	
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['isi'] = 'mst-supplier/vformview';
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-supplier/cform/view');
  }
}
