<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('mst-supplier/mmaster');
	}

	// 11-06-2015, skrg pake field id utk primary key. nanti di tabel2 lain ganti pake id_supplier, bukan kode_supplier
	function updateidsupplier()
	{
		$sql = " SELECT * FROM tm_supplier ORDER BY tgl_input ";
		$query	= $this->db->query($sql);

		if ($query->num_rows() > 0) {
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil id tertinggi
				$sql2 = " SELECT id FROM tm_supplier ORDER BY id DESC LIMIT 1 ";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0) {
					$hasil2 = $query2->row();
					$idlama	= $hasil2->id;
					$idbaru = $idlama + 1;

					$this->db->query(" UPDATE tm_supplier SET id='$idbaru' WHERE kode_supplier='$row1->kode_supplier' ");
				}
			}
		}
		echo "sukses";
	}

	function index()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_supplier 	= $this->uri->segment(4);

		if ($id_supplier != '') {
			$hasil = $this->mmaster->get($id_supplier);
			$edit = 1;

			foreach ($hasil as $row) {
				$eid = $row->id;
				$ekode = $row->kode_supplier;
				$enama = $row->nama;
				$ealamat = $row->alamat;
				$ekota = $row->kota;
				$ekontak_person = $row->kontak_person;
				$etelp = $row->telp;
				$efax = $row->fax;
				$etop = $row->top;
				$epkp = $row->pkp;
				$enpwp = $row->npwp;
				$enama_npwp = $row->nama_npwp;
				$etipe_pajak = $row->tipe_pajak;
				$jenis = $row->jenis;
				$kategori = $row->kategori;
				$einisial = substr($ekode, 1, 1);
				$statusaktif = $row->f_aktif;
				$paymenttype = $row->payment_type; /* PENAMBAHAN 11 AGS 2022 */
				$bankname = $row->bank_name; /* PENAMBAHAN 11 AGS 2022 */
				$accountnumber = $row->account_number; /* PENAMBAHAN 11 AGS 2022 */
				$accountname = $row->account_name; /* PENAMBAHAN 11 AGS 2022 */
				$email = $row->email; /* PENAMBAHAN 11 OKT 2022 */
			}
			$ekategori =  $this->mmaster->getekategori();
			$ejeniskat =  $this->mmaster->getekategorijenis();
			//~ $ekategori =  $this->mmaster->getekategori($kategori);
			//~ $ejeniskat =  $this->mmaster->getekategorijenis($jenis);
		} else {
			$eid = '';
			$ekode = '';
			$enama = '';
			$ealamat = '';
			$ekota = '';
			$ekontak_person = '';
			$etelp = '';
			$efax = '';
			$etop = '';
			$epkp = '';
			$enpwp = '';
			$enama_npwp = '';
			$etipe_pajak = '';
			$jenis = '';
			$kategori = '';
			$ekategori =  $this->mmaster->getekategori();
			$ejeniskat =  $this->mmaster->getekategorijenis();
			//~ $ekategori =  $this->mmaster->getekategori($kategori);
			//~ $ejeniskat =  $this->mmaster->getekategorijenis($jenis);
			$einisial = '';
			$edit = '';
			$statusaktif = 't';
			$paymenttype = '';
			$bankname = '';
			$accountnumber = '';
			$accountname = '';
			$email = '';
		}
		$data['eid'] = $eid;
		$data['ekode'] = $ekode;
		$data['enama'] = $enama;
		$data['ealamat'] = $ealamat;
		$data['ekota'] = $ekota;
		$data['ekontak_person'] = $ekontak_person;
		$data['etelp'] = $etelp;
		$data['efax'] = $efax;
		$data['etop'] = $etop;
		$data['epkp'] = $epkp;
		$data['enpwp'] = $enpwp;
		$data['enama_npwp'] = $enama_npwp;
		$data['etipe_pajak'] = $etipe_pajak;
		$data['ekategori'] = $ekategori;
		$data['ejeniskat'] = $ejeniskat;
		$data['kategori'] = $kategori;
		$data['jenis'] = $jenis;
		$data['einisial'] = $einisial;

		$statusaktif = $statusaktif == 't' ? 'checked' : '';

		$data['statusaktif'] = $statusaktif;

		$data['paymenttype'] = $paymenttype; /* PENAMBAHAN 11 AGS 2022 */
		$data['bankname'] = $bankname; /* PENAMBAHAN 11 AGS 2022 */
		$data['accountnumber'] = $accountnumber; /* PENAMBAHAN 11 AGS 2022 */
		$data['accountname'] = $accountname; /* PENAMBAHAN 11 AGS 2022 */
		$data['email'] = $email; /* PENAMBAHAN 11 OKT 2022 */

		$data['edit'] = $edit;
		$data['msg'] = '';

		//$data['query'] = $this->mmaster->getAll();
		$data['isi'] = 'mst-supplier/vmainform';

		$this->load->view('template', $data);
	}

	function submit()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		//$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);

		// 29-06-2015 dimodif
		//$kode 	= $this->input->post('kode', TRUE);
		//$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$inisial = $this->input->post('inisial', TRUE);

		$id_supplier 	= $this->input->post('id_supplier', TRUE);
		$nama 	= $this->input->post('nama', TRUE);
		$alamat 	= $this->input->post('alamat', TRUE);
		$telp 	= $this->input->post('telp', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode Supplier', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required');
			$this->form_validation->set_rules('telp', 'Telepon', 'required');
			$this->form_validation->set_rules('top', 'TOP', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode Supplier', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required');
			$this->form_validation->set_rules('telp', 'Telepon', 'required');
			$this->form_validation->set_rules('top', 'TOP', 'required');
		} */

		//if ($this->form_validation->run() == FALSE)
		/*if ($kode == '' || $nama == '' || $alamat == '' || $telp == '')
		{
			//redirect('mst-unit-quilting/cform');
			$data['isi'] = 'mst-supplier/vmainform';
			$data['msg'] = 'Field2 kode, nama, alamat, telp, dan TOP tidak boleh kosong..!';
			$ekode = '';
			$enama = '';
			$ealamat = '';
			$ekota = '';
			$ekontak_person = '';
			$etelp = '';
			$efax = '';
			$etop = '';
			$epkp = '';
			$enpwp = '';
			$enama_npwp = '';
			$etipe_pajak = '';
			$ekategori = '1';
			$edit = '';
			
			$data['ekode'] = $ekode;
			$data['enama'] = $enama;
			$data['ealamat'] = $ealamat;
			$data['ekota'] = $ekota;
			$data['ekontak_person'] = $ekontak_person;
			$data['etelp'] = $etelp;
			$data['efax'] = $efax;
			$data['etop'] = $etop;
			$data['epkp'] = $epkp;
			$data['enpwp'] = $enpwp;
			$data['enama_npwp'] = $enama_npwp;
			$data['etipe_pajak'] = $etipe_pajak;
			$data['ekategori'] = $ekategori;
			
			$data['edit'] = $edit;
			
			//$data['query'] = $this->mmaster->getAll();
			$this->load->view('template',$data);
		}
		else */
		//{
		$kode 	= $this->input->post('kode', TRUE);
		$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$nama 	= $this->input->post('nama', TRUE);
		$alamat 	= $this->input->post('alamat', TRUE);
		$kota 	= $this->input->post('kota', TRUE);
		$kontak_person 	= $this->input->post('kontak_person', TRUE);
		$telp 	= $this->input->post('telp', TRUE);
		$fax 	= $this->input->post('fax', TRUE);
		$top 	= $this->input->post('top', TRUE);
		$pkp 	= $this->input->post('pkp', TRUE);
		$npwp 	= $this->input->post('npwp', TRUE);
		$nama_npwp 	= $this->input->post('nama_npwp', TRUE);
		$tipe_pajak = $this->input->post('tipe_pajak', TRUE);
		$kategori = $this->input->post('kategori', TRUE);
		$jenis = $this->input->post('jenis', TRUE);
		$statusaktif = $this->input->post('chkstatus', TRUE);
		$paymenttype = $this->input->post('paymenttype', TRUE); /* PENAMBAHAN 11 AGS 2022 */
		$bankname = $this->input->post('bankname', TRUE); /* PENAMBAHAN 11 AGS 2022 */
		$accountnumber = $this->input->post('accountnumber', TRUE); /* PENAMBAHAN 11 AGS 2022 */
		$accountname = $this->input->post('accountname', TRUE); /* PENAMBAHAN 11 AGS 2022 */
		$email = $this->input->post('email', TRUE); /* PENAMBAHAN 11 OKT 2022 */

		$statusaktif = $statusaktif != '' ? 't' : 'f';

		if ($goedit == '') {
			// 29-06-2015, dikomen aja. pake kode otomatis
			$queryxx = $this->db->query(" SELECT kode_supplier FROM tm_supplier
													WHERE kode_supplier LIKE 'S" . $inisial . "%' ORDER BY kode_supplier DESC LIMIT 1 ");

			if ($queryxx->num_rows() > 0) {
				$hasilxx = $queryxx->row();
				$kode_supplier_last = $hasilxx->kode_supplier;
				$digitbaru = (substr($kode_supplier_last, 2, 3)) + 1; // 3 digit no urut

				switch (strlen($digitbaru)) {
					case "1":
						$newdigit	= "00" . $digitbaru;
						break;
					case "2":
						$newdigit	= "0" . $digitbaru;
						break;
					case "3":
						$newdigit	= $digitbaru;
						break;
				}
				$kode_supplier_baru = 'S' . $inisial . $newdigit;
			} else
				$kode_supplier_baru = 'S' . $inisial . '001';

			/*$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-supplier/vmainform';
					$data['msg'] = 'Data kode supplier '.$kode.' sudah ada..!';
					$eid = '';
					$ekode = '';
					$enama = '';
					$ealamat = '';
					$ekota = '';
					$ekontak_person = '';
					$etelp = '';
					$efax = '';
					$etop = '';
					$epkp = '';
					$enpwp = '';
					$enama_npwp = '';
					$etipe_pajak = '';
					$ekategori = '1';
					$edit = '';
					$data['eid'] = $eid;
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;
					$data['ealamat'] = $ealamat;
					$data['ekota'] = $ekota;
					$data['ekontak_person'] = $ekontak_person;
					$data['etelp'] = $etelp;
					$data['efax'] = $efax;
					$data['etop'] = $etop;
					$data['epkp'] = $epkp;
					$data['enpwp'] = $enpwp;
					$data['enama_npwp'] = $enama_npwp;
					$data['etipe_pajak'] = $etipe_pajak;
					$data['ekategori'] = $ekategori;
					
					$data['edit'] = $edit;
					
					//$data['query'] = $this->mmaster->getAll();
					$this->load->view('template',$data);
				}
				else { */
			$this->mmaster->save($kode_supplier_baru, $nama, $alamat, $kota, $kontak_person, $telp, $fax, $top, $pkp, $npwp, $nama_npwp, $tipe_pajak, $kategori, $id_supplier, $goedit, $jenis, $statusaktif, $paymenttype, $bankname, $accountnumber, $accountname, $email);
			redirect('mst-supplier/cform/view');
			//}
		} // end if goedit == ''
		else {
			// EDIT DATA
			// 29-06-2015 DIGANTI
			/*if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'mst-supplier/vmainform';
						$data['msg'] = "Data kode supplier ".$kode." sudah ada..!";
						$data['eid'] = $id_supplier;
						$data['ekode'] = $kode;
						$data['enama'] = $nama;
						$data['ealamat'] = $alamat;
						$data['ekota'] = $kota;
						$data['ekontak_person'] = $kontak_person;
						$data['etelp'] = $telp;
						$data['efax'] = $fax;
						$data['etop'] = $top;
						$data['epkp'] = $pkp;
						$data['enpwp'] = $npwp;
						$data['enama_npwp'] = $nama_npwp;
						$data['etipe_pajak'] = $tipe_pajak;
						$data['ekategori'] = $ekategori;
						$edit = '1';
						$data['edit'] = $edit;
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kode,$nama, $alamat,$kota, $kontak_person, $telp, $fax, $top, $pkp, $npwp, $nama_npwp, $tipe_pajak, $kategori, $id_supplier, $goedit);
						redirect('mst-supplier/cform/view');
					}
				}
				else { */
			$kode_supplier_baru = '';
			$this->mmaster->save($kode_supplier_baru, $nama, $alamat, $kota, $kontak_person, $telp, $fax, $top, $pkp, $npwp, $nama_npwp, $tipe_pajak, $kategori, $id_supplier, $goedit, $jenis, $statusaktif, $paymenttype, $bankname, $accountnumber, $accountname, $email);
			redirect('mst-supplier/cform/view');
			//}
		}

		//}
	}

	function view()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$keywordcari = $this->input->post('cari', TRUE);
		//echo $keywordcari; die();
		if ($keywordcari == '') {
			$keywordcari 	= $this->uri->segment(5);
		}

		if ($keywordcari == '')
			$keywordcari 	= "all";

		$jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
		$config['base_url'] = base_url() . 'index.php/mst-supplier/cform/view/index/' . $keywordcari . '/';
		//$config['total_rows'] = $query->num_rows(); 
		$config['total_rows'] = count($jum_total);
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(6);
		$this->pagination->initialize($config);
		$data['query'] = $this->mmaster->getAll($config['per_page'], $this->uri->segment(6), $keywordcari);
		$data['jum_total'] = count($jum_total);

		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$data['isi'] = 'mst-supplier/vformview';
		$this->load->view('template', $data);
	}

	function delete()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$kode 	= $this->uri->segment(4);
		$this->mmaster->delete($kode);
		redirect('mst-supplier/cform/view');
	}

	// 18-08-2014
	function exportexcel()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$jenis_dokumen 	= $this->uri->segment(4);

		$query = $this->mmaster->getAlltanpalimit("all");

		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='8' align='center'>DATA SUPPLIER</th>
		 </tr>
		 <tr>
			 <th>Kode</th>
			 <th>Nama Supplier</th>
			 <th>Alamat</th>
			 <th>Kota</th>
			 <th>Telepon</th>
			 <th>Kontak Person</th>
			 <th>PKP</th>
			 <th>Kategori</th>
		 </tr>
		</thead>
		<tbody>";

		foreach ($query as $row) {
			$html_data .= "<tr>";
			$html_data .=    "<td>$row->kode_supplier</td>";
			$html_data .=    "<td>$row->nama</td>";
			$html_data .=    "<td>$row->alamat</td>";
			$html_data .=    "<td>$row->kota</td>";
			$html_data .=    "<td>$row->telp</td>";
			$html_data .=    "<td>$row->kontak_person</td>";
			if ($row->pkp == 't')
				$html_data .=    "<td>Ya</td>";
			else
				$html_data .=    "<td>Tidak</td>";
			if ($row->kategori == 1)
				$html_data .=    "<td>Pembelian</td>";
			else
				$html_data .=    "<td>Makloon</td>";
			$html_data .=  "</tr>";
		}

		$html_data .= "</tbody>
		</table>";

		$nama_file = "master_supplier";
		if ($jenis_dokumen == '1') {
			$export_excel1 = "excel";
			$nama_file .= ".xls";
		} else {
			$export_excel1 = "";
			$nama_file .= ".ods";
		}
		$data = $html_data;

		$dir = getcwd();
		include($dir . "/application/libraries/generateExcelFile.php");
		return true;
	}
}
