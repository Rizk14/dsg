<?php
include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {
	
	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0 ) 
		{	
			$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
			$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
			$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
			$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
			$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$this->load->view('prntfpajak_test/vmainform',$data);
		}
	}
	
	function carilistpenjualanperdo() {
		$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
		$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
		$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
		$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
		$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
		$data['list_pjkpenjperdo_no_do']	= $this->lang->line('list_pjkpenjperdo_no_do');
		$data['list_pjkpenjperdo_nm_brg']	= $this->lang->line('list_pjkpenjperdo_nm_brg');
		$data['list_pjkpenjperdo_qty']	= $this->lang->line('list_pjkpenjperdo_qty');
		$data['list_pjkpenjperdo_hjp']	= $this->lang->line('list_pjkpenjperdo_hjp');
		$data['list_pjkpenjperdo_amount']	= $this->lang->line('list_pjkpenjperdo_amount');
		$data['list_pjkpenjperdo_total_pengiriman']	= $this->lang->line('list_pjkpenjperdo_total_pengiriman');
		$data['list_pjkpenjperdo_total_penjualan']	= $this->lang->line('list_pjkpenjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		// 11-10-2012
		$fpengganti	= $this->input->post('fpengganti');
		// end 11-10-2012
		
		$nofaktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->input->get_post('no_faktur');
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->input->get_post('d_do_first');
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->input->get_post('d_do_last');

		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";
		
		$data['tgldomulai']	= $d_do_first; // DD/MM/YYYY
		$data['tgldoakhir']	= $d_do_last;
		
		$data['tgldomulai2']= $n_d_do_first; // YYYY-MM-DD
		$data['tgldoakhir2']= $n_d_do_last;
		
		$data['nofaktur']	= $nofaktur;
		if ($fpengganti == '')
			$fpengganti = 'n';
		$data['fpengganti']	= $fpengganti;
						
		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";
		
		$this->load->model('prntfpajak_test/mclass');
		$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo($nofaktur,$n_d_do_first,$n_d_do_last, $fpengganti);
		$this->load->view('prntfpajak_test/vlistform',$data);
	}
	
	function cpenjualanperdo() {
		$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
		$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
		$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
		$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
		$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
		$data['list_pjkpenjperdo_no_do']	= $this->lang->line('list_pjkpenjperdo_no_do');
		$data['list_pjkpenjperdo_nm_brg']	= $this->lang->line('list_pjkpenjperdo_nm_brg');
		$data['list_pjkpenjperdo_qty']	= $this->lang->line('list_pjkpenjperdo_qty');
		$data['list_pjkpenjperdo_hjp']	= $this->lang->line('list_pjkpenjperdo_hjp');
		$data['list_pjkpenjperdo_amount']	= $this->lang->line('list_pjkpenjperdo_amount');
		$data['list_pjkpenjperdo_total_pengiriman']	= $this->lang->line('list_pjkpenjperdo_total_pengiriman');
		$data['list_pjkpenjperdo_total_penjualan']	= $this->lang->line('list_pjkpenjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		$nofaktur	= $this->uri->segment(4); 
		$d_do_first	= $this->uri->segment(5); // YYYY-MM-DD
		$d_do_last	= $this->uri->segment(6);
		$fpengganti	= $this->uri->segment(7);
		
		$exp_ddofirst	= explode("-",$d_do_first,strlen($d_do_first)); // YYYY-MM-DD
		$exp_ddolast	= explode("-",$d_do_last,strlen($d_do_last));
		$new_ddofirst	= !empty($exp_ddofirst[2])?$exp_ddofirst[2].'/'.$exp_ddofirst[1].'/'.$exp_ddofirst[0]:""; // DD-MM-YYYY
		$new_ddolast	= !empty($exp_ddolast[2])?$exp_ddolast[2].'/'.$exp_ddolast[1].'/'.$exp_ddolast[0]:""; // DD-MM-YYYY
		
		$data['tgldomulai']	= $new_ddofirst; // DD/MM/YYYY
		$data['tgldoakhir']	= $new_ddolast;
		$data['tgldomulai2']	= $d_do_first; // YYYY-MM-DD
		$data['tgldoakhir2']	= $d_do_last;
		$data['nofaktur']	= $nofaktur;
		$data['fpengganti']	= $fpengganti;
		
		$this->load->model('prntfpajak_test/mclass');
	
		$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo($nofaktur,$d_do_first,$d_do_last, $fpengganti);
		$this->load->view('prntfpajak_test/vprintform',$data);
	}
	
	function cpopup() {
		
		/*
		Nomor Seri Faktur Pajak : 010.000.10.00000001
		011	= pada digit titik pertama jensi faktur, 011 jika faktur pengganti
		10	= pada digit titik ke-tiga adalah tahun sekarang
		*/
		
		$this->load->model('prntfpajak_test/mclass');
		
		$iduserid	= $this->session->userdata('user_idx');
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.194';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;

		$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
		$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
		$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
		$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
		$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
		$data['list_pjkpenjperdo_no_do']	= $this->lang->line('list_pjkpenjperdo_no_do');
		$data['list_pjkpenjperdo_nm_brg']	= $this->lang->line('list_pjkpenjperdo_nm_brg');
		$data['list_pjkpenjperdo_qty']	= $this->lang->line('list_pjkpenjperdo_qty');
		$data['list_pjkpenjperdo_hjp']	= $this->lang->line('list_pjkpenjperdo_hjp');
		$data['list_pjkpenjperdo_amount']	= $this->lang->line('list_pjkpenjperdo_amount');
		$data['list_pjkpenjperdo_total_pengiriman']	= $this->lang->line('list_pjkpenjperdo_total_pengiriman');
		$data['list_pjkpenjperdo_total_penjualan']	= $this->lang->line('list_pjkpenjperdo_total_penjualan');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		$qpenyetorpajak		= $this->mclass->penyetorpajak();
		if($qpenyetorpajak->num_rows()>0) {
			$rpenyetorpajak	= $qpenyetorpajak->row();
			$data['TtdPajak01']	= $rpenyetorpajak->e_penyetor;
		}else{
			$data['TtdPajak01']	= $this->lang->line('TtdPajak01');
		}
		
		$data['TtdPajak02']	= $this->lang->line('TtdPajak02');
		
		$data['falamat']	= $this->lang->line('AddInisial');
				
		$nofaktur	= $this->uri->segment(4); 
		$d_do_first	= $this->uri->segment(5); // YYYY-MM-DD
		$d_do_last	= $this->uri->segment(6);
		$fpengganti	= $this->uri->segment(7);

		$bglobal	= array(
			'01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
			'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
		
		$tanggal= date("d");
		$bulan	= date("m");
		$tahun	= date("Y");
		
		$tgl	= substr($tanggal,0,1)=='0'?substr($tanggal,1,1):$tanggal;
		$nw_tgl	= $tgl;
		$bln	= $bglobal[$bulan];
		
		$data['ftgl']	= $nw_tgl." ".$bln." ".$tahun;
		
		$this->mclass->fprinted($nofaktur);
		
		/*
		$qry_source_remote	= $this->mclass->remote($remote);
		*/
		$qry_source_remote	= $this->mclass->remote($iduserid);

		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;
		
		$qry_infoheader		= $this->mclass->clistfpenjperdo($nofaktur,$d_do_first,$d_do_last);
			
		if($qry_infoheader->num_rows() > 0) {
			
			$row_infoheader	= $qry_infoheader->row();
			$nofaktur		= $row_infoheader->ifakturcode;
			$tglfaktur		= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$tglpajak		= $row_infoheader->dpajak;
			
			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$exp_tpajak		= explode("-",$tglpajak,strlen($tglpajak));	
			
			$tahunpajak	= !empty($exp_tpajak[0])?substr($exp_tpajak[0],2,2):date("y");
			
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];
			
			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']		= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;
		}
		
		$qry_jml	= $this->mclass->clistfpenjperdo_jml($nofaktur,$d_do_first,$d_do_last);
		$qry_totalnyaini	= $this->mclass->clistfpenjperdo_totalnyaini($nofaktur,$d_do_first,$d_do_last);
		
		if($qry_jml->num_rows() > 0) {
			$row_jml = $qry_jml->row_array();		
			$row_totalnyaini = $qry_totalnyaini->row_array();
			$total	 = $row_totalnyaini['totalnyaini'];
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			/*
			$total=0;
			$n_disc=0;
			$v_disc=0;
			foreach($qry_jml->result() as $row){
				$total	 += $row->total;
				$n_disc	 += $row->n_disc;
				$v_disc	 += $row->v_disc;
			}
			*/
			$total_pls_disc	= $total - $v_disc; // DPP
			$nilai_ppn		= (($total_pls_disc*10) / 100 ); // PPN
			$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			
			$data['jumlah']	= $total; 
			$data['dpp']	= $total_pls_disc; // DPP = Dasar Pengenaan Pajak
			$data['diskon']	= $v_disc;
			$data['nilai_ppn']		= $nilai_ppn;
			$data['nilai_faktur']	= round($nilai_faktur);
		} else {
			$data['jumlah']	= 0;
			$data['dpp']	= 0;
			$data['diskon']	= 0;
			$data['nilai_ppn']		= 0;
			$data['nilai_faktur']	= 0;		
		}
		
		$qrypelanggan	= $this->mclass->pelanggan($nofaktur);
		if($qrypelanggan->num_rows() >0) {
			$row_pelanggan		= $qrypelanggan->row_array();
			
			$data['nmkostumer']	= $row_pelanggan['customername'];
			$data['almtkostumer']	= $row_pelanggan['customeraddress'];
			$data['npwp']		= $row_pelanggan['npwp'];
		}
		
		$ititas			= $this->mclass->ititas();
		if($ititas->num_rows() > 0) {
			$row_ititas	= $ititas->row();
			$data['nminitial']	= $row_ititas->e_initial_name;
			$data['almtperusahaan']	= $row_ititas->e_initial_address;
			$data['npwpperusahaan']	= $row_ititas->e_initial_npwp;
		}
		$nopajak		= $this->mclass->pajak($nofaktur);
		if($nopajak->num_rows() > 0) {
			$row_pajak	= $nopajak->row();			
			$nomorpajak	= $row_pajak->ifakturpajak;			
			
			switch(strlen($nomorpajak)) {
				case "1":
					$nfaktur	= '0000000'.$nomorpajak;
				break;
				case "2":
					$nfaktur	= '000000'.$nomorpajak;
				break;
				case "3":
					$nfaktur	= '00000'.$nomorpajak;
				break;
				case "4":
					$nfaktur	= '0000'.$nomorpajak;
				break;
				case "5":
					$nfaktur	= '000'.$nomorpajak;
				break;
				case "6":
					$nfaktur	= '00'.$nomorpajak;
				break;
				case "7":
					$nfaktur	= '0'.$nomorpajak;
				break;
				default:
					$nfaktur	= $nomorpajak;
			}
			if ($fpengganti == 'y')
				$kodeawal = '011';
			else
				$kodeawal = '010';
			$data['nomorpajak']	= $kodeawal."."."000"."-".$tahunpajak.".".$nfaktur;			
		}
		$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo2($nofaktur,$d_do_first,$d_do_last);
		
		$this->load->view('prntfpajak_test/vtestform',$data);
	}

	function cpopuptest() {
		
		$iduserid	= $this->session->userdata('user_idx');
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.194';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;		

		$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
		$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
		$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
		$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
		$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
		$data['list_pjkpenjperdo_no_do']	= $this->lang->line('list_pjkpenjperdo_no_do');
		$data['list_pjkpenjperdo_nm_brg']	= $this->lang->line('list_pjkpenjperdo_nm_brg');
		$data['list_pjkpenjperdo_qty']	= $this->lang->line('list_pjkpenjperdo_qty');
		$data['list_pjkpenjperdo_hjp']	= $this->lang->line('list_pjkpenjperdo_hjp');
		$data['list_pjkpenjperdo_amount']	= $this->lang->line('list_pjkpenjperdo_amount');
		$data['list_pjkpenjperdo_total_pengiriman']	= $this->lang->line('list_pjkpenjperdo_total_pengiriman');
		$data['list_pjkpenjperdo_total_penjualan']	= $this->lang->line('list_pjkpenjperdo_total_penjualan');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();

		$data['TtdPajak01']	= $this->lang->line('TtdPajak01');
		$data['TtdPajak02']	= $this->lang->line('TtdPajak02');
		$data['falamat']	= $this->lang->line('AddInisial');
				
		$nofaktur	= $this->uri->segment(4); 

		$d_do_first	= $this->uri->segment(5); // YYYY-MM-DD
		$d_do_last	= $this->uri->segment(6);

		$bglobal	= array(
			'01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
			'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
		
		$tanggal= date("d");
		$bulan	= date("m");
		$tahun	= date("Y");
		
		$tgl	= substr($tanggal,0,1)=='0'?substr($tanggal,1,1):$tanggal;
		$nw_tgl	= $tgl;
		$bln	= $bglobal[$bulan];
		
		$data['ftgl']	= $nw_tgl." ".$bln." ".$tahun;
		
		$this->load->model('prntfpajak_test/mclass');
		
		$qry_source_remote	= $this->mclass->remote($iduserid);

		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;
		
		$qry_infoheader		= $this->mclass->clistfpenjperdo($nofaktur,$d_do_first,$d_do_last);
			
		if($qry_infoheader->num_rows() > 0) {
			
			$row_infoheader	= $qry_infoheader->row();
			$nofaktur		= $row_infoheader->ifakturcode;
			$tglfaktur		= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$tglpajak		= $row_infoheader->dpajak;
			
			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$exp_tpajak		= explode("-",$tglpajak,strlen($tglpajak));	
			
			$tahunpajak	= !empty($exp_tpajak[0])?substr($exp_tpajak[0],2,2):date("y");
			
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];
			
			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']		= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;
		}
		
		$qry_jml	= $this->mclass->clistfpenjperdo_jml($nofaktur,$d_do_first,$d_do_last);
		$qry_totalnyaini	= $this->mclass->clistfpenjperdo_totalnyaini($nofaktur,$d_do_first,$d_do_last);
		
		if($qry_jml->num_rows() > 0 ) {
			$row_jml = $qry_jml->row_array();		
			$row_totalnyaini = $qry_totalnyaini->row_array();
			$total	 = $row_totalnyaini['totalnyaini'];
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			$total_pls_disc	= $total - $v_disc; // DPP
			$nilai_ppn		= (($total_pls_disc*10) / 100 ); // PPN
			$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			
			$data['jumlah']	= $total; 
			$data['dpp']	= $total_pls_disc; // DPP = Dasar Pengenaan Pajak
			$data['diskon']	= $v_disc;
			$data['nilai_ppn']		= $nilai_ppn;
			$data['nilai_faktur']	= round($nilai_faktur);
		}
		
		$qrypelanggan	= $this->mclass->pelanggan($nofaktur);
		if($qrypelanggan->num_rows() >0) {
			$row_pelanggan		= $qrypelanggan->row_array();
			
			$data['nmkostumer']	= $row_pelanggan['customername'];
			$data['almtkostumer']	= $row_pelanggan['customeraddress'];
			$data['npwp']		= $row_pelanggan['npwp'];
		}
		
		$ititas			= $this->mclass->ititas();
		if($ititas->num_rows() > 0) {

			$row_ititas	= $ititas->row();
			$data['nminitial']	= $row_ititas->e_initial_name;
			$data['almtperusahaan']	= $row_ititas->e_initial_address;
			$data['npwpperusahaan']	= $row_ititas->e_initial_npwp;
		}
		$nopajak		= $this->mclass->pajak($nofaktur);
		if($nopajak->num_rows() > 0) {
			$row_pajak	= $nopajak->row();			
			$nomorpajak	= $row_pajak->ifakturpajak;			
			
			switch(strlen($nomorpajak)) {
				case "1":
					$nfaktur	= '0000000'.$nomorpajak;
				break;
				case "2":
					$nfaktur	= '000000'.$nomorpajak;
				break;
				case "3":
					$nfaktur	= '00000'.$nomorpajak;
				break;
				case "4":
					$nfaktur	= '0000'.$nomorpajak;
				break;
				case "5":
					$nfaktur	= '000'.$nomorpajak;
				break;
				case "6":
					$nfaktur	= '00'.$nomorpajak;
				break;
				case "7":
					$nfaktur	= '0'.$nomorpajak;
				break;
				default:
					$nfaktur	= $nomorpajak;
			}
			$data['nomorpajak']	= "010"."."."000".".".$tahunpajak.".".$nfaktur;			
		}
		$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo2($nofaktur,$d_do_first,$d_do_last);

		$this->load->view('prntfpajak_test/vtestform',$data);
	}
	
	function listbarangjadi() {
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpajak_test/mclass');
		
		$fpengganti	= $this->uri->segment(4); 

		$query	= $this->mclass->lbarangjadi($fpengganti);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/prntfpajak_test/cform/listbarangjadinext/'.$fpengganti;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'], $fpengganti);			

		$this->load->view('prntfpajak_test/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpajak_test/mclass');
		$fpengganti	= $this->uri->segment(4); 
		$query	= $this->mclass->lbarangjadi($fpengganti);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntfpajak_test/cform/listbarangjadinext/'.$fpengganti;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'], $fpengganti);				
		
		$this->load->view('prntfpajak_test/vlistformbrgjadi',$data);
	}
	
	function flistbarangjadi() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('prntfpajak_test/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
		
		echo $item;
	}
}
?>
