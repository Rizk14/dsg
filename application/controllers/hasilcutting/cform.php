<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('hasilcutting/mmaster');
  }

   // start 13-12-2014. PEMBAHARUAN 15-10-2015
   
  function viewsohasilcutting(){
     $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
    
    $data['isi'] = 'hasilcutting/vformviewsohasilcutting';
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);  
	
	if ($bulan=='' && $tahun=='') {
		$bulan 	= $this->uri->segment(4);
		$tahun 	= $this->uri->segment(5);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";

    $jum_total = $this->mmaster->get_sohasilcuttingtanpalimit($bulan, $tahun);

			$config['base_url'] = base_url().'index.php/hasilcutting/cform/viewsohasilcutting/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mmaster->get_sohasilcutting($config['per_page'],$this->uri->segment(6), $bulan, $tahun);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;

	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  // 15-12-2014
  function editsohasilcutting(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cunit 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cunit'] = $cunit;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mmaster->get_sohasilcuttingbyid($id_so); 
	$data['jum_total'] = count($data['query']);
	$data['isi'] = 'hasilcutting/veditsohasilcutting';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  function updatesohasilcutting() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	
		$tgl_so = $this->input->post('tgl_so', TRUE);
		$pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  $tgl = date("Y-m-d H:i:s"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_hasil_cutting SET tgl_update = '$tgl', tgl_so='$tgl_so', 
						status_approve='t' where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mmaster->savesohasilcutting($id_so, $tgl_so,
			 $this->input->post('id_'.$i, TRUE), 
			 $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 );
		 }
			  
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "hasilcutting/cform/viewsohasilcutting/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
  // 17-12-2014
  //SET STOK AWAL PERTAMA KALI
  function addstokawal(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$th_now	= date("Y");

	$data['isi'] = 'hasilcutting/vform1stokawal';
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function submitstokawal(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
		$no 	= $this->input->post('no', TRUE);
		$jumlah_input=$no-1;
			
		for ($i=1;$i<=$jumlah_input;$i++)
		{
			$this->mmaster->savestokawal($this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
						$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
						$this->input->post('qty_warna_'.$i, TRUE) );
		}
		redirect('hasilcutting/cform/viewstokawal');
  }
  
  function viewstokawal(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '')
			$keywordcari = $this->uri->segment(4);
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$jum_total = $this->mmaster->getAllstokawaltanpalimit($keywordcari);
								$config['base_url'] = base_url().'index.php/hasilcutting/cform/viewstokawal/'.$keywordcari.'/';
								$config['total_rows'] = count($jum_total); 
								$config['per_page'] = '20';
								$config['first_link'] = 'Awal';
								$config['last_link'] = 'Akhir';
								$config['next_link'] = 'Selanjutnya';
								$config['prev_link'] = 'Sebelumnya';
								$config['cur_page'] = $this->uri->segment(5);
								$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllstokawal($config['per_page'],$this->uri->segment(5), $keywordcari);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['startnya'] = $config['cur_page'];
		
		$data['isi'] = 'hasilcutting/vformviewstokawal';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		
		$this->load->view('template',$data);
  }
  
  function show_popup_brgjadi_stokawal(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$posisi 	= $this->input->post('posisi', TRUE);  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '') {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
	// +++++++++++++++

		$qjum_total = $this->mmaster->get_brgjadi_stokawaltanpalimit($keywordcari);
		
				$config['base_url'] = base_url()."index.php/hasilcutting/cform/show_popup_brgjadi_stokawal/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi_stokawal($config['per_page'],$config['cur_page'], $keywordcari);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
		
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('hasilcutting/vpopupbrgjadistokawal',$data);
  }
  
  function editstokawal(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	$is_simpan 	= $this->input->post('is_simpan', TRUE);
	
	if ($is_simpan == '') {
		$id_stok 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$carinya 	= $this->uri->segment(6);
		
		$query3	= $this->db->query(" SELECT a.id_gudang, a.kode_brg_jadi, a.stok, b.e_product_motifname 
								FROM tm_stok_hasil_jahit a, tr_product_motif b 
								WHERE a.kode_brg_jadi = b.i_product_motif AND a.id = '$id_stok' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_gudang = $hasilrow->id_gudang;
			$kode_brg_jadi = $hasilrow->kode_brg_jadi;
			$nama_brg_jadi = $hasilrow->e_product_motifname;
			$stok = $hasilrow->stok;
		}
		
		$data['msg'] = '';
		$data['id_stok'] = $id_stok;
		$data['cur_page'] = $cur_page;
		if ($carinya == '')
			$data['carinya'] = "all";
		else
			$data['carinya'] = $carinya;
		$data['brg_jadi'] = $kode_brg_jadi." - ".$nama_brg_jadi;
		$data['stok'] = $stok;

		$data['isi'] = 'hasilcutting/veditstokawal';
		$this->load->view('template',$data);
	}
	else { // simpan
		$id_stok 	= $this->input->post('id_stok', TRUE);
		$cur_page 	= $this->input->post('cur_page', TRUE);  
		$carinya 	= $this->input->post('carinya', TRUE);  
		$stok = $this->input->post('stok', TRUE);
		$tgl = date("Y-m-d");
		
		// update stoknya
		$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$stok', tgl_update_stok = '$tgl'
							WHERE id= '$id_stok' ");
		
		if ($carinya == '') $carinya = "all";
		$url_redirectnya = "hasilcutting/cform/viewstokawal/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
	}

  }
  
  function additemwarna(){

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ORDER BY b.nama");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna_brg_jadi'=> $rowxx->id,
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('hasilcutting/vlistwarna', $data); 
		return true;
  }
  
}
