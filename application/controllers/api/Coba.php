<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coba extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('directory');
        $this->load->helper('file');
        $this->load->library('excel');
        require_once "php/fungsi.php";
    }

    public function va()
    {
        $query = $this->db->query("select a.i_area,a.i_customer, b.i_customer_groupbayar from tr_customer a, tr_customer_groupbayar b
        where
        a.i_customer = b.i_customer
        and a.i_customer not in(select i_customer from tr_customer_va)
	and a.i_customer in('01H53',
'01H54',
'01H55',
'01H56',
'01H75',
'01H76',
'01H77',
'01H93',
'01I01',
'01I02',
'01I05',
'01I06',
'01I07',
'01I08',
'01I34',
'01I35',
'01I62',
'01I63',
'01I67',
'01I68',
'01H52')");

        $va_perusahaan = "50020";
        foreach ($query->result() as $row) {
            $i_area = $row->i_area;
            $qnoterakhir = $this->db->query("select n_modul_no as max from tm_dgu_no
            where i_modul='VA'
            and i_area='$i_area'");
            if ($qnoterakhir->num_rows() > 0) {
                $qnoterakhir = $qnoterakhir->row();
                $terakhir = $qnoterakhir->max;
                $no_va = $terakhir + 1;
                $this->db->query("update tm_dgu_no
                set n_modul_no=$no_va
                where i_modul='VA'
                and i_area='$i_area'");

            } else {
                $thbl = date('Ym');
                $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                values ('VA','$i_area','$thbl',1)");
                $no_va = 1;
            }
            $nosekarang = $no_va;
            settype($nosekarang, "string");
            $a = strlen($nosekarang);
            while ($a < 5) {
                $nosekarang = "0" . $nosekarang;
                $a = strlen($nosekarang);
            }
            $i_customer_va = $va_perusahaan . $row->i_area . $nosekarang;
            // $i_customer_va = '500200100410';
            $data = array(
                'i_customer' => $row->i_customer,
                'i_customer_groupbayar' => $row->i_customer_groupbayar,
                'i_customer_va' => $i_customer_va,
            );

            $this->db->insert('tr_customer_va', $data);

        }
    }

    public function hutang1()
    {
        $qperiode = $this->db->query("select i_periode from tm_periode")->row();
        $periode = $qperiode->i_periode;
        $query = $this->db->query("
        select x.* from(
            select a.i_op, a.i_do, a.i_supplier, a.i_product, round(a.v_pabrik, 2) as v_pabrik from tm_dtap_item a, tm_dtap b where
            a.i_area = b.i_area
            and a.i_dtap = b.i_dtap
            and a.i_supplier = b.i_supplier
            and a.n_dtap_year = b.n_dtap_year
            and to_char(b.d_dtap,'yyyymm')='$periode'
            and b.f_dtap_cancel = 'f'
            order by a.i_op
            ) as x
            where
            x.i_do||x.i_supplier||x.i_product||x.v_pabrik not in(
            select x.i_do||x.i_supplier||x.i_product||x.v_product_mill from(
            select a.i_do, a.i_supplier, a.i_product, round(a.v_product_mill, 2) as v_product_mill from tm_do_item a, tm_do b
            where to_char(a.d_do,'yyyymm')='$periode'
            and a.i_op = b.i_op
            and b.f_do_cancel = 'f'
            ) as x

            )
        ");

        foreach ($query->result() as $row) {
            $kode_barang = $row->i_product;
            $i_op = $row->i_op;
            $i_do = $row->i_do;
            $i_supplier = $row->i_supplier;
            $harga = $row->v_pabrik;
            $this->db->query("update tm_do_item set v_product_mill = $harga where i_op = '$i_op' and i_do = '$i_do' and i_product = '$kode_barang'");

        }
        // echo "berhasil";
    }

    public function hutang2()
    {
        $qperiode = $this->db->query("select i_periode from tm_periode")->row();
        $periode = $qperiode->i_periode;
        $query = $this->db->query("
        select x.* from(
            select a.i_op, a.i_do, a.i_supplier, a.i_product, round(a.v_pabrik, 2) as v_pabrik from tm_dtap_item a, tm_dtap b where
            a.i_area = b.i_area
            and a.i_dtap = b.i_dtap
            and a.i_supplier = b.i_supplier
            and a.n_dtap_year = b.n_dtap_year
            and to_char(b.d_dtap,'yyyymm')='$periode'
            and b.f_dtap_cancel = 'f'
            order by a.i_op
            ) as x
            where
            x.i_op||x.i_supplier||x.i_product||x.v_pabrik not in(
            select x.i_op||x.i_supplier||x.i_product||x.v_product_mill from(
            select a.i_op, a.i_product, round(a.v_product_mill, 2) as v_product_mill, b.i_supplier from tm_op_item a, tm_op b
            where to_char(b.d_op,'yyyymm')='$periode'
            and a.i_op = b.i_op
            and b.f_op_cancel = 'f'
            ) as x

            )
        ");

        foreach ($query->result() as $row) {
            $kode_barang = $row->i_product;
            $i_op = $row->i_op;
            $harga = $row->v_pabrik;
            $this->db->query("update tm_op_item set v_product_mill = $harga where i_op = '$i_op' and i_product = '$kode_barang'");

        }
        // echo "berhasil";
    }

    public function update_sisa()
    {
        $this->db->query("update tm_nota set v_sisa = v_nota_netto where
        v_sisa > v_nota_netto
        and f_nota_cancel = 'f'
        and i_nota isnull");
        echo "berhasil";
    }

    public function trend()
    {
        $ohari_ini = "01-01-" . date("Y");
        $hari_ini = date("d-m-Y");

        $olhari_ini = "01-01-" . date('Y', strtotime('-1 year', strtotime($ohari_ini)));
        $lhari_ini = date('d-m-Y', strtotime('-1 year', strtotime($hari_ini)));

        $conn = pg_connect("host=192.168.0.93 port=5432 dbname=report user=dedy password=g#>m[J2P^^") or die('Gagal');

        pg_query("ALTER SEQUENCE tt_trendtokoperitem_id_seq restart");
        pg_query("DELETE from tt_trendtokoperitem");

        /**DGU**/
        /*pg_query("  INSERT INTO dgu_report (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name,
        e_customer_classname, e_product_categoryname, i_product, e_product_name, vnota, qnota, oa, prevvnota,
        prevqnota, prevoa, e_company)
        SELECT *,'DGU' FROM baca_trenditem('$ohari_ini','$hari_ini','$olhari_ini','$lhari_ini')");*/

        pg_query("  insert into tt_trendtokoperitem
                    (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name, e_customer_classname, e_product_categoryname,
                    i_product, e_product_name, vnota, qnota, oa, prevvnota, prevqnota, prevoa, e_company)
                    select * from dblink('host=192.168.0.93 user=dedy password=g#>m[J2P^^ dbname=dialogue_new','
                        SELECT *,''DGU'' FROM baca_trenditem(''$ohari_ini'',''$hari_ini'',''$olhari_ini'',''$lhari_ini'')')as b
                        (i_area character(2), e_area_name character varying(50), i_nota character(15), d_nota date, i_customer character(5), e_customer_name character varying(50), e_customer_classname character varying(25), e_product_categoryname character varying(30), i_product character(7), e_product_name character varying(100), vnota numeric(12,0), qnota numeric(10,0), oa numeric(10,0), prevvnota numeric(12,0), prevqnota numeric(10,0), prevoa numeric(10,0), e_company character varying(50));");

        /**BLN**/
        pg_query(" insert into tt_trendtokoperitem
                            (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name, i_customer_dgu, e_customer_classname, e_product_categoryname,
                            i_product, e_product_name, vnota, qnota, oa, prevvnota, prevqnota, prevoa, e_company)
                            select * from dblink('host=192.168.0.93 user=dedy password=g#>m[J2P^^ dbname=bln','
                            SELECT *,''BLN'' FROM baca_trenditem(''$ohari_ini'',''$hari_ini'',''$olhari_ini'',''$lhari_ini'')')as b
                            (i_area character(2), e_area_name character varying(50), i_nota character(15), d_nota date, i_customer character(5), e_customer_name character varying(50),  i_customer_dgu character(5), e_customer_classname character varying(25), e_product_categoryname character varying(30), i_product character(7), e_product_name character varying(100), vnota numeric(12,0), qnota numeric(10,0), oa numeric(10,0), prevvnota numeric(12,0), prevqnota numeric(10,0), prevoa numeric(10,0), e_company character varying(50));");

        /**DPP**/
        pg_query(" insert into tt_trendtokoperitem
                            (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name, i_customer_dgu, e_customer_classname, e_product_categoryname,
                            i_product, e_product_name, vnota, qnota, oa, prevvnota, prevqnota, prevoa, e_company)
                            select * from dblink('host=192.168.0.93 user=dedy password=g#>m[J2P^^ dbname=dpp','
                            SELECT *,''DPP'' FROM baca_trenditem(''$ohari_ini'',''$hari_ini'',''$olhari_ini'',''$lhari_ini'')')as b
                            (i_area character(2), e_area_name character varying(50), i_nota character(15), d_nota date, i_customer character(5), e_customer_name character varying(50),  i_customer_dgu character(5), e_customer_classname character varying(25), e_product_categoryname character varying(30), i_product character(7), e_product_name character varying(100), vnota numeric(12,0), qnota numeric(10,0), oa numeric(10,0), prevvnota numeric(12,0), prevqnota numeric(10,0), prevoa numeric(10,0), e_company character varying(50));");

        /**PRS**/
        pg_query(" insert into tt_trendtokoperitem
                            (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name, i_customer_dgu, e_customer_classname, e_product_categoryname,
                            i_product, e_product_name, vnota, qnota, oa, prevvnota, prevqnota, prevoa, e_company)
                            select * from dblink('host=192.168.0.93 user=dedy password=g#>m[J2P^^ dbname=prs','
                            SELECT *,''PRS'' FROM baca_trenditem(''$ohari_ini'',''$hari_ini'',''$olhari_ini'',''$lhari_ini'')')as b
                            (i_area character(2), e_area_name character varying(50), i_nota character(15), d_nota date, i_customer character(5), e_customer_name character varying(50),  i_customer_dgu character(5), e_customer_classname character varying(25), e_product_categoryname character varying(30), i_product character(7), e_product_name character varying(100), vnota numeric(12,0), qnota numeric(10,0), oa numeric(10,0), prevvnota numeric(12,0), prevqnota numeric(10,0), prevoa numeric(10,0), e_company character varying(50));");

        pg_close($conn);
    }

    public function opvsdoimma()
    {
        $ohari_ini = date("Y") . "-01-01";
        $hari_ini = date("Y-m-d");

        $conn = pg_connect("host=192.168.0.93 port=5432 dbname=sysdbimma user=dedy password=g#>m[J2P^^") or die('Gagal');

        pg_query("ALTER SEQUENCE tt_op_vs_do_id_seq restart");
        pg_query("DELETE from tt_op_vs_do");

        pg_query("  insert into tt_op_vs_do
                    (codecus, namecab, alamat, kota, area, sales, tglop, noop, opreferensi, tgldo, nodo, i_faktur_code, imotif, emotif, harga, qty, v_discount, v_discount1, v_discount2, qtydo, qtysumdo, i_group_code, tglspb)
                    select * from f_exp_op_vs_do ('$ohari_ini','$hari_ini');");

        pg_close($conn);
    }

    public function update_stock()
    {
        // $thbln = $this->db->query("select i_periode from tm_periode")->row()->i_periode;
        $thbln = date('Ym');
        // $thblnkemarin = '202003';
        $area = $this->db->query("SELECT  i_store, i_store_location
        from f_mutasi_stock_daerah_all_saldoakhir('$thbln')
        group by i_store, i_store_location
        order by i_store");

        foreach ($area->result() as $area) {
            $i_store = $area->i_store;
            $istorelocation = $area->i_store_location;
            // $cek_saldo_awal = $this->db->query("select * from tm_mutasi_saldoawal where e_mutasi_periode = '$thbln' and i_store = '$i_store'");

            // if($cek_saldo_awal->num_rows() > 0){
            $this->db->query("UPDATE tm_ic SET n_quantity_stock = 0 WHERE i_store='$i_store' and i_store_location='$istorelocation'");
            $que2 = $this->db->query("select i_product, i_store, i_product_grade, i_product_motif, i_store_location, (n_saldo_akhir)-(n_saldo_git+n_git_penjualan) as saldo_akhir from(
                    select e_product_groupname, i_product, i_product_grade,i_product_motif,sum(n_saldo_awal) as n_saldo_awal,
                    sum(n_mutasi_pembelian) as n_mutasi_pembelian,
                    sum(n_mutasi_returoutlet) as n_mutasi_returoutlet, sum(n_mutasi_bbm) as n_mutasi_bbm,
                    sum(n_mutasi_penjualan) as n_mutasi_penjualan,
                    sum(n_mutasi_bbk) as n_mutasi_bbk, sum(n_saldo_akhir) as n_saldo_akhir, sum(n_mutasi_ketoko) as n_mutasi_ketoko,
                    sum(n_mutasi_daritoko) as n_mutasi_daritoko, sum(n_mutasi_git) as n_saldo_git, e_mutasi_periode, i_store,
                    sum(n_git_penjualan) as n_git_penjualan, sum(n_git_penjualanasal) as n_git_penjualanasal,
                    sum(n_mutasi_gitasal) as n_mutasi_gitasal,
                    sum(n_saldo_stockopname) as n_saldo_stockopname, e_product_name, i_store_location
                    from f_mutasi_stock_daerah_saldoakhir('$thbln','$i_store','$istorelocation')
                    group by i_product, i_product_grade, e_product_groupname, e_mutasi_periode, i_store, e_product_name,
                             i_store_location, i_product_motif
                    order by e_product_groupname, e_product_name, i_product) as z");

            if ($que2->num_rows() > 0) {
                foreach ($que2->result() as $xx2) {

                    $this->db->query("UPDATE tm_ic
                            SET n_quantity_stock = $xx2->saldo_akhir
                            WHERE
                             i_product='$xx2->i_product' and i_store='$xx2->i_store' and i_product_grade='$xx2->i_product_grade' and i_store_location='$xx2->i_store_location' and i_product_motif='$xx2->i_product_motif'");
                }
            }
            // }else{
            //     $this->db->query("
            //     delete from tm_mutasi_saldoawal  where e_mutasi_periode='$thbln' and i_store = '$i_store';
            //     insert into tm_mutasi_saldoawal
            //     SELECT   i_store, i_store_location, i_store_locationbin, '$thbln', i_product, i_product_grade, i_product_motif, n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal
            //     from f_mutasi_stock_daerah_all_saldoakhir('$thblnkemarin') where i_store = '$i_store'");
            // }
        }
        echo 'sudah';
        die;
    }

    public function update_jatuh_tempo()
    {

        $query = $this->db->query("select a.i_nota, a.i_spb, a.i_sj, a.i_area,a.d_nota, b.n_spb_toplength, a.d_sj_receive, a.d_jatuh_tempo, a.d_dkb, a.i_dkb from tm_nota a, tm_spb b where
        a.i_nota = b.i_nota
        and a.i_area = b.i_area
        and a.i_spb = b.i_spb
        and a.i_nota in('FP-1912-0000615',
        'FP-1912-0000616',
        'FP-1912-0000617',
        'FP-1912-0000618',
        'FP-1912-0000619',
        'FP-1912-0104130',
        'FP-1912-0104131',
        'FP-1912-0104132',
        'FP-1912-0104133',
        'FP-1912-0104134',
        'FP-1912-0104135',
        'FP-1912-0104136',
        'FP-1912-0104137',
        'FP-1912-0104138',
        'FP-1912-0104139',
        'FP-1912-0104140',
        'FP-1912-0104141',
        'FP-1912-0104142',
        'FP-1912-0104143',
        'FP-1912-0104144',
        'FP-1912-0104145',
        'FP-1912-0104146',
        'FP-1912-0104147',
        'FP-1912-0104148',
        'FP-1912-0104149',
        'FP-1912-0104150',
        'FP-1912-0104151',
        'FP-1912-0104152',
        'FP-1912-0104153',
        'FP-1912-0104154',
        'FP-1912-0104155',
        'FP-1912-0104156',
        'FP-1912-0104157',
        'FP-1912-0104158',
        'FP-1912-0104159',
        'FP-1912-0104160',
        'FP-1912-0104161',
        'FP-1912-0104162',
        'FP-1912-0104163',
        'FP-1912-0104164',
        'FP-1912-0104165',
        'FP-1912-0104166',
        'FP-1912-0104167',
        'FP-1912-0104168',
        'FP-1912-0104169',
        'FP-1912-0207535',
        'FP-1912-0207536',
        'FP-1912-0207537',
        'FP-1912-0207538',
        'FP-1912-0207539',
        'FP-1912-0207540',
        'FP-1912-0207541',
        'FP-1912-0207542',
        'FP-1912-0207543',
        'FP-1912-0207544',
        'FP-1912-0207545',
        'FP-1912-0207546',
        'FP-1912-0207547',
        'FP-1912-0207548',
        'FP-1912-0207549',
        'FP-1912-0207550',
        'FP-1912-0207551',
        'FP-1912-0207552',
        'FP-1912-0207553',
        'FP-1912-0207554',
        'FP-1912-0207555',
        'FP-1912-0207556',
        'FP-1912-0207557',
        'FP-1912-0207558',
        'FP-1912-0207559',
        'FP-1912-0207560',
        'FP-1912-0207561',
        'FP-1912-0207562',
        'FP-1912-0207563',
        'FP-1912-0207564',
        'FP-1912-0207565',
        'FP-1912-0207566',
        'FP-1912-0207567',
        'FP-1912-0207568',
        'FP-1912-0207569',
        'FP-1912-0207570',
        'FP-1912-0207571',
        'FP-1912-0207572',
        'FP-1912-0207573',
        'FP-1912-0207574',
        'FP-1912-0207575',
        'FP-1912-0207576',
        'FP-1912-0207577',
        'FP-1912-0207578',
        'FP-1912-0207579',
        'FP-1912-0207580',
        'FP-1912-0207581',
        'FP-1912-0207582',
        'FP-1912-0207583',
        'FP-1912-0207584',
        'FP-1912-0207585',
        'FP-1912-0207586',
        'FP-1912-0207587',
        'FP-1912-0207588',
        'FP-1912-0207589',
        'FP-1912-0207590',
        'FP-1912-0207591',
        'FP-1912-0207592',
        'FP-1912-0207593',
        'FP-1912-0207594',
        'FP-1912-0207595',
        'FP-1912-0207596',
        'FP-1912-0207597',
        'FP-1912-0207598',
        'FP-1912-0207599',
        'FP-1912-0207600',
        'FP-1912-0207601',
        'FP-1912-0207602',
        'FP-1912-0207603',
        'FP-1912-0207604',
        'FP-1912-0207605',
        'FP-1912-0207606',
        'FP-1912-0207607',
        'FP-1912-0207608',
        'FP-1912-0207609',
        'FP-1912-0207610',
        'FP-1912-0207611',
        'FP-1912-0207612',
        'FP-1912-0207613',
        'FP-1912-0207614',
        'FP-1912-0207615',
        'FP-1912-0207616',
        'FP-1912-0207617',
        'FP-1912-0207618',
        'FP-1912-0207619',
        'FP-1912-0207620',
        'FP-1912-0207621',
        'FP-1912-0207622',
        'FP-1912-0207623',
        'FP-1912-0207624',
        'FP-1912-0207625',
        'FP-1912-0207626',
        'FP-1912-0207627',
        'FP-1912-0207628',
        'FP-1912-0207629',
        'FP-1912-0207630',
        'FP-1912-0207631',
        'FP-1912-0207632',
        'FP-1912-0207633',
        'FP-1912-0207634',
        'FP-1912-0207635',
        'FP-1912-0207636',
        'FP-1912-0207637',
        'FP-1912-0207638',
        'FP-1912-0207639',
        'FP-1912-0207640',
        'FP-1912-0207641',
        'FP-1912-0207642',
        'FP-1912-0207643',
        'FP-1912-0207644',
        'FP-1912-0207645',
        'FP-1912-0207646',
        'FP-1912-0207647',
        'FP-1912-0207648',
        'FP-1912-0207649',
        'FP-1912-0207650',
        'FP-1912-0207651',
        'FP-1912-0207652',
        'FP-1912-0207653',
        'FP-1912-0305478',
        'FP-1912-0305479',
        'FP-1912-0305480',
        'FP-1912-0305481',
        'FP-1912-0305482',
        'FP-1912-0305483',
        'FP-1912-0305484',
        'FP-1912-0305485',
        'FP-1912-0305486',
        'FP-1912-0305487',
        'FP-1912-0305488',
        'FP-1912-0305489',
        'FP-1912-0305490',
        'FP-1912-0305491',
        'FP-1912-0305492',
        'FP-1912-0305493',
        'FP-1912-0305494',
        'FP-1912-0305495',
        'FP-1912-0305496',
        'FP-1912-0305497',
        'FP-1912-0305498',
        'FP-1912-0305499',
        'FP-1912-0305500',
        'FP-1912-0305501',
        'FP-1912-0305502',
        'FP-1912-0305503',
        'FP-1912-0305504',
        'FP-1912-0305505',
        'FP-1912-0305506',
        'FP-1912-0305507',
        'FP-1912-0305508',
        'FP-1912-0305509',
        'FP-1912-0305510',
        'FP-1912-0305511',
        'FP-1912-0305512',
        'FP-1912-0305513',
        'FP-1912-0305514',
        'FP-1912-0305515',
        'FP-1912-0305516',
        'FP-1912-0305517',
        'FP-1912-0305518',
        'FP-1912-0305519',
        'FP-1912-0305520',
        'FP-1912-0305521',
        'FP-1912-0305522',
        'FP-1912-0305523',
        'FP-1912-0305524',
        'FP-1912-0305525',
        'FP-1912-0305526',
        'FP-1912-0305527',
        'FP-1912-0305528',
        'FP-1912-0305529',
        'FP-1912-0305530',
        'FP-1912-0305531',
        'FP-1912-0305532',
        'FP-1912-0305533',
        'FP-1912-0305534',
        'FP-1912-0305535',
        'FP-1912-0305536',
        'FP-1912-0305537',
        'FP-1912-0305538',
        'FP-1912-0305539',
        'FP-1912-0305540',
        'FP-1912-0305541',
        'FP-1912-0305542',
        'FP-1912-0405887',
        'FP-1912-0405888',
        'FP-1912-0405889',
        'FP-1912-0405890',
        'FP-1912-0405891',
        'FP-1912-0405892',
        'FP-1912-0405893',
        'FP-1912-0405894',
        'FP-1912-0405895',
        'FP-1912-0405896',
        'FP-1912-0405897',
        'FP-1912-0405898',
        'FP-1912-0405899',
        'FP-1912-0405900',
        'FP-1912-0405901',
        'FP-1912-0405902',
        'FP-1912-0405903',
        'FP-1912-0405904',
        'FP-1912-0405905',
        'FP-1912-0405906',
        'FP-1912-0405907',
        'FP-1912-0405908',
        'FP-1912-0405909',
        'FP-1912-0405910',
        'FP-1912-0405911',
        'FP-1912-0405912',
        'FP-1912-0405913',
        'FP-1912-0405914',
        'FP-1912-0405915',
        'FP-1912-0405916',
        'FP-1912-0405917',
        'FP-1912-0405918',
        'FP-1912-0405919',
        'FP-1912-0405920',
        'FP-1912-0405921',
        'FP-1912-0405922',
        'FP-1912-0405923',
        'FP-1912-0405924',
        'FP-1912-0405925',
        'FP-1912-0405926',
        'FP-1912-0405927',
        'FP-1912-0405928',
        'FP-1912-0405929',
        'FP-1912-0405930',
        'FP-1912-0405931',
        'FP-1912-0405932',
        'FP-1912-0405933',
        'FP-1912-0405934',
        'FP-1912-0405935',
        'FP-1912-0405936',
        'FP-1912-0405937',
        'FP-1912-0405938',
        'FP-1912-0405939',
        'FP-1912-0405940',
        'FP-1912-0405941',
        'FP-1912-0405942',
        'FP-1912-0405943',
        'FP-1912-0405944',
        'FP-1912-0405945',
        'FP-1912-0405946',
        'FP-1912-0405947',
        'FP-1912-0405948',
        'FP-1912-0405949',
        'FP-1912-0405950',
        'FP-1912-0405951',
        'FP-1912-0405952',
        'FP-1912-0405953',
        'FP-1912-0405954',
        'FP-1912-0405955',
        'FP-1912-0405956',
        'FP-1912-0405957',
        'FP-1912-0405958',
        'FP-1912-0405959',
        'FP-1912-0405960',
        'FP-1912-0405961',
        'FP-1912-0405962',
        'FP-1912-0405963',
        'FP-1912-0405964',
        'FP-1912-0502342',
        'FP-1912-0502343',
        'FP-1912-0502344',
        'FP-1912-0502345',
        'FP-1912-0502346',
        'FP-1912-0502347',
        'FP-1912-0502348',
        'FP-1912-0502349',
        'FP-1912-0502350',
        'FP-1912-0502351',
        'FP-1912-0502352',
        'FP-1912-0502353',
        'FP-1912-0502354',
        'FP-1912-0502355',
        'FP-1912-0502356',
        'FP-1912-0502357',
        'FP-1912-0502358',
        'FP-1912-0502359',
        'FP-1912-0502360',
        'FP-1912-0502361',
        'FP-1912-0502362',
        'FP-1912-0502363',
        'FP-1912-0502364',
        'FP-1912-0502365',
        'FP-1912-0502366',
        'FP-1912-0502367',
        'FP-1912-0502368',
        'FP-1912-0502369',
        'FP-1912-0502370',
        'FP-1912-0502371',
        'FP-1912-0502372',
        'FP-1912-0502373',
        'FP-1912-0502374',
        'FP-1912-0600768',
        'FP-1912-0600769',
        'FP-1912-0700641',
        'FP-1912-0700642',
        'FP-1912-0700643',
        'FP-1912-0700644',
        'FP-1912-0700645',
        'FP-1912-0700646',
        'FP-1912-0700647',
        'FP-1912-0700648',
        'FP-1912-0700649',
        'FP-1912-0700650',
        'FP-1912-0700651',
        'FP-1912-0700652',
        'FP-1912-0700653',
        'FP-1912-0901163',
        'FP-1912-0901164',
        'FP-1912-0901165',
        'FP-1912-0901166',
        'FP-1912-0901167',
        'FP-1912-0901168',
        'FP-1912-0901169',
        'FP-1912-0901170',
        'FP-1912-0901171',
        'FP-1912-0901172',
        'FP-1912-0901173',
        'FP-1912-0901174',
        'FP-1912-0901175',
        'FP-1912-0901176',
        'FP-1912-0901177',
        'FP-1912-0901178',
        'FP-1912-0901179',
        'FP-1912-1001436',
        'FP-1912-1001437',
        'FP-1912-1001438',
        'FP-1912-1001439',
        'FP-1912-1001440',
        'FP-1912-1001441',
        'FP-1912-1001442',
        'FP-1912-1001443',
        'FP-1912-1001444',
        'FP-1912-1001445',
        'FP-1912-1001446',
        'FP-1912-1001447',
        'FP-1912-1001448',
        'FP-1912-1001449',
        'FP-1912-1001450',
        'FP-1912-1001451',
        'FP-1912-1001452',
        'FP-1912-1001453',
        'FP-1912-1001454',
        'FP-1912-1001455',
        'FP-1912-1001456',
        'FP-1912-1101570',
        'FP-1912-1101571',
        'FP-1912-1101572',
        'FP-1912-1101573',
        'FP-1912-1200542',
        'FP-1912-1200543',
        'FP-1912-1200544',
        'FP-1912-1200545',
        'FP-1912-1200546',
        'FP-1912-1200547',
        'FP-1912-1200548',
        'FP-1912-1200549',
        'FP-1912-1200550',
        'FP-1912-1200551',
        'FP-1912-1500363',
        'FP-1912-1500364',
        'FP-1912-1500365',
        'FP-1912-1500366',
        'FP-1912-1500367',
        'FP-1912-1500368',
        'FP-1912-1500369',
        'FP-1912-1500370',
        'FP-1912-1500371',
        'FP-1912-1500372',
        'FP-1912-1500373',
        'FP-1912-1500374',
        'FP-1912-1500375',
        'FP-1912-1500376',
        'FP-1912-1500377',
        'FP-1912-1500378',
        'FP-1912-1500379',
        'FP-1912-1500380',
        'FP-1912-1700991',
        'FP-1912-1700992',
        'FP-1912-1700993',
        'FP-1912-1700994',
        'FP-1912-1700995',
        'FP-1912-1700996',
        'FP-1912-1700997',
        'FP-1912-1700998',
        'FP-1912-1700999',
        'FP-1912-1701000',
        'FP-1912-1701001',
        'FP-1912-1701002',
        'FP-1912-1701003',
        'FP-1912-1800121',
        'FP-1912-1800122',
        'FP-1912-2000161',
        'FP-1912-2000162',
        'FP-1912-2000163',
        'FP-1912-2000164',
        'FP-1912-2200339',
        'FP-1912-2200340',
        'FP-1912-2300797',
        'FP-1912-2300798',
        'FP-1912-2300799',
        'FP-1912-2300800',
        'FP-1912-2300801',
        'FP-1912-2300802',
        'FP-1912-2401091',
        'FP-1912-2401092',
        'FP-1912-2401093',
        'FP-1912-2401094',
        'FP-1912-2401095',
        'FP-1912-3102790',
        'FP-1912-3102791',
        'FP-1912-3102792',
        'FP-1912-3102793',
        'FP-1912-3102794',
        'FP-1912-3102795',
        'FP-1912-3102796',
        'FP-1912-3102797',
        'FP-1912-3102798',
        'FP-1912-3102799',
        'FP-1912-3102800',
        'FP-1912-3102801',
        'FP-1912-3102802',
        'FP-1912-3102803',
        'FP-1912-3102804',
        'FP-1912-3102805',
        'FP-1912-3102806',
        'FP-1912-3102807',
        'FP-1912-3102808',
        'FP-1912-3102809',
        'FP-1912-3102810',
        'FP-1912-3102811',
        'FP-1912-3102812',
        'FP-1912-3102813',
        'FP-1912-3102814',
        'FP-1912-3102815',
        'FP-1912-3102816',
        'FP-1912-3102817',
        'FP-1912-3102818',
        'FP-1912-3102819',
        'FP-1912-PB05824',
        'FP-1912-PB05825',
        'FP-1912-PB05826',
        'FP-1912-PB05827',
        'FP-1912-PB05828',
        'FP-1912-PB05829',
        'FP-1912-PB05781',
        'FP-1912-PB05782',
        'FP-1912-PB05783',
        'FP-1912-PB05784',
        'FP-1912-PB05785',
        'FP-1912-PB05786',
        'FP-1912-PB05787',
        'FP-1912-PB05788',
        'FP-1912-PB05789',
        'FP-1912-PB05790',
        'FP-1912-PB05791',
        'FP-1912-PB05792',
        'FP-1912-PB05793',
        'FP-1912-PB05794',
        'FP-1912-PB05795',
        'FP-1912-PB05796',
        'FP-1912-PB05797',
        'FP-1912-PB05798',
        'FP-1912-PB05799',
        'FP-1912-PB05800',
        'FP-1912-PB05801',
        'FP-1912-PB05802',
        'FP-1912-PB05803',
        'FP-1912-PB05804',
        'FP-1912-PB05805',
        'FP-1912-PB05806',
        'FP-1912-PB05807',
        'FP-1912-PB05808',
        'FP-1912-PB05809',
        'FP-1912-PB05810',
        'FP-1912-PB05811',
        'FP-1912-PB05812',
        'FP-1912-PB05813',
        'FP-1912-PB05814',
        'FP-1912-PB05815',
        'FP-1912-PB05816',
        'FP-1912-PB05817',
        'FP-1912-PB05818',
        'FP-1912-PB05819',
        'FP-1912-PB05820',
        'FP-1912-PB05821',
        'FP-1912-PB05822',
        'FP-1912-PB05823')");
        $this->load->library('fungsi');

        foreach ($query->result() as $row) {
            $i_nota = $row->i_nota;
            $i_spb = $row->i_spb;
            $i_sj = $row->i_sj;
            $i_area = $row->i_area;
            $d_dkb = $row->d_dkb;
            $i_dkb = $row->i_dkb;
            $d_nota = '2019-12-27';
            $n_spb_toplength = $row->n_spb_toplength;
            $d_sj_receive = $row->d_sj_receive;
            $d_jatuh_tempo = $row->d_jatuh_tempo;
            $dudet = $this->fungsi->dateAdd("d", $n_spb_toplength, $d_nota);

            $this->db->query("update tm_spb set d_nota = '$d_nota', d_sj = '$d_nota' where i_nota ='$i_nota' and i_spb = '$i_spb' and i_area = '$i_area'");
            $this->db->query("update tm_nota_item set d_nota = '$d_nota' where i_nota ='$i_nota' and i_sj = '$i_sj' and i_area = '$i_area'");
            if ($d_sj_receive != '') {
                // SUdah DI Receive
                $this->db->query("update tm_nota set d_nota = '$d_nota', d_jatuh_tempo = '$dudet', d_sj = '$d_nota', d_sj_receive = '$d_nota' where i_nota ='$i_nota' and i_spb = '$i_spb' and i_area = '$i_area'");
            } else {
                $this->db->query("update tm_nota set d_nota = '$d_nota', d_jatuh_tempo = '$dudet', d_sj = '$d_nota' where i_nota ='$i_nota' and i_spb = '$i_spb' and i_area = '$i_area'");

            }

            if ($d_dkb != '') {

                $this->db->query("update tm_nota set d_dkb = '$d_nota' where i_nota ='$i_nota' and i_spb = '$i_spb' and i_area = '$i_area'");
                $this->db->query("update tm_dkb_item set d_sj = '$d_nota' where i_sj ='$i_sj' and i_area = '$i_area' and i_dkb = '$i_dkb'");
                $this->db->query("update tm_dkb set d_dkb = '$d_nota' where i_area = '$i_area' and i_dkb = '$i_dkb'");

            }
            // $dudet = $this->fungsi->dateAdd("d",60,$d_spb);
            // echo $d_spb."   ".$dudet."<br>";
            // $this->db->query("update tm_spb set n_spb_toplength = '60' where i_nota ='$i_nota' and i_spb = '$i_spb'");
            // $this->db->query("update tm_nota set d_jatuh_tempo = '$dudet', n_nota_toplength = '60' where i_nota ='$i_nota' and i_spb = '$i_spb'");

        }

        // echo "berhasil";
    }

    public function pindah_saldo()
    {
        $qsebelum = $this->db->query("select i_coa, v_saldo_akhir from tm_coa_saldo where i_periode = '202002'
        and i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kec%')");

        foreach ($qsebelum->result() as $row) {
            $i_coa = $row->i_coa;
            $v_saldo_akhir = $row->v_saldo_akhir;

            $data_saldo = $this->db->query("select v_mutasi_debet, v_mutasi_kredit from tm_coa_saldo where i_coa = '$i_coa' and i_periode = '202003'");
            if ($data_saldo->num_rows() > 0) {
                $data_saldo = $data_saldo->row();
                $v_mutasi_debet = $data_saldo->v_mutasi_debet;
                $v_mutasi_kredit = $data_saldo->v_mutasi_kredit;
                $akhir = ($v_saldo_akhir + $v_mutasi_debet - $v_mutasi_kredit);

                $this->db->query("update tm_coa_saldo set v_saldo_awal = '$v_saldo_akhir', v_saldo_akhir = '$akhir' where i_coa = '$i_coa'
                and i_periode = '202003'");

            }
            // $this->db->query()
        }
        // echo 'berhasil';
    }

    public function hitung_saldo()
    {
        $data_saldo = $this->db->query("select * from tm_coa_saldo where i_periode = '202002'
        and i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kec%')");

        foreach ($data_saldo->result() as $row) {

            $i_coa = $row->i_coa;
            $v_saldo_awal = $row->v_saldo_awal;
            $v_mutasi_debet = $row->v_mutasi_debet;
            $v_mutasi_kredit = $row->v_mutasi_kredit;
            $akhir = ($v_saldo_awal + $v_mutasi_debet - $v_mutasi_kredit);

            $this->db->query("update tm_coa_saldo set v_saldo_akhir = '$akhir' where i_coa = '$i_coa'
            and i_periode = '202002'");

        }
    }

    public function cari_debet_kredit()
    {
        $data = $this->db->query("select x.i_coa, z.i_area, sum(z.debet) as debet, sum(z.kredit) as kredit from(
            select x.i_area, sum(x.v_kk) as debet, 0 as kredit from(

            select a.* from(
             select i_kb as i_kk, d_kb as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_kb as v_kk, a.i_area, d_bukti, i_coa
             from tm_kb a
             inner join tr_area on (a.i_area=tr_area.i_area)
             where a.i_periode='202002' and to_char(a.d_kb,'yyyymm')='202002'
             and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
             and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where
             b.d_kk = a.d_kb and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '900-000%'
             and b.i_periode='202002')
             union all
             select i_kbank as i_kk, d_bank as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_bank as v_kk, a.i_area, d_bank as d_bukti, i_coa
             from tm_kbank a
             inner join tr_area on (a.i_area=tr_area.i_area)
             where a.i_periode='202002'
             and to_char(a.d_bank,'yyyymm')='202002'
             and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
             and a.v_bank not in (select b.v_kk as v_kb from tm_kk b where
             b.d_kk = a.d_bank and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '110-2%'
             and b.i_periode='202002')
             union all
             select a.i_kk as i_kk, a.d_kk, a.f_debet, a.e_description, a.i_kendaraan, a.n_km, a.v_kk, a.i_area, d_bukti, i_coa
             from tm_kk a, tr_area b
             where to_char(a.d_kk,'yyyymm')='202002'
             and a.i_area=b.i_area and a.f_kk_cancel='f'
             ) as a
             order by a.i_area, a.d_kk, a.i_kk


            ) as x
            where x.f_debet = 'f' or (substr(x.i_kk,1,2) = 'KB' or substr(x.i_kk,1,2) = 'BK')
            group by x.i_area
            union all
            /* Pemisah */
            select x.i_area, 0 as debet, sum(x.v_kk) as kredit from(

                select a.* from(
                    select i_kb as i_kk, d_kb as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_kb as v_kk, a.i_area, d_bukti, i_coa
                    from tm_kb a
                    inner join tr_area on (a.i_area=tr_area.i_area)
                    where a.i_periode='202002' and to_char(a.d_kb,'yyyymm')='202002'
                    and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
                    and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where
                    b.d_kk = a.d_kb and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '900-000%'
                    and b.i_periode='202002')
                    union all
                    select i_kbank as i_kk, d_bank as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_bank as v_kk, a.i_area, d_bank as d_bukti, i_coa
                    from tm_kbank a
                    inner join tr_area on (a.i_area=tr_area.i_area)
                    where a.i_periode='202002'
                    and to_char(a.d_bank,'yyyymm')='202002'
                    and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
                    and a.v_bank not in (select b.v_kk as v_kb from tm_kk b where
                    b.d_kk = a.d_bank and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '110-2%'
                    and b.i_periode='202002')
                    union all
                    select a.i_kk as i_kk, a.d_kk, a.f_debet, a.e_description, a.i_kendaraan, a.n_km, a.v_kk, a.i_area, d_bukti, i_coa
                    from tm_kk a, tr_area b
                    where to_char(a.d_kk,'yyyymm')='202002'
                    and a.i_area=b.i_area and a.f_kk_cancel='f'
                    ) as a
                    order by a.i_area, a.d_kk, a.i_kk

            ) as x
            where x.f_debet = 't'
            group by x.i_area
            ) as z
            inner join tr_coa x on(z.i_area = x.i_area and x.e_coa_name like '%Kas Keci%')
            group by z.i_area, x.i_coa");

        foreach ($data->result() as $row) {
            $i_coa = $row->i_coa;
            $v_mutasi_debet = $row->debet;
            $v_mutasi_kredit = $row->kredit;

            $this->db->query("update tm_coa_saldo set v_mutasi_debet = '$v_mutasi_debet', v_mutasi_kredit = '$v_mutasi_kredit'
                where i_coa = '$i_coa'
                and i_periode = '202002'");
        }
    }

    public function update_so()
    {
        $periode = '202004';
        $i_store = 'AA';
        $i_area = '00';

        $i_stockopname = $this->db->query("select i_stockopname_akhir from tm_mutasi_header where e_mutasi_periode = '$periode' and i_store = '$i_store'")->row()->i_stockopname_akhir;

        $data_baru = $this->db->query("select i_product, sum(n_deliver) as n_deliver from tm_nota_item where
        i_sj in('SJ-2001-000358',
        'SJ-2001-000359',
        'SJ-2001-000360',
        'SJ-2001-000361',
        'SJ-2001-000362',
        'SJ-2001-000363',
        'SJ-2001-000364',
        'SJ-2001-000365') and i_area = '32'
        group by i_product");

        foreach ($data_baru->result() as $row) {
            $i_product = $row->i_product;
            $n_deliver = $row->n_deliver;

            $cek_barang = $this->db->query("select * from tm_stockopname_item where i_stockopname = '$i_stockopname' and i_area ='$i_area' AND i_store = '$i_store'
            AND i_product = '$i_product' and i_product_grade = 'A' ");

            if ($cek_barang->num_rows() > 0) {
                $n_stockopname = $cek_barang->row()->n_stockopname;
                $stok_baru = $n_stockopname - $n_deliver;

                $this->db->query("update tm_stockopname_item set n_stockopname = '$stok_baru' where i_stockopname = '$i_stockopname' and i_area ='$i_area' AND i_store = '$i_store'
                AND i_product = '$i_product' and i_product_grade = 'A'");

            }
            // else {
            //     $data_barang = $this->db->query("select * from tm_stockopname_item where i_product = '$i_product' order by i_stockopname desc limit 1")->row();

            //     $d_stockopname = $this->db->query("select * from tm_stockopname where i_stockopname = '$i_stockopname' and i_area ='$i_area' AND i_store = '$i_store' ")->row()->d_stockopname;

            //     $data_insert = array(
            //         'i_stockopname' => $i_stockopname,
            //         'i_product' => $i_product,
            //         'i_product_grade' => 'A',
            //         'e_product_name' => $data_barang->e_product_name,
            //         'n_stockopname' => $n_deliver,
            //         'i_product_motif' => $data_barang->i_product_motif,
            //         'i_store' => $i_store,
            //         'i_store_location' => '01',
            //         'i_store_locationbin' => '00',
            //         'd_stockopname' => $d_stockopname,
            //         'i_area' => $i_area,
            //         'e_mutasi_periode' => $periode,
            //         'n_item_no' => '99',

            //     );

            //     $this->db->insert('tm_stockopname_item', $data_insert);

            // }
        }
    }

    public function harga()
    {
        $data = $this->db->query("select * from tr_product_price where i_price_group = '01'");

        foreach ($data->result() as $row) {

            $vproductretail = $row->v_product_retail;
            $diskon = ($vproductretail * 10) / 100;
            $row->v_product_retail = (int) $vproductretail - (int) $diskon;

            $data = array(
                'i_product' => $row->i_product,
                'i_product_grade' => $row->i_product_grade,
                'i_price_group' => 'G1',
                'e_product_name' => $row->e_product_name,
                'v_product_retail' => $row->v_product_retail,
                'v_product_mill' => $row->v_product_mill,
                'd_product_priceentry' => $row->d_product_priceentry,
                'd_product_priceupdate' => $row->d_product_priceupdate,
                'n_product_margin' => $row->n_product_margin,
            );

            $this->db->insert('tr_product_price', $data);

        }
    }

    public function closing_kasbank(){
        date_default_timezone_set("Asia/Jakarta");
        
        $tgl = date("d-m-Y");
		$periodex   = date('Ym', strtotime('-1 month', strtotime(date('Y-m-d'))));
        $periodey   = date('Ym');

        //$periodex   = date('Ym', strtotime('-1 month', strtotime('2021-02-28')));
        //$periodey   = '202102';

        $this->db->trans_begin();
        /* CLOSING KAS BESAR & BANK */
        // Update Debet Kredit Kas Besar Karena Sering berbeda saldonya ^_^ #Wahyu 2020-07-07
        $saldo_kb = $this->db->query("select sum(z.debet) as debet, sum(z.kredit) as kredit from(
            select x.d_kb, x.d_bukti, c.i_rv, d.i_pv, x.i_kb, x.e_description, x.i_area||' - '||b.e_area_name as i_area, x.i_coa, sum(x.debet) as debet, sum(x.kredit) as kredit from(
            select d_kb, d_bukti, i_kb, e_description, i_area, i_coa, e_coa_name, 0 as debet, v_kb as kredit from tm_kb where
            to_char(d_kb,'yyyymm')='$periodex' and f_debet = 't' and f_kb_cancel = 'f'
            union all
            select d_kb, d_bukti, i_kb, e_description, i_area, i_coa, e_coa_name, v_kb as debet,  0 as kredit from tm_kb where
            to_char(d_kb,'yyyymm')='$periodex' and f_debet = 'f' and f_kb_cancel = 'f'
            ) as x
            inner join tr_area b on(x.i_area = b.i_area)
            left join tm_rv_item c on(x.i_kb = c.i_kk and x.i_area = c.i_area_kb)
            left join tm_pv_item d on(x.i_kb = d.i_kk and x.i_area = d.i_area_kb)
            group by x.d_kb, x.i_kb, x.e_description, x.i_area, x.i_coa, b.e_area_name, x.d_bukti, c.i_rv, d.i_pv
            order by x.d_kb
            ) as z");
    
        if($saldo_kb->num_rows() > 0){
            $data_coa_saldo = $this->db->query("select i_coa, v_saldo_awal from tm_coa_saldo where i_periode = '$periodex' and e_coa_name like '%Kas Besar%'")->row();
            $saldo_kb = $saldo_kb->row();
            $kb_coa = $data_coa_saldo->i_coa;
            $kb_awal = $data_coa_saldo->v_saldo_awal;
            $kb_debet = $saldo_kb->debet;
            $kb_kredit = $saldo_kb->kredit;
            $kb_akhir = ((int) $kb_awal + (int) $kb_debet) - (int) $kb_kredit;
    
            $this->db->query("update tm_coa_saldo set v_mutasi_debet = '$kb_debet', v_mutasi_kredit = '$kb_kredit', v_saldo_akhir = '$kb_akhir' where i_periode = '$periodex'
            and i_coa = '$kb_coa' and e_coa_name like '%Kas Besar%'");
            //echo $kb_debet."|".$kb_kredit."|".$kb_akhir."|".$periodex."|".$kb_coa."<br>";
        }

        ###### closing
        $this->db->select(" * from tm_coa_saldo where i_periode = '$periodex' order by i_coa",false);
        $que = $this->db->get();
        if ($que->num_rows() > 0){
            foreach($que->result() as $ro){
            /* 1. MULAI UPDATE DULU SALDO DEBET KREDIT SAHIR NYA */
                /* UPDATE DEBET KREDIT SAHIR BANK */
                $data1  =   $this->db->query("  SELECT
                                                    i_coa_bank,
                                                    sum(sawal) AS sawal,
                                                    sum(debet) AS debet,
                                                    sum(kredit) AS kredit,
                                                    (sum(sawal)+sum(debet)-sum(kredit)) AS sahir
                                                FROM
                                                    (
                                                        SELECT
                                                            i_coa AS i_coa_bank,
                                                            i_periode,
                                                            sum(v_saldo_awal) AS sawal,
                                                            0 AS debet,
                                                            0 AS kredit
                                                        FROM
                                                            tm_coa_saldo
                                                        WHERE
                                                            i_coa LIKE '110-20%'
                                                        GROUP BY
                                                            i_coa,
                                                            i_periode
                                                    UNION ALL
                                                        SELECT
                                                            i_coa_bank,
                                                            i_periode,
                                                            0 AS sawal,
                                                            sum(v_bank) AS debet,
                                                            0 AS kredit
                                                        FROM
                                                            tm_kbank
                                                        WHERE
                                                            f_debet = 'f'
                                                            AND f_kbank_cancel = 'f'
                                                        GROUP BY
                                                            i_coa_bank,
                                                            i_periode
                                                    UNION ALL
                                                        SELECT
                                                            i_coa_bank,
                                                            i_periode,
                                                            0 AS sawal,
                                                            0 AS debet,
                                                            sum(v_bank) AS kredit
                                                        FROM
                                                            tm_kbank
                                                        WHERE
                                                            f_debet = 't'
                                                            AND f_kbank_cancel = 'f'
                                                        GROUP BY
                                                            i_coa_bank,
                                                            i_periode ) AS a
                                                WHERE 
                                                    a.i_periode = '$periodex'
                                                GROUP BY
                                                    i_coa_bank");
                foreach ($data1->result() as $row) {
                    $this->db->query("  UPDATE
                                            tm_coa_saldo
                                        SET
                                            v_mutasi_debet  = '$row->debet',
                                            v_mutasi_kredit = '$row->kredit',
                                            v_saldo_akhir   = '$row->sahir'
                                        WHERE
                                            i_coa = '$row->i_coa_bank'
                                            AND i_periode = '$periodex' ");
                }
                /* END UPDATE DEBET KREDIT SAHIR BANK */

                /* UPDATE DEBET KREDIT SAHIR KAS KECIL */
                $data2  =   $this->db->query("  SELECT
                                                    a.i_coa,
                                                    e_coa_name ,
                                                    sum(sawal) AS sawal,
                                                    sum(debet) AS debet,
                                                    sum(kredit) AS kredit,
                                                    (sum(sawal)+ sum(debet)-sum(kredit)) AS sahir
                                                FROM
                                                    (
                                                    SELECT
                                                        i_coa,
                                                        i_periode,
                                                        sum(v_saldo_awal) AS sawal,
                                                        0 AS debet,
                                                        0 AS kredit
                                                    FROM
                                                        tm_coa_saldo
                                                    WHERE
                                                        i_coa LIKE '110-12%'
                                                    GROUP BY
                                                        i_coa,
                                                        i_periode
                                                UNION ALL
                                                    SELECT
                                                        b.i_coa,
                                                        i_periode,
                                                        0 AS sawal,
                                                        sum(v_kk) AS debet,
                                                        0 AS kredit
                                                    FROM
                                                        tm_kk a
                                                    INNER JOIN tr_coa b ON
                                                        (a.i_area = b.i_area)
                                                    WHERE
                                                        f_debet = 'f'
                                                        AND f_kk_cancel = 'f'
                                                    GROUP BY
                                                        b.i_coa,
                                                        i_periode
                                                UNION ALL
                                                    SELECT
                                                        b.i_coa,
                                                        i_periode,
                                                        0 AS sawal,
                                                        0 AS debet,
                                                        sum(v_kk) AS kredit
                                                    FROM
                                                        tm_kk a
                                                    INNER JOIN tr_coa b ON
                                                        (a.i_area = b.i_area)
                                                    WHERE
                                                        f_debet = 't'
                                                        AND f_kk_cancel = 'f'
                                                    GROUP BY
                                                        b.i_coa,
                                                        i_periode ) AS a
                                                        INNER JOIN tr_coa b ON
                                                        (a.i_coa = b.i_coa)
                                                WHERE
                                                    a.i_periode = '$periodex'
                                                GROUP BY
                                                    a.i_coa,e_coa_name
                                                ORDER BY a.i_coa ");
                foreach ($data2->result() as $row) {
                    $this->db->query("  UPDATE
                                            tm_coa_saldo
                                        SET
                                            v_mutasi_debet  = '$row->debet',
                                            v_mutasi_kredit = '$row->kredit',
                                            v_saldo_akhir   = '$row->sahir'
                                        WHERE
                                            i_coa = '$row->i_coa'
                                            AND i_periode = '$periodex' ");
                }
                /* END DEBET KREDIT SAHIR KAS KECIL */
            /* NAH QUERY UPDATE SALDO DEBET KREDIT SAHIR NYA SAMPE SINI*/    
            
            /* 2. BARU PINDAH SALDO DISINI */
                $this->db->select("v_saldo_akhir as jum from tm_coa_saldo where i_periode='$periodex' and i_coa='$ro->i_coa'",false);
                $query = $this->db->get();
                if ($query->num_rows() > 0){
                    foreach($query->result() as $row){
                        if($row->jum==null)$row->jum=0;
                        $quer 	= $this->db->query("SELECT current_timestamp as c");
                        $saldo	= $row->jum;
                        $riw   	= $quer->row();
                        $update	= $riw->c;
                        $coa=$ro->i_coa;
                        
                        $qu=$this->db->query("select e_coa_name from tr_coa where i_coa like '$coa'");
                        if ($qu->num_rows() > 0){
                            foreach($qu->result() as $tes){
                                $coaname=$tes->e_coa_name;
                            }
                            $qu->free_result();
                        }
                        #select data periode yang sudah ada
                        $this->db->select("* from tm_coa_saldo where i_periode='$periodey' and i_coa='$ro->i_coa'",false);
                        $qu = $this->db->get();
                        if ($qu->num_rows()== 0){
                            $quer 	= $this->db->query("SELECT current_timestamp as c");
                            $row   	= $quer->row();
                            $entry	= $row->c;
                            $coa=$ro->i_coa;
                            $user=$this->session->userdata('user_id');
                        #update saldo akhir=saldo+debet-kredit (yang di update saldo awal, saldo akhir)
                            $this->db->query(" insert into tm_coa_saldo 
                                            (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
                                            i_entry, e_coa_name)
                                            values
                                            ('$periodey','$coa',$saldo,0,0,$saldo,'$entry','$user','$coaname')");
                        }else{
                            $quer 	= $this->db->query("SELECT current_timestamp as c");
                            $row   	= $quer->row();
                            $update	= $row->c;
                            $coa=$ro->i_coa;
                            $user=$this->session->userdata('user_id');
                            $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo+v_mutasi_debet-v_mutasi_kredit, 
                                                d_update='$update', i_update='$user' where i_periode='$periodey' and i_coa='$coa'");
                        }
                    }/* END FOREACH */
                }/* END BACA DATA COA SALDO */
            }
        }
        /* CLOSING PERIODE KAS BANK */
            $hariini  = date('Y-m-d');
            $tanggal1 = date('Y-m').'-01';
            $data = $this->db->query(" SELECT * FROM tm_closing_kas_bank ");
            if($data->num_rows() > 0){
                $this->db->query("
                    UPDATE 
                        tm_closing_kas_bank
                    SET 
                        d_closing_kb		= '$hariini',
                        d_open_kb			= '$tanggal1',
                        d_closing_kbin		= '$hariini',
                        d_open_kbin			= '$tanggal1',
                        d_closing_kbank		= '$hariini',
                        d_open_kbank		= '$tanggal1',
                        d_closing_kbankin	= '$hariini',
                        d_open_kbankin		= '$tanggal1',
                        d_closing_kk		= '$hariini',
                        d_open_kk			= '$tanggal1',
                        d_closing_kkin		= '$hariini',
                        d_open_kkin			= '$tanggal1'
                ");
            }else{
                $this->db->query("  INSERT INTO tm_closing_kas_bank
                                    VALUES('$hariini','$tanggal1','$hariini','$tanggal1','$hariini','$tanggal1'
                                    ,'$hariini','$tanggal1','$hariini','$tanggal1','$hariini','$tanggal1') ");
            }
        /* END  CLOSING PERIODE KAS BESAR & BANK */
        if (($this->db->trans_status() === false)) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();

            $id         = "SYSTEM";
            $ip_address = 'kosong';

            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }

            $pesan = 'Pindah Saldo Kas Bank Otomatis '.$tgl;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);
        }
    } //END closing_kasbank
    
    public function pindah_mutasi_saldoawal()
    {
        date_default_timezone_set("Asia/Jakarta");
        $tgl = date("d-m-Y");
        $periode        = date('Ym');
        $periodekemarin = date('Ym', strtotime('-1 month', strtotime($periode)));

        $this->db->trans_begin();

        $this->db->query(" delete from tm_mutasi_saldoawal where e_mutasi_periode='$periode'");

        /**************SALDO AWAL STOK BUAT GUDANG CABANG*******************/
        $data1 = $this->db->query(" SELECT i_store, i_store_location, i_store_locationbin, '$periode' as e_mutasi_periode, i_product, i_product_grade, i_product_motif, 
                                    n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal  
                                    from f_mutasi_stock_daerah_all_saldoakhir('$periodekemarin')");
        foreach ($data1->result() as $row) {
            $data1 = array(
                            'i_store'               => $row->i_store,
                            'i_store_location'      => $row->i_store_location,
                            'i_store_locationbin'   => $row->i_store_locationbin,
                            'e_mutasi_periode'      => $row->e_mutasi_periode,
                            'i_product'             => $row->i_product,
                            'i_product_grade'       => $row->i_product_grade,
                            'i_product_motif'       => $row->i_product_motif,
                            'n_saldo_awal'          => $row->n_saldo_awal,
            );
            $this->db->insert('tm_mutasi_saldoawal', $data1);
        }

        /**************SALDO AWAL STOK BUAT GUDANG PUSAT*******************/
        $data2 = $this->db->query(" SELECT i_store, i_store_location, i_store_locationbin, '$periode' as e_mutasi_periode, i_product, i_product_grade, i_product_motif, 
                                    n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal  
                                    from f_mutasi_stock_pusat_saldoakhir('$periodekemarin')");
        
        foreach ($data2->result() as $row) {
            $data2 = array(
                            'i_store'               => $row->i_store,
                            'i_store_location'      => $row->i_store_location,
                            'i_store_locationbin'   => $row->i_store_locationbin,
                            'e_mutasi_periode'      => $row->e_mutasi_periode,
                            'i_product'             => $row->i_product,
                            'i_product_grade'       => $row->i_product_grade,
                            'i_product_motif'       => $row->i_product_motif,
                            'n_saldo_awal'          => $row->n_saldo_awal,
            );
            $this->db->insert('tm_mutasi_saldoawal', $data2);
        }

        if (($this->db->trans_status() === false)) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            
            $id         = "SYSTEM";
            $ip_address = 'kosong';

            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }

            $pesan = 'Update Mutasi Saldo Awal Otomatis '.$tgl;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);
        }
    }
    public function update_saldo_piutang()
    {
        date_default_timezone_set("Asia/Jakarta");
        $tgl = date("d-m-Y");
        
        $this->db->trans_begin();

        $query = $this->db->query(" SELECT i_area, i_customer, e_customer_name, v_flapond, piutang, v_saldo FROM v_saldo_piutang ");

        foreach($query->result() AS $row){
            $this->db->query("  UPDATE
                                    tr_customer_groupar
                                SET
                                    v_saldo = '$row->v_saldo'
                                WHERE
                                    i_customer = '$row->i_customer' ");
        }

        if (($this->db->trans_status() === false)) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();

            $id         = "SYSTEM";
            $ip_address = 'kosong';
           
            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }

            $pesan = 'Update Saldo Piutang Otomatis '.$tgl;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);
        }
    }
}

/* End of file Coba.php */
