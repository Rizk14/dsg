<?php
class Cform extends CI_Controller {

  function __construct () {
	  
    parent::__construct();
    
    $this->load->library('pagination');
    $this->load->model('info-opvssj/mmaster');
  }

  function index() {

	$is_logged_in = $this->session->userdata('is_logged_in');
	
	if (!isset($is_logged_in) || $is_logged_in!=true) {
		redirect('loginform');
	}
	
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['isi'] = 'info-opvssj/vmainform';
	$this->load->view('template',$data);
	
  }
  
  function view() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	
	if (!isset($is_logged_in) || $is_logged_in!=true) {
		redirect('loginform');
	}
	
    $data['isi'] = 'info-opvssj/vformview';
    
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$supplier = $this->input->post('supplier', TRUE);
	
	if ($date_from=='' && $date_to=='') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
	}

    $jum_total = $this->mmaster->get_all_ordertanpalimit($date_from, $date_to, $supplier);
						
	$data['query'] = $this->mmaster->get_all_order($date_from, $date_to, $supplier);
	$data['jum_total'] = count($jum_total);

	// ambil data nama supplier
	$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp FROM tm_supplier WHERE id = '$supplier' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$kode_supplier	= $hasilrow->kode_supplier;
		$nama_supplier	= $hasilrow->nama;
	}
	else {
		$kode_supplier = '';
		$nama_supplier = '';
	}
		
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['supplier'] = $supplier;
	$data['kode_supplier'] = $kode_supplier;
	$data['nama_supplier'] = $nama_supplier;
	
	$this->load->view('template',$data);
	
  }
  
  function do_print() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  $date_from 	= $this->uri->segment(4);
	  $date_to 	= $this->uri->segment(5);
	  $supplier 	= $this->uri->segment(6);
	
	  $data['query'] = $this->mmaster->get_all_order($date_from, $date_to, $supplier);
	  
	  // ambil data nama supplier
		$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp FROM tm_supplier WHERE id = '$supplier' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
		}
		else {
			$kode_supplier = '';
			$nama_supplier = '';
		}
		
	  $data['date_from'] = $date_from;
	  $data['date_to'] = $date_to;
	  $data['supplier'] = $supplier;
	  $data['kode_supplier'] = $kode_supplier;
	  $data['nama_supplier'] = $nama_supplier;
	  $this->load->view('info-opvssj/vprint',$data);
  }
  
  // 04-08-2012
  function export_excel() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  $date_from 	= $this->uri->segment(4);
	  $date_to 	= $this->uri->segment(5);
	  $supplier 	= $this->uri->segment(6);
	  $jenis 	= $this->uri->segment(7);
	
	  $query = $this->mmaster->get_all_order_forexport($date_from, $date_to, $supplier);
	  
	  // ambil data nama supplier
		$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp FROM tm_supplier WHERE id = '$supplier' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
		}
		else {
			$kode_supplier = '';
			$nama_supplier = '';
		}
		
		if ($supplier == '0')
			$nama_suppliernya = "All";
		else
			$nama_suppliernya = $kode_supplier." - ".$nama_supplier;
		
	  $html_data = " 
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='7' align='center'>LAPORAN OP vs SJ BUKTI PENERIMAAN BARANG</th>
		 </tr>
		 <tr>
			<th colspan='7' align='center'>Supplier: $nama_suppliernya</th>
		 </tr>
		 <tr>
			<th colspan='7' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 
		 <tr>
		<th>Tgl </th>
		 <th>Nomor OP</th>
		 <th>Supplier</th>
		 <th>List Barang</th>
		 <th>Qty OP</th>
		 <th>Bukti Penerimaan</th>
		 <th>Sisa OP</th>
		 <th>Keterangan</th>
	 </tr>
	</thead>
	<tbody>
	  ";
	  
	  if (is_array($query)) {
		  for($j=0;$j<count($query);$j++) {
				$pisah1 = explode("-", $query[$j]['tgl_op']);
				 
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				if ($bln1=='01')
					$nama_bln = "Januari";
				else if ($bln1=='02')
					$nama_bln = "Februari";
				else if ($bln1=='03')
					$nama_bln = "Maret";
				else if ($bln1=='04')
					$nama_bln = "April";
				else if ($bln1=='05')
					$nama_bln = "Mei";
				else if ($bln1=='06')
					$nama_bln = "Juni";
				else if ($bln1=='07')
					$nama_bln = "Juli";
				else if ($bln1=='08')
					$nama_bln = "Agustus";
				else if ($bln1=='09')
					$nama_bln = "September";
				else if ($bln1=='10')
					$nama_bln = "Oktober";
				else if ($bln1=='11')
					$nama_bln = "November";
				else if ($bln1=='12')
					$nama_bln = "Desember";
				
				$tgl_op = $tgl1." ".$nama_bln." ".$thn1;
				
				$html_data.= "<tr class=\"record\" valign=\"top\">
				<td width='10%'>".$tgl_op."</td>
				<td width='10%'>".$query[$j]['no_op']."</td>
				<td width='15%'>".$query[$j]['nama_supplier']."</td>
				<td nowrap width='20%'>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>
				<td align='right'>".$query[$j]['qty']."</td>
				<td align='right'>".$query[$j]['pemenuhan']."</td>
				<td align='right'>".$query[$j]['sisa']."</td>
				<td align='right'>".$query[$j]['keterangan']."</td>
				</tr>";

		 } // end for
		 $html_data.="</tbody></table>";
	   }
		 $nama_file = "laporan_opvssj";
		if ($jenis == 1) {
			$export_excel1 = "export_excel";
			$nama_file.= ".xls";
		}
		else {
			$export_excel1 = "export_ods";
			$nama_file.= ".ods";
		}
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
	  
  }

}
