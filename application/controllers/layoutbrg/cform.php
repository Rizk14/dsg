<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->library('pagination');
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "LAYOUT BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('layoutbrg/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/layoutbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$qry_codelayout	= $this->mclass->layoutcode();
		if($qry_codelayout->num_rows()>0) {
			$row_codelayout= $qry_codelayout->row();
			$data['codelayout']	= $row_codelayout->ilayout;
		} else {
			$data['codelayout']	= 1;
		}
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']='layoutbrg/vmainform';
		$this->load->view('template',$data);	
	}
function tambah() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "LAYOUT BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('layoutbrg/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/layoutbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$qry_codelayout	= $this->mclass->layoutcode();
		if($qry_codelayout->num_rows()>0) {
			$row_codelayout= $qry_codelayout->row();
			$data['codelayout']	= $row_codelayout->ilayout;
		} else {
			$data['codelayout']	= 1;
		}
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']='layoutbrg/vmainformadd';
		$this->load->view('template',$data);	
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "LAYOUT BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('layoutbrg/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/layoutbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();

		$qry_codelayout	= $this->mclass->layoutcode();
		if($qry_codelayout->num_rows()>0) {
			$row_codelayout= $qry_codelayout->row();
			$data['codelayout']	= $row_codelayout->ilayout;
		} else {
			$data['codelayout']	= 1;
		}
				
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
	$data['isi']='layoutbrg/vmainform';
		$this->load->view('template',$data);	
		
	}

	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']='layoutbrg/vmainform';
		$this->load->view('template',$data);	
		
	}

	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ilayout	= $this->input->post('ilayout')?$this->input->post('ilayout'):$this->input->get_post('ilayout');
		$elayoutname	= $this->input->post('elayoutname')?$this->input->post('elayoutname'):$this->input->get_post('elayoutname');
		if( (isset($ilayout) && !empty($ilayout)) && 
		    (isset($elayoutname) && !empty($elayoutname)) ) {
			$this->load->model('layoutbrg/mclass');
			$this->mclass->msimpan($ilayout,$elayoutname);
		} else {
			$data['page_title']	= "LAYOUT BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			$this->load->model('layoutbrg/mclass');
	
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= base_url().'index.php/layoutbrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$qry_codelayout	= $this->mclass->layoutcode();
			if($qry_codelayout->num_rows()>0) {
				$row_codelayout= $qry_codelayout->row();
				$data['codelayout']	= $row_codelayout->ilayout;
			} else {
				$data['codelayout']	= 1;
			}
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
					
			$data['isi']='layoutbrg/vmainform';
		$this->load->view('template',$data);	
		}
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
				
		$this->load->model('layoutbrg/mclass');

		$qry_layoutbrg		= $this->mclass->medit($id);
		if($qry_layoutbrg->num_rows() > 0 ) {
			$row_layoutbrg	= $qry_layoutbrg->row();
			$data['i_layout'] = $row_layoutbrg->i_layout;
			$data['e_layout_name'] = $row_layoutbrg->e_layout_name;	
		} else {
			$data['i_layout'] = "";
			$data['e_layout_name'] = "";	
		}
		$data['isi']='layoutbrg/veditform';
		$this->load->view('template',$data);	
		
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ilayout	= @$this->input->post('ilayout')?@$this->input->post('ilayout'):@$this->input->get_post('ilayout');
		$e_layout_name = @$this->input->post('elayoutname')?@$this->input->post('elayoutname'):@$this->input->get_post('elayoutname');
		
		if( (isset($ilayout) && !empty($ilayout)) &&
		    (isset($e_layout_name) && !empty($e_layout_name)) ) {
			$this->load->model('layoutbrg/mclass');
			$this->mclass->mupdate($ilayout,$e_layout_name);
		} else {
						$data['page_title']	= "LAYOUT BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			$this->load->model('layoutbrg/mclass');
	
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= base_url().'index.php/layoutbrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$qry_codelayout	= $this->mclass->layoutcode();
			if($qry_codelayout->num_rows()>0) {
				$row_codelayout= $qry_codelayout->row();
				$data['codelayout']	= $row_codelayout->ilayout;
			} else {
				$data['codelayout']	= 1;
			}
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
		
			$data['isi']='layoutbrg/vmainform';
		$this->load->view('template',$data);	
		}	
	}
	
	/* 20052011
	function actdelete() {
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_layout',array('i_layout'=>$id));
	}
	*/

	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('layoutbrg/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_layout	= $this->input->post('txt_i_layout')?$this->input->post('txt_i_layout'):$this->input->get_post('txt_i_layout');
		$txt_e_layout_name	= $this->input->post('txt_e_layout_name')?$this->input->post('txt_e_layout_name'):$this->input->get_post('txt_e_layout_name');
		
		$data['page_title']	= "LAYOUT BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('layoutbrg/mclass');

		$query	= $this->mclass->viewcari($txt_i_layout,$txt_e_layout_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/layoutbrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txt_i_layout,$txt_e_layout_name,$pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('layoutbrg/vcariform',$data);			
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_layout	= $this->input->post('txt_i_layout')?$this->input->post('txt_i_layout'):$this->input->get_post('txt_i_layout');
		$txt_e_layout_name	= $this->input->post('txt_e_layout_name')?$this->input->post('txt_e_layout_name'):$this->input->get_post('txt_e_layout_name');
		
		$data['page_title']	= "LAYOUT BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('layoutbrg/mclass');

		$query	= $this->mclass->viewcari($txt_i_layout,$txt_e_layout_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/layoutbrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txt_i_layout,$txt_e_layout_name,$pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('layoutbrg/vcariform',$data);			
	}	
}
?>
