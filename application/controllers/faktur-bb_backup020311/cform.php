<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-bb/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================

	$id_op = $this->input->post('id_op', TRUE);  
	$no_op = $this->input->post('no_op', TRUE);  
	$jenis_brg = $this->input->post('jenis_brg', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	$id_op_detail = $this->input->post('id_brg', TRUE);  
	$list_brg = explode(";", $id_op_detail);
	
	if ($proses_submit == "Proses") {
		if ($id_op !='') {
			$data['op_detail'] = $this->mmaster->get_detail_op($id_op, $jenis_brg, $list_brg);
			$data['msg'] = '';
			$data['id_op'] = $id_op;
			$data['no_op'] = $no_op;
			$data['supplier'] = $this->mmaster->get_supplier();
			$query2	= $this->db->query(" SELECT id_pp FROM tm_op WHERE id = '$id_op' ");
			$hasilrow = $query2->row();
			$id_pp	= $hasilrow->id_pp;
			
			$query2	= $this->db->query(" SELECT no_pp FROM tm_pp WHERE id = '$id_pp' ");
			$hasilrow = $query2->row();
			$no_pp	= $hasilrow->no_pp;
			$data['no_pp'] = $no_pp;
		}
		else {
			$data['msg'] = 'OP harus dipilih';
			$data['id_op'] = '';
			$data['no_op'] = '';
			$data['no_pp'] = '';
		}
		$data['go_proses'] = '1';
	}
	else {
		$data['msg'] = '';
		$data['id_op'] = '';
		$data['go_proses'] = '';
	}
	$data['isi'] = 'faktur-bb/vmainform';
	$data['jenis_brg'] = $jenis_brg;
	$this->load->view('template',$data);

  }
  
  function edit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================

	$id_pembelian 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_pembelian($id_pembelian);
	$data['supplier'] = $this->mmaster->get_supplier();
	$data['msg'] = '';

	$data['isi'] = 'faktur-bb/veditform';
	$this->load->view('template',$data);

  }
  
  function editfjual(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================

	$id_pembelian 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_pembelian($id_pembelian);
	$data['supplier'] = $this->mmaster->get_supplier();
	$data['msg'] = '';
	$jenis_brg = $data['query'][0]['jenis_brg'];
	$th_now	= date("Y");
	
	if ($data['query'][0]['no_faktur'] == '') {
		// generate no faktur
		$query3	= $this->db->query(" SELECT no_faktur FROM tm_pembelian WHERE jenis_brg = '$jenis_brg' ORDER BY no_faktur DESC LIMIT 1 ");
		$hasilrow = $query3->row();
		$no_faktur	= $hasilrow->no_faktur;
		if(strlen($no_faktur)==12) {
			$nofb = substr($no_faktur, 3, 9);
			$n_fb	= (substr($nofb,4,5))+1;
			$th_fb	= substr($nofb,0,4);
			if($th_now==$th_fb) {
					$jml_n_fb	= $n_fb;
					switch(strlen($jml_n_fb)) {
						case "1": $kodefb	= "0000".$jml_n_fb;
						break;
						case "2": $kodefb	= "000".$jml_n_fb;
						break;	
						case "3": $kodefb	= "00".$jml_n_fb;
						break;
						case "4": $kodefb	= "0".$jml_n_fb;
						break;
						case "5": $kodefb	= $jml_n_fb;
						break;	
					}
					$nomorfb = $th_now.$kodefb;
			}
			else {
				$nomorfb = $th_now."00001";
			}
		}
		else {
			$nomorfb	= $th_now."00001";
		}
		$nomorfb = "FJ-".$nomorfb;
		$data['no_fb'] = $nomorfb;
	}

	$data['isi'] = 'faktur-bb/veditform2';
	$this->load->view('template',$data);

  }
  
  function updatedata() {
			$id_pembelian 	= $this->input->post('id_pembelian', TRUE);
			$no_fb 	= $this->input->post('no_sj', TRUE);
			$tgl_fb 	= $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_fb);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fb = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$jenis_brg = $this->input->post('jenis_brg', TRUE);  
			$kode_supplier = $this->input->post('hide_supplier', TRUE);  
			$gtotal = $this->input->post('gtotal', TRUE);  
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			
			$tgl = date("Y-m-d");
			if ($sisa_hutang == 0) {
				$status_lunas = 1;
			}
			else {
				$status_lunas = 0;
			}
			
			//----------				
				//ambil data gudang, sementara di-fixkan dulu. nantinya sih pengennya ada pemilihan tempat gudangnya
				if ($jenis_brg == '1') {
					$nama_gudang = "Gudang Bahan Baku";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_bahan_baku";
				}
				else if ($jenis_brg == '2') {
					$nama_gudang = "Gudang Accessories";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_asesoris";
				}
				else if ($jenis_brg == '3') {
					$nama_gudang = "Gudang Bahan Pendukung";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_bahan_pendukung";
				}
				else
					$nama_tabel = "tm_perlengkapan";
				
				if ($jenis_brg != '4') {
					$query3	= $this->db->query(" SELECT id FROM tm_gudang WHERE nama = '$nama_gudang' AND kode_lokasi = '$lokasi'");
					$hasilrow = $query3->row();
					$id_gudang	= $hasilrow->id;
				}
				else
					$id_gudang = 0;
			
			//update headernya
			$this->db->query(" UPDATE tm_pembelian SET no_sj = '$no_fb', 
								tgl_sj = '$tgl_fb', total = '$gtotal',
								kode_supplier = '$kode_supplier', uang_muka = '$uang_muka',
								sisa_hutang = '$sisa_hutang', keterangan = '$ket',
								tgl_update = '$tgl', status_lunas = '$status_lunas',
								pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak',
								total_pajak = '$total_pajak' where id= '$id_pembelian' ");
			
				//$cek_data = $this->mmaster->cek_data($no_fb, $jenis_brg);
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
					   if (($this->input->post('harga_'.$i, TRUE) != $this->input->post('harga_lama_'.$i, TRUE)) || 
					   ($this->input->post('qty_'.$i, TRUE) != $this->input->post('qty_lama_'.$i, TRUE)) ) {
						$this->db->query(" UPDATE tm_pembelian_detail SET qty = '".$this->input->post('qty_'.$i, TRUE)."', 
								harga = '".$this->input->post('harga_'.$i, TRUE)."', diskon = '".$this->input->post('diskon_'.$i, TRUE)."',
								pajak = '".$this->input->post('pajak_'.$i, TRUE)."', total = '".$this->input->post('total_'.$i, TRUE)."'
								where id= '".$this->input->post('id_detail_'.$i, TRUE)."' ");
						
						//-----------------------------
							$data_harga = array(
								'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
								'kode_supplier'=>$kode_supplier,
								'jenis_brg'=>$jenis_brg,
								'harga'=>$this->input->post('harga_'.$i, TRUE),
								'tgl_input'=>$tgl
							);
							$this->db->insert('tt_harga', $data_harga);
					
							// edit transaksi stoknya
							//1. cek stok brg berdasarkan no_bukti di tt_stok
							$query3	= $this->db->query(" SELECT masuk FROM tt_stok WHERE no_bukti = '$no_fb' 
											AND jenis_brg = '$jenis_brg' AND kode_brg='".$this->input->post('kode_'.$i, TRUE)."'
											AND id_gudang = '$id_gudang' ");
							$hasilrow = $query3->row();
							$masuk	= $hasilrow->masuk;
							
							//2. ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE jenis_brg = '$jenis_brg' 
											AND kode_brg='".$this->input->post('kode_'.$i, TRUE)."'
											AND id_gudang = '$id_gudang' ");
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$qty_baru = $this->input->post('qty_'.$i, TRUE);
							$stokreset = $stok_lama-$masuk;
							$stokskrg = ($stok_lama-$masuk) + $qty_baru;
							
							//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
							$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, jenis_brg, keluar, saldo, id_gudang, tgl_input, harga)
							VALUES ('".$this->input->post('kode_'.$i, TRUE)."', '$no_fb', '$jenis_brg', '$masuk', '$stokreset', '$id_gudang', '$tgl', '".$this->input->post('harga_lama_'.$i, TRUE)."' ) ");
							
							//4. insert ke tabel tt_stok dgn tipe masuk, utk mengupdate data stoknya
							$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, jenis_brg, masuk, saldo, id_gudang, tgl_input, harga)
							VALUES ('".$this->input->post('kode_'.$i, TRUE)."', '$no_fb', '$jenis_brg', '$qty_baru', '$stokskrg', '$id_gudang', '$tgl', '".$this->input->post('harga_'.$i, TRUE)."' ) ");
							
							//5. update stok di tm_stok
							$this->db->query(" UPDATE tm_stok SET stok = '$stokskrg', tgl_update_stok = '$tgl'
												where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."' 
												AND jenis_brg = '$jenis_brg' AND id_gudang = '$id_gudang' ");
							
							//update harga di tabel master
							$this->db->query(" UPDATE ".$nama_tabel." SET harga = '".$this->input->post('harga_'.$i, TRUE)."' 
												where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."' ");
						
						//--------------------------------------------------------------
					  }
					}
					redirect('faktur-bb/cform/view');
				
  }
  
  // updatefjual
  function updatefjual() {
			$id_pembelian 	= $this->input->post('id_pembelian', TRUE);
			$no_fb 	= $this->input->post('no_faktur', TRUE);
			$tgl_fb 	= $this->input->post('tgl_faktur', TRUE);  
			$pisah1 = explode("-", $tgl_fb);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fb = $thn1."-".$bln1."-".$tgl1;
									
			$tgl = date("Y-m-d");			
			
			//update headernya
			$this->db->query(" UPDATE tm_pembelian SET no_faktur = '$no_fb', 
								tgl_faktur = '$tgl_fb' where id= '$id_pembelian' ");
			
					redirect('faktur-bb/cform/view2');
				
  }
  
/*  function show_popup_brg($keywordcari){
	// =======================
	// disini coding utk pengecekan user login
	//$departemen = ambil infonya dari tabel user
//========================

	$posisi 	= $this->uri->segment(4);
	// if $departemen == 'Bahan Baku' { } else { get_asesoris}
	//$data['bahan_baku'] = $this->mmaster->get_bahan_baku($num, $offset, $keywordcari);
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jum_total = $this->mmaster->get_bahan_bakutanpalimit($keywordcari);
							$config['base_url'] = base_url()."index.php/faktur-bb/cform/show_popup_brg/".$posisi."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_bahan_baku($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total); //echo count($jum_total); die();
	$data['posisi'] = $posisi;

	$this->load->view('faktur-bb/vpopupbrg',$data);

  } */
  
  function show_popup_op(){
	// =======================
	// disini coding utk pengecekan user login
//========================

	$jenis_brg 	= $this->uri->segment(4);
	if ($jenis_brg == '')
		$jenis_brg = $this->input->post('jenis_brg', TRUE);  
		
	$kode_bagian = ''; // ini nanti dari user login
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jum_total = $this->mmaster->get_optanpalimit($jenis_brg, $keywordcari);
							$config['base_url'] = base_url()."index.php/faktur-bb/cform/show_popup_op/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_op($config['per_page'],$this->uri->segment(5), $jenis_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$data['jenis_brg'] = $jenis_brg;

	$this->load->view('faktur-bb/vpopupop',$data);

  }

  function submit(){
	$this->load->library('form_validation');
		//$goedit 	= $this->input->post('goedit', TRUE);

			$this->form_validation->set_rules('no_sj', 'Nomor SJ', 'required');
			$this->form_validation->set_rules('tgl_sj', 'Tanggal SJ', 'required');

		if ($this->form_validation->run() == FALSE)
		{
		/*	$data['isi'] = 'faktur-bb/vmainform';
			$data['msg'] = 'Field2 nomor faktur, tanggal faktur tidak boleh kosong..!';
			$this->load->view('template',$data); */
		}
		else
		{
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj = $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$id_op 	= $this->input->post('id_op', TRUE);
			$jenis_brg = $this->input->post('jenis_brg', TRUE);  
			$kode_supplier = $this->input->post('kode_supplier', TRUE);  
			$gtotal = $this->input->post('gtotal', TRUE);  
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			
		//	if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($no_sj, $jenis_brg);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'faktur-bb/vmainform';
					$data['msg'] = "Data no SJ ".$no_sj." sudah ada..!";
					//$data['msg'] = '';
					$data['id_pp'] = '';
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
					// inspired from proIT inventory
						$this->mmaster->save($no_sj,$tgl_sj,$id_op,$jenis_brg, $kode_supplier,$gtotal,$total_pajak,$uang_muka, 
								$sisa_hutang,$ket,$hide_pkp, $hide_tipe_pajak, $this->input->post('id_op_detail_'.$i, TRUE), 
								$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE), 
									$this->input->post('pajak_'.$i, TRUE), $this->input->post('diskon_'.$i, TRUE), 
									$this->input->post('total_'.$i, TRUE) );
					}
					redirect('faktur-bb/cform/view');
				}
		//	} // end if goedit == ''
			
			
		}
  }
  
  function view(){
    $data['isi'] = 'faktur-bb/vformview';
    $keywordcari = '';
    $csupplier = '0';
	$kode_bagian = ''; // ini nanti ambil dari login user
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-bb/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  // view utk edit faktur penj
  function view2(){
    $data['isi'] = 'faktur-bb/vformview2';
    $keywordcari = '';
    $jenis_brg = '0';
    
	$kode_bagian = ''; // ini nanti ambil dari login user
	
    $jum_total = $this->mmaster->getAlltanpalimit($jenis_brg, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-bb/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $jenis_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-bb/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'faktur-bb/vformview';
	$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  //cari utk edit faktur penj
  function cari2(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jenis_brg = $this->input->post('jenis_brg', TRUE);  
    $jum_total = $this->mmaster->getAlltanpalimit($jenis_brg, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-bb/cform/view2/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $jenis_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'faktur-bb/vformview2';
	$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('faktur-bb/cform/view');
  }
  
  function get_pkp_tipe_pajak() {
		$kode_sup 	= $this->uri->segment(4);
		$rows = array();
		if(isset($kode_sup)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			$rows = $this->mmaster->get_pkp_tipe_pajak_bykodesup($kode_sup);
			
			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);

  }
}
