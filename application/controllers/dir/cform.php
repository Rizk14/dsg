<?php

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
		$this->load->helper('directory');
		$this->load->helper('file');
	}
	
	function index() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		
		$data['base_url']	= base_url();
		
		$map = directory_map('./dir/', TRUE);
		
		//~ $data['directories']	= 'dir';
		//hasil $map diatas adalah array nama filenya
		//Contoh: $map[0], $map[1]. Urutan filenya sesuai 
		//  abjad nama file. Gunakan $map ini untuk kode src 
		//  pada tag <image>
		//Sekarang buat validasi apakah user telah memilih 
		//  gambar tertentu melalui link Prev atau Next
		
		//~ $jmlpic = count($map); // itung banyaknya pic
		//~ 
		//~ if($jmlpic>0) {
			//~ $no = $this->uri->segment(4,0);
			//~ 
			//~ if($no==0) { //jika belum ada pilihan 
			   //~ $data['prev']=$jmlpic;
			   //~ $data['next']=$no+1;
			   //~ $data['filename'] =$map[$no];
			//~ }
			//~ if($no==$jmlpic) { //pilihan pic adalah file terakhir
			   //~ $data['prev']	= $no-1;
			   //~ $data['next']	= 0;
			   //~ $data['filename']	= $map[$no];
			   //~ $data['map']	= $map;
			//~ } else {
			   //~ $data['prev']	= $no-1;
			   //~ $data['next']	= $no+1;
			   //~ $data['filename']	= $map[$no];
			   //~ $data['map']	= $map;
			//~ }
		//~ } else {
			//~ $data['map']	= array();
		//~ }	
			$data['map']	= $map;
			$data['isi']	='dir/vform';
			$this->load->view('template',$data);	
	
	}
	
	function actdelete() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$file				= $this->uri->segment(4);
		$directories		= 'files';
		$tmp_directories	= 'tmp_files';
		$string = read_file('./'.$directories.'/'.$file);
		
		if($string) {
			chmod('./'.$directories.'/'.$file, 0777);
			//delete_files('./'.$directories.'/'.$file);
			@copy('./'.$directories.'/'.$file, './'.$tmp_directories.'/'.$file);
			@unlink('./'.$directories.'/'.$file);
			print "<script>alert(\"file ".$file." telah dihapus.\");window.open(\"../index\", \"_self\");</script>";
			//redirect('directories/cform');
		} else {
			chmod('./'.$directories.'/'.$file, 0755);
			print "<script>alert(\"Maaf, file Laporan gagal dihapus. Terimakasih.\");window.open(\"../index\", \"_self\");</script>";
			//redirect('directories/cform');
		}
	}
}
?>
