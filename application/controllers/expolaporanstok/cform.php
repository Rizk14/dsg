<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		//$this->load->helper('directory');
		//$this->load->helper('file');
		$this->load->model('expolaporanstok/mclass');
	}
	
	function index() {
		
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
			$data['form_title_detail_laporansok']	= $this->lang->line('form_title_detail_laporansok');
			$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
			$data['list_laporansok_kd_brg']			= $this->lang->line('list_laporansok_kd_brg');
			$data['list_laporansok_nm_brg']			= $this->lang->line('list_laporansok_nm_brg');
			$data['list_laporansok_awalstok_brg']	= $this->lang->line('list_laporansok_awalstok_brg');
			$data['list_laporansok_akhirstok_brg']	= $this->lang->line('list_laporansok_akhirstok_brg');
			$data['list_laporansok_ket_brg']		= $this->lang->line('list_laporansok_ket_brg');
			$data['button_batal']					= $this->lang->line('button_batal');
			$data['button_excel']					= $this->lang->line('button_excel');		
			$data['detail']							= "";
			$data['list']							= "";
			$data['limages']						= base_url();
	
			$blnarr	= array(
				"01"=>"Januari",
				"02"=>"Februari",
				"03"=>"Maret",
				"04"=>"April",
				"05"=>"Mei",
				"06"=>"Juni",
				"07"=>"Juli",
				"08"=>"Agustus",
				"09"=>"September",
				"10"=>"Oktober",
				"11"=>"Nopember",
				"12"=>"Desember");
			
			$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->uri->segment(4);
			$stp	= $this->input->post('stp')?$this->input->post('stp'):$this->uri->segment(5);
			$stp	= ($stp=='t')?('t'):('f');
			
			$uri1	= ($cari!='')?($cari):('kosong');
			$uri2	= ($this->uri->segment(5)=='')?($stp):($this->uri->segment(5));
			
			/*** $this->load->model('expolaporanstok/mclass'); ***/
			
			$qnomorso	= $this->mclass->statusSO($uri2);
			if($qnomorso->num_rows()>0){
				$rnomorso		= $qnomorso->row();
				$iso			= $rnomorso->i_so;
				$istatusso		=  $rnomorso->i_status_so; // hrs yang statusnya 0
				$stopproduct	= $rnomorso->f_stop_produksi; // true jika telah STP
				$dso			= explode("-",$rnomorso->d_so,strlen($rnomorso->d_so)); // Y-m-d
				$blso			= $dso[1];
				$data['blnso']	= $blnarr[$blso];
				$data['thnso']	= $dso[0];			
			}else{
			}
			$query	= $this->mclass->view($cari,$iso,$uri2);
			$jml	= $query->num_rows();
			
			$data['checked']	= ($uri2=='t')?(' checked '):('');
			$data['cari']		= ($cari!='' && $cari!='kosong')?($cari):('');
			$data['disabled']	= 'f';
			$data['uri1']	= $uri1;
			$data['uri2']	= $uri2;
			
			$thnawalbln	= date("Y");
			$blnawalbln	= date("m");
			$data['tglawalbln']	= $thnawalbln.'-'.$blnawalbln.'-'.'01';
			$data['tglakhirbln']	= date("Y-m-d");
			
			$pagination['base_url'] 	= '/expolaporanstok/cform/index/'.$uri1.'/'.$uri2.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(6,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page'],$cari,$iso,$uri2);
			
			/*** $this->load->model('expolaporanstok/mclass'); ***/
				$data['isi']	= 'expolaporanstok/vlistform';	
			$this->load->view('template',$data);	
			
		
	}
	
	function expolaporanstok() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
		$data['form_title_detail_laporansok']	= $this->lang->line('form_title_detail_laporansok');
		$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
		$data['list_laporansok_kd_brg']			= $this->lang->line('list_laporansok_kd_brg');
		$data['list_laporansok_nm_brg']			= $this->lang->line('list_laporansok_nm_brg');
		$data['list_laporansok_awalstok_brg']	= $this->lang->line('list_laporansok_awalstok_brg');
		$data['list_laporansok_akhirstok_brg']	= $this->lang->line('list_laporansok_akhirstok_brg');
		$data['list_laporansok_ket_brg']		= $this->lang->line('list_laporansok_ket_brg');
		$data['button_batal']					= $this->lang->line('button_batal');
		$data['button_excel']					= $this->lang->line('button_excel');		
		$data['detail']							= "";
		$data['list']							= "";
		$data['limages']						= base_url();

		/* remark 19052011
		$data['page_title_file_laporan']= $this->lang->line('page_title_file_laporan');
		$data['list_no_file_laporan']	= $this->lang->line('list_no_file_laporan');
		$data['list_nm_file_laporan']	= $this->lang->line('list_nm_file_laporan');	
		$data['link_aksi']				= $this->lang->line('link_aksi');		
		$map = directory_map('./files/', TRUE);		
		$data['directories']	= 'files';
		//hasil $map diatas adalah array nama filenya
		//Contoh: $map[0], $map[1]. Urutan filenya sesuai 
		//  abjad nama file. Gunakan $map ini untuk kode src 
		//  pada tag <image>
		//Sekarang buat validasi apakah user telah memilih 
		//  gambar tertentu melalui link Prev atau Next		
		$jmlpic = count($map); // itung banyaknya pic		
		if($jmlpic>0) {
			$no = $this->uri->segment(4,0);			
			if($no==0) { //jika belum ada pilihan 
			   $data['prev']=$jmlpic;
			   $data['next']=$no+1;
			   $data['filename'] =$map[$no];
			}
			if($no==$jmlpic) { //pilihan pic adalah file terakhir
			   $data['prev']	= $no-1;
			   $data['next']	= 0;
			   $data['filename']	= $map[$no];
			   $data['map']	= $map;
			} else {
			   $data['prev']	= $no-1;
			   $data['next']	= $no+1;
			   $data['filename']	= $map[$no];
			   $data['map']	= $map;
			}
		} else {
			$data['map']	= array();
		}			
		*/

		$tgl	= date("d");
		$bln	= date("m");
		$thn	= date("Y");
		
		$barr	= array(
			'01'=>'Januari',
			'02'=>'Februari',
			'03'=>'Maret',
			'04'=>'April',
			'05'=>'Mei',
			'06'=>'Juni',
			'07'=>'Juli',
			'08'=>'Agustus',
			'09'=>'Sepetember',
			'10'=>'Oktober',
			'11'=>'Nopember',
			'12'=>'Desember'
		);
		
		$tglzero	= substr($tgl,0,1)=='0'?substr($tgl,1,1):$tgl;
		$periode	= $tglzero." ".$barr[$bln]." ".$thn;
		$tglcreate	= $thn."-".$bln."-".$tgl;
				
		$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->uri->segment(4);
		$stp	= $this->input->post('stp')?$this->input->post('stp'):$this->uri->segment(5);
		$stp	= ($stp=='t')?('t'):('f');
		
		$uri1	= ($cari!='')?($cari):('kosong');
		$uri2	= ($this->uri->segment(5)=='')?($stp):($this->uri->segment(5));
		
		$categorytitle = $stp	= $stp=='t'?"BARANG STP":" BARANG NON STP";
		
		/*** $this->load->model('expolaporanstok/mclass'); ***/
		
		$qnomorso	= $this->mclass->statusSO($uri2);
		if($qnomorso->num_rows()>0){
			$rnomorso		= $qnomorso->row();
			$iso			= $rnomorso->i_so;
			$istatusso		=  $rnomorso->i_status_so; // hrs yang statusnya 0
			$stopproduct	= $rnomorso->f_stop_produksi; // true jika telah STP
			$dso			= explode("-",$rnomorso->d_so,strlen($rnomorso->d_so)); // Y-m-d
			$blso			= $dso[1];
			$data['blnso']	= $barr[$blso];
			$data['thnso']	= $dso[0];	
		}else{
		}
		
		$query	= $this->mclass->view($cari,$iso,$uri2);
		$jml	= $query->num_rows();
		
		$data['checked']	= ($uri2=='t')?(' checked '):('');
		$data['cari']		= ($cari!='' && $cari!='kosong')?($cari):('');
		$data['disabled']	= 't';

		$data['uri1']	= $uri1;
		$data['uri2']	= $uri2;
				
		$pagination['base_url'] 	= '/expolaporanstok/cform/index/'.$uri1.'/'.$uri2.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page'],$cari,$iso,$uri2);
		
		$qlapstok	= $this->mclass->expolaporanstok($cari,$iso,$uri2);
		
		$nomor2	= 1;
		$cel	= 9;
				
		if($qlapstok->num_rows()>0) {

			$j	= 0;
			$totalsaldo = 0;
			
			$bonmkeluar	= array();
			$bonmmasuk	= array();
			$bbk	= array();
			$bbm	= array();
			$do	= array();
			$sj = array();
			
			$x11	= array();
			$x22	= array();
			$z11	= array();
			$saldoakhirnya	= array();

			$thnawalbln	= date("Y");
			$blnawalbln	= date("m");
			$tglawalbln	= $thnawalbln.'-'.$blnawalbln.'-'.'01';
			$tglakhirbln	= date("Y-m-d");

			$data['tglawalbln']	= $tglawalbln;
			$data['tglakhirbln']	= $tglakhirbln;
								
			$ObjPHPExcel = new PHPExcel();
			
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Data Stok Barang")
				->setSubject("Stok Barang")
				->setDescription("Laporan Data Stok Barang")
				->setKeywords("Laporan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(7);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(6);			
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:V2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN DATA STOK '.$categorytitle);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:V3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:V4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A6:A8');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('B6:B8');
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Kode Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),						
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('C6:C8');
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Nama Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('D6:D8');
			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'Stok');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('E6:E8');
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Ket.');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('F6:Z6');
			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Stok Produksi');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('F7:H7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('F7', 'Wip Jahit');
			$ObjPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('I7:K7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('I7', 'Wip Jahit');
			$ObjPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('L7:N7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('L7', 'Wip Jahit');
			$ObjPHPExcel->getActiveSheet()->getStyle('L7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('O7:Q7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('O7', 'Wip Jahit');
			$ObjPHPExcel->getActiveSheet()->getStyle('O7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('R7:T7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('R7', 'Wip Jahit');
			$ObjPHPExcel->getActiveSheet()->getStyle('R7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('U7:W7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('U7', 'Wip Jahit');
			$ObjPHPExcel->getActiveSheet()->getStyle('U7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('X7:Z7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('X7', 'Wip Jahit');
			$ObjPHPExcel->getActiveSheet()->getStyle('X7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('AA6:AA8');
			$ObjPHPExcel->getActiveSheet()->setCellValue('AA6', '');
			$ObjPHPExcel->getActiveSheet()->getStyle('AA6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('F8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('G8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('H8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('I8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('J8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('K8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('L8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('M8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('N8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('O8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('P8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('Q8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('R8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('S8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('T8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('U8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('V8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),		
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('W8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('X8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('Y8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('Z8')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
							
			$ObjPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');		
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('F7:Z7')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('F8:Z8')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('AA6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
																												
			foreach($qlapstok->result() as $field2) {

				$qbonmkeluar	= $this->mclass->lbonkeluar($tglawalbln,$tglakhirbln,$field2->imotif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}
	
				$qbonmmasuk	= $this->mclass->lbonmasuk($tglawalbln,$tglakhirbln,$field2->imotif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk; 
				} else {
					$bonmmasuk[$j] = 0;
				}
	
				$qbbk	= $this->mclass->lbbk($tglawalbln,$tglakhirbln,$field2->imotif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk; 
				} else {
					$bbk[$j] = 0;
				}
	
				$qbbm	= $this->mclass->lbbm($tglawalbln,$tglakhirbln,$field2->imotif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm; 
				} else {
					$bbm[$j] = 0;
				}
	
				$qdo	= $this->mclass->ldo($tglawalbln,$tglakhirbln,$field2->imotif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}

				$qsj	= $this->mclass->lsj($tglawalbln,$tglakhirbln,$field2->imotif);
				if($qsj->num_rows()>0) {
					$row_sj	= $qsj->row();
					$sj[$j]	= $row_sj->jsj; 
				} else {
					$sj[$j] = 0;
				}
					
				$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
				$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
				$z11[$j]	= $field2->qtyawal+$x11[$j];
				$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
									
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$cel, $nomor2);
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
						
				$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$cel, $field2->imotif);
				$ObjPHPExcel->getActiveSheet()->getStyle('B'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$cel, $field2->eproductname);
				$ObjPHPExcel->getActiveSheet()->getStyle('C'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$cel, $saldoakhirnya[$j]);
				$ObjPHPExcel->getActiveSheet()->getStyle('D'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
							
				$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('F'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('G'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('H'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('I'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('J'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('K'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
																	
				$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('L'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('M'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('M'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				  
				$ObjPHPExcel->getActiveSheet()->setCellValue('N'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('N'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('O'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('O'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				  
				$ObjPHPExcel->getActiveSheet()->setCellValue('P'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('P'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('Q'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('Q'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('R'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('R'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('S'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('S'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('T'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('T'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('U'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('U'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('V'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('V'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('W'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('W'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('X'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('X'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('Y'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('Y'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('Z'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('Z'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('AA'.$cel, '');
				$ObjPHPExcel->getActiveSheet()->getStyle('AA'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				$totalsaldo = $totalsaldo+$saldoakhirnya[$j];
					
				$j++;																																		
				$nomor2++;	
				$cel++;
			}
			$cel2 = $cel+1;

			$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$cel2, 'TOTAL :');
			$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$cel2, $totalsaldo);						
			
			
			$ObjWriter	= IOFactory::createWriter($ObjPHPExcel, 'Excel5');	

			$stp	= $stp=='t'?'brgstp':'nonstp';
			$files	= $this->session->userdata('gid')."laporan_data_stok"."_".$stp."_".$tglcreate.".xls";
			$ObjWriter->save("files/".$files);
		}

		$efilename = @substr($files,1,strlen($files));
	
		$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
		
		$data['isi']		= 'expolaporanstok/vlistform';
			$this->load->view('template',$data);
		

		/* remark 19052011
		$this->load->view('directories/vform', $data);
		*/
	}
}
?>
