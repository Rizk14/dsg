<?php
class approve_makloon_baju_wip extends CI_Controller
{
    public $data = array(
        'halaman' => 'approve_makloon_baju_wip',
        'isi' => 'approve_makloon_baju_wip/approve_makloon_baju_wip_list',
        'title' => 'Data Approve Makloon Baju',
    );

     public function __construct()
    {
        parent::__construct();
        $this->load->model('approve_makloon_baju_wip/approve_makloon_baju_wip_model', 'approve_makloon_baju_wip');
    }
    
     public function view($offset = 0)
    {
        $approve_makloon_baju_wip = $this->approve_makloon_baju_wip->get_all_paged_custom($offset);
        if ($approve_makloon_baju_wip) {
            $this->data['approve_makloon_baju_wip'] = $approve_makloon_baju_wip;
            $this->data['paging'] = $this->approve_makloon_baju_wip->paging('biasa', site_url('approve_makloon_baju_wip/approve_makloon_baju_wip/halaman/'), 4);
        } else {
            $this->data['approve_makloon_baju_wip'] = 'Tidak ada data Approve Makloon Baju.';
        }
        $this->load->view('template', $this->data);
    }

    public function cari($offset = 0)
    {
        $approve_makloon_baju_wip = $this->approve_makloon_baju_wip->cari($offset);
        if ($approve_makloon_baju_wip) {
            $this->data['approve_makloon_baju_wip'] = $approve_makloon_baju_wip;
            $this->data['paging'] = $this->approve_makloon_baju_wip->paging('pencarian', site_url('approve_makloon_baju_wip/approve_makloon_baju_wip/cari/'), 4);
        } else {
            $this->data['approve_makloon_baju_wip'] = 'Data tidak ditemukan.'. anchor('approve_makloon_baju_wip/approve_makloon_baju_wip', ' Tampilkan semua approve_makloon_baju_wip.', 'class="alert-link"');
        }
        $this->load->view('template', $this->data);
    }

   
    public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data Approve Makloon Baju';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data Approve Makloon Baju';
        $this->load->view('template', $this->data);
    }

   

    // Ubah status verifikasi
    public function ubah_status_verifikasi($id)
    {
        // Pastikan data approve_makloon_baju_wip ada.
        $approve_makloon_baju_wip = $this->approve_makloon_baju_wip->get($id);
        if (! $approve_makloon_baju_wip) {
            $this->session->set_flashdata('pesan_error', 'Data Approve Makloon Baju tidak ada. Kembali ke halaman ' . anchor('approve_makloon_baju_wip/approve_makloon_baju_wip/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_wip/approve_makloon_baju_wip/error');
        }

        // Ubah status verifikasi
        if ($this->approve_makloon_baju_wip->ubah_status_approve($id, $approve_makloon_baju_wip->status_approve)) {
            $this->session->set_flashdata('pesan', 'Status verifikasi berhasil diubah. Kembali ke halaman ' . anchor('approve_makloon_baju_wip/approve_makloon_baju_wip/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_wip/approve_makloon_baju_wip/sukses');
        } else {
            $this->session->set_flashdata('pesan', 'Status verifikasi gagal diubah. Kembali ke halaman ' . anchor('approve_makloon_baju_wip/approve_makloon_baju_wip/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_wip/approve_makloon_baju_wip/error');
        }
    }

    
    public function link_detail()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $approve_makloon_baju_wip = $this->approve_makloon_baju_wip->getAllDetail($id);
        if (! $approve_makloon_baju_wip) {
            $this->session->set_flashdata('pesan_error', 'Data Detail Makloon Baju Gudang Jadi Wip tidak ada. Kembali ke halaman ' . anchor('approve_makloon_baju_wip/approve_makloon_baju_wip/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_wip/master-barang/error');
        }
		$this->data['list_unit_jahit'] = $this->approve_makloon_baju_wip->get_unit_jahit(); 
		$this->data['list_unit_packing'] = $this->approve_makloon_baju_wip->get_unit_packing(); 
		$this->data['list_gudang'] = $this->approve_makloon_baju_wip->get_gudang(); 
        $this->data['values'] = $approve_makloon_baju_wip;
		$this->data['isi'] = 'approve_makloon_baju_wip/approve_makloon_baju_wip_form_edit';
        $this->load->view('template', $this->data);
	 }

    
}
