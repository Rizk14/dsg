<?php

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	
		$this->load->model('prntdokelompok/mclass');
		$this->load->library('pagination_ori');
	}
	
	function index() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
			/*$data['form_nomor_do']	= $this->lang->line('form_nomor_do');
			$data['form_option_pel_do']	= $this->lang->line('form_option_pel_do');			
			$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');		
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['button_batal']	= $this->lang->line('button_batal');		
			$data['page_title_do']	= $this->lang->line('page_title_do');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['lpelanggan']	= ""; */
			
			$this->load->model('prntdokelompok/mclass');
			$data['isi']	='prntdokelompok/vmainform';
		$this->load->view('template',$data);
			
		
	}
	
	/*function listdetaildo() {
		
		$ibranch	= $this->uri->segment(4);
		
		//$data['ibranch']	= $ibranch;
		$data['page_title']	= "NOMOR DO";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('prntdokelompok/mclass');

		$data['isi']	= $this->mclass->ldodetail();
				
		$this->load->view('prntdokelompok/vlistdo',$data);			
	} */
	
	function showpopupdo(){
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '') {
			$keywordcari 	= $this->uri->segment(4);
		}
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
	// +++++++++++++++
		$qjum_total = $this->mclass->get_dotanpalimit($keywordcari);
		
				$config['base_url'] = base_url()."index.php/prntdokelompok/cform/showpopupdo/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination_ori->initialize($config);
	$data['query'] = $this->mclass->get_do($config['per_page'],$config['cur_page'], $keywordcari);
	$data['jum_total'] = count($qjum_total);
		
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('prntdokelompok/vpopupdo',$data);
  }
		
	function cetak() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
	$db2 = $this->load->database('db_external', TRUE);
			$nomordo		= $this->input->post('i_do_code', TRUE);  
			$nomordo = trim($nomordo);
			$listdo = explode(";", $nomordo); 
			$iduserid	= $this->session->userdata('user_idx');
		
			
			$host		= '192.168.0.173'; // IP address agus
			$uri		= '/printers/EPSON-LQ-2180yani'; // contoh aja
			$remote		= $_SERVER['REMOTE_ADDR'];
			$ldo		= '';
			$nowdate	= date('Y-m-d');
			$logfile	= 'logs'.'-'.$nowdate;	
			$this->load->model('prntdo_test/mclass');
			
			// ================================ 19-03-2014 =======================================================
			$doprint= array();
			foreach($listdo as $row1) {
				$row1= trim($row1);
				if ($row1!='') {
					$sql = " SELECT * FROM tm_do WHERE i_do_code='$row1' AND f_do_cancel='f' ";
					$query = $db2->query($sql);
					$datado = array();
					if ($query->num_rows() > 0){
						$hasilrow = $query->row();
						
						// 19-03-2014 kelompokin utk persiapan cetak
						// sementara yg ini dikomen
						//$this->mclass->fprinted($row1);
						
						$qtgl		= $this->mclass->ctgldobrg($row1);
						if($qtgl->num_rows() > 0 ) {
							$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
											'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
							
							$row_tgl	= $qtgl->row();
							$exp_tgl	= explode("-",$row_tgl->tgl,strlen($row_tgl->tgl));
							$f_tgl		= substr($exp_tgl[2],0,1)=='0'?substr($exp_tgl[2],1,1):$exp_tgl[2];
							$f_bln		= $bglobal[$exp_tgl[1]];
							$f_thn		= $exp_tgl[0];
							
							//$data['nomor']	= $kddo;
							$shtgl	= (string) html_entity_decode($f_tgl.'#'.$f_bln.'#'.$f_thn);
							
							$dotgl	= (string) html_entity_decode($f_tgl);
							$dobln	= (string) html_entity_decode($f_bln);
							$dothn	= (string) html_entity_decode($f_thn);
				
						} else {
							//$data['nomor']	= '';
							$shtgl	= '';		
						}
						
						$qcabang	= $this->mclass->getcabang($row1);
						if($qcabang->num_rows() > 0 ) {
							$row_cabang	= $qcabang->row_array();
							$nmcabang	= $row_cabang['cabangname'];
						} else {
							$nmcabang	= '';
						}
						
						$qnmrop		= $this->mclass->cnmrop($row1);
						if($qnmrop->num_rows() > 0) {
							$row_nmrop	= $qnmrop->row();
							$nomorop = $row_nmrop->iopcode;
							//$result_nmrop	= $qnmrop->result();
							//foreach($result_nmrop as $row_nmrop) {
								//$nomorop .= "Nomor OP : <b>".$row_nmrop->iopcode."</b>"."<br>";
							//}
						} else {
							$nomorop = "";
						}
						
						$qinitial	= $this->mclass->getinitial();
						if($qinitial->num_rows()>0) {
							$row_initial	= $qinitial->row_array();
							$nminitial	= $row_initial['e_initial_name'];
						} else {
							$nminitial	= "";
						}
						
						$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli', 
										'08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
						$tgl	= date("d");
						$bln	= date("m");						
						$thn	= date("Y");
						$new_bln= $bglobal[$bln];
						$tglprint	= $tgl."-".$new_bln."-".$thn;
						
						$detaildo = $this->mclass->clistdobrg($row1);
						
						$doprint[] = array( 'i_do_code'=> $row1,
										'shtgl'=> $shtgl,
										'dotgl'=> $dotgl,
										'dobln'=> $dobln,
										'dothn'=> $dothn,
										'nmcabang'=> $nmcabang,
										'nminitial'=> $nminitial,
										'nomorop'=> $nomorop,
										'tglprint'=> $tglprint,
										'detaildo'=> $detaildo
									);
									
					} // end if query > 0
					
				} // end if row1!=''
			}
			// ===================================================================================================
			
			//$qry_source_remote	= $this->mclass->remote($remote);
			$qry_source_remote	= $this->mclass->remote($iduserid, $remote);
			if($qry_source_remote->num_rows()>0) {
				$row_source_remote	= $qry_source_remote->row();
				$source_printer_name= $row_source_remote->e_printer_name;
				$source_ip_remote	= $row_source_remote->ip;
				$source_uri_remote	= $row_source_remote->e_uri;
			} else {
				$source_printer_name= "Default Printer";
				$source_ip_remote	= $host;
				$source_uri_remote	= $uri;
			}
			
			$data['printer_name']	= $source_printer_name;
			$data['host']		= $source_ip_remote;
			$data['uri']		= $source_uri_remote;
			$data['log_destination']= 'logs/'.$logfile;
			$data['doheader']	= $doprint;
			$data['nomordo']	= $nomordo;
					
			$this->load->view('prntdokelompok/vcetakdo',$data);
			print "<script>alert(\"Proses mencetak DO sudah selesai.\");window.open(\"index\", \"_self\");</script>";
		} // end if cek login
	
	
}
?>
