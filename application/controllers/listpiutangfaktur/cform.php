<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_piutang']			= $this->lang->line('page_title_piutang');
			$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
			$data['list_piutang_no_faktur']	= $this->lang->line('list_piutang_no_faktur');
			$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
			$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
			$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
			$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
			$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
			$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
			$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
			$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
			$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
			$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
			$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
			$data['list_piutang_nilai_faktur'] = $this->lang->line('list_piutang_nilai_faktur');
			$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
			$data['list_faktur_nota_sederhana'] = $this->lang->line('list_faktur_nota_sederhana');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi']	= 'listpiutangfaktur/vmainform';	
			$this->load->view('template',$data);	
		
			
	}
	
	function carilistfaktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_piutang']		= $this->lang->line('page_title_piutang');
		$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
		$data['list_piutang_no_faktur']	= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
		$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
		$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
		$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_nota_sederhana']	= $this->lang->line('list_nota_sederhana');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpiutang']	= "";		
		$data['limages']	= base_url();
		
		$no_faktur	= $this->input->post('no_faktur');
		$i_faktur	= $this->input->post('i_faktur');
		$dfakturfirst= $this->input->post('d_faktur_first');
		$dfakturlast	= $this->input->post('d_faktur_last');
		
		$tf_nota_sederhana	= $this->input->post('tf_nota_sederhana');
		
		$data['tglfakturmulai']	= (!empty($dfakturfirst) && $dfakturfirst!='0')?$dfakturfirst:'';
		$data['tglfakturakhir']	= (!empty($dfakturlast) && $dfakturlast!='0')?$dfakturlast:'';
		$data['nofaktur']		= (!empty($no_faktur) && $no_faktur!='0')?$no_faktur:'';
		$data['ifaktur']		= (!empty($i_faktur) && $i_faktur!='0')?$i_faktur:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
				
		$e_d_faktur_first= explode("/",$dfakturfirst,strlen($dfakturfirst));
		$e_d_faktur_last	= explode("/",$dfakturlast,strlen($dfakturlast));
		
		$ndfakturfirst	= !empty($dfakturfirst)?$e_d_faktur_first[2].'-'.$e_d_faktur_first[1].'-'.$e_d_faktur_first[0]:'0';
		$ndfakturlast	= !empty($dfakturlast)?$e_d_faktur_last[2].'-'.$e_d_faktur_last[1].'-'.$e_d_faktur_last[0]:'0';
		
		$turi1	= ($no_faktur!='' && $no_faktur!='0')?$no_faktur:'0';
		$turi2	= ($i_faktur!='' && $i_faktur!='0')?$i_faktur:'0';
		$turi3	= $ndfakturfirst;
		$turi4	= $ndfakturlast;
		$turi5	= $tf_nota_sederhana;
		
		$this->load->model('listpiutangfaktur/mclass');
		
		$pagination['base_url'] 	= 'listpiutangfaktur/cform/carilistfakturnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if($dfakturfirst!='' && $dfakturlast!='') {
			$qlistfakturallpage	= $this->mclass->clistfakturallpage1($i_faktur,$ndfakturfirst,$ndfakturlast,$tf_nota_sederhana);
			$data['template'] = 1;
		}else{
			$qlistfakturallpage	= $this->mclass->clistfakturallpage2($i_faktur,$ndfakturfirst,$ndfakturlast,$tf_nota_sederhana);
			$data['template'] = 2;
		}
		
		$pagination['total_rows']	= $qlistfakturallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dfakturfirst!='' && $dfakturlast!='') {
			$data['query']	= $this->mclass->clistfaktur1($pagination['per_page'],$pagination['cur_page'],$i_faktur,$ndfakturfirst,$ndfakturlast,$tf_nota_sederhana);
		}else{
			$data['query']	= $this->mclass->clistfaktur2($pagination['per_page'],$pagination['cur_page'],$i_faktur,$ndfakturfirst,$ndfakturlast,$tf_nota_sederhana);
		}
		
		$jmltotalfaktur = 0;
		$qvtotalfaktur	= $this->mclass->vtotalfaktur($pagination['per_page'],$pagination['cur_page'],$i_faktur,$ndfakturfirst,$ndfakturlast,$tf_nota_sederhana);

		if(sizeof($qvtotalfaktur)) {
			foreach($qvtotalfaktur as $vtotalfaktur) {
				$jmltotalfaktur	= $jmltotalfaktur+($vtotalfaktur->v_total_fppn);
			}
			$data['totalfaktur2'] = $jmltotalfaktur;
		}else{
			$data['totalfaktur2'] = array();
		}
		$data['isi']	= 'listpiutangfaktur/vlistform';	
			$this->load->view('template',$data);	
		
	}

	function carilistfakturnext() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_piutang']		= $this->lang->line('page_title_piutang');
		$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
		$data['list_piutang_no_faktur']	= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
		$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
		$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
		$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_faktur_nota_sederhana']	= $this->lang->line('list_faktur_nota_sederhana');
		$data['list_nota_sederhana']	= $this->lang->line('list_nota_sederhana');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpiutang']	= "";		
		$data['limages']	= base_url();

		$no_faktur	= $this->uri->segment(4);
		$i_faktur	= $this->uri->segment(5);
		$dfakturfirst= $this->uri->segment(6);
		$dfakturlast	= $this->uri->segment(7);
		$tf_nota_sederhana = $this->uri->segment(8); 
		
		$e_d_faktur_first = ($dfakturfirst!='0')?explode("-",$dfakturfirst,strlen($dfakturfirst)):'';
		$e_d_faktur_last	 = ($dfakturlast!='0')?explode("-",$dfakturlast,strlen($dfakturlast)):'';

		$ndfakturfirst	= $dfakturfirst!='0'?$e_d_faktur_first[2].'/'.$e_d_faktur_first[1].'/'.$e_d_faktur_first[0]:'0';
		$ndfakturlast	= $dfakturlast!='0'?$e_d_faktur_last[2].'/'.$e_d_faktur_last[1].'/'.$e_d_faktur_last[0]:'0';
						
		$data['tglfakturmulai']	= (!empty($ndfakturfirst) && $ndfakturfirst!='0')?$ndfakturfirst:'';
		$data['tglfakturakhir']	= (!empty($ndfakturlast) && $ndfakturlast!='0')?$ndfakturlast:'';
		$data['nofaktur']	= (!empty($no_faktur) && $no_faktur!='0')?$no_faktur:'';
		$data['ifaktur']		= (!empty($i_faktur) && $i_faktur!='0')?$i_faktur:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
				
		$turi1	= ($no_faktur!='' && $no_faktur!='0')?$no_faktur:'0';
		$turi2	= ($i_faktur!='' && $i_faktur!='0')?$i_faktur:'0';
		$turi3	= $dfakturfirst;
		$turi4	= $dfakturlast;
		$turi5	= $tf_nota_sederhana;
		
		$this->load->model('listpiutangfaktur/mclass');
		
		$pagination['base_url'] 	= '/listpiutangfaktur/cform/carilistfakturnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if($dfakturfirst!='0' && $dfakturlast!='0') {
			$qlistfakturallpage	= $this->mclass->clistfakturallpage1($i_faktur,$dfakturfirst,$dfakturlast,$tf_nota_sederhana);
		}else{
			$qlistfakturallpage	= $this->mclass->clistfakturallpage2($i_faktur,$dfakturfirst,$dfakturlast,$tf_nota_sederhana);
		}
		
		$pagination['total_rows']	= $qlistfakturallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dfakturfirst!='0' && $dfakturlast!='0') {
			$data['isi']	= $this->mclass->clistfaktur1($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$tf_nota_sederhana);
			$data['template'] = 1;
		}else{
			$data['isi']	= $this->mclass->clistfaktur2($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$tf_nota_sederhana);
			$data['template'] = 2;
		}
		
		$jmltotalfaktur = 0;
		$qvtotalfaktur	= $this->mclass->vtotalfaktur($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$tf_nota_sederhana);
		if(sizeof($qvtotalfaktur)) {
			foreach($qvtotalfaktur as $vtotalfaktur) {
				$jmltotalfaktur	= $jmltotalfaktur+($vtotalfaktur->v_total_fppn);
			}
			$data['totalfaktur2'] = $jmltotalfaktur;
		}else{
			$data['totalfaktur2'] = array();
		}
		//~ foreach($qvtotalfaktur as $vtotalfaktur) {
			//~ $jmltotalfaktur	= $jmltotalfaktur+($vtotalfaktur->v_total_fppn);
		//~ }
		$data['totalfaktur2'] = $jmltotalfaktur;

		$this->load->view('listpiutangfaktur/vlistform',$data);
					
	}
	
	function listfaktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana	= $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		$this->load->model('listpiutangfaktur/mclass');

		$query	= $this->mclass->lfaktur($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpiutangfaktur/cform/listfakturnext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);		
				
		$this->load->view('listpiutangfaktur/vlistfaktur',$data);			
	}

	function listfakturnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana	= $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		$this->load->model('listpiutangfaktur/mclass');

		$query	= $this->mclass->lfaktur($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpiutangfaktur/cform/listfakturnext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);		
				
		$this->load->view('listpiutangfaktur/vlistfaktur',$data);			
	}		

	function flistfaktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$fnotasederhana = $this->input->post('fnotasederhana')?$this->input->post('fnotasederhana'):$this->input->get_post('fnotasederhana');
		
		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('listpiutangfaktur/mclass');

		$query	= $this->mclass->flfaktur($key,$fnotasederhana);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_faktur_code','$row->i_faktur')\">".$row->i_faktur_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_faktur_code','$row->i_faktur')\">".$row->d_faktur."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}

}
?>
