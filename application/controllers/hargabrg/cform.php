<?php

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
			$data['page_title']	= "HARGA BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
		
			$this->load->model('hargabrg/mclass');

			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
		
			$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']= $this->pagination->create_links();
		
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
		
			$qry_price	= $this->mclass->pricecode();
			if($qry_price->num_rows() > 0) {
				$row_price	= $qry_price->row();

				$icodeprice0	= $row_price->i_product_price; // H00001
				$icodeprice_angka	= substr($icodeprice0,1,strlen($icodeprice0)-1); // 00001
				$icodeprice_jml	= $icodeprice_angka + 1;

				switch(strlen($icodeprice_jml)) {
					case 1:
						$icode	= 'H'.'0000'.$icodeprice_jml;
					break;
					case 2:
						$icode	= 'H'.'000'.$icodeprice_jml;
					break;
					case 3:
						$icode	= 'H'.'00'.$icodeprice_jml;
					break;
					case 4:
						$icode	= 'H'.'0'.$icodeprice_jml;
					break;
					default:
						$icode	= 'H'.$icodeprice_jml;
				}
				
				$icodeprice	= $icode;
								
				$pricecode	= $row_price->pricecode;
			} else {
				$icodeprice	= "H00001";
				$pricecode	= 1;
			}
					
			$data['pricecode']	= $pricecode;
			$data['priceicode']	= $icodeprice;
			
			$data['query']		= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);

			$data['isi']		= 'hargabrg/vmainform';
			$this->load->view('template',$data);
	}

function tambah() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
			$data['page_title']	= "HARGA BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
		
			$this->load->model('hargabrg/mclass');

			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
		
			$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']= $this->pagination->create_links();
		
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
		
			$qry_price	= $this->mclass->pricecode();
			if($qry_price->num_rows() > 0) {
				$row_price	= $qry_price->row();

				$icodeprice0	= $row_price->i_product_price; // H00001
				$icodeprice_angka	= substr($icodeprice0,1,strlen($icodeprice0)-1); // 00001
				$icodeprice_jml	= $icodeprice_angka + 1;

				switch(strlen($icodeprice_jml)) {
					case 1:
						$icode	= 'H'.'0000'.$icodeprice_jml;
					break;
					case 2:
						$icode	= 'H'.'000'.$icodeprice_jml;
					break;
					case 3:
						$icode	= 'H'.'00'.$icodeprice_jml;
					break;
					case 4:
						$icode	= 'H'.'0'.$icodeprice_jml;
					break;
					default:
						$icode	= 'H'.$icodeprice_jml;
				}
				
				$icodeprice	= $icode;
								
				$pricecode	= $row_price->pricecode;
			} else {
				$icodeprice	= "H00001";
				$pricecode	= 1;
			}
					
			$data['pricecode']	= $pricecode;
			$data['priceicode']	= $icodeprice;
			
			$data['query']		= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);

			$data['isi']		= 'hargabrg/vmainformadd';
			$this->load->view('template',$data);
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
			$data['page_title']	= "HARGA BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
		
			$this->load->model('hargabrg/mclass');

			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
		
			$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']= $this->pagination->create_links();
		
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
		
			$qry_price	= $this->mclass->pricecode();
			if($qry_price->num_rows() > 0) {
				$row_price	= $qry_price->row();

				$icodeprice0	= $row_price->i_product_price; // H00001
				$icodeprice_angka	= substr($icodeprice0,1,strlen($icodeprice0)-1); // 00001
				$icodeprice_jml	= $icodeprice_angka + 1;

				switch(strlen($icodeprice_jml)) {
					case 1:
						$icode	= 'H'.'0000'.$icodeprice_jml;
					break;
					case 2:
						$icode	= 'H'.'000'.$icodeprice_jml;
					break;
					case 3:
						$icode	= 'H'.'00'.$icodeprice_jml;
					break;
					case 4:
						$icode	= 'H'.'0'.$icodeprice_jml;
					break;
					default:
						$icode	= 'H'.$icodeprice_jml;
				}
				$icodeprice	= $icode;
								
				$pricecode	= $row_price->pricecode;
			} else {
				$icodeprice	= "H00001";
				$pricecode	= 1;
			}
					
			$data['pricecode']	= $pricecode;
			$data['priceicode']	= $icodeprice;
			
			$data['query']		= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);

				$data['isi']		= 'hargabrg/vmainform';
			$this->load->view('template',$data);
		}
	
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$data['isi']		= 'hargabrg/vmainform';
			$this->load->view('template',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproductprice	= $this->input->post('iproductprice');
		$iproductprice2	= $this->input->post('iproductprice2');
		$i_customer		= $this->input->post('i_customer');
		$iproduct		= $this->input->post('iproduct');
		$iproductmotif	= $this->input->post('iproductmotif');
		$vprice			= $this->input->post('vprice');
		
		$this->load->model('hargabrg/mclass');
		
		if( !empty($iproductprice) && 
		    !empty($iproduct) &&
			!empty($iproductprice2) &&
			!empty($iproductmotif) &&
			!empty($vprice) ) {

				if($i_customer!=0) {
					
					$cek_harga	= $this->mclass->cek_harga($i_customer,$iproductmotif);
					if($cek_harga->num_rows()==0){
						$this->mclass->msimpan($iproductprice,$iproductprice2,$i_customer,$iproduct,$iproductmotif,$vprice);
					}else{
						//print "<script>alert(\"Maaf, Pelanggan dgn Barang tsb sudah ada!\");show(\"hargabrg/cform\",\"#content\");</script>";
						print "<script>alert(\"Maaf, Harga Barang tsb sudah ada!\");window.open(\"tambah\", \"_self\");</script>";
					}
				} else {
					print "<script>alert(\"Maaf, Nama Pelanggan hrs dipilih!\");window.open(\"tambah\", \"_self\");</script>";
				}

		}else{
			
			$data['page_title']	= "HARGA BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih.";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";

			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
		
			$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']= $this->pagination->create_links();
		
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
		
			$qry_price	= $this->mclass->pricecode();
			if($qry_price->num_rows() > 0) {
				$row_price	= $qry_price->row();

				$icodeprice0	= $row_price->i_product_price; // H00001
				$icodeprice_angka	= substr($icodeprice0,1,strlen($icodeprice0)-1); // 00001
				$icodeprice_jml	= $icodeprice_angka + 1;

				switch(strlen($icodeprice_jml)) {
					case "1":
						$icode	= 'H'.'0000'.$icodeprice_jml;
					break;
					case "2":
						$icode	= 'H'.'000'.$icodeprice_jml;
					break;
					case "3":
						$icode	= 'H'.'00'.$icodeprice_jml;
					break;
					case "4":
						$icode	= 'H'.'0'.$icodeprice_jml;
					break;
					default:
						$icode	= 'H'.$icodeprice_jml;
				}
				$icodeprice	= $icode;
								
				$pricecode	= $row_price->pricecode;
			} else {
				$icodeprice	= "H00001";
				$pricecode	= 1;
			}
					
			$data['pricecode']	= $pricecode;
			$data['priceicode']	= $icodeprice;
			
			$data['query']		= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			
				$data['isi']		= 'hargabrg/vmainform';
			$this->load->view('template',$data);
		}
	}

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$icustomer	= $this->uri->segment(4);
			
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('hargabrg/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/listbarangjadinext/'.$icustomer.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('hargabrg/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$icustomer	= $this->uri->segment(4);
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('hargabrg/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/listbarangjadinext/'.$icustomer.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		
			$data['isi']		= 'hargabrg/vmainform';
			$this->load->view('template',$data);
	}	

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');

		$this->load->model('hargabrg/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->flbarangjadi($key);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 			
			foreach($query->result() as $row){
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:setTextfield('$row->iproduct','$row->iproductmotif','$row->v_price')\">".$row->iproduct."</a></td>	 
				  <td><a href=\"javascript:setTextfield('$row->iproduct','$row->iproductmotif','$row->v_price')\">".$row->iproductmotif."</a></td>
				  <td><a href=\"javascript:setTextfield('$row->iproduct','$row->iproductmotif','$row->v_price')\">".$row->productmotif."</a></td>
				  <td><a href=\"javascript:setTextfield('$row->iproduct','$row->iproductmotif','$row->v_price')\">".$row->v_price."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_codeharga	= $this->input->post('txt_codeharga')?$this->input->post('txt_codeharga'):$this->input->get_post('txt_codeharga');
		
		$data['page_title']	= "HARGA BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('hargabrg/mclass');

		$query	= $this->mclass->viewcari($txt_codeharga);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']= $this->pagination->create_links();
		
		$data['query']		= $this->mclass->mcari($txt_codeharga,$pagination['per_page'],$pagination['cur_page']);
		
			$data['isi']		= 'hargabrg/vmainform';
			$this->load->view('template',$data);
	}

	function carinext() {
		
		$txt_codeharga	= $this->input->post('txt_codeharga')?$this->input->post('txt_codeharga'):$this->input->get_post('txt_codeharga');

		$data['page_title']	= "HARGA BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('hargabrg/mclass');

		$query	= $this->mclass->viewcari($txt_codeharga);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']= $this->pagination->create_links();
		
		$data['isi']		= $this->mclass->mcari($txt_codeharga,$pagination['per_page'],$pagination['cur_page']);

			$data['isi']		= 'hargabrg/vmainform';
			$this->load->view('template',$data);
	}
	
	function actdelete() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->uri->segment(4);
		
		$this->db->delete('tr_product_price',array('i_price'=>$id));		
		
			$data['isi']		= 'hargabrg/vmainform';
			$this->load->view('template',$data);
	}

	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		
		$data['id']	= $id;
		$data['page_title']	= "HARGA BARANG";
		$limages			= base_url();
		$data['list']		= "";
		$data['lpelanggan']	= "";
		
		$this->load->model('hargabrg/mclass');
		$qry_hargabrg		= $this->mclass->medit($id);
		
		$data['opt_pelanggan']	= $this->mclass->lpelanggan();
		
		if($qry_hargabrg->num_rows()>0) {
			
			$row_hargabrg	= $qry_hargabrg->row();
			
			$data['iprice']			= $id;
			$data['iproductprice']	= $row_hargabrg->i_product_price;
			$data['i_product']		= $row_hargabrg->i_product;
			$data['i_product_motif']= $row_hargabrg->i_product_motif;
			$data['v_price']		= $row_hargabrg->v_price;
			$data['i_customer']		= $row_hargabrg->i_customer;
			$data['f_active']		= $row_hargabrg->f_active;
		}else{
			$data['iprice'] 		= "";
			$data['iproductprice'] 	= "";
			$data['i_product']		= "";
			$data['i_product_motif']= "";
			$data['v_price']		= "";
			$data['i_customer']		= "";
			$data['f_active']		= "";
		}
		
		$data['isi']='hargabrg/veditform';	
		$this->load->view('template',$data);	
	}
		
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproductprice	= $this->input->post('iproductprice');
		$iproductprice2	= $this->input->post('iproductprice2');
		$iproduct		= $this->input->post('iproduct');
		$iproductmotif	= $this->input->post('iproductmotif');
		$vprice			= $this->input->post('vprice');
		$vpricehidden	= $this->input->post('vpricehidden');
		$iprice_hidden	= $this->input->post('iprice_hidden');
		$iproductprice_hidden	= $this->input->post('iproductprice_hidden');
		$i_customer		= $this->input->post('i_customer');
		$i_customer_hidden	= $this->input->post('i_customer_hidden');
		$f_active_hidden	= $this->input->post('f_active_hidden');
		$iproductmotifhidden	= $this->input->post('iproductmotifhidden');
		
		$this->load->model('hargabrg/mclass');
		
		if(!empty($iproductprice) && 
		    !empty($iproduct) &&
			!empty($iproductmotif) &&
			!empty($vprice)) {
				$this->mclass->mupdate($iproductprice,$iproductprice2,$i_customer,$i_customer_hidden,$iproduct,$iproductmotif,$vprice,$iproductprice_hidden,$iprice_hidden,$f_active_hidden,$vpricehidden,$iproductmotifhidden);
		}else{
			$data['page_title']	= "HARGA BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
	
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= base_url().'/index.php/hargabrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']= $this->pagination->create_links();
			
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			$qry_price	= $this->mclass->pricecode();
			if($qry_price->num_rows() > 0) {
				$row_price	= $qry_price->row();
				$pricecode	= $row_price->pricecode;
			}else{
				$pricecode	= 1;
			}
			
			$data['pricecode']	= $pricecode;
			$data['isi']		= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			
			$this->load->view('hargabrg/vmainform',$data);
		}
		
	}
	
	function cari_kode() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$nkode	= $this->input->post('nkode')?$this->input->post('nkode'):$this->input->get_post('nkode');
		
		$this->load->model('hargabrg/mclass');
		
		$qnkode	= $this->mclass->cari_kode($nkode);
		
		if($qnkode->num_rows()>0) {
			echo "Maaf, Kode tdk dpt diubah!";
		}		
	}
}

?>
