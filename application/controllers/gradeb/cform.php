<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('gradeb/mmaster');
  }

   function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'gradeb/vformviewgradeb';
    $keywordcari = "all";

    $jum_total = $this->mmaster->getAllgradebtanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/gradeb/cform/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllgradeb($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('template',$data);
  }
  function addgradeb(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$data['query'] = $this->mmaster->get_hpp_gradeb_pengadaan();
	$data['query'] = '';
	if (is_array($data['query']) )
		$data['jum_data'] = count($data['query'])+1;
	else
		$data['jum_data'] = 2;
	$data['isi'] = 'gradeb/vmainformgradeb';
	$this->load->view('template',$data);
  }
  function caribrgjadi(){
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel brg jadi utk ambil kode, nama, dan stok
		$queryxx = $this->db->query(" SELECT id,nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_jadi."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$nama_brg_jadi = $hasilxx->nama_brg;
			$id_brg_jadi = $hasilxx->id;
		}
		else{
			$nama_brg_jadi = '';
			$id_brg_jadi='';
		}
		//echo $nama_brg_jadi; die();
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['id_brg_jadi'] = $id_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('gradeb/vinfobrgjadi', $data); 
		return true;
  }
  
   function submitgradeb(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$jum_data = $this->input->post('jum_data', TRUE)-1;

		for ($i=1;$i<=$jum_data;$i++)
		{
			// cek apakah datanya udh ada
			$cek_data = $this->mmaster->cek_data_gradeb($this->input->post('kode_brg_jadi_'.$i, TRUE));
			if (count($cek_data) == 0) { 
				$this->mmaster->savegradeb($this->input->post('kode_brg_jadi_'.$i, TRUE), 
							$this->input->post('harga_'.$i, TRUE),$this->input->post('id_brg_jadi_'.$i, TRUE),$this->input->post('bulan_'.$i, TRUE),$this->input->post('tahun_'.$i, TRUE));
			}
			else {
				if ($this->input->post('harga_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0)
					$this->db->query(" UPDATE tm_gradeb SET harga = '".$this->input->post('harga_'.$i, TRUE)."',id_barang_wip = '".$this->input->post('id_brg_jadi_'.$i, TRUE)."'
							WHERE kode_brg_wip = '".$this->input->post('kode_'.$i, TRUE)."' ");
			}
			
		}
		redirect('gradeb/cform');
		
  }
   function editgradeb() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $idharga 	= $this->uri->segment(4);  
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($idharga == '')
		$idharga 	= $this->input->post('idharga', TRUE);
    
    if ($go_edit == '') {
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
		
		$data['query'] = $this->mmaster->get_gradeb($idharga);
		$data['isi'] = 'gradeb/veditformgradeb';
		$data['idharga'] = $idharga;
		$this->load->view('template',$data);
	}
	else { // update data harga
		$harga 	= $this->input->post('harga', TRUE);
		$harga_lama 	= $this->input->post('harga_lama', TRUE);
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		
		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		
		$tgl = date("Y-m-d H:i:s");
		
		if ($harga != $harga_lama) {
			$this->db->query(" UPDATE tm_gradeb SET harga = '$harga', tgl_update = '$tgl' WHERE id = '$idharga' ");    
		}
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "gradeb/cform/index/".$cur_page;
		else
			$url_redirectnya = "gradeb/cform/carigradeb/index/".$carinya."/".$cur_page;
		
		redirect($url_redirectnya);
	}
  }
   function deletegradeb(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $idharga 	= $this->uri->segment(4);
    $this->mmaster->deletegradeb($idharga);
    redirect('gradeb/cform/');
  }
 }
