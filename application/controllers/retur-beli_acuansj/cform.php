<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('retur-beli/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	//$id_faktur = $this->input->post('id_faktur', TRUE);  
	//$no_faktur = $this->input->post('no_faktur', TRUE);  // ini bisa banyak
	$proses_submit = $this->input->post('submit', TRUE); 
	$kode_supplier = $this->input->post('kode_supplier', TRUE);  
	$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
	$date_from = $this->input->post('date_from', TRUE);  
	$date_to = $this->input->post('date_to', TRUE);  
	$is_luar_range = $this->input->post('is_luar_range', TRUE);  
	
	//$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
		if ($date_from !='' || $date_to != '') {
			// cek di apply stok, jika sudah diapply, maka boleh retur
			//$cek = $this->mmaster->cek_apply_stok($no_faktur, $kode_supplier);
			
			$data['brg_detail'] = $this->mmaster->get_detail_brg($date_from, $date_to, $kode_supplier, $jenis_pembelian);
			$data['msg'] = '';
			//$data['id_faktur'] = $id_faktur;
			
			//$data['no_faktur'] = $no_faktur;
			$data['date_from'] = $date_from;
			$data['date_to'] = $date_to;
			
			//$data['supplier'] = $this->mmaster->get_supplier();
			
			// generate no DN retur
			$th_now	= date("y");
			$bln_now = date("m");
			$romawi_now =  $this->mmaster->konversi_angka2romawi($bln_now);
			$query3	= $this->db->query(" SELECT no_dn_retur FROM tm_retur_beli ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_dn	= $hasilrow->no_dn_retur;
			else
				$no_dn = '';
			if($no_dn!='') {
				$pisah1 = explode("/", $no_dn);
				$nomornya= $pisah1[0];
				$bulannya= $pisah1[1];
				$tahunnya= $pisah1[2];
				//
				$nodn_angka = (substr($nomornya, 2, 4))+1;
				if ($th_now == $tahunnya) {
					$jml_n_dn = $nodn_angka;
					switch(strlen($jml_n_dn)) {
						case "1": $kodedn	= "000".$jml_n_dn;
						break;
						case "2": $kodedn	= "00".$jml_n_dn;
						break;	
						case "3": $kodedn	= "0".$jml_n_dn;
						break;
						case "4": $kodedn	= $jml_n_dn;
						break;
					}
					$nomordn = $kodedn;
				}
				else {
					$nomordn = "0001";
				}
				// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			}
			else {
				$nomordn	= "0001";
			}
			$nomordn = "Dn".$nomordn."/".$romawi_now."/".$th_now;
			$data['nomordn'] = $nomordn;
		}
		else {
			$data['msg'] = 'Range tanggal faktur harus dipilih';
			//$data['id_faktur'] = '';
			
			//$data['no_faktur'] = '';
			$data['date_from'] = '';
			$data['date_to'] = '';
			$data['supplier'] = $this->mmaster->get_supplier();
			$data['nomordn'] = '';
		}
		$data['go_proses'] = '1';
		
		$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$kode_supplier' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) 
			$nama_supplier	= $hasilrow->nama;
				
		$data['nama_supplier'] = $nama_supplier;
		$data['kode_supplier'] = $kode_supplier;
		$data['is_luar_range'] = $is_luar_range;
		$data['jenis_pembelian'] = $jenis_pembelian;
	}
	else {
		$data['msg'] = '';
		//$data['id_faktur'] = '';
		$data['go_proses'] = '';
		$data['supplier'] = $this->mmaster->get_supplier();
	}
	$data['isi'] = 'retur-beli/vmainform';
	$this->load->view('template',$data);

  }
  
  function edit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_retur 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$csupplier 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
	
	$data['query'] = $this->mmaster->get_retur($id_retur);
	$data['msg'] = '';
	$data['id_retur'] = $id_retur;
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['csupplier'] = $csupplier;
	$data['carinya'] = $carinya;

	$data['isi'] = 'retur-beli/veditform';
	$this->load->view('template',$data);

  }
    
  function show_popup_faktur(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	// faktur pembelian yg sudah ada faktur pajaknya
	
	$csupplier 	= $this->uri->segment(4);
	if ($csupplier == '')
		$csupplier 	= $this->input->post('csupplier', TRUE);  
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
	if ($csupplier != '') {
		// cek apakah PKP atau non PKP
		$query3	= $this->db->query(" SELECT pkp FROM tm_supplier WHERE kode_supplier = '$csupplier' ");
		if ($query3->num_rows() == 0){
			$pkp = 'f';
		}
		else {
			$hasilrow = $query3->row();
			$pkp	= $hasilrow->pkp;
		}
	}
		
	$jum_total = $this->mmaster->get_fakturtanpalimit($csupplier, $keywordcari, $pkp);
						/*	$config['base_url'] = base_url()."index.php/retur-beli/cform/show_popup_faktur/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		 */
	//$data['query'] = $this->mmaster->get_faktur($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['query'] = $this->mmaster->get_faktur($csupplier, $keywordcari, $pkp);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['csupplier'] = $csupplier;

	$this->load->view('retur-beli/vpopupfaktur',$data);

  }

  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			//$no_retur 	= $this->input->post('no_retur', TRUE);
			$tgl_retur 	= $this->input->post('tgl_retur', TRUE);  
		//	$no_sj_keluar 	= $this->input->post('no_sj_keluar', TRUE);
			$pisah1 = explode("-", $tgl_retur);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_retur = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$no_dn 	= $this->input->post('no_dn', TRUE);
			$id_retur 	= $this->input->post('id_retur', TRUE);
			$kode_supplier = $this->input->post('kode_supplier', TRUE);  
			$ket = $this->input->post('ket', TRUE);  
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
			$date_from 	= $this->input->post('date_from', TRUE);
			$date_to 	= $this->input->post('date_to', TRUE);
			$is_luar_range 	= $this->input->post('is_luar_range', TRUE);
			$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);
			
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$date_from = $thn1."-".$bln1."-".$tgl1;
			
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$date_to = $thn1."-".$bln1."-".$tgl1;
			
			// jika edit, var ini ada isinya
			$id_retur 	= $this->input->post('id_retur', TRUE);
			
			if ($id_retur == '') { // insert
				$cek_data = $this->mmaster->cek_data($no_dn);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'retur-beli/vmainform';
					$data['msg'] = "Data No DN ".$no_dn." sudah ada..!";
					$data['id_sj'] = '';
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$tgl = date("Y-m-d");
					$jumlah_input=$no-1;
					//echo $no."<br>";
					// &&&&&&&&&& test (kayaknya bener, nanti ari suruh tes lagi)
					/*for ($i=1;$i<=$jumlah_input;$i++)
					{
						if ($this->input->post('cek_'.$i, TRUE) == 'y') {
							echo $i." ";
							echo "qty: ".$this->input->post('qty_'.$i, TRUE).", luar: ".$this->input->post('kode_'.$i, TRUE).
							", no sj:".$this->input->post('no_sj_'.$i, TRUE)."<br>";
						}
					}
					// &&&&&&&&&&&&&&&&&
					die(); */
					
					// save headernya
					// cek apa udah ada datanya blm
					$this->db->select("id from tm_retur_beli WHERE no_dn_retur = '$no_dn' ", false);
					$query = $this->db->get();
					$hasil = $query->result();
						// jika data header blm ada 
						if(count($hasil)== 0) {
							// insert di tm_retur_beli
							$data_header = array(
							  'no_dn_retur'=>$no_dn,
							  'tgl_retur'=>$tgl_retur,
							  'kode_supplier'=>$kode_supplier,
							  'keterangan'=>$ket,
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl,
							  'faktur_date_from'=>$date_from,
							  'faktur_date_to'=>$date_to,
							  'jenis_pembelian'=>$jenis_pembelian
							);
							$this->db->insert('tm_retur_beli',$data_header);
						}
					// ############
					
					// ambil id retur di tabel tm_retur_beli
					$query2	= $this->db->query(" SELECT id FROM tm_retur_beli ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_retur	= $hasilrow->id;
			
					// insert ke tabel retur_beli_faktur ==========================
				//	$list_faktur = explode(",", $no_faktur); 
				/*	foreach($list_faktur as $rowutama) {
						$rowutama = trim($rowutama);
						if ($rowutama != '') {
							$this->db->query(" INSERT INTO tm_retur_beli_faktur (id_retur_beli, no_faktur) 
							VALUES ('$id_retur', '$rowutama') ");

							//=============================================================
						 } // end if  
					} // end foreach faktur
				*/
					
					// save no fakturnya (10-04-2012, ini sudah tidak dipake lagi)
					/*for ($i=1;$i<=$jumlah_input;$i++)
					{
						if ($this->input->post('cek_'.$i, TRUE) == 'y') {
							if ($this->input->post('no_sj_'.$i, TRUE) != '') {
								// cek dulu apakah sudah ada data dgn faktur tsb
								$query3	= $this->db->query(" SELECT id FROM tm_retur_beli_faktur WHERE id_retur_beli = '$id_retur'
											AND no_faktur = '".$this->input->post('faktur_'.$i, TRUE)."' ");
								if ($query3->num_rows() == 0) {
									$this->db->query(" INSERT INTO tm_retur_beli_faktur (id_retur_beli, no_faktur) 
										VALUES ('$id_retur', '".$this->input->post('faktur_'.$i, TRUE)."') ");
								}
							}
							// #####################################
						}
					} */
					
					// save detailnya
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						if ($this->input->post('cek_'.$i, TRUE) == 'y') {
							$this->mmaster->save($no_dn, $tgl_retur, $kode_supplier, 
									$ket, $this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE), 
									$this->input->post('faktur_'.$i, TRUE), $this->input->post('no_sj_'.$i, TRUE),
									$this->input->post('id_pembelian_detail_'.$i, TRUE));
						}
					}
			
					redirect('retur-beli/cform/view');
				}
			} // end if id_retur == ''
			else { // update
				$tgl = date("Y-m-d");
				$this->db->query(" UPDATE tm_retur_beli SET tgl_retur = '$tgl_retur',  
								keterangan = '$ket', tgl_update = '$tgl'
								where id= '$id_retur' ");
				
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$qty_lama = $this->input->post('qty_lama_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$harga = $this->input->post('harga_'.$i, TRUE);
						
						if ($this->input->post('qty_'.$i, TRUE) != $this->input->post('qty_lama_'.$i, TRUE)) {
						
							$this->db->query(" UPDATE tm_retur_beli_detail SET 
							 qty = '".$this->input->post('qty_'.$i, TRUE)."'
							where id= '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							
							// stoknya edit lagi
							// edit transaksi stoknya
								//1. cek stok brg berdasarkan no_bukti di tt_stok (koreksi: udh ga dipake)
							/*	$query3	= $this->db->query(" SELECT id, keluar_lain FROM tt_stok WHERE no_bukti = '$no_dn' 
												AND kode_brg='".$this->input->post('kode_'.$i, TRUE)."' 
												AND harga = '$harga'
												ORDER BY id DESC LIMIT 1 ");
								$hasilrow = $query3->row();
								$keluar	= $hasilrow->keluar; */
								
								//2. ambil stok terkini di tm_stok
								$query3	= $this->db->query(" SELECT stok FROM tm_stok 
												WHERE kode_brg='".$this->input->post('kode_'.$i, TRUE)."' ");
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
								$qty_baru = $qty;
								//$stokreset = $stok_lama+$keluar;
								$stokreset = $stok_lama+$qty_lama;
								//$stokskrg = ($stok_lama+$keluar) - $qty_baru;
								$stokskrg = ($stok_lama+$qty_lama) - $qty_baru;
								
								// NEW 140611, tm_stok_harga
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga 
												WHERE kode_brg='".$this->input->post('kode_'.$i, TRUE)."'
												AND harga = '$harga' ");
								$hasilrow = $query3->row();
								$stok_lama2	= $hasilrow->stok;
								$qty_baru = $qty;
								$stokreset2 = $stok_lama2+$qty_lama;
								$stokskrg2 = ($stok_lama2+$qty_lama) - $qty_baru;
								
								//3. insert ke tabel tt_stok dgn tipe masuk, utk membatalkan data brg keluar
								// 11-04-2012: ini udh ga dipake
								/*$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk_lain, saldo, tgl_input, harga)
								VALUES ('".$this->input->post('kode_'.$i, TRUE)."', '$no_dn', '$qty_lama', '$stokreset2', '$tgl_retur',
								'".$this->input->post('harga_'.$i, TRUE)."')  ");
								
								//4. insert ke tabel tt_stok dgn tipe keluar, utk mengupdate data stoknya
								$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar_lain, saldo, tgl_input, harga)
								VALUES ('".$this->input->post('kode_'.$i, TRUE)."', '$no_dn', '$qty_baru', '$stokskrg2', '$tgl_retur',
								'".$this->input->post('harga_'.$i, TRUE)."') "); */
								
									//5. update stok di tm_stok
								$this->db->query(" UPDATE tm_stok SET stok = '$stokskrg', tgl_update_stok = '$tgl'
													where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."'  ");
								
								$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokskrg2', tgl_update_stok = '$tgl'
													where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."' 
													AND harga = '$harga' ");

							} // end if qty != qty_lama
					} // end for
					
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "retur-beli/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "retur-beli/cform/cari/".$csupplier."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('retur-beli/cform/view');
			}
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'retur-beli/vformview';
    $keywordcari = "all";
    $csupplier = '0';
	//$kode_bagian = '';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/retur-beli/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);				
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/retur-beli/cform/cari/'.$csupplier.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'retur-beli/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }


  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $csupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($kode);
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "retur-beli/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "retur-beli/cform/cari/".$csupplier."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    
    //redirect('retur-beli/cform/view');
  }
  
  function notaretur(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
	//$th_now	= date("Y");
	
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	// generate no nota retur
			$th_now	= date("y");
			$bln_now = date("m");
			$romawi_now =  $this->mmaster->konversi_angka2romawi($bln_now);
			$query3	= $this->db->query(" SELECT no_nota FROM tm_nota_retur_beli ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_nota	= $hasilrow->no_nota;
			else
				$no_nota = '';
			if($no_nota!='') {
				$pisah1 = explode("/", $no_nota);
				$nomornya= $pisah1[0];
				$bulannya= $pisah1[1];
				$tahunnya= $pisah1[2];
				//
				$nonota_angka = (substr($nomornya, 0, 3))+1;
				if ($th_now == $tahunnya) {
					$jml_n_dn = $nonota_angka;
					switch(strlen($jml_n_dn)) {
						case "1": $kodedn	= "00".$jml_n_dn;
						break;	
						case "2": $kodedn	= "0".$jml_n_dn;
						break;
						case "3": $kodedn	= $jml_n_dn;
						break;
					}
					$nomornota = $kodedn;
				}
				else {
					$nomornota = "001";
				}
				// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			}
			else {
				$nomornota	= "001";
			}
			$nomornota = $nomornota."/".$romawi_now."/".$th_now;
	// ################
	
	// generate no retur
	/*		$query3	= $this->db->query(" SELECT no_nota FROM tm_nota_retur_beli ORDER BY no_nota DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_retur	= $hasilrow->no_nota;
			else
				$no_retur = '';
			if(strlen($no_retur)==12) {
				$noretur = substr($no_retur, 3, 9);
				$n_retur	= (substr($noretur,4,5))+1;
				$th_retur	= substr($noretur,0,4);
				if($th_now==$th_retur) {
						$jml_n_retur	= $n_retur;
						switch(strlen($jml_n_retur)) {
							case "1": $koderetur	= "0000".$jml_n_retur;
							break;
							case "2": $koderetur	= "000".$jml_n_retur;
							break;	
							case "3": $koderetur	= "00".$jml_n_retur;
							break;
							case "4": $koderetur	= "0".$jml_n_retur;
							break;
							case "5": $koderetur	= $jml_n_retur;
							break;	
						}
						$nomorretur = $th_now.$koderetur;
				}
				else {
					$nomorretur = $th_now."00001";
				}
			}
			else {
				$nomorretur	= $th_now."00001";
			}
			$nomorretur = "RB-".$nomorretur;
	*/
	$data['isi'] = 'retur-beli/vnotaretur';
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['msg'] = '';
	$data['no_nota'] = $nomornota;
	$this->load->view('template',$data);

  }
  
  function show_popup_retur(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jnsaction = $this->uri->segment(4); 
	$csupplier = $this->uri->segment(5); 
	$no_notanya = $this->uri->segment(6); 
	
	if ($jnsaction == '')
		$jnsaction = $this->input->post('jnsaction', TRUE); 
	if ($csupplier == '')
		$csupplier = $this->input->post('csupplier', TRUE);  
	if ($no_notanya == '')
		$no_notanya = $this->input->post('no_notanya', TRUE);  
		
	if ($keywordcari == '' && ($csupplier == '' || $jnsaction == '' || $no_notanya == '') ) {
		$jnsaction 	= $this->uri->segment(4);
		$csupplier 	= $this->uri->segment(5);
		$no_notanya 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($csupplier != '') {
		// cek apakah PKP atau non PKP
		$query3	= $this->db->query(" SELECT pkp FROM tm_supplier WHERE kode_supplier = '$csupplier' ");
		if ($query3->num_rows() == 0){
			$pkp = 'f';
		}
		else {
			$hasilrow = $query3->row();
			$pkp	= $hasilrow->pkp;
		}
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
	//$jum_total = $this->mmaster->get_sj_returtanpalimit($jnsaction, $csupplier, $no_notanya, $keywordcari);
		/*		$config['base_url'] = base_url()."index.php/faktur-jual/cform/show_popup_pembelian/".$jnsaction."/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		*/
	//$data['query'] = $this->mmaster->get_pembelian($config['per_page'],$this->uri->segment(7), $jnsaction, $csupplier, $keywordcari);						
	$data['query'] = $this->mmaster->get_sj_retur($jnsaction, $csupplier, $no_notanya, $keywordcari, $pkp);
	//$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$data['jnsaction'] = $jnsaction;
	$data['no_notanya'] = $no_notanya;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_supplier
				WHERE kode_supplier = '$csupplier' ");
				$hasilrow3 = $query3->row();
				$data['nama_supplier']	= $hasilrow3->nama;

	$this->load->view('retur-beli/vpopupsjretur',$data);

  }
  
  function submitnota(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$supplier 	= $this->input->post('supplier', TRUE);
			$jum 	= $this->input->post('jum', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj = trim($no_sj);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;						
			//$jum 	= $this->input->post('jum', TRUE);

			$cek_data = $this->mmaster->cek_data_notaretur($no_fp);
			if (count($cek_data) > 0) { 
				$data['isi'] = 'retur-beli/vnotaretur';
				$data['list_supplier'] = $this->mmaster->get_supplier(); 
				$data['msg'] = "Data no nota ".$no_fp." sudah ada..!";
				$this->load->view('template',$data);
			}
			else {
				$this->mmaster->savenota($no_fp, $tgl_fp, $supplier, $jum, $no_sj);
				redirect('retur-beli/cform/viewnotaretur');
			}

  }
  
  function viewnotaretur(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'retur-beli/vformviewnotaretur';
    $keywordcari = "all";
    $csupplier = '0';
	
    $jum_total = $this->mmaster->getAllnotatanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/retur-beli/cform/viewnotaretur/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllnota($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);		
				
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function carinotaretur(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
		
    $jum_total = $this->mmaster->getAllnotatanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/retur-beli/cform/cari/'.$csupplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllnota($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'retur-beli/vformviewnotaretur';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function editnotaretur(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_nota 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_nota($id_nota);
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['msg'] = '';
	$data['isi'] = 'retur-beli/veditformnotaretur';
	$data['id_nota'] = $id_nota;
	$this->load->view('template',$data);

  }
  
  // updatedatanota
  function updatedatanota() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
			$id_nota 	= $this->input->post('id_nota', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj_lama = $this->input->post('no_sj_lama', TRUE);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$supplier = $this->input->post('supplier', TRUE);  
			
			$no_nota_lama = $this->input->post('no_nota_lama', TRUE);
			$jum = $this->input->post('jum', TRUE);
			
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;
									
			$tgl = date("Y-m-d");			
			
			if ($no_fp != $no_nota_lama) {
				$cek_data = $this->mmaster->cek_data($no_fp, $supplier);
				if (count($cek_data) == 0) {
					$this->db->query(" UPDATE tm_nota_retur_beli SET 
									tgl_nota = '$tgl_fp', tgl_update= '$tgl', jumlah = '$jum' where id= '$id_nota' "); 
									
					// reset dulu status_faktur menjadi FALSE --------------------------------
					$list_sj_lama = explode(",", $no_sj_lama);
					foreach($list_sj_lama as $row2) {
						$row2 = trim($row2);
						if ($row2 != '') {
							$this->db->query(" UPDATE tm_retur_beli SET status_nota = 'f', no_nota = '' 
							WHERE kode_supplier = '$supplier' AND no_dn_retur = '$row2' "); 
						}
					}
					//-------------------------------------------------------------------------
					
					$this->db->query(" DELETE FROM tm_nota_retur_beli_sj where id_nota_retur_beli= '$id_nota' ");
					
					// insert tabel detail sj-nya
					$list_sj = explode(",", $no_sj); 
					foreach($list_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') { 
							$data_detail = array(
							  'id_nota_retur_beli'=>$id_nota,
							  'no_dn_retur'=>$row1
							);
							$this->db->insert('tm_nota_retur_beli_sj',$data_detail);
							
							// update status_nota di tabel tm_retur_beli
							$this->db->query(" UPDATE tm_retur_beli SET no_nota = '$no_fp', status_nota = 't' 
								WHERE no_dn_retur = '$row1' AND kode_supplier = '$supplier' ");
						}
					}
				
					redirect('retur-beli/cform/viewnotaretur');
				}
				else {
					$data['query'] = $this->mmaster->get_nota($id_nota);
					$data['list_supplier'] = $this->mmaster->get_supplier(); 
					$data['isi'] = 'retur-beli/veditformnotaretur';
					$data['id_nota'] = $id_nota;
					$data['msg'] = "Update gagal. Data no nota ".$no_fp." untuk supplier ".$supplier." sudah ada..!";
					$this->load->view('template',$data);
				}
			}
			else { // jika sama
				$this->db->query(" UPDATE tm_nota_retur_beli SET 
									tgl_nota = '$tgl_fp', tgl_update= '$tgl', jumlah = '$jum' where id= '$id_nota' "); 
				
					// reset dulu status_faktur menjadi FALSE --------------------------------
					$list_sj_lama = explode(",", $no_sj_lama);
					foreach($list_sj_lama as $row2) {
						$row2 = trim($row2);
						if ($row2 != '') {
							$this->db->query(" UPDATE tm_retur_beli SET status_nota = 'f', no_nota = '' 
							WHERE kode_supplier = '$supplier' AND no_dn_retur = '$row2' "); 
						}
					}
					//-------------------------------------------------------------------------
				
					$this->db->query(" DELETE FROM tm_nota_retur_beli_sj where id_nota_retur_beli= '$id_nota' ");
					
					// insert tabel detail sj-nya
					$list_sj = explode(",", $no_sj); 
					foreach($list_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') { 
							$data_detail = array(
							  'id_nota_retur_beli'=>$id_nota,
							  'no_dn_retur'=>$row1
							);
							$this->db->insert('tm_nota_retur_beli_sj',$data_detail);
							
							// update status_nota di tabel tm_retur_beli
							$this->db->query(" UPDATE tm_retur_beli SET no_nota = '$no_fp', status_nota = 't' 
								WHERE no_dn_retur = '$row1' AND kode_supplier = '$supplier' ");
						}
					}
				
					redirect('retur-beli/cform/viewnotaretur');
			}	
  }
  
  function deletenotaretur(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $this->mmaster->deletenotaretur($kode);
    redirect('retur-beli/cform/viewnotaretur');
  }
  
  function print_nota_debet(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$th_now	= date("y");
	$bln_now = date("m");
	$id_sj_retur 	= $this->uri->segment(4);
	
	$romawi_now =  $this->mmaster->konversi_angka2romawi($bln_now);
		
	// generate no DN retur (ralat: ini ga usah ada disini, soalnya udh ada di input data)
	/*		$query3	= $this->db->query(" SELECT no_dn_retur FROM tm_retur_beli ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_dn	= $hasilrow->no_dn_retur;
			else
				$no_dn = '';
			if($no_dn!='') {
				$pisah1 = explode("/", $no_dn);
				$nomornya= $pisah1[0];
				$bulannya= $pisah1[1];
				$tahunnya= $pisah1[2];
				//
				$nodn_angka = (substr($nomornya, 2, 4))+1;
				if ($th_now == $tahunnya) {
					$jml_n_dn = $nodn_angka;
					switch(strlen($jml_n_dn)) {
						case "1": $kodedn	= "000".$jml_n_dn;
						break;
						case "2": $kodedn	= "00".$jml_n_dn;
						break;	
						case "3": $kodedn	= "0".$jml_n_dn;
						break;
						case "4": $kodedn	= $jml_n_dn;
						break;
					}
					$nomordn = $kodedn;
				}
				else {
					$nomordn = "0001";
				}
				// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			}
			else {
				$nomordn	= "0001";
			}
			$nomordn = "Dn".$nomordn."/".$romawi_now."/".$th_now; */
			
			//cek apakah nomer Dn udh ada atau blm. kalo blm maka diupdate
			$query3	= $this->db->query(" SELECT no_dn_retur FROM tm_retur_beli WHERE id = '$id_sj_retur' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() == 0) {
				$this->db->query(" UPDATE tm_retur_beli SET no_dn_retur = '$nomordn' WHERE id = '$id_sj_retur' ");
			}
			
			$tot_sj = 0;
			// ambil jumlah total retur utk Dn tsb
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_faktur 
											WHERE id_retur_beli = '$id_sj_retur' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT SUM(qty*harga) as jumlahnya FROM tm_retur_beli_detail 
						WHERE id_retur_beli = '$id_sj_retur' AND id_retur_beli_faktur = '$row2->id' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jumlahnya	= $hasilrow->jumlahnya;
							$tot_sj+= $jumlahnya;
						}
						else {
							$jumlahnya	= '0';
						}
					}
				}
				
				$query3	= $this->db->query(" SELECT SUM(qty*harga) as jumlahnya FROM tm_retur_beli_detail 
						WHERE id_retur_beli = '$id_sj_retur' AND id_retur_beli_faktur = '0' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jumlahnya	= $hasilrow->jumlahnya;
							$tot_sj+= $jumlahnya;
						}
						else {
							$jumlahnya	= '0';
						}
			// #######################################
		
	$rownya = $this->mmaster->get_retur($id_sj_retur);
	
	$pisah1 = explode("-", $rownya[0]['faktur_date_from']);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
	$nama_bln1 =  $this->mmaster->konversi_bln2deskripsi($bln1);
	$faktur_date_from = $tgl1." ".$nama_bln1." ".$thn1;
	
	$pisah1 = explode("-", $rownya[0]['faktur_date_to']);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
	$nama_bln2 =  $this->mmaster->konversi_bln2deskripsi($bln1);
	$faktur_date_to = $tgl1." ".$nama_bln2." ".$thn1;
	
	$data['query'] = $this->mmaster->get_retur($id_sj_retur);
	
	$data['id_sj_retur'] = $id_sj_retur;
	$data['tot_retur'] = $tot_sj;
	$data['faktur_date_from'] = $faktur_date_from;
	$data['faktur_date_to'] = $faktur_date_to;
	//$data['terbilang_tot_retur'] = $this->mmaster->terbilang($tot_sj);

	$this->load->view('retur-beli/vprintdn',$data);

  }
  
  function print_nota_retur(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$th_now	= date("y");
	$bln_now = date("m");
	$id_nota_retur 	= $this->uri->segment(4);
	
	$proses = $this->input->post('proses', TRUE);  
	if ($id_nota_retur == '')
		$id_nota_retur = $this->input->post('id_nota_retur', TRUE);  
	
	if ($proses != "") {
		$nama_kasie_acc = $this->input->post('nama_kasie_acc', TRUE);  
		$data['nama_kasie_acc'] = $nama_kasie_acc;
	
		$romawi_now =  $this->mmaster->konversi_angka2romawi($bln_now);			
			$tot_sj = 0;
			// ambil jumlah total retur utk nota tsb
				$query2	= $this->db->query(" SELECT * FROM tm_nota_retur_beli_sj 
											WHERE id_nota_retur_beli = '$id_nota_retur' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT SUM(b.qty*(b.harga/1.1)) as jumlahnya 
							FROM tm_retur_beli a, tm_retur_beli_detail b 
							WHERE a.id = b.id_retur_beli AND
							a.no_dn_retur = '$row2->no_dn_retur' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jumlahnya	= $hasilrow->jumlahnya;
							$tot_sj+= $jumlahnya;
						}
						else {
							$jumlahnya	= '0';
						}
					}
				}
			// #######################################
		$data['query'] = $this->mmaster->get_nota($id_nota_retur);
		$data['tot_retur'] = $tot_sj;
	}
	else 
		$data['nama_kasie_acc'] = '';
	
	$data['id_nota_retur'] = $id_nota_retur;
	$data['proses'] = $proses;

	$this->load->view('retur-beli/vprintnotaretur',$data);

  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi 	= $this->uri->segment(4);
	$kode_supplier 	= $this->uri->segment(5);

	if ($posisi == '' || $kode_supplier == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kode_supplier = $this->input->post('kode_supplier', TRUE);  
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);
		
		if ($keywordcari == '' || ($posisi == '' && $kode_supplier == '')) {
			$posisi 	= $this->uri->segment(4);
			$kode_supplier 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}

		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kode_supplier);

			$config['base_url'] = base_url()."index.php/retur-beli/cform/show_popup_brg/".$posisi."/".$kode_supplier."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kode_supplier);						
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['startnya'] = $config['cur_page'];
	$data['kode_supplier'] = $kode_supplier;
	
	$this->load->view('retur-beli/vpopupbrg',$data);
  }
  
}
