<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function indexold() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_sj']	= $this->lang->line('page_title_sj');
			$data['form_title_detail_sj']	= $this->lang->line('form_title_detail_sj');
			$data['form_nomor_sj']	= $this->lang->line('form_nomor_sj');
			$data['form_tanggal_sj']	= $this->lang->line('form_tanggal_sj');
			$data['form_kode_produk_sj']	= $this->lang->line('form_kode_produk_sj');
			$data['form_nm_produk_sj']	= $this->lang->line('form_nm_produk_bbk');
			$data['form_hjp_sj']	= $this->lang->line('form_hjp_sj');
			$data['form_unit_sj']	= $this->lang->line('form_unit_sj');
			$data['form_total_sj']	= $this->lang->line('form_total_sj');
			$data['form_total_nilai_brg_sj']	= $this->lang->line('form_total_nilai_brg_sj');
			$data['form_ket_sj']	= $this->lang->line('form_ket_sj');
			$data['form_cabang_sj']	= $this->lang->line('form_cabang_sj');
			$data['form_option_pel_sj']	= $this->lang->line('form_option_pel_sj');
			$data['lpelanggan']	= "";
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$tahun	= date("Y");
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");

			$data['dateTime']	= date("m/d/Y",time());
			$data['tsj']	= $tgl."/".$bln."/".$thn;
				
			$this->load->model('sjpopup/mclass');
			
			/*** Disabled 04072011
			$q_itemiproduct	= $this->mclass->itemiproduct();
			if($q_itemiproduct->num_rows()>0) {
				$row_itemiproduct	= $q_itemiproduct->row();
				$iproductitem	= $row_itemiproduct->i_product;
				switch(strlen($iproductitem)) {
					case "1": $iprocitem	= '000000'.$iproductitem;
					break;
					case "2": $iprocitem	= '00000'.$iproductitem;
					break;
					case "3": $iprocitem	= '0000'.$iproductitem;
					break;
					case "4": $iprocitem	= '000'.$iproductitem;
					break;
					case "5": $iprocitem	= '00'.$iproductitem;
					break;
					case "6": $iprocitem	= '0'.$iproductitem;
					break;
					case "7": $iprocitem	= $iproductitem;
					break;
				}
			} else {
				$iprocitem	= '0000001';
				$iproductitem	= 1;
			}
			//$data['iprocitem']	= $iprocitem;
			$data['iprocitem']	= $iproductitem;
			***/
			
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			/***
			$qsj	= $this->mclass->get_nomorsj();
			if($qsj->num_rows() > 0 ) {
				$row_isj	= $qsj->row();
				if(strlen($row_isj->i_sj_code) > 4 ) {
					$getth	= substr($row_isj->i_sj_code,0,4);
					if($getth==date("Y")) {
						$now_thn	= $getth;
						$getnum		= substr($row_isj->i_sj_code,4,strlen($row_isj->i_sj_code)-4);
						$ch_isj	= $getnum+1;
					} else {
						$now_thn	= date("Y");
						$ch_isj	= 1;
					}
				} else {
					$now_thn	= date("Y");
					$ch_isj	= 1;
				}
			} else {
				$now_thn	= date("Y");
				$ch_isj	= 1;
			}
			$data['nomorisj']	= $now_thn.$ch_isj;
			***/
			
			$qthn	= $this->mclass->get_thn();
			$qnomor	= $this->mclass->get_nomor();

			if($qthn->num_rows() > 0) {
				$th		= $qthn->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}

			if($thn==$tahun) {
				if($qnomor->num_rows() > 0)  {
					$row	= $qnomor->row_array();
					$isjcode		= $row['isjcode']+1;		
					switch(strlen($isjcode)) {
						case "1": $nomor	= "0000".$isjcode;
						break;
						case "2": $nomor	= "000".$isjcode;
						break;	
						case "3": $nomor	= "00".$isjcode;
						break;
						case "4": $nomor	= "0".$isjcode;
						break;
						case "5": $nomor	= $isjcode;
						break;	
					}
				} else {
					$nomor		= "00001";
				}
				$nomor	= $tahun.$nomor;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['nomorisj']	= $nomor;
			
			$this->load->view('sj/vmainform', $data);
		
	}
	
	// 28-12-2012
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			/*$data['form_nomor_do']	= $this->lang->line('form_nomor_do');
			$data['form_option_pel_do']	= $this->lang->line('form_option_pel_do');			
			$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');		
			
			$data['page_title_do']	= $this->lang->line('page_title_do');
			$data['detail']		= "";
			$data['list']		= ""; 
			$data['lpelanggan']	= ""; */
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['button_batal']	= $this->lang->line('button_batal');		
			$data['lpelanggan']	= "";
			$data['limages']	= base_url();
			
			$this->load->model('sjpopup/mclass');
			
			$data['opt_cabang']	= $this->mclass->lcabang();
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			
			$data['isi']	='sjpopup/vmainform';
			$this->load->view('template',$data);
		
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iteration	= $this->input->post('iteration');
		$iterasi	= $iteration;
		$i_sj		= $this->input->post('i_sj');
		$d_sj		= $this->input->post('d_sj');
		
		$ex_d_sj	= explode("/",$d_sj,strlen($d_sj));
		$dsj		= $ex_d_sj[2]."-".$ex_d_sj[1]."-".$ex_d_sj[0];
		
		$i_customer	= $this->input->post('i_customernya');
		$i_branch	= $this->input->post('i_branchnya');
		$v_sj_total	= $this->input->post('v_sj_total');
		$e_note		= $this->input->post('e_note');
				
		$i_product	= array();
		$e_product_name	= array();
		$v_price= array();
		$n_count_product	= array();
		$v_sj_gross	= array();
		$i_outbonm	= array();
		
		for($cacah=0;$cacah<=$iteration;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_price[$cacah]	= $this->input->post('price'.'_'.'tblItem'.'_'.$cacah);
			$n_count_product[$cacah]	= $this->input->post('n_count_product'.'_'.'tblItem'.'_'.$cacah);
			$v_sj_gross[$cacah]	= $this->input->post('v_sj_gross'.'_'.'tblItem'.'_'.$cacah);
			// i_outbonm_sebunyi_tblItem_
			$i_outbonm[$cacah]	= $this->input->post('i_outbonm_sebunyi'.'_'.'tblItem'.'_'.$cacah);
		}

		if ( !empty($i_sj) &&
		     !empty($d_sj) && 
		     !empty($v_sj_total) ) {
			 //	if(!empty($i_product_0)) {
				$this->load->model('sjpopup/mclass');
				$qnsj	= $this->mclass->carisj($i_sj);
				if($qnsj->num_rows()==0) {	
					$this->mclass->msimpan($i_sj,$dsj,$v_sj_total,$e_note,$i_product,$e_product_name,$v_price,$n_count_product,
					$v_sj_gross,$iterasi,$i_customer,$i_branch, $i_outbonm);
				} else {
					print "<script>alert(\"Maaf, Nomor SJ tsb sebelumnya telah diinput. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
				}	
			/*	} else {
					print "<script>alert(\"Maaf, item SJ harus terisi. Terimakasih.\");show(\"sj/cform\",\"#content\");</script>";
				} */
		} else {
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['button_batal']	= $this->lang->line('button_batal');		
			$data['lpelanggan']	= "";
			$data['limages']	= base_url();
			
			$this->load->model('sjpopup/mclass');
			
			$data['opt_cabang']	= $this->mclass->lcabang();
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			print "<script>alert(\"Maaf, data SJ gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			$this->load->view('sjpopup/vmainform',$data);
		}	
	}

	function carisj() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$nsj	= $this->input->post('nsj')?$this->input->post('nsj'):$this->input->get_post('nsj');
		$this->load->model('sj/mclass');
		$qnsj	= $this->mclass->carisj($nsj);
		if($qnsj->num_rows()>0) {
			echo "Maaf, No. SJ sdh ada!";
		} else {
		}
	}	

	function cari_cabang() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_customer	= $this->input->post('ibranch')?$this->input->post('ibranch'):$this->input->get_post('ibranch');
		$this->load->model('sjpopup/mclass');
		
		$query	= $this->mclass->getcabang($i_customer);
		if($query->num_rows()>0) {
			$c	= "";
			$cabang	= $query->result();
			foreach($cabang as $row) {
				$c.="<option value=".$row->i_branch_code." >".$row->e_branch_name." ( ".$row->e_initial." ) "."</option>";
			}
			echo "<select name=\"i_branch\" id=\"i_branch\" >
					<option value=\"\">[Pilih Cabang Pelanggan]</option>".$c."</select>";
		}
	}

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4,0);
		$dsj		= $this->uri->segment(5,0);

		$expdsj	= explode("-",$dsj,strlen($dsj));
		$bulan	= $expdsj[1];
		$tahun	= $expdsj[0];
		
		$data['dsj']	= $dsj;
		$data['iterasi']	= $iterasi;

		$data['page_title']	= "ITEM BARANG";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('sj/mclass');

		$query	= $this->mclass->lbarangjadi($bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->lbarangjadiopsi($bulan,$tahun);
			$jml	= $query->num_rows();
		}
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/sj/cform/listbarangjadinext/'.$iterasi.'/'.$dsj.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpagesopsi($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		
		$this->load->view('sj/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4,0);
		$dsj		= $this->uri->segment(5,0);

		$expdsj	= explode("-",$dsj,strlen($dsj));
		$bulan	= $expdsj[1];
		$tahun	= $expdsj[0];
		
		$data['dsj']	= $dsj;
		$data['iterasi']	= $iterasi;
		
		$data['page_title']	= "ITEM BARANG";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('sj/mclass');

		$query	= $this->mclass->lbarangjadi($bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->lbarangjadiopsi($bulan,$tahun);
			$jml	= $query->num_rows();
		}
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/sj/cform/listbarangjadinext/'.$iterasi.'/'.$dsj.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']		= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpagesopsi($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		
		$this->load->view('sj/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$dsj	= $this->input->post('dsj')?$this->input->post('dsj'):$this->input->get_post('dsj');

		$expdsj	= explode("-",$dsj,strlen($dsj));
		$bulan	= $expdsj[1];
		$tahun	= $expdsj[0];
		
		$data['dsj']	= $dsj;
				
		$iterasi	= $this->uri->segment(4);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "ITEM BARANG";
		$data['lurl']		= base_url();

		$this->load->model('sj/mclass');

		$query	= $this->mclass->flbarangjadi($key,$bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->flbarangjadiopsi($key,$bulan,$tahun);
			$jml	= $query->num_rows();
		}
		
		$list	= "";
		$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
		
		if($jml>0){
			$cc	= 1;
			
			foreach($query->result() as $row){
				
				$iproduct	= trim($row->iproduct);
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				   <td><a href=\"javascript:settextfield('$iproduct','$row->productname','$row->price','$row->stp','$row->iso')\">".$row->iproduct."</a></td>
				  <td><a href=\"javascript:settextfield('$iproduct','$row->productname','$row->price','$row->stp','$row->iso')\">".$row->productname."</a></td>	
				  <td><a href=\"javascript:settextfield('$iproduct','$row->productname','$row->price','$row->stp','$row->iso')\">".$row->price."</a></td>				  
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
	
	// 28-12-2012
	function listdetailbonm() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		//$ibranch	= $this->uri->segment(4);
		
		//$data['ibranch']	= $ibranch;
		$data['page_title']	= "NOMOR BON M KELUAR PENJUALAN GRADE A";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('sjpopup/mclass');

		$data['isi']	= $this->mclass->lbonmdetail();
				
		$this->load->view('sjpopup/vlistbonm',$data);			
	}
	
	// 28-12-2012
	function detailsimpan() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_sj']	= $this->lang->line('page_title_sj');
			$data['form_title_detail_sj']	= $this->lang->line('form_title_detail_sj');
			$data['form_nomor_sj']	= $this->lang->line('form_nomor_sj');
			$data['form_tanggal_sj']	= $this->lang->line('form_tanggal_sj');
			$data['form_kode_produk_sj']	= $this->lang->line('form_kode_produk_sj');
			$data['form_nm_produk_sj']	= $this->lang->line('form_nm_produk_bbk');
			$data['form_hjp_sj']	= $this->lang->line('form_hjp_sj');
			$data['form_unit_sj']	= $this->lang->line('form_unit_sj');
			$data['form_total_sj']	= $this->lang->line('form_total_sj');
			$data['form_total_nilai_brg_sj']	= $this->lang->line('form_total_nilai_brg_sj');
			$data['form_ket_sj']	= $this->lang->line('form_ket_sj');
			$data['form_cabang_sj']	= $this->lang->line('form_cabang_sj');
			$data['form_option_pel_sj']	= $this->lang->line('form_option_pel_sj');
			$data['lpelanggan']	= "";
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$data['lpelanggan']	= "";
			$data['lcabang']	= "";
		
			$tahun	= date("Y");
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
		
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgSJ']	= $tgl."/".$bln."/".$thn;

			$List = "";
			
			$i_outbonm_code	= $this->input->post('i_outbonm_code');
			$i_outbonm		= $this->input->post('i_outbonm');
			$iproduct	= $this->input->post('i_product');
			$ibranch	= $this->input->post('i_branch');
			$icustomer	= $this->input->post('i_customer');
			$exp_i_outbonm	= explode("#",$i_outbonm,strlen($i_outbonm));
			$exp_iproduct	= explode("#",$iproduct,strlen($iproduct));
			
			$sql="SELECT a.e_customer_name, b.e_branch_name, b.e_initial FROM tr_customer a, tr_branch b 
								 WHERE a.i_customer = b.i_customer AND  a.i_customer= '$icustomer' AND b.i_branch = '$ibranch'";
			
			$query3	= $this->db->query(" SELECT a.e_customer_name, b.e_branch_name, b.e_initial FROM tr_customer a, tr_branch b 
								 WHERE a.i_customer = b.i_customer AND  a.i_customer= '$icustomer' AND b.i_branch_code = '$ibranch' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$nama_cust	= $hasilrow->e_customer_name;
				$nama_cabang	= $hasilrow->e_branch_name;
				$nama_inisial	= $hasilrow->e_initial;
			}
			else {
				$nama_cust = '';
				$nama_cabang = '';
				$nama_inisial = '';
			}
			$data['ibranch']	= $ibranch;
			$data['icustomer']	= $icustomer;
			$data['nama_cust']	= $nama_cust;
			$data['nama_cabang']	= $nama_cabang;
			$data['nama_inisial']	= $nama_inisial;
			
			$this->load->model('sjpopup/mclass');

			$iter = 0;	
			$totjual = 0;
			foreach($exp_i_outbonm as $i_outbonm) {
				$query	= $this->mclass->detailsimpan($i_outbonm,$exp_iproduct[$iter]);
				if($query->num_rows()>0) {

					$row	= $query->row();

					$qhargaperpelanggan = $this->mclass->hargaperpelanggan($row->iproduct,$icustomer);
					$qhargadefault 	    = $this->mclass->hargadefault($row->iproduct);
						
					if($qhargaperpelanggan->num_rows()>0){
						$rhargaperpelanggan = $qhargaperpelanggan->row();
							
						$hargaperunit = $rhargaperpelanggan->v_price;
						$harga = $row->n_count_product*$hargaperunit;
							
					}elseif($qhargadefault->num_rows()>0){
						$rhargadefault = $qhargadefault->row();
							
						$hargaperunit = $rhargadefault->v_price;
						$harga = $row->n_count_product*$hargaperunit;
							
					}else{
						$hargaperunit = 0;
						$harga = $row->n_count_product*$hargaperunit;
					}
					$totjual+= $harga;
					$List .= "
						<tr>
							<td><div style=\"font:11px/24px;text-align:right;width:18px;margin-right:0px;\">".($iter+1)."</div></td>
							<td><DIV ID=\"ajax_i_outbonm_tblItem_".$iter."\" >
							<input type=\"text\" ID=\"i_outbonm_tblItem_".$iter."\" name=\"i_outbonm_tblItem_".$iter."\" style=\"width:60px;\" value=\"".$row->i_outbonm_code."\" readonly >
							<input type=\"hidden\" name=\"qty_outbonm_tblItem_".$iter."\" id=\"qty_outbonm_tblItem_".$iter."\" value=\"".$row->n_count_product."\" ></DIV>
							</td>
							<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" ><input type=\"text\" ID=\"i_product_tblItem_".$iter."\" name=\"i_product_tblItem_".$iter."\" style=\"width:100px;\" value=\"".$row->iproduct."\" readonly ></DIV></td>
							<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" ><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\"  name=\"e_product_name_tblItem_".$iter."\" style=\"width:190px;\" value=\"".$row->productname."\" readonly ></DIV></td>
							
							<td><input type=\"text\" ID=\"price_tblItem_".$iter."\" name=\"price_tblItem_".$iter."\" style=\"width:50px;text-align:right;\" value=\"".$hargaperunit."\" readonly='true' ></td>
							<td><DIV ID=\"ajax_n_count_product_tblItem_".$iter."\" ><input type=\"text\" ID=\"n_count_product_tblItem_".$iter."\"  name=\"n_count_product_tblItem_".$iter."\" style=\"width:50px;text-align:right;\" value=\"".$row->n_count_product."\" onkeyup=\"validNum('n_count_product_tblItem','".$iter."');validStok(".$iter.");validStok2(".$iter.");totalharga(this.value,".$iter.");\" readonly='true' ></DIV></td>
							
							<td><DIV ID=\"ajax_v_sj_gross_tblItem_".$iter."\" ><input type=\"text\" ID=\"v_sj_gross_tblItem_".$iter."\"  name=\"v_sj_gross_tblItem_".$iter."\" style=\"width:115px;text-align:right;\" value=\"".$harga."\" readonly ></DIV>
							<input type=\"hidden\" ID=\"i_outbonm_sebunyi_tblItem_".$iter."\" name=\"i_outbonm_sebunyi_tblItem_".$iter."\" value=\"".$row->i_outbonm."\" >
							<input type=\"hidden\" ID=\"f_stp_tblItem_".$iter."\" name=\"f_stp_tblItem_".$iter."\" value=\"".$row->stp."\" >
							<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$iter."\">
							</td>
						</tr>
					";
				}
				$iter++;
			} // sampe sini 11:24. lanjut after jumatan
			
			// ======= tambahan 28-12-2012 ===========================
			$qthn	= $this->mclass->get_thn();
			$qnomor	= $this->mclass->get_nomor();

			if($qthn->num_rows() > 0) {
				$th		= $qthn->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}

			if($thn==$tahun) {
				if($qnomor->num_rows() > 0)  {
					$row	= $qnomor->row_array();
					$isjcode		= $row['isjcode']+1;		
					switch(strlen($isjcode)) {
						case "1": $nomor	= "0000".$isjcode;
						break;
						case "2": $nomor	= "000".$isjcode;
						break;	
						case "3": $nomor	= "00".$isjcode;
						break;
						case "4": $nomor	= "0".$isjcode;
						break;
						case "5": $nomor	= $isjcode;
						break;	
					}
				} else {
					$nomor		= "00001";
				}
				$nomor	= $tahun.$nomor;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['nomorisj']	= $nomor;
			
			// =========== end tambahan ==============================
					
		/*	$qryth	= $this->mclass->getthndo();
			$qrydo	= $this->mclass->getnomordo(); */
			
			$data['opt_cabang']	= $this->mclass->lcabang();	
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			$data['no']	= $nomor;
			
			$jumnya = $iter-1;
			$data['jumdata'] = $iter;
			
			$data['List']	= $List;
			$tahun	= date("Y");
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");

			$data['dateTime']	= date("m/d/Y",time());
			$data['tsj']	= $tgl."/".$bln."/".$thn;
			$data['totjual']	= $totjual;
			$data['isi']	='sjpopup/vform';
			$this->load->view('template',$data);
		
		
	}	
}
?>
