<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('setting/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  
	  $submit 	= $this->input->post('submit');
	  
	  $data['msg'] = "";
	  if ($submit != '') {
		$id 	= $this->input->post('id');
		$nama 	= $this->input->post('nama');
		$alamat 	= $this->input->post('alamat', TRUE);
		$kota 	= $this->input->post('kota', TRUE);
		$bag_pembelian 	= $this->input->post('bag_pembelian', TRUE);
		$bag_pembelian2 	= $this->input->post('bag_pembelian2', TRUE);
		$bag_keuangan 	= $this->input->post('bag_keuangan', TRUE);
		$kepala_bagian 	= $this->input->post('kepala_bagian', TRUE);
		$bag_admstok 	= $this->input->post('bag_admstok', TRUE);
		$bag_admstok2 	= $this->input->post('bag_admstok2', TRUE);
		$id_gudang_admstok 	= $this->input->post('id_gudang_admstok', TRUE);
		$id_gudang_admstok2 	= $this->input->post('id_gudang_admstok2', TRUE);
		$spv_bag_admstok 	= $this->input->post('spv_bag_admstok', TRUE);
		
		$this->mmaster->save($id, $nama, $alamat, $kota, $bag_pembelian, $bag_pembelian2, $bag_keuangan, $kepala_bagian, $bag_admstok, $bag_admstok2, $id_gudang_admstok,$id_gudang_admstok2, $spv_bag_admstok);
		$data['msg'] = "Data sukses disimpan";
	  }
	  
	$datasetting = $this->mmaster->get_perusahaan();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	
	if ($datasetting['id_gudang_admstok'] != '')
		$list_id_gudang_admstok = explode(";", $datasetting['id_gudang_admstok']);
	else
		$list_id_gudang_admstok = '';
	
	if ($datasetting['id_gudang_admstok2'] != '')
		$list_id_gudang_admstok2 = explode(";", $datasetting['id_gudang_admstok2']);
	else
		$list_id_gudang_admstok2 = '';

	$data['list_id_gudang_admstok'] = $list_id_gudang_admstok;
	$data['list_id_gudang_admstok2'] = $list_id_gudang_admstok2;
	$data['datasetting'] = $datasetting;
    $data['isi'] = "setting/vmainform";
	$this->load->view('template',$data);
  }

	// 22-09-2015
	function konversisatuan(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->getkonversisatuan($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$eid_satuan_awal = $row->id_satuan_awal;
			$eid_satuan_konversi = $row->id_satuan_konversi;
			$erumus_konversi = $row->rumus_konversi;
			$eangka_faktor_konversi = $row->angka_faktor_konversi;
			$ekonversi_harga_mutasi_stok = $row->konversi_harga_mutasi_stok;
		}
	}
	else {
			$eid = '';
			$eid_satuan_awal = '';
			$eid_satuan_konversi = '0';
			$erumus_konversi = '0';
			$eangka_faktor_konversi = '0';
			$ekonversi_harga_mutasi_stok = '0';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['eid_satuan_awal'] = $eid_satuan_awal;
	$data['eid_satuan_konversi'] = $eid_satuan_konversi;
	$data['erumus_konversi'] = $erumus_konversi;
	$data['eangka_faktor_konversi'] = $eangka_faktor_konversi;
	$data['ekonversi_harga_mutasi_stok'] = $ekonversi_harga_mutasi_stok;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAllkonversisatuan();
	$data['satuan'] = $this->mmaster->get_satuan();
    $data['isi'] = 'setting/vmainformkonversi';
    
	$this->load->view('template',$data);
  }

  function submitkonversisatuan(){
		$goedit 	= $this->input->post('goedit', TRUE);
		$id_konversi 	= $this->input->post('id_konversi', TRUE);
		$id_satuan_awal 	= $this->input->post('id_satuan_awal', TRUE);
		$id_satuan_konversi 	= $this->input->post('id_satuan_konversi', TRUE); 
		$rumus_konversi = $this->input->post('rumus_konversi', TRUE);
		$angka_faktor_konversi 	= $this->input->post('angka_faktor_konversi', TRUE);
		$konversi_harga_mutasi_stok 	= $this->input->post('konversi_harga_mutasi_stok', TRUE);
			
		$this->mmaster->savekonversisatuan($id_konversi, $id_satuan_awal, $id_satuan_konversi, $rumus_konversi, $angka_faktor_konversi, $konversi_harga_mutasi_stok, $goedit);
		redirect('setting/cform/konversisatuan');
  }

  function deletekonversisatuan(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->deletekonversisatuan($id);
    redirect('setting/cform/konversisatuan');
  }
  
}
