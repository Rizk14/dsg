<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-rekap-hutang/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		
	$data['isi'] = 'info-rekap-hutang/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $data['isi'] = 'info-rekap-hutang/vformview';
    $jenis_beli = $this->input->post('jenis_beli', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	if ($date_from == '' && $date_to == '') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
	}

    $jum_total = $this->mmaster->get_all_rekap_pembeliantanpalimit($jenis_beli, $date_from, $date_to);
						/*	$config['base_url'] = base_url().'index.php/info-pembelian/cform/view/index/'.$date_from.'/'.$date_to.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						*/
	//$data['query'] = $this->mmaster->get_all_pembelian($config['per_page'],$this->uri->segment(7), $jenis_beli, $date_from, $date_to);						
	$data['query'] = $this->mmaster->get_all_rekap_pembelian($jenis_beli, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	//$data['cari'] = $keywordcari;
	//$data['list_supplier'] = $this->mmaster->get_supplier();
	//$data['csupplier'] = $csupplier;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$this->load->view('template',$data);
  }

}
