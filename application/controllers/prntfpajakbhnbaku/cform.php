<?php
include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}		
			$data['page_title_fpajakbhnbaku']	= $this->lang->line('page_title_fpajakbhnbaku');
			$data['form_title_detail_fpajakbhnbaku']	= $this->lang->line('form_title_detail_fpajakbhnbaku');
			$data['list_fpajakbhnbaku_faktur']	= $this->lang->line('list_fpajakbhnbaku_faktur');
			$data['list_fpajakbhnbaku_kd_brg']	= $this->lang->line('list_fpajakbhnbaku_kd_brg');
			$data['list_fpajakbhnbaku_jenis_brg']	= $this->lang->line('list_fpajakbhnbaku_jenis_brg');
			$data['list_fpajakbhnbaku_s_produksi'] = $this->lang->line('list_fpajakbhnbaku_s_produksi');
			$data['list_fpajakbhnbaku_satuan']	= $this->lang->line('list_fpajakbhnbaku_satuan');
			$data['list_fpajakbhnbaku_nm_brg']	= $this->lang->line('list_fpajakbhnbaku_nm_brg');
			$data['list_fpajakbhnbaku_qty']	= $this->lang->line('list_fpajakbhnbaku_qty');
			$data['list_fpajakbhnbaku_hjp']	= $this->lang->line('list_fpajakbhnbaku_hjp');
			$data['list_fpajakbhnbaku_amount']	= $this->lang->line('list_fpajakbhnbaku_amount');
			$data['list_fpajakbhnbaku_total_pengiriman']	= $this->lang->line('list_fpajakbhnbaku_total_pengiriman');
			$data['list_fpajakbhnbaku_total_penjualan']	= $this->lang->line('list_fpajakbhnbaku_total_penjualan');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			
			$this->load->model('prntfpajakbhnbaku/mclass');
			
			$data['opt_jns_brg']	= $this->mclass->lklsbrg();
			$data['isi']	='prntfpajakbhnbaku/vmainform';
		$this->load->view('template',$data);
			
		
	}

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpajakbhnbaku/mclass');
		$fpengganti	= $this->uri->segment(4); 

		$query	= $this->mclass->lbarangjadi($fpengganti);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/prntfpajakbhnbaku/cform/listbarangjadinext/'.$fpengganti;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'], $fpengganti);		

		$this->load->view('prntfpajakbhnbaku/vlistformbrgjadi',$data);			
	}
	
	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpajakbhnbaku/mclass');
		$fpengganti	= $this->uri->segment(4); 

		$query	= $this->mclass->lbarangjadi($fpengganti);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntfpajakbhnbaku/cform/listbarangjadinext/'.$fpengganti;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'], $fpengganti);		

		$this->load->view('prntfpajakbhnbaku/vlistformbrgjadi',$data);			
	}

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('prntfpajakbhnbaku/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}		

	function carilistpenjualanndo() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$nofaktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->input->get_post('no_faktur');
		$ecategory	= $this->input->post('e_category')?$this->input->post('e_category'):$this->input->get_post('e_category');
		$fstopproduksi	= $this->input->post('f_stop_produksi')?$this->input->post('f_stop_produksi'):$this->input->get_post('f_stop_produksi');

		$data['nofaktur']	= $nofaktur;
		$data['kategori']	= $ecategory;
		$data['sproduksi']	= $fstopproduksi=1?" checked ":"";
		
		$data['page_title_fpajakbhnbaku']	= $this->lang->line('page_title_fpajakbhnbaku');
		$data['form_title_detail_fpajakbhnbaku']	= $this->lang->line('form_title_detail_fpajakbhnbaku');
		$data['list_fpajakbhnbaku_faktur']	= $this->lang->line('list_fpajakbhnbaku_faktur');
		$data['list_fpajakbhnbaku_kd_brg']	= $this->lang->line('list_fpajakbhnbaku_kd_brg');
		$data['list_fpajakbhnbaku_jenis_brg']	= $this->lang->line('list_fpajakbhnbaku_jenis_brg');
		$data['list_fpajakbhnbaku_s_produksi'] = $this->lang->line('list_fpajakbhnbaku_s_produksi');
		$data['list_fpajakbhnbaku_satuan']	= $this->lang->line('list_fpajakbhnbaku_satuan');
		$data['list_fpajakbhnbaku_nm_brg']	= $this->lang->line('list_fpajakbhnbaku_nm_brg');
		$data['list_fpajakbhnbaku_qty']	= $this->lang->line('list_fpajakbhnbaku_qty');
		$data['list_fpajakbhnbaku_hjp']	= $this->lang->line('list_fpajakbhnbaku_hjp');
		$data['list_fpajakbhnbaku_amount']	= $this->lang->line('list_fpajakbhnbaku_amount');
		$data['list_fpajakbhnbaku_total_pengiriman']	= $this->lang->line('list_fpajakbhnbaku_total_pengiriman');
		$data['list_fpajakbhnbaku_total_penjualan']	= $this->lang->line('list_fpajakbhnbaku_total_penjualan');
		
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";
		$data['limages']	= base_url();
		
		// 16-10-2012
		$fpengganti	= $this->input->post('fpengganti');
				
		$this->load->model('prntfpajakbhnbaku/mclass');
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		$data['query']	= $this->mclass->clistpenjualanndo($nofaktur);
		if ($fpengganti == '')
			$fpengganti = 'n';
		$data['fpengganti']	= $fpengganti;
		
		$data['isi']	='prntfpajakbhnbaku/vlistform';
		$this->load->view('template',$data);
		
	}
	
	function cpenjualanndo() {			
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_fpajakbhnbaku']	= $this->lang->line('page_title_fpajakbhnbaku');
		$data['form_title_detail_fpajakbhnbaku']	= $this->lang->line('form_title_detail_fpajakbhnbaku');
		$data['list_fpajakbhnbaku_faktur']	= $this->lang->line('list_fpajakbhnbaku_faktur');
		$data['list_fpajakbhnbaku_kd_brg']	= $this->lang->line('list_fpajakbhnbaku_kd_brg');
		$data['list_fpajakbhnbaku_jenis_brg']	= $this->lang->line('list_fpajakbhnbaku_jenis_brg');
		$data['list_fpajakbhnbaku_s_produksi'] = $this->lang->line('list_fpajakbhnbaku_s_produksi');
		$data['list_fpajakbhnbaku_satuan']	= $this->lang->line('list_fpajakbhnbaku_satuan');
		$data['list_fpajakbhnbaku_nm_brg']	= $this->lang->line('list_fpajakbhnbaku_nm_brg');
		$data['list_fpajakbhnbaku_qty']	= $this->lang->line('list_fpajakbhnbaku_qty');
		$data['list_fpajakbhnbaku_hjp']	= $this->lang->line('list_fpajakbhnbaku_hjp');
		$data['list_fpajakbhnbaku_amount']	= $this->lang->line('list_fpajakbhnbaku_amount');
		$data['list_fpajakbhnbaku_total_pengiriman']	= $this->lang->line('list_fpajakbhnbaku_total_pengiriman');
		$data['list_fpajakbhnbaku_total_penjualan']	= $this->lang->line('list_fpajakbhnbaku_total_penjualan');
			
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();
		
		$nofaktur	= $this->uri->segment(4); 
		$fpengganti	= $this->uri->segment(5); 
		$data['nofaktur']	= $nofaktur;
				
		$this->load->model('prntfpajakbhnbaku/mclass');
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		$data['query']	= $this->mclass->clistpenjualanndo($nofaktur);
		$data['fpengganti']	= $fpengganti;
		$data['isi']	='prntfpajakbhnbaku/vprintform';
		$this->load->view('template',$data);

	}	

	function cpopup() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		/*
		Nomor Seri Faktur Pajak : 010.000.10.00000001
		011	= pada digit titik pertama jensi faktur, 011 jika faktur pengganti
		10	= pada digit titik ke-tiga adalah tahun sekarang
		*/
		
		$this->load->model('prntfpajakbhnbaku/mclass');
		
		$iduserid	= $this->session->userdata('user_idx');	
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.194';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;	
			
		$data['page_title_fpajakbhnbaku']	= $this->lang->line('page_title_fpajakbhnbaku');
		$data['form_title_detail_fpajakbhnbaku']	= $this->lang->line('form_title_detail_fpajakbhnbaku');
		$data['list_fpajakbhnbaku_faktur']	= $this->lang->line('list_fpajakbhnbaku_faktur');
		$data['list_fpajakbhnbaku_kd_brg']	= $this->lang->line('list_fpajakbhnbaku_kd_brg');
		$data['list_fpajakbhnbaku_jenis_brg']	= $this->lang->line('list_fpajakbhnbaku_jenis_brg');
		$data['list_fpajakbhnbaku_s_produksi'] = $this->lang->line('list_fpajakbhnbaku_s_produksi');
		$data['list_fpajakbhnbaku_satuan']	= $this->lang->line('list_fpajakbhnbaku_satuan');
		$data['list_fpajakbhnbaku_nm_brg']	= $this->lang->line('list_fpajakbhnbaku_nm_brg');
		$data['list_fpajakbhnbaku_qty']	= $this->lang->line('list_fpajakbhnbaku_qty');
		$data['list_fpajakbhnbaku_hjp']	= $this->lang->line('list_fpajakbhnbaku_hjp');
		$data['list_fpajakbhnbaku_amount']	= $this->lang->line('list_fpajakbhnbaku_amount');
		$data['list_fpajakbhnbaku_total_pengiriman']	= $this->lang->line('list_fpajakbhnbaku_total_pengiriman');
		$data['list_fpajakbhnbaku_total_penjualan']	= $this->lang->line('list_fpajakbhnbaku_total_penjualan');
			
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();

		$qpenyetorpajak		= $this->mclass->penyetorpajak();
		if($qpenyetorpajak->num_rows()>0) {
			$rpenyetorpajak	= $qpenyetorpajak->row();
			$data['TtdPajak01']	= $rpenyetorpajak->e_penyetor;
		}else{
			$data['TtdPajak01']	= $this->lang->line('TtdPajak01');
		}
		
		$data['TtdPajak02']	= $this->lang->line('TtdPajak02');
		$data['falamat']	= $this->lang->line('AddInisial');
				
		$nofaktur	= $this->uri->segment(4); 
		$fpengganti	= $this->uri->segment(5);
		
		$this->mclass->fprinted($nofaktur);
	
		$qry_source_remote	= $this->mclass->remote($iduserid);

		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;
				
		$qry_infoheader		= $this->mclass->clistfpenjndo_header($nofaktur);
		
		if($qry_infoheader->num_rows() > 0){
			$bglobal	= array(
						'01'=>'Januari',
						'02'=>'Februari',
						'03'=>'Maret',
						'04'=>'April',
						'05'=>'Mei',
						'06'=>'Juni',
						'07'=>'Juli',
						'08'=>'Agustus',
						'09'=>'September',
						'10'=>'Oktober',
						'11'=>'Nopember',
						'12'=>'Desember');

			$tanggal= date("d");
			$bulan	= date("m");
			$tahun	= date("Y");
			
			$tgl	= substr($tanggal,0,1)=='0'?substr($tanggal,1,1):$tanggal;
			$nw_tgl	= $tgl;
			$bln	= $bglobal[$bulan];
			
			$data['ftgl']	= $nw_tgl." ".$bln." ".$tahun;
			
			$row_infoheader	= $qry_infoheader->row();
			$nofaktur		= $row_infoheader->ifakturcode;
			$tglfaktur		= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$tglpajak		= $row_infoheader->dpajak;
											
			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$exp_tpajak		= explode("-",$tglpajak,strlen($tglpajak));	
			
			$tahunpajak	= !empty($exp_tpajak[0])?substr($exp_tpajak[0],2,2):date("y");
			
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];

			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']		= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;			
		}else{
			$data['ftgl']			= "";
			$data['nomorfaktur']	= "";
			$data['tglfaktur']		= "";
			$data['tgljthtempo']	= "";
		}
		
		$qry_jml	= $this->mclass->clistfpenjndo_jml($nofaktur);
		$qry_fincludeppn = $this->mclass->fincludeppn($nofaktur);
		
		if($qry_jml->num_rows() > 0 ) {
			
			$row_jml = $qry_jml->row_array();
			$row_fincludeppn = $qry_fincludeppn->row_array();
			
			$total	 = $row_jml['total'];
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			if($row_fincludeppn['fincludeppn']=='t') {
				$total_pls_disc	= $total - $v_disc; // DPP
				$nilai_ppn		= (($total_pls_disc*10) / 100); // PPN
				$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			}else{
				$total_pls_disc	= $total - $v_disc; // DPP
				$nilai_ppn		= 0; // PPN
				$nilai_faktur	= $total_pls_disc + $nilai_ppn;				
			}
			
			/*  17122011
			$data['jumlah']		= $total;
			$data['dpp']		= $total_pls_disc;
			$data['diskon']		= $v_disc;
			$data['nilai_ppn']	= $nilai_ppn;
			$data['nilai_faktur']	= round($nilai_faktur);		
			*/ 

			$data['jumlah']		= round($total,2);
			$data['dpp']		= round($total_pls_disc,2);
			$data['diskon']		= round($v_disc,2);
			$data['nilai_ppn']	= round($nilai_ppn,2);
			$data['nilai_faktur']	= round($nilai_faktur,2);			
		}else{
			$data['jumlah']		= 0;
			$data['dpp']		= 0;
			$data['diskon']		= 0;
			$data['nilai_ppn']	= 0;
			$data['nilai_faktur']	= 0;
		}

		$ititas		= $this->mclass->ititas();
		if($ititas->num_rows() > 0) {
			$row_ititas	= $ititas->row();
			$data['nminisial']	= $row_ititas->e_initial_name;
			$data['almtperusahaan']	= $row_ititas->e_initial_address;
			$data['npwpperusahaan']	= $row_ititas->e_initial_npwp;
		}else{
			$data['nminisial']	= "";
			$data['almtperusahaan']	= "";
			$data['npwpperusahaan']	= "";
		}

		$nopajak	= $this->mclass->pajak($nofaktur);
		if($nopajak->num_rows() > 0) {
			$row_pajak	= $nopajak->row();			
			$nomorpajak	= $row_pajak->ifakturpajak;			
			
			switch(strlen($nomorpajak)) {
				case "1":
					$nfaktur	= '0000000'.$nomorpajak;
				break;
				case "2":
					$nfaktur	= '000000'.$nomorpajak;
				break;
				case "3":
					$nfaktur	= '00000'.$nomorpajak;
				break;
				case "4":
					$nfaktur	= '0000'.$nomorpajak;
				break;
				case "5":
					$nfaktur	= '000'.$nomorpajak;
				break;
				case "6":
					$nfaktur	= '00'.$nomorpajak;
				break;
				case "7":
					$nfaktur	= '0'.$nomorpajak;
				break;
				default:
					$nfaktur	= $nomorpajak;
			}
			
			// +++++++++++++++++++ 23-03-2013, ambil data format faktur pajak (khusus faktur >= april 2013) ++++++++++++++++++++++++++++++++++++++
			$query=$this->db->query(" SELECT d_faktur FROM tm_faktur_bhnbaku WHERE i_faktur_code='$nofaktur' ");
			if ($query->num_rows() > 0) {
				$hasilrow = $query->row();
				$d_faktur = $hasilrow->d_faktur;
			}
			else
				$d_faktur = '';
			
			if ($d_faktur != '' && $d_faktur >='2013-04-01') { // mulai dari faktur april 2013
				$query=$this->db->query(" SELECT * FROM tr_format_pajak ");
				if ($query->num_rows() > 0) {
					$hasilrow = $query->row();
					$segmen1 = $hasilrow->segmen1;
					$noawal = $hasilrow->nourut_awal;
					$noakhir = $hasilrow->nourut_akhir;
				}
				
				// 1. cek no fakturnya di tabel tm_nomor_pajak_faktur, apakah udh ada atau blm. Jika blm ada, maka generate nomor urut baru, kemudian insert baru
				// 2. Ambil data terakhir dari tm_nomor_pajak_faktur, cek nomor faktur pajak terakhir
				$query=$this->db->query(" SELECT * FROM tm_nomor_pajak_faktur WHERE i_faktur_code = '$nofaktur' ");
				if ($query->num_rows() == 0) {
					// ambil no urut faktur pajak terbaru berdasarkan id
					$query3	= $this->db->query(" SELECT * FROM tm_nomor_pajak_faktur ORDER BY id DESC LIMIT 1 ");
					if ($query3->num_rows() > 0) {
						$hasilrow3 = $query3->row();
						$last_id	= $hasilrow3->id;
						$last_nomor_pajak	= $hasilrow3->nomor_pajak;
					}
					else {
						$last_id = 0;
						$last_nomor_pajak = '';
					}
					
					if ($last_nomor_pajak != '')
						$nourutpajak = substr($last_nomor_pajak, 11, 8);
					else
						$nourutpajak = $noawal;
					
					if ($nourutpajak == $noakhir) {
						print "<script>alert(\"Nomor urut faktur pajak sudah mencapai jatah maksimum, hubungi kantor pajak.\");</script>";
						exit;
					}
					else {
						if ($last_nomor_pajak != '')
							$new_nourutpajak = $nourutpajak+1;
						else
							$new_nourutpajak = $noawal;
							
						$new_id = $last_id+1;
						
						$exp_segmen1 = explode(".",$segmen1);
						$kodeawal1= $exp_segmen1[0];
						$kodeawal2= $exp_segmen1[1];
						
						if ($fpengganti == 'y') {
							$kodeawal = '011';
						}
						else {
							$kodeawal = '010';
						}
						
						$new_segmen1 = $kodeawal.".".$kodeawal2;
						
						$new_nomorpajak = $new_segmen1."-".$tahunpajak.".".$new_nourutpajak;
						//echo $new_nomorpajak; die();

						$str = array(
							'id'=>$new_id,
							'i_faktur_code'=>$nofaktur,
							'nomor_pajak'=>$new_nomorpajak
						);
						$this->db->insert('tm_nomor_pajak_faktur',$str);
					}
					
					$data['nomorpajak'] = $new_nomorpajak;
				}
				else {
					$hasilrow = $query->row();
					$nomor_pajaknya	= $hasilrow3->nomor_pajak;
					$data['nomorpajak'] = $nomor_pajaknya;
				}
			 } // end if
			//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			 else {
				if ($fpengganti == 'y') {
					$kodeawal = '011';
				}
				else {
					$kodeawal = '010';
				}
				$data['nomorpajak']	= $kodeawal."."."000"."-".$tahunpajak.".".$nfaktur;
			 }
			
		/*	if ($fpengganti == 'y')
				$kodeawal = '011';
			else
				$kodeawal = '010';
			
			$data['nomorpajak']	= $kodeawal.'.'.'000'.'-'.$tahunpajak.'.'.$nfaktur;	*/
		} else {
			$data['nomorpajak']	= "";		
		}
		
		$qrypelanggan	= $this->mclass->pelanggan($nofaktur);

		if($qrypelanggan->num_rows() >0) {
			$row_pelanggan		= $qrypelanggan->row_array();			
			$data['nmkostumer']	= $row_pelanggan['customername'];
			$data['almtkostumer']	= $row_pelanggan['customeraddress'];
			$data['npwp']		= $row_pelanggan['npwp'];
		}else{
			$data['nmkostumer']	= "";
			$data['almtkostumer']	= "";
			$data['npwp']		= "";
		}
		
		$data['opt_jns_brg']= $this->mclass->lklsbrg();
		$data['isi']		= $this->mclass->clistpenjualanndo2($nofaktur);
		
		$this->load->view('prntfpajakbhnbaku/vtestform',$data);
		
	}	
}
?>
