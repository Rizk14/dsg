<?php
class Cform extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('realisasi-cutting/mmaster');
  }

  function index(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$id			= $this->input->post('id', TRUE);
	$id_detail	= $this->input->post('id_detail', TRUE);
	$ischheader2= $this->input->post('ischheader', TRUE);
	$norealisasi= $this->input->post('norealisasi', TRUE);
	$proses_submit	= $this->input->post('submit', TRUE);
	$iditem			= explode(";", $id_detail);
	$ischhead		= explode(";", $ischheader2);
	$data['ischhead']	= $ischhead;
	//$idx			= explode(";", $id);
	$data['id']		= $id;
	$jenis_schedule	= $this->input->post('jenis_schedule', TRUE);
	if ($proses_submit=="Proses"){
		if ($id !=''){
			//$data['xx']	= $id;
			$data['schedule_detail'] = $this->mmaster->get_detail_schedule($id, $iditem,$ischhead);
			$data['msg'] = '';
			
			$thn	= date("Y");
			$t	= $this->mmaster->thnrealisasi();
			$n	= $this->mmaster->nomorrealisasi();
			
			if($t->num_rows()>0){
				$row_t	= $t->row();
				$row_n	= $n->row();

				if($thn==$row_t->tahun){
					$nomer		= $row_n->nomor+1;
					
					switch(strlen($nomer)){
						case "1":
							$nomor	= "0000".$nomer;
						break;
						case "2":
							$nomor	= "000".$nomer;
						break;
						case "3":
							$nomor	= "00".$nomer;
						break;
						case "4":
							$nomor	= "0".$nomer;
						break;
						case "5":
							$nomor	= $nomer;
						break;
					}
					$nomernya	= $nomor;
					$tahunnya	= $row_t->tahun;
				}else{
					$nomernya	= "00001";
					$tahunnya	= $thn;
				}
				$data['norealisasi']	= "RC"."-".$tahunnya.$nomernya;
			}else{
				$data['norealisasi']	= "RC"."-".$thn."00001";
			}
		}
		else {
			$data['msg'] = 'Item harus dipilih';
		}
		$data['go_proses'] = '1';
		$data['jenis_schedule'] = $jenis_schedule;
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
	}
	
	$data['isi'] = 'realisasi-cutting/vmainform';
	$this->load->view('template',$data);
  }
  
  function show_popup_brg(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$query	= array();
  		$keywordcari	= $this->input->post('cari')?$this->input->post('cari'):"0";
		$posisi 	= $this->uri->segment(4);
		$qjum_total = $this->mmaster->barangjditanpalimit($keywordcari);
		$jum_total	= $qjum_total->num_rows();
		
		$config['base_url'] 	= base_url()."index.php/realisasi-cutting/cform/show_popup_brg/".$keywordcari."/";
		$config['total_rows'] 	= $jum_total; 
		$config['per_page'] 	= '10';
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $this->uri->segment(5);
		$this->pagination->initialize($config);		
		$data['query'] 		= $this->mmaster->barangjdi($config['per_page'],$config['cur_page'], $keywordcari);
		$data['jum_total']	= count($jum_total);
		$data['posisi']	= $posisi;
		
		if ($keywordcari=='0')
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$this->load->view('realisasi-cutting/vpopupbrg',$data);  	
  }
  
  function edit(){ 
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$id 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_realisasi_cutting($id);
	$data['msg'] = '';

	$data['isi'] = 'realisasi-cutting/veditform';
	$this->load->view('template',$data);
  }
  
  function updatedata() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$this->load->library('form_validation');
	//$this->form_validation->set_rules('norealisasi', 'Nomor Realisasi', 'required');

	//if ($this->form_validation->run() == FALSE) {
	//} else {  
		$norealisasi 	= $this->input->post('norealisasi', TRUE);
		$tglrealisasi 	= $this->input->post('tglrealisasi', TRUE);
		$ket_pekerjaan 	= $this->input->post('ket_pekerjaan', TRUE);
		$pisah1 = explode("-", $tglrealisasi);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tglrealisasi = $thn1."-".$bln1."-".$tgl1;
		
		$is_dacron 	= $this->input->post('is_dacron', TRUE);
		$no_bonm 		= $this->input->post('nobonm', TRUE);
		$qty_dacron 	= $this->input->post('qty_dacron', TRUE);
		$qty_dacron_lama = $this->input->post('qty_dacron_lama', TRUE);
		$idheader		= $this->input->post('idheader', TRUE);
		$iterasi		= $this->input->post('iterasi', TRUE);
		$no		= $this->input->post('no', TRUE);

		//$jumlah_input	= $iterasi;
		$jumlah_input	= $no-1;
		
		$iddetail	= array();
		//$nmbrgjadi	= array();
		$imotif	= array();
		$nama_brg_jadi_other	= array();
		$qty		= array();
		$qty_klr = array();
		$qty_klr_lama = array();
		//$nmbrg	= array();
		$kode		= array();
		$pjg_kain	= array();
		//$hide_pjg_kain = array();
				
		$gelaran	= array();
		$set		= array();
		$jml_gelar		= array();
		$hide_jml_gelar = array();
		
		$hsl_potong		= array();
		$hide_hsl_potong = array();
		
		$sisa_planning= array();
		$aktual	= array();
		$sisa_allow= array();
		$allowance= array();
		$sisa_selisih= array();
		
		$jammulai		= array();
		$jamselesai		= array();
		$keterangan		= array();
		$id_detail		= array();
		$id_schedule_cutting	= array();
		
		$jmlgelarlama	= array();
		$hasilpotonglama = array();
		$qtylama = array();
		$id_apply_stok_hsl_cutting	= array();
		
		$tgl	= date("Y-m-d");
		$update	= 0;
		$ii		= 1;
		
		if ($qty_dacron == '')
			$qty_dacron = 0;
			
		$this->db->query(" UPDATE tt_realisasi_cutting SET no_realisasi='$norealisasi', tgl_realisasi = '$tglrealisasi', 
							qty_dacron = '$qty_dacron', ket_pekerjaan_operator = '$ket_pekerjaan', 
							tgl_update = '$tgl'
							WHERE id='$idheader' "); //
		
		if($update==0){
			$this->db->query(" UPDATE tm_apply_stok_hasil_cutting SET tgl_update='$tgl' WHERE 
							status_stok='f' AND id_realisasi_cutting='$idheader' AND no_bonm='$no_bonm' "); // ???
			
			//$q_idnya	= $this->db->query(" SELECT id FROM tm_apply_stok_hasil_cutting WHERE no_bonm='$no_bonm' AND status_stok='f' AND id_realisasi_cutting='$idheader' "); 
			//$r_idnya	= $q_idnya->row();
			
			$update	= 1;
		}
		$tgl = date("Y-m-d");
		// 1. reset stoknya #############################################
		//cek stok terakhir tm_stok, dan update stoknya
		if ($is_dacron == 't' && $qty_dacron != $qty_dacron_lama) {
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '".$this->input->post('kode_1', TRUE)."' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama+$qty_dacron_lama; // menambah stok krn reset
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$this->input->post('kode_1', TRUE),
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
					where kode_brg= '".$this->input->post('kode_1', TRUE)."' ");
				}
				
				// &&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
								$query4	= $this->db->query(" SELECT id, kode_brg, masuk, keluar, harga FROM tt_stok WHERE 
								kode_brg = '".$this->input->post('kode_1', TRUE)."' AND no_bukti = '$norealisasi' ORDER BY id DESC ");
									if ($query4->num_rows() > 0){
										$hasil4=$query4->result();
										
										foreach ($hasil4 as $row4) {
											$ttmasuk = $row4->masuk;
											$ttkeluar = $row4->keluar;
											$ttharga = $row4->harga;
											
											if ($ttmasuk != '')
												break;
											
											$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE 
																kode_brg = '$row4->kode_brg' AND harga = '$ttharga' ");
											if ($query3->num_rows() == 0){
												$stok_lama = 0;
											}
											else {
												$hasilrow = $query3->row();
												$stok_lama	= $hasilrow->stok;
											}
											$stokreset = $stok_lama+$ttkeluar;
											
											$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
												where kode_brg= '$row4->kode_brg' AND harga = '$ttharga' "); //
									
											$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
													VALUES ('$row4->kode_brg','$norealisasi', '$ttkeluar', '$stokreset', '$tgl', '$ttharga') ");
										}
									}
				// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		// 2. update stok terbaru
		//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '".$this->input->post('kode_1', TRUE)."' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama-$qty_dacron; // mengurangi stok
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$this->input->post('kode_1'.$i, TRUE),
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok', $data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
					where kode_brg= '".$this->input->post('kode_1', TRUE)."' ");
				}
				
				// &&&&&&&&&&&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&
					$selisih = 0;
					$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE 
										kode_brg = '".$this->input->post('kode_1', TRUE)."' ORDER BY id ASC ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
							
						$temp_selisih = 0;				
						foreach ($hasil2 as $row2) {
							$stoknya = $row2->stok; // data ke-1: 500		data ke-2: 10
							$harganya = $row2->harga; // data ke-1: 20000	data ke-2: 370000
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) // temp_selisih = -6
									$selisih = $stoknya-$qty_dacron; // selisih1 = 500-506 = -6
								else
									$selisih = $stoknya+$temp_selisih; // selisih2 = 10-6 = 4
								
								if ($selisih < 0) {
									$temp_selisih = $selisih; // temp_selisih = -6
									$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '".$this->input->post('kode_1', TRUE)."' AND harga = '$harganya' "); //
									
									$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
											VALUES ('".$this->input->post('kode_1', TRUE)."','$norealisasi', '$stoknya', '0', '$tgl', '$harganya') ");
								}
									
								if ($selisih > 0) { // ke-2 masuk sini
									if ($temp_selisih == 0)
										$temp_selisih = $qty_dacron; // temp_selisih = -6
										
									break;
								}
							}
						} // end for
					}
					
					if ($selisih != 0) {
						$new_stok = $selisih; // new_stok = 4
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '".$this->input->post('kode_1', TRUE)."' AND harga = '$harganya' ");
						
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
								VALUES ('".$this->input->post('kode_1', TRUE)."','$norealisasi', '".abs($temp_selisih)."', 
								'$new_stok', '$tgl', '$harganya') ");
					}
				// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end modifikasi 5 okt 2011
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	} // end if
	
		for($i=0;$i<$jumlah_input;$i++){
			
			$iddetail[$ii]	= $this->input->post('i_'.$ii, TRUE);
			//$nmbrgjadi[$ii]	= $this->input->post('nmbrgjadi_'.$ii, TRUE);
			$imotif[$ii]	= $this->input->post('imotif_'.$ii, TRUE);
			$nama_brg_jadi_other[$ii]	= $this->input->post('nama_brg_jadi_other_'.$ii, TRUE);
			$qty[$ii]		= $this->input->post('qty_'.$ii, TRUE);
			$qty_klr[$ii]		= $this->input->post('qty_klr_'.$ii, TRUE);
			$qty_klr_lama[$ii]	= $this->input->post('qty_klr_lama_'.$ii, TRUE);
			//$nmbrg[$ii]	= $this->input->post('nmbrg_'.$ii, TRUE);
			$kode[$ii]		= $this->input->post('kode_'.$ii, TRUE);
			$jml_lot[$ii]	= $this->input->post('jml_lot_'.$ii, TRUE);
			$pjg_kain[$ii]	= $this->input->post('pjg_kain_'.$ii, TRUE);
			//$hide_pjg_kain[$ii]	= $this->input->post('hide_pjg_kain_'.$ii, TRUE);
			$gelaran[$ii]	= $this->input->post('gelaran_'.$ii, TRUE);
			$set[$ii]		= $this->input->post('set_'.$ii, TRUE);
			$jml_gelar[$ii]		= $this->input->post('jml_gelar_'.$ii, TRUE);
			$hide_jml_gelar[$ii]		= $this->input->post('hide_jml_gelar_'.$ii, TRUE);
			$hsl_potong[$ii]		= $this->input->post('hsl_potong_'.$ii, TRUE);
			$hide_hsl_potong[$ii]		= $this->input->post('hide_hsl_potong_'.$ii, TRUE);
			//$aktual[$ii]	= $this->input->post('aktual_'.$ii, TRUE);
			
			$sisa_planning[$ii]		= $this->input->post('planning_'.$ii, TRUE);
			$aktual[$ii]		= $this->input->post('aktual_'.$ii, TRUE);
			$sisa_allow[$ii]		= $this->input->post('allow_'.$ii, TRUE);
			$allowance[$ii]		= $this->input->post('allowance_'.$ii, TRUE);
			$sisa_selisih[$ii]		= $this->input->post('selisih_'.$ii, TRUE); //

			$jammulai[$ii]		= $this->input->post('jammulai_'.$ii, TRUE);
			$jamselesai[$ii]		= $this->input->post('jamselesai_'.$ii, TRUE);
			$keterangan[$ii]		= $this->input->post('keterangan_'.$ii, TRUE);
			$id_detail[$ii]		= $this->input->post('id_detail_'.$ii, TRUE);
			$operator[$ii]	= $this->input->post('operator_'.$ii, TRUE);
			$id_schedule_cutting[$ii]	= $this->input->post('id_schedule_cutting_'.$ii, TRUE);
			/*$pjg_kainyard	= $pjg_kain[$ii]/0.91;
			
			$jmlgelarlama[$ii]	= $this->input->post('jmlgelarlama_'.$ii, TRUE);
			$hasilpotonglama[$ii]= $this->input->post('hasilpotonglama_'.$ii, TRUE);
			$qtylama[$ii]	= $this->input->post('qtylama_'.$ii, TRUE); */
			$id_apply_stok_hsl_cutting[$ii]	= $this->input->post('id_apply_stok_hsl_cutting_'.$ii, TRUE);
			
			//$hasilpotong	= $jml_gelar[$ii]*$set[$ii];
			/*$sisaplanning	= $pjg_kain[$ii]-($jml_gelar[$ii]*$gelaran[$ii]);
			$sisaallow	= $jml_gelar[$ii]*2/100;
			$sisaselisih	= $sisaplanning-$aktual[$ii]-$sisaallow; */
			
			/*if ($jml_lot[$ii] == '')
				$jml_lot[$ii] = 0;
			
			if ($pjg_kain[$ii] == '')
				$pjg_kain[$ii] = 0;
			
			if ($gelaran[$ii] == '')
				$gelaran[$ii] = 0;
			
			if ($set[$ii] == '')
				$set[$ii] = 0;
						
			if ($aktual[$ii] == '')
				$aktual[$ii] = 0; */
			
			if ($jml_lot[$ii] == '')
				$jml_lot[$ii] = "0;";
			
			if ($pjg_kain[$ii] == '')
				$pjg_kain[$ii] = "0;";
			
			if ($gelaran[$ii] == '')
				$gelaran[$ii] = "0;";
			
			if ($set[$ii] == '')
				$set[$ii] = "0;";
			
			if ($sisa_planning[$ii] == '')
				$sisa_planning[$ii] = "0;";
			
			if ($aktual[$ii] == '')
				$aktual[$ii] = "0;";
			
			if ($sisa_allow[$ii] == '')
				$sisa_allow[$ii] = "0;";
			
			if ($sisa_selisih[$ii] == '')
				$sisa_selisih[$ii] = "0;";
			
			if ($qty_klr[$ii] == '')
				$qty_klr[$ii] = 0;
									
			//if($imotif[$ii]!='' && $qty[$ii]!='' && $kode[$ii]!='' && $jammulai[$ii]!='' && $jamselesai[$ii]!='' && $pjg_kain[$ii]!=''){
				/*
				$this->db->query(" UPDATE tt_realisasi_cutting_detail SET qty_realisasi='$qty[$ii]',
					jam_mulai='$jammulai[$ii]',
					jam_selesai='$jamselesai[$ii]',
					operator_cutting='$operator[$ii]',
					kode_brg_jadi='$imotif[$ii]',
					kode_brg='$kode[$ii]',
					jum_lot='$jml_lot[$ii]',
					panjang_kain_yard='$pjg_kainyard',
					panjang_kain_mtr='$pjg_kain[$ii]',
					gelaran='$gelaran[$ii]',
					set='$set[$ii]',
					jum_gelar='$jml_gelar[$ii]',
					hasil_potong='$hasilpotong',
					sisa_planning_mtr='$sisaplanning',
					sisa_aktual_mtr='$aktual[$ii]',
					sisa_allow_mtr='$sisaallow',
					sisa_selisih_mtr='$sisaselisih',
					keterangan='$keterangan[$ii]',
					id_schedule_cutting='$id_schedule_cutting[$ii]' WHERE id='$id_detail[$ii]' ");
				*/
				
				/*
				 * sisa_planning_mtr='$sisaplanning',
					sisa_aktual_mtr='$aktual[$ii]',
					sisa_allow_mtr='$sisaallow',
					sisa_selisih_mtr='$sisaselisih',
					* 
				 * */
				 
				 if ($is_dacron == 't') {
					$hasil_potongnya = $hsl_potong[$ii];
					$jml_gelarnya = $jml_gelar[$ii];
					$detail_hasil_potongnya = $hsl_potong[$ii];
					$detail_jml_gelarnya = $jml_gelar[$ii];
				 }
				 else {
					$hasil_potongnya = $hide_hsl_potong[$ii];
					$jml_gelarnya = $hide_jml_gelar[$ii];
					$detail_hasil_potongnya = $hsl_potong[$ii];
					$detail_jml_gelarnya = $jml_gelar[$ii];
				 }
				
				// ini yg sebelumnya di query UPDATE:
				/*
				 *  jum_gelar='$hide_jml_gelar[$ii]',
					hasil_potong='$hide_hsl_potong[$ii]',
					detail_jum_gelar = '$jml_gelar[$ii]', 
					detail_hasil_potong = '$hsl_potong[$ii]',
				 * */
				$this->db->query(" UPDATE tt_realisasi_cutting_detail SET 
									jam_mulai='$jammulai[$ii]',
									jam_selesai='$jamselesai[$ii]',
									operator_cutting='$operator[$ii]',
									jum_lot='$jml_lot[$ii]',
									gelaran='$gelaran[$ii]',
									set='$set[$ii]',
									jum_gelar='$jml_gelarnya',
									hasil_potong='$hasil_potongnya',
									keterangan='$keterangan[$ii]',
									
									detail_panjang_kain_mtr = '$pjg_kain[$ii]',
									detail_jum_gelar = '$detail_jml_gelarnya', 
									detail_hasil_potong = '$detail_hasil_potongnya',
									
									sisa_planning_mtr = '$sisa_planning[$ii]',
									sisa_aktual_mtr = '$aktual[$ii]',
									sisa_allow_mtr = '$sisa_allow[$ii]',
									allowance = '$allowance[$ii]',
									sisa_selisih_mtr = '$sisa_selisih[$ii]',
									
									qty_klr = '$qty_klr[$ii]'
									WHERE id='$id_detail[$ii]' "); // 
									
				if($update==1){
					$this->db->query(" UPDATE tm_apply_stok_hasil_cutting_detail SET qty='$hasil_potongnya' 
								WHERE status_stok='f' AND id='$id_apply_stok_hsl_cutting[$ii]' ");
				}
				
				// update stok jika qty_klr != 0
				if ($qty_klr[$ii] != 0 && $qty_klr_lama[$ii] != 0 && $qty_klr[$ii] != $qty_klr_lama[$ii]) {
					// 1. reset stoknya #############################################
						//cek stok terakhir tm_stok, dan update stoknya (ini blm ada pengecekan ke tabel brg quilting, blm perlu)
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode[$ii]' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty_klr_lama[$ii]; // menambah stok krn reset
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$kode[$ii],
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode[$ii]' ");
							}
							
							// &&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
								$query4	= $this->db->query(" SELECT id, kode_brg, masuk, keluar, harga FROM tt_stok WHERE 
								kode_brg = '$kode[$ii]' AND no_bukti = '$norealisasi' ORDER BY id DESC ");
									if ($query4->num_rows() > 0){
										$hasil4=$query4->result();
										
										foreach ($hasil4 as $row4) {
											$ttmasuk = $row4->masuk;
											$ttkeluar = $row4->keluar;
											$ttharga = $row4->harga;
											
											if ($ttmasuk != '')
												break;
											
											$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE 
																kode_brg = '$row4->kode_brg' AND harga = '$ttharga' ");
											if ($query3->num_rows() == 0){
												$stok_lama = 0;
											}
											else {
												$hasilrow = $query3->row();
												$stok_lama	= $hasilrow->stok;
											}
											$stokreset = $stok_lama+$ttkeluar;
											
											$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
												where kode_brg= '$row4->kode_brg' AND harga = '$ttharga' "); //
									
											$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
													VALUES ('$row4->kode_brg','$norealisasi', '$ttkeluar', '$stokreset', '$tgl', '$ttharga') ");
										}
									}
					// 2. update stok baru
					$new_stok_skrg = $new_stok-$qty_klr[$ii];
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok_skrg', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode[$ii]' ");
					
					// &&&&&&&&&&&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&
							$selisih = 0;
							//if ($quilting == 'f')
								$sql = "SELECT id, stok, harga FROM tm_stok_harga WHERE 
										kode_brg = '$kode[$ii]' ORDER BY id ASC";
							/*else
								$sql = "SELECT id, stok, harga FROM tm_stok_harga WHERE 
										kode_brg_quilting = '$kode[$ii]' AND quilting = 't' ORDER BY id ASC"; */
										
							$query2	= $this->db->query($sql);
							if ($query2->num_rows() > 0){
								$hasil2=$query2->result();
									
								$temp_selisih = 0;				
								foreach ($hasil2 as $row2) {
									$stoknya = $row2->stok; 
									$harganya = $row2->harga;
									
									if ($stoknya > 0) { 
										if ($temp_selisih == 0)
											$selisih = $stoknya-$qty_klr[$ii]; 
										else
											$selisih = $stoknya+$temp_selisih; 
										
										if ($selisih < 0) {
											$temp_selisih = $selisih; //
										//	if ($quilting == 'f')
												$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
														where kode_brg= '$kode[$ii]' AND harga = '$harganya' ");
										/*	else
												$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
														where kode_brg_quilting= '$kode[$x]' AND harga = '$harganya' "); */
											
											$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
													VALUES ('$kode[$ii]','$norealisasi', '$stoknya', '0', '$tglrealisasi', '$harganya') ");
										}
											
										if ($selisih > 0) { // ke-2 masuk sini
											if ($temp_selisih == 0)
												$temp_selisih = $qty_klr[$ii]; // 
											break;
										}
									}
								} // end for
							}
							
							if ($selisih != 0) {
								$new_stok = $selisih; // 
								//if ($quilting == 'f')
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok_skrg', tgl_update_stok = '$tgl' 
										where kode_brg= '$kode[$ii]' AND harga = '$harganya' ");
								/*else
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg_quilting= '$kode[$x]' AND harga = '$harganya' "); */
								
								$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
														VALUES ('$kode[$ii]','$norealisasi', '".abs($temp_selisih)."', 
														'$new_stok_skrg', '$tglrealisasi', '$harganya') ");
							}
							// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end modifikasi 5 okt 2011
					
				} // end if $qty_klr
					
				/*** agus
				$qstokhslcutting = $this->db->query(" SELECT * FROM tm_stok_hasil_cutting WHERE kode_brg_jadi='$imotif[$ii]' AND kode_brg='$kode[$ii]' ");
				if($qstokhslcutting->num_rows()>0){
					$rstokhslcutting = $qstokhslcutting->row();
					$stokhslcutting_new = (($rstokhslcutting->stok)-($qtylama[$ii]))+$qty[$ii];
					$jmlgeluarhslcutting_new = (($rstokhslcutting->total_gelar)-$jmlgelarlama[$ii])+$jml_gelar[$ii];
					
					$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok='$stokhslcutting_new', total_gelar='$jmlgeluarhslcutting_new' WHERE kode_brg_jadi='$imotif[$ii]' AND kode_brg='$kode[$ii]' AND id='$rstokhslcutting->id' ");	
					
					//$qttstokhsljahit = $this->db->query(" SELECT * FROM tt_stok_hasil_cutting WHERE kode_brg_jadi='$imotif[$ii]' AND no_bukti='$norealisasi' ");	
					//if($qttstokhsljahit->num_rows()>0){
						//$rttstokhsljahit = $qttstokhsljahit->row();
						//$jml_gelar_keluar = $rttstokhsljahit->jum_gelar_masuk;
						//$jml_potong_keluar  = $rttstokhsljahit->jum_potong_masuk;
					//}
					//$this->db->query(" INSERT INTO tt_stok_hasil_cutting (kode_brg_jadi, no_bukti, jum_gelar_masuk, jum_potong_masuk, jum_gelar_keluar, jum_potong_keluar, saldo) VALUES('$imotif[$ii]','$norealisasi','$jml_gelar[$ii]','$hasilpotong','$jml_gelar_keluar','$jml_potong_keluar','$stokhslcutting_new') ");
					
					//$this->db->query(" INSERT INTO tt_stok_hasil_cutting (kode_brg_jadi, no_bukti, jum_gelar_masuk, jum_potong_masuk, jum_gelar_keluar, jum_potong_keluar, saldo) VALUES('$imotif[$ii]','$norealisasi','$jml_gelar[$ii]','$hasilpotong','$jmlgelarlama[$ii]','$hasilpotonglama[$ii]','$stokhslcutting_new') ");
					$this->db->query(" INSERT INTO tt_stok_hasil_cutting (kode_brg_jadi, kode_brg, no_bukti, masuk, keluar, saldo) VALUES('$imotif[$ii]','$kode[$ii]','$norealisasi','$hasilpotong','$hasilpotonglama[$ii]','$stokhslcutting_new') ");
				}
				***/

				// 25-01-2012
				/*$qrealisasi_cut_detail	= $this->db->query(" SELECT sum(hasil_potong) AS total_qty_realisasi FROM tt_realisasi_cutting_detail WHERE id_schedule_cutting_detail='$id_schedule_cutting[$ii]' ");
				if($qrealisasi_cut_detail->num_rows()>0){
					$rrealisasi_cut_detail	= $qrealisasi_cut_detail->row();
					$total_qty_realisasi	= $rrealisasi_cut_detail->total_qty_realisasi;
					
					$qschedule_cut_detail	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id='$id_schedule_cutting[$ii]' ");
					if($qschedule_cut_detail->num_rows()>0){
						$rscheudle_cut_detail	= $qschedule_cut_detail->row();
						$qty_plan_sche	= $rscheudle_cut_detail->qty_bhn;
					}else{
						$qty_plan_sche	= 0;
					}
					
					$belum_realisasi	= $qty_plan_sche - $total_qty_realisasi;
					
					if($belum_realisasi>0){
						$status_realisasi	= 'f';
					}else{
						$status_realisasi	= 't';
					}
					
					$this->db->query(" UPDATE tt_schedule_cutting_detail SET status_realisasi='$status_realisasi' WHERE id='$id_schedule_cutting[$ii]' ");
				} */
							
			//}
			$ii++;	
		}
	//}	
	redirect('realisasi-cutting/cform/view');
  }
  
  function show_popup_schedule() {	
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$query	= array();
		$jenisnya 	= $this->uri->segment(4);
		if ($jenisnya == '')
			$jenisnya = $this->input->post('jenisnya', TRUE);
		
  		$keywordcari	= $this->input->post('cari');
  		if ($keywordcari == '' && $jenisnya == '') {
			$jenisnya 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
  		}
  		if ($keywordcari == '')
			$keywordcari 	= "all";
  		
		$jum_total = $this->mmaster->get_scheduletanpalimit($jenisnya, $keywordcari);
		$config['base_url'] 	= base_url()."index.php/realisasi-cutting/cform/show_popup_schedule/".$jenisnya."/".$keywordcari."/";
		$config['total_rows'] 	= count($jum_total); 
		$config['per_page'] 	= '10';
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $this->uri->segment(6);
		$this->pagination->initialize($config);		
		$data['query'] 		= $this->mmaster->get_schedule($config['per_page'],$this->uri->segment(6), $jenisnya, $keywordcari);
		$data['jum_total']	= count($jum_total);

		if ($keywordcari=="all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		
		$data['jenisnya'] = $jenisnya;

		$this->load->view('realisasi-cutting/vpopupschedule',$data);
  }

  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$this->load->library('form_validation');
	//$this->form_validation->set_rules('norealisasi', 'Tanggal Realisasi', 'required');

	//if ($this->form_validation->run()==FALSE) {
	//}else{
		$norealisasi= $this->input->post('norealisasi', TRUE);
		$tglrealisasi= $this->input->post('tglrealisasi', TRUE);
		$nobonm		= $this->input->post('nobonm', TRUE);
		$is_dacron	= $this->input->post('is_dacron', TRUE);
		$qty_dacron	= $this->input->post('qty_dacron', TRUE);
		$ket_pekerjaan	= $this->input->post('ket_pekerjaan', TRUE);
		$idx 		= $this->input->post('id', TRUE); // id schedule
		$iterasi	= $this->input->post('iterasi', TRUE);
		$eksternal	= $this->input->post('eksternal', TRUE);
		
		$pisah1 = explode("-", $tglrealisasi);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tglrealisasi = $thn1."-".$bln1."-".$tgl1;
		
		$cek_data 	= $this->mmaster->cek_data($norealisasi);
		
		if($cek_data->num_rows()>0) { 
			$data['isi'] 	= 'realisasi-cutting/vmainform';
			$data['msg'] 	= "Data no Realisasi ".$norealisasi." sudah ada..!";
			$data['go_proses'] = '';
			$this->load->view('template',$data);
		}else{
			$jumlah_input	= $iterasi;
			
			//$nmbrgjadi	= array();
			$diprint= array();
			$imotif	= array();
			$nama_brg_jadi_other	= array();
			$qty	= array();
			//$nmbrg	= array();
			$kode	= array();
			$qty_klr	= array();
			$is_sisa	= array();
			$jml_lot	= array();
			$pjg_kain	= array();
			//$hide_pjg_kain	= array(); //
			$gelaran	= array();
			$set	= array();
			$jml_gelar	= array();
			$hide_jml_gelar = array();//
			$hsl_potong	= array();
			$hide_hsl_potong = array();//
			
			$sisa_planning= array();
			$aktual	= array();
			$sisa_allow= array();
			$allowance= array();
			$sisa_selisih= array();
			
			$id_bagian_brg_jadi	= array();
			$ukuran_pola	= array();
			$jammulai	= array();
			$jamselesai	= array();
			$keterangan	= array();
			$id_detail	= array();
			$operator	= array();
			
			for($i=0;$i<=$jumlah_input;$i++){
				//$nmbrgjadi[$i]	= $this->input->post('nmbrgjadi_'.$i, TRUE);
				$diprint[$i]	= $this->input->post('diprint_'.$i, TRUE);
				$imotif[$i]	= $this->input->post('imotif_'.$i, TRUE);
				// nama_brg_jadi_other
				$nama_brg_jadi_other[$i]	= $this->input->post('nama_brg_jadi_other_'.$i, TRUE);
				
				$qty[$i]	= $this->input->post('qty_'.$i, TRUE);
				//$nmbrg[$i]	= $this->input->post('nmbrg_'.$i, TRUE);
				$kode[$i]	= $this->input->post('kode_'.$i, TRUE);
				$qty_klr[$i]	= $this->input->post('qty_klr_'.$i, TRUE);
				//31-03-2012
				$is_sisa[$i]	= $this->input->post('sisa_'.$i, TRUE);
				
				$jml_lot[$i]	= $this->input->post('jml_lot_'.$i, TRUE);
				$pjg_kain[$i]	= $this->input->post('pjg_kain_'.$i, TRUE);
				//$hide_pjg_kain[$i]	= $this->input->post('hide_pjg_kain_'.$i, TRUE);
				$gelaran[$i]	= $this->input->post('gelaran_'.$i, TRUE);
				$set[$i]		= $this->input->post('set_'.$i, TRUE);
				$jml_gelar[$i]	= $this->input->post('jml_gelar_'.$i, TRUE);
				$hide_jml_gelar[$i]	= $this->input->post('hide_jml_gelar_'.$i, TRUE);
				$hsl_potong[$i]	= $this->input->post('hsl_potong_'.$i, TRUE);
				$hide_hsl_potong[$i]	= $this->input->post('hide_hsl_potong_'.$i, TRUE);
				$sisa_planning[$i]		= $this->input->post('planning_'.$i, TRUE);
				$aktual[$i]		= $this->input->post('aktual_'.$i, TRUE);
				$sisa_allow[$i]		= $this->input->post('allow_'.$i, TRUE);
				$allowance[$i]		= $this->input->post('allowance_'.$i, TRUE);
				$sisa_selisih[$i]		= $this->input->post('selisih_'.$i, TRUE);
				
				$id_bagian_brg_jadi[$i]	= $this->input->post('id_bagian_brg_jadi_'.$i, TRUE);
				$ukuran_pola[$i]	= $this->input->post('ukuran_pola_'.$i, TRUE);
				
				$jammulai[$i]	= $this->input->post('jammulai_'.$i, TRUE);
				$jamselesai[$i]	= $this->input->post('jamselesai_'.$i, TRUE);
				$keterangan[$i]	= $this->input->post('keterangan_'.$i, TRUE);
				$id_detail[$i]	= $this->input->post('id_detail_'.$i, TRUE);	
				$operator[$i]	= $this->input->post('operator_'.$i, TRUE);
			}
			
			$this->mmaster->save($eksternal, $norealisasi, $tglrealisasi, $diprint, $imotif, $qty, $kode, $jml_lot, $pjg_kain, $gelaran, $set, $jml_gelar, 
							$hsl_potong, $sisa_planning, $aktual, $sisa_allow, $allowance, $sisa_selisih, $id_bagian_brg_jadi, $ukuran_pola, $jammulai, $jamselesai, $keterangan, 
							$id_detail, $idx, $operator, $jumlah_input, $nobonm, $is_dacron, $qty_dacron, $ket_pekerjaan,
							$hide_jml_gelar, $hide_hsl_potong, $qty_klr, $is_sisa, $nama_brg_jadi_other); // 
			//echo $jumlah_input;
		}
	//}
	redirect('realisasi-cutting/cform/view');
  }
  
  function view() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
    $data['isi'] = 'realisasi-cutting/vformview';
	$keywordcari	= $this->input->post('keywordcari');
	
	if($keywordcari=='')
		$keywordcari = '0';
	
	$date_from = "00-00-0000";
	$date_to = "00-00-0000";
		
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/realisasi-cutting/cform/view/index/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $date_from, $date_to);						
	$data['jum_total'] = count($jum_total);
	
	if ($keywordcari=='0')
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;

	$this->load->view('template',$data);
  }
  
  function cari(){
	//$keywordcari 	= $this->input->post('cari', TRUE)?$this->input->post('cari', TRUE):$this->uri->segment(4);  
	$keywordcari 	= $this->input->post('cari', TRUE);
	$date_from 	= $this->input->post('date_from', TRUE);
	$date_to 	= $this->input->post('date_to', TRUE);
	
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(6);
	
	if ($keywordcari=='')
		$keywordcari	= '0';
	
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/realisasi-cutting/cform/cari/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] 		= $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $date_from, $date_to);						
	$data['jum_total'] 	= count($jum_total);
	$data['isi'] 		= 'realisasi-cutting/vformview';
	
	if ($keywordcari=='0')
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
		
	$this->load->view('template',$data);
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('realisasi-cutting/cform/view');
  }
    
  /*function get_qty_op_detail() {		
		$id_op	= $this->input->post('id_op');
		$kode_brg	= $this->input->post('kode_brg');
		$qty_brg	= $this->input->post('qty');

		// ambil qty op_detail
		$query3	= $this->db->query(" SELECT qty FROM tm_op_detail WHERE id_op = '".$id_op."'
		AND kode_brg = '$kode_brg' ");
		
		if($query3->num_rows()>0) {
			$hasilrow = $query3->row();
			$qty_op = $hasilrow->qty;
			//echo $qtynya;
		}
		else
			$qtynya = 0;
		
		// ambil sum qty pembelian detail
			$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b 
							WHERE a.id_pembelian = b.id AND b.id_op = '$id_op' AND a.kode_brg = '".$kode_brg."' ");
		
		if($query3->num_rows()>0) {
			$hasilrow = $query3->row();
			$jum_beli = $hasilrow->jum;
		}
		else
			$jumnya = 0;

		$qty_skrg = $jum_beli + $qty_brg;
		if ($qty_skrg > $qty_op)
			echo "1";
		else
			echo "0";

		return true;
  } */
  
  // popup utk input detail
  function show_popup_detail(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi 	= $this->uri->segment(4);
	$proses 	= $this->uri->segment(5);
	$is_set 	= $this->uri->segment(6);
	
	if ($proses ==1)
		$nama_proses = "Jumlah Lot";
	else if ($proses ==2)
		$nama_proses = "Pjg Kain (m)";
	else if ($proses ==3)
		$nama_proses = "Jml Gelar";
	else if ($proses ==4)
		$nama_proses = "Hasil Potong";
	else if ($proses ==5)
		$nama_proses = "Sisa Aktual (m)";
	$data['posisi'] = $posisi;
	$data['proses'] = $proses;
	$data['nama_proses'] = $nama_proses;
	$data['is_set'] = $is_set;
	$this->load->view('realisasi-cutting/vpopupdetail',$data);
  }
  
    // new 30-01-2012
  function show_popup_detail2(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi 	= $this->uri->segment(4);
	$gelaran 	= $this->uri->segment(5);
	$jum_set 	= $this->uri->segment(6);
	$id_realisasi_detail 	= $this->uri->segment(7);
	
	if ($id_realisasi_detail != '')
		$is_edit = 1;
	else
		$is_edit = 0;

	$data['posisi'] = $posisi;
	$data['gelaran'] = $gelaran;
	$data['jum_set'] = $jum_set;
	$data['id_realisasi_detail'] = $id_realisasi_detail;
	$data['is_edit'] = $is_edit;
	$this->load->view('realisasi-cutting/vpopupdetail2',$data);
  }
  
  // 23-04-2012
  function print_rc(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$id_rc 	= $this->uri->segment(4);	
		$data['query'] = $this->mmaster->get_rc_for_print($id_rc);
		//print_r($data['query']); die();

	$data['id_rc'] = $id_rc;
	$this->load->view('realisasi-cutting/vprintrc',$data);
  }
}
