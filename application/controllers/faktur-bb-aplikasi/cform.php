<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-bb-aplikasi/mmaster');
  }
    
  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	/*$id_op = $this->input->post('id_op', TRUE);  
	$no_op = $this->input->post('no_op', TRUE);   */
	
	$kd_brg = $this->input->post('kd_brg', TRUE);  
	
	$proses_submit = $this->input->post('submit', TRUE); 
	$id_op_detail = $this->input->post('id_brg', TRUE);  
	$list_brg = explode(";", $id_op_detail);
	$ambil_pp = $this->input->post('ambil_pp', TRUE);
	$id_supplier2 = $this->input->post('id_supplier2', TRUE);    
	$is_no_ppop = $this->input->post('is_no_ppop', TRUE);    
	
	// 03-07-2015
	$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);    
	
	if ($proses_submit == "Proses") {
	  if ($is_no_ppop == '') { // jika ambil dari pp ataupun op
		if ($kd_brg !='') {
			if ($ambil_pp == '') {
				// modifikasi 20-06-11, 
				$data['id_supplier']	= $id_supplier2;
				$suppliernya = $id_supplier2;
				
				// 04-07-2015
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_supplier2' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
				}
				else {
					$kode_supplier = '';
					$nama_supplier = "Lain-lain";
				}
				
				$data['kode_supplier']	= $kode_supplier;
				$data['nama_supplier']	= $nama_supplier;
				// -------------------
			}
			else {
				$data['id_supplier']	= $id_supplier2;
				$suppliernya = $id_supplier2;
			}

			$detail_opnya = $this->mmaster->get_detail_op($list_brg, $suppliernya, $ambil_pp);
			$data['msg'] = '';
			$data['supplier'] = $this->mmaster->get_supplier();
			$data['kel_brg'] = $this->mmaster->get_kel_brg();
			
			$data['op_detail'] = $detail_opnya;
			
			$no_ppop = ""; $id_ppop = ""; $temp_no_ppop = ""; $temp_id_ppop = "";
			for($j=0;$j<count($detail_opnya);$j++){
				//if ($ambil_pp == '') {
					if ($detail_opnya[$j]['no_op'] != $temp_no_ppop)
						$no_ppop.= $detail_opnya[$j]['no_op']."; ";
					
					if ($detail_opnya[$j]['id_op'] != $temp_id_ppop)
						$id_ppop.= $detail_opnya[$j]['id_op'].";";
						
					$temp_no_ppop = $detail_opnya[$j]['no_op'];
					$temp_id_ppop = $detail_opnya[$j]['id_op'];
				//}
				
			}			
			
			$data['no_ppop'] = $no_ppop;
			$data['id_ppop'] = $id_ppop;
		}
		
		$data['go_proses'] = '1';
		$data['ambil_pp'] = $ambil_pp;
		$data['jenis_pembelian'] = $jenis_pembelian;
		$data['isi'] = 'faktur-bb-aplikasi/vmainform';
		$this->load->view('template',$data);
      } // end if is_no_ppop == ''	
      else { // jika is_no_ppop == 't'
		$data['is_no_ppop'] = $is_no_ppop;
		$data['id_supplier']	= $id_supplier2;
		$data['supplier'] = $this->mmaster->get_supplier();
		$data['kel_brg'] = $this->mmaster->get_kel_brg();
		$data['jenis_pembelian'] = $jenis_pembelian;
		$data['isi'] = 'faktur-bb-aplikasi/vmainformnoppop';
		$this->load->view('template',$data);
	  }

	}
	else {
		$data['msg'] = '';
		$data['id_op'] = '';
		$data['go_proses'] = '';
		$data['supplier2'] = $this->mmaster->get_supplier();
		
		$data['isi'] = 'faktur-bb-aplikasi/vmainform';
		$this->load->view('template',$data);
	}
  }
  
  function edit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_pembelian_aplikasi 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$csupplier 	= $this->uri->segment(7);
	$tgl_awal 	= $this->uri->segment(8);
	$tgl_akhir 	= $this->uri->segment(9);
	$carinya 	= $this->uri->segment(10);
	$caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
	$data['query'] = $this->mmaster->get_pembelian($id_pembelian_aplikasi);
	$data['supplier'] = $this->mmaster->get_supplier();
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['csupplier'] = $csupplier;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['kel_brg'] = $this->mmaster->get_kel_brg();

	$data['isi'] = 'faktur-bb-aplikasi/veditform';
	$this->load->view('template',$data);

  }
  
  function edittgl(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	$is_simpan 	= $this->input->post('is_simpan', TRUE);
	
	if ($is_simpan == '') {
		$id_pembelian_aplikasi 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$csupplier 	= $this->uri->segment(7);
		$tgl_awal 	= $this->uri->segment(8);
		$tgl_akhir 	= $this->uri->segment(9);
		$carinya 	= $this->uri->segment(10);
		$caribrg 	= $this->uri->segment(11);
		$filterbrg 	= $this->uri->segment(12);
		
		$query3	= $this->db->query(" SELECT no_sj, tgl_sj, id_supplier FROM tm_pembelian_aplikasi WHERE id = '$id_pembelian' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$no_sj = $hasilrow->no_sj;
			$tgl_sj = $hasilrow->tgl_sj;
			$id_supplier = $hasilrow->id_supplier;
			
			$pisah1 = explode("-", $tgl_sj);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_sj = $tgl1."-".$bln1."-".$thn1;
			
			// ambil data nama supplier
			$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_supplier' ");
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
		}
		else {
			$no_sj = '';
			$tgl_sj = '';
			$kode_supplier = '';
			$nama_supplier = '';
		}
		
		$data['msg'] = '';
		$data['id_pembelian'] = $id_pembelian;
		$data['no_sj'] = $no_sj;
		$data['tgl_sj'] = $tgl_sj;
		$data['id_supplier'] = $id_supplier;
		$data['kode_supplier'] = $kode_supplier;
		$data['nama_supplier'] = $nama_supplier;
		
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['csupplier'] = $csupplier;
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['carinya'] = $carinya;
		$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;

		$data['isi'] = 'faktur-bb-aplikasi/vedittgl';
		$this->load->view('template',$data);
	}
	else { // simpan
		$id_pembelian_aplikasi 	= $this->input->post('id_pembelian', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
		$pisah1 = explode("-", $tgl_sj);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_sj = $thn1."-".$bln1."-".$tgl1;
		$tgl = date("Y-m-d H:i:s");
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
		
		// update tglnya
		$this->db->query(" UPDATE tm_pembelian_aplikasi SET tgl_sj = '$tgl_sj', tgl_update = '$tgl'
							WHERE id= '$id_pembelian' ");
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "faktur-bb-aplikasi/cform/view/index/".$cur_page;
		else
			$url_redirectnya = "faktur-bb-aplikasi/cform/cari/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
			
		redirect($url_redirectnya);
	}

  }
  
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_pembelian_aplikasi 	= $this->input->post('id_pembelian', TRUE);
			$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj_lama 	= $this->input->post('no_sj_lama', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$id_supplier_lama = $this->input->post('hide_supplier', TRUE);  
			$id_supplier = $this->input->post('id_supplier', TRUE);  
			
			//$gtotal = $this->input->post('gtotal', TRUE);  
			$asligtotal = $this->input->post('asligtotal', TRUE);  
			
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			$ambil_pp = $this->input->post('ambil_pp', TRUE);
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
			
			//$tgl = date("Y-m-d");
			$tgl = date("Y-m-d H:i:s");

			$id_gudang = 0;
			$lokasi = "01"; // duta
			
			/*$lain_cash 	= $this->input->post('hide_lain_cash', TRUE);
			$lain_kredit 	= $this->input->post('hide_lain_kredit', TRUE); */
			$lain_cash 	= $this->input->post('lain_cash', TRUE);
			$lain_kredit 	= $this->input->post('lain_kredit', TRUE);
			//$lain_cash_lama 	= $this->input->post('lain_cash_lama', TRUE);
			//$lain_kredit_lama 	= $this->input->post('lain_kredit_lama', TRUE);
			
			if ($lain_cash == '')
				$lain_cash = 'f';
			if ($lain_kredit == '')
				$lain_kredit = 'f';
				
			/*if ($lain_cash_lama == '')
				$lain_cash_lama = 'f';
			if ($lain_kredit_lama == '')
				$lain_kredit_lama = 'f'; */
			
			$submit2 = $this->input->post('submit2', TRUE);
				
			// 20-07-2012
			if ($submit2 != '') { // hapus per item
				/*	20-06-2015 ALGORITMA PROSES HAPUS ITEM (sama dgn penjelasan yg dibawah): DONE ALL
				* - reset status op/pp dan op / pp_detail menjadi 'f' utk yg ada acuan OP / PP. dan sambil hapus item brgnya
				* - Reset stoknya jika status_stok = 't' dan hapus dari tm_apply_stok_pembelian_detail
				* - Hapus dari tm_pembelian_aplikasi_detail 
				* - Hitung ulang DPP, PPN, dan grand total
				* - Update header di tm_pembelian_aplikasi dan tm_apply_stok_pembelian
				*/		
				// 20-06-2015 DIMODIF
				
				//1. ambil no_sj dan id_supplier
				$query4	= $this->db->query(" SELECT no_sj, id_supplier from tm_pembelian_aplikasi where id = '$id_pembelian_aplikasi' ");
				$hasilrow = $query4->row();
				$no_sj	= $hasilrow->no_sj;
				$id_supplier	= $hasilrow->id_supplier;
				//$id_opnya = $hasilrow->id_op;
				//$id_ppnya = $hasilrow->id_pp;

				$jumlah_input=$no-1;
				$hitungulangtotal = 0; $hitungulangppn = 0;
				for ($i=1;$i<=$jumlah_input;$i++) {
					if ($this->input->post('cek_'.$i, TRUE) == '') {
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							// ambil data total dan pajak dari tm_pembelian_aplikasi_detail 
							$query4	= $this->db->query(" SELECT pajak, total from tm_pembelian_aplikasi_detail where id = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							$hasilrow = $query4->row();
							$pajaknya	= $hasilrow->pajak;
							$totalnya	= $hasilrow->total;
							
							//$hitungulangtotal+= $this->input->post('total_'.$i, TRUE);
							//$hitungulangppn+= $this->input->post('pajak_'.$i, TRUE);
							$hitungulangtotal+= $totalnya;
							$hitungulangppn+= $pajaknya;
						}
					}
					
					if ($this->input->post('cek_'.$i, TRUE) == 'y') {
						/* algoritma:
						1. Cek apakah ada id_op_detail ataupun id_op di tm_pembelian/tm_pembelian_aplikasi_detail . 
						   Jika ada, maka edit status OP-nya menjadi 'f' di tm_op_detail dan di tm_op
						2. Reset stoknya jika status_stok = 't' dan hapus dari tm_apply_stok_pembelian_detail
						3. Hapus dari tm_pembelian_aplikasi_detail 
						4. Hitung ulang DPP, PPN, dan grand total
						*/
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							
							//========= start here 20-07-2012. 19-06-2015 ==========
							$id_brgnya = $this->input->post('id_brg_'.$i, TRUE);
						    $id_brg_lama = $this->input->post('id_brg_lama_'.$i, TRUE); 
						    $id_op_detail = $this->input->post('id_op_detail_'.$i, TRUE); 
						    $id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE); 
							
							if ($id_op_detail != '0') {
								$query4	= $this->db->query(" SELECT id_op FROM tm_op_detail where id = '$id_op_detail' ");
								$hasilrow = $query4->row();
								$id_opupdatestatus	= $hasilrow->id_op;
								
								$this->db->query("UPDATE tm_op set status_op = 'f' where id= '$id_opupdatestatus' ");
								$this->db->query("UPDATE tm_op_detail set status_op = 'f' where id= '$id_op_detail' ");
							}
							
							if ($id_pp_detail != '0') {
								$query4	= $this->db->query(" SELECT id_pp FROM tm_pp_detail where id = '$id_pp_detail' ");
								$hasilrow = $query4->row();
								$id_ppupdatestatus	= $hasilrow->id_pp;
								
								$this->db->query("UPDATE tm_pp set status_faktur = 'f' where id= '$id_ppupdatestatus' ");
								$this->db->query("UPDATE tm_pp_detail set status_faktur = 'f' where id= '$id_pp_detail' ");
							}
													 
							 // 2. Reset stoknya jika status_stok = 't' dan hapus dari tm_apply_stok_pembelian_detail
							 // 20-06-2015
							 $qty_lama = $this->input->post('qty_lama_'.$i, TRUE); 
							 $harga_lama = $this->input->post('harga_lama_'.$i, TRUE); 
							 $id_brg_lama = $this->input->post('id_brg_lama_'.$i, TRUE); 
							 
							 //ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg='$id_brg_lama' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							else
								$stok_lama = 0;
										
							$stokreset1 = $stok_lama-$qty_lama;
										
							//cek stok terakhir tm_stok_harga, dan update stoknya
							// 10-12-2015 dikomen, ga dipake
							/*$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE id_brg = '$id_brg_lama' 
														AND harga = '$harga_lama' ");
							if ($query3->num_rows() == 0){
								$stok_harga_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_harga_lama	= $hasilrow->stok;
							}
							$stokreset2 = $stok_harga_lama-$qty_lama; */
														
							$this->db->query(" UPDATE tm_stok SET stok = '$stokreset1', tgl_update_stok = '$tgl'
												where id_brg= '$id_brg_lama' ");
											
							// 10-12-2015 dikomen
							/*$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl' 
												where id_brg= '$id_brg_lama' AND harga = '$harga_lama' "); */
							$this->db->query("DELETE FROM tm_pembelian_aplikasi_detail WHERE id='".$this->input->post('id_detail_'.$i, TRUE)."' ");
						} // end if ($this->input->post('id_detail_'.$i, TRUE) != 'n')
					} // end if cek_
				} // end for
				// hitung DPP
				if ($hide_pkp == 't')
					$hitungulangdpp = $hitungulangtotal/1.1;
				else
					$hitungulangdpp = 0;
				$hitungulangdpp = round($hitungulangdpp, 2);
				$hitungulangppn = round($hitungulangppn, 2);
				$hitungulangsisahutang = $hitungulangtotal-$uang_muka;
				
				$uid_update_by = $this->session->userdata('uid');
				
				// uang_muka = '$uang_muka', sisa_hutang = '$hitungulangsisahutang', 
				$this->db->query(" UPDATE tm_pembelian_aplikasi SET no_sj = '$no_sj', id_supplier = '$id_supplier', 
								tgl_sj = '$tgl_sj', jenis_pembelian='$jenis_pembelian', tgl_update = '$tgl', 
								total_pajak = '$hitungulangppn', dpp = '$hitungulangdpp', total = '$hitungulangtotal',
								keterangan='$ket', uid_update_by='$uid_update_by'
								where id= '$id_pembelian_aplikasi' ");
			
			} // end 20-07-2012
			else {
				// UPDATE DATA
				// 20-06-2015
			/* ALGORITMA PROSES EDIT:
					- item lama tidak bisa dihapus via tombol (-). berarti asumsinya item2 brg yg udh ada itu cuma diedit aja, ga perlu dihapus
					- SEMUA ITEM BRG LAMA DICEK DULU apakah ada acuan OP/PP, trus tentukan status OPnya / PPnya udh terpenuhi blm dari pengecekan qtynya (on prog)
					- Item2 baru diinsert ke tm_pembelian_aplikasi_detail dan tm_apply_stok_pembelian_detail, dgn status stok = 'f' (OK)
					- Update stok utk item2 lama dan update juga qty-nya di tm_pembelian_aplikasi_detail dan tm_apply_stok_pembelian_detail dgn status_stok = 't'
					- update data harga juga
					- update data header di tm_pembelian_aplikasi dan tm_pembelian_aplikasi_detail 
			*/
					// ====================================================
				$queryxx2	= $this->db->query(" SELECT pkp FROM tm_supplier WHERE id = '".$id_supplier."' ");
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->row();
					$pkp	= $hasilxx2->pkp;
				}
				else
					$pkp = 'f';
										
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$id_brg = $this->input->post('id_brg_'.$i, TRUE);
						$id_brg_lama = $this->input->post('id_brg_lama_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$qty_lama = $this->input->post('qty_lama_'.$i, TRUE);
						$harga = $this->input->post('harga_'.$i, TRUE);
						$harga_lama = $this->input->post('harga_lama_'.$i, TRUE);
						//21-09-2015
						$id_satuan_lama = $this->input->post('id_satuan_lama_'.$i, TRUE);
						$id_satuan = $this->input->post('id_satuan_'.$i, TRUE);
						$id_satuan_konversi = $this->input->post('id_satuan_konversi_'.$i, TRUE);
						$nama_brg = $this->input->post('nama_'.$i, TRUE);
						
						// 06-01-2015
						$id_brgnya = $this->input->post('id_brg_'.$i, TRUE);
						$id_brg_lama = $this->input->post('id_brg_lama_'.$i, TRUE); 
						$id_op_detail = $this->input->post('id_op_detail_'.$i, TRUE); 
						$id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE); 
							
						// 9 sept 2011, bisa insert item brg yg baru
						if ($this->input->post('id_detail_'.$i, TRUE) == 'n') {
							// a. insert item detail
							$data_detail = array(
								'id_brg'=>$this->input->post('id_brg_'.$i, TRUE),
								'nama_brg'=>$this->input->post('nama_'.$i, TRUE),
								'qty'=>$this->input->post('qty_'.$i, TRUE),
								'id_satuan'=>$this->input->post('id_satuan_'.$i, TRUE),
								'id_satuan_konversi'=>$this->input->post('id_satuan_konversi_'.$i, TRUE),
								'harga'=>$this->input->post('harga_'.$i, TRUE),
								'pajak'=>$this->input->post('pajak_'.$i, TRUE),
								'diskon'=>$this->input->post('diskon_'.$i, TRUE),
								'total'=>$this->input->post('aslitotal_'.$i, TRUE),
								'id_pembelian_aplikasi'=>$id_pembelian_aplikasi,
								'id_op_detail'=>$this->input->post('id_op_detail_'.$i, TRUE),
								'id_pp_detail'=>$this->input->post('id_pp_detail_'.$i, TRUE)
							);
							$this->db->insert('tm_pembelian_aplikasi_detail ',$data_detail);
							
							$query3	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail ORDER BY id DESC LIMIT 1 ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$id_pembelian_detail = $hasilrow->id;
							}
							else
								$id_pembelian_detail = 0;
							
							// 18-06-2015, save ke apply_stok digabung kesini
							$th_now	= date("Y");
	
							// =============================20-06-2015=======================
				
							// insert Bon M masuk secara otomatis di tm_apply_stok_pembelian
								// ambil header bon M
								$query3	= $this->db->query(" SELECT id, no_bonm FROM tm_apply_stok_pembelian WHERE no_sj = '$no_sj_lama' 
												AND id_supplier = '$id_supplier_lama' AND status_aktif = 't' ");
								if ($query3->num_rows() > 0) {
									$hasilrow = $query3->row();
									$no_bonm = $hasilrow->no_bonm;
									$id_apply_stok = $hasilrow->id;
									
									//save ke tabel tm_apply_stok_pembelian_detail
									// jika semua data tdk kosong, insert ke tm_apply_stok_pembelian_detail
									$data_detail = array(
											'id_brg'=>$id_brg,
											'nama_brg'=>$nama_brg,
											'qty'=>$qty,
											'id_satuan'=>$id_satuan,
											'id_satuan_konversi'=>$id_satuan_konversi,
											'harga'=>$harga,
											'id_apply_stok'=>$id_apply_stok,
											'no_bonm'=>$no_bonm,
											'id_pembelian_detail'=>$id_pembelian_detail
									);
									$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
								}
						}
						else {
							// 11-07-2015, update harga
							if (($harga != $harga_lama) && ($id_supplier != $id_supplier_lama)) {
								//$this->db->query(" DELETE FROM tt_harga WHERE id_brg = '$id_brg' AND id_supplier = '$id_supplier_lama'
								//			 AND harga = '$harga_lama' ");
								$this->db->query(" DELETE FROM tm_harga_brg_supplier WHERE id_brg = '$id_brg' AND id_supplier = '$id_supplier_lama'
											AND id_satuan = '$id_satuan_lama' AND harga = '$harga_lama' ");
								
								// 30-09-2015 GA DIPAKE
								/*$data_harga = array(
									'id_brg'=>$id_brg,
									'id_supplier'=>$id_supplier,
									'harga'=>$harga,
									'tgl_input'=>$tgl
								);
								$this->db->insert('tt_harga', $data_harga); */
								
								$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE id_brg = '$id_brg'
												AND id_satuan = '$id_satuan' AND id_supplier = '$id_supplier' ");
								if ($query3->num_rows() == 0){
									$this->db->query(" INSERT INTO tm_harga_brg_supplier (id_brg, id_supplier, id_satuan, harga, 
									tgl_input, tgl_update) VALUES ('$id_brg', '$id_supplier', '$id_satuan', '$harga', '$tgl', '$tgl') ");
								}
								else {
									$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '$harga', tgl_update='$tgl'
												where id_brg= '$id_brg' AND id_supplier = '$id_supplier' AND id_satuan = '$id_satuan' ");
								}
								
								// 30-09-2015. cek ke tabel history harga (tm_stok_harga), jika harga blm ada maka insert
								// 10-12-2015, tambahin filter is_harga_pkp
								$query3	= $this->db->query(" SELECT id FROM tm_stok_harga WHERE id_brg = '$id_brg' 
															AND harga = '$harga' AND id_satuan = '$id_satuan'
															AND is_harga_pkp = '$pkp' ");
								if ($query3->num_rows() == 0){
									$data_stok = array(
										'id_brg'=>$id_brg,
										'stok'=>0,
										'harga'=>$harga,
										'tgl_update_stok'=>$tgl,
										'is_harga_pkp'=>$pkp,
										'id_satuan'=>$id_satuan
										);
									$this->db->insert('tm_stok_harga',$data_stok);
								}
							}
							else if (($harga != $harga_lama) && ($id_supplier == $id_supplier_lama)) {
								/*$data_harga = array(
									'id_brg'=>$id_brg,
									'id_supplier'=>$id_supplier,
									'harga'=>$harga,
									'tgl_input'=>$tgl
								);
								$this->db->insert('tt_harga', $data_harga); */
								
								$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '$harga', tgl_update='$tgl'
												where id_brg= '$id_brg' AND id_supplier = '$id_supplier' AND id_satuan = '$id_satuan' ");
								
								// 30-09-2015. cek ke tabel history harga (tm_stok_harga), jika harga blm ada maka insert
								$query3	= $this->db->query(" SELECT id FROM tm_stok_harga WHERE id_brg = '$id_brg' 
															AND harga = '$harga' AND id_satuan = '$id_satuan'
															AND is_harga_pkp = '$pkp' ");
								if ($query3->num_rows() == 0){									
									$data_stok = array(
										'id_brg'=>$id_brg,
										'stok'=>0,
										'harga'=>$harga,
										'tgl_update_stok'=>$tgl,
										'is_harga_pkp'=>$pkp,
										'id_satuan'=>$id_satuan
										);
									$this->db->insert('tm_stok_harga',$data_stok);
								}
							}
							
							// 30-09-2015 CEK APAKAH DATA BRG ADA PERUBAHAN (KHUSUS YG TANPA ACUAN OP)
							if ($id_op_detail == '0') {
								if ($id_brg_lama != $id_brg) {
									$queryxx	= $this->db->query(" SELECT status_stok
												FROM tm_apply_stok_pembelian_detail WHERE id_pembelian_detail = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
									if ($queryxx->num_rows() > 0){
										$hasilxx = $queryxx->row();
										$status_stok	= $hasilxx->status_stok;
										
										if ($status_stok == 't') {
											// reset stoknya dulu
											$queryxx2	= $this->db->query(" SELECT stok FROM tm_stok 
															WHERE id_brg = '".$id_brg_lama."' AND id_satuan = '$id_satuan_lama' ");
											if ($queryxx2->num_rows() > 0){
												$hasilxx2 = $queryxx2->row();
												$stok_lama	= $hasilxx2->stok;
												
												$stok_baru = $stok_lama-$qty_lama;
												$this->db->query(" UPDATE tm_stok SET stok='$stok_baru', tgl_update_stok='$tgl' 
															WHERE id_brg = '".$id_brg_lama."' AND id_satuan = '$id_satuan_lama' ");
											}
											
											// tambahkan stok id_brg
											$queryxx2	= $this->db->query(" SELECT stok FROM tm_stok 
															WHERE id_brg = '".$id_brg."' AND id_satuan = '$id_satuan' ");
											if ($queryxx2->num_rows() > 0){
												$hasilxx2 = $queryxx2->row();
												$stok_lama	= $hasilxx2->stok;
												
												$stok_baru = $stok_lama+$qty;
												$this->db->query(" UPDATE tm_stok SET stok='$stok_baru', tgl_update_stok='$tgl' 
																WHERE id_brg = '".$id_brg."' AND id_satuan = '$id_satuan' ");
											}
										} // end if status_Stok
									} // end cek status stok
									
									$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET 
												id_brg = '$id_brg',
												nama_brg= '".$this->input->post('nama_'.$i, TRUE)."', 
												qty = '".$this->input->post('qty_'.$i, TRUE)."', 
												harga = '".$this->input->post('harga_'.$i, TRUE)."', 
												id_satuan = '".$this->input->post('id_satuan_'.$i, TRUE)."',
												id_satuan_konversi = '".$this->input->post('id_satuan_konversi_'.$i, TRUE)."',
												tgl_update = '$tgl'
												where id_pembelian_detail = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
										// 29-10-2015 dikeluarin nama_brg= '".$this->db->escape_str($this->input->post('nama_'.$i, TRUE))."', 
								} // end if cek
							} // end if id_op_detail == 0
							// --------------------------------------------------------------
							
							// reset status OP/PP
							// ambil id_op_detail / id_pp_detail
							$id_op_detail = $this->input->post('id_op_detail_'.$i, TRUE);
							$id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE);
							
							if ($id_op_detail != '0') {
								// ambil qty di op_detail
								$query3	= $this->db->query(" SELECT id_op, qty FROM tm_op_detail WHERE id = '$id_op_detail' ");
								if ($query3->num_rows() > 0) {
									$hasilrow = $query3->row();
									$id_op = $hasilrow->id_op;
									$qty_op = $hasilrow->qty;
									
									//cek jumlah pembelian. jika > qty OP maka jika sudah t semua di tabel detail, maka update tabel tm_op di field status_op menjadi t
									$sqlxx = " SELECT sum(b.qty) as jumbeli FROM tm_pembelian_aplikasi a INNER JOIN tm_pembelian_aplikasi_detail b 
												ON a.id = b.id_pembelian_aplikasi WHERE a.status_aktif='t' AND b.id_op_detail = '$id_op_detail' ";
									$queryxx	= $this->db->query($sqlxx);
									if ($queryxx->num_rows() > 0) {
										$hasilxx = $queryxx->row();
										$jumbeli = $hasilxx->jumbeli;
										
										$selisih = $jumbeli-$qty_lama+$qty;
										
										if ($selisih >= $qty_op) {
											$this->db->query(" UPDATE tm_op_detail SET status_op = 't' WHERE id = '$id_op_detail' ");
											
											// cek udh t semua blm, kalo udh, maka ganti jadi t headernya
											$sqlxx2 = " SELECT id FROM tm_op_detail WHERE status_op = 'f' ";
											$queryxx2	= $this->db->query($sqlxx2);
											if ($queryxx2->num_rows() == 0) {
												$this->db->query(" UPDATE tm_op SET status_op = 't' WHERE id='$id_op' ");
											}
										}
									}
									
								}
							}
							
							// NOTICE 30-09-2015: SKRIP INI GA DIPAKE LAGI!
							if ($id_pp_detail != '0') {
								// ambil qty di pp_detail
								/*$query3	= $this->db->query(" SELECT qty FROM tm_pp_detail WHERE id = '$id_pp_detail' ");
								if ($query3->num_rows() > 0) {
									$hasilrow = $query3->row();
									$qty_pp = $hasilrow->qty;
								}
								else
									$qty_pp = 0; */
								
								// ambil qty di op_detail
								$query3	= $this->db->query(" SELECT id_pp, qty FROM tm_pp_detail WHERE id = '$id_pp_detail' ");
								if ($query3->num_rows() > 0) {
									$hasilrow = $query3->row();
									$id_pp = $hasilrow->id_pp;
									$qty_pp = $hasilrow->qty;
									
									//cek jumlah pembelian. jika > qty OP maka jika sudah t semua di tabel detail, maka update tabel tm_pp di field status_faktur menjadi t
									$sqlxx = " SELECT sum(qty) as jumbeli FROM tm_pembelian_aplikasi_detail WHERE id_pp_detail = '$id_pp_detail' ";
									$queryxx	= $this->db->query($sqlxx);
									if ($queryxx->num_rows() > 0) {
										$hasilxx = $queryxx->row();
										$jumbeli = $hasilxx->jumbeli;
										
										$selisih = $jumbeli-$qty_lama+$qty;
										
										if ($selisih >= $qty_pp) {
											$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 't' WHERE id = '$id_pp_detail' ");
											
											// cek udh t semua blm, kalo udh, maka ganti jadi t headernya
											$sqlxx2 = " SELECT id FROM tm_pp_detail WHERE status_faktur = 'f' ";
											$queryxx2	= $this->db->query($sqlxx2);
											if ($queryxx2->num_rows() == 0) {
												$this->db->query(" UPDATE tm_pp SET status_faktur = 't' WHERE id='$id_pp' ");
											}
										}
									}
									
								}
							}// end if
							
							// 06-01-2016 PINDAH KESINI YG UPDATE tm_pembelian_aplikasi_detail 
								//20-06-2015
							   $sql = " UPDATE tm_pembelian_aplikasi_detail SET qty = '".$this->input->post('qty_'.$i, TRUE)."', ";
							   if ($id_brg_lama != $id_brg)
									$sql.= " id_brg = '$id_brg', ";
									
								$sql.= " nama_brg= '".$this->db->escape_str($this->input->post('nama_'.$i, TRUE))."', 
									id_satuan = '".$this->input->post('id_satuan_'.$i, TRUE)."',
									id_satuan_konversi = '".$this->input->post('id_satuan_konversi_'.$i, TRUE)."',
									harga = '".$this->input->post('harga_'.$i, TRUE)."', diskon = '".$this->input->post('diskon_'.$i, TRUE)."',
									pajak = '".$this->input->post('pajak_'.$i, TRUE)."', total = '".$this->input->post('aslitotal_'.$i, TRUE)."'
									where id= '".$this->input->post('id_detail_'.$i, TRUE)."' ";
								//echo $sql; die();
								$this->db->query($sql);
								
							// 13-07-2015 cek di apply_stok_pembelian_detail. Jika status_stok='t' maka hrs diupdate pake qty yg baru. 
							//setelah itu update juga item brg di tm_apply_stok_pembelian_detail
							// 30-09-2015, skrip dibawah ini utk id_brg yg tidak ada perubahan, hanya perubahan qty aja
							if ($id_brg == $id_brg_lama) {
								$queryxx	= $this->db->query(" SELECT status_stok
											FROM tm_apply_stok_pembelian_detail WHERE id_pembelian_detail = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$status_stok	= $hasilxx->status_stok;
									
									if ($status_stok == 't') {
										$queryxx2	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg = '".$id_brg."' AND id_satuan = '$id_satuan' ");
										if ($queryxx2->num_rows() > 0){
											$hasilxx2 = $queryxx2->row();
											$stok_lama	= $hasilxx2->stok;
											
											$stok_baru = $stok_lama-$qty_lama+$qty;
											$this->db->query(" UPDATE tm_stok SET stok='$stok_baru', tgl_update_stok='$tgl' 
															WHERE id_brg = '".$id_brg."' AND id_satuan = '$id_satuan' ");
										}
									} // end if status_Stok
								} // end cek status stok
								
								$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET 
											nama_brg= '".$this->db->escape_str($this->input->post('nama_'.$i, TRUE))."', 
											qty = '".$this->input->post('qty_'.$i, TRUE)."', 
											id_satuan = '".$this->input->post('id_satuan_'.$i, TRUE)."',
											id_satuan_konversi = '".$this->input->post('id_satuan_konversi_'.$i, TRUE)."',
											harga = '".$this->input->post('harga_'.$i, TRUE)."', 
											tgl_update = '$tgl'
											where id_pembelian_detail = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
						   }
						   
						   // ======================= 10-12-2015 =========================================================
						   //cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
							$queryxx	= $this->db->query(" SELECT id_apply_stok
													FROM tm_apply_stok_pembelian_detail WHERE id_pembelian_detail = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$cid_apply_stok = $hasilxx->id_apply_stok;
								
								$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE status_stok = 'f' 
													AND id_apply_stok = '$cid_apply_stok' ");
								if ($query3->num_rows() == 0){
									$this->db->query(" UPDATE tm_apply_stok_pembelian SET status_stok = 't' WHERE id= '$cid_apply_stok' ");
								}
																		
								//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
								$queryxx2	= $this->db->query(" SELECT id_pembelian_aplikasi 
														FROM tm_pembelian_aplikasi_detail WHERE id = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
								if ($queryxx2->num_rows() > 0){
									$hasilxx2 = $queryxx2->row();
									$cid_pembelian_aplikasi = $hasilxx2->id_pembelian_aplikasi ;
									
									$query3	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE status_stok = 'f' 
														AND id_pembelian_aplikasi = '$cid_pembelian_aplikasi' ");
									if ($query3->num_rows() == 0){
										$this->db->query(" UPDATE tm_pembelian_aplikasi SET status_stok = 't' WHERE id = '$cid_pembelian_aplikasi' ");
									}
								}

							}
							// ================ END 10-12-2015 ========================================================
							// ++++++++++++ end 06-01-2016 +++++++++++++++++++++++++++++++++++

						} // end if != n
						// ==================================================== GOOD 20-06-2015 =============================
					   
					} // end for
					
					// 20-06-2015
					//update headernya INI DI BELAKANG AJA
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tm_pembelian_aplikasi SET no_sjmasukpembelian_aplikasi = '$no_sj', id_supplier = '$id_supplier', 
										jenis_pembelian = '$jenis_pembelian',
										tgl_sj = '$tgl_sj', total = '$asligtotal',
										keterangan = '$ket', tgl_update = '$tgl', 
										pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak',
										total_pajak = '$total_pajak', dpp = '$dpp', 
										stok_masuk_lain_cash = '$lain_cash', 
										stok_masuk_lain_kredit = '$lain_kredit',
										uid_update_by='$uid_update_by'
										where id= '$id_pembelian_aplikasi' ");
					$this->db->query(" UPDATE tm_apply_stok_pembelian SET no_sj = '$no_sj', id_supplier = '$id_supplier', 
									tgl_update = '$tgl'
									where no_sj = '$no_sj_lama' AND id_supplier = '$id_supplier_lama' ");
				} // 20-07-2012, end if $submit2 == ''
				//ini dikeluarin dari query update, karena bukan uid adm pembelian yg bagian update bonmmasuk
				// , uid_update_by='$uid_update_by'
				
				if ($carinya == '') $carinya = "all";
				if ($is_cari == 0)
					$url_redirectnya = "faktur-bb-aplikasi/cform/view/index/".$cur_page;
				else
					$url_redirectnya = "faktur-bb-aplikasi/cform/cari/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
					
				redirect($url_redirectnya);
				//redirect('faktur-bb-aplikasi/cform/view');
				
  }
    
/*  function show_popup_brg($keywordcari){
	// =======================
	// disini coding utk pengecekan user login
	//$departemen = ambil infonya dari tabel user
//========================

	$posisi 	= $this->uri->segment(4);
	// if $departemen == 'Bahan Baku' { } else { get_asesoris}
	//$data['bahan_baku'] = $this->mmaster->get_bahan_baku($num, $offset, $keywordcari);
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jum_total = $this->mmaster->get_bahan_bakutanpalimit($keywordcari);
							$config['base_url'] = base_url()."index.php/faktur-bb-aplikasi/cform/show_popup_brg/".$posisi."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_bahan_baku($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total); //echo count($jum_total); die();
	$data['posisi'] = $posisi;

	$this->load->view('faktur-bb-aplikasi/vpopupbrg',$data);

  } */
  
  function show_popup_op(){
	// =======================
	// disini coding utk pengecekan user login
//========================
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id_sup 	= $this->uri->segment(4);
	$cek_pp 	= $this->uri->segment(5);
	$jenis_pembelian 	= $this->uri->segment(6);
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	//$csupplier 	= $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && ($id_sup == '' || $cek_pp == '' || $jenis_pembelian == '') ) {
		$id_sup 	= $this->uri->segment(4);
		$cek_pp 	= $this->uri->segment(5);
		$jenis_pembelian 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($cek_pp == '')
		$cek_pp = $this->input->post('cek_pp', TRUE);  
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($id_sup == '')
		$id_sup = $this->input->post('id_sup', TRUE);  
	if ($jenis_pembelian == '')
		$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
	
	$jum_total = $this->mmaster->get_optanpalimit($id_sup, $cek_pp, $jenis_pembelian, $keywordcari); 
					/*		$config['base_url'] = base_url()."index.php/faktur-bb-aplikasi/cform/show_popup_op/".$cek_pp."/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		 */
	//$data['query'] = $this->mmaster->get_op($config['per_page'],$this->uri->segment(7), $csupplier, $cek_pp, $keywordcari);
	$data['query'] = $this->mmaster->get_op($id_sup, $cek_pp, $jenis_pembelian, $keywordcari);
	//$data['jum_total'] = count($jum_total);
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['id_sup'] = $id_sup;
	$data['cek_pp'] = $cek_pp;
	$data['jenis_pembelian'] = $jenis_pembelian;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	// 04-07-2015
	$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_sup' ");
	$hasilrow = $query3->row();
	if ($query3->num_rows() != 0) {
		$kode_supplier	= $hasilrow->kode_supplier;
		$nama_supplier	= $hasilrow->nama;
	}
	else {
		$kode_supplier = '';
		$nama_supplier = "Lain-lain";
	}
	$data['kode_supplier'] = $kode_supplier;
	$data['nama_supplier'] = $nama_supplier;
	$this->load->view('faktur-bb-aplikasi/vpopupop',$data);

  }

  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		//$this->load->library('form_validation');
		//$goedit 	= $this->input->post('goedit', TRUE);

		//	$this->form_validation->set_rules('no_sj', 'Nomor SJ', 'required');
		//	$this->form_validation->set_rules('tgl_sj', 'Tanggal SJ', 'required');

		//if ($this->form_validation->run() == FALSE)
		//{
		/*	$data['isi'] = 'faktur-bb-aplikasi/vmainform';
			$data['msg'] = 'Field2 nomor faktur, tanggal faktur tidak boleh kosong..!';
			$this->load->view('template',$data); */
	/*	}
		else
		{ */
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj = $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			
			//04-04-2012			
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			
			$no 	= $this->input->post('no', TRUE);
			//$id_op 	= $this->input->post('id_op', TRUE);
			$id_supplier = $this->input->post('id_sup', TRUE);  
			// 04-07-2015
			if ($id_supplier == '0') {
				$id_supplierbaru = $this->input->post('id_supplierx', TRUE);
			}
			else
				$id_supplierbaru = '0';
			$gtotal = $this->input->post('gtotal', TRUE);  
			
			//06-06-2015
			$asligtotal = $this->input->post('asligtotal', TRUE);  
			
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			$ambil_pp 	= $this->input->post('ambil_pp', TRUE);
			$lain_cash 	= $this->input->post('lain_cash', TRUE);
			$lain_kredit 	= $this->input->post('lain_kredit', TRUE);
			
			$is_no_ppop 	= $this->input->post('is_no_ppop', TRUE);
			
			if ($lain_cash == '')
				$lain_cash = 'f';
			
			if ($lain_kredit == '')
				$lain_kredit = 'f';
			
		//	if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($no_sj, $id_supplier);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'faktur-bb-aplikasi/vmainform';
					$data['msg'] = "Data no SJ ".$no_sj." sudah ada..!";
					//$data['msg'] = '';
					$data['id_pp'] = '';
					$data['go_proses'] = '';
					$data['supplier2'] = $this->mmaster->get_supplier();
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
					// inspired from proIT inventory
						$this->mmaster->save($no_sj,$tgl_sj,$id_supplier,$id_supplierbaru, $gtotal, $asligtotal, $total_pajak, $dpp, 
						$uang_muka, $sisa_hutang,$ket,$hide_pkp, $hide_tipe_pajak, $ambil_pp, $lain_cash, $lain_kredit,
						$is_no_ppop, $jenis_pembelian, $this->input->post('id_op_detail_'.$i, TRUE), 
						$this->input->post('id_op_'.$i, TRUE),
						$this->input->post('id_brg_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
						$this->input->post('id_satuan_'.$i, TRUE), $this->input->post('id_satuan_konversi_'.$i, TRUE),
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE), 
									$this->input->post('harga_lama_'.$i, TRUE), 
									$this->input->post('pajak_'.$i, TRUE), $this->input->post('diskon_'.$i, TRUE), 
									$this->input->post('total_'.$i, TRUE), $this->input->post('aslitotal_'.$i, TRUE) );
						// 25-06-2015 $this->input->post('satuan_lain_'.$i, TRUE), $this->input->post('qty_sat_lain_'.$i, TRUE) DIBUANG
						
						//if ($lain_cash == 'f' && $lain_kredit == 'f' ) {
						// 18-06-2015 DIPINDAH KEDALAM SKRIP SAVE
							/*$stok_lain = 'f';
							$this->mmaster->create_bonm($no_sj, $id_supplier, $this->input->post('id_brg_'.$i, TRUE), 
									$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE), $stok_lain ); */
						//}
						
						// 18-06-2015 DIKOMEN, GA DIPAKE
						/*if ($lain_cash == 't') {
							// tetap bikin bon M, tapi update stoknya masuk lain2
							$stok_lain = 't';
							$this->mmaster->create_bonm($no_sj, $kode_supplier, $this->input->post('kode_'.$i, TRUE), 
									$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE), $stok_lain );
						}
						
						if ($lain_kredit == 't') {
							 $tgl = date("Y-m-d");
							// tidak ada bon M, langsung update stok dgn tipe masuk dan tipe keluar lain2
							
							$kode_brg = $this->input->post('kode_'.$i, TRUE);
							$qty = $this->input->post('qty_'.$i, TRUE);
							$harga = $this->input->post('harga_'.$i, TRUE);
							
							// ########################################################
							// 1. INSERT STOK MASUK
							//cek stok terakhir tm_stok, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$kode_brg,
									'stok'=>$new_stok,
								//	'id_gudang'=>$id_gudang, //
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode_brg' ");
							}
							
							// 14 juni 2011, ada tabel baru, yaitu tm_stok_harga (utk membedakan stok berdasarkan harga)
							//cek stok terakhir tm_stok_harga, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
														AND harga = '$harga' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_harga, insert
								$data_stok = array(
									'kode_brg'=>$kode_brg,
									'stok'=>$new_stok,
									'harga'=>$harga,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_harga',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode_brg' AND harga = '$harga' ");
							}
							// #########################################################################################
							
							$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_supplier, masuk, saldo, tgl_input, harga) 
													VALUES ('$kode_brg','$no_sj', '$kode_supplier', '$qty', '$new_stok', '$tgl', '$harga' ) ");
							
							// 2. INSERT STOK KELUAR
							//cek stok terakhir tm_stok, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$kode_brg,
									'stok'=>$new_stok,
								//	'id_gudang'=>$id_gudang, //
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode_brg' ");
							}
							
							// 14 juni 2011, ada tabel baru, yaitu tm_stok_harga (utk membedakan stok berdasarkan harga)
							//cek stok terakhir tm_stok_harga, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
														AND harga = '$harga' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_harga, insert
								$data_stok = array(
									'kode_brg'=>$kode_brg,
									'stok'=>$new_stok,
									'harga'=>$harga,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_harga',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode_brg' AND harga = '$harga' ");
							}
							// #########################################################################################
							
							$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_supplier, keluar_lain, saldo, tgl_input, harga) 
													VALUES ('$kode_brg','$no_sj', '$kode_supplier', '$qty', '$new_stok', '$tgl', '$harga' ) ");
							
							// ######################################################
						} */

					}
					redirect('faktur-bb-aplikasi/cform/view');
				}
		//	} // end if goedit == ''

		//} end form validation (ini ga usah dipake)
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'faktur-bb-aplikasi/vformview';
    $keywordcari = "all";
    $csupplier = '0';
	$id_bagian = ''; // ini nanti ambil dari login user
	
	$date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
	//10-04-2014
	$caribrg = "all";
	$filterbrg = "n";
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/faktur-bb-aplikasi/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
	
	if ($csupplier == '')
		$csupplier 	= $this->uri->segment(4);
	if ($date_from == '')
		$date_from = $this->uri->segment(5);
	if ($date_to == '')
		$date_to = $this->uri->segment(6);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(7);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(8);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(9);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($csupplier == '')
		$csupplier = '0';
	if ($filterbrg == '')
		$filterbrg = 'n';
	if ($caribrg == '')
		$caribrg = "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/faktur-bb-aplikasi/cform/cari/'.$csupplier.'/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(10);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(10), $csupplier, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'faktur-bb-aplikasi/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
		
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $csupplier 	= $this->uri->segment(7);
    $tgl_awal 	= $this->uri->segment(8);
    $tgl_akhir 	= $this->uri->segment(9);
    $carinya 	= $this->uri->segment(10);
    $caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "faktur-bb-aplikasi/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "faktur-bb-aplikasi/cform/cari/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
	
	redirect($url_redirectnya);
					
    //redirect('faktur-bb-aplikasi/cform/view');
  }
  
  function get_pkp_tipe_pajak() {
		$id_sup 	= $this->uri->segment(4);
		$rows = array();
		if(isset($id_sup)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			$rows = $this->mmaster->get_pkp_tipe_pajak_bykodesup($id_sup);
			
			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);

  }
  
  /*function get_qty_op_detail() {		
		$id_op	= $this->input->post('id_op');
		$kode_brg	= $this->input->post('kode_brg');
		$qty_brg	= $this->input->post('qty');

		// ambil qty op_detail
		$query3	= $this->db->query(" SELECT qty FROM tm_op_detail WHERE id_op = '".$id_op."'
		AND kode_brg = '$kode_brg' ");
		
		if($query3->num_rows()>0) {
			$hasilrow = $query3->row();
			$qty_op = $hasilrow->qty;
			//echo $qtynya;
		}
		else
			$qtynya = 0;
		
		// ambil sum qty pembelian detail
			$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b 
							WHERE a.id_pembelian_aplikasi = b.id AND b.id_op = '$id_op' AND a.kode_brg = '".$kode_brg."' ");
		
		if($query3->num_rows()>0) {
			$hasilrow = $query3->row();
			$jum_beli = $hasilrow->jum;
		}
		else
			$jumnya = 0;

		$qty_skrg = $jum_beli + $qty_brg;
		if ($qty_skrg > $qty_op)
			echo "1";
		else
			echo "0";

		return true;
  } */
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
	//$departemen = ambil infonya dari tabel user
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);
	$id_supplier 	= $this->uri->segment(6);

	if ($posisi == '' || $kel_brg =='' || $id_supplier == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg = $this->input->post('kel_brg', TRUE);  
		$id_supplier = $this->input->post('id_supplier', TRUE);  
	}	
	
		$keywordcari 	= $this->input->post('cari', TRUE);
		$id_jenis_brg = $this->input->post('id_jenis_brg', TRUE);    
		
		if ($keywordcari == '' && ($id_jenis_brg == '' || $posisi == '' || $kel_brg == '' || $id_supplier == '') ) {
			$kel_brg 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$id_supplier 	= $this->uri->segment(6);
			$id_jenis_brg 	= $this->uri->segment(7);
			$keywordcari 	= $this->uri->segment(8);
		}

		if ($keywordcari == '')
			$keywordcari 	= "all";
			
		if ($id_jenis_brg == '')
			$id_jenis_brg 	= '0';
		
		if ($keywordcari != '') {
			$ganti = str_replace("%20"," ", $keywordcari);
			$keywordcari = $ganti;
		}
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $id_jenis_brg);
		//$jum_total	= $qjum_total->num_rows(); 

			$config['base_url'] = base_url()."index.php/faktur-bb-aplikasi/cform/show_popup_brg/".$kel_brg."/".$posisi."/".$id_supplier."/".$id_jenis_brg."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(9,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $id_jenis_brg);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	$data['kel_brg'] = $kel_brg;
	$data['id_supplier'] = $id_supplier;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;
	$data['jenis_brg'] = $this->mmaster->get_jenis_brg($kel_brg);
	$data['cjenis_brg'] = $id_jenis_brg;
	$data['startnya'] = $config['cur_page'];
	
	$this->load->view('faktur-bb-aplikasi/vpopupbrg',$data);
  }
  
  function input_ppop(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_pembelian_aplikasi 	= $this->uri->segment(4);
	$jenisnya 	= $this->uri->segment(5);
	$cur_page 	= $this->uri->segment(6);
	$is_cari 	= $this->uri->segment(7);
	$csupplier 	= $this->uri->segment(8);
	$tgl_awal 	= $this->uri->segment(9);
	$tgl_akhir 	= $this->uri->segment(10);
	$carinya 	= $this->uri->segment(11);
	
	$data['query'] = $this->mmaster->get_pembelian($id_pembelian, 0);
	//$data['msg'] = '';
	$data['jenisnya'] = $jenisnya; // 1. PP, 2. OP
	
	$data['id_pembelian'] = $id_pembelian;
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['csupplier'] = $csupplier;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;

	$data['isi'] = 'faktur-bb-aplikasi/vinputppopform';
	$this->load->view('template',$data);

  }
  
  function show_popup_ppop(){
	// =======================
	// disini coding utk pengecekan user login
//========================
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$jenisnya 	= $this->uri->segment(4);
	$kode_sup 	= $this->uri->segment(5);
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	//$csupplier 	= $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && ($jenisnya == '' || $kode_sup == '') ) {
		$jenisnya 	= $this->uri->segment(4);
		$kode_sup 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
		
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($jenisnya == '')
		$jenisnya = $this->input->post('jenisnya', TRUE);  
	if ($kode_sup == '')
		$kode_sup = $this->input->post('kode_sup', TRUE);  
	
	$data['query'] = $this->mmaster->get_ppop($jenisnya, $kode_sup, $keywordcari);

	$data['jenisnya'] = $jenisnya;
	$data['kode_sup'] = $kode_sup;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('faktur-bb-aplikasi/vpopupppop',$data);

  }
  
  function save_ppop(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$tgl = date("Y-m-d");
			
			$no_ppop 	= $this->input->post('no_ppop', TRUE);
			$id_ppop = $this->input->post('id_ppop', TRUE);  
			
			$id_ppopdetail = $this->input->post('id_ppopdetail', TRUE);  
			$list_id_ppopdetail = explode(";", $id_ppopdetail);
			
			$id_pembelian_aplikasi = $this->input->post('id_pembelian', TRUE);  
			$jenisnya = $this->input->post('jenisnya', TRUE);  
			
			$cur_page = $this->input->post('cur_page', TRUE);  
			$is_cari = $this->input->post('is_cari', TRUE);  
			$csupplier = $this->input->post('csupplier', TRUE);  
			$tgl_awal = $this->input->post('tgl_awal', TRUE);  
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);  
			$carinya = $this->input->post('carinya', TRUE);  
			
			if ($jenisnya == 1) {
				$this->db->query(" UPDATE tm_pembelian_aplikasi SET id_pp = '$id_ppop', tgl_update = '$tgl' WHERE id = '$id_pembelian' ");
				
				foreach($list_id_ppopdetail as $row1) {
					if ($row1 != '') {
						$query2	= $this->db->query(" SELECT kode_brg, qty, id_pp FROM tm_pp_detail WHERE id = '$row1' ");
						$hasilrow = $query2->row();
						$kode_brg	= $hasilrow->kode_brg;
						$qty	= $hasilrow->qty;
						$id_pp_header	= $hasilrow->id_pp;
						
						$query3	= $this->db->query(" SELECT * FROM tm_pembelian_aplikasi_detail WHERE id_pembelian_aplikasi = '$id_pembelian'
									AND kode_brg = '$kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach ($hasil3 as $row3) {
								$this->db->query(" UPDATE tm_pembelian_aplikasi_detail SET id_pp_detail = '$row1' WHERE id = '$row3->id' ");
								
								// 24 des 2011, cek status op tiap2 item detail dan juga headernya
								$qty_pp = $qty; // ini qty di OP detail berdasarkan kode brgnya
								
								// ini sum OP berdasarkan kode brg tsb
								$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$id_pp_header' AND a.kode_brg = '$kode_brg' ");
								$hasilrow = $query3->row();
								$jum_op = $hasilrow->jum;
								
								if ($row3->satuan_lain != 0)
									$query2	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_aplikasi_detail a, 
												tm_pembelian_aplikasi b
												WHERE a.id_pembelian_aplikasi = b.id AND a.id_pp_detail = '$row1' AND b.status_aktif = 't'
												AND a.kode_brg = '$kode_brg' "); //
								else
									$query2	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a, 
												tm_pembelian_aplikasi b
												WHERE a.id_pembelian_aplikasi = b.id AND a.id_pp_detail = '$row1' AND b.status_aktif = 't'
												AND a.kode_brg = '$kode_brg' "); //
								
								$hasilrow2 = $query2->row();
								$jum_beli = $hasilrow2->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
								
								if ($jum_beli >= $qty_pp-$jum_op) {
									$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 't' where id= '$row1' ");
									//cek di tabel tm_pp_detail, apakah status_faktur sudah 't' semua?
									$this->db->select("id from tm_pp_detail WHERE status_faktur = 'f' AND id_pp = '$id_pp_header' ", false);
									$query = $this->db->get();
									//jika sudah t semua, maka update tabel tm_pp di field status_faktur menjadi t
									if ($query->num_rows() == 0){
										$this->db->query(" UPDATE tm_pp SET status_faktur = 't' where id= '$id_pp_header' ");
									}
								}
							}
						}
					} // end if
				} // end foreach
			}
			else { // jenisnya 2 (OP)
				$this->db->query(" UPDATE tm_pembelian_aplikasi SET id_op = '$id_ppop', tgl_update = '$tgl' WHERE id = '$id_pembelian' ");
				
				foreach($list_id_ppopdetail as $row1) {
					if ($row1 != '') {
						$query2	= $this->db->query(" SELECT kode_brg, qty, id_op FROM tm_op_detail WHERE id = '$row1' ");
						$hasilrow = $query2->row();
						$kode_brg	= $hasilrow->kode_brg;
						$qty	= $hasilrow->qty;
						$id_op_header	= $hasilrow->id_op;
						
						$query3	= $this->db->query(" SELECT * FROM tm_pembelian_aplikasi_detail WHERE id_pembelian_aplikasi = '$id_pembelian'
									AND kode_brg = '$kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach ($hasil3 as $row3) {
								$this->db->query(" UPDATE tm_pembelian_aplikasi_detail SET id_op_detail = '$row1' WHERE id = '$row3->id' ");
								
								// 24 des 2011, cek status op tiap2 item detail dan juga headernya
								$qty_op = $qty; // ini qty di OP detail berdasarkan kode brgnya
								
								if ($row3->satuan_lain != 0)
									$query2	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_aplikasi_detail a, 
												tm_pembelian_aplikasi b
												WHERE a.id_pembelian_aplikasi = b.id AND a.id_op_detail = '$row1' AND b.status_aktif = 't'
												AND a.kode_brg = '$kode_brg'
												AND b.kode_supplier = '$kode_supplier' "); //
								else
									$query2	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a, 
												tm_pembelian_aplikasi b
												WHERE a.id_pembelian_aplikasi = b.id AND a.id_op_detail = '$row1' AND b.status_aktif = 't'
												AND a.kode_brg = '$kode_brg'
												AND b.kode_supplier = '$kode_supplier' "); //
								
								$hasilrow2 = $query2->row();
								$jum_beli = $hasilrow2->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
								
								if ($jum_beli >= $qty_op) {
									$this->db->query(" UPDATE tm_op_detail SET status_op = 't' where id= '$row1' ");
									//cek di tabel tm_op_detail, apakah status_op sudah 't' semua?
									$this->db->select("id from tm_op_detail WHERE status_op = 'f' AND id_op = '$id_op_header' ", false);
									$query = $this->db->get();
									//jika sudah t semua, maka update tabel tm_op di field status_op menjadi t
									if ($query->num_rows() == 0){
										$this->db->query(" UPDATE tm_op SET status_op = 't' where id= '$id_op_header' ");
									}
								}
								// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end didieu
							} // end foreach
						}
					}
				}
			}
						
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "faktur-bb-aplikasi/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "faktur-bb-aplikasi/cform/cari/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
				
			redirect($url_redirectnya);	
  }
  
  // 25-06-2015, cetak bukti penerimaan brg
  function print_sj(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_sj 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_pembelian($id_sj);
	// 09-07-2015
	$data['datasetting'] = $this->mmaster->get_perusahaan();
	
	// 31-07-2015
	$datasetting = $data['datasetting'];
	$datasj = $data['query'];
	$list_gudang_sj = $datasj[0]['list_id_gudang'];
	
	// 27-08-2015. ambil data admgudang_uid_update_by
	$uid_update_by = $datasj[0]['admgudang_uid_update_by'];
	
	if ($uid_update_by == 0) {
		$exp_list_gudang_sj = explode(";", $list_gudang_sj);
		// cek id_gudang_admstok dan admstok2, apakah ada yg matching dgn yg di variabel $datasj
		$adastaf1 = ''; $adastaf2 = '';
		foreach($exp_list_gudang_sj as $gud) {
			if ($gud != '') {
				$gud = trim($gud);
				if ($datasetting['id_gudang_admstok'] != '') {
					$list_id_gudang_admstok = explode(";", $datasetting['id_gudang_admstok']);
					foreach($list_id_gudang_admstok as $a1) {
						if ($a1 != '') {
							$a1 = trim($a1);
							if ($gud == $a1) {
								$adastaf1 = 1;
								break;
							}
						}
					}
				}
			}
		}
		
		if ($adastaf1 == '') {
			foreach($exp_list_gudang_sj as $gud) {
				if ($gud != '') {
					$gud = trim($gud);
					if ($datasetting['id_gudang_admstok2'] != '') {
						$list_id_gudang_admstok2 = explode(";", $datasetting['id_gudang_admstok2']);
						foreach($list_id_gudang_admstok2 as $a2) {
							if ($a2 != '') {
								$a2 = trim($a2);
								if ($gud == $a2) {
									$adastaf2 = 1;
									break;
								}
							}
						}
					}
				}
			}
		}
		
		$data['adastaf1'] = $adastaf1;
		$data['adastaf2'] = $adastaf2;
		$data['namastaf'] = '';
	}
	else {
		// ambil nama usernya
		$sqlxx = " SELECT nama FROM tm_user WHERE uid = '$uid_update_by' ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$nama_user = $hasilxx->nama;
			$data['namastaf'] = $nama_user;
		}
		else
			$data['namastaf'] = '';
	}

	$data['id_sj'] = $id_sj;
	$data['uid_update_by'] = $uid_update_by;
	$this->load->view('faktur-bb-aplikasi/vprintsj',$data);
  }
  
  // 21-09-2015, migrasi data satuan ke field id_satuan di tabel tm_pembelian_aplikasi_detail dan tm_apply_stok_pembelian_detail
  function migrasisatuan() {
	 /* $sql = " SELECT id, id_brg FROM tm_pembelian_aplikasi_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$id_satuan	= $hasilrow->satuan;
						
			$this->db->query(" UPDATE tm_pembelian_aplikasi_detail SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
		}
	  }
	  echo "sukses migrasi id_satuan di tm_pembelian_aplikasi_detail <br>";
	  
	  $sql = " SELECT id, id_brg FROM tm_apply_stok_pembelian_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$id_satuan	= $hasilrow->satuan;
						
			$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
		}
	  }
	  echo "sukses migrasi id_satuan di tm_apply_stok_pembelian_detail  <br>";
	  
	  $sql = " SELECT id, id_brg FROM tm_pp_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$id_satuan	= $hasilrow->satuan;
						
			$this->db->query(" UPDATE tm_pp_detail SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
		}
	  }
	  echo "sukses migrasi id_satuan di tm_pp_detail <br>";
	  
	  $sql = " SELECT id, id_brg FROM tm_op_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$id_satuan	= $hasilrow->satuan;
						
			$this->db->query(" UPDATE tm_op_detail SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
		}
	  }
	  echo "sukses migrasi id_satuan di tm_op_detail <br>"; */
	  
	  // 2
	 /* $sql = " SELECT id, id_brg FROM tm_stok ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$id_satuan	= $hasilrow->satuan;
				$this->db->query(" UPDATE tm_stok SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
			}
		}
	  }
	  echo "sukses migrasi id_satuan di tm_stok <br>";
	  
	  $sql = " SELECT id, id_brg FROM tm_bonmkeluar_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$id_satuan	= $hasilrow->satuan;
							
				$this->db->query(" UPDATE tm_bonmkeluar_detail SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
			}
		}
	  }
	  echo "sukses migrasi id_satuan di tm_bonmkeluar_detail <br>";
	  
	  $sql = " SELECT id, id_brg FROM tm_bonmmasuklain_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$id_satuan	= $hasilrow->satuan;
						
			$this->db->query(" UPDATE tm_bonmmasuklain_detail SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
		}
	  }
	  echo "sukses migrasi id_satuan di tm_bonmmasuklain_detail <br>"; */
	  
	  /*$sql = " SELECT id, id_brg FROM tm_harga_brg_supplier ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$id_satuan	= $hasilrow->satuan;
							
				$this->db->query(" UPDATE tm_harga_brg_supplier SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
			}
		}
	  }
	  echo "sukses migrasi id_satuan di tm_harga_brg_supplier <br>"; */
	  
	  $sql = " SELECT id, id_brg FROM tm_bonmmasukmanual_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id_brg' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$id_satuan	= $hasilrow->satuan;
							
				$this->db->query(" UPDATE tm_bonmmasukmanual_detail SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
			}
		}
	  }
	  echo "sukses migrasi id_satuan di tm_bonmmasukmanual_detail <br>";
  }
  
  // 21-09-2015, migrasi data nama_brg ke field nama_brg di tabel transaksi
  //~ function migrasinamabrg() {
	  //~ $sql = " SELECT id, id_brg FROM tm_pembelian_aplikasi_detail ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ $query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ $hasilrow = $query3->row();
			//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
						//~ 
			//~ $this->db->query(" UPDATE tm_pembelian_aplikasi_detail SET nama_brg = '$nama_brg' WHERE id = '$row1->id' ");
		//~ }
	  //~ }
	  //~ echo "sukses migrasi nama_brg di tm_pembelian_aplikasi_detail <br>";
	  //~ 
	  //~ $sql = " SELECT id, id_brg FROM tm_apply_stok_pembelian_detail ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ $query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ $hasilrow = $query3->row();
			//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
						//~ 
			//~ $this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET nama_brg = '$nama_brg' WHERE id = '$row1->id' ");
		//~ }
	  //~ }
	  //~ echo "sukses migrasi nama_brg di tm_apply_stok_pembelian_detail <br>";
	  //~ 
	  //~ $sql = " SELECT id, id_brg FROM tm_pp_detail ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ $query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ $hasilrow = $query3->row();
			//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
						//~ 
			//~ $this->db->query(" UPDATE tm_pp_detail SET nama_brg = '$nama_brg' WHERE id = '$row1->id' ");
		//~ }
	  //~ }
	  //~ echo "sukses migrasi nama_brg di tm_pp_detail <br>";
	  //~ 
	  //~ $sql = " SELECT id, id_brg FROM tm_op_detail ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ $query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ $hasilrow = $query3->row();
			//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
						//~ 
			//~ $this->db->query(" UPDATE tm_op_detail SET nama_brg = '$nama_brg' WHERE id = '$row1->id' ");
		//~ }
	  //~ }
	  //~ echo "sukses migrasi nama_brg di tm_op_detail <br>";
	  //~ 
	  //~ $sql = " SELECT id, id_brg FROM tm_bonmkeluar_detail ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ 
			//~ $query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ if ($query3->num_rows() > 0){
				//~ $hasilrow = $query3->row();
				//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
							//~ 
				//~ $this->db->query(" UPDATE tm_bonmkeluar_detail SET nama_brg = '$nama_brg' WHERE id = '$row1->id' ");
			//~ }
		//~ }
	  //~ }
	  //~ echo "sukses migrasi nama_brg di tm_bonmkeluar_detail <br>";
	  //~ 
	  //~ $sql = " SELECT id, id_brg FROM tm_bonmmasuklain_detail ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ $query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ if ($query3->num_rows() > 0){
				//~ $hasilrow = $query3->row();
				//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
							//~ 
				//~ $this->db->query(" UPDATE tm_bonmmasuklain_detail SET nama_brg = '$nama_brg' WHERE id = '$row1->id' ");
			//~ }
		//~ }
	  //~ }
	  //~ echo "sukses migrasi nama_brg di tm_bonmmasuklain_detail <br>";
	  //~ 
	  //~ $sql = " SELECT id, id_brg FROM tm_bonmmasukmanual_detail ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ $query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ if ($query3->num_rows() > 0){
				//~ $hasilrow = $query3->row();
				//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
							//~ 
				//~ $this->db->query(" UPDATE tm_bonmmasukmanual_detail SET nama_brg = '$nama_brg' WHERE id = '$row1->id' ");
			//~ }
		//~ }
	  //~ }
	  //~ echo "sukses migrasi nama_brg di tm_bonmmasukmanual_detail <br>";
	  //~ 
	  //~ // 2
	 //~ /* $sql = " SELECT id, id_brg FROM tm_stok ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ $query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										//~ WHERE a.id = '$row1->id_brg' ");
			//~ if ($query3->num_rows() > 0){
				//~ $hasilrow = $query3->row();
				//~ $id_satuan	= $hasilrow->satuan;
				//~ $this->db->query(" UPDATE tm_stok SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
			//~ }
		//~ }
	  //~ }
	  //~ echo "sukses migrasi id_satuan di tm_stok <br>";
	 //~ */
	  //~ 
	  //~ /*$sql = " SELECT id, id_brg FROM tm_harga_brg_supplier ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ $query3	= $this->db->query(" SELECT a.satuan, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										//~ WHERE a.id = '$row1->id_brg' ");
			//~ if ($query3->num_rows() > 0){
				//~ $hasilrow = $query3->row();
				//~ $id_satuan	= $hasilrow->satuan;
							//~ 
				//~ $this->db->query(" UPDATE tm_harga_brg_supplier SET id_satuan = '$id_satuan' WHERE id = '$row1->id' ");
			//~ }
		//~ }
	  //~ }
	  //~ echo "sukses migrasi id_satuan di tm_harga_brg_supplier <br>"; */
  //~ }
  //~ 
  //~ // 27-10-2015
  //~ function koreksinamabrg271015() {
	  //~ $sql = " select a.id, a.nama_brg as nama_brg_sj, b.nama_brg as nama_brg_master from tm_pembelian_aplikasi_detail a
				//~ inner join tm_barang b on a.id_brg = b.id where a.nama_brg <> b.nama_brg AND a.nama_brg like '%Kain Twill 20%' ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ /*$query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ $hasilrow = $query3->row();
			//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg); */
			//~ $nama_brg_master	= $this->db->escape_str($row1->nama_brg_master);
						//~ 
			//~ $this->db->query(" UPDATE tm_pembelian_aplikasi_detail SET nama_brg = '$nama_brg_master' WHERE id = '$row1->id' ");
		//~ }
	  //~ }
	  //~ echo "sukses koreksi nama_brg di tm_pembelian_aplikasi_detail untuk item brg kain 's <br>";
	  //~ 
	  //~ $sql = " select a.id, a.nama_brg as nama_brg_sj, b.nama_brg as nama_brg_master from tm_apply_stok_pembelian_detail a
				//~ inner join tm_barang b on a.id_brg = b.id where a.nama_brg <> b.nama_brg AND a.nama_brg like '%Kain Twill 20%' ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ /*$query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ $hasilrow = $query3->row();
			//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg); */
			//~ $nama_brg_master	= $this->db->escape_str($row1->nama_brg_master);
						//~ 
			//~ $this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET nama_brg = '$nama_brg_master' WHERE id = '$row1->id' ");
		//~ }
	  //~ }
	  //~ echo "sukses koreksi nama_brg di tm_apply_stok_pembelian_detail untuk item brg kain 's <br>";
	  //~ 
	  //~ $sql = " select a.id, a.nama_brg as nama_brg_sj, b.nama_brg as nama_brg_master from tm_pembelian_makloon_detail a
				//~ inner join tm_barang b on a.id_brg = b.id where a.nama_brg <> b.nama_brg AND a.nama_brg like '%Kain Twill 20%' ";
	  //~ $query	= $this->db->query($sql);
	  //~ if ($query->num_rows() > 0){
		//~ $hasil=$query->result();
		//~ foreach ($hasil as $row1) {
			//~ /*$query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			//~ $hasilrow = $query3->row();
			//~ $nama_brg	= $this->db->escape_str($hasilrow->nama_brg); */
			//~ $nama_brg_master	= $this->db->escape_str($row1->nama_brg_master);
						//~ 
			//~ $this->db->query(" UPDATE tm_pembelian_makloon_detail SET nama_brg = '$nama_brg_master' WHERE id = '$row1->id' ");
		//~ }
	  //~ }
	  //~ echo "sukses koreksi nama_brg di tm_pembelian_makloon_detail untuk item brg kain 's <br>";
  //~ }
}
