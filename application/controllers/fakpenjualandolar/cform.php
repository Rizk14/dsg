<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	

			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$data['tjthtempo']	= "";
		
			$tgl	= date("d");
			$bln	= date("m");
			$tahun	= date("Y");
			
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$tahun;
			$data['tgPajak']	= $tgl."/".$bln."/".$tahun;
			
			$this->load->model('fakpenjualandolar/mclass');
						
			//$data['opt_cabang']	= $this->mclass->lcabang();
			
			// -------------------------------------------------------------	
				$data['isi']='fakpenjualandolar/vmainform';		
			$this->load->view('template',$data);		
			
		
	}

	// 20-07-2013
	function listbarangjadi() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4);
		//$ibranch	= $this->uri->segment(5);
		
		$data['iterasi']	= $iterasi;
		//$data['ibranch']	= $ibranch;
		
		$data['page_title']	= "DATA MASTER BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualandolar/mclass');

		//$query	= $this->mclass->lbarangjadi($ibranch, $is_pakai_sj);
		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/fakpenjualandolar/cform/listbarangjadinext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('fakpenjualandolar/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4);
		//$ibranch	= $this->uri->segment(5);
		
		$data['iterasi']	= $iterasi;
		//$data['ibranch']	= $ibranch;
		
		$data['page_title']	= "DATA MASTER BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualandolar/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/fakpenjualandolar/cform/listbarangjadinext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('fakpenjualandolar/vlistformbrgjadi',$data);
	}
	// ------------------------

	function flistbarangjadi() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		//$ibranchhidden	= $this->input->post('ibranchhidden')?$this->input->post('ibranchhidden'):$this->input->get_post('ibranchhidden');
		
		$iterasi	= $this->uri->segment(4);
		//$ibranch	= $this->uri->segment(5);
		
		$data['iterasi']	= $iterasi;
		//$data['ibranch']	= $ibranch;
		
		//$data['page_title']	= "SURAT JALAN (SJ)";
		$data['isi']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualandolar/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();

		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
					$list .= "
					 <tr>
					  <td width=\"2px;\">".$cc."</td>
					  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->imotif','$row->productname','0','0','0','0','0');\">".$row->imotif."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','0','0','0','0','0');\">".$row->productname."</a></td>
					 </tr>";
				
				 $cc+=1;
			}
		}else{
			$list .= "";
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
	
	function simpan() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['isi']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
				
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$i_faktur	= $this->input->post('i_faktur')?$this->input->post('i_faktur'):$this->input->get_post('i_faktur');
		$d_faktur	= $this->input->post('d_faktur')?$this->input->post('d_faktur'):$this->input->get_post('d_faktur');
		$nama_pelanggan	= $this->input->post('nama_pelanggan');
		
		$i_product	= array();
		$e_product_name	= array();
		$v_hjp	= array();
		$n_quantity	= array();
		$v_unit_price	= array();		
		$e_product_name_0	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iteration;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_hjp[$cacah]	= $this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('v_hjp'.'_'.'tblItem'.'_'.$cacah);
			$n_quantity[$cacah]	= $this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('n_quantity'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
		}
		
		$e_note_faktur	= $this->input->post('e_note_faktur')?$this->input->post('e_note_faktur'):$this->input->get_post('e_note_faktur');
		$v_total_faktur	= $this->input->post('v_total_faktur')?$this->input->post('v_total_faktur'):$this->input->get_post('v_total_faktur');
		//$f_cetak	= $this->input->post('f_cetak')?$this->input->post('f_cetak'):$this->input->get_post('f_cetak');
		$ex_d_faktur	= explode("/",$d_faktur,strlen($d_faktur));
		$nw_d_faktur	= $ex_d_faktur[2]."-".$ex_d_faktur[1]."-".$ex_d_faktur[0]; //YYYY-mm-dd
		
		$ex_v_total_faktur	= explode(".",$v_total_faktur,strlen($v_total_faktur));		
		$nw_v_total_faktur	= $ex_v_total_faktur[0];
		
		$this->load->model('fakpenjualandolar/mclass');
		
		if(!empty($i_faktur) &&
		   !empty($d_faktur)
		) {

				if(!empty($e_product_name_0)) {

					$qnsop	= $this->mclass->cari_fpenjualan($i_faktur);	
					if($qnsop->num_rows() < 0 || $qnsop->num_rows()==0) {
					$this->mclass->msimpan($i_faktur,$nw_d_faktur,$nama_pelanggan,$e_note_faktur,$nw_v_total_faktur,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration);
					} else {
						print "<script>alert(\"Maaf, Nomor Faktur tsb sebelumnya telah diinput. Terimakasih.\");show(\"fakpenjualanjahit/cform\",\"#content\");</script>";
					}

				} else {
					print "<script>alert(\"Maaf, item Faktur Penjulan hrs terisi. Terimakasih.\");show(\"fakpenjualannondo/cform\",\"#content\");</script>";
				}

		} else {
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$tahun	= date("Y");
			$data['tjthtempo']	= "";
			
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
		
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$thn;
			//$data['tgPajak']	= $tgl."/".$bln."/".$thn;
					
			$this->load->model('fakpenjualandolar/mclass');
			
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"fakpenjualandolar/cform\",\"#content\");</script>";
		}
	}
}
?>
