<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-rekap-stok-bb/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'info-rekap-stok-bb/vmainform';
	$data['kel_brg'] = $this->mmaster->get_kel_brg(); 
	$this->load->view('template',$data);

  }
  
  function view(){
	 $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-rekap-stok-bb/vformview';
	$kel_brg = $this->input->post('kel_brg', TRUE);
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
		// query dari tabel tt_stok
		// revisi 15-02-2012: tabel tt_stok udh ga dipake. querynya harus cek dari tabel2 transaksi
		$jum_total = $this->mmaster->get_stoktanpalimit($bulan, $tahun, $kel_brg);
		$data['query'] = $this->mmaster->get_stok($bulan, $tahun, $kel_brg);

		
		// cek di tabel rekap, apakah ada datanya?
		$cek_rekap = $this->mmaster->cek_rekap_stok($bulan, $tahun);
	
		if (count($cek_rekap) != 0) {
			// delete dulu di tabel rekap stok
			$this->db->query(" DELETE FROM tm_rekap_stok_bb WHERE bulan = '$bulan' AND tahun = '$tahun' ");
		}
		
		// save ke tabel rekap stok
		$rekapnya = $data['query'];
		$tgl = date("Y-m-d"); 
		
	/*	if (is_array($rekapnya)) {
			for($j=0;$j<count($rekapnya);$j++){
				$data_header = array(
				  'kode_brg'=>$rekapnya[$j]['kode_brg'],
				  'saldo_awal'=>$rekapnya[$j]['saldo_awal'],
				  'masuk'=>$rekapnya[$j]['jum_masuk'],
				  'total_masuk'=>$rekapnya[$j]['jum_masuk'],
				  'keluar'=>$rekapnya[$j]['jum_keluar'],
				  'total_keluar'=>$rekapnya[$j]['jum_keluar'],
				  'saldo_akhir'=>$rekapnya[$j]['saldo_akhir'],
				  'stok_opname'=>$rekapnya[$j]['jum_stok_opname'],
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'bulan'=>$bulan,
				  'tahun'=>$tahun
				);
				$this->db->insert('tm_rekap_stok_bb',$data_header);
			}
		} */
	
					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";

				$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang 
						WHERE kode = '$kel_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_kel	= $hasilrow->nama;
				}
				else {
					$nama_kel = '';
				}
	
	$data['nama_kel_brg'] = $nama_kel;
	$data['jum_total'] = count($jum_total);
	$data['nama_bulan'] = $nama_bln;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	//$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function cekstokhargadobel(){
	// function ini utk menghapus data2 stok harga yg dobel
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$sql = "SELECT distinct kode_brg, harga, stok FROM tm_stok_harga where quilting='f' ORDER BY kode_brg ASC";
	$query = $this->db->query($sql);
	
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		$hitung = 0;
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT count(*) as jum FROM tm_stok_harga where kode_brg = '$row1->kode_brg'
						AND harga='$row1->harga' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$jumlah	= $hasilrow->jum;
				if ($jumlah >1) {
					//echo $row1->kode_brg." ".$row1->harga." ".$jumlah."<br>";
					$query4	= $this->db->query(" SELECT id FROM tm_stok_harga where kode_brg = '$row1->kode_brg'
						AND harga='$row1->harga' ORDER BY id DESC LIMIT 1 ");
					$hasilrow4 = $query4->row();
					$idnya	= $hasilrow4->id;
					
					$this->db->query(" DELETE FROM tm_stok_harga where id='$idnya' ");
					
					$hitung++;
				}
				else
					echo "data udh dihapus";
			}
		}
	}
	echo $hitung;
	/*$data['isi'] = 'info-rekap-stok-bb/vmainform';
	$data['kel_brg'] = $this->mmaster->get_kel_brg(); 
	$this->load->view('template',$data); */

  }
  
}
