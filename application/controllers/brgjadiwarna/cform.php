<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
			/*$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
			$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
			$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
			$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
			$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
			$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
			$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');		*/
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['lcolor']	= "";
		
			$this->load->model('brgjadiwarna/mclass');
			
			$query	= $this->mclass->view();
			$jml = count($query);
			
			$pagination['base_url'] = base_url().'index.php/brgjadiwarna/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']	= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
		
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['opt_color']	= $this->mclass->icolor();
		
			$data['isi']= 'brgjadiwarna/vmainform';
			$this->load->view('template',$data);
		
	}
function tambah() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
			/*$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
			$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
			$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
			$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
			$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
			$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
			$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');		*/
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['lcolor']	= "";
		
			$this->load->model('brgjadiwarna/mclass');
			
			$query	= $this->mclass->view();
			$jml = count($query);
			
			$pagination['base_url'] = base_url().'/index.php/brgjadiwarna/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']	= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
		
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['opt_color']	= $this->mclass->icolor();
		
			$data['isi']= 'brgjadiwarna/vmainformadd';
			$this->load->view('template',$data);
		
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		/*$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
		$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
		$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
		$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
		$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
		$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
		$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');		
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi'); */
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$data['lcolor']	= "";
		
		$this->load->model('brgjadiwarna/mclass');
		$query	= $this->mclass->view();
		$jml = count($query);
		
		$pagination['base_url'] = base_url().'/index.php/brgjadiwarna/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['opt_color']	= $this->mclass->icolor();
		$data['isi']= 'brgjadiwarna/vmainform';
			$this->load->view('template',$data);
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	$data['isi']= 'brgjadiwarna/vmainform';
			$this->load->view('template',$data);
	}

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['uri']	= $this->uri->segment(4);
		
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']	= "";
		$data['list']	= "";
		$data['lurl']	= base_url();

		$this->load->model('brgjadiwarna/mclass');
		
		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'/index.php/brgjadiwarna/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['query']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		
			$this->load->view('brgjadiwarna/vlistformbrgjadi',$data);
		
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['uri']	= $this->uri->segment(4);
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']	= "";
		$data['list']	= "";
		$data['lurl']	= base_url();

		$this->load->model('brgjadiwarna/mclass');
		$this->load->library('pagination');
		
		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'/index.php/brgjadiwarna/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['query']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('brgjadiwarna/vlistformbrgjadi',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproduct		= $this->input->post('i_product2');
		$jumwarna	= $this->input->post('jumwarna');
		$iproduct = strtoupper($iproduct);
		$icolor = "";
		
		for ($i=1; $i<=$jumwarna; $i++) {
			$icolor	.= $this->input->post('icolor_'.$i).";";
		}
		
		// ------------------------------------END-------------------------------------------
			$this->load->model('brgjadiwarna/mclass');
			$this->mclass->msimpan($iproduct,$icolor);
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4);
		$data['id']	= $id;
		$db2=$this->load->database('db_external', TRUE);
		/*$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
		$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
		$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
		$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
		$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
		$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
		$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');		
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi');		*/
		$data['query']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$data['lcolor']		= "";
		
		$this->load->model('brgjadiwarna/mclass');
		$data['opt_color']	= $this->mclass->icolor();
		
		//--------------------------------------------------------------
		$sqlnya	= $db2->query(" SELECT distinct a.i_product_motif, b.e_product_motifname 
							FROM tr_product_color a, tr_product_motif b
							WHERE a.i_product_motif = b.i_product_motif 
							AND UPPER(a.i_product_motif) = '$id'
							ORDER BY a.i_product_motif ");
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
										WHERE a.i_color = b.i_color AND a.i_product_motif = '".$rownya->i_product_motif."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'i_color'=> $rowxx->i_color,
												'e_color_name'=> $rowxx->e_color_name
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'i_product_motif'=> $rownya->i_product_motif,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		$data['query']	= $outputdata;
		$data['isi']	= 'brgjadiwarna/veditform';
		$this->load->view('template',$data);
		//--------------------------------------------------------------
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproduct		= $this->input->post('i_product2');
		$jumwarna	= $this->input->post('jumwarna');
		$iproduct = strtoupper($iproduct);
		$icolor = "";
		
		for ($i=1; $i<=$jumwarna; $i++) {
			$icolor	.= $this->input->post('icolor_'.$i).";";
		}
		
		// ------------------------------------END-------------------------------------------
			$this->load->model('brgjadiwarna/mclass');
			$this->mclass->mupdate($iproduct,$icolor);
	}
	
	function delete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4);
		$this->load->model('brgjadiwarna/mclass');
		$this->mclass->delete($id);
	}
	
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->input->get_post('cari');
		$tocari	= strtoupper($cari);
		
		/*$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
		$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
		$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
		$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');

		$data['link_aksi']		= $this->lang->line('link_aksi'); */
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('brgjadiwarna/mclass');
		$query	= $this->mclass->viewcari($tocari);
		$jml	= count($query);
		
		$pagination['base_url'] = base_url().'/brgjadiwarna/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($tocari,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('brgjadiwarna/vcariform',$data);
		
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->input->get_post('cari');
		$tocari	= strtoupper($cari);
		
		$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
		$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
		$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
		$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');

		$data['link_aksi']		= $this->lang->line('link_aksi');		
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('brgjadiplusmotif/mclass');
		$query	= $this->mclass->viewcari($tocari);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'/index.php/brgjadiplusmotif/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($tocari,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('brgjadiplusmotif/vcariform',$data);
	}

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$tokey	= strtoupper($key);
		
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']	= "";
		$data['list']	= "";
		$data['lurl']	= base_url();

		$this->load->model('brgjadiwarna/mclass');
		
		$query	= $this->mclass->flbarangjadi($tokey);
		$jml	= $query->num_rows();
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 	
			foreach($query->result() as $row) {
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:setTextfield('$row->motifname','$row->imotif')\">".$row->imotif."</a></td>	 
				  <td><a href=\"javascript:setTextfield('$row->motifname','$row->imotif')\">".$row->motifname."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
		
		echo $item;
	}
	
}
?>
