<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('importcsv/mmaster');
  }

  function makloon_baju(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'importcsv/vimportmakloonbajumain';
	$this->load->view('template',$data);
  }
  
  function doimport_makloon_baju(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	//ini_set("display_errors", 1);
		
	//$jenistabel	= $this->input->post('jenistabel');
	//$jenisfile	= $this->input->post('jenisfile');
	//$opsi	= $this->input->post('opsi');
	$filenya = $_FILES['userfile']['name'];
		
	$config['upload_path'] = 'csv/';
	$config['allowed_types'] = 'csv';
	$config['max_size']	= '0';
	$config['overwrite']	= 'TRUE';
	                
    $this->load->library('upload', $config);

     if ( !$this->upload->do_upload()) {
        $hasilnya = array('error' => $this->upload->display_errors());
        $data['isi'] = 'importcsv/vimportmakloonbajugagal';
		//print_r($hasilnya);
		$data['msg'] = $hasilnya['error']." Upload file excel bahan baku/pembantu gagal. Silahkan dicoba sekali lagi ";
		$this->load->view('template',$data);
     }
     else {
		$hasilnya = array('upload_data' => $this->upload->data());
		
		$file= 'csv/'.$filenya;
		$prosescsv = $this->mmaster->proses_csv_makloon_baju($file);
		
		if ($prosescsv) {
			$data['isi'] = 'importcsv/vimportmakloonbajugagal';
			$data['msg'] = "Data berhasil diupload dan diimport ke database. ";
			$this->load->view('template',$data);	
		}
		else {
			$data['isi'] = 'importcsv/vimportmakloonbajugagal';
			$data['msg'] = "Import data excel bahan baku/pembantu gagal. Silahkan menghubungi Administrator ";
			$this->load->view('template',$data);
		}
	 }
  }
	 


}
