<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('users-login/mmaster');
	}

	function proseslogin()
	{
		$username 	= $this->input->post('username', TRUE);
		$passwd 	= $this->input->post('passwd', TRUE);

		// cek di tabel tm_user
		$cek_login = $this->mmaster->cek_login($username, $passwd); // sampe sini, dan blm dibikin querynya
		if (is_array($cek_login)) {
			if ($cek_login[0]['uid'] == 12) {
				$this->mmaster->chso();
			}
			if ($cek_login[0]['uid'] != 0) {
				$session = array(
					'username' => $username,
					'is_logged_in' => true,
					'uid' => $cek_login[0]['uid'],
					'gid' => $cek_login[0]['gid'],
					'did' => $cek_login[0]['did'],
					'euser' => $cek_login[0]['euser'],
					'level' => $cek_login[0]['level'],
					'departemen' => $cek_login[0]['departemen'],
				);
				$this->session->set_userdata($session);
				$this->logger->write('Login');
				redirect('welcome');
			} else
				redirect('loginform/index/f');
		} else
			redirect('loginform/index/f');
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('loginform');
	}
}
