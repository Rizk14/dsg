<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('trial-gudangwip-packing/mmaster');
  }

  function sounitpacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$data['isi'] = 'trial-gudangwip-packing/vformsounitpacking';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$data['list_unit'] = $this->mmaster->get_unit_packing();
	$this->load->view('template',$data);

  }
  function viewsounitpacking(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$unit_packing = $this->input->post('unit_packing', TRUE);  

	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
			
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	
	$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
	$hasilrow = $query3->row();
	$kode_unit	= $hasilrow->kode_unit;
	$nama_unit	= $hasilrow->nama;
	
	$sql = "SELECT id FROM tt_stok_opname_unit_packing WHERE status_approve = 'f' AND id_unit = '$unit_packing' ";
	if ($bulan == 1) // 24-03-2014
		//$sql.= " AND ((bulan <= '$bulan_sebelumnya' AND bulan<>'$bulan' AND tahun ='$tahun') OR (bulan<='12' AND tahun<'$tahun')) ";
		$sql.= " AND bulan<='12' AND tahun<'$tahun' ";
	else
		$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";
	//echo $sql; die();
	$query3	= $this->db->query($sql);
	
	// 14-01-2016 SEMENTARA DIKOMEN UTK BERESIN DATA SO NOV 2015
	// 22-01-2016 DIBUKA LAGI
	if ($query3->num_rows() > 0){
	$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
		$data['list_unit'] = $this->mmaster->get_unit_packing();
		$data['isi'] = 'trial-gudangwip-packing/vformsounitpacking';
		$this->load->view('template',$data);
	}
	else {
		// cek apakah sudah ada data SO di bulan setelahnya
		
		// 14-01-2016 SEMENTARA DIKOMEN UTK BERESIN DATA SO NOV 2015. udh dibuka lagi
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE bulan > '$bulan' AND tahun >= '$tahun' 
							AND id_unit = '$unit_packing' ");

		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
			$data['list_unit'] = $this->mmaster->get_unit_packing();
			$data['isi'] = 'trial-gudangwip-packing/vformsounitpacking';
			$this->load->view('template',$data);
		}
		else {
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['unit_packing'] = $unit_packing;
			$data['kode_unit'] = $kode_unit;
			$data['nama_unit'] = $nama_unit;

			$cek_data = $this->mmaster->cek_so_unit_packing($bulan, $tahun, $unit_packing); 
			
			if ($cek_data['idnya'] == '' ) { 
				// 30-10-2015, CEK APAKAH DATA DI TABEL SO MASIH KOSONG? JIKA MASIH KOSONG, MAKA MUNCULKAN FORM INPUT BARU
				
				// 09-01-2016, SEMENTARA PAKE INPUT SATU2 DULU SAMPE DATA DESEMBER 2015, JADI YG CEK SOKOSONG DIKOMEN DULU
				// 22-01-2016 DIBUKA LAGI
				$is_sokosong = $this->mmaster->cek_sokosong_unitpacking($unit_packing); 
				if ($is_sokosong == 'f') { // JIKA SUDAH ADA SO
					// jika data so blm ada, maka ambil stok terkini dari tabel tm_stok_unit_packing_warna
					$data['query'] = $this->mmaster->get_stok_unit_packing($bulan, $tahun, $unit_packing);
					$data['is_new'] = '1';
					if (is_array($data['query']) )
						$data['jum_total'] = count($data['query']);
					else
						$data['jum_total'] = 0;
					
					$data['tgl_so'] = '';
					$data['jenis_perhitungan_stok'] ='';
					$data['isi'] = 'trial-gudangwip-packing/vviewsounitpacking';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 30-10-2015 ---------------
						$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing
												WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					$data['is_new'] = '1';
					$data['tgl_so'] = '';
					$data['id_so'] = $id_so;
					$data['isi'] = 'trial-gudangwip-packing/vviewsounitpackingfirst';
					$this->load->view('template',$data);
				}
			}
			else { // jika sudah diapprove maka munculkan msg
				if ($cek_data['status_approve'] == 't') {
					$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah di-approve..!";
					$data['list_unit'] = $this->mmaster->get_unit_packing();
					$data['isi'] = 'trial-gudangwip-packing/vformsounitpacking';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 30-10-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					// get data dari tabel tt_stok_opname_unit_packing yg statusnya 'f'
					$data['query'] = $this->mmaster->get_all_stok_opname_unit_packing($bulan, $tahun, $unit_packing);
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					
					$tgl_so = $cek_data['tgl_so'];
					$pisah1 = explode("-", $tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
					
					$data['tgl_so'] = $tgl_so;
					$data['id_so'] = $id_so;
					$data['jenis_perhitungan_stok'] = $cek_data['jenis_perhitungan_stok'];
					
					// 30-10-2015 sementara dikomen dulu, karena utk input SO pertama kali
					// 22-01-2016 DIBUKA LAGI
					
					// 30-01-2016 kasih pengecekan, apakah ini SO satu2nya yg pernah diinput. kalo ya, maka munculkan firstedit
					$is_satu2nya = $this->mmaster->cek_sopertamakali_unitpacking($unit_packing);
					if ($is_satu2nya == 'f')
						$data['isi'] = 'trial-gudangwip-packing/vviewsounitpacking';
					else
						$data['isi'] = 'trial-gudangwip-packing/vviewsounitpackingfirstedit';
					$this->load->view('template',$data);
					
				}
			} // end else
		} // end else bln setelahnya
	} // end else bln sebelumnya
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++

  }
  function submitsounitpacking() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_packing= $this->input->post('unit_packing', TRUE);  
	  // 12-03-2014 variabel $no ga perlu dipake lg, karena udh ada $jum_data
	 // $no = $this->input->post('no', TRUE);  
	 
	  // 30-10-2015 utk pertama kali, pake yg ini
	  // 22-01-2016 DITUTUP
	  //$jum_data = $this->input->post('no', TRUE)-1; 
	  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  $is_pertamakali = $this->input->post('is_pertamakali', TRUE);
	  
	  // 30-01-2016
	  if ($is_pertamakali == '1')
		$jum_data = $this->input->post('no', TRUE)-1; 
	  else
		$jum_data = $this->input->post('jum_data', TRUE);
		
	  // 05-11-2015
	  $jenis_hitung 	= $this->input->post('jenis_hitung', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
	  $hasilrow = $query3->row();
	  $kode_unit	= $hasilrow->kode_unit;
	  $nama_unit	= $hasilrow->nama;
	  
	  if ($is_new == '1') {
		  $uid_update_by = $this->session->userdata('uid');
	      // insert ke tabel tt_stok_opname_unit_packing
	        $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$id_stok	= $seqrow->id+1;
			}else{
				$id_stok	= 1;
			}
	      $data_header = array(
			  'id'=>$id_stok,
			  'id_unit'=>$unit_packing,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_so'=>$tgl_so,
			  'jenis_perhitungan_stok'=>$jenis_hitung,
			  'uid_update_by'=>$uid_update_by
			);
		  $this->db->insert('tt_stok_opname_unit_packing',$data_header);
		  
		  // ambil data terakhir di tabel tt_stok_opname_
		 /*$query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id; */
	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mmaster->savesounitpacking($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_warna_'.$i, TRUE),
			 
			 //01-12-2015
			 $this->input->post('id_warna2_'.$i, TRUE),
			 $this->input->post('saldo_akhir_warna_'.$i, TRUE),
			 
			 // 22-01-2016, item brg yg mau dihapus dari tm_stok_unit_packing
			 $unit_packing, $this->input->post('hapusitem_'.$i, TRUE)
			 );
		  }
		  
		$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah berhasil disimpan. Silahkan lakukan approval untuk mengupdate stok";
		$data['list_unit'] = $this->mmaster->get_unit_packing();
		$data['isi'] = 'trial-gudangwip-packing/vformsounitpacking';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
			  // ambil data id dari tabel tt_stok_opname_unit_packing
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE id_unit = '$unit_packing'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 			 
			$this->db->query(" delete from tt_stok_opname_unit_packing_detail where id_stok_opname_unit_packing = '$id_stok' ");
			$this->db->query(" delete from tt_stok_opname_unit_packing where id = '$id_stok' ");
			
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah berhasil dihapus";
			$data['list_unit'] = $this->mmaster->get_unit_packing();
			$data['isi'] = 'trial-gudangwip-packing/vformsounitpacking';
			$this->load->view('template',$data);
		  }
		  else { // function update
			  if ($is_pertamakali == '1') {
				  $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE
								bulan = '$bulan' AND tahun = '$tahun' AND id_unit='$unit_packing' ");
					$hasilrow = $query2->row();
					$id_stok	= $hasilrow->id;
					
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tt_stok_opname_unit_packing SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								jenis_perhitungan_stok='$jenis_hitung', uid_update_by='$uid_update_by' where id = '$id_stok' ");
					
					// 1. update data yg sudah ada
					$jum_data_ada = $this->input->post('jum_data', TRUE); 
					for ($i=1;$i<=$jum_data_ada;$i++)
					 {															
						$id_brg_wip1 = $this->input->post('id_brg_wip1_'.$i, TRUE);
						$id_warna1 = $this->input->post('id_warna1_'.$i, TRUE);
						$qty_warna1 = $this->input->post('qty_warna1_'.$i, TRUE);
						$stok1 = $this->input->post('stok1_'.$i, TRUE);
						// 01-12-2015
						$id_warna12 = $this->input->post('id_warna12_'.$i, TRUE);
						$qty_warna12 = $this->input->post('qty_warna12_'.$i, TRUE);
						
						$queryxx	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail WHERE
								id_stok_opname_unit_packing = '$id_stok' AND id_brg_wip = '$id_brg_wip1' ");
						$hasilxx = $queryxx->row();
						$iddetail	= $hasilxx->id;
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna1); $xx++) {
							$id_warna1[$xx] = trim($id_warna1[$xx]);
							$stok1[$xx] = trim($stok1[$xx]);
							$qtytotalstokawal+= $stok1[$xx];				
							$qty_warna1[$xx] = trim($qty_warna1[$xx]);
							$qtytotalstokfisikwarna+= $qty_warna1[$xx];
							
							// 01-12-2015
							$id_warna12[$xx] = trim($id_warna12[$xx]);
							$qty_warna12[$xx] = trim($qty_warna12[$xx]);
							$qtytotalsaldoakhirwarna+= $qty_warna12[$xx];
							
							// 04-02-2016 update di tabel perwarna
							$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail_warna SET jum_stok_opname = '".$qty_warna1[$xx]."',
									saldo_akhir = '".$qty_warna12[$xx]."' WHERE id_stok_opname_unit_packing_detail = '$iddetail'
									AND id_warna = '".$id_warna1[$xx]."' ");
							
						} // end for
												
						$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail SET jum_stok_opname = '".$qtytotalstokfisikwarna."', 
									saldo_akhir = '".$qtytotalsaldoakhirwarna."' where id_brg_wip = '$id_brg_wip1' AND id_stok_opname_unit_packing = '$id_stok' ");
					 } // end for
					// --------------------------------------------------------
					
					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {						
						$tgl = date("Y-m-d H:i:s"); 
						 //-------------- hitung total qty dari detail tiap2 warna -------------------
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$stok_fisik_warna = $this->input->post('stok_fisik_warna_'.$i, TRUE);
						$stok = $this->input->post('stok_'.$i, TRUE);
						// 01-12-2015
						$id_warna2 = $this->input->post('id_warna2_'.$i, TRUE);
						$saldo_akhir_warna = $this->input->post('saldo_akhir_warna_'.$i, TRUE);
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok[$xx] = trim($stok[$xx]);
							$qtytotalstokawal+= $stok[$xx];				
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
							
							// 01-12-2015
							$id_warna2[$xx] = trim($id_warna2[$xx]);
							$saldo_akhir_warna[$xx] = trim($saldo_akhir_warna[$xx]);
							$qtytotalsaldoakhirwarna+= $saldo_akhir_warna[$xx];
						} // end for
						
						$seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
						if($seq->num_rows() > 0) {
							$seqrow	= $seq->row();
							$id_stok_detail	= $seqrow->id+1;
						}else{
							$id_stok_detail	= 1;
						}
																		
						if ($id_brg_wip != '') {
							$data_detail = array(
									'id'=>$id_stok_detail,
									'id_stok_opname_unit_packing'=>$id_stok,
									'id_brg_wip'=>$id_brg_wip, 
									'stok_awal'=>$qtytotalstokawal,
									'jum_stok_opname'=>$qtytotalstokfisikwarna,
									'saldo_akhir'=>$qtytotalsaldoakhirwarna
								);
							$this->db->insert('tt_stok_opname_unit_packing_detail',$data_detail);
							
							// ------------------stok berdasarkan warna brg wip----------------------------
						   if (isset($id_warna) && is_array($id_warna)) {
							for ($xx=0; $xx<count($id_warna); $xx++) {
								$id_warna[$xx] = trim($id_warna[$xx]);
								$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
								//01-12-2015
								$id_warna2[$xx] = trim($id_warna2[$xx]);
								$saldo_akhir_warna[$xx] = trim($saldo_akhir_warna[$xx]);
								
								$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
								if($seq_warna->num_rows() > 0) {
									$seqrow	= $seq_warna->row();
									$idbaru	= $seqrow->id+1;
								}else{
									$idbaru	= 1;
								}
								
								$tt_stok_opname_unit_packing_detail_warna	= array(
										 'id'=>$idbaru,
										 'id_stok_opname_unit_packing_detail'=>$id_stok_detail,
										 'id_warna'=>$id_warna[$xx],
										 'jum_stok_opname'=>$stok_fisik_warna[$xx],
										 'saldo_akhir'=>$saldo_akhir_warna[$xx]
									);
								$this->db->insert('tt_stok_opname_unit_packing_detail_warna',$tt_stok_opname_unit_packing_detail_warna);
							} // end for
						  } // end if
					  }
					// ---------------------------------------------------------------------
				} // END FOR
			  }
			  else {
				  // update ke tabel tt_stok_opname_
				  $uid_update_by = $this->session->userdata('uid');
				   // ambil data terakhir di tabel tt_stok_opname_
					 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE id_unit = '$unit_packing' 
									AND bulan = '$bulan' AND tahun = '$tahun' ");
					 $hasilrow = $query2->row();
					 $id_stok	= $hasilrow->id; 
					 
					 $this->db->query(" UPDATE tt_stok_opname_unit_packing SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								jenis_perhitungan_stok='$jenis_hitung', uid_update_by='$uid_update_by' where id = '$id_stok' ");
					 
				  for ($i=1;$i<=$jum_data;$i++)
				  {
					
					 
					 $this->mmaster->savesounitpacking($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
					 $this->input->post('id_warna_'.$i, TRUE),
					 $this->input->post('stok_'.$i, TRUE),
					 $this->input->post('stok_fisik_warna_'.$i, TRUE),
					 
					 //01-12-2015
					 $this->input->post('id_warna2_'.$i, TRUE),
					 $this->input->post('saldo_akhir_warna_'.$i, TRUE),
					 
					 // 22-01-2016, item brg yg mau dihapus dari tm_stok_unit_packing
					 $unit_packing, $this->input->post('hapusitem_'.$i, TRUE)
					 );
				  }
			  }
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah berhasil diupdate. Silahkan lakukan approval untuk mengupdate stok";
			$data['list_unit'] = $this->mmaster->get_unit_packing();
			$data['isi'] = 'trial-gudangwip-packing/vformsounitpacking';
			$this->load->view('template',$data);
		}
      }
  }
   function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$isedit 	= $this->input->post('isedit', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		if ($isedit == '0') {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
										WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		else {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
			}
			else {
				$id_brg_wip = '0';
			}
			
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
					WHERE id = '".$id_brg_wip."' AND id NOT IN 
					(SELECT b.id_brg_wip FROM tt_stok_opname_unit_packing a 
					INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
					WHERE a.id = '$id_so' ) ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		
		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('trial-gudangwip-packing/vinfobrgwip', $data); 
		return true;
  }
    function additemwarna(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		if ($id_so != '') {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							AND a.id_brg_wip NOT IN 
						(SELECT b.id_brg_wip FROM tt_stok_opname_unit_packing a 
						INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
						WHERE a.id = '$id_so' )
							ORDER BY b.nama");
		}
		else {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							ORDER BY b.nama");
		}
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('trial-gudangwip-packing/vlistwarna', $data); 
		return true;
  }
  function additemwarna2(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		if ($id_so != '') {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							AND a.id_brg_wip NOT IN 
						(SELECT b.id_brg_wip FROM tt_stok_opname_unit_packing a 
						INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
						WHERE a.id = '$id_so' )
							ORDER BY b.nama");
		}
		else {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							ORDER BY b.nama");
		}
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('trial-gudangwip-packing/vlistwarna2', $data); 
		return true;
  }
}
?>
