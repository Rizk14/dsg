<?php
class Cform extends CI_Controller { 
	
  function __construct () {
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('sj-keluar-makloon/mmaster');
  }

function index() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$data['isi'] = 'sj-keluar-makloon/vform';
	$data['msg'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	
	$data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
	$this->load->view('template',$data);

  }
  
  function show_detailprosescutting(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$unit_makloon = $this->uri->segment(4,0);
	
	//$nobonm = $this->input->post('nobonm',FALSE);
	//$cari	= $nobonm;
	
	/*if($nobonm==''){
		$nobonm = "all";
		$cari	= "";
	}*/
	
	//$data['cari']	= $cari;
	
	$data['msg'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	
	$data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
	$data['detailprosescutting'] = $this->mmaster->detailprosescutting();
	
	$this->load->view('sj-keluar-makloon/vpopupbhn',$data);	  
  }
  
  function detailprosescutting(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		 redirect('loginform');
	  }
	  
	  $unit_makloon	= $this->input->post('unit_makloon');
	  //$makloon_internal	= $this->input->post('makloon_internal');
	  $bhn_baku	= $this->input->post('bhn_baku');
	  $iddetail	= $this->input->post('iddetail');

	  //$data['checked_makloon_internal'] = $makloon_internal=='t'?'checked':'';
	  //$data['value_makloon_internal'] = $makloon_internal=='t'?'t':'';
	  
	  $data['unit_makloon'] = $unit_makloon;
	  //$data['makloon_internal'] = $makloon_internal;
	  $data['iddetail'] = $iddetail;
	  
	  $data['isi'] = 'sj-keluar-makloon/vmainform';
	  $data['msg'] = '';
	  $data['kel_brg'] = $this->mmaster->get_kel_brg();
	  $data['list_quilting'] = $this->mmaster->get_supplier();
	  
	  $qget_supplier = $this->mmaster->get_supplier2($unit_makloon);
	  if($qget_supplier){
		  $rget_supplier = $qget_supplier->row();
		  $data['kode_supplier'] = $rget_supplier->kode_supplier;
		  $data['nama_supplier'] = $rget_supplier->nama;
	  }
	  
	  $data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
		  
	  //$expiddetail	= explode(';',$iddetail,strlen($iddetail));
	  $data['detail']	= $this->mmaster->detailprosescuttingnya($iddetail);
	  //$data['detail']	= $iddetail;
	  
	  $this->load->view('template',$data);
  }
  
  function edit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_sj 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$unit_makloon 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
	
	$data['query'] = $this->mmaster->get_sj($id_sj);
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	$data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['unit_makloon'] = $unit_makloon;
	$data['carinya'] = $carinya;

	$data['isi'] = 'sj-keluar-makloon/veditform';
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj = $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$unit_makloon = $this->input->post('unit_makloon', TRUE);  
			$makloon_internal = $this->input->post('makloon_internal', TRUE);  
			if ($makloon_internal == '')
				$makloon_internal = 'f';
			
			$ket = $this->input->post('ket', TRUE);  
			$jenis_makloon = $this->input->post('jenis_makloon', TRUE);  
			$kode_brg_jadi = $this->input->post('kode_brg_jadi', TRUE);  
			//$kode_brg_makloon = $this->input->post('kode_brg_makloon', TRUE);  
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$cunit_makloon = $this->input->post('cunit_makloon', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			// jika edit, var ini ada isinya
			$id_sj_makloon 	= $this->input->post('id_sj_makloon', TRUE);
			
			if ($id_sj_makloon == '') {
				$cek_data = $this->mmaster->cek_data($no_sj, $unit_makloon);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'sj-keluar-makloon/vmainform';
					$data['msg'] = "Data no SJ ".$no_sj." untuk supplier ".$unit_makloon." sudah ada..!";
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$this->mmaster->save($no_sj,$tgl_sj, $unit_makloon, $jenis_makloon, $makloon_internal, 
							$kode_brg_jadi, $ket, 
							$this->input->post('id_apply_stok_detail_'.$i, TRUE), $this->input->post('kode_brg_quilting_'.$i, TRUE),
							$this->input->post('kode_'.$i, TRUE), $this->input->post('nama_'.$i, TRUE), 
							$this->input->post('qty_'.$i, TRUE), $this->input->post('qtym_'.$i, TRUE),
							$this->input->post('detail_pjg_'.$i, TRUE), $this->input->post('ket_benang_'.$i, TRUE) );
					}
					redirect('sj-keluar-makloon/cform/view');
				}
			} // end if id_sj_makloon == ''
			else { // update
				$tgl = date("Y-m-d");
				
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++) {
						$this->db->query(" UPDATE tm_sj_proses_quilting_detail SET 
									ket_benang = '".$this->input->post('ket_benang_'.$i, TRUE)."'
									where id= '".$this->input->post('id_'.$i, TRUE)."' ");
					}
					
					// delete detail, berarti sekaligus reset stok!
				/*	$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting_detail 
											WHERE id_sj_proses_quilting = '$id_sj_makloon' ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
											
						foreach ($hasil2 as $row2) {
							// ============ update stok =====================
							//cek stok terakhir tm_stok, dan update stoknya
									$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$row2->kode_brg' ");
									if ($query3->num_rows() == 0){
										$stok_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_lama	= $hasilrow->stok;
									}
									$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset dari SJ keluar utk makloon
									
									if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
										$data_stok = array(
											'kode_brg'=>$row2->kode_brg,
											'stok'=>$new_stok,
										//	'id_gudang'=>$id_gudang, //
											'tgl_update_stok'=>$tgl
											);
										$this->db->insert('tm_stok',$data_stok);
									}
									else {
										$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg= '$row2->kode_brg' ");
									}
									
									$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input) 
															VALUES ('$row2->kode_brg','$no_sj', '$row2->qty', '$new_stok', '$tgl') ");
							
							// ==============================================
						} // end foreach
					} // end if
					*/
					
					$this->db->query(" UPDATE tm_sj_proses_quilting SET tgl_sj = '$tgl_sj', 
									makloon_internal = '$makloon_internal', 
									keterangan = '$ket', tgl_update = '$tgl'
									where id= '$id_sj_makloon' ");
					
					// ===================================================== what the ****
					//$this->db->delete('tm_sj_proses_quilting_detail', array('id_sj_proses_quilting' => $id_sj_makloon));
										
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "sj-keluar-makloon/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "sj-keluar-makloon/cform/cari/".$cunit_makloon."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('sj-keluar-makloon/cform/view');
			}

  }
  
  function view(){ 
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'sj-keluar-makloon/vformview';
    $keywordcari = "all";
    $unit_makloon = '0';
	//$kode_bagian = '';
	
   $jum_total = $this->mmaster->getAlltanpalimit($unit_makloon, $keywordcari); 
							$config['base_url'] = base_url().'index.php/sj-keluar-makloon/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $unit_makloon, $keywordcari);		
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	$data['unit_makloon'] = $unit_makloon;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$unit_makloon = $this->input->post('unit_makloon', TRUE);  
	
	if ($keywordcari == '' && $unit_makloon == '') {
		$unit_makloon 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($unit_makloon == '')
		$unit_makloon = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($unit_makloon, $keywordcari);
							$config['base_url'] = base_url().'index.php/sj-keluar-makloon/cform/cari/'.$unit_makloon.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $unit_makloon, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'sj-keluar-makloon/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	$data['unit_makloon'] = $unit_makloon;
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_sj 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$unit_makloon 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($id_sj);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "sj-keluar-makloon/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "sj-keluar-makloon/cform/cari/".$unit_makloon."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
					
    //redirect('sj-keluar-makloon/cform/view');
  }
  
  function generate_nomor() {
		//$jenis_brg 	= $this->uri->segment(4);
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);

	if ($posisi == '' || $kel_brg == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg = $this->input->post('kel_brg', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		$id_jenis_bhn = $this->input->post('id_jenis_bhn', TRUE);  
		
		if ($keywordcari == '' && ($id_jenis_bhn == '' || $posisi == '' || $kel_brg == '') ) {
			$kel_brg 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$id_jenis_bhn 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
			
		if ($id_jenis_bhn == '')
			$id_jenis_bhn 	= "0";
		  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $id_jenis_bhn);
		
		$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg/".$kel_brg."/".$posisi."/".$id_jenis_bhn."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $id_jenis_bhn);

	$data['jum_total']	= count($qjum_total);
	$data['posisi']		= $posisi;
	$data['jumdata']	= $posisi-1;
	$data['kel_brg']	= $kel_brg;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;
	$data['jenis_bhn'] = $this->mmaster->get_jenis_bhn($kel_brg);
	$data['cjenis_bhn'] = $id_jenis_bhn;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('sj-keluar-makloon/vpopupbrg',$data);
  }
  
  // brg makloon
  function show_popup_brg_makloon(){
	// =======================
	// disini coding utk pengecekan user login
	//========================

	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jenis_makloon	= $this->uri->segment(4);
	//$posisi 	= $this->uri->segment(5);
	
	/*if ($posisi == '' || $jenis_makloon == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$jenis_makloon 	= $this->input->post('jenis_makloon', TRUE);  
	}*/
	if ($jenis_makloon == '') 
		$jenis_makloon 	= $this->input->post('jenis_makloon', TRUE);  
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		/*if ($keywordcari == '' && ($jenis_makloon == '' || $posisi == '')) {
			$jenis_makloon 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}*/
		
		if ($keywordcari == '' && $jenis_makloon == '') {
			$jenis_makloon 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";

		$qjum_total = $this->mmaster->get_bahan_makloontanpalimit($keywordcari, $jenis_makloon);
		
		//$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_makloon/".$jenis_makloon."/".$posisi."/".$keywordcari."/";
		$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_makloon/".$jenis_makloon."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan_makloon($config['per_page'],$config['cur_page'], $keywordcari, $jenis_makloon);

	$data['jum_total'] = count($qjum_total);
	$data['jenis_makloon'] = $jenis_makloon;
	//$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_jenis_makloon WHERE id = '$jenis_makloon' ");
	$hasilrow = $query3->row();
	$nama_jenis	= $hasilrow->nama;
	$data['nama_jenis'] = $nama_jenis;

	$this->load->view('sj-keluar-makloon/vpopupbrgmakloon',$data);
	
  }
  
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	/*$posisi 	= $this->uri->segment(4);
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}*/
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		/*if ($keywordcari == '' || $posisi == '' ) {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}*/
		if ($keywordcari == '')
			$keywordcari 	= $this->uri->segment(4);
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari);

				//$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_jadi/".$posisi."/".$keywordcari."/";
				$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_jadi/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	//$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('sj-keluar-makloon/vpopupbrgjadi',$data);
  }
  
  // popup utk input detail pjg kain
  function show_popup_detail_pjg(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$posisi 	= $this->uri->segment(4);
	
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
		
		if ($posisi == '') {
			$posisi 	= $this->uri->segment(4);
		}
		
	$data['posisi'] = $posisi;
	$this->load->view('sj-keluar-makloon/vpopupdetailpjg',$data);
	
  }
  
  function cetak() {
	  $isjprosesquilting = $this->uri->segment(4);
	  $unitmakloon = $this->uri->segment(5);

	  $nowdate	= date('Y-m-d');
	  $logfile	= 'logs'.'-'.$nowdate;
	
	  $uri = "/printers/EPSONLX300";
	  $data['host']		= "192.168.0.181";
	  $data['uri']		= $uri;
	  $data['log_destination']= 'logs/'.$logfile;
	  
	  $List = "";
	  $panjang = 95;
	  $Line_banyak = 20;
	  $Line_barang = 75;
	  
	  $arrbulan = array(
		'01'=>'Januari',
		'02'=>'Februari',
		'03'=>'Maret',
		'04'=>'April',
		'05'=>'Mei',
		'06'=>'Juni',
		'07'=>'Juli',
		'08'=>'Agustus',
		'09'=>'September',
		'10'=>'Oktober',
		'11'=>'Nopember',
		'12'=>'Desember');
	  
	  $qsjprosesquilting = $this->mmaster->sjprosesquilting($isjprosesquilting);
	  
	  if($qsjprosesquilting->num_rows()>0) {
		  
		  $rsjprosesquilting = $qsjprosesquilting->row();
		  
		  $qgetsupplier = $this->mmaster->getsupplier($rsjprosesquilting->kode_unit);
		  $rgetsupplier	= $qgetsupplier->row();
		  
		  $no_sj	= $rsjprosesquilting->no_sj;
		  $tglsj	= $rsjprosesquilting->tgl_sj;
		  $exp_tsj	= explode("-",$tglsj,strlen($tglsj));
		  $exp_blnsj	= $exp_tsj[1];
		  $exp_tglsj	= substr($exp_tsj[2],0,1)=='0'?substr($exp_tsj[2],1,1):$exp_tsj[2];
		  $exp_thsj	= substr($exp_tsj[0],2,2);
		  $tgl_sj	= $exp_tglsj." ".$arrbulan[$exp_blnsj];
		  $keterangan = $rsjprosesquilting->keterangan;

		  $data['no_sj'] = $no_sj;
		  $data['tgl_sj'] = $tgl_sj;
		  $data['keterangan'] = $keterangan;
		  
		  $char_tgl = strlen("Bandung")+2+strlen($exp_tglsj)+1+strlen($arrbulan[$exp_blnsj])+9+strlen($exp_thsj)+1; // 1 " "
		  $char_supplier = strlen($rgetsupplier->nama);
		  
		  $List .= CHR(27).CHR(48);
		  
		  $List .= CHR(15)."\n".CHR(18);
		  
		  $List .= CHR(27).CHR(69).str_repeat(" ",($panjang-$char_tgl)-23)."Bandung, ".$tgl_sj.str_repeat(" ",9).$exp_thsj." ".CHR(27).CHR(70)."\n";
		  
		  for($z=0;$z<3;$z++) {
			$List .= CHR(15)."\n".CHR(18);
		  }
		  
		  $List .= CHR(27).CHR(69).str_repeat(" ",($panjang-$char_supplier)-27).$rgetsupplier->nama.CHR(27).CHR(70)."\n";

		  for($z=0;$z<2;$z++) {
			$List .= CHR(15)."\n".CHR(18);
		  }
		  
		  $List .= CHR(15).CHR(27).CHR(69).str_repeat(" ",20).$no_sj.CHR(27).CHR(70).CHR(18);
		  
		  for($z=0;$z<7;$z++) {
			$List .= CHR(15)."\n".CHR(18);
		  }
		
		  $qsjprosesquiltingdetail = $this->mmaster->sjprosesquiltingdetail($isjprosesquilting);
		  
		  $c = 0;
		   
		  foreach($qsjprosesquiltingdetail->result() as $row1) {
			  
			  $qgetkodebrg = $this->mmaster->getkodebrg($row1->kode_brg);
			  $rgetkodebrg = $qgetkodebrg->row();

			  $qgetbrgjadi = $this->mmaster->getbrgjadi($row1->kode_brg_jadi);
			  $rgetbrgjadi = $qgetbrgjadi->row();
			  
			  $qgetbrghslmakloon = $this->mmaster->getbrghslmakloon($row1->kode_brg_makloon);
			  $rgetbrghslmakloon = $qgetbrghslmakloon->row();
			  
			  $detailpjgkain = explode(";",$row1->detail_pjg_kain,strlen($row1->detail_pjg_kain));
			  
			  $linebanyak = $Line_banyak-(strlen($row1->qty_meter)+2); // 2 " m"
			  $linebarang = $Line_barang-(strlen($rgetbrghslmakloon->nama_brg));
			  
			  $List .= CHR(27).CHR(103);
			  	
			  $List .= CHR(15).str_repeat(" ",$linebanyak).$row1->qty_meter." M".str_repeat(" ",8).$rgetkodebrg->nama_brg."".str_repeat(" ",$linebarang)."".$rgetbrghslmakloon->kode_brg."\n".CHR(18);
			  //$List .= CHR(15)."\n".CHR(18);

			  $pkain = 0;
			  $tot = count($detailpjgkain);
			  
			  while($pkain<count($detailpjgkain)) {

				if($c>0 && ($c%2)==0) {
					$List .= CHR(15)."\n".CHR(18);
				}
				
				if($pkain==0) {
					$List .= CHR(15).str_repeat(" ",($Line_banyak+8)).CHR(18);
				}
				
				if(($tot-1)!=$pkain) {
					
					if($pkain%5==0 && $pkain!=0) {
						for($z=0;$z<1;$z++) {
							$List .= CHR(15)."\n".CHR(18);
						}
						$List .= CHR(15).str_repeat(" ",($Line_banyak+8)).CHR(18);
					}
					
					$detailpjgkain_digit_blkng = explode("/",$detailpjgkain[$pkain],strlen($detailpjgkain[$pkain]));
					
					if(($tot-2)!=$pkain) {
						//$List .= CHR(15)."".$detailpjgkain[$pkain].str_repeat(" ",2)."; ".CHR(18);
						$List .= CHR(15)."".$detailpjgkain_digit_blkng[1].str_repeat(" ",2)."; ".CHR(18);
					}else{
						//$List .= CHR(15)."".$detailpjgkain[$pkain].str_repeat(" ",2)."".CHR(18);
						$List .= CHR(15)."".$detailpjgkain_digit_blkng[1].str_repeat(" ",2)."".CHR(18);
					}
					
				}else{
					$List .= CHR(15)."\n".CHR(18);
				}
				
				$c+=1;
				$pkain+=1;
				
			  }
			  
			  $List .= CHR(15)."\n".CHR(18);
			  $List .= CHR(0);
			  
		  }
		  $List .= CHR(0);
		  
		  $data['List'] = $List;
		  
	      $this->load->view('sj-keluar-makloon/vprintform',$data);		
	  }
	  
	  
  }
  
}
