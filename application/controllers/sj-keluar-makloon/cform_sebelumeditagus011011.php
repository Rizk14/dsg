<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('sj-keluar-makloon/mmaster');
  }

function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$data['isi'] = 'sj-keluar-makloon/vmainform';
	$data['msg'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	
	$data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
	$this->load->view('template',$data);

  }
  
  function edit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_sj 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$unit_makloon 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
	
	$data['query'] = $this->mmaster->get_sj($id_sj);
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	$data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['unit_makloon'] = $unit_makloon;
	$data['carinya'] = $carinya;

	$data['isi'] = 'sj-keluar-makloon/veditform';
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj = $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$unit_makloon = $this->input->post('unit_makloon', TRUE);  
			$makloon_internal = $this->input->post('makloon_internal', TRUE);  
			if ($makloon_internal == '')
				$makloon_internal = 'f';
			
			$ket = $this->input->post('ket', TRUE);  
			$jenis_makloon = $this->input->post('jenis_makloon', TRUE);  
			$kode_brg_jadi = $this->input->post('kode_brg_jadi', TRUE);  
			$kode_brg_makloon = $this->input->post('kode_brg_makloon', TRUE);  
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$cunit_makloon = $this->input->post('cunit_makloon', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			// jika edit, var ini ada isinya
			$id_sj_makloon 	= $this->input->post('id_sj_makloon', TRUE);
			
			if ($id_sj_makloon == '') {
				$cek_data = $this->mmaster->cek_data($no_sj, $unit_makloon);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'sj-keluar-makloon/vmainform';
					$data['msg'] = "Data no SJ ".$no_sj." untuk supplier ".$unit_makloon." sudah ada..!";
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$this->mmaster->save($no_sj,$tgl_sj, $unit_makloon, $jenis_makloon, $makloon_internal, 
							$kode_brg_jadi, $kode_brg_makloon, $ket, 
							$this->input->post('kode_'.$i, TRUE), $this->input->post('nama_'.$i, TRUE), 
							$this->input->post('qty_'.$i, TRUE), $this->input->post('qtym_'.$i, TRUE),
							$this->input->post('detail_pjg_'.$i, TRUE) );
						
					}
					redirect('sj-keluar-makloon/cform/view');
				}
			} // end if id_sj_makloon == ''
			else { // update
				$tgl = date("Y-m-d");
				
					$jumlah_input=$no-1;
					
					// delete detail, berarti sekaligus reset stok!
				/*	$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting_detail 
											WHERE id_sj_proses_quilting = '$id_sj_makloon' ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
											
						foreach ($hasil2 as $row2) {
							// ============ update stok =====================
							//cek stok terakhir tm_stok, dan update stoknya
									$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$row2->kode_brg' ");
									if ($query3->num_rows() == 0){
										$stok_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_lama	= $hasilrow->stok;
									}
									$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset dari SJ keluar utk makloon
									
									if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
										$data_stok = array(
											'kode_brg'=>$row2->kode_brg,
											'stok'=>$new_stok,
										//	'id_gudang'=>$id_gudang, //
											'tgl_update_stok'=>$tgl
											);
										$this->db->insert('tm_stok',$data_stok);
									}
									else {
										$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg= '$row2->kode_brg' ");
									}
									
									$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input) 
															VALUES ('$row2->kode_brg','$no_sj', '$row2->qty', '$new_stok', '$tgl') ");
							
							// ==============================================
						} // end foreach
					} // end if
					*/
					
					$this->db->query(" UPDATE tm_sj_proses_quilting SET tgl_sj = '$tgl_sj', 
									makloon_internal = '$makloon_internal', 
									keterangan = '$ket', tgl_update = '$tgl'
									where id= '$id_sj_makloon' ");
					
					// ===================================================== what the ****
					//$this->db->delete('tm_sj_proses_quilting_detail', array('id_sj_proses_quilting' => $id_sj_makloon));
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$qty = $this->input->post('qty_'.$i, TRUE);
						$qtym = $this->input->post('qtym_'.$i, TRUE);
						$qty_lama = $this->input->post('qty_lama_'.$i, TRUE);
						$kodenya = $this->input->post('kode_'.$i, TRUE);
						$detail_pjg = $this->input->post('detail_pjg_'.$i, TRUE);
						
						if (($qty != $qty_lama) && ($qty!='' || $qty!=0)) {
							// cek dulu satuan brgnya. jika satuannya yard, maka qty (m) yg tadi diambil itu konversikan lagi ke yard
						/*	$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
												WHERE a.satuan = b.id AND a.kode_brg = '$kodenya' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$satuan	= '';
								}
								
								if ($satuan == "Yard") {
									$qty_sat_awal = $qty / 0.91;
									$qty_lama_sat_awal = $qty_lama / 0.91;
								}
								else {
									$qty_sat_awal = $qty;
									$qty_lama_sat_awal = $qty_lama;
								} */
							
							// ============ reset stok =====================
							//cek stok terakhir tm_stok, dan update stoknya
									$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kodenya' ");
									if ($query3->num_rows() == 0){
										$stok_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_lama	= $hasilrow->stok;
									}
									$new_stok = $stok_lama+$qty_lama; // bertambah stok karena reset dari SJ keluar utk makloon
									
									if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
										$data_stok = array(
											'kode_brg'=>$kodenya,
											'stok'=>$new_stok,
											'tgl_update_stok'=>$tgl
											);
										$this->db->insert('tm_stok',$data_stok);
									}
									else {
										$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg= '$kodenya' ");
									}
									
									// ############################ new 140611, tm_stok_harga. cek stok di tiap2 harga, ambil stok yg msh ada
									// diurutkan dari harga lama
								/*	$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga 
																WHERE kode_brg = '$kodenya' ORDER BY id ASC ");
									if ($query2->num_rows() > 0){
										$hasil2=$query2->result();
															
										foreach ($hasil2 as $row2) {
											$stoknya = $row2->stok;
											$harganya = $row2->harga;
											
											if ($stoknya != 0) {
												break;
											}
										}
									}
									//
									$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kodenya' 
																AND harga = '$harganya' ");
									if ($query3->num_rows() == 0){
										$stok_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_lama	= $hasilrow->stok;
									}
										
									$new_stok = $stok_lama+$qty_lama_sat_awal; // 
									
									if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
										$data_stok = array(
											'kode_brg'=>$kodenya,
											'stok'=>$new_stok,
											'harga'=>$harganya,
											'tgl_update_stok'=>$tgl
											);
										$this->db->insert('tm_stok_harga',$data_stok);
									}
									else {
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg= '$kodenya' AND harga = '$harganya' "); // sampe siniii
									}
									*/
									// #############################
									
									// @@@@@@@ modifikasi 15 juni 2011
									// query dari tabel tt_stok
									$query2	= $this->db->query(" SELECT id, masuk, keluar, harga FROM tt_stok 
													WHERE kode_brg = '$kodenya' AND no_bukti = '$no_sj' 
													AND kode_unit= '$unit_makloon' ORDER BY id DESC ");
									if ($query2->num_rows() > 0){
										$hasil2=$query2->result();															
										foreach ($hasil2 as $row2) {
											$ttmasuk = $row2->masuk;
											$ttkeluar = $row2->keluar;
											$ttharga = $row2->harga;
											
											if ($ttmasuk != '')
												break;
											
											$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kodenya' 
																AND harga = '$ttharga' ");
											if ($query3->num_rows() == 0){
												$stok_lama = 0;
											}
											else {
												$hasilrow = $query3->row();
												$stok_lama	= $hasilrow->stok;
											}
											$stokreset = $stok_lama+$ttkeluar;
											
											$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
												where kode_brg= '$kodenya' AND harga = '$ttharga' "); //
									
											$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_unit, masuk, saldo, tgl_input, harga) 
													VALUES ('$kodenya','$no_sj', '$unit_makloon', '$ttkeluar', '$stokreset', '$tgl', '$ttharga') ");
										}
									}
									// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
									
									/*$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
															VALUES ('$kodenya','$no_sj', '$qty_lama_sat_awal', '$new_stok', '$tgl', '$harganya') "); */

							// ==============================================
							
							//cek qty, jika lebih besar dari stok maka otomatis disamakan dgn sisa stoknya
							// koreksi: ini udh dihandle pake jquery (18-08-2011)
						/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '".$kodenya."' ");
							$hasilrow = $query3->row();
							$stok = $hasilrow->stok; // ini stok terkini di tm_stok
						*/		
							/*if ($satuan == "Yard") {
								$qty_sat_awal = $qty / 0.91;
							}
							
							if ($qty_sat_awal > $stok)
								$qty_sat_awal = $stok;
							if ($stok == '')
								$qty_sat_awal = '0'; */
							
						/*	if ($qty > $stok)
								$qty = $stok;
							if ($stok == '')
								$qty = '0'; */
							
							// jika semua data tdk kosong, update
							$this->db->query(" UPDATE tm_sj_proses_quilting_detail SET qty = '$qty', 
									qty_meter = '$qtym', detail_pjg_kain = '$detail_pjg'
									where id = '".$this->input->post('id_'.$i, TRUE)."' ");
						
						// ============ update stok =====================
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok 
								WHERE kode_brg = '".$kodenya."' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								
								//$new_stokreset = $stok_lama+$qty_lama_sat_awal;
								$new_stok = $stok_lama-$qty;
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok',$data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."' ");
								}
								
								// ############################ new 140611, tm_stok_harga. cek stok di tiap2 harga, ambil stok yg msh ada
								// diurutkan dari harga lama
								$selisih = 0;
								$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga 
												WHERE kode_brg = '$kodenya' ORDER BY id ASC ");
								if ($query2->num_rows() > 0){
									$hasil2=$query2->result();
										
									$temp_selisih = 0;				
									foreach ($hasil2 as $row2) {
										$stoknya = $row2->stok;
										$harganya = $row2->harga;
										
										if ($stoknya > 0) {
											if ($temp_selisih == 0)
												$selisih = $stoknya-$qty;
											else
												$selisih = $stoknya+$temp_selisih;
											
											if ($selisih < 0) {
												$temp_selisih = $selisih;
												$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
															where kode_brg= '$kodenya' AND harga = '$harganya' "); //
												
												$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_unit, keluar, saldo, tgl_input, harga) 
														VALUES ('$kodenya','$no_sj', '$unit_makloon', '$stoknya', '0', '$tgl', '$harganya') ");
											}
												
											if ($selisih > 0) { // ke-2 masuk sini
												if ($temp_selisih == 0)
													$temp_selisih = $qty; // temp_selisih = -6
													
												break;
											}
										}
									}
								}
								
								if ($selisih != 0) {
									$new_stok = $selisih;
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg= '$kodenya' AND harga = '$harganya' "); //
									
									// #############################
									
									//1. insert ke tabel tt_stok dgn tipe keluar, 
									$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_unit, keluar, saldo, tgl_input, harga) 
									VALUES ('".$kodenya."','$no_sj', '$unit_makloon', '".abs($temp_selisih)."', '$new_stok', '$tgl', '$harganya') ");
								 }
						
						// ==============================================
						}
						
					} // end for
					
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "sj-keluar-makloon/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "sj-keluar-makloon/cform/cari/".$cunit_makloon."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('sj-keluar-makloon/cform/view');
			}

  }
  
  function view(){ 
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'sj-keluar-makloon/vformview';
    $keywordcari = "all";
    $unit_makloon = '0';
	//$kode_bagian = '';
	
   $jum_total = $this->mmaster->getAlltanpalimit($unit_makloon, $keywordcari); 
							$config['base_url'] = base_url().'index.php/sj-keluar-makloon/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $unit_makloon, $keywordcari);		
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	$data['unit_makloon'] = $unit_makloon;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$unit_makloon = $this->input->post('unit_makloon', TRUE);  
	
	if ($keywordcari == '' && $unit_makloon == '') {
		$unit_makloon 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($unit_makloon == '')
		$unit_makloon = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($unit_makloon, $keywordcari);
							$config['base_url'] = base_url().'index.php/sj-keluar-makloon/cform/cari/'.$unit_makloon.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $unit_makloon, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'sj-keluar-makloon/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['list_quilting'] = $this->mmaster->get_supplier();
	$data['unit_makloon'] = $unit_makloon;
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_sj 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$unit_makloon 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($id_sj);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "sj-keluar-makloon/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "sj-keluar-makloon/cform/cari/".$unit_makloon."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
					
    //redirect('sj-keluar-makloon/cform/view');
  }
  
  function generate_nomor() {
		//$jenis_brg 	= $this->uri->segment(4);
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);

	if ($posisi == '' || $kel_brg == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg 	= $this->input->post('kel_brg', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		$id_jenis_bhn = $this->input->post('id_jenis_bhn', TRUE);  
		
		if ($keywordcari == '' && ($id_jenis_bhn == '' || $posisi == '' || $kel_brg == '') ) {
			$kel_brg 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$id_jenis_bhn 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
			
		if ($id_jenis_bhn == '')
			$id_jenis_bhn 	= "0";
		  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $id_jenis_bhn);
		
		$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg/".$kel_brg."/".$posisi."/".$id_jenis_bhn."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $id_jenis_bhn);

	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	$data['kel_brg'] = $kel_brg;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;
	$data['jenis_bhn'] = $this->mmaster->get_jenis_bhn($kel_brg);
	$data['cjenis_bhn'] = $id_jenis_bhn;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('sj-keluar-makloon/vpopupbrg',$data);
  }
  
  // brg makloon
  function show_popup_brg_makloon(){
	// =======================
	// disini coding utk pengecekan user login
	//========================

	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jenis_makloon	= $this->uri->segment(4);
	//$posisi 	= $this->uri->segment(5);
	
	/*if ($posisi == '' || $jenis_makloon == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$jenis_makloon 	= $this->input->post('jenis_makloon', TRUE);  
	}*/
	if ($jenis_makloon == '') 
		$jenis_makloon 	= $this->input->post('jenis_makloon', TRUE);  
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		/*if ($keywordcari == '' && ($jenis_makloon == '' || $posisi == '')) {
			$jenis_makloon 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}*/
		
		if ($keywordcari == '' && $jenis_makloon == '') {
			$jenis_makloon 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";

		$qjum_total = $this->mmaster->get_bahan_makloontanpalimit($keywordcari, $jenis_makloon);
		
		//$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_makloon/".$jenis_makloon."/".$posisi."/".$keywordcari."/";
		$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_makloon/".$jenis_makloon."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan_makloon($config['per_page'],$config['cur_page'], $keywordcari, $jenis_makloon);

	$data['jum_total'] = count($qjum_total);
	$data['jenis_makloon'] = $jenis_makloon;
	//$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_jenis_makloon WHERE id = '$jenis_makloon' ");
	$hasilrow = $query3->row();
	$nama_jenis	= $hasilrow->nama;
	$data['nama_jenis'] = $nama_jenis;

	$this->load->view('sj-keluar-makloon/vpopupbrgmakloon',$data);
	
  }
  
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	/*$posisi 	= $this->uri->segment(4);
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}*/
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		/*if ($keywordcari == '' || $posisi == '' ) {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}*/
		if ($keywordcari == '')
			$keywordcari 	= $this->uri->segment(4);
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari);

				//$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_jadi/".$posisi."/".$keywordcari."/";
				$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_jadi/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	//$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('sj-keluar-makloon/vpopupbrgjadi',$data);
  }
  
  // popup utk input detail pjg kain
  function show_popup_detail_pjg(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$posisi 	= $this->uri->segment(4);
	
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
		
		if ($posisi == '') {
			$posisi 	= $this->uri->segment(4);
		}
		
	$data['posisi'] = $posisi;
	$this->load->view('sj-keluar-makloon/vpopupdetailpjg',$data);
	
  }
  
}
