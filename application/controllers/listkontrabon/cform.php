<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			
			$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
			$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
			$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
			$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
			$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
			$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
			$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
			$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
			$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
			$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
			$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
			$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
			$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
			$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
			$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
			$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			
			$data['detail']		= "";
			$data['list']		= "";
			$data['disabled']	= 'f';
			$data['limages']	= base_url();
			$data['isi']	= 'listkontrabon/vmainform';	
			$this->load->view('template',$data);	
			
			$this->load->model('listkontrabon/mclass');
			
			
		
	}
	
	function carilistkontrabon() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
		$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
		$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
		$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
		$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
		$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
		$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
		$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
		$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
		$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
		$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
		$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
		$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lkontrabon']	= "";		
		$data['limages']	= base_url();
		
		$nokontrabon		= $this->input->post('no_kontrabon');
		$ikontrabon			= $this->input->post('i_kontrabon');
		
		$dkontrabonfirst= $this->input->post('d_kontrabon_first');
		$dkontrabonlast	= $this->input->post('d_kontrabon_last');
		
		$tf_nota_sederhana	= $this->input->post('tf_nota_sederhana');
		
		$data['tglkontrabonmulai']	= (!empty($dkontrabonfirst) || $dkontrabonfirst!='0')?$dkontrabonfirst:'';
		$data['tglkontrabonakhir']	= (!empty($dkontrabonlast) || $dkontrabonlast!='0')?$dkontrabonlast:'';
		$data['nokotrabon']	= (!empty($nokontrabon))?$nokontrabon:'';
		$data['ikontrabon']	= (!empty($ikontrabon))?$ikontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
		
		$e_d_kontrabon_first= explode("/",$dkontrabonfirst,strlen($dkontrabonfirst));
		$e_d_kontrabon_last	= explode("/",$dkontrabonlast,strlen($dkontrabonlast));
		
		$ndkontrabonfirst	= !empty($e_d_kontrabon_first[2])?$e_d_kontrabon_first[2].'-'.$e_d_kontrabon_first[1].'-'.$e_d_kontrabon_first[0]:'0';
		$ndkontrabonlast	= !empty($e_d_kontrabon_last[2])?$e_d_kontrabon_last[2].'-'.$e_d_kontrabon_last[1].'-'.$e_d_kontrabon_last[0]:'0';
		
		$turi1	= ($nokontrabon!='' && $nokontrabon!='0')?$nokontrabon:'0';
		$turi2	= ($ikontrabon!='' && $ikontrabon!='0')?$ikontrabon:'0';
		$turi3	= $ndkontrabonfirst;
		$turi4	= $ndkontrabonlast;
		$turi5	= $tf_nota_sederhana;
		
		$data['turi1']	= $nokontrabon;
		$data['turi2']	= $ikontrabon;
		$data['turi3']	= $ndkontrabonfirst;
		$data['turi4']	= $ndkontrabonlast;
		$data['turi5']	= $tf_nota_sederhana;
		
		$this->load->model('listkontrabon/mclass');
		
		$pagination['base_url'] 	= '/listkontrabon/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if(($dkontrabonfirst!='') && ($dkontrabonlast!='')) {
			$qlistkontrabon	= $this->mclass->clistkontrabon($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 1;
		}else{
			$qlistkontrabon	= $this->mclass->clistkontrabon2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 2;
		}
		
		$pagination['total_rows']	= $qlistkontrabon->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if(($dkontrabonfirst!='') && ($dkontrabonlast!='')) {
			$data['query']	= $this->mclass->clistkontrabonperpages($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}else{
			$data['query']	= $this->mclass->clistkontrabonperpages2($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}
		$data['isi']	= 'listkontrabon/vlistform';	
			$this->load->view('template',$data);	
		
	}

	function carilistkontrabonnext() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
		$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
		$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
		$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
		$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
		$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
		$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
		$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
		$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
		$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
		$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
		$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
		$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lkontrabon']	= "";		
		$data['limages']	= base_url();
		
		$nokontrabon	= $this->uri->segment(4);
		$ikontrabon		= $this->uri->segment(5);
		$ndkontrabonfirst	= $this->uri->segment(6);
		$ndkontrabonlast	= $this->uri->segment(7);
		$tf_nota_sederhana = $this->uri->segment(8); 

		$data['turi1']	= $nokontrabon;
		$data['turi2']	= $ikontrabon;
		$data['turi3']	= $ndkontrabonfirst;
		$data['turi4']	= $ndkontrabonlast;
		$data['turi5']	= $tf_nota_sederhana;
		
		$e_d_do_first	= ($ndkontrabonfirst!='0')?explode("-",$ndkontrabonfirst,strlen($ndkontrabonfirst)):'';
		$e_d_do_last	= ($ndkontrabonlast!='0')?explode("-",$ndkontrabonlast,strlen($ndkontrabonlast)):'';

		$nddofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:'0';
		$nddolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:'0';
						
		$data['tglkontrabonmulai']	= (!empty($nddofirst) && $nddofirst!='0')?$nddofirst:'';
		$data['tglkontrabonakhir']	= (!empty($nddolast) && $nddolast!='0')?$nddolast:'';
		$data['nokotrabon']	= (!empty($nokontrabon) || $nokontrabon!='0')?$nokontrabon:'';
		$data['ikontrabon']	= (!empty($ikontrabon))?$ikontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
		
		$turi1	= ($nokontrabon!='' || $nokontrabon!='0')?$nokontrabon:'0';
		$turi2	= ($ikontrabon!='' || $ikontrabon!='0')?$ikontrabon:'0';
		$turi3	= $ndkontrabonfirst;
		$turi4	= $ndkontrabonlast;
		$turi5	= $tf_nota_sederhana;
		
		$this->load->model('listkontrabon/mclass');
		
		$pagination['base_url'] 	= '/listkontrabon/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if(($ndkontrabonfirst!='0') && ($ndkontrabonlast!='0')) {
			$qlistkontrabon	= $this->mclass->clistkontrabon($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 1;
		} else {
			$qlistkontrabon	= $this->mclass->clistkontrabon2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 2;
		}
		
		$pagination['total_rows']	= $qlistkontrabon->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if(($ndkontrabonfirst!='0') && ($ndkontrabonlast!='0')) {
			$data['isi']	= $this->mclass->clistkontrabonperpages($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}else{
			$data['isi']	= $this->mclass->clistkontrabonperpages2($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}
		
		$this->load->view('listkontrabon/vlistform',$data);	
	}
	
	
	function listbarangjadi() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana = $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		$this->load->model('listkontrabon/mclass');

		$query	= $this->mclass->lbarangjadi($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listkontrabon/cform/listbarangjadinext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);		
				
		$this->load->view('listkontrabon/vlistformbrgjadi',$data);			
	}

	function listbarangjadinext() {
		$data['page_title']	= "KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana = $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		$this->load->model('listkontrabon/mclass');

		$query	= $this->mclass->lbarangjadi($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listkontrabon/cform/listbarangjadinext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);		
				
		$this->load->view('listkontrabon/vlistformbrgjadi',$data);			
	}		

	function flistbarangjadi() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$fnotasederhana = $this->input->post('fnotasederhana')?$this->input->post('fnotasederhana'):$this->input->get_post('fnotasederhana');
		
		$data['page_title']	= "KONTRA BON";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('listkontrabon/mclass');

		$query	= $this->mclass->flbarangjadi($key,$fnotasederhana);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				$f_nota_sederhana = ($row->f_nota_sederhana=='t')?'Faktur Non DO':'';
				
				$ikontrabon	= trim($row->idtcode);
					
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->idt','$ikontrabon')\">".$row->idtcode."</a></td>
				  <td><a href=\"javascript:settextfield('$row->idt','$ikontrabon')\">".$row->ddt."</a></td>
				  <td><a href=\"javascript:settextfield('$row->idt','$ikontrabon')\">".$f_nota_sederhana."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	
	function listfaktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4);
		$f_nota_sederhana = $this->uri->segment(5);
		
		$data['iterasi']	= $iterasi;
		
		$data['f_nota_sederhana']	= $f_nota_sederhana;
		
		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listkontrabon/mclass');

		$query	= $this->mclass->lfaktur($f_nota_sederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listkontrabon/cform/listfakturnext/'.$iterasi.'/'.$f_nota_sederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$f_nota_sederhana);		

		$this->load->view('listkontrabon/vlistfaktur',$data);
	}
	
	function listfakturnext() {
		
		$iterasi	= $this->uri->segment(4);
		$f_nota_sederhana = $this->uri->segment(5);
		
		$data['iterasi']	= $iterasi;
		$data['f_nota_sederhana']	= $f_nota_sederhana;

		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listkontrabon/mclass');

		$query	= $this->mclass->lfaktur($f_nota_sederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listkontrabon/cform/listfakturnext/'.$iterasi.'/'.$f_nota_sederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$f_nota_sederhana);		

		$this->load->view('listkontrabon/vlistfaktur',$data);		
	}
	
	function flistfaktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key2	= $this->input->post('key');
		$f_nota_sederhana = $this->input->post('f_nota_sederhana');
		
		$data['f_nota_sederhana']	= $f_nota_sederhana;
		
		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['lurl']		= base_url();

		$this->load->model('listkontrabon/mclass');

		$query	= $this->mclass->flfaktur($key2,$f_nota_sederhana);
		$jml	= $query->num_rows();
		
		$list	= "";
		
		$bln	= array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember');
		
		if($jml>0) {
			
			$cc	= 1;
			
			foreach($query->result() as $row){
				
				$tgl	= explode("-",$row->d_faktur,strlen($row->d_faktur)); // YYYY-mm-dd
				$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
				$tanggal		= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
				
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_faktur_code','$row->i_faktur','$row->d_faktur','$row->d_faktur','$row->d_due_date','$row->d_due_date','$row->e_branch_name','$row->v_total_fppn','$row->i_customer','$row->i_branch_code')\">".$row->i_faktur_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_faktur_code','$row->i_faktur','$row->d_faktur','$row->d_faktur','$row->d_due_date','$row->d_due_date','$row->e_branch_name','$row->v_total_fppn','$row->i_customer','$row->i_branch_code')\">".$tanggal."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= "<table class=\"listtable2\"><tbody>".$list."</tbody></table>";
		
		echo $item;		
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
			$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
			$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
			$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
			$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
			$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
			$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
			$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
			$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
			$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
			$data['list_kontrabon_no_kontrabon']= $this->lang->line('list_kontrabon_no_kontrabon');
			
			$data['button_update']	= $this->lang->line('button_update');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['disabled']	= 'f';
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$tahun	= date("Y");		
			$data['tjthtempo']	= "";
			
			$idt	= $this->uri->segment(4);
			$idtcode= $this->uri->segment(5);
			$fnotasederhana = $this->uri->segment(6);
			
			$data['idt']	= $idt;
			$data['idtcode']= $idtcode;
			$data['fnotasederhana'] = $fnotasederhana;
			 
			$this->load->model('listkontrabon/mclass');
			
			$qdt	= $this->mclass->getdtheader($idt,$fnotasederhana);
			
			if($qdt->num_rows()>0) {
				
				$rdt	= $qdt->row();
				
				$exp_ddt= explode("-",$rdt->d_dt,strlen($rdt->d_dt)); // YYYY-mm-dd
				
				$data['idtcode']	= $rdt->i_dt_code;
				$data['ddt']		= $exp_ddt[2]."/".$exp_ddt[1]."/".$exp_ddt[0];
				$data['idt']		= $rdt->i_dt;
			} else {
				$data['idtcode']	= "";
				$data['ddt']		= "";
				$data['idt']		= "";
			}		
			
			$data['idtitem']	= $this->mclass->ldtitem($idt,$fnotasederhana);
			
			$this->load->view('listkontrabon/veditform',$data);
		
	}
	
	
	function detail() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
			$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
			$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
			$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
			$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
			$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
			$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
			$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
			$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
			$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
			$data['list_kontrabon_no_kontrabon']= $this->lang->line('list_kontrabon_no_kontrabon');
			$data['list_piutang_nilai_kontrabon'] = $this->lang->line('list_piutang_nilai_kontrabon');
			
			$data['button_update']	= $this->lang->line('button_update');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['disabled']	= 'f';
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$tahun	= date("Y");
			$data['tjthtempo']	= "";
			
			$idtcode	= $this->uri->segment(4);			 			
			$idt		= $this->uri->segment(5);
			$ndkontrabonfirst	= $this->uri->segment(6);
			$ndkontrabonlast	= $this->uri->segment(7);
			$fnotasederhana 	= $this->uri->segment(8);
			
			$data['idt']	= $idt;
			$data['idtcode']= $idtcode;
			$data['fnotasederhana'] = $fnotasederhana;

			$data['turi1']	= $idtcode;
			$data['turi2']	= $idt;
			$data['turi3']	= $ndkontrabonfirst;
			$data['turi4']	= $ndkontrabonlast;
			$data['turi5']	= $fnotasederhana;
									 
			$this->load->model('listkontrabon/mclass');
			
			$qdt	= $this->mclass->getdtheader2($idt,$fnotasederhana);
			
			if($qdt->num_rows()>0) {
				
				$rdt	= $qdt->row();
				
				$exp_ddt= explode("-",$rdt->d_dt,strlen($rdt->d_dt)); // YYYY-mm-dd
				
				$data['idtcode']	= $rdt->i_dt_code;
				$data['ddt']		= $exp_ddt[2]."/".$exp_ddt[1]."/".$exp_ddt[0];
				$data['idt']		= $rdt->i_dt;
			}else{
				$data['idtcode']	= "";
				$data['ddt']		= "";
				$data['idt']		= "";
			}
			
			$data['idtitem']	= $this->mclass->ldtitem2($idt,$fnotasederhana);
			
			$this->load->view('listkontrabon/vformdetail',$data);
			
	}

	function backlistkontrabon() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
		$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
		$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
		$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
		$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
		$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
		$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
		$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
		$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
		$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
		$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
		$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
		$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lkontrabon']	= "";		
		$data['limages']	= base_url();
				
		$nokontrabon		= $this->uri->segment(4);
		$ikontrabon			= $this->uri->segment(5);
		$ndkontrabonfirst	= $this->uri->segment(6);
		$ndkontrabonlast	= $this->uri->segment(7);
		$tf_nota_sederhana	= $this->uri->segment(8);
		
		$data['tglkontrabonmulai']	= '';
		$data['tglkontrabonakhir']	= '';
		$data['nokotrabon']	= (!empty($nokontrabon))?$nokontrabon:'';
		$data['ikontrabon']	= (!empty($ikontrabon))?$ikontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
		
		$turi1	= $nokontrabon;
		$turi2	= $ikontrabon;
		$turi3	= '0';
		$turi4	= '0';
		$turi5	= $tf_nota_sederhana;
			
		$data['turi1']	= $turi1;
		$data['turi2']	= $turi2;
		$data['turi3']	= $ndkontrabonfirst;
		$data['turi4']	= $ndkontrabonlast;
		$data['turi5']	= $tf_nota_sederhana;
		
		$this->load->model('listkontrabon/mclass');
		
		$pagination['base_url'] 	= '/listkontrabon/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		$qlistkontrabon	= $this->mclass->clistkontrabon2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		$data['templates']	= 2;
		
		$pagination['total_rows']	= $qlistkontrabon->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->clistkontrabonperpages2($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			
		
		$data['isi']	='listkontrabon/vmainform';
		$this->load->view('template',$data);	
	}
		
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_kontrabon_code	= $this->input->post('i_kontrabon_code');
		$ikontrabon			= $this->input->post('idt');
		$tgl_kontrabon		= $this->input->post('tgl_kontrabon');
		$iteration	= $this->input->post('iteration');
		$fnotasederhana	= $this->input->post('f_nota_sederhana');
		
		$i_faktur_code_0	= $this->input->post('i_faktur_code'.'_'.'tblItem'.'_'.'0');
		
		$i_faktur_code	= array();
		$i_faktur	= array();
		$tgl_faktur	= array();
		$tgl_fakturhidden	= array();
		$due_date	= array();
		$due_datehidden	= array();
		$e_branch_name	= array();
		$v_total_plusppn = array();
		$i_customer = array();
		$i_branch_code = array();
		
		for($cacah=0;$cacah<=$iteration;$cacah++){
			
			$i_faktur_code[$cacah]	= $this->input->post('i_faktur_code'.'_'.'tblItem'.'_'.$cacah);
			$i_faktur[$cacah]	= $this->input->post('i_faktur'.'_'.'tblItem'.'_'.$cacah);
			$tgl_faktur[$cacah]	= $this->input->post('tgl_faktur'.'_'.'tblItem'.'_'.$cacah);
			$tgl_fakturhidden[$cacah]	= $this->input->post('tgl_fakturhidden'.'_'.'tblItem'.'_'.$cacah);
			$due_date[$cacah]	= $this->input->post('due_date'.'_'.'tblItem'.'_'.$cacah);
			$due_datehidden[$cacah]	= $this->input->post('due_datehidden'.'_'.'tblItem'.'_'.$cacah);
			$e_branch_name[$cacah]	= $this->input->post('e_branch_name'.'_'.'tblItem'.'_'.$cacah);
			$v_total_plusppn[$cacah]	= $this->input->post('v_total_plusppn'.'_'.'tblItem'.'_'.$cacah);
			$i_customer[$cacah]	= $this->input->post('i_customer'.'_'.'tblItem'.'_'.$cacah);
			$i_branch_code[$cacah]	= $this->input->post('i_branch_code'.'_'.'tblItem'.'_'.$cacah);		
			
		}
		
		$this->load->model('listkontrabon/mclass');
		
		if(!empty($i_kontrabon_code) &&
		   !empty($ikontrabon) && 
		   !empty($tgl_kontrabon)
		) {
			$exp_tgl_kontrabon = explode("/",$tgl_kontrabon,strlen($tgl_kontrabon)); // dd/mm/YYYY
			$tglkontrabon2	= $exp_tgl_kontrabon[2]."-".$exp_tgl_kontrabon[1]."-".$exp_tgl_kontrabon[0];
			
			if(!empty($i_faktur_code_0)) {
				$this->mclass->mupdate($i_kontrabon_code,$ikontrabon,$tglkontrabon2,$iteration,$i_faktur_code,$i_faktur,$tgl_faktur,$tgl_fakturhidden,$due_date,$due_datehidden,$e_branch_name,$v_total_plusppn,$i_customer,$i_branch_code,$fnotasederhana);
			} else {
				print "<script>alert(\"Maaf, item Kontra Bon hrs terisi. Terimakasih.\");show(\"listkontrabon/cform\",\"#content\");</script>";
			}
		} else {
			$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
			$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
			$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
			$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
			$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
			$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
			$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
			$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
			$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
			$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
			$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
			$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
			$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
			$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
			$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['disabled']	= 'f';
			$data['limages']	= base_url();

			$this->load->model('listkontrabon/mclass');

			print "<script>alert(\"Maaf, Kontra Bon gagal disimpan. Terimakasih.\");show(\"listkontrabon/cform\",\"#content\");</script>";				
			
			$this->load->view('listkontrabon/vmainform',$data);			
		}	
	}

	function cari_fpenjualan(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$fpenj	= $this->input->post('fpenj')?$this->input->post('fpenj'):$this->input->get_post('fpenj');
		$fpenjhiden	= $this->input->post('fpenjhiden')?$this->input->post('fpenjhiden'):$this->input->get_post('fpenjhiden');
		$this->load->model('listpenjualanperdo/mclass');
		$qnsop	= $this->mclass->cari_fpenjualan($fpenj,$fpenjhiden);
		if($qnsop->num_rows()>0) {
			echo "Maaf, No. faktur sudah ada!";		
		}
	}
	
	function undo(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_dt	= $this->uri->segment(4);
		$i_dt_code	= $this->uri->segment(5);
		$fnotasederhana = $this->uri->segment(6);
		
		$this->load->model('listkontrabon/mclass');
		$this->mclass->mbatal($i_dt,$i_dt_code,$fnotasederhana);		
	}
}
?>
