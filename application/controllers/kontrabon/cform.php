<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
			$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
			$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
			$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
			$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
			$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
			$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
			$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
			$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
			$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
			$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			$data['isi']	= 'kontrabon/vmainform';
			$this->load->view('template',$data);
			$this->load->model('kontrabon/mclass');
			
		
	}

	function listfaktur() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana	= $this->uri->segment(4);
		$data['fnotasederhana']	= $fnotasederhana;
		// var_dump($fnotasederhana);
		// die();
		
		$this->load->model('kontrabon/mclass');

		$query	= $this->mclass->lfaktur($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/kontrabon/cform/listfakturnext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 2000;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);
				
		$this->load->view('kontrabon/vlistformbrgjadi',$data);			
	}	

	function listfakturnext() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana	= $this->uri->segment(4);
		$data['fnotasederhana']	= $fnotasederhana;
		
		$this->load->model('kontrabon/mclass');

		$query	= $this->mclass->lfaktur($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/kontrabon/cform/listfakturnext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 2000;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);
				
		$this->load->view('kontrabon/vlistformbrgjadi',$data);			
	}

	function flistfaktur() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$fnotasederhana = $this->input->post('fnotasederhana')?$this->input->post('fnotasederhana'):$this->input->get_post('fnotasederhana');
		
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('kontrabon/mclass');

		$query	= $this->mclass->flfaktur($key,$fnotasederhana);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode','$row->i_faktur')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode','$row->i_faktur')\">".$row->dfaktur."</a></td>
				  <td><a href=\"javascript:settextfield('$row->ifakturcode','$row->i_faktur')\">"."Blm Kontra Bon"."</a></td>
				  <td>&nbsp;</td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	
	function carifaktur() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
		$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
		$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
		$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
		$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
		$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
		$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
		$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
		$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
		$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
		$data['list_kontrabon_nota_sederhana']	= $this->lang->line('list_kontrabon_nota_sederhana');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['limages']	= base_url();

		$tahun	= date("Y");
		
		$this->load->model('kontrabon/mclass');
		

		$qthn	= $this->mclass->get_thnkontrabon();
		$qno	= $this->mclass->get_nokontrabon();

		if($qthn->num_rows() > 0) {
			$th		= $qthn->row_array();
			$thn	= $th['thn'];
		} else {
			$thn	= $tahun;
		}
		
		if($thn==$tahun) {
			if($qno->num_rows() > 0)  {
				$row	= $qno->row_array();
				$ino	= $row['icode']+1;
				
				switch(strlen($ino)) {
					case "1":
						$nomorkontrabon	= "0000".$ino;
					break;
					case "2":
						$nomorkontrabon	= "000".$ino;
					break;	
					case "3":
						$nomorkontrabon	= "00".$ino;
					break;
					case "4":
						$nomorkontrabon	= "0".$ino;
					break;
					case "5":
						$nomorkontrabon	= $ino;
					break;	
				}
			} else {
				$nomorkontrabon		= "00001";
			}
			$nomor	= $tahun.$nomorkontrabon;
		} else {
			$nomor	= $tahun."00001";
		}
		$data['no']	= $nomor;
	 
		$nofaktur	= $this->input->post('no_faktur');
		$ifaktur	= $this->input->post('i_faktur');
		$tf_nota_sederhana = $this->input->post('tf_nota_sederhana');
		
		$data['nofaktur']	= $nofaktur;
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
		
		$expifaktur	= explode("#",$ifaktur);
		
		if(sizeof($expifaktur)>0) {
			
			$wh = 0;
			$list = '';
			$iter = 0;
			
			$discount	= 0;
			$totalnya = 0;
			$nilai_ppn	= 0;
			$totalgrand	= 0;

			
			
			while($wh<count($expifaktur)) {
				
				if($tf_nota_sederhana=='f') {
					$query	= $this->mclass->clistfaktur($expifaktur[$wh]);
					$query2 = $this->mclass->totalfaktur($expifaktur[$wh]);
				}else{
					$query	= $this->mclass->clistfakturNONDO($expifaktur[$wh]);
					$query2 = $this->mclass->totalfakturNONDO($expifaktur[$wh]);
				}
				
				$row	= $query->row();
				$row2	= $query2->row();
				
				$nilai_ppn_perfaktur = (($row2->totalfaktur*11) / 100);
				
				if($row->v_discount>0){
					$total_sblm_ppn_perfaktur	= round($row2->totalfaktur - $row->v_discount); // DPP
					$nilai_ppn2_perfaktur	= round((($total_sblm_ppn_perfaktur*11) / 100));
					$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
					$total_grand_perfakturm	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur+$row->v_materai);
				}else{
					$total_sblm_ppn_perfaktur	= round($row2->totalfaktur); // DPP
					$nilai_ppn2_perfaktur	= round($nilai_ppn_perfaktur);
					$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
					$total_grand_perfakturm	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur+$row->v_materai);
				}
									
				$exp_d_faktur = explode("-",$row->d_faktur,strlen($row->d_faktur)); // YYYY-mm-dd
				$exp_d_due_date	= explode("-",$row->d_due_date,strlen($row->d_due_date));
				
				$list	.= "
					<tr>
						<td><div style=\"font:24px;text-align:right;width:12px;margin-right:0px;\">".($iter*1+1)."</div></td>
						
						<td>
						<DIV ID=\"ajax_i_faktur_code_tblItem_".$iter."\" style=\"width:80px;\">
						<input type=\"text\" ID=\"i_faktur_code_tblItem_".$iter."\" name=\"i_faktur_code_tblItem_".$iter."\" style=\"width:78px;\" value=\"".trim($row->i_faktur_code)."\" readonly >
						<input type=\"hidden\" ID=\"i_faktur_tblItem_".$iter."\" name=\"i_faktur_tblItem_".$iter."\" value=\"".$row->i_faktur."\">
						</DIV>
						</td>
						
						<td>
						<DIV ID=\"ajax_tgl_faktur_tblItem_".$iter."\" style=\"width:100px;\" >
						<input type=\"text\" ID=\"tgl_faktur_tblItem_".$iter."\" name=\"tgl_faktur_tblItem_".$iter."\" style=\"width:98px;text-align:right;\" value=\"".$exp_d_faktur[2]."/".$exp_d_faktur[1]."/".$exp_d_faktur[0]."\" readonly >
						<input type=\"hidden\" name=\"tgl_fakturhidden_tblItem_".$iter."\" id=\"tgl_fakturhidden_tblItem_".$iter."\" value=\"".$row->d_faktur."\">
						</DIV>
						</td>
						
						<td>
						<DIV ID=\"ajax_due_date_tblItem_".$iter."\" style=\"width:100px;\" >
						<input type=\"text\" ID=\"due_date_tblItem_".$iter."\" name=\"due_date_tblItem_".$iter."\" style=\"width:100px;text-align:right;\" value=\"".$exp_d_due_date[2]."/".$exp_d_due_date[1]."/".$exp_d_due_date[0]."\" readonly >
						<input type=\"hidden\" name=\"due_datehidden_tblItem_".$iter."\" id=\"due_datehidden_tblItem_".$iter."\" value=\"".$row->d_due_date."\">
						</DIV>
						</td>
								
						<td><DIV ID=\"ajax_e_branch_name_tblItem_".$iter."\" style=\"width:220px;\" >
						<input type=\"text\" ID=\"e_branch_name_tblItem_".$iter."\" name=\"e_branch_name_tblItem_".$iter."\" style=\"width:218px;text-align:left;\" value=\"".$row->e_branch_name."\" readonly ></DIV></td>

						<td><DIV ID=\"ajax_materai_tblItem_".$iter."\" style=\"width:220px;\" >
						<input type=\"text\" ID=\"materai_tblItem_".$iter."\" name=\"e_branch_name_tblItem_".$iter."\" style=\"width:218px;text-align:left;\" value=\"".$row->v_materai."\" readonly ></DIV></td>


						<td><DIV ID=\"ajax_v_total_plusppn_tblItem_".$iter."\" style=\"width:95px;text-align:right;\" >


						<input type=\"hidden\" ID=\"v_total_plusppn_tblItem_".$iter."\" name=\"v_total_plusppn_tblItem_".$iter."\" style=\"width:93px;text-align:right;\" value=\"".number_format($total_grand_perfaktur,'2','.',',')."\" readonly ></DIV>
						<input type=\"text\" ID=\"v_total_plusppnm_tblItem_".$iter."\" name=\"v_total_plusppnm_tblItem_".$iter."\" style=\"width:93px;text-align:right;\" value=\"".number_format($total_grand_perfakturm,'2','.',',')."\" readonly ></DIV>
						<input type=\"hidden\" ID=\"i_customer_tblItem_".$iter."\" name=\"i_customer_tblItem_".$iter."\" value=\"".$row->i_customer."\">
						<input type=\"hidden\" ID=\"i_branch_code_tblItem_".$iter."\" name=\"i_branch_code_tblItem_".$iter."\" value=\"".$row->i_branch_code."\">
						<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\">
						</td>
					</tr>";
					
					$discount	= $discount+$row->v_discount;
					
					/*** 17112011
					$v_total	= $row->v_discount+$row->v_total_faktur; // di field tsb sudah dikurangi discount maka hrs ditambah dulu
					$totalnya 	= $totalnya+$v_total;
					
					$ppn		= (($row->v_total_faktur*10) / 100); // total fakturnya itu adalah total faktur setelah dikurangi discount
					$nilai_ppn	= $nilai_ppn+$ppn;
					
					$totalgrand = $totalgrand+$row->v_total_fppn; // setelah dikurangi discount & ditambah ppn
					***/
					
					$v_total	= $row2->totalfaktur;
					$totalnya 	= $totalnya+$v_total;
					
					$ppn		= $nilai_ppn2_perfaktur;
					$nilai_ppn	= $nilai_ppn+$ppn;
					
					$totalgrand = $totalgrand+$total_grand_perfaktur;
										
				$iter++;	
				$wh+=1;
			}
		}else{
			$query	= $this->mclass->clistfaktur($ifaktur);
			$row	= $query->row();
			
			$list	.= "
			
			";
		}
		
		$data['total']		= $totalnya;
		$data['discount']	= $discount;
		$data['nilai_ppn']	= $nilai_ppn;
		$data['totalgrand'] = $totalgrand;
		$data['query']		= $list;
		
		$data['isi']='kontrabon/vlistform';		
			$this->load->view('template',$data);
		
	}
	
	function carinokontrabon() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$nokontrabon = $this->input->post('nokontrabon');
		
		$this->load->model('kontrabon/mclass');
		$qndo	= $this->mclass->carinokontrabon($nokontrabon);
		if($qndo->num_rows()>0) {
			echo "Maaf, No. Kontra Bon sdh ada!";
		}		
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_faktur_code	= array();
		$i_faktur	= array();
		$tgl_faktur	= array();
		$due_date	= array();
		$e_branch_name	= array();
		$v_total_plusppn = array();
		$i_customer	= array();
		$i_branch_code	= array();
		
		$iteration	= $this->input->post('iteration');
		$totalgrandhidden	= $this->input->post('totalgrandhidden');
		$totalhidden		= $this->input->post('totalhidden');
		$discounthidden		= $this->input->post('discounthidden');
		$nilai_ppnhidden	= $this->input->post('nilai_ppnhidden');
		$f_nota_sederhana	= $this->input->post('tf_nota_sederhana');
		$no_kontrabon = $this->input->post('no_kontrabon');
		
		$tgl_kontrabon	= $this->input->post('tgl_kontrabon');
		$exptgl_kontrabon	= explode("/",$tgl_kontrabon,strlen($tgl_kontrabon)); // dd/mm/YYYY
		$tglkontrabon	= $exptgl_kontrabon[2]."-".$exptgl_kontrabon[1]."-".$exptgl_kontrabon[0];
		
		$i_faktur_code_tblItem_0	= $this->input->post('i_faktur_code_tblItem_0');
		
		for($jumlah=0; $jumlah<=$iteration; $jumlah++){
			$i_faktur_code[$jumlah]	= $this->input->post('i_faktur_code_tblItem_'.$jumlah);
			$i_faktur[$jumlah]	= $this->input->post('i_faktur_tblItem_'.$jumlah);
			$tgl_faktur[$jumlah]	= $this->input->post('tgl_fakturhidden_tblItem_'.$jumlah);
			$due_date[$jumlah]	= $this->input->post('due_datehidden_tblItem_'.$jumlah);
			$e_branch_name[$jumlah]	= $this->input->post('e_branch_name_tblItem_'.$jumlah);
			$v_total_plusppn[$jumlah] = $this->input->post('v_total_plusppn_tblItem_'.$jumlah);
			$i_customer[$jumlah]	= $this->input->post('i_customer_tblItem_'.$jumlah);
			$i_branch_code[$jumlah]	= $this->input->post('i_branch_code_tblItem_'.$jumlah);
		}
		
		$this->load->model('kontrabon/mclass');
		
		if(!empty($i_faktur_code_tblItem_0)) {
			$this->mclass->msimpan($i_faktur_code,$i_faktur,$tgl_faktur,$due_date,$e_branch_name,$v_total_plusppn,$i_customer,$i_branch_code,$totalgrandhidden,$totalhidden,$discounthidden,$nilai_ppnhidden,$tglkontrabon,$iteration,$f_nota_sederhana,$no_kontrabon);
		}else{
				print "<script>alert(\"Maaf, Kontra Bon gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
	}
	
}
?>
