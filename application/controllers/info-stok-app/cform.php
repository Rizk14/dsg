<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-stok-app/mmaster');
  }

 function wip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang_qc();
	$data['isi'] = 'info-stok-app/vmainformwip';
	$this->load->view('template',$data);

  }
 function viewwip(){ // modif 12-11-2015
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-stok-app/vformviewwip';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }
   function exportexport_excel_wip() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='16' align='center'>LAPORAN MUTASI STOK BARANG WIP</th>
		 </tr>
		 <tr>
			<th colspan='16' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='16' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%' rowspan='2'>No</th>
			 <th width='15%' rowspan='2'>Kode Brg</th>
			 <th width='25%' rowspan='2'>Nama Brg</th>
			 <th width='8%' rowspan='2'>S.Awal</th>
			 <th colspan='6'>Masuk</th>
			 <th colspan='5'>Keluar</th>
			 <th width='8%' rowspan='2'>Saldo Akhir</th>
			 <th width='8%' rowspan='2'>Stok Opname</th>
			 <th width='8%' rowspan='2'>Selisih</th>
		 </tr>
		 <tr>
			 <th width='8%'>Bgs</th>
			 <th width='8%'>Hsl Perbaikan</th>
			 <th width='8%'>Retur Unit Packing</th>
			 <th width='8%'>Retur Gdg Jadi</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Total</th>
			 <th width='8%'>Bgs Packing</th>
			 <th width='8%'>Bgs Gdg Jadi</th>
			 <th width='8%'>Retur Perbaikan</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Total</th>
		 </tr>
		</thead>
		<tbody>";
		// 12-09-2014, kolom setelah retur ke unit jahit dihilangkan
		// <th width='8%'>Lainnya</th>
		
			if (is_array($query)) {
				$temp_kodekel = "";
			 for($j=0;$j<count($query);$j++){
				 if ($temp_kodekel != $query[$j]['kode_kel']) {
					 $temp_kodekel = $query[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='18'>&nbsp;<b>".$query[$j]['kode_kel']." - ".$query[$j]['nama_kel']."</b></td>
					</tr>";
				 }
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td align='right'>".$query[$j]['saldo_awal']."</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_perb_unit']."</td>
				 <td align='right'>".$query[$j]['jum_ret_unit_pack']."</td>
				 <td align='right'>".$query[$j]['jum_ret_gd_jadi']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain']."</td>";
				 $tot_masuk = $query[$j]['jum_masuk']+$query[$j]['jum_perb_unit']+$query[$j]['jum_ret_unit_pack']+$query[$j]['jum_ret_gd_jadi']+$query[$j]['jum_masuk_lain'];
				 $html_data.="<td align='right'>".$tot_masuk."</td>";
				 
				 $html_data.=
				 "<td align='right'>".$query[$j]['jum_keluar_pack']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_gdjadi']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_perb']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain']."</td>";
				 $tot_keluar = $query[$j]['jum_keluar_pack']+$query[$j]['jum_keluar_gdjadi']+$query[$j]['jum_keluar_perb']+$query[$j]['jum_keluar_lain'];
				 $html_data.="<td align='right'>".$tot_keluar."</td>";
				 
				//$saldo_akhir = $query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain1']+$query[$j]['jum_masuk_lain2']+$query[$j]['jum_masuk_lain3']+$query[$j]['jum_masuk_lain4']-$query[$j]['jum_keluar1']-$query[$j]['jum_keluar2']-$query[$j]['jum_keluar_lain1']-$query[$j]['jum_keluar_lain2'];
				$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_perb_unit']+$query[$j]['jum_ret_unit_pack']+$query[$j]['jum_ret_gd_jadi']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar_pack']-$query[$j]['jum_keluar_gdjadi']-$query[$j]['jum_keluar_perb']-$query[$j]['jum_keluar_lain'];
				$selisih = $query[$j]['jum_stok_opname']-$saldo_akhir;
				$html_data.= "<td align='right'>".$saldo_akhir."</td>
				<td align='right'>".$query[$j]['jum_stok_opname']."</td>
				<td align='right'>".$selisih."</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_approve__mutasi_stok_wip";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;
		
		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  function transaksihasiljahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-stok-app/vformlaptransaksihasiljahit';
	$this->load->view('template',$data);
  }
    function viewtransaksihasiljahit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-stok-app/vviewlaptransaksihasiljahit';
	$id_gudang = $this->input->post('id_gudang', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	
	$data['query'] = $this->mmaster->get_transaksi_hasil_jahit($id_gudang, $bulan, $tahun, $id_brg_wip);
	$data['jum_total'] = count($data['query']);
	
	if ($id_gudang != '0') {
		$query3	= $this->db->query(" SELECT a.nama as nama_lokasi, b.kode_gudang, b.nama as nama_gudang FROM tm_lokasi_gudang a 
				INNER JOIN tm_gudang b ON a.id = b.id_lokasi 
				WHERE b.id = '$id_gudang' ");
		$hasilrow = $query3->row();
		$nama_lokasi	= $hasilrow->nama_lokasi;
		$nama_gudang	= $hasilrow->nama_gudang;
		$kode_gudang	= $hasilrow->kode_gudang;
		$gudangnya = "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang;
	}
	else {
		//$gudangnya = "Semua";
		$kode_gudang = "";
		$nama_gudang = "";
	}
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip;
	
	$data['id_gudang'] = $id_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
}
