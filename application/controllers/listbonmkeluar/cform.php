<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() { 
 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$data['page_title_outbonm']	= $this->lang->line('page_title_outbonm');
			$data['list_tgl_mulai_outbonm']	= $this->lang->line('list_tgl_mulai_outbonm');
			$data['form_title_detail_outbonm']	= $this->lang->line('form_title_detail_outbonm');
			$data['list_no_outbonm']	= $this->lang->line('list_no_outbonm');
			$data['list_tgl_outbonm']	= $this->lang->line('list_tgl_outbonm');
			$data['list_kd_brg_outbonm']	= $this->lang->line('list_kd_brg_outbonm');
			$data['list_nm_brg_outbonm']	= $this->lang->line('list_nm_brg_outbonm');
			$data['list_qty_brg_outbonm']	= $this->lang->line('list_qty_brg_outbonm');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');		
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$this->load->model('listbonmkeluar/mclass');
			$data['isi']	= 'listbonmkeluar/vmainform';
			$this->load->view('template',$data);
			
			
	}
	
	function carilistbonmkeluar() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_outbonm']	= $this->lang->line('page_title_outbonm');
		$data['list_tgl_mulai_outbonm']	= $this->lang->line('list_tgl_mulai_outbonm');
		$data['form_title_detail_outbonm']	= $this->lang->line('form_title_detail_outbonm');
		$data['list_no_outbonm']	= $this->lang->line('list_no_outbonm');
		$data['list_tgl_outbonm']	= $this->lang->line('list_tgl_outbonm');
		$data['list_kd_brg_outbonm']	= $this->lang->line('list_kd_brg_outbonm');
		$data['list_nm_brg_outbonm']	= $this->lang->line('list_nm_brg_outbonm');
		$data['list_qty_brg_outbonm']	= $this->lang->line('list_qty_brg_outbonm');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lbonmkeluar']	= "";
		
		$bonmkeluar	= $this->input->post('bonmkeluar')? $this->input->post('bonmkeluar'): $this->input->get_post('bonmkeluar');
		$dbonmkeluarfirst	= $this->input->post('d_bonmkeluar_first')? $this->input->post('d_bonmkeluar_first'): $this->input->get_post('d_bonmkeluar_first');
		$dbonmkeluarlast	= $this->input->post('d_bonmkeluar_last')? $this->input->post('d_bonmkeluar_last'): $this->input->get_post('d_bonmkeluar_last');
		
		$data['bonmkeluar']	= $bonmkeluar;
		$data['tglbonmkeluarmulai']	= $dbonmkeluarfirst;
		$data['tglbonmkeluarakhir']	= $dbonmkeluarlast;
		
		$edbonmkeluarfirst	= explode("/", $dbonmkeluarfirst, strlen($dbonmkeluarfirst)); // dd/mm/YYYY
		$edbonmkeluarlast	= explode("/", $dbonmkeluarlast, strlen($dbonmkeluarlast)); // dd/mm/YYYY
		
		$ndbonmkeluar_first	= (!empty($edbonmkeluarfirst[2]) && strlen($edbonmkeluarfirst[2])==4)?$edbonmkeluarfirst[2].'-'.$edbonmkeluarfirst[1].'-'.$edbonmkeluarfirst[0]:"";
		$ndbonmkeluar_last	= (!empty($edbonmkeluarlast[2]) && strlen($edbonmkeluarfirst[2])==4)?$edbonmkeluarlast[2].'-'.$edbonmkeluarlast[1].'-'.$edbonmkeluarlast[0]:"";
				
		$this->load->model('listbonmkeluar/mclass');

		if($ndbonmkeluar_first=='' || $ndbonmkeluar_last==''){
			$data['template']	= '1';
		}else{
			$data['template']	= '0';
		}
		
		if($ndbonmkeluar_first=='' || $ndbonmkeluar_last==''){		
			$data['query']	= $this->mclass->clistbonmkeluar($bonmkeluar,$ndbonmkeluar_first,$ndbonmkeluar_last);
		}elseif($bonmkeluar!=''){
			$data['query']	= $this->mclass->clistbonmkeluar2($bonmkeluar,$ndbonmkeluar_first,$ndbonmkeluar_last);
		}else{
			$data['query']	= $this->mclass->clistbonmkeluar2($bonmkeluar,$ndbonmkeluar_first,$ndbonmkeluar_last);
		}
		
		//print_r($data['isi']); die();
		$data['isi']	= 'listbonmkeluar/vlistform';
		$this->load->view('template',$data);
		
	}
	
	function edit() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ioutbonm	= $this->uri->segment(4);
		
		$data['page_title_bmk']	= $this->lang->line('page_title_bmk');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$data['form_kepada_bmk']	= $this->lang->line('form_kepada_bmk');
		$data['form_option_kepada_bmk']	= $this->lang->line('form_option_kepada_bmk');
		$data['form_nomor_bmk']	= $this->lang->line('form_nomor_bmk');
		$data['form_tgl_bmk']	= $this->lang->line('form_tgl_bmk');
		$data['form_title_detail_bmk']	= $this->lang->line('form_title_detail_bmk');
		$data['form_kode_produk_bmk']	= $this->lang->line('form_kode_produk_bmk');
		$data['form_nm_produk_bmk']	= $this->lang->line('form_nm_produk_bmk');
		$data['form_jml_bmk']	= $this->lang->line('form_jml_bmk');
		$data['button_update']	= $this->lang->line('button_update');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['lkepada']	= "";

		$this->load->model('listbonmkeluar/mclass');

		$qbonmkeluar	= $this->mclass->getbonmkeluar($ioutbonm);
		
		if($qbonmkeluar->num_rows()>0) {
			$row_bonmkeluar		= $qbonmkeluar->row();
			$ibonmkeluarcode	= $row_bonmkeluar->i_outbonm_code;
			$ibonmkeluar		= $row_bonmkeluar->i_outbonm;
			$data['ibonmkeluarcode']	= $ibonmkeluarcode;
			$data['ibonmkeluar']	= $ibonmkeluar;
			$data['etooutbonm']		= trim($row_bonmkeluar->e_to_outbonm);
			$tglbonmkeluar	= explode("-",$row_bonmkeluar->d_outbonm,strlen($row_bonmkeluar->d_outbonm)); // YYYY-mm-dd
			$data['tBonMkeluar']	= $tglbonmkeluar[2]."/".$tglbonmkeluar[1]."/".$tglbonmkeluar[0];
			$data['tBonMkeluarH']	= $row_bonmkeluar->d_outbonm;
			$data['bonmkeluaritem']	= $this->mclass->getbonmkeluaritem($ibonmkeluar);
		}

		$data['opt_kepada']	= $this->mclass->lkepada();
		$data['isi']	= 'listbonmkeluar/veditform';
		$this->load->view('template',$data);
		
	}
	
	
	function actedit() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration;
		$e_to_outbonm	= $this->input->post('e_to_outbonm');
		$i_outbonm_code	= $this->input->post('i_outbonm');
		$i_outbonm_hidden	= $this->input->post('i_outbonm_hidden');
		$i_outbonm_code_hidden	= $this->input->post('i_outbonm_code_hidden');
		$d_outbonm	= $this->input->post('d_outbonm');
		$ex_d_outbonm	= explode("/",$d_outbonm,strlen($d_outbonm));
		$doutbonm	= $ex_d_outbonm[2]."-".$ex_d_outbonm[1]."-".$ex_d_outbonm[0];
		
		if($ex_d_outbonm[2]=='' || $ex_d_outbonm[1]=='' || $ex_d_outbonm[0]=='')
			$doutbonm	= date("Y-m-d");
		
		$i_product	= array();
		$e_product_name	= array();
		//$n_count_product	= array();
		$qty_product	= array();
		$f_stp	= array();
		$iso	= array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah);
			
			//$n_count_product[$cacah]	= $this->input->post('n_count_product_'.'tblItem'.'_'.$cacah);
			$qty_warna[$cacah]	= $this->input->post('qty_warna_'.$cacah);
			$i_product_color[$cacah]	= $this->input->post('i_product_color_'.$cacah);
			$i_color[$cacah]	= $this->input->post('i_color_'.$cacah);
			
			$qty_product[$cacah]	= $this->input->post('qty_product_'.'tblItem'.'_'.$cacah);
			$f_stp[$cacah]	= $this->input->post('f_stp_'.'tblItem'.'_'.$cacah);
			$iso[$cacah]	= $this->input->post('iso_'.'tblItem'.'_'.$cacah);
		}
		
		$this->load->model('listbonmkeluar/mclass');
		
		if( !empty($e_to_outbonm) && 
		    !empty($i_outbonm_code) && 
		    !empty($d_outbonm) ) {
			if(!empty($i_product_0)) {
				$this->mclass->mupdate($i_outbonm_code,$e_to_outbonm,$i_outbonm_hidden,$i_outbonm_code_hidden,$doutbonm,$i_product,$e_product_name,
				$qty_warna, $i_product_color, $i_color, $qty_product,$iterasi,$f_stp,$iso);
			}else{
				print "<script>alert(\"Maaf, item utk Bon M Keluar harus terisi. Terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
				
			}
		}else{
			print "<script>alert(\"Maaf, Bon M Keluar gagal diupdate!\");
			window.open(\"index\", \"_self\");</script>";
		}		
	}
	
	function actedit_old() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration;
		$e_to_outbonm	= $this->input->post('e_to_outbonm')?$this->input->post('e_to_outbonm'):$this->input->get_post('e_to_outbonm');
		$i_outbonm_code	= $this->input->post('i_outbonm');
		$i_outbonm_hidden	= $this->input->post('i_outbonm_hidden')?$this->input->post('i_outbonm_hidden'):$this->input->get_post('i_outbonm_hidden');
		$i_outbonm_code_hidden	= $this->input->post('i_outbonm_code_hidden')?$this->input->post('i_outbonm_code_hidden'):$this->input->get_post('i_outbonm_code_hidden');
		$d_outbonm	= $this->input->post('d_outbonm')?$this->input->post('d_outbonm'):$this->input->get_post('d_outbonm');
		$ex_d_outbonm	= explode("/",$d_outbonm,strlen($d_outbonm)); // dd/mm/YYYY
		$doutbonm	= $ex_d_outbonm[2]."-".$ex_d_outbonm[1]."-".$ex_d_outbonm[0];
		
		$i_product	= array();
		$e_product_name	= array();
		$n_count_product	= array();
		$qty_product	= array();
		$f_stp	= array();
		$iso	= array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem'.'_'.$cacah)?$this->input->post('i_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah)?$this->input->post('e_product_name_'.'tblItem'.'_'.$cacah):$this->input->get_post('e_product_name_'.'tblItem'.'_'.$cacah);
			$n_count_product[$cacah]	= $this->input->post('n_count_product_'.'tblItem'.'_'.$cacah)?$this->input->post('n_count_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('n_count_product_'.'tblItem'.'_'.$cacah);
			$qty_product[$cacah]	= $this->input->post('qty_product_'.'tblItem'.'_'.$cacah)?$this->input->post('qty_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('qty_product_'.'tblItem'.'_'.$cacah);
			$f_stp[$cacah]	= $this->input->post('f_stp_'.'tblItem'.'_'.$cacah)?$this->input->post('f_stp_'.'tblItem'.'_'.$cacah):$this->input->get_post('f_stp_'.'tblItem'.'_'.$cacah);
			$iso[$cacah]	= $this->input->post('iso_'.'tblItem'.'_'.$cacah)?$this->input->post('iso_'.'tblItem'.'_'.$cacah):$this->input->get_post('iso_'.'tblItem'.'_'.$cacah);
		}

		if( !empty($e_to_outbonm) && 
		    !empty($i_outbonm_code) && 
		    !empty($d_outbonm) ) {
			if(!empty($i_product_0)) {
				$this->load->model('listbonmkeluar/mclass');
				$this->mclass->mupdate($i_outbonm_code,$e_to_outbonm,$i_outbonm_hidden,$i_outbonm_code_hidden,$doutbonm,$i_product,$e_product_name,$n_count_product,$qty_product,$iterasi,$f_stp,$iso);
			} else {
				print "<script>alert(\"Maaf, item utk Bon M Keluar harus terisi. Terimakasih.\");show(\"listbonmkeluar/cform\",\"#content\");</script>";
			}
		} else {
			print "<script>alert(\"Maaf, Bon M Keluar gagal diupdate!\");show(\"listbonmkeluar/cform\",\"#content\");</script>";
		}		
	}
	
		
	function cari_bonmkeluar() { $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$nobonmkeluar	= $this->input->post('nobonmkeluar');
		$i_outbonm_code_hidden	= $this->input->post('i_outbonm_code_hidden');
		
		$this->load->model('listbonmkeluar/mclass');
		$qbonmkeluar	= $this->mclass->cari_bonmkeluar($nobonmkeluar);
		$num = $qbonmkeluar->num_rows();
		
		if($num>0) {
			$rbonmkeluar = $qbonmkeluar->row();
			
			if(($num>0) && ($i_outbonm_code_hidden!=($rbonmkeluar->i_outbonm_code))){
				echo "Maaf, No. Bon M tdk dpt diubah!";
			}
		}	
	}
	/* End 0f Edit Session */
	
	/* Cancel Session */
	function undo() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ioutbonm	= $this->uri->segment(4);
		$this->load->model('listbonmkeluar/mclass');
		$this->mclass->mbatal($ioutbonm);
	}
	/* End 0f Cancel Session */

	/* Pop Up Bon M Keluar */
	function listbarangjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "BON M KELUAR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbonmkeluar/mclass');

		$query	= $this->mclass->lbonmkeluar();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbonmkeluar/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbonmkeluarperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listbonmkeluar/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "BON M KELUAR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbonmkeluar/mclass');

		$query	= $this->mclass->lbonmkeluar();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbonmkeluar/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbonmkeluarperpages($pagination['per_page'],$pagination['cur_page']);

		$this->load->view('listbonmkeluar/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "BON M KELUAR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbonmkeluar/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->flbonmkeluar($key);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
					'01'=>'Januari', 
					'02'=>'Februari', 
					'03'=>'Maret', 
					'04'=>'April', 
					'05'=>'Mei', 
					'06'=>'Juni', 
					'07'=>'Juli', 
					'08'=>'Agustus', 
					'09'=>'September', 
					'10'=>'Oktober', 
					'11'=>'Nopember', 
					'12'=>'Desember' );
							
			$cc	= 1; 			
			foreach($query->result() as $row){
					$tgl			= (!empty($row->doutbonm) || strlen($row->doutbonm)!=0)?@explode("-",$row->doutbonm,strlen($row->doutbonm)):"";
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
								
				$list .= "
				 <tr>
				  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->ioutbonmcode')\">".$row->ioutbonmcode."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->ioutbonmcode')\">".$tanggal[$cc]."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}		
	/* End 0f Pop Up Bon M Keluar */

	function listbarangjadi2() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iterasi	= $this->uri->segment(4,0);
		$dbonmkeluar= $this->uri->segment(5,0);

		$expdbonmkeluar	= explode("-",$dbonmkeluar,strlen($dbonmkeluar));
		$bulan	= $expdbonmkeluar[1];
		$tahun	= $expdbonmkeluar[0];
				
		$data['dbonmkeluar']= $dbonmkeluar;
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('listbonmkeluar/mclass');

		$query	= $this->mclass->lbarangjadi2($bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->lbarangjadi2opsi($bulan,$tahun);
			$jml	= $query->num_rows();			
		}
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbonmkeluar/cform/listbarangjadinext2/'.$iterasi.'/'.$dbonmkeluar.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages2($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpages2opsi($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		
		$this->load->view('listbonmkeluar/vlistformbrgjadi2',$data);
	}

	function listbarangjadinext2(){
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$iterasi	= $this->uri->segment(4,0);
		$dbonmkeluar= $this->uri->segment(5,0);

		$expdbonmkeluar	= explode("-",$dbonmkeluar,strlen($dbonmkeluar));
		$bulan	= $expdbonmkeluar[1];
		$tahun	= $expdbonmkeluar[0];
				
		$data['dbonmkeluar']= $dbonmkeluar;
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('listbonmkeluar/mclass');
		
		$query	= $this->mclass->lbarangjadi2($bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->lbarangjadi2opsi($bulan,$tahun);
			$jml	= $query->num_rows();
		}
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbonmkeluar/cform/listbarangjadinext2/'.$iterasi.'/'.$dbonmkeluar.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages2($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpages2opsi($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		
		$this->load->view('listbonmkeluar/vlistformbrgjadi2',$data);
	}	

	function flistbarangjadi2() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$db2=$this->load->database('db_external', TRUE);
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$dbonmkeluar	= $this->input->post('dbonmkeluar')?$this->input->post('dbonmkeluar'):$this->input->get_post('dbonmkeluar');
		
		$expdbonmkeluar	= explode("-",$dbonmkeluar,strlen($dbonmkeluar));
		$bulan	= $expdbonmkeluar[1];
		$tahun	= $expdbonmkeluar[0];
		
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('listbonmkeluar/mclass');

		$query	= $this->mclass->flbarangjadi2($key,$bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0) {
			$query	= $this->mclass->flbarangjadi2opsi($key,$bulan,$tahun);
			$jml	= $query->num_rows();			
		}
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				//--------------08-01-2014 23-10-2014 ----------------------------
				// ambil data warna dari tr_product_color
				$sqlxx	= $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b 
										 WHERE a.i_color=b.i_color AND a.i_product_motif = '$row->imotif' ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0){
					$hasilxx = $sqlxx->result();
					
					foreach ($hasilxx as $rownya) {
						$listwarna.= $rownya->e_color_name."<br>";
					}
				}
				
				//-----------------------------------------------------
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->qty','$row->stp','$row->iso')\">".$row->imotif."</a></td>
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->qty','$row->stp','$row->iso')\">".$row->motifname."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->qty','$row->stp','$row->iso')\">".$row->qty."</a></td>	
				  <td>".$listwarna."</td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;		
	}	
	
	// 08-01-2014 23-10-2014
	function additemwarna(){
 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$db2=$this->load->database('db_external', TRUE);
		$iproductmotif 	= $this->input->post('iproductmotif', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
									WHERE a.i_color = b.i_color AND a.i_product_motif = '".$iproductmotif."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'i_product_color'=> $rowxx->i_product_color,
										'i_color'=> $rowxx->i_color,
										'e_color_name'=> $rowxx->e_color_name
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['iproductmotif'] = $iproductmotif;
		$data['posisi'] = $posisi;
		$this->load->view('listbonmkeluar/vlistwarna', $data); 
		//print_r($detailwarna); die();
		//echo $iproductmotif; die();
		return true;
  }
}
?>
