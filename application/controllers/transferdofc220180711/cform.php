<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		/*if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0 ) */
		//{
			$data['page_title_transferdo2']		= $this->lang->line('page_title_transferdo2').' FORECAST';
			$data['page_title_detail_transferdo2']		= $this->lang->line('page_title_detail_transferdo2').' FORECAST';
			$data['form_kode_do_transferdo2']	= $this->lang->line('form_kode_do_transferdo2');
			$data['form_tgl_do_transferdo2']	= $this->lang->line('form_tgl_do_transferdo2');
			$data['form_kode_op_transferdo2']	= $this->lang->line('form_kode_op_transferdo2');
			$data['form_tgl_op_transferdo2']	= $this->lang->line('form_tgl_op_transferdo2');
			$data['form_kodebrg_transferdo2']	= $this->lang->line('form_kodebrg_transferdo2');
			$data['form_nmbrg_transferdo2']		= $this->lang->line('form_nmbrg_transferdo2');
			$data['form_jmldo_transfer2']		= $this->lang->line('form_jmldo_transfer2');
			$data['form_note_transfer2']		= $this->lang->line('form_note_transfer2');
			$data['form_area_transfer2']		= $this->lang->line('form_area_transfer2');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']			= "";
			$data['list']			= "";
			$data['limages']		= base_url();
			
			$this->load->model('transferdofc2/mclass');
			$data['query']	= $this->mclass->tampilkandata();
			$data['isi']	= 'transferdofc2/vform';	
			$this->load->view('template',$data);		
			
		//}
	}
	
	function simpan() {

		$iterasi	= $this->input->post('iteration');
		
		$i_do_code	= array();
		$d_do	= array();
		$i_op_code	= array();
		$d_op	= array();
		$i_customer	= array();
		$i_area	= array();
		$i_product 			= array();
		$e_product_name 	= array();	
		$n_jmldo	= array();
		$i_do	= array();
		$v_do_gross	= array();
		$e_note	= array();
		$fck	= array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem_'.'0');

		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_do_code[$cacah] = $this->input->post('i_do_code_'.'tblItem_'.$cacah);
			$d_do[$cacah] = $this->input->post('d_do_'.'tblItem_'.$cacah);
			$i_op_code[$cacah] = $this->input->post('i_op_code_'.'tblItem_'.$cacah);
			$d_op[$cacah] = $this->input->post('d_op_'.'tblItem_'.$cacah);
			$i_customer[$cacah] = $this->input->post('i_customer_'.'tblItem_'.$cacah);
			$i_area[$cacah] = $this->input->post('i_area_'.'tblItem_'.$cacah);
			$i_product[$cacah] = $this->input->post('i_product_'.'tblItem_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem_'.$cacah);
			$n_jmldo[$cacah] = $this->input->post('n_jmldo_'.'tblItem_'.$cacah);
			$i_do[$cacah] = $this->input->post('i_do_'.'tblItem_'.$cacah); // hidden
			$v_do_gross[$cacah] = $this->input->post('v_do_gross_'.'tblItem_'.$cacah); // hidden
			$e_note[$cacah] = $this->input->post('e_note_'.'tblItem_'.$cacah); // hidden
			$fck[$cacah] = $this->input->post('f_ck_tblItem'.$cacah);
		}
		$this->load->model('transferdofc2/mclass');
		
		if(!empty($i_product_0)) {
			$this->mclass->simpan($i_do_code,$d_do,$i_op_code,$d_op,$i_customer,$i_area,$i_product,$e_product_name,$n_jmldo,$i_do,$v_do_gross,$e_note,$fck,$iterasi);
		} else {
			print "<script>alert(\"Maaf, tdk ada Item Barang yg diisi!\");show(\"transferdofc2/cform\",\"#content\");</script>";
		}
	}
	
}
?>
