<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('app-stok-opname-hsl-cutting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$data['isi'] = 'app-stok-opname-hsl-cutting/vmainform';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

	$cek_data = $this->mmaster->cek_so_hasil_cutting($bulan, $tahun); 
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname hasil cutting untuk bulan ".$nama_bln." ".$tahun." belum diinput..!";
		//$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'app-stok-opname-hsl-cutting/vmainform';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname hasil cutting untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
			//$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname-hsl-cutting/vmainform';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_ yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname_hasil_cutting($bulan, $tahun);
	
			$data['jum_total'] = count($data['query']);
			
			$tgl_so = $cek_data['tgl_so'];
			$pisah1 = explode("-", $tgl_so);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_so = $tgl1."-".$bln1."-".$thn1;
			
			$data['tgl_so'] = $tgl_so;
			$data['isi'] = 'app-stok-opname-hsl-cutting/vformview';
			$this->load->view('template',$data);
			
		}
	} // end else

  }
  
  function submit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);
	  
	  // 04-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan < 12) {
		$bulanplus1 = $bulan+1;
		$tahunplus1 = $tahun;
		if ($bulanplus1 < 10)
			$bulanplus1= '0'.$bulanplus1;
	  }
	  else {
		$bulanplus1 = '01';
		$tahunplus1 = $tahun+1;
	  }
	  
	  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $tgl = date("Y-m-d H:i:s"); 
		  	      
	      for ($i=1;$i<=$no;$i++)
		  {
				 $id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
				 $id_warna = $this->input->post('id_warna_'.$i, TRUE);
				 $stok_fisik_warna = $this->input->post('stok_fisik_warna_'.$i, TRUE);
				 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
				 				 				 
				 $idnya = $this->input->post('id_'.$i, TRUE);
			 
				// ============== 03-12-2014 ================================================
				//-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotalstokfisikwarna = 0;
				if (isset($id_warna) && is_array($id_warna)) {
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
						$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
					} // end for
					// ---------------------------------------------------------------------
					$totalxx = $qtytotalstokfisikwarna;
					//06-04-2015
					//$totalxx_saldo_akhir = $qtytotal_saldo_akhir;
				}
								
				// ======== update stoknya! 04-12-2014 =============
			// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelah tgl SO
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar setelah tgl so keatas
			  
			  //1. hitung brg masuk dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a
								INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
								WHERE a.tgl_bonm > '$tgl_so'
								AND b.id_brg_wip = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$masuk_bgs = $hasilrow->jum_masuk;
					
					if ($masuk_bgs == '')
						$masuk_bgs = 0;
				}
				else
					$masuk_bgs = 0;
			  
			  // 2. hitung brg masuk retur dari tm_sjmasukbhnbakupic
			  $query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
								INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
								WHERE  a.tgl_sj > '$tgl_so'
								AND b.id_brg_wip = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$masuk_retur = $hasilrow->jum_masuk;
					
					if ($masuk_retur == '')
						$masuk_retur = 0;
				}
				else
					$masuk_retur = 0;
			  
			  $jum_masuk = $masuk_bgs+$masuk_retur;
			  // ========================= END MASUK =========================================================
			  
			  // 3. hitung brg keluar ke unit jahit
			  $query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm > '$tgl_so'
									AND b.id_brg_wip = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$keluar = $hasilrow->jum_keluar;
					
					if ($keluar == '')
						$keluar = 0;
				}
				else
					$keluar = 0;
			  
			  $jum_keluar = $keluar;
			  // ========================= END KELUAR =========================================================
			  $jum_stok_akhir = $jum_masuk - $jum_keluar;
			  $totalxx = $totalxx + $jum_stok_akhir;
			  
			  //cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
			$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$id_brg_wip' ");
			if ($query3->num_rows() == 0){
				$data_stok = array(
					'id_brg_wip'=>$id_brg_wip,
					'stok'=>$totalxx,
					'tgl_update_stok'=>$tgl
					);
				$this->db->insert('tm_stok_hasil_cutting', $data_stok);
						
				// ambil id_stok utk dipake di stok warna
				$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
				if($sqlxx->num_rows() > 0) {
					$hasilxx	= $sqlxx->row();
					$id_stok	= $hasilxx->id;
				}else{
					$id_stok	= 1;
				}
			}
			else {
				$hasilrow = $query3->row();
				$id_stok	= $hasilrow->id;
				
				// $totalxx
				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$totalxx', tgl_update_stok = '$tgl' 
				where id_brg_wip= '$id_brg_wip' ");
			}
			
				// A. stok_hasil_cutting_warna
				// ----------------------------------------------
				if (isset($id_warna) && is_array($id_warna)) {
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);						
						$totalxx = $stok_fisik_warna[$xx];
						
						//------------------------------------------------------------------------------
						// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
						// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar tgl setelah pencatatan SO
						
						//1. hitung brg masuk dari tm_bonmmasukcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmmasukcutting a 
										INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
										INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
										WHERE a.tgl_bonm > '$tgl_so'
										AND b.id_brg_wip = '$id_brg_wip'
										AND c.id_warna = '".$id_warna[$xx]."'
										 ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
					  
					  // 2. hitung brg masuk retur dari tm_sjmasukbhnbakupic
					  $query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
										INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
										INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
										WHERE a.tgl_sj > '$tgl_so'
										AND b.id_brg_wip = '$id_brg_wip'
										AND c.id_warna = '".$id_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_retur = $hasilrow->jum_masuk;
							
							if ($masuk_retur == '')
								$masuk_retur = 0;
						}
						else
							$masuk_retur = 0;
						
						$jum_masuk = $masuk_bgs+$masuk_retur;
						// ========================= END MASUK warna =========================================================
						
						// 3. hitung brg keluar ke unit jahit
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
											INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
											INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
											WHERE a.tgl_bonm > '$tgl_so'
											AND b.id_brg_wip = '$id_brg_wip'
											AND c.id_warna = '".$id_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar = $hasilrow->jum_keluar;
							
							if ($keluar == '')
								$keluar = 0;
						}
						else
							$keluar = 0;
					  
						$jum_keluar = $keluar;
						// ========================= END KELUAR =========================================================
						$jum_stok_akhir = $jum_masuk - $jum_keluar;
						$totalxx = $totalxx + $jum_stok_akhir;						
						// ------------ end hitung transaksi keluar/masuk ------------------------------
						
						// ========================= stok per warna ===============================================
						//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE id_warna = '".$id_warna[$xx]."'
								AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() == 0){
							// jika blm ada data di tm_stok_hasil_cutting_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							//26-06-2014, pake $totalxx dari hasil perhitungan transaksi keluar/masuk
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_cutting'=>$id_stok,
								'id_warna'=>$id_warna[$xx],
								'stok'=>$totalxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
						}
						else {
							// $totalxx
							$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
							where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
						}
					} // end for warna
				}
				// ----------------------------------------------
			  
				$this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail SET status_approve = 't'
								where id = '$idnya' ");
		  } // end for
		  
		  $this->db->query(" UPDATE tt_stok_opname_hasil_cutting SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  
		  $data['msg'] = "Data stok opname hasil cutting untuk bulan ".$nama_bln." ".$tahun." sudah berhasil di-approve, dan otomatis mengupdate stok";
		  $data['isi'] = 'app-stok-opname-hsl-cutting/vmainform';
		  $this->load->view('template',$data);
		  
		  //redirect('app-stok-opname-hsl-cutting/cform');
  }

}
