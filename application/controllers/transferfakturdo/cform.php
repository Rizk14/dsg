<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_transferdo2']		= $this->lang->line('page_title_transferdo2');
			$data['page_title_detail_transferdo2']		= $this->lang->line('page_title_detail_transferdo2');
			$data['form_kode_do_transferdo2']	= $this->lang->line('form_kode_do_transferdo2');
			$data['form_tgl_do_transferdo2']	= $this->lang->line('form_tgl_do_transferdo2');
			$data['form_kode_op_transferdo2']	= $this->lang->line('form_kode_op_transferdo2');
			$data['form_tgl_op_transferdo2']	= $this->lang->line('form_tgl_op_transferdo2');
			$data['form_kodebrg_transferdo2']	= $this->lang->line('form_kodebrg_transferdo2');
			$data['form_nmbrg_transferdo2']		= $this->lang->line('form_nmbrg_transferdo2');
			$data['form_jmldo_transfer2']		= $this->lang->line('form_jmldo_transfer2');
			$data['form_note_transfer2']		= $this->lang->line('form_note_transfer2');
			$data['form_area_transfer2']		= $this->lang->line('form_area_transfer2');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']			= "";
			$data['list']			= "";
			$data['limages']		= base_url();
			
			$this->load->model('transferfakturdo/mclass');
			$data['isi']	= 'transferfakturdo/vmainform';
			$data['query']	= $this->mclass->tampilkandata();
			$this->load->view('template',$data);
		
	}
	
	function simpan() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->input->post('iteration');
		
		$i_faktur	= array();
		$i_faktur_item	= array();
		$i_code	= array();
		$i_faktur_code	= array();
		$d_faktur	= array();
		$n_discount	= array();
		$v_discount	= array();
		$v_total_faktur	= array();
		$v_total_fppn	= array();
		$i_do_code	= array();
		$d_do	= array();
		//$i_customer	= array();
		$i_product 			= array();
		$e_product_name 	= array();	
		$n_quantity	= array();
		$v_unit_price	= array();
		$fck	= array();
		$i_customer = 1;
		
		//$i_product_0	= $this->input->post('i_product_'.'tblItem_'.'0');
		
		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_code[$cacah] = $this->input->post('i_code_'.'tblItem_'.$cacah); // cabang
			$i_faktur[$cacah] = $this->input->post('i_faktur_'.'tblItem_'.$cacah);
			$i_faktur_item[$cacah] = $this->input->post('i_faktur_item_'.'tblItem_'.$cacah);
			$i_faktur_code[$cacah] = $this->input->post('i_faktur_code_'.'tblItem_'.$cacah);
			$d_faktur[$cacah] = $this->input->post('d_faktur_'.'tblItem_'.$cacah);
			$n_discount[$cacah] = $this->input->post('n_discount_'.'tblItem_'.$cacah);
			$v_discount[$cacah] = $this->input->post('v_discount_'.'tblItem_'.$cacah);
			$v_total_faktur[$cacah] = $this->input->post('v_total_faktur_'.'tblItem_'.$cacah);
			$v_total_fppn[$cacah] = $this->input->post('v_total_fppn_'.'tblItem_'.$cacah);
			$i_do_code[$cacah] = $this->input->post('i_do_code_'.'tblItem_'.$cacah);
			$d_do[$cacah] = $this->input->post('d_do_'.'tblItem_'.$cacah);
			
			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem_'.$cacah);
			$n_quantity[$cacah] = $this->input->post('n_quantity_'.'tblItem_'.$cacah);
			$v_unit_price[$cacah] = $this->input->post('v_unit_price_'.'tblItem_'.$cacah); 
			$fck[$cacah] = $this->input->post('f_ck_tblItem'.$cacah);
		}
		
		$this->load->model('transferfakturdo/mclass');
		
		//if(!empty($i_product_0)) {
			$this->mclass->simpan($i_customer,$i_code,$i_faktur, $i_faktur_item, $i_faktur_code,$d_faktur, $n_discount, $v_discount, 
								$v_total_faktur,$v_total_fppn,
								$i_do_code, $d_do,$i_product,$e_product_name, $n_quantity,$v_unit_price, $fck,$iterasi);
		/*} else {
			print "<script>alert(\"Maaf, tdk ada Item Barang yg dipilih!\");show(\"transferfakturdo/cform\",\"#content\");</script>";
		} */
	}
	
}
?>
