<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-bb/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================

	$id_op = $this->input->post('id_op', TRUE);  
	$no_op = $this->input->post('no_op', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	$id_op_detail = $this->input->post('id_brg', TRUE);  
	$list_brg = explode(";", $id_op_detail);
	
	if ($proses_submit == "Proses") {
		if ($id_op !='') {
			$query2	= $this->db->query(" SELECT id_pp, kode_supplier FROM tm_op WHERE id = '$id_op' ");
			$hasilrow = $query2->row();
			$id_pp	= $hasilrow->id_pp;
			$kode_supplier = $hasilrow->kode_supplier;
			$data['kode_supplier']	= $hasilrow->kode_supplier;
			
			$data['op_detail'] = $this->mmaster->get_detail_op($id_op, $list_brg, $kode_supplier);
			$data['msg'] = '';
			$data['id_op'] = $id_op;
			$data['no_op'] = $no_op;
			$data['supplier'] = $this->mmaster->get_supplier();
			
			
			$query2	= $this->db->query(" SELECT no_pp FROM tm_pp WHERE id = '$id_pp' ");
			$hasilrow = $query2->row();
			$no_pp	= $hasilrow->no_pp;
			$data['no_pp'] = $no_pp;
		}
		else {
			$data['msg'] = 'OP harus dipilih';
			$data['id_op'] = '';
			$data['no_op'] = '';
			$data['no_pp'] = '';
		}
		$data['go_proses'] = '1';
	}
	else {
		$data['msg'] = '';
		$data['id_op'] = '';
		$data['go_proses'] = '';
	}
	$data['isi'] = 'faktur-bb/vmainform';
	$this->load->view('template',$data);

  }
  
  function edit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================

	$id_pembelian 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_pembelian($id_pembelian);
	$data['supplier'] = $this->mmaster->get_supplier();
	$data['msg'] = '';

	$data['isi'] = 'faktur-bb/veditform';
	$this->load->view('template',$data);

  }
  
  function updatedata() {
			$id_pembelian 	= $this->input->post('id_pembelian', TRUE);
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			$no_fb 	= $this->input->post('no_sj', TRUE);
			$tgl_fb 	= $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_fb);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fb = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$kode_supplier = $this->input->post('hide_supplier', TRUE);  
			$gtotal = $this->input->post('gtotal', TRUE);  
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			
			$tgl = date("Y-m-d");
		/*	if ($sisa_hutang == 0) {
				$status_lunas = 1;
			}
			else {
				$status_lunas = 0;
			} */
			
			//----------				
				//data gudang, sementara dikosongin dl. nantinya sih pengennya ada pemilihan tempat gudangnya
			/*	if ($jenis_brg == '1') {
					$nama_gudang = "Gudang Bahan Baku";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_bahan_baku";
				}
				else if ($jenis_brg == '2') {
					$nama_gudang = "Gudang Accessories";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_asesoris";
				}
				else if ($jenis_brg == '3') {
					$nama_gudang = "Gudang Bahan Pendukung";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_bahan_pendukung";
				}
				else
					$nama_tabel = "tm_perlengkapan";
				
				if ($jenis_brg != '4') {
					$query3	= $this->db->query(" SELECT id FROM tm_gudang WHERE nama = '$nama_gudang' AND kode_lokasi = '$lokasi'");
					$hasilrow = $query3->row();
					$id_gudang	= $hasilrow->id;
				}
				else */
					$id_gudang = 0;
					$lokasi = "01"; // duta
			
			//update headernya
			$this->db->query(" UPDATE tm_pembelian SET no_sj = '$no_fb', 
								tgl_sj = '$tgl_fb', total = '$gtotal',
								jenis_pembelian = '$jenis_pembelian',
								uang_muka = '$uang_muka',
								sisa_hutang = '$sisa_hutang', keterangan = '$ket',
								tgl_update = '$tgl', 
								pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak',
								total_pajak = '$total_pajak', dpp = '$dpp', status_stok = 'f' where id= '$id_pembelian' ");
								
					// balikin lagi stoknya, trus delete di tabel tm_apply_stok_pembelian
							//1. ambil no_sj dan kode_supplier
							$query4	= $this->db->query(" SELECT no_sj, kode_supplier from tm_pembelian where id = '$id_pembelian' ");
							$hasilrow = $query4->row();
							$no_sj	= $hasilrow->no_sj;
							$kode_supplier	= $hasilrow->kode_supplier;
							
							//2. ambil id tm_apply_stok_pembelian (25-04-11: bon M tdk boleh dihapus!)
							
							$query4	= $this->db->query(" SELECT * from tm_apply_stok_pembelian where no_sj = '$no_sj' 
							AND kode_supplier = '$kode_supplier' ");
							if ($query4->num_rows() > 0){
								$hasil4=$query4->result();
								foreach ($hasil4 as $row4) {
									// ambil id detail.......
									$query5	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail 
									where id_apply_stok = '$row4->id' ");
									if ($query5->num_rows() > 0){
										$hasil5=$query5->result();
										foreach ($hasil5 as $row5) {
											// edit transaksi stoknya
											//1. cek stok brg berdasarkan no_bukti di tt_stok
										/*	$sql = "SELECT id, masuk FROM tt_stok 
											WHERE no_bukti = '$row4->no_bonm' AND kode_brg='$row5->kode_brg' 
											ORDER BY id DESC LIMIT 1"; die($sql); */
											
											$status_stok_header = $row4->status_stok;
											$status_stok_detail = $row5->status_stok;
											
											if ($status_stok_header == 't' || $status_stok_detail == 't' ) {
												$query3	= $this->db->query(" SELECT id, masuk FROM tt_stok 
												WHERE no_bukti = '$row4->no_bonm' AND kode_brg='$row5->kode_brg' 
												ORDER BY id DESC LIMIT 1 ");
												if ($query3->num_rows() > 0){
													$hasilrow = $query3->row();
													$masuk	= $hasilrow->masuk;
												}
												else
													$masuk = 0;
												
												//2. ambil stok terkini di tm_stok
												$query3	= $this->db->query(" SELECT stok FROM tm_stok 
																WHERE kode_brg='$row5->kode_brg' ");
												$hasilrow = $query3->row();
												$stok_lama	= $hasilrow->stok;
												$stokreset = $stok_lama-$masuk;
												
												if ($masuk > 0) {
													//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
													$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga)
													VALUES ('$row5->kode_brg', '$row4->no_bonm', '$masuk', '$stokreset', '$tgl', '$row5->harga' ) ");
																								
													//4. update stok di tm_stok
													$this->db->query(" UPDATE tm_stok SET stok = '$stokreset', tgl_update_stok = '$tgl'
																		where kode_brg= '$row5->kode_brg'  ");
												}
											} // end if status_stoknya = 't'
										} // end foreach
										
									} // end if query5
									// 3. hapus detailnya
									$this->db->query(" DELETE FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row4->id' ");
								}
							}
							
							//4. hapus tm_apply_stok_pembelian
							$this->db->query(" DELETE FROM tm_apply_stok_pembelian WHERE no_sj = '$no_sj' AND kode_supplier = '$kode_supplier' ");
			

					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
					   if (($this->input->post('harga_'.$i, TRUE) != $this->input->post('harga_lama_'.$i, TRUE)) || 
					   ($this->input->post('qty_'.$i, TRUE) != $this->input->post('qty_lama_'.$i, TRUE)) ) {
						   
						   // ambil id_op
							$query3	= $this->db->query(" SELECT id_op FROM tm_pembelian WHERE id = '".$id_pembelian."' ");
							$hasilrow = $query3->row();
							$id_op = $hasilrow->id_op;
							
							// ambil id_op_detail
							$query3	= $this->db->query(" SELECT id FROM tm_op_detail WHERE id_op = '".$id_op."'
										AND kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
							$hasilrow = $query3->row();
							$id_detail = $hasilrow->id;
						   
						   // cek qtynya di OP ============================================
						   // cek sum qty di pembelian. jika sum di pembelian = qty OP, maka update status OPnya menjadi true
							$query3	= $this->db->query(" SELECT a.qty FROM tm_op_detail a, tm_op b 
										WHERE b.id = '$id_op' AND a.id_op = b.id AND a.kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
							$hasilrow = $query3->row();
							$qty_op = $hasilrow->qty; // ini qty di OP detail berdasarkan kode brgnya
							//echo $qty_op."<br>"; 
							
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b 
										WHERE a.id_pembelian = b.id AND b.id_op = '$id_op' AND a.kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' "); //
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
							//echo $jum_beli."<br>"; 
						   
							$jumtot = $jum_beli - $this->input->post('qty_lama_'.$i, TRUE) + $this->input->post('qty_'.$i, TRUE);
							//echo $jumtot."<br>"; 
							//$tes = $jum_beli-$this->input->post('qty_lama_'.$i, TRUE)+$this->input->post('qty_'.$i, TRUE);
							//echo $tes; die();
							
							if ($jumtot > $qty_op)
								$qty = $qty_op-($jum_beli - $this->input->post('qty_lama_'.$i, TRUE));
							else
								$qty = $this->input->post('qty_'.$i, TRUE);
						   
						   // =============================================================
						   
							$this->db->query(" UPDATE tm_pembelian_detail SET qty = '".$qty."', 
								harga = '".$this->input->post('harga_'.$i, TRUE)."', diskon = '".$this->input->post('diskon_'.$i, TRUE)."',
								pajak = '".$this->input->post('pajak_'.$i, TRUE)."', total = '".$this->input->post('total_'.$i, TRUE)."',
								status_stok = 'f'
								where id= '".$this->input->post('id_detail_'.$i, TRUE)."' ");
						
							// cek status_op =======================================
							if ($qty_op == ($jum_beli-$this->input->post('qty_lama_'.$i, TRUE)+$this->input->post('qty_'.$i, TRUE) )) {
								
								$this->db->query(" UPDATE tm_op_detail SET status_op = 't' 
								where id= '$id_detail' ");
								
								//cek di tabel tm_op_detail, apakah status_faktur sudah 't' semua?
								$this->db->select("id from tm_op_detail WHERE status_op = 'f' 
											AND id_op = '$id_op' ", false);
								$query = $this->db->get();
								//jika sudah t semua, maka update tabel tm_pp di field status_op menjadi t
								if ($query->num_rows() == 0){
									$this->db->query(" UPDATE tm_op SET status_op = 't' 
									where id= '$id_op' ");
								}
							} // end if
							else { // jika tidak sama, maka statusnya false
								$this->db->query(" UPDATE tm_op_detail SET status_op = 'f' 
								where id= '$id_detail' ");
								$this->db->query(" UPDATE tm_op SET status_op = 'f' 
									where id= '$id_op' ");
							}
							// =========================================================
						
						//-----------------------------
						
						if ($this->input->post('harga_'.$i, TRUE) != $this->input->post('harga_lama_'.$i, TRUE)) {
							$data_harga = array(
								'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
								'kode_supplier'=>$kode_supplier,
								'harga'=>$this->input->post('harga_'.$i, TRUE),
								'tgl_input'=>$tgl
							);
							$this->db->insert('tt_harga', $data_harga);
						}
					
						// ============= WARNING...!! EDIT STOK BUKAN DISINI NANTINYA, TAPI DI GUDANG / Bon M ==================
							// edit transaksi stoknya
							//1. cek stok brg berdasarkan no_bukti di tt_stok
					/*		$query3	= $this->db->query(" SELECT id, masuk FROM tt_stok WHERE no_bukti = '$no_fb' 
											AND kode_brg='".$this->input->post('kode_'.$i, TRUE)."' ORDER BY id DESC LIMIT 1 ");
							$hasilrow = $query3->row();
							$masuk	= $hasilrow->masuk;
							
							//2. ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok 
											WHERE kode_brg='".$this->input->post('kode_'.$i, TRUE)."' ");
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$qty_baru = $this->input->post('qty_'.$i, TRUE);
							$stokreset = $stok_lama-$masuk;
							$stokskrg = ($stok_lama-$masuk) + $qty_baru;
							
							//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
							$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga)
							VALUES ('".$this->input->post('kode_'.$i, TRUE)."', '$no_fb', '$masuk', '$stokreset', '$tgl', '".$this->input->post('harga_lama_'.$i, TRUE)."' ) ");
							
							//4. insert ke tabel tt_stok dgn tipe masuk, utk mengupdate data stoknya
							$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga)
							VALUES ('".$this->input->post('kode_'.$i, TRUE)."', '$no_fb', '$qty_baru', '$stokskrg', '$tgl', '".$this->input->post('harga_'.$i, TRUE)."' ) ");
							
							//5. update stok di tm_stok
							$this->db->query(" UPDATE tm_stok SET stok = '$stokskrg', tgl_update_stok = '$tgl'
												where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."'  "); */
						// ====================================================================================================
							
							//update harga di tabel harga_brg_supplier
							if ($this->input->post('harga_'.$i, TRUE) != $this->input->post('harga_lama_'.$i, TRUE)) {
								$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '".$this->input->post('harga_'.$i, TRUE)."', tgl_update='$tgl'
									where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."' AND kode_supplier = '$kode_supplier' ");
							}
						// ----------------------------------------------------
							
								//echo $this->input->post('qty_'.$i, TRUE)."<br>";
							// 5. create bon M lagi
							$this->mmaster->create_bonm($no_sj, $kode_supplier,  
								$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE) );
													
						//--------------------------------------------------------------
					  } // end if
					} // end for
					redirect('faktur-bb/cform/view');
				
  }
  
  // updatefjual
  function updatefjual() {
			$id_pembelian 	= $this->input->post('id_pembelian', TRUE);
			$no_fb 	= $this->input->post('no_faktur', TRUE);
			$tgl_fb 	= $this->input->post('tgl_faktur', TRUE);  
			$pisah1 = explode("-", $tgl_fb);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fb = $thn1."-".$bln1."-".$tgl1;
									
			$tgl = date("Y-m-d");			
			
			//update headernya
			$this->db->query(" UPDATE tm_pembelian SET no_faktur = '$no_fb', 
								tgl_faktur = '$tgl_fb' where id= '$id_pembelian' ");
			
					redirect('faktur-bb/cform/view2');
				
  }
  
/*  function show_popup_brg($keywordcari){
	// =======================
	// disini coding utk pengecekan user login
	//$departemen = ambil infonya dari tabel user
//========================

	$posisi 	= $this->uri->segment(4);
	// if $departemen == 'Bahan Baku' { } else { get_asesoris}
	//$data['bahan_baku'] = $this->mmaster->get_bahan_baku($num, $offset, $keywordcari);
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jum_total = $this->mmaster->get_bahan_bakutanpalimit($keywordcari);
							$config['base_url'] = base_url()."index.php/faktur-bb/cform/show_popup_brg/".$posisi."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_bahan_baku($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total); //echo count($jum_total); die();
	$data['posisi'] = $posisi;

	$this->load->view('faktur-bb/vpopupbrg',$data);

  } */
  
  function show_popup_op(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier 	= $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
	$jum_total = $this->mmaster->get_optanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url()."index.php/faktur-bb/cform/show_popup_op/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_op($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('faktur-bb/vpopupop',$data);

  }

  function submit(){
	$this->load->library('form_validation');
		//$goedit 	= $this->input->post('goedit', TRUE);

			$this->form_validation->set_rules('no_sj', 'Nomor SJ', 'required');
			$this->form_validation->set_rules('tgl_sj', 'Tanggal SJ', 'required');

		if ($this->form_validation->run() == FALSE)
		{
		/*	$data['isi'] = 'faktur-bb/vmainform';
			$data['msg'] = 'Field2 nomor faktur, tanggal faktur tidak boleh kosong..!';
			$this->load->view('template',$data); */
		}
		else
		{
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj = $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			$no 	= $this->input->post('no', TRUE);
			$id_op 	= $this->input->post('id_op', TRUE);
			$kode_supplier = $this->input->post('kode_sup', TRUE);  
			$gtotal = $this->input->post('gtotal', TRUE);  
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			$faktur_sj = $this->input->post('faktur_sj', TRUE);
			
		//	if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($no_sj, $kode_supplier);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'faktur-bb/vmainform';
					$data['msg'] = "Data no SJ ".$no_sj." sudah ada..!";
					//$data['msg'] = '';
					$data['id_pp'] = '';
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
					// inspired from proIT inventory
						$this->mmaster->save($jenis_pembelian, $no_sj,$tgl_sj,$id_op,$faktur_sj, $kode_supplier,$gtotal,$total_pajak, $dpp, 
						$uang_muka, $sisa_hutang,$ket,$hide_pkp, $hide_tipe_pajak, $this->input->post('id_op_detail_'.$i, TRUE), 
								$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE), 
									$this->input->post('harga_lama_'.$i, TRUE), 
									$this->input->post('pajak_'.$i, TRUE), $this->input->post('diskon_'.$i, TRUE), 
									$this->input->post('total_'.$i, TRUE) );
									
						$this->mmaster->create_bonm($no_sj, $kode_supplier,  
								$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE) );
					}
					redirect('faktur-bb/cform/view');
				}
		//	} // end if goedit == ''
			
			
		}
  }
  
  function view(){
    $data['isi'] = 'faktur-bb/vformview';
    $keywordcari = "all";
    $csupplier = '0';
	$kode_bagian = ''; // ini nanti ambil dari login user
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-bb/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-bb/cform/cari/'.$csupplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'faktur-bb/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $list_brg 	= $this->uri->segment(5);
    $this->mmaster->delete($kode, $list_brg);
    redirect('faktur-bb/cform/view');
  }
  
  function get_pkp_tipe_pajak() {
		$kode_sup 	= $this->uri->segment(4);
		$rows = array();
		if(isset($kode_sup)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			$rows = $this->mmaster->get_pkp_tipe_pajak_bykodesup($kode_sup);
			
			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);

  }
  
  function get_qty_op_detail() {		
		$id_op	= $this->input->post('id_op');
		$kode_brg	= $this->input->post('kode_brg');
		$qty_brg	= $this->input->post('qty');

		// ambil qty op_detail
		$query3	= $this->db->query(" SELECT qty FROM tm_op_detail WHERE id_op = '".$id_op."'
		AND kode_brg = '$kode_brg' ");
		
		if($query3->num_rows()>0) {
			$hasilrow = $query3->row();
			$qty_op = $hasilrow->qty;
			//echo $qtynya;
		}
		else
			$qtynya = 0;
		
		// ambil sum qty pembelian detail
			$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b 
							WHERE a.id_pembelian = b.id AND b.id_op = '$id_op' AND a.kode_brg = '".$kode_brg."' ");
		
		if($query3->num_rows()>0) {
			$hasilrow = $query3->row();
			$jum_beli = $hasilrow->jum;
		}
		else
			$jumnya = 0;

		$qty_skrg = $jum_beli + $qty_brg;
		if ($qty_skrg > $qty_op)
			echo "1";
		else
			echo "0";

		return true;
  }
  
}
