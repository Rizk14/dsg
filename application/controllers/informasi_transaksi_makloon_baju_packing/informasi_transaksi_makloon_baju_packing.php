<?php
class informasi_transaksi_makloon_baju_packing extends CI_Controller
{
    public $data = array(
        'halaman' => 'informasi_transaksi_makloon_baju_packing',        
        'title' => 'Informasi Transaksi Makloon Baju Packing',
        'isi' => 'informasi_transaksi_makloon_baju_packing/informasi_transaksi_makloon_baju_packing_form'
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('informasi_transaksi_makloon_baju_packing/informasi_transaksi_makloon_baju_packing_model', 'informasi_transaksi_makloon_baju_packing');
    }

   
    public function index()
    {
		$no=$this->input->post('no');
		
        $this->data['values'] = (object) $this->informasi_transaksi_makloon_baju_packing->default_values;
        $this->data['unit_packing'] =  $this->informasi_transaksi_makloon_baju_packing->get_unit_packing();
		$this->load->view('template', $this->data);
			
    }
    
 public function view($offset=null)
    {
		$no=$this->input->post('no');
		$tanggal_sj_dari = $this->input->post('tanggal_sj_dari',TRUE);
		$tanggal_sj_ke = $this->input->post('tanggal_sj_ke',TRUE);
		$unit_packing = $this->input->post('unit_packing',TRUE);
		$id_barang_bb = $this->input->post('id_barang_bb_1',TRUE);
		
		$informasi_transaksi_makloon_baju_packing = $this->informasi_transaksi_makloon_baju_packing->get_all_inner_paged($tanggal_sj_dari,$tanggal_sj_ke,$unit_packing,$id_barang_bb);
		
		if ($informasi_transaksi_makloon_baju_packing) {
            $this->data['informasi_transaksi_makloon_baju_packing'] = $informasi_transaksi_makloon_baju_packing;  
        } else {
            $this->data['informasi_transaksi_makloon_baju_packing'] = 'Tidak ada data Makloon Baju Gudang Jadi Wip, Silahkan Melakukan '.anchor('/informasi_transaksi_makloon_baju_packing/informasi_transaksi_makloon_baju_packing/view', 'Pencarian kembali.', 'class="alert-link"');
        }
		$this->data['tanggal_sj_dari'] = $tanggal_sj_dari;
		$this->data['tanggal_sj_ke'] = $tanggal_sj_ke;
		
		$this->data['unit_packing'] = $unit_packing;
		$this->data['isi'] = 'informasi_transaksi_makloon_baju_packing/informasi_transaksi_makloon_baju_packing_list';
		$this->load->view('template', $this->data);		
    }
     
}

