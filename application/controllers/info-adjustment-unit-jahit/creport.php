<?php
class Creport extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-adjustment-unit-jahit/mreport');
  }
  
  function viewadjunitjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
    $data['isi'] = 'info-adjustment-unit-jahit/vformviewadjunitjahit';
	$id_unit 	= $this->input->post('id_unit', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);
	$s_approve  = $this->input->post('s_approve', TRUE);
	
	if ($id_unit == '' && $bulan=='' && $tahun=='') {
		$id_unit 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	if ($id_unit == '')
		$id_unit = '0';

    $jum_total = $this->mreport->get_adjwipunittanpalimit($id_unit, $bulan, $tahun, $s_approve);

			$config['base_url'] = base_url().'index.php/info-adjustment-unit-jahit/creport/viewadjunitjahit/'.$id_unit.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mreport->get_adjwipunit($config['per_page'],$this->uri->segment(7), $id_unit, $bulan, $tahun, $s_approve);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['cunit'] = $id_unit;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
   function editadjunitjahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_adj 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cgudang 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cgudang'] = $cgudang;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_adj'] = $id_adj;
	
	$data['query'] = $this->mreport->get_adjunitjahit($id_adj); 
	
	
	//$data['jum_total'] = count($data['query']);
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	
		
	$data['isi'] = 'info-adjustment-unit-jahit/veditadjunitjahit';
	$this->load->view('template',$data);
	//-------------------------
  }
  
   function updateadjunitjahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_adj = $this->input->post('id_adj', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cgudang = $this->input->post('cgudang', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  $tgl_adj	= $this->input->post('tgl_adj', TRUE);
	  $pisah1 = explode("-", $tgl_adj);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_adj = $thn1."-".$bln1."-".$tgl1;
	  
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  $id_unit = $this->input->post('id_unit', TRUE);
	  $tgl = date("Y-m-d H:i:s"); 
	  
		  			 
		 $this->db->query(" UPDATE tt_adjustment_unit_jahit SET tgl_adj = '$tgl_adj', 
							tgl_update = '$tgl'
							where id = '$id_adj' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mreport->saveadjunitjahit($id_adj, $tgl_adj, $jenis_perhitungan_stok, $id_unit,
			 $this->input->post('id_'.$i, TRUE),
			 $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 );
		 }
			  
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "info-adjustment-unit-jahit/creport/viewadjunitjahit/".$cgudang."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
}
