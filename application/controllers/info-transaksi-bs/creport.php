<?php
class Creport extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-transaksi-bs/mreport');
  }
  //~ 
  function index(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mreport->get_gudang();
	$data['isi'] = 'info-transaksi-bs/vformmutasigudang';
	$this->load->view('template',$data);
  }
  
  /*
   function viewmutasigudang(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-transaksi-bs/vviewmutasigudang';
    //~ 
	$date_from = $this->input->post('date_from', TRUE);
	$pisah1 = explode("-", $date_from);
	$tanggal= $pisah1[0];
	$bulan= $pisah1[1];
	$tahun= $pisah1[2];
	$tgldari= $tahun."-".$bulan."-".$tanggal;
	
	$date_to = $this->input->post('date_to', TRUE); 
	$pisah1 = explode("-", $date_to); 
	$tanggal= $pisah1[0];
	$bulan= $pisah1[1];
	$tahun= $pisah1[2];
	$tglke= $tahun."-".$bulan."-".$tanggal;
	$gudang = $this->input->post('gudang', TRUE);  
	
	$data['query'] = $this->mreport->get_mutasi_gudang($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	//~ 
	
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$kode_gudang = "";
		$nama_gudang = "";
	}
	//~ $data['bulan'] = $bulan;
	//~ $data['tahun'] = $tahun;	
	$data['gudang'] = $gudang;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$this->load->view('template',$data);
  }
  */
  function caribrgwip_laptransaksi(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil kode, nama
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = '';
			$nama_brg_wip = '';
		}
		
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['id_brg_wip'] = $id_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('info-transaksi-bs/vinfobrgwip2', $data); 
		return true;
  }
  function transaksi(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mreport->get_gudang();
	$data['isi'] = 'info-transaksi-bs/vformlaptransaksihasilgudang';
	$this->load->view('template',$data);
  }
   function viewtransaksigudang(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	//~ 
   
	$gudang = $this->input->post('gudang', TRUE);
	//$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	//$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	//$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	
	//~ 
	//~ $bulan = $this->input->post('bulan', TRUE);
	//~ $tahun = $this->input->post('tahun', TRUE);
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);
	
	// 12-01-2016
	$jumbrg = $this->input->post('jumbrg', TRUE);  
	$list_id_brg_wip = "";
	for ($i=1; $i<=$jumbrg; $i++) {
		$list_id_brg_wip.= $this->input->post('id_brg_wip_'.$i, TRUE).";";
	}
	
	//$data['query'] = $this->mreport->get_transaksi_gudang($gudang, $bulan, $tahun, $list_id_brg_wip);
	$data['query'] = $this->mreport->get_transaksi_gudang($gudang, $date_from, $date_to, $list_id_brg_wip);

	$data['jum_total'] = count($data['query']);
	
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$kode_gudang = "";
		$nama_gudang = "";
	}
		
	$data['gudang'] = $gudang;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
		
	/*if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember"; */
		
	/*$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip; */
	$data['list_id_brg_wip'] = $list_id_brg_wip;
	$data['jumbrg'] = $jumbrg;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	//$data['bulan'] = $bulan;
	//$data['tahun'] = $tahun;
	//$data['nama_bulan'] = $nama_bln;
	 $data['isi'] = 'info-transaksi-bs/vviewlaptransaksigudang';
	$this->load->view('template',$data);
  }



 function export_excel_mutasi() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$gudang = $id_gudang;#$this->input->post('gudang', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query  = $this->mreport->get_mutasi_gudang($date_from, $date_to, $gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='23' align='center'>LAPORAN MUTASI GUDANG BS</th>
		 </tr>
		 <tr>
			<th colspan='23' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='23' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
	<tr class='judulnya'>
		 <th width='3%' >No</th>
		 <th width='15%'>Kode</th>
		 <th width='25%'>Nama Brg WIP</th>
		 <th width='8%' >Saldo Awal</th>
		 <th width='8%'>Masuk</th>
		 <th width='8%'>Keluar</th>
		 <th width='8%'>Saldo Akhir</th>
		 <th width='8%'>Stok Opname</th>
		
	 </tr>
		</thead>
		<tbody>";
		// 12-09-2014, kolom setelah retur ke unit jahit dihilangkan
		// <th width='8%'>Lainnya</th>
		
			if (is_array($query)) {
				$temp_kodekel = "";
			 for($j=0;$j<count($query);$j++){
				 if ($temp_kodekel != $query[$j]['kode_kel']) {
					 $temp_kodekel = $query[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='23'>&nbsp;<b>".$query[$j]['kode_kel']." - ".$query[$j]['nama_kel']."</b></td>
					</tr>";
				 }



				 $html_data.= "<tr class=\"record\">
				  <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td align='center'> ".$query[$j]['saldo_awal']."</td>
				 <td align='center'> ".$query[$j]['jum_masuk']."</td> 
				 <td align='center'> ".$query[$j]['jum_keluar_pack']."</td>
				 <td align='right'>".$query[$j]['jum_saldo_akhir']."</td>
				 <td align='right'>".$query[$j]['jum_stok_opname']."</td>";

				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_gudang_bs";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }



  function viewmutasigudang(){ // modif 12-11-2015
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
    
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	
	
	$data['query'] = $this->mreport->get_mutasi_gudang($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['isi'] = 'info-transaksi-bs/vviewmutasigudang';
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	
	
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }
  
  }

 


