<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-pajak/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$data['isi'] = 'faktur-pajak/vmainform';
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['msg'] = '';
	$this->load->view('template',$data);

  }
  
  function edit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id_pajak 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$csupplier 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
	
	$data['query'] = $this->mmaster->get_pajak($id_pajak);
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['csupplier'] = $csupplier;
	$data['carinya'] = $carinya;
	$data['isi'] = 'faktur-pajak/veditform';
	$this->load->view('template',$data);

  }
  
  // updatedata
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_pajak 	= $this->input->post('id_pajak', TRUE);
			$faktur_pajak_lama = $this->input->post('no_faktur_pajak_lama', TRUE);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$jum = $this->input->post('jum', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			
			$no_faktur = $this->input->post('no_faktur', TRUE);
			$no_faktur_lama = $this->input->post('no_faktur_lama', TRUE);
			//19-05-2014
			$id_faktur = $this->input->post('id_faktur', TRUE);
			$id_faktur_lama = $this->input->post('id_faktur_lama', TRUE);
			$id_faktur = trim($id_faktur);
			$id_faktur_lama = trim($id_faktur_lama);
			
			$is_quilting = $this->input->post('is_quilting', TRUE);
			$is_quilting_lama = $this->input->post('is_quilting_lama', TRUE);
			$is_quilting = trim($is_quilting);
			$is_quilting_lama = trim($is_quilting_lama);
			
			$supplier = $this->input->post('supplier', TRUE);
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;
									
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
			if ($no_fp != $faktur_pajak_lama) {
				$cek_data = $this->mmaster->cek_data($no_fp, $supplier);
				if (count($cek_data) == 0) {
					$this->db->query(" UPDATE tm_pembelian_pajak SET no_faktur_pajak = '$no_fp', tgl_update='$tgl',
									tgl_faktur_pajak = '$tgl_fp', jumlah= '$jum', dpp = '$dpp', uid_update_by='$uid_update_by'
									 where id= '$id_pajak' ");
					
					// reset dulu status_faktur_pajak menjadi FALSE --------------------------------
					// 5 des 2011
					$list_faktur_lama = explode(",", $no_faktur_lama); //
					$list_is_quilting_lama = explode(",", $is_quilting_lama);
					$list_id_faktur_lama = explode(";", $id_faktur_lama);
					
					for($j=0; $j<count($list_faktur_lama)-1; $j++){
						if ($list_is_quilting_lama[$j] == 'f') {
							// 25-06-2015 dikomen
							//if ($list_id_faktur_lama[$j] == '0')
							//	$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
							//		WHERE kode_supplier = '$supplier' AND no_faktur = '".$list_faktur_lama[$j]."' "); 
							//else
								$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
									WHERE id = '".$list_id_faktur_lama[$j]."' "); 
						}
						else {
							// 25-06-2015 dikomen
							//if ($list_id_faktur_lama[$j] == '0')
							//	$this->db->query(" UPDATE tm_faktur_makloon SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
							//		WHERE kode_unit = '$supplier' AND no_faktur = '".$list_faktur_lama[$j]."' AND jenis_makloon = '1' "); 
							//else
								$this->db->query(" UPDATE tm_faktur_makloon SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
									WHERE id = '".$list_id_faktur_lama[$j]."' AND jenis_makloon = '1' "); 
						}
					} 
					//-------------------------------------------------------------------------
					
					$this->db->query(" DELETE FROM tm_pembelian_pajak_detail where id_pembelian_pajak= '$id_pajak' ");
					
					// insert tabel detail faktur-nya
					$list_faktur = explode(",", $no_faktur);
					$list_is_quilting = explode(",", $is_quilting);
					$list_id_faktur = explode(";", $id_faktur);
					// 5 des 2011
					for($j=0; $j<count($list_faktur)-1; $j++){
						$data_detail = array(
							  'id_pembelian_pajak'=>$id_pajak,
							  'id_faktur_pembelian'=>$list_id_faktur[$j],
							  'no_faktur'=>''
							);
							$this->db->insert('tm_pembelian_pajak_detail',$data_detail);
							
							if ($list_is_quilting[$j] == 'f') {
								$this->db->query(" UPDATE tm_pembelian_nofaktur SET no_faktur_pajak = '$no_fp', 
											status_faktur_pajak = 't' WHERE id = '".$list_id_faktur[$j]."' ");
							}
							else {
								$this->db->query(" UPDATE tm_faktur_makloon SET no_faktur_pajak = '$no_fp', 
											status_faktur_pajak = 't' WHERE id = '".$list_id_faktur[$j]."' 
											AND jenis_makloon = '1' ");
							}
					}
										
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "faktur-pajak/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "faktur-pajak/cform/cari/".$csupplier."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('faktur-pajak/cform/view');
				}
				else {
					$data['query'] = $this->mmaster->get_pajak($id_pajak);
					$data['isi'] = 'faktur-pajak/veditform';
					$data['list_supplier'] = $this->mmaster->get_supplier(); 
					$data['msg'] = "Update gagal. Data no faktur pajak ".$no_fp." untuk pembelian dgn faktur ".$no_faktur." sudah ada..!";
					$this->load->view('template',$data);
				}	
			}
			else {
					$this->db->query(" UPDATE tm_pembelian_pajak SET no_faktur_pajak = '$no_fp', tgl_update='$tgl',
									tgl_faktur_pajak = '$tgl_fp', jumlah= '$jum', uid_update_by='$uid_update_by' 
									where id= '$id_pajak' ");
					
					// reset dulu status_faktur_pajak menjadi FALSE --------------------------------
					// 5 des 2011
					$list_faktur_lama = explode(",", $no_faktur_lama); //
					$list_is_quilting_lama = explode(",", $is_quilting_lama);
					$list_id_faktur_lama = explode(";", $id_faktur_lama);
					
					for($j=0; $j<count($list_faktur_lama)-1; $j++){
						if ($list_is_quilting_lama[$j] == 'f') {
							// 25-06-2015 dikomen
							//if ($list_id_faktur_lama[$j] == '0')
							//	$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
							//		WHERE kode_supplier = '$supplier' AND no_faktur = '".$list_faktur_lama[$j]."' "); 
							//else
								$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
									WHERE id = '".$list_id_faktur_lama[$j]."' "); 
						}
						else {
							// 25-06-2015 dikomen
							//if ($list_id_faktur_lama[$j] == '0')
							//	$this->db->query(" UPDATE tm_faktur_makloon SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
							//		WHERE kode_unit = '$supplier' AND no_faktur = '".$list_faktur_lama[$j]."' AND jenis_makloon = '1' "); 
							//else
								$this->db->query(" UPDATE tm_faktur_makloon SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
									WHERE id = '".$list_id_faktur_lama[$j]."' AND jenis_makloon = '1' "); 
						}
					}
					//-------------------------------------------------------------------------
					
					$this->db->query(" DELETE FROM tm_pembelian_pajak_detail where id_pembelian_pajak= '$id_pajak' ");
					
					// insert tabel detail faktur-nya
					$list_faktur = explode(",", $no_faktur);
					$list_is_quilting = explode(",", $is_quilting);
					$list_id_faktur = explode(";", $id_faktur);
					// 5 des 2011
					for($j=0; $j<count($list_faktur)-1; $j++){
						$data_detail = array(
							  'id_pembelian_pajak'=>$id_pajak,
							  'id_faktur_pembelian'=>$list_id_faktur[$j],
							  'no_faktur'=>''
							);
							$this->db->insert('tm_pembelian_pajak_detail',$data_detail);
							
							if ($list_is_quilting[$j] == 'f')
								$this->db->query(" UPDATE tm_pembelian_nofaktur SET no_faktur_pajak = '$no_fp', 
											status_faktur_pajak = 't' WHERE id = '".$list_id_faktur[$j]."' ");
							else
								$this->db->query(" UPDATE tm_faktur_makloon SET no_faktur_pajak = '$no_fp', 
											status_faktur_pajak = 't' WHERE id = '".$list_id_faktur[$j]."' 
											AND jenis_makloon = '1' ");
					}
					
				if ($carinya == '') $carinya = "all";
				if ($is_cari == 0)
					$url_redirectnya = "faktur-pajak/cform/view/index/".$cur_page;
				else
					$url_redirectnya = "faktur-pajak/cform/cari/".$csupplier."/".$carinya."/".$cur_page;
				
				redirect($url_redirectnya);
				//redirect('faktur-pajak/cform/view');
			}
  }
    
  function show_popup_faktur_pembelian(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jnsaction = $this->uri->segment(4); 
	$csupplier = $this->uri->segment(5); 
	if ($jnsaction == '')
		$jnsaction = $this->input->post('jnsaction', TRUE); 
	if ($csupplier == '')
		$csupplier = $this->input->post('supplier', TRUE); 
	
	if ($keywordcari == '' && ($csupplier == '' || $jnsaction == '') ) {
		$jnsaction 	= $this->uri->segment(4);
		$csupplier 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';	
		
	//$jum_total = $this->mmaster->get_faktur_pembeliantanpalimit($jnsaction, $csupplier, $keywordcari);
						/*	$config['base_url'] = base_url()."index.php/faktur-pajak/cform/show_popup_faktur_pembelian/".$jnsaction."/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		*/
	//$data['query'] = $this->mmaster->get_faktur_pembelian($config['per_page'],$this->uri->segment(7), $jnsaction, $csupplier, $keywordcari);						
	$data['query'] = $this->mmaster->get_faktur_pembelian($jnsaction, $csupplier, $keywordcari);
	$data['jum_total'] = count($data['query']);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$data['jnsaction'] = $jnsaction;

	$this->load->view('faktur-pajak/vpopuppemb',$data);
  }

  function submit(){
	//$this->load->library('form_validation');
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

			//$no_faktur 	= $this->input->post('no_faktur', TRUE);
			//$no_faktur = trim($no_faktur);
			//19-05-2014, pake id_faktur
			$id_faktur 	= $this->input->post('id_faktur', TRUE);
			$id_faktur = trim($id_faktur);
			
			$is_quilting 	= $this->input->post('is_quilting', TRUE);
			$is_quilting = trim($is_quilting);
			$supplier = $this->input->post('supplier', TRUE);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;						
			$jum 	= $this->input->post('jum', TRUE);
			$dpp 	= $this->input->post('dpp', TRUE);
			
				$cek_data = $this->mmaster->cek_data($no_fp, $supplier);
				if (count($cek_data) > 0) {
					$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$supplier' ");
					$hasilrow3 = $query3->row();
					$kode_supplier	= $hasilrow3->kode_supplier;
					$nama_supplier	= $hasilrow3->nama;
				
					$data['isi'] = 'faktur-pajak/vmainform';
					$data['msg'] = "Data no faktur ".$no_fp." dari supplier ".$kode_supplier." - ".$nama_supplier." sudah ada..!";
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($no_fp, $tgl_fp, $supplier, $jum, $dpp, $id_faktur, $is_quilting);
					redirect('faktur-pajak/cform/view');
				}
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'faktur-pajak/vformview';
    $keywordcari = "all";
    $csupplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-pajak/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);		
				
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-pajak/cform/cari/'.$csupplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'faktur-pajak/vformview';
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $csupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "faktur-pajak/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "faktur-pajak/cform/cari/".$csupplier."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('faktur-pajak/cform/view');
  }
  
  function get_detail_supplier() {
		$id_sup 	= $this->uri->segment(4);
		$rows = array();
		if(isset($kode_sup)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			//$rows = $this->mmaster->get_pkp_tipe_pajak_bykodesup($kode_sup);
			$rows = $this->mmaster->get_detail_supplier($id_sup);
			
			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);
  }
  
}
