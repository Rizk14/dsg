<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "WARNA BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('color/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/color/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$qry_color	= $this->mclass->colorcode();
		if($qry_color->num_rows()>0) {
			$row_color	= $qry_color->row();
			$data['icolor']	= $row_color->colorcode;
		} else {
			$data['icolor']	= "";
		}
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
		$data['isi']	= 'color/vmainform';
		$this->load->view('template',$data);
		
	}
function tambah(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "WARNA BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('color/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/color/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$qry_color	= $this->mclass->colorcode();
		if($qry_color->num_rows()>0) {
			$row_color	= $qry_color->row();
			$data['icolor']	= $row_color->colorcode;
		} else {
			$data['icolor']	= "";
		}
		$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
			
		$data['isi']	= 'color/vmainformadd';
		$this->load->view('template',$data);
	}
	function pagesnext(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "WARNA BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('color/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/color/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']	= 'color/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail(){	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']	= 'color/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$icolor		= $this->input->post('icolor')?$this->input->post('icolor'):$this->input->get_post('icolor');
		$ecolorname	= $this->input->post('ecolorname')?$this->input->post('ecolorname'):$this->input->get_post('ecolorname');
		
		if( (isset($icolor) && !empty($icolor)) && 
		    (isset($ecolorname) && !empty($ecolorname)) ) {
			$this->load->model('color/mclass');
			$this->mclass->msimpan($icolor,$ecolorname);
		} else {
			$data['page_title']	= "WARNA BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('color/mclass');
	
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= base_url().'index.php/color/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$qry_color	= $this->mclass->colorcode();
			if($qry_color->num_rows()>0) {
				$row_color	= $qry_color->row();
				$data['icolor']	= $row_color->colorcode;
			} else {
				$data['icolor']	= "";
			}
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);			

			$data['isi']	= 'color/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	function edit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		
		$data['page_title']	= "KELAS BARANG";
		$limages			= base_url();
		$data['list']		= "";
		$this->load->model('color/mclass');
		$qry_warnabrg		= $this->mclass->medit($id);
		if( $qry_warnabrg->num_rows() > 0 ) {
			$row_warnabrg	= $qry_warnabrg->row();
			$data['i_color']	= $row_warnabrg->i_color;
			$data['e_color_name']	= $row_warnabrg->e_color_name;
		} else {
			$data['i_color']	= "";
			$data['e_color_name']	= "";
		}
		$data['isi']	= 'color/veditform';
		$this->load->view('template',$data);
		
	}
	
	function actedit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$icolor	= @$this->input->post('icolor')?@$this->input->post('icolor'):@$this->input->get_post('icolor');
		$ecolorname	= @$this->input->post('ecolorname')?@$this->input->post('ecolorname'):@$this->input->get_post('ecolorname');
		
		if( !empty($icolor) && 
			!empty($ecolorname)) {
			$this->load->model('color/mclass');
			$this->mclass->mupdate($icolor,$ecolorname);
		} else {
			$data['page_title']	= "WARNA BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('color/mclass');
	
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= base_url().'index.php/color/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$qry_color	= $this->mclass->colorcode();
			if($qry_color->num_rows()>0) {
				$row_color	= $qry_color->row();
				$data['icolor']	= $row_color->colorcode;
			} else {
				$data['icolor']	= "";
			}
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
				
			$data['isi']	= 'color/veditform';
		$this->load->view('template',$data);
		}
	}
	/* 20052011
	function actdelete() {
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_color',array('i_color'=>$id));
	}
	*/

	function actdelete(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('color/mclass');
		$this->mclass->delete($id);
	}
		
	function cari(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txticolor	= $this->input->post('txticolor')?$this->input->post('txticolor'):$this->input->get_post('txticolor');
		$txtecolorname	= $this->input->post('txtecolorname')?$this->input->post('txtecolorname'):$this->input->get_post('txtecolorname');

		$data['page_title']	= "WARNA BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('color/mclass');

		$query	= $this->mclass->viewcari($txticolor,$txtecolorname);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/color/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txticolor,$txtecolorname,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('color/vcariform',$data);
	}
}
?>
