<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-makloon/mmaster');
  }
  // start 13-08-2015
	
  function ukuranbisbisan(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get_ukuran_bisbisan($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$enama = $row->nama;
		}
	}
	else {
			$eid = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAllukuranbisbisan();
    $data['isi'] = 'mst-makloon/vmainformukuranbisbisan';
    
	$this->load->view('template',$data);
  }

  function submitukuranbisbisan(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		
			$id_ukuran 	= $this->input->post('id_ukuran', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == 1) {
				$this->mmaster->saveukuranbisbisan($id_ukuran, $nama, $goedit);
			}
			else {
				$cek_data = $this->mmaster->cek_data_ukuran_bisbisan($nama);
				if (count($cek_data) == 0) { 
					$this->mmaster->saveukuranbisbisan($id_ukuran, $nama, $goedit);
				}
			}
			
			redirect('mst-makloon/cform/ukuranbisbisan');
  }

  function deleteukuranbisbisan(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->deleteukuranbisbisan($id);
    redirect('mst-makloon/cform/ukuranbisbisan');
  }
  
  function hargabisbisan(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get_harga_bisbisan($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$eid_supplier = $row->id_supplier;
			$ejenis_potong = $row->jenis_potong;
			$eharga = $row->harga;
		}
	}
	else {
			$eid='';
			$eid_supplier = '';
			$ejenis_potong = '';
			$eharga = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['eid_supplier'] = $eid_supplier;
	$data['ejenis_potong'] = $ejenis_potong;
	$data['eharga'] = $eharga;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAllhargabisbisan();
	$data['supplier'] = $this->mmaster->get_supplier();
	$data['jumdata'] = count($data['query']);
    $data['isi'] = 'mst-makloon/vmainformhargabisbisan';
	$this->load->view('template',$data);
  }
  
  function submithargabisbisan(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		
			$id_harga 	= $this->input->post('id_harga', TRUE);
			$id_supplier 	= $this->input->post('supplier', TRUE);
			$jenis_potong 	= $this->input->post('jenis_potong', TRUE);
			$harga 	= $this->input->post('harga', TRUE);
			
			if ($goedit == 1) {
				$this->mmaster->savehargabisbisan($id_harga, $id_supplier, $jenis_potong, $harga, $goedit);
			}
			else {
				$cek_data = $this->mmaster->cek_data_harga_bisbisan($id_supplier, $jenis_potong);
				if (count($cek_data) == 0) { 
					$this->mmaster->savehargabisbisan($id_harga, $id_supplier, $jenis_potong, $harga, $goedit);
				}
			}
			redirect('mst-makloon/cform/hargabisbisan');
  }
  
  function deletehargabisbisan(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->deletehargabisbisan($id);
    redirect('mst-makloon/cform/hargabisbisan');
  }
  
}
