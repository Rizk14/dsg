<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-btb-baju/mmaster');
  }
     function index(){

		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			
			redirect('loginform');
		}

	$kd_brg = $this->input->post('kd_brg', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	$id_sjmakloon_baju_gudang_jadi_wip_detail = $this->input->post('id_brg', TRUE);  
	$list_brg = explode(";", $id_sjmakloon_baju_gudang_jadi_wip_detail);
	$id_unit_jahit2 = $this->input->post('id_unit_jahit2', TRUE);    
	$tgl_sjmd = $this->input->post('tgl_sjmd', TRUE);    
	$tgl_sjmk = $this->input->post('tgl_sjmk', TRUE); 
	$jenismasuk  =$this->input->post('jenismasuk',TRUE);  
	
		if ($proses_submit == "Proses") {

				$data['id_unit_jahit']	= $id_unit_jahit2;
				$unit_jahitnya = $id_unit_jahit2;
				

				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_unit_jahit2' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_unit_jahit	= $hasilrow->kode_unit;
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else{
					$kode_unit_jahit	= '';
					$nama_unit_jahit	= 'ALL';
					}
				
				$data['kode_unit_jahit']	= $kode_unit_jahit;
				$data['nama_unit_jahit']	= $nama_unit_jahit;
				// -------------------
			

			$detail_sjmakloon_baju_gudang_jadi_wipnya = $this->mmaster->get_detail_sjmakloon_baju_gudang_jadi_wip($list_brg, $unit_jahitnya);
			
			$data['msg'] = '';
			$data['unit_jahit'] = $this->mmaster->get_unit_jahit();
			$data['kel_brg'] = $this->mmaster->get_kel_brg();
			
			$data['sjmakloon_baju_gudang_jadi_wip_detail'] = $detail_sjmakloon_baju_gudang_jadi_wipnya;
			
			$no_sjmakloon_baju_gudang_jadi_wip = ""; $id_sjmakloon_baju_gudang_jadi_wip = ""; $temp_no_sjmakloon_baju_gudang_jadi_wip = ""; $temp_id_sjmakloon_baju_gudang_jadi_wip = "";
			for($j=0;$j<count($detail_sjmakloon_baju_gudang_jadi_wipnya);$j++){
				//if ($ambil_pp == '') {
					if ($detail_sjmakloon_baju_gudang_jadi_wipnya[$j]['no_sjmakloon_baju_gudang_jadi_wip'] != $temp_no_sjmakloon_baju_gudang_jadi_wip)
						$no_sjmakloon_baju_gudang_jadi_wip.= $detail_sjmakloon_baju_gudang_jadi_wipnya[$j]['no_sjmakloon_baju_gudang_jadi_wip']."; ";
					
					if ($detail_sjmakloon_baju_gudang_jadi_wipnya[$j]['id_makloon_baju_gudang_jadi_wip'] != $temp_id_sjmakloon_baju_gudang_jadi_wip)
						$id_sjmakloon_baju_gudang_jadi_wip.= $detail_sjmakloon_baju_gudang_jadi_wipnya[$j]['id_makloon_baju_gudang_jadi_wip'].";";
						
					$temp_no_sjmakloon_baju_gudang_jadi_wip = $detail_sjmakloon_baju_gudang_jadi_wipnya[$j]['no_sjmakloon_baju_gudang_jadi_wip'];
					$temp_id_sjmakloon_baju_gudang_jadi_wip = $detail_sjmakloon_baju_gudang_jadi_wipnya[$j]['id_makloon_baju_gudang_jadi_wip'];
				//}
				
			}			
			
			$data['no_sjmakloon_baju_gudang_jadi_wip'] = $no_sjmakloon_baju_gudang_jadi_wip;
			$data['jenismasuk'] = $jenismasuk;
			$data['id_sjmakloon_baju_gudang_jadi_wip'] = $id_sjmakloon_baju_gudang_jadi_wip;
			$data['tgl_sjmd'] = $tgl_sjmd;	
			$data['tgl_sjmk'] = $tgl_sjmk;
		$data['go_proses'] = '1';
		
		$data['isi'] = 'faktur-btb-baju/vmainform';
		$this->load->view('template',$data);
     
		}
		else {
		$data['msg'] = '';
		$data['id_sjmakloon_baju_gudang_jadi_wip'] = '';
		$data['go_proses'] = '';
		$data['unit_jahit2'] = $this->mmaster->get_unit_jahit();
		$data['tgl_sjmd'] = $tgl_sjmd;	
		$data['tgl_sjmk'] = $tgl_sjmk;
		
		
		$data['isi'] = 'faktur-btb-baju/vmainform';
		$this->load->view('template',$data);
	}
	
  }
  
   function show_popup_sjmakloon_baju_gudang_jadi_wip(){
	// =======================
	// disini coding utk pengecekan user login
//========================
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		
	$id_ujh 	= $this->uri->segment(4);
	$js_msk		= $this->uri->segment(5);
	$tgl_sjmd	= $this->uri->segment(6);
	//print_r($tgl_sjmd);
	if (empty($tgl_sjmd)){
		$tgl_sjmd="00-00-0000";
		}
	$pisah1 = explode("-", $tgl_sjmd);
						$tgl1= $pisah1[0];
						$bln1= $pisah1[1];
						$thn1= $pisah1[2];
	$tgldari=$thn1."-".$bln1."-".$tgl1;
	//print_r($tgldari);
	
	$tgl_sjmk	= $this->uri->segment(7);
	if (empty($tgl_sjmk)){
		$tgl_sjmk="00-00-0000";
		}
	$pisah2 = explode("-", $tgl_sjmk);
						$tgl2= $pisah2[0];
						$bln2= $pisah2[1];
						$thn2= $pisah2[2];
	$tglke=$thn2."-".$bln2."-".$tgl2;
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	//$cunit_jahit 	= $this->input->post('unit_jahit', TRUE);  
	
	if ($keywordcari == '' && ($id_ujh == '' ) ) {
		$id_ujh 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(8);
	}
	

	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($id_ujh == '')
		$id_ujh = $this->input->post('id_ujh', TRUE);  
	
	
	$jum_total = $this->mmaster->get_sjmakloon_baju_gudang_jadi_wip_tanpa_limit($id_ujh, $keywordcari, $tgldari, $tglke,$js_msk); 
					
	$data['query'] = $this->mmaster->get_sjmakloon_baju_gudang_jadi_wip($id_ujh, $keywordcari, $tgldari, $tglke,$js_msk); 

	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['id_ujh'] = $id_ujh;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	
	if ($id_ujh != 0){
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_ujh' ");
	}
	else{
	$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit ");
	}
	
	$hasilrow = $query3->row();
	if ($query3->num_rows() != 0) {
		$kode_unit_jahit	= $hasilrow->kode_unit;
		$nama_unit_jahit	= $hasilrow->nama;
	}
	else {
		$kode_unit_jahit	= $hasilrow->kode_unit;
		$nama_unit_jahit	= $hasilrow->nama;
	}
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['nama_unit_jahit'] = $nama_unit_jahit;
	
	$this->load->view('faktur-btb-baju/vpopupsjmakloon_baju_gudang_jadi_wip',$data);

  }
    function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
			$jenismasuk				=$this->input->post('jenismasuk',TRUE);
			$no_sjmasukpembelian 	= $this->input->post('no_sjmasukpembelian', TRUE);
			$tgl_sjpembelian = $this->input->post('tgl_sjpembelian', TRUE);  
			$pisah1 = explode("-", $tgl_sjpembelian);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sjpembelian = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			$id_unit_jahit = $this->input->post('id_ujh', TRUE);  
			// 04-07-2015
			if ($id_unit_jahit == '0') {
				$id_unit_jahitbaru = $this->input->post('id_unit_jahitx', TRUE);
			}
			else
				$id_unit_jahitbaru = '0';
			$gtotal = $this->input->post('gtotal', TRUE);  
			
			//06-06-2015
			$asligtotal = $this->input->post('asligtotal', TRUE);  
			
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			if ($hide_pkp=='')
			$hide_pkp='f';
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			//$ambil_pp 	= $this->input->post('ambil_pp', TRUE);
			$lain_cash 	= $this->input->post('lain_cash', TRUE);
			$lain_kredit 	= $this->input->post('lain_kredit', TRUE);
			
			
			
			if ($lain_cash == '')
				$lain_cash = 'f';
			
			if ($lain_kredit == '')
				$lain_kredit = 'f';
			
			if ($id_unit_jahit == 0 ){
				$data['isi'] = 'faktur-btb-baju/vmainform';
					$data['msg'] = "Kode / Nama Unit tidak valid";
	
					$data['id_pp'] = '';
					$data['go_proses'] = '';
					$data['unit_jahit2'] = $this->mmaster->get_unit_jahit();
					$this->load->view('template',$data);
				
				}
				$cek_data = $this->mmaster->cek_data($no_sjmasukpembelian, $id_unit_jahit);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'faktur-btb-baju/vmainform';
					$data['msg'] = "Data no SJ ".$no_sjmasukpembelian." sudah ada..!";
	
					$data['id_pp'] = '';
					$data['go_proses'] = '';
					$data['unit_jahit2'] = $this->mmaster->get_unit_jahit();
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
	
						$this->mmaster->save($no_sjmasukpembelian,$tgl_sjpembelian,$id_unit_jahit,$id_unit_jahitbaru, $gtotal, $asligtotal, $total_pajak, $dpp, 
						$uang_muka, $sisa_hutang,$ket,$hide_pkp, $hide_tipe_pajak, $lain_cash, $lain_kredit,$jenismasuk,
						$this->input->post('id_sjmakloon_baju_gudang_jadi_wip_detail_'.$i, TRUE), 
						$this->input->post('id_sjmakloon_baju_gudang_jadi_wip_'.$i, TRUE),
						$this->input->post('id_brg_'.$i, TRUE),
						$this->input->post('nama_'.$i, TRUE), 
						$this->input->post('id_satuan_'.$i, TRUE), 
						$this->input->post('id_satuan_konversi_'.$i, TRUE),
									$this->input->post('qty_'.$i, TRUE), 
									$this->input->post('harga_'.$i, TRUE), 
									$this->input->post('harga_lama_'.$i, TRUE), 
									$this->input->post('pajak_'.$i, TRUE), 
									$this->input->post('diskon_'.$i, TRUE), 
									$this->input->post('total_'.$i, TRUE), 
									$this->input->post('aslitotal_'.$i, TRUE) );
						

					}
					redirect('faktur-btb-baju/cform/view');
				}
		
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'faktur-btb-baju/vformview';
    $keywordcari = "all";
    $cunit_jahit = '0';
	$id_bagian = ''; 
	
	$date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
	$caribrg = "all";
	$filterbrg = "n";
	
    $jum_total = $this->mmaster->getAlltanpalimit($cunit_jahit, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/faktur-btb-baju/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $cunit_jahit, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
	//print_r($data['query']);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['cunit_jahit'] = $cunit_jahit;
	$this->load->view('template',$data);
  }
  
   function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $cunit_jahit 	= $this->uri->segment(7);
    $tgl_awal 	= $this->uri->segment(8);
    $tgl_akhir 	= $this->uri->segment(9);
    $carinya 	= $this->uri->segment(10);
    $caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "faktur-btb-baju/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "faktur-btb-baju/cform/cari/".$cunit_jahit."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
	
	redirect($url_redirectnya);
					
   
  }
  
   function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$cunit_jahit = $this->input->post('unit_jahit', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
	
	if ($cunit_jahit == '')
		$cunit_jahit 	= $this->uri->segment(4);
	if ($date_from == '')
		$date_from = $this->uri->segment(5);
	if ($date_to == '')
		$date_to = $this->uri->segment(6);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(7);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(8);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(9);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($cunit_jahit == '')
		$cunit_jahit = '0';
	if ($filterbrg == '')
		$filterbrg = 'n';
	if ($caribrg == '')
		$caribrg = "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($cunit_jahit, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/faktur-btb-baju/cform/cari/'.$cunit_jahit.'/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(10);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(10), $cunit_jahit, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'faktur-btb-baju/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
		
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['cunit_jahit'] = $cunit_jahit;
	$this->load->view('template',$data);
  }
  function edit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_pembelian_wip 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$cunit_jahit 	= $this->uri->segment(7);
	$tgl_awal 	= $this->uri->segment(8);
	$tgl_akhir 	= $this->uri->segment(9);
	$carinya 	= $this->uri->segment(10);
	$caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
   $data['query'] = $this->mmaster->get_pembelian($id_pembelian_wip);
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit(); 
	$data['msg'] = '';
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['cunit_jahit'] = $cunit_jahit;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['isi'] = 'faktur-btb-baju/veditform';
	$data['id_pembelian_wip'] = $id_pembelian_wip;
	$this->load->view('template',$data);
					
   
  }
  
   function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_pembelian_wip 	= $this->input->post('id_pembelian_wip', TRUE);
			$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
			$no_sjmasukpembelian_baju 	= $this->input->post('no_sjmasukpembelian_baju', TRUE);
			$no_sjmasukpembelian_baju_lama 	= $this->input->post('no_sjmasukpembelian_baju_lama', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$id_unit_jahit_lama = $this->input->post('hide_unit_jahit', TRUE);  
			$id_unit_jahit = $this->input->post('id_unit_jahit', TRUE);  
			
			
			$asligtotal = $this->input->post('asligtotal', TRUE);  
			
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			$ambil_pp = $this->input->post('ambil_pp', TRUE);
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$cunit_jahit = $this->input->post('cunit_jahit', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
			
			
			$tgl = date("Y-m-d H:i:s");

			$id_gudang = 0;
			$lokasi = "01"; // duta
			
			
			$lain_cash 	= $this->input->post('lain_cash', TRUE);
			$lain_kredit 	= $this->input->post('lain_kredit', TRUE);
			
			
			if ($lain_cash == '')
				$lain_cash = 'f';
			if ($lain_kredit == '')
				$lain_kredit = 'f';
				
			
			
			$submit2 = $this->input->post('submit2', TRUE);
				
			// 20-07-2012
			if ($submit2 != '') { 
				$query4	= $this->db->query(" SELECT no_sjmasukpembelian, id_unit_jahit from tm_pembelian_baju where id = '$id_pembelian_wip' ");
				$hasilrow = $query4->row();
				$no_sjmasukpembelian	= $hasilrow->no_sjmasukpembelian;
				$id_unit_jahit	= $hasilrow->id_unit_jahit;
				

				$jumlah_input=$no-1;
				$hitungulangtotal = 0; $hitungulangppn = 0;
				for ($i=1;$i<=$jumlah_input;$i++) {
					if ($this->input->post('cek_'.$i, TRUE) == '') {
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							// ambil data total dan pajak dari tm_pembelian_baju_detail
							$query4	= $this->db->query(" SELECT pajak, total from tm_pembelian_baju_detail where id = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							$hasilrow = $query4->row();
							$pajaknya	= $hasilrow->pajak;
							$totalnya	= $hasilrow->total;
						
							$hitungulangtotal+= $totalnya;
							$hitungulangppn+= $pajaknya;
						}
					}
					
					if ($this->input->post('cek_'.$i, TRUE) == 'y') {
						
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							
							//========= start here 20-07-2012. 19-06-2015 ==========
							$id_brg_wipnya = $this->input->post('id_brg_wip_'.$i, TRUE);
						    $id_brg_wip_lama = $this->input->post('id_brg_wip_lama_'.$i, TRUE); 
						    $id_sjmakloon_baju_gudang_jadi_wip_detail = $this->input->post('id_sjmakloon_baju_gudang_jadi_wip_detail_'.$i, TRUE); 
						    $id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE); 
							
							if ($id_sjmakloon_baju_gudang_jadi_wip_detail != '0') {
								$query4	= $this->db->query(" SELECT id_sjmakloon_baju_gudang_jadi_wip FROM tm_sjmakloon_baju_gudang_jadi_wip_detail where id = '$id_sjmakloon_baju_gudang_jadi_wip_detail' ");
								$hasilrow = $query4->row();
								$id_sjmakloon_baju_gudang_jadi_wipupdatestatus	= $hasilrow->id_sjmakloon_baju_gudang_jadi_wip;
								
								$this->db->query("UPDATE tm_sjmakloon_baju_gudang_jadi_wip set status_sjmakloon_baju_gudang_jadi_wip = 'f' where id= '$id_sjmakloon_baju_gudang_jadi_wipupdatestatus' ");
								$this->db->query("UPDATE tm_sjmakloon_baju_gudang_jadi_wip_detail set status_sjmakloon_baju_gudang_jadi_wip = 'f' where id= '$id_sjmakloon_baju_gudang_jadi_wip_detail' ");
							}
							
							
													 
							
							 $qty_lama = $this->input->post('qty_lama_'.$i, TRUE); 
							 $harga_lama = $this->input->post('harga_lama_'.$i, TRUE); 
							 $id_brg_wip_lama = $this->input->post('id_brg_wip_lama_'.$i, TRUE); 
							 
							 //ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit WHERE id_brg_wip='$id_brg_wip_lama' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							else
								$stok_lama = 0;
										
							$stokreset1 = $stok_lama-$qty_lama;
										
							/*
														
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$stokreset1', tgl_update_stok = '$tgl'
												where id_brg_wip= '$id_brg_wip_lama' ");
									*/		
							$this->db->query("DELETE FROM tm_pembelian_baju_detail WHERE id='".$this->input->post('id_detail_'.$i, TRUE)."' ");
						} 
					} 
				} 
				if ($hide_pkp == 't')
					$hitungulangdpp = $hitungulangtotal/1.1;
				else
					$hitungulangdpp = 0;
				$hitungulangdpp = round($hitungulangdpp, 2);
				$hitungulangppn = round($hitungulangppn, 2);
				$hitungulangsisahutang = $hitungulangtotal-$uang_muka;
				
				$uid_update_by = $this->session->userdata('uid');
				
				// uang_muka = '$uang_muka', sisa_hutang = '$hitungulangsisahutang', 
				$this->db->query(" UPDATE tm_pembelian_baju SET no_sjmasukpembelian = '$no_sjmasukpembelian', id_unit_jahit = '$id_unit_jahit', 
								tgl_sjpembelian = '$tgl_sj', jenis_pembelian='$jenis_pembelian', tgl_update = '$tgl', 
								total_pajak = '$hitungulangppn', dpp = '$hitungulangdpp', total = '$hitungulangtotal',
								keterangan='$ket', uid_update_by='$uid_update_by'
								where id= '$id_pembelian_wip' ");
				
			} 
			else {
				
				$queryxx2	= $this->db->query(" SELECT pkp FROM tm_unit_jahit WHERE id = '".$id_unit_jahit."' ");
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->row();
					$pkp	= $hasilxx2->pkp;
				}
				else
					$pkp = 'f';
										
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_brg_wip_lama = $this->input->post('id_brg_wip_lama_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$qty_lama = $this->input->post('qty_lama_'.$i, TRUE);
						$harga = $this->input->post('harga_'.$i, TRUE);
						$harga_lama = $this->input->post('harga_lama_'.$i, TRUE);
						//21-09-2015
						$id_satuan_lama = $this->input->post('id_satuan_lama_'.$i, TRUE);
						$id_satuan = $this->input->post('id_satuan_'.$i, TRUE);
						$id_satuan_konversi = $this->input->post('id_satuan_konversi_'.$i, TRUE);
						$nama_brg = $this->input->post('nama_'.$i, TRUE);
						
						// 06-01-2015
						$id_brg_wipnya = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_brg_wip_lama = $this->input->post('id_brg_wip_lama_'.$i, TRUE); 
						$id_sjmakloon_baju_gudang_jadi_wip_detail = $this->input->post('id_sjmakloon_baju_gudang_jadi_wip_detail_'.$i, TRUE); 
						$id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE); 
							
						// 9 sept 2011, bisa insert item brg yg baru
						if ($this->input->post('id_detail_'.$i, TRUE) == 'n') {
							// a. insert item detail
							$data_detail = array(
								'id_brg_wip'=>$this->input->post('id_brg_wip_'.$i, TRUE),
								'nama_brg'=>$this->input->post('nama_'.$i, TRUE),
								'qty'=>$this->input->post('qty_'.$i, TRUE),
								'id_satuan'=>$this->input->post('id_satuan_'.$i, TRUE),
								'id_satuan_konversi'=>$this->input->post('id_satuan_konversi_'.$i, TRUE),
								'harga'=>$this->input->post('harga_'.$i, TRUE),
								'pajak'=>$this->input->post('pajak_'.$i, TRUE),
								'diskon'=>$this->input->post('diskon_'.$i, TRUE),
								'total'=>$this->input->post('aslitotal_'.$i, TRUE),
								'id_pembelian_wip'=>$id_pembelian_wip,
								'id_sjmakloon_baju_gudang_jadi_wip_detail'=>$this->input->post('id_sjmakloon_baju_gudang_jadi_wip_detail_'.$i, TRUE),
								'id_pp_detail'=>$this->input->post('id_pp_detail_'.$i, TRUE)
							);
							$this->db->insert('tm_pembelian_baju_detail',$data_detail);
							
							$query3	= $this->db->query(" SELECT id FROM tm_pembelian_baju_detail ORDER BY id DESC LIMIT 1 ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$id_pembelian_wip_detail = $hasilrow->id;
							}
							else
								$id_pembelian_wip_detail = 0;
							
							// 18-06-2015, save ke apply_stok digabung kesini
							$th_now	= date("Y");
	
							/*
							if (($harga != $harga_lama) && ($id_unit_jahit != $id_unit_jahit_lama)) {
								
								$this->db->query(" DELETE FROM tm_harga_brg_unit_jahit WHERE id_brg_wip = '$id_brg_wip' AND id_unit_jahit = '$id_unit_jahit_lama'
											AND id_satuan = '$id_satuan_lama' AND harga = '$harga_lama' ");
								
							
								
								$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_unit_jahit WHERE id_brg_wip = '$id_brg_wip'
												AND id_satuan = '$id_satuan' AND id_unit_jahit = '$id_unit_jahit' ");
								if ($query3->num_rows() == 0){
									$this->db->query(" INSERT INTO tm_harga_brg_unit_jahit (id_brg_wip, id_unit_jahit, id_satuan, harga, 
									tgl_input, tgl_update) VALUES ('$id_brg_wip', '$id_unit_jahit', '$id_satuan', '$harga', '$tgl', '$tgl') ");
								}
								else {
									$this->db->query(" UPDATE tm_harga_brg_unit_jahit SET harga = '$harga', tgl_update='$tgl'
												where id_brg_wip= '$id_brg_wip' AND id_unit_jahit = '$id_unit_jahit' AND id_satuan = '$id_satuan' ");
								}
								
								
								
							}
							else if (($harga != $harga_lama) && ($id_unit_jahit == $id_unit_jahit_lama)) {
								
								
								$this->db->query(" UPDATE tm_harga_brg_unit_jahit SET harga = '$harga', tgl_update='$tgl'
												where id_brg_wip= '$id_brg_wip' AND id_unit_jahit = '$id_unit_jahit' AND id_satuan = '$id_satuan' ");
								
								// 30-09-2015. cek ke tabel history harga (tm_stok_harga), jika harga blm ada maka insert
								$query3	= $this->db->query(" SELECT id FROM tm_stok_harga WHERE id_brg_wip = '$id_brg_wip' 
															AND harga = '$harga' AND id_satuan = '$id_satuan'
															AND is_harga_pkp = '$pkp' ");
								
							}
							
						
							*/
							$id_sjmakloon_baju_gudang_jadi_wip_detail = $this->input->post('id_sjmakloon_baju_gudang_jadi_wip_detail_'.$i, TRUE);
							$id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE);
							
							if ($id_sjmakloon_baju_gudang_jadi_wip_detail != '0') {
								// ambil qty di sjmakloon_baju_gudang_jadi_wip_detail
								$query3	= $this->db->query(" SELECT id_sjmakloon_baju_gudang_jadi_wip, qty FROM tm_sjmakloon_baju_gudang_jadi_wip_detail WHERE id = '$id_sjmakloon_baju_gudang_jadi_wip_detail' ");
								if ($query3->num_rows() > 0) {
									$hasilrow = $query3->row();
									$id_sjmakloon_baju_gudang_jadi_wip = $hasilrow->id_sjmakloon_baju_gudang_jadi_wip;
									$qty_sjmakloon_baju_gudang_jadi_wip = $hasilrow->qty;
									
									//cek jumlah pembelian. jika > qty sjmakloon_baju_gudang_jadi_wip maka jika sudah t semua di tabel detail, maka update tabel tm_sjmakloon_baju_gudang_jadi_wip di field status_sjmakloon_baju_gudang_jadi_wip menjadi t
									$sqlxx = " SELECT sum(b.qty) as jumbeli FROM tm_pembelian_baju a INNER JOIN tm_pembelian_baju_detail b 
												ON a.id = b.id_pembelian_wip WHERE a.status_aktif='t' AND b.id_sjmakloon_baju_gudang_jadi_wip_detail = '$id_sjmakloon_baju_gudang_jadi_wip_detail' ";
									$queryxx	= $this->db->query($sqlxx);
									if ($queryxx->num_rows() > 0) {
										$hasilxx = $queryxx->row();
										$jumbeli = $hasilxx->jumbeli;
										
										$selisih = $jumbeli-$qty_lama+$qty;
										
										if ($selisih >= $qty_sjmakloon_baju_gudang_jadi_wip) {
											$this->db->query(" UPDATE tm_sjmakloon_baju_gudang_jadi_wip_detail SET status_sjmakloon_baju_gudang_jadi_wip = 't' WHERE id = '$id_sjmakloon_baju_gudang_jadi_wip_detail' ");
											
											// cek udh t semua blm, kalo udh, maka ganti jadi t headernya
											$sqlxx2 = " SELECT id FROM tm_sjmakloon_baju_gudang_jadi_wip_detail WHERE status_sjmakloon_baju_gudang_jadi_wip = 'f' ";
											$queryxx2	= $this->db->query($sqlxx2);
											if ($queryxx2->num_rows() == 0) {
												$this->db->query(" UPDATE tm_sjmakloon_baju_gudang_jadi_wip SET status_sjmakloon_baju_gudang_jadi_wip = 't' WHERE id='$id_sjmakloon_baju_gudang_jadi_wip' ");
											}
										}
									}
									
								}
							}
							
							
					
							
								
							
						
						   
						   // ======================= 10-12-2015 =========================================================
						   //cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
							
																		
								//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
								$queryxx2	= $this->db->query(" SELECT id_pembelian_wip
														FROM tm_pembelian_baju_detail WHERE id = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
								if ($queryxx2->num_rows() > 0){
									$hasilxx2 = $queryxx2->row();
									$cid_pembelian_wip = $hasilxx2->id_pembelian_wip;
									
									$query3	= $this->db->query(" SELECT id FROM tm_pembelian_baju_detail WHERE status_stok = 'f' 
														AND id_pembelian_wip = '$cid_pembelian_wip' ");
									if ($query3->num_rows() == 0){
										$this->db->query(" UPDATE tm_pembelian_baju SET status_stok = 't' WHERE id = '$cid_pembelian_wip' ");
									}
								}

						} 
						// 06-01-2016 PINDAH KESINI YG UPDATE tm_pembelian_baju_detail
								//20-06-2015
							   $sql = " UPDATE tm_pembelian_baju_detail SET qty = '".$this->input->post('qty_'.$i, TRUE)."', ";
							   if ($id_brg_wip_lama != $id_brg_wip)
									$sql.= " id_brg_wip = '$id_brg_wip', ";
									
								$sql.= " nama_brg= '".$this->db->escape_str($this->input->post('nama_'.$i, TRUE))."', 
									id_satuan = '".$this->input->post('id_satuan_'.$i, TRUE)."',
									id_satuan_konversi = '".$this->input->post('id_satuan_konversi_'.$i, TRUE)."',
									harga = '".$this->input->post('harga_'.$i, TRUE)."', diskon = '".$this->input->post('diskon_'.$i, TRUE)."',
									pajak = '".$this->input->post('pajak_'.$i, TRUE)."', total = '".$this->input->post('aslitotal_'.$i, TRUE)."'
									where id= '".$this->input->post('id_detail_'.$i, TRUE)."' ";
								//echo $sql; die();
								$this->db->query($sql);
					   
					} 
					
			
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tm_pembelian_baju SET no_sjmasukpembelian_baju= '$no_sjmasukpembelian_baju', id_unit_jahit = '$id_unit_jahit', 
										jenis_pembelian = '$jenis_pembelian',
										tgl_sjpembelian_baju = '$tgl_sj', total = '$asligtotal',
										keterangan = '$ket', tgl_update = '$tgl', 
										pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak',
										total_pajak = '$total_pajak', dpp = '$dpp', 
										stok_masuk_lain_cash = '$lain_cash', 
										stok_masuk_lain_kredit = '$lain_kredit',
										uid_update_by='$uid_update_by'
										where id= '$id_pembelian_wip' ");
					
				}
				
				if ($carinya == '') $carinya = "all";
				if ($is_cari == 0)
					$url_redirectnya = "faktur-btb-baju/cform/view/index/".$cur_page;
				else
					$url_redirectnya = "faktur-btb-baju/cform/cari/".$cunit_jahit."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
					
				redirect($url_redirectnya);
				
				
  }
   function edittgl(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	$is_simpan 	= $this->input->post('is_simpan', TRUE);
	
	if ($is_simpan == '') {
		$id_pembelian_wip 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$cunit_jahit 	= $this->uri->segment(7);
		$tgl_awal 	= $this->uri->segment(8);
		$tgl_akhir 	= $this->uri->segment(9);
		$carinya 	= $this->uri->segment(10);
		$caribrg 	= $this->uri->segment(11);
		$filterbrg 	= $this->uri->segment(12);
		
		$query3	= $this->db->query(" SELECT no_sjmasukpembelian_baju, tgl_sjpembelian_baju, id_unit_jahit FROM tm_pembelian_baju WHERE id = '$id_pembelian_wip' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$no_sj = $hasilrow->no_sjmasukpembelian_baju;
			$tgl_sj = $hasilrow->tgl_sjpembelian_baju;
			$id_unit_jahit = $hasilrow->id_unit_jahit;
			
			$pisah1 = explode("-", $tgl_sj);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_sj = $tgl1."-".$bln1."-".$thn1;
			
			
			$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_unit_jahit' ");
			$hasilrow = $query3->row();
			$kode_unit_jahit	= $hasilrow->kode_unit;
			$nama_unit_jahit	= $hasilrow->nama;
		}
		else {
			$no_sj = '';
			$tgl_sj = '';
			$kode_unit_jahit = '';
			$nama_unit_jahit = '';
		}
		
		$data['msg'] = '';
		$data['id_pembelian_wip'] = $id_pembelian_wip;
		$data['no_sj'] = $no_sj;
		$data['tgl_sj'] = $tgl_sj;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['kode_unit_jahit'] = $kode_unit_jahit;
		$data['nama_unit_jahit'] = $nama_unit_jahit;
		
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['cunit_jahit'] = $cunit_jahit;
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['carinya'] = $carinya;
		$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;

		$data['isi'] = 'faktur-btb-baju/vedittgl';
		$this->load->view('template',$data);
	}
	else { // simpan
		$id_pembelian_wip 	= $this->input->post('id_pembelian_wip', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sjpembelian', TRUE);  
		$pisah1 = explode("-", $tgl_sj);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_sj = $thn1."-".$bln1."-".$tgl1;
		$tgl = date("Y-m-d H:i:s");
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$cunit_jahit = $this->input->post('cunit_jahit', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
		
		// update tglnya
		$this->db->query(" UPDATE tm_pembelian_baju SET tgl_sjpembelian_baju = '$tgl_sj', tgl_update = '$tgl'
							WHERE id= '$id_pembelian_wip' ");
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "faktur-btb-baju/cform/view/index/".$cur_page;
		else
			$url_redirectnya = "faktur-btb-baju/cform/cari/".$cunit_jahit."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
			
		redirect($url_redirectnya);
	}

  }
}
