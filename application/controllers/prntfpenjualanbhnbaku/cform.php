<?php
include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}		
			$data['page_title_fpenjualanbhnbaku']	= $this->lang->line('page_title_fpenjualanbhnbaku');
			$data['form_title_detail_fpenjualanbhnbaku']	= $this->lang->line('form_title_detail_fpenjualanbhnbaku');
			$data['list_fpenjualanbhnbaku_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_faktur');
			$data['list_fpenjualanbhnbaku_kd_brg']	= $this->lang->line('list_fpenjualanbhnbaku_kd_brg');
			$data['list_fpenjualanbhnbaku_jenis_brg']	= $this->lang->line('list_fpenjualanbhnbaku_jenis_brg');
			$data['list_fpenjualanbhnbaku_s_produksi'] = $this->lang->line('list_fpenjualanbhnbaku_s_produksi');
			$data['list_fpenjualanbhnbaku_nm_brg']	= $this->lang->line('list_fpenjualanbhnbaku_nm_brg');
			$data['list_fpenjualanbhnbaku_qty']	= $this->lang->line('list_fpenjualanbhnbaku_qty');
			$data['list_fpenjualanbhnbaku_satuan']	= $this->lang->line('list_fpenjualanbhnbaku_satuan');
			$data['list_fpenjualanbhnbaku_hjp']	= $this->lang->line('list_fpenjualanbhnbaku_hjp');
			$data['list_fpenjualanbhnbaku_amount']	= $this->lang->line('list_fpenjualanbhnbaku_amount');
			$data['list_fpenjualanbhnbaku_total_pengiriman']	= $this->lang->line('list_fpenjualanbhnbaku_total_pengiriman');
			$data['list_fpenjualanbhnbaku_total_penjualan']	= $this->lang->line('list_fpenjualanbhnbaku_total_penjualan');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();

			$this->load->model('prntfpenjualanbhnbaku/mclass');
			
			$data['opt_jns_brg']	= $this->mclass->lklsbrg();
			$data['isi']		= 'prntfpenjualanbhnbaku/vmainform';
			$this->load->view('template',$data);
			
		}	
	

	function listfakturbhnbaku() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpenjualanbhnbaku/mclass');

		$query	= $this->mclass->lfakturbhnbaku();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/prntfpenjualanbhnbaku/cform/listfakturbhnbakunext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['isi']	= $this->mclass->lfakturbhnbakuperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('prntfpenjualanbhnbaku/vlistformfakturbhnbaku',$data);			
	}

	function listfakturbhnbakunext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpenjualanbhnbaku/mclass');

		$query	= $this->mclass->lfakturbhnbaku();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntfpenjualanbhnbaku/cform/listfakturbhnbakunext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturbhnbakuperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('prntfpenjualanbhnbaku/vlistformfakturbhnbaku',$data);			
	}

	function flistfakturbhnbaku() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('prntfpenjualanbhnbaku/mclass');

		$query	= $this->mclass->flfakturbhnbaku($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	
	function carilistpenjualanbhnbaku() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$nofaktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->input->get_post('no_faktur');
		$data['nofaktur']	= $nofaktur;
		
		$data['page_title_fpenjualanbhnbaku']	= $this->lang->line('page_title_fpenjualanbhnbaku');
		$data['form_title_detail_fpenjualanbhnbaku']	= $this->lang->line('form_title_detail_fpenjualanbhnbaku');
		$data['list_fpenjualanbhnbaku_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_faktur');
		$data['list_fpenjualanbhnbaku_kd_brg']	= $this->lang->line('list_fpenjualanbhnbaku_kd_brg');
		$data['list_fpenjualanbhnbaku_jenis_brg']	= $this->lang->line('list_fpenjualanbhnbaku_jenis_brg');
		$data['list_fpenjualanbhnbaku_s_produksi'] = $this->lang->line('list_fpenjualanbhnbaku_s_produksi');
		$data['list_fpenjualanbhnbaku_nm_brg']	= $this->lang->line('list_fpenjualanbhnbaku_nm_brg');
		$data['list_fpenjualanbhnbaku_qty']	= $this->lang->line('list_fpenjualanbhnbaku_qty');
		$data['list_fpenjualanbhnbaku_satuan']	= $this->lang->line('list_fpenjualanbhnbaku_satuan');
		$data['list_fpenjualanbhnbaku_hjp']	= $this->lang->line('list_fpenjualanbhnbaku_hjp');
		$data['list_fpenjualanbhnbaku_amount']	= $this->lang->line('list_fpenjualanbhnbaku_amount');
		$data['list_fpenjualanbhnbaku_total_pengiriman']	= $this->lang->line('list_fpenjualanbhnbaku_total_pengiriman');
		$data['list_fpenjualanbhnbaku_total_penjualan']	= $this->lang->line('list_fpenjualanbhnbaku_total_penjualan');
		
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();
				
		$this->load->model('prntfpenjualanbhnbaku/mclass');
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		$data['query']	= $this->mclass->clistpenjualanbhnbaku($nofaktur);
		$data['isi']		= 'prntfpenjualanbhnbaku/vlistform';
			$this->load->view('template',$data);
		
	}
	
	function cpenjualanbhnbaku() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_fpenjualanbhnbaku']	= $this->lang->line('page_title_fpenjualanbhnbaku');
		$data['form_title_detail_fpenjualanbhnbaku']	= $this->lang->line('form_title_detail_fpenjualanbhnbaku');
		$data['list_fpenjualanbhnbaku_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_faktur');
		$data['list_fpenjualanbhnbaku_kd_brg']	= $this->lang->line('list_fpenjualanbhnbaku_kd_brg');
		$data['list_fpenjualanbhnbaku_jenis_brg']	= $this->lang->line('list_fpenjualanbhnbaku_jenis_brg');
		$data['list_fpenjualanbhnbaku_s_produksi'] = $this->lang->line('list_fpenjualanbhnbaku_s_produksi');
		$data['list_fpenjualanbhnbaku_nm_brg']	= $this->lang->line('list_fpenjualanbhnbaku_nm_brg');
		$data['list_fpenjualanbhnbaku_qty']	= $this->lang->line('list_fpenjualanbhnbaku_qty');
		$data['list_fpenjualanbhnbaku_satuan']	= $this->lang->line('list_fpenjualanbhnbaku_satuan');
		$data['list_fpenjualanbhnbaku_hjp']	= $this->lang->line('list_fpenjualanbhnbaku_hjp');
		$data['list_fpenjualanbhnbaku_amount']	= $this->lang->line('list_fpenjualanbhnbaku_amount');
		$data['list_fpenjualanbhnbaku_total_pengiriman']	= $this->lang->line('list_fpenjualanbhnbaku_total_pengiriman');
		$data['list_fpenjualanbhnbaku_total_penjualan']	= $this->lang->line('list_fpenjualanbhnbaku_total_penjualan');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();
		
		$nofaktur	= $this->uri->segment(4); 
		$data['nofaktur']	= $nofaktur;
				
		$this->load->model('prntfpenjualanbhnbaku/mclass');
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		$data['query']		= $this->mclass->clistpenjualanbhnbaku($nofaktur);
			$data['isi']		= 'prntfpenjualanbhnbaku/vprintform';
			$this->load->view('template',$data);
		
	}
	
	function cpopup() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iduserid	= $this->session->userdata('user_idx');	
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.194';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;	

		$data['page_title_fpenjualanbhnbaku']	= $this->lang->line('page_title_fpenjualanbhnbaku');
		$data['form_title_detail_fpenjualanbhnbaku']	= $this->lang->line('form_title_detail_fpenjualanbhnbaku');
		$data['list_fpenjualanbhnbaku_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_faktur');
		$data['list_fpenjualanbhnbaku_kd_brg']	= $this->lang->line('list_fpenjualanbhnbaku_kd_brg');
		$data['list_fpenjualanbhnbaku_jenis_brg']	= $this->lang->line('list_fpenjualanbhnbaku_jenis_brg');
		$data['list_fpenjualanbhnbaku_s_produksi'] = $this->lang->line('list_fpenjualanbhnbaku_s_produksi');
		$data['list_fpenjualanbhnbaku_nm_brg']	= $this->lang->line('list_fpenjualanbhnbaku_nm_brg');
		$data['list_fpenjualanbhnbaku_qty']	= $this->lang->line('list_fpenjualanbhnbaku_qty');
		$data['list_fpenjualanbhnbaku_satuan']	= $this->lang->line('list_fpenjualanbhnbaku_satuan');
		$data['list_fpenjualanbhnbaku_hjp']	= $this->lang->line('list_fpenjualanbhnbaku_hjp');
		$data['list_fpenjualanbhnbaku_amount']	= $this->lang->line('list_fpenjualanbhnbaku_amount');
		$data['list_fpenjualanbhnbaku_total_pengiriman']	= $this->lang->line('list_fpenjualanbhnbaku_total_pengiriman');
		$data['list_fpenjualanbhnbaku_total_penjualan']	= $this->lang->line('list_fpenjualanbhnbaku_total_penjualan');

		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";
		$data['limages']	= base_url();
		$data['lfaktur']	= "";

		$nofaktur	= $this->uri->segment(4); 

		$this->load->model('prntfpenjualanbhnbaku/mclass');

		$qry_infoheader		= $this->mclass->clistfpenjualanbhnbaku_header($nofaktur);

		if($qry_infoheader->num_rows()>0) {

			$bglobal	= array(
			'01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
			'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember');

			$row_infoheader	= $qry_infoheader->row();
			$nofaktur		= $row_infoheader->ifakturcode;
			$tglfaktur		= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$enote			= $row_infoheader->enote;

			$qcabang	= $this->mclass->getcabang($nofaktur);
			$qinitial	= $this->mclass->getinitial();
			
			if($qcabang->num_rows()>0) {
				$row_cabang	= $qcabang->row_array();
				$data['nmcabang']	= $row_cabang['cabangname'];
				$data['alamatcabang']	= $row_cabang['address'];
			} else {
				$data['nmcabang']	= "";
				$data['alamatcabang']	= "";
			}
					
			if($qinitial->num_rows()>0) {
				$row_initial	= $qinitial->row_array();
				$data['nminitial']	= $row_initial['e_initial_name'];
			} else {
				$data['nminitial']	= "";
			}
					
			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];

			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']		= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;
			$data['enote']			= $enote;
		}

		$qry_source_remote	= $this->mclass->remote($iduserid);

		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}

		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;
		
		$qry_jml	= $this->mclass->clistfpenjualanbhnbaku_jml($nofaktur);
		$qry_fincludeppn = $this->mclass->fincludeppn($nofaktur);
		
		if($qry_jml->num_rows() > 0) {
			
			$row_jml = $qry_jml->row_array();
			$row_fincludeppn = $qry_fincludeppn->row();
			
			$total	 = $row_jml['total']; // 2432300
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			if($row_fincludeppn->f_include_ppn == true) {
				$total_pls_disc	= $total - $v_disc; // DPP
				$nilai_ppn		= (($total_pls_disc*10) / 100 ); // PPN
				$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			}else{
				$total_pls_disc	= $total - $v_disc; // DPP
				$nilai_ppn		= 0; // PPN
				$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			}
			
			$data['jumlah']		= $total; // Jumlah
			$data['dpp']		= $total_pls_disc; // DPP
			$data['diskon']		= $v_disc; // Diskon
			$data['nilai_ppn']	= $nilai_ppn; // PPN
			$data['nilai_faktur']	= round($nilai_faktur); // Nilai Faktur
			
			/*
			$data['jumlah']		= round($total,2); // Jumlah
			$data['dpp']		= round($total_pls_disc,2); // DPP
			$data['diskon']		= round($v_disc,2); // Diskon
			$data['nilai_ppn']	= round($nilai_ppn,2); // PPN
			$data['nilai_faktur']	= round($nilai_faktur,2); // Nilai Faktur
			*/ 
		}
		
		$data['opt_jns_brg']= $this->mclass->lklsbrg();
		$data['isi']		= $this->mclass->clistpenjualanbhnbaku2($nofaktur);
		//print_r($data['isi']); die(); test aja
		/*$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif); 
		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$jmlqty[$lup]	= $row_value->qty;
			$uprice[$lup]	= $row_value->unitprice;
			$tprice[$lup]	= $row_value->amount;
		}
		echo($qvalue); die(); */
		
		$this->load->view('prntfpenjualanbhnbaku/vtestform',$data);
	}
}
?>
