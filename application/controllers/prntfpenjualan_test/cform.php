<?php
include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {
	
	function __construct() { 
		parent::__construct();
	}
	
	function index() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_fpenjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
			$data['form_title_detail_fpenjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
			$data['list_fpenjperno_faktur']	= $this->lang->line("list_fpenjperno_faktur");
			$data['list_fpenjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
			$data['list_fpenjperdo_tgl_mulai']	= $this->lang->line('list_penjperdo_tgl_mulai');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
		
		$data['isi']		= 'prntfpenjualan_test/vmainform';
			$this->load->view('template',$data);
	}
	
	function carilistpenjualanperdo() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_fpenjualanperdo']	= $this->lang->line('page_title_fpenjualanperdo');
		$data['form_title_detail_fpenjualanperdo']	= $this->lang->line('form_title_detail_fpenjualanperdo');
		$data['list_fpenjperno_faktur']	= $this->lang->line("list_fpenjperno_faktur");		
		$data['list_fpenjperdo_kd_brg']	= $this->lang->line('list_fpenjperdo_kd_brg');
		$data['list_fpenjperdo_tgl_mulai']	= $this->lang->line('list_fpenjperdo_tgl_mulai');
		$data['list_fpenjperdo_no_do']	= $this->lang->line('list_fpenjperdo_no_do');
		$data['list_fpenjperdo_nm_brg']	= $this->lang->line('list_fpenjperdo_nm_brg');
		$data['list_fpenjperdo_qty']	= $this->lang->line('list_fpenjperdo_qty');
		$data['list_fpenjperdo_hjp']	= $this->lang->line('list_fpenjperdo_hjp');
		$data['list_fpenjperdo_amount']	= $this->lang->line('list_fpenjperdo_amount');
		$data['list_fpenjperdo_total_pengiriman']	= $this->lang->line('list_fpenjperdo_total_pengiriman');
		$data['list_fpenjperdo_total_penjualan']	= $this->lang->line('list_fpenjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		$nofaktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->input->get_post('no_faktur');
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->input->get_post('d_do_first');
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->input->get_post('d_do_last');
			
		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";

		$data['tgldomulai']	= $d_do_first; // DD/MM/YYYY
		$data['tgldoakhir']	= $d_do_last;
		
		$data['tgldomulai2']= $n_d_do_first; // YYYY-MM-DD
		$data['tgldoakhir2']= $n_d_do_last;
		
		$data['nofaktur']	= $nofaktur;
				
		$this->load->model('prntfpenjualan_test/mclass');
		$data['query']	= $this->mclass->clistfpenjbrgjadiperdo($nofaktur,$n_d_do_first,$n_d_do_last);
		$data['isi']		= 'prntfpenjualan_test/vlistform';
			$this->load->view('template',$data);
		
	}

	function cpenjualanperdo() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_fpenjualanperdo']	= $this->lang->line('page_title_fpenjualanperdo');
		$data['form_title_detail_fpenjualanperdo']	= $this->lang->line('form_title_detail_fpenjualanperdo');
		$data['list_fpenjperno_faktur']	= $this->lang->line("list_fpenjperno_faktur");		
		$data['list_fpenjperdo_kd_brg']	= $this->lang->line('list_fpenjperdo_kd_brg');
		$data['list_fpenjperdo_tgl_mulai']	= $this->lang->line('list_fpenjperdo_tgl_mulai');
		$data['list_fpenjperdo_no_do']	= $this->lang->line('list_fpenjperdo_no_do');
		$data['list_fpenjperdo_nm_brg']	= $this->lang->line('list_fpenjperdo_nm_brg');
		$data['list_fpenjperdo_qty']	= $this->lang->line('list_fpenjperdo_qty');
		$data['list_fpenjperdo_hjp']	= $this->lang->line('list_fpenjperdo_hjp');
		$data['list_fpenjperdo_amount']	= $this->lang->line('list_fpenjperdo_amount');
		$data['list_fpenjperdo_total_pengiriman']	= $this->lang->line('list_fpenjperdo_total_pengiriman');
		$data['list_fpenjperdo_total_penjualan']	= $this->lang->line('list_fpenjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();

		$nofaktur	= $this->uri->segment(4); 
		$d_do_first	= $this->uri->segment(5); // YYYY-MM-DD
		$d_do_last	= $this->uri->segment(6);
		
		$exp_ddofirst	= explode("-",$d_do_first,strlen($d_do_first)); // YYYY-MM-DD
		$exp_ddolast	= explode("-",$d_do_last,strlen($d_do_last));
		$new_ddofirst	= !empty($exp_ddofirst[2])?$exp_ddofirst[2].'/'.$exp_ddofirst[1].'/'.$exp_ddofirst[0]:""; // DD-MM-YYYY
		$new_ddolast	= !empty($exp_ddolast[2])?$exp_ddolast[2].'/'.$exp_ddolast[1].'/'.$exp_ddolast[0]:""; // DD-MM-YYYY
		
		$data['tgldomulai']	= $new_ddofirst; // DD/MM/YYYY
		$data['tgldoakhir']	= $new_ddolast;
		$data['tgldomulai2']	= $d_do_first; // YYYY-MM-DD
		$data['tgldoakhir2']	= $d_do_last;
		$data['nofaktur']	= $nofaktur;
		
		$this->load->model('prntfpenjualan_test/mclass');
		$data['query']	= $this->mclass->clistfpenjbrgjadiperdo($nofaktur,$d_do_first,$d_do_last);
		$data['isi']	='prntfpenjualan_test/vprintform';
			$this->load->view('template',$data);
	}
	
	/* Cetak Faktur Penjualan dgn Windows Popup
	di remark
	*/
	function cpopup() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iduserid	= $this->session->userdata('user_idx');
		$remote		= $_SERVER['REMOTE_ADDR'];
		//ini IP jenny meureun
		$host		= '192.168.0.194';
		//$host		= '192.168.0.134';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;	

		//$data['charset']	= 'us-ascii';
		//$data['setLanguage']= 'en_us';
		//$data['mime_media_type']	= 'application/octet-stream';
			
		$data['page_title_fpenjualanperdo']	= $this->lang->line('page_title_fpenjualanperdo');
		$data['form_title_detail_fpenjualanperdo']	= $this->lang->line('form_title_detail_fpenjualanperdo');
		$data['list_fpenjperno_faktur']	= $this->lang->line("list_fpenjperno_faktur");		
		$data['list_fpenjperdo_kd_brg']	= $this->lang->line('list_fpenjperdo_kd_brg');
		$data['list_fpenjperdo_tgl_mulai']	= $this->lang->line('list_fpenjperdo_tgl_mulai');
		$data['list_fpenjperdo_no_do']	= $this->lang->line('list_fpenjperdo_no_do');
		$data['list_fpenjperdo_nm_brg']	= $this->lang->line('list_fpenjperdo_nm_brg');
		$data['list_fpenjperdo_qty']	= $this->lang->line('list_fpenjperdo_qty');
		$data['list_fpenjperdo_hjp']	= $this->lang->line('list_fpenjperdo_hjp');
		$data['list_fpenjperdo_amount']	= $this->lang->line('list_fpenjperdo_amount');
		$data['list_fpenjperdo_total_pengiriman']	= $this->lang->line('list_fpenjperdo_total_pengiriman');
		$data['list_fpenjperdo_total_penjualan']	= $this->lang->line('list_fpenjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		$data['lfaktur']	= "";
				
		$d_do_first	= $this->uri->segment(5); // YYYY-MM-DD
		$d_do_last	= $this->uri->segment(6);
		$nofaktur	= $this->uri->segment(4);
		
		$data['nofaktur']	= trim($nofaktur);
		
		$this->load->model('prntfpenjualan_test/mclass');

		/*
		$qry_source_remote	= $this->mclass->remote($remote);
		*/
		$qry_source_remote	= $this->mclass->remote($iduserid);
		// sementara khusus 19-05-2012, ntar dibuka lagi (25-05-2012 udh dibuka lg)
		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;

		$qry_infoheader		= $this->mclass->clistfpenjperdo($nofaktur,$d_do_first,$d_do_last);

		$qcabang	= $this->mclass->getcabang($nofaktur);
		$qinitial	= $this->mclass->getinitial();
		
		if($qcabang->num_rows()>0) {
			$row_cabang	= $qcabang->row_array();
			$data['nmcabang']	= $row_cabang['cabangname'];
			$data['alamatcabang']	= $row_cabang['address'];
		} else {
			$data['nmcabang']	= "";
			$data['alamatcabang']	= "";
		}
				
		if($qinitial->num_rows()>0) {
			$row_initial	= $qinitial->row_array();
			$data['nminitial']	= $row_initial['e_initial_name'];
		} else {
			$data['nminitial']	= "";
		}
		
		if($qry_infoheader->num_rows() > 0){

			$bglobal	= array(
			'01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
			'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
			
			$row_infoheader	= $qry_infoheader->row();
			$nofaktur	= $row_infoheader->ifakturcode;
			$tglfaktur	= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$nmcabang	= $row_infoheader->branchname;

			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];

			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']	= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;
			$data['namacab']	= $nmcabang;			
		}

		/***
			nilai	= document.getElementById('v_total_nilai');
			val		= document.getElementById('n_discount');
			
			hasil 	= ( (parseInt(nilai.value)*parseInt(val.value)) / 100 );
			total_sblm_ppn	= parseInt(nilai.value) - hasil;
			nilai_ppn	= ( (total_sblm_ppn*10) / 100 );
			total_grand	= total_sblm_ppn + nilai_ppn;		
		***/
		
		$qry_jml	= $this->mclass->clistfpenjperdo_jml($nofaktur,$d_do_first,$d_do_last);
		$qry_totalnyaini	= $this->mclass->clistfpenjperdo_totalnyaini($nofaktur,$d_do_first,$d_do_last);
		
		if($qry_jml->num_rows() > 0 ) {
			$row_jml = $qry_jml->row_array();
			$row_totalnyaini = $qry_totalnyaini->row_array();
			$total	 = $row_totalnyaini['totalnyaini'];
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			/*
			$total=0;
			$n_disc=0;
			$v_disc=0;
			foreach($qry_jml->result() as $row){
				$total	 += $row->total;
				$n_disc	 += $row->n_disc;
				$v_disc	 += $row->v_disc;
			}
			*/	
			$total_pls_disc	= $total - $v_disc; // DPP
			$nilai_ppn		= (($total_pls_disc*11) / 100 ); // PPN
			$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			
			$data['jumlah']		= $total; // Jumlah
			$data['dpp']		= $total_pls_disc; // DPP
			$data['diskon']		= $v_disc; // Diskon
			$data['nilai_ppn']	= $nilai_ppn; // PPN
			//$nilaifaktur		= explode(".",$nilai_faktur,strlen($nilai_faktur)); // Nilai Faktur
			//$data['nilai_faktur']	= $nilaifaktur[0];
			$data['nilai_faktur']	= round($nilai_faktur);
		} else {
			$data['jumlah']		= 0;
			$data['dpp']		= 0;
			$data['diskon']		= 0;
			$data['nilai_ppn']	= 0;
			$data['nilai_faktur']	= 0;
		}
		
		//$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo($nofaktur,$d_do_first,$d_do_last);
		$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo2($nofaktur,$d_do_first,$d_do_last);
		$data['iopdo']	= $this->mclass->clistfpenjperdo_detail_opdo($nofaktur,$d_do_first,$d_do_last);
		
		$this->load->view('prntfpenjualan_test/vtestform',$data);
		//$this->load->view('prntfpenjualan/vpopupform',$data);
	}
	
	function listbarangjadi() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpenjualan_test/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntfpenjualan_test/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('prntfpenjualan_test/vlistformbrgjadi',$data);
	}
	
	function listbarangjadinext() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpenjualan/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntfpenjualan_test/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('prntfpenjualan_test/vlistformbrgjadi',$data);
	}	
	
	function flistbarangjadi() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('prntfpenjualan_test/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
}
?>
