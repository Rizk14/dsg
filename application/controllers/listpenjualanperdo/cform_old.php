<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0 ) 
		{	
			$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
			$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
			$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
			$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
			$data['list_penjperdo_tgl_mulai']	= $this->lang->line('list_penjperdo_tgl_mulai');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			
			$data['detail']		= "";
			$data['list']		= "";
			$data['disabled']	= 'f';
			$data['limages']	= base_url();
			$this->load->model('listpenjualanperdo/mclass');
			$this->load->view('listpenjualanperdo/vmainform',$data);
		}
	}
	
	function carilistpenjualanperdo() {
		
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_mulai']	= $this->lang->line('list_penjperdo_tgl_mulai');
		$data['list_penjperdo_no_do']	= $this->lang->line('list_penjperdo_no_do');
		$data['list_penjperdo_nm_brg']	= $this->lang->line('list_penjperdo_nm_brg');
		$data['list_penjperdo_qty']	= $this->lang->line('list_penjperdo_qty');
		$data['list_penjperdo_hjp']	= $this->lang->line('list_penjperdo_hjp');
		$data['list_penjperdo_amount']	= $this->lang->line('list_penjperdo_amount');
		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";		
		$data['limages']	= base_url();
		
		$no_faktur	= $this->input->post('no_faktur');
		$ddofirst	= $this->input->post('d_do_first');
		$ddolast	= $this->input->post('d_do_last');
		$iproduct	= trim($this->input->post('i_product'));
		
		$data['tgldomulai']	= (!empty($ddofirst) && $ddofirst!='0')?$ddofirst:'';
		$data['tgldoakhir']	= (!empty($ddolast) && $ddolast!='0')?$ddolast:'';
		$data['nofaktur']	= (!empty($no_faktur) && $no_faktur!='0')?$no_faktur:'';
		$data['iproduct']	= (!empty($iproduct) && $iproduct!='0')?$iproduct:'';
		
		$e_d_do_first	= explode("/",$ddofirst,strlen($ddofirst));
		$e_d_do_last	= explode("/",$ddolast,strlen($ddolast));
		
		$nddofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:'0';
		$nddolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:'0';
		
		$turi1	= ($no_faktur!='' && $no_faktur!='0')?$no_faktur:'0';
		$turi2	= $nddofirst;
		$turi3	= $nddolast;
		$turi4	= $iproduct!=''?$iproduct:'0';
		
		$this->load->model('listpenjualanperdo/mclass');
		
		$pagination['base_url'] 	= '/listpenjualanperdo/cform/carilistpenjualanperdonext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/';
		
		if(trim($this->input->post('i_product'))!='') {
			$qlistpenjualanallpage	= $this->mclass->clistpenjualanperdoallpage_itembarang($nddofirst,$nddolast,$iproduct);
		}else{
			$qlistpenjualanallpage	= $this->mclass->clistpenjualanperdoallpage($no_faktur,$nddofirst,$nddolast,$iproduct);
		}
		
		$pagination['total_rows']	= $qlistpenjualanallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if(trim($this->input->post('i_product'))=='') {			
			$qtotalpenjualan	= $this->mclass->clistpenjualanperdototal($no_faktur,$nddofirst,$nddolast,$iproduct);
			
			/*$sqlcek = $this->db->query(" select distinct a.i_faktur_code, a.v_total_faktur from tm_faktur_do_t a, tm_faktur_do_item_t b, tm_do c, tm_do_item d 
							where a.i_faktur = b.i_faktur AND b.i_do = c.i_do AND c.i_do = d.i_do AND b.i_product = d.i_product
							AND c.d_do>='$nddofirst' AND c.d_do <='$nddolast'
							AND a.f_faktur_cancel = 'f'  ORDER BY a.i_faktur_code DESC, a.v_total_faktur ");
			if ($sqlcek->num_rows() > 0){
				$hasil = $sqlcek->result();
				$jumfaktur = 0; 
				foreach ($hasil as $row1) { 
					$v_total_faktur = $row1->v_total_faktur;
					$jumfaktur+=$v_total_faktur;
				}
			}
			echo $jumfaktur; */
			
			if($qtotalpenjualan->num_rows()>0) {
				$rtotalpenjualan			= $qtotalpenjualan->row();
				$data['jmltotalpenjualan']	= $rtotalpenjualan->qty;
				$data['nilaitotalpenjualan']= $rtotalpenjualan->amount;
			}else{
				$data['jmltotalpenjualan']	= 0;
				$data['nilaitotalpenjualan']= 0;
			}
		}else{
			$data['jmltotalpenjualan']	= "";
			$data['nilaitotalpenjualan']= "";
		}
	
		if(trim($this->input->post('i_product'))!='') {
			$data['isi']	= $this->mclass->clistpenjualanperdo_itembarang($pagination['per_page'],$pagination['cur_page'],$nddofirst,$nddolast,$iproduct);
			$data['template']	= 0;
		}else{
			$data['isi']	= $this->mclass->clistpenjualanperdo($pagination['per_page'],$pagination['cur_page'],$no_faktur,$nddofirst,$nddolast,$iproduct);
			$data['template']	= 1;
		}
		
		$this->load->view('listpenjualanperdo/vlistform',$data);	
	}

	function carilistpenjualanperdonext() {
		
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_mulai']	= $this->lang->line('list_penjperdo_tgl_mulai');
		$data['list_penjperdo_no_do']	= $this->lang->line('list_penjperdo_no_do');
		$data['list_penjperdo_nm_brg']	= $this->lang->line('list_penjperdo_nm_brg');
		$data['list_penjperdo_qty']	= $this->lang->line('list_penjperdo_qty');
		$data['list_penjperdo_hjp']	= $this->lang->line('list_penjperdo_hjp');
		$data['list_penjperdo_amount']	= $this->lang->line('list_penjperdo_amount');
		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		$no_faktur	= $this->uri->segment(4); // Y-m-d
		$ddofirst	= $this->uri->segment(5);
		$ddolast	= $this->uri->segment(6);
		$iproduct	= $this->uri->segment(7);
		
		$e_d_do_first	= ($ddofirst!='0')?explode("-",$ddofirst,strlen($ddofirst)):'';
		$e_d_do_last	= ($ddolast!='0')?explode("-",$ddolast,strlen($ddolast)):'';

		$nddofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:'0';
		$nddolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:'0';
		
		$data['tgldomulai']	= (!empty($nddofirst) && $nddofirst!='0')?$nddofirst:'';
		$data['tgldoakhir']	= (!empty($nddolast) && $nddolast!='0')?$nddolast:'';
		$data['nofaktur']	= (!empty($no_faktur) && $no_faktur!='0')?$no_faktur:'';
		$data['iproduct']	= (!empty($iproduct) && $iproduct!='0')?$iproduct:'';
		
		$this->load->model('listpenjualanperdo/mclass');
		
		$pagination['base_url'] 	= '/listpenjualanperdo/cform/carilistpenjualanperdonext/'.$no_faktur.'/'.$ddofirst.'/'.$ddolast.'/'.$iproduct.'/';

		if($iproduct!='0'){
			$qlistpenjualanallpage	= $this->mclass->clistpenjualanperdoallpage_itembarang($ddofirst,$ddolast,$iproduct);
		}else{
			$qlistpenjualanallpage	= $this->mclass->clistpenjualanperdoallpage($no_faktur,$ddofirst,$ddolast,$iproduct);
		}
				
		$pagination['total_rows']	= $qlistpenjualanallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($iproduct=='0') {	
			$qtotalpenjualan	= $this->mclass->clistpenjualanperdototal($no_faktur,$ddofirst,$ddolast,$iproduct);
			
			if($qtotalpenjualan->num_rows()>0) {
				$rtotalpenjualan	= $qtotalpenjualan->row();
				$data['jmltotalpenjualan']	= $rtotalpenjualan->qty;
				$data['nilaitotalpenjualan']	= $rtotalpenjualan->amount;
			}else{
				$data['jmltotalpenjualan']	= 0;
				$data['nilaitotalpenjualan']	= 0;
			}
		}else{
			$data['jmltotalpenjualan']	= "";
			$data['nilaitotalpenjualan']	= "";			
		}
		
		if($iproduct!='0') {
			$data['isi']	= $this->mclass->clistpenjualanperdo_itembarang($pagination['per_page'],$pagination['cur_page'],$ddofirst,$ddolast,$iproduct);
			$data['template']	= 0;
		}else{
			$data['isi']	= $this->mclass->clistpenjualanperdo($pagination['per_page'],$pagination['cur_page'],$no_faktur,$ddofirst,$ddolast,$iproduct);
			$data['template']	= 1;
		}
				
		$this->load->view('listpenjualanperdo/vlistform',$data);	
	}
	
	function listbarangjadi() {
		
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanperdo/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpenjualanperdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('listpenjualanperdo/vlistformbrgjadi',$data);			
	}

	function listbarangjadinext() {
		
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanperdo/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpenjualanperdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('listpenjualanperdo/vlistformbrgjadi',$data);			
	}

	function flistbarangjadi() {
		
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');

		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('listpenjualanperdo/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				$ifakturcode	= trim($row->ifakturcode);
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	

	function listdo() {
		
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);

		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;

		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanperdo/mclass');

		$query	= $this->mclass->ldo($ibranch);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listpenjualanperdo/cform/listdonext/'.$iterasi.'/'.$ibranch.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ldoperpages($ibranch,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listpenjualanperdo/vlistdo',$data);
	}

	function listdonext() {
		
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanperdo/mclass');

		$query	= $this->mclass->lbarangjadi($ibranch);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpenjualanperdo/cform/listdonext/'.$iterasi.'/'.$ibranch.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']		= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ldoperpages($ibranch,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listpenjualanperdo/vlistdo',$data);
	}

	function flistdo() {
		
		$key2	= $this->input->post('key');
		$ibranch	= $this->input->post('cab');

		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanperdo/mclass');

		$query	= $this->mclass->fldo($key2,$ibranch);
		$jml	= $query->num_rows();
		
		$list	= "";
		$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
		
		if($jml>0) {
			$cc	= 1;
			$tanggal	= array();
			
			foreach($query->result() as $row){

				$tgl	= (!empty($row->ddo) || strlen($row->ddo)!=0)?@explode("-",$row->ddo,strlen($row->ddo)):"";
				$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
				$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->ido_code','$row->iproduct','$row->motifname','$row->qty','$row->price','$row->nilai')\">".$row->ido_code."</a></td>	 
				  <td width=\"130px;\"><a href=\"javascript:settextfield('$row->ido_code','$row->iproduct','$row->motifname','$row->qty','$row->price','$row->nilai')\">".$tanggal[$cc]."</a></td>
				   <td><a href=\"javascript:settextfield('$row->ido_code','$row->iproduct','$row->motifname','$row->qty','$row->price','$row->nilai')\">".$row->iproduct."</a></td>
				  <td><a href=\"javascript:settextfield('$row->ido_code','$row->iproduct','$row->motifname','$row->qty','$row->price','$row->nilai')\">".$row->motifname."</a></td>	
				  <td><a href=\"javascript:settextfield('$row->ido_code','$row->iproduct','$row->motifname','$row->qty','$row->price','$row->nilai')\">".$row->qty."</a></td>				  
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}

	function edit() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('ses_level') && 
			$this->session->userdata('isession')!=0) 
		{
			$data['page_title_fpenjualando']	= $this->lang->line('page_title_fpenjualando');
			$data['form_detail_f_fpenjualando']	= $this->lang->line('form_detail_f_fpenjualando');
			$data['form_nomor_f_fpenjualando']	= $this->lang->line('form_nomor_f_fpenjualando');
			$data['form_tgl_f_fpenjualando']	= $this->lang->line('form_tgl_f_fpenjualando');	
			$data['form_cabang_fpenjualando']	= $this->lang->line('form_cabang_fpenjualando');
			$data['form_pilih_cab_fpenjualando']	= $this->lang->line('form_pilih_cab_fpenjualando');
			$data['form_nomor_do_f_fpenjualando']	= $this->lang->line('form_nomor_do_f_fpenjualando');
			$data['form_tgl_do_f_fpenjualando']	= $this->lang->line('form_tgl_do_f_fpenjualando');
			$data['form_kd_brg_fpenjualando']	= $this->lang->line('form_kd_brg_fpenjualando');
			$data['form_nm_brg_fpenjualando']	= $this->lang->line('form_nm_brg_fpenjualando');
			$data['form_hjp_fpenjualando']	= $this->lang->line('form_hjp_fpenjualando');
			$data['form_qty_fpenjualando']	= $this->lang->line('form_qty_fpenjualando');
			$data['form_nilai_fpenjualando']	= $this->lang->line('form_nilai_fpenjualando');
			$data['form_tgl_jtempo_fpenjualando']	= $this->lang->line('form_tgl_jtempo_fpenjualando');
			$data['form_tnilai_fpenjualando']	= $this->lang->line('form_tnilai_fpenjualando');
			$data['form_diskon_fpenjualando']	= $this->lang->line('form_diskon_fpenjualando');
			$data['form_total_fpenjualando']	= $this->lang->line('form_total_fpenjualando');
			$data['form_no_fpajak_fpenjualando']	= $this->lang->line('form_no_fpajak_fpenjualando');
			$data['form_tgl_fpajak_fpenjualando']	= $this->lang->line('form_tgl_fpajak_fpenjualando');
			$data['form_ppn_fpenjualando']	= $this->lang->line('form_ppn_fpenjualando');
			$data['form_ket_cetak_fpenjualando']	= $this->lang->line('form_ket_cetak_fpenjualando');
			$data['form_grand_t_fpenjualando']	= $this->lang->line('form_grand_t_fpenjualando');
			$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
			$data['form_dlm_fpenjualando']	= $this->lang->line('form_dlm_fpenjualando');
			
			$data['button_update']	= $this->lang->line('button_update');
			$data['button_batal']	= $this->lang->line('button_batal');
			
			$data['disabled']	= 'f';
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$tahun	= date("Y");		
			$data['tjthtempo']	= "";
			
			$ifakturcode	= $this->uri->segment(4);
			$ifaktur		= $this->uri->segment(5);
			$data['ifakturcode']	= $ifakturcode;
			
			$this->load->model('listpenjualanperdo/mclass');
			
			$qryfakheader	= $this->mclass->getfakheader($ifaktur);
			
			if($qryfakheader->num_rows()>0) {
				$row_fakheader	= $qryfakheader->row();
				//$i_faktur_code	= $row_fakheader->i_faktur_code;
				$data['i_faktur']		= $row_fakheader->i_faktur;
				//$data['i_faktur_code']	= $i_faktur_code;
				$data['i_faktur_pajak']	= $row_fakheader->i_faktur_pajak;
				$tglfaktur	= explode("-",$row_fakheader->d_faktur,strlen($row_fakheader->d_faktur)); // YYYY-mm-dd
				$tjthtempo	= explode("-",$row_fakheader->d_due_date,strlen($row_fakheader->d_due_date)); // YYYY-mm-dd
				$tgPajak	= explode("-",$row_fakheader->d_pajak,strlen($row_fakheader->d_pajak)); // YYYY-mm-dd
				$data['f_printed']	= $row_fakheader->f_printed=='t'?'checked':'';
				$data['tgFAKTUR']	= $tglfaktur[2]."/".$tglfaktur[1]."/".$tglfaktur[0];
				$data['tjthtempo']	= $tjthtempo[2]."/".$tjthtempo[1]."/".$tjthtempo[0];
				$data['tgPajak']	= $tgPajak[2]."/".$tgPajak[1]."/".$tgPajak[0];
				$data['n_discount']	= $row_fakheader->n_discount;
				$data['v_discount']	= $row_fakheader->v_discount;
				$ibranchname		= $row_fakheader->e_branch_name;		
				$data['ibranchname']	= $ibranchname;
				
				$qicustomer	= $this->mclass->geticustomer($ibranchname);
				
				if($qicustomer->num_rows()>0) {
					$row_icustomer	= $qicustomer->row();
					
					$icustomer	= $row_icustomer->i_customer;
					$ibranch	= $row_icustomer->i_branch;
					
					$data['icustomer']	= $icustomer;
					$data['ibranch']	= $ibranch;
				}
				
				$data['fakturitem']	= $this->mclass->lfakturitem2($ifakturcode);
				$data['opt_cabang']	= $this->mclass->lcabang($icustomer);
									
			} else {
				$data['ifakturcode']	= "";
				$data['icustomer']		= "";
				$data['ibranch']		= "";
				$data['i_faktur']		= "";
				//$data['i_faktur_code']	= "";
				$data['tgFAKTUR']		= "";
				$data['fakturitem']		= "";
				$data['opt_cabang']		= "";
			}					
			$this->load->view('listpenjualanperdo/veditform',$data);
		}	
	}	
	
	function actedit() {
		
		$data['page_title_fpenjualando']	= $this->lang->line('page_title_fpenjualando');
		$data['form_detail_f_fpenjualando']	= $this->lang->line('form_detail_f_fpenjualando');
		$data['form_nomor_f_fpenjualando']	= $this->lang->line('form_nomor_f_fpenjualando');
		$data['form_tgl_f_fpenjualando']	= $this->lang->line('form_tgl_f_fpenjualando');	
		$data['form_cabang_fpenjualando']	= $this->lang->line('form_cabang_fpenjualando');
		$data['form_pilih_cab_fpenjualando']	= $this->lang->line('form_pilih_cab_fpenjualando');
		$data['form_nomor_do_f_fpenjualando']	= $this->lang->line('form_nomor_do_f_fpenjualando');
		$data['form_kd_brg_fpenjualando']	= $this->lang->line('form_kd_brg_fpenjualando');
		$data['form_nm_brg_fpenjualando']	= $this->lang->line('form_nm_brg_fpenjualando');
		$data['form_hjp_fpenjualando']	= $this->lang->line('form_hjp_fpenjualando');
		$data['form_qty_fpenjualando']	= $this->lang->line('form_qty_fpenjualando');
		$data['form_nilai_fpenjualando']	= $this->lang->line('form_nilai_fpenjualando');
		$data['form_tgl_jtempo_fpenjualando']	= $this->lang->line('form_tgl_jtempo_fpenjualando');
		$data['form_tnilai_fpenjualando']	= $this->lang->line('form_tnilai_fpenjualando');
		$data['form_diskon_fpenjualando']	= $this->lang->line('form_diskon_fpenjualando');
		$data['form_total_fpenjualando']	= $this->lang->line('form_total_fpenjualando');
		$data['form_no_fpajak_fpenjualando']	= $this->lang->line('form_no_fpajak_fpenjualando');
		$data['form_tgl_fpajak_fpenjualando']	= $this->lang->line('form_tgl_fpajak_fpenjualando');
		$data['form_ppn_fpenjualando']	= $this->lang->line('form_ppn_fpenjualando');
		$data['form_ket_cetak_fpenjualando']	= $this->lang->line('form_ket_cetak_fpenjualando');
		$data['form_grand_t_fpenjualando']	= $this->lang->line('form_grand_t_fpenjualando');
		$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
		$data['form_dlm_fpenjualando']	= $this->lang->line('form_dlm_fpenjualando');
		
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
		$data['limages']	= base_url();
		
		$this->load->model('listpenjualanperdo/mclass');
		
		/*
		
		$ifakturhiden	= $this->input->post('ifakturhiden')?$this->input->post('ifakturhiden'):$this->input->get_post('ifakturhiden');
		$ifakturcodehiden	= $this->input->post('ifakturcodehiden')?$this->input->post('ifakturcodehiden'):$this->input->get_post('ifakturcodehiden');
		$ifakturcode	= $this->input->post('i_faktur_code')?$this->input->post('i_faktur_code'):$this->input->get_post('i_faktur_code');
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$d_faktur	= $this->input->post('d_faktur')?$this->input->post('d_faktur'):$this->input->get_post('d_faktur');
		$i_branch	= $this->input->post('i_branch')?$this->input->post('i_branch'):$this->input->get_post('i_branch');
		$v_total_nilai	= $this->input->post('v_total_nilai')?$this->input->post('v_total_nilai'):$this->input->get_post('v_total_nilai');
		$n_discount	= $this->input->post('n_discount')?$this->input->post('n_discount'):$this->input->get_post('n_discount');
		$v_discount	= $this->input->post('v_discount')?$this->input->post('v_discount'):$this->input->get_post('v_discount');
		$d_due_date	= $this->input->post('d_due_date')?$this->input->post('d_due_date'):$this->input->get_post('d_due_date');
		$v_total_faktur	= $this->input->post('v_total_faktur')?$this->input->post('v_total_faktur'):$this->input->get_post('v_total_faktur');
		$i_faktur_pajak	= $this->input->post('i_faktur_pajak')?$this->input->post('i_faktur_pajak'):$this->input->get_post('i_faktur_pajak');
		$d_pajak	= $this->input->post('d_pajak')?$this->input->post('d_pajak'):$this->input->get_post('d_pajak');
		$n_ppn	= $this->input->post('n_ppn')?$this->input->post('n_ppn'):$this->input->get_post('n_ppn');
		$f_printed	= $this->input->post('f_cetak')?$this->input->post('f_cetak'):$this->input->get_post('f_cetak');
		$v_total_fppn	= $this->input->post('v_total_fppn')?$this->input->post('v_total_fppn'):$this->input->get_post('v_total_fppn');
		
		*/ 

		$ifakturhiden	= $this->input->post('ifakturhiden');
		$ifakturcodehiden	= $this->input->post('ifakturcodehiden');
		$ifakturcode	= $this->input->post('i_faktur_code');
		$iteration	= $this->input->post('iteration');
		$d_faktur	= $this->input->post('d_faktur');
		$i_branch	= $this->input->post('i_branch');
		$v_total_nilai	= $this->input->post('v_total_nilai');
		$n_discount	= $this->input->post('n_discount');
		$v_discount	= $this->input->post('v_discount');
		$d_due_date	= $this->input->post('d_due_date');
		$v_total_faktur	= $this->input->post('v_total_faktur');
		$i_faktur_pajak	= $this->input->post('i_faktur_pajak');
		$d_pajak	= $this->input->post('d_pajak');
		$n_ppn	= $this->input->post('n_ppn');
		$f_printed	= $this->input->post('f_cetak');
		$v_total_fppn	= $this->input->post('v_total_fppn');
		
		$fprinted	= $f_printed=='1'?'TRUE':'FALSE';
		
		$ex_d_pajak		= explode("/",$d_pajak,strlen($d_pajak)); // dd/mm/YYYY
		$ex_d_due_date	= explode("/",$d_due_date,strlen($d_due_date));
		$ex_d_faktur	= explode("/",$d_faktur,strlen($d_faktur));
		
		$nw_d_pajak	= $ex_d_pajak[2]."-".$ex_d_pajak[1]."-".$ex_d_pajak[0]; //YYYY-mm-dd
		$nw_d_due_date	= $ex_d_due_date[2]."-".$ex_d_due_date[1]."-".$ex_d_due_date[0]; //YYYY-mm-dd
		$nw_d_faktur	= $ex_d_faktur[2]."-".$ex_d_faktur[1]."-".$ex_d_faktur[0]; //YYYY-mm-dd
		
		$ex_v_total_faktur	= explode(".",$v_total_faktur,strlen($v_total_faktur));
		$ex_v_total_fppn	= explode(".",$v_total_fppn,strlen($v_total_fppn));
		$ex_v_discount	= explode(".",$v_discount,strlen($v_discount));
		
		$nw_v_total_faktur	= $ex_v_total_faktur[0];
		$nw_v_total_fppn	= $ex_v_total_fppn[0];
		$nw_v_discount		= $ex_v_discount[0];
		
		$i_do	= array();
		$i_product	= array();
		$e_product_name	= array();
		$v_hjp	= array();
		$n_quantity	= array();
		$v_unit_price	= array();
		
		$i_do_0	= $this->input->post('i_do'.'_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iteration;$cacah++) {
			
			/*
			$i_do[$cacah]	= $this->input->post('i_do'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('i_do'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('i_do'.'_'.'tblItem'.'_'.$cacah);
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_hjp[$cacah]	= $this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('v_hjp'.'_'.'tblItem'.'_'.$cacah);
			$n_quantity[$cacah]	= $this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('n_quantity'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
			*/ 
			
			$i_do[$cacah]	= $this->input->post('i_do'.'_'.'tblItem'.'_'.$cacah);
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_hjp[$cacah]	= $this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah);
			$n_quantity[$cacah]	= $this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
		}
		
		if(!empty($ifakturcode) &&
		   !empty($d_faktur) && 
		   !empty($i_branch)
		) {
			if(!empty($i_do_0)) {
				$qnsop	= $this->mclass->cari_fpenjualan($ifakturcode,$ifakturcodehiden);
				if($qnsop->num_rows()==0 || $qnsop->num_rows()<0) {
					$this->mclass->mupdate($ifakturhiden,$nw_d_faktur,$i_branch,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$fprinted,$nw_v_total_fppn,$i_do,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$ifakturcode);
				} else {
					print "<script>alert(\"Maaf, Nomor Faktur tsb sudah ada. Terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
				}
			}else{
				print "<script>alert(\"Maaf, item Faktur Penjulan hrs terisi. Terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
			}
			
		}else{
			
			$data['page_title_fpenjualando']	= $this->lang->line('page_title_fpenjualando');
			$data['form_detail_f_fpenjualando']	= $this->lang->line('form_detail_f_fpenjualando');
			$data['form_nomor_f_fpenjualando']	= $this->lang->line('form_nomor_f_fpenjualando');
			$data['form_tgl_f_fpenjualando']	= $this->lang->line('form_tgl_f_fpenjualando');	
			$data['form_cabang_fpenjualando']	= $this->lang->line('form_cabang_fpenjualando');
			$data['form_pilih_cab_fpenjualando']	= $this->lang->line('form_pilih_cab_fpenjualando');
			$data['form_nomor_do_f_fpenjualando']	= $this->lang->line('form_nomor_do_f_fpenjualando');
			$data['form_tgl_do_f_fpenjualando']	= $this->lang->line('form_tgl_do_f_fpenjualando');
			$data['form_kd_brg_fpenjualando']	= $this->lang->line('form_kd_brg_fpenjualando');
			$data['form_nm_brg_fpenjualando']	= $this->lang->line('form_nm_brg_fpenjualando');
			$data['form_hjp_fpenjualando']	= $this->lang->line('form_hjp_fpenjualando');
			$data['form_qty_fpenjualando']	= $this->lang->line('form_qty_fpenjualando');
			$data['form_nilai_fpenjualando']	= $this->lang->line('form_nilai_fpenjualando');
			$data['form_tgl_jtempo_fpenjualando']	= $this->lang->line('form_tgl_jtempo_fpenjualando');
			$data['form_tnilai_fpenjualando']	= $this->lang->line('form_tnilai_fpenjualando');
			$data['form_diskon_fpenjualando']	= $this->lang->line('form_diskon_fpenjualando');
			$data['form_total_fpenjualando']	= $this->lang->line('form_total_fpenjualando');
			$data['form_no_fpajak_fpenjualando']	= $this->lang->line('form_no_fpajak_fpenjualando');
			$data['form_tgl_fpajak_fpenjualando']	= $this->lang->line('form_tgl_fpajak_fpenjualando');
			$data['form_ppn_fpenjualando']	= $this->lang->line('form_ppn_fpenjualando');
			$data['form_ket_cetak_fpenjualando']	= $this->lang->line('form_ket_cetak_fpenjualando');
			$data['form_grand_t_fpenjualando']	= $this->lang->line('form_grand_t_fpenjualando');
			$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
			$data['form_dlm_fpenjualando']	= $this->lang->line('form_dlm_fpenjualando');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$tahun	= date("Y");		
			$data['tjthtempo']	= "";
	
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
		
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$thn;
			$data['tgPajak']	= $tgl."/".$bln."/".$thn;
						
			$qryth	= $this->mclass->getthnfaktur();
			$qryfaktur	= $this->mclass->getnomorfaktur();		
			
			$data['opt_cabang']	= $this->mclass->lcabang();
	
			if($qryth->num_rows()>0) {
				$th		= $qryth->row_array();
				$thn	= $th['thn'];
			}else{
				$thn	= $tahun;
			}
			
			if($thn==$tahun) {
				
				if($qryfaktur->num_rows() > 0)  {
					
					$row	= $qryfaktur->row_array();
					$faktur		= $row['ifaktur']+1;		
					
					switch(strlen($faktur)) {
						case "1": $nomorfaktur	= "000".$faktur;
						break;
						case "2": $nomorfaktur	= "00".$faktur;
						break;	
						case "3": $nomorfaktur	= "0".$faktur;
						break;
						case "4": $nomorfaktur	= $faktur;
						break;
					}
				}else{
					$nomorfaktur = "0001";
				}
				$nomor	= $tahun.$nomorfaktur;
			}else{
				$nomor	= $tahun."0001";
			}
			
			$data['no']	= $nomor;

			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"fakpenjualan/cform\",\"#content\");</script>";				
			
			/*** $this->load->view('listpenjualanperdo/vmainform',$data); ***/
		}
	}

	function cari_fpenjualan() {
		
		$fpenj	= $this->input->post('fpenj')?$this->input->post('fpenj'):$this->input->get_post('fpenj');
		$fpenjhiden	= $this->input->post('fpenjhiden')?$this->input->post('fpenjhiden'):$this->input->get_post('fpenjhiden');
		
		$this->load->model('listpenjualanperdo/mclass');
		
		$qnsop	= $this->mclass->cari_fpenjualan($fpenj,$fpenjhiden);
		
		if($qnsop->num_rows()>0) {
			//echo "Maaf, No. faktur tdk dpt diubah!";
			echo "Maaf, No. faktur sudah ada!";		
		}
	}	

	function refresh() {
		
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_mulai']	= $this->lang->line('list_penjperdo_tgl_mulai');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['disabled']	= 't';
		
		$ifakturitem	= array();
		$iproduct	= array();
		$vunitprice	= array();
		
		$iproductbase	= array();
		$price	= array();
		
		$inde	= 0;
		
		$qdoitem	= $this->db->query(" SELECT i_faktur_item, i_product, v_unit_price FROM tm_faktur_do_item_t WHERE f_price_refresh='f' ");
		if($qdoitem->num_rows()>0){
			foreach($qdoitem->result() as $row1){
				$ifakturitem[$inde]	= $row1->i_faktur_item;
				$iproduct[$inde]	= $row1->i_product; // i_product_motif
				$vunitprice[$inde]	= $row1->v_unit_price;

				$qitembarang	= $this->db->query(" SELECT a.i_product_base, a.v_unitprice, b.i_product_motif, b.i_product FROM tr_product_base a INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product WHERE b.i_product_motif='$iproduct[$inde]' ORDER BY b.i_product_motif DESC ");
				if($qitembarang->num_rows()>0){
					$row2	= $qitembarang->row();
					$iproductbase[$inde]	= $row2->i_product_base; // i_product_base
					$price[$inde]	= $row2->v_unitprice;
					$this->db->query(" UPDATE tm_faktur_do_item_t SET v_unit_price='$price[$inde]', f_price_refresh='t' WHERE i_faktur_item='$ifakturitem[$inde]' AND i_product='$iproduct[$inde]' AND f_price_refresh='f' ");
				}						
				$inde+=1;
			}
		}	
		$this->load->view('listpenjualanperdo/vmainform',$data);		
	}	
	
	function refreshperitem() {
		
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_mulai']	= $this->lang->line('list_penjperdo_tgl_mulai');
		
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');	
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['disabled']	= 't';
		
		$ifaktur	= $this->uri->segment(4);
		$icustomer	= $this->uri->segment(5);
		
		$ifakturitem	= array();
		$iproduct	= array();
		$vunitprice	= array();
		
		$iproductbase	= array();
		$price	= array();
		
		$inde	= 0;
		
		/*** 02012012
		$qdoitem	= $this->db->query(" SELECT i_faktur_item, i_product, v_unit_price FROM tm_faktur_do_item_t WHERE f_price_refresh='f' AND i_faktur='$ifaktur' ");
		if($qdoitem->num_rows()>0){
			foreach($qdoitem->result() as $row1){
				$ifakturitem[$inde]	= $row1->i_faktur_item;
				$iproduct[$inde]	= $row1->i_product; // i_product_motif
				$vunitprice[$inde]	= $row1->v_unit_price;

				$qitembarang	= $this->db->query(" SELECT a.i_product_base, a.v_unitprice, b.i_product_motif, b.i_product FROM tr_product_base a INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product WHERE b.i_product_motif='$iproduct[$inde]' ORDER BY b.i_product_motif DESC ");
				if($qitembarang->num_rows()>0){
					$row2	= $qitembarang->row();
					$iproductbase[$inde]	= $row2->i_product_base; // i_product_base
					$price[$inde]	= $row2->v_unitprice;
					$this->db->query(" UPDATE tm_faktur_do_item_t SET v_unit_price='$price[$inde]', f_price_refresh='t' WHERE i_faktur_item='$ifakturitem[$inde]' AND i_product='$iproduct[$inde]' AND f_price_refresh='f' ");
				}						
				$inde+=1;
			}
		}
		***/

		$qpenjualan	= $this->db->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur='$ifaktur' AND f_faktur_cancel='f' ");	
		$rpenjualan = $qpenjualan->row();
					
		$qdoitem	= $this->db->query(" SELECT i_faktur_item, i_product, v_unit_price, n_quantity FROM tm_faktur_do_item_t WHERE i_faktur='$ifaktur' ");
		
		if($qdoitem->num_rows()>0) {
			
			$vtotal	= 0;
			foreach($qdoitem->result() as $row1) {
				
				$qitembarang	= $this->db->query(" SELECT * FROM tr_product_price WHERE i_product_motif='$row1->i_product' AND i_customer='$icustomer' AND f_active='t' LIMIT 1 ");
				
				if($qitembarang->num_rows()>0) {
					$row2	= $qitembarang->row();
					
					$price[$inde]	= $row2->v_price;
					
					$this->db->query(" UPDATE tm_faktur_do_item_t SET v_unit_price='$price[$inde]' WHERE i_faktur_item='$row1->i_faktur_item' AND i_product='$row1->i_product' ");
				}else{
					$qitembarang	= $this->db->query(" SELECT * FROM tr_product_price WHERE i_product_motif='$row1->i_product' AND i_customer='0' AND f_active='t' LIMIT 1 ");
					if($qitembarang->num_rows()>0) {
						$row2	= $qitembarang->row();
												
						$price[$inde]	= $row2->v_price;
						
						$this->db->query(" UPDATE tm_faktur_do_item_t SET v_unit_price='$price[$inde]' WHERE i_faktur_item='$row1->i_faktur_item' AND i_product='$row1->i_product' ");
					}
				}

				$amount = ($price[$inde] * $row1->n_quantity);
				$vtotal += $amount;
				
				$inde+=1;
			}

			if($qpenjualan->num_rows()>0) {
				if($rpenjualan->n_discount>0 || $rpenjualan->n_discount!=0) {
					$diskon			= (($vtotal*$rpenjualan->n_discount)/100); // diskon
					$v_total_faktur	= $vtotal-$diskon;
					$nilai_ppn		= (($v_total_faktur*10)/100);
					$v_total_fppn	= $v_total_faktur + $nilai_ppn;
				}else{
					$diskon			= 0; // diskon
					$v_total_faktur	= $vtotal-$diskon;
					$nilai_ppn		= (($v_total_faktur*10)/100);
					$v_total_fppn	= $v_total_faktur + $nilai_ppn;
				}
				
				$this->db->query(" UPDATE tm_faktur_do_t SET v_total_faktur='$v_total_faktur', v_total_fppn='$v_total_fppn' WHERE i_faktur='$rpenjualan->i_faktur' AND f_faktur_cancel='f' ");
			}

		}
				 	
		$this->load->view('listpenjualanperdo/vmainform',$data);			
	}

	function updatefakdo() {
		
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_mulai']	= $this->lang->line('list_penjperdo_tgl_mulai');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['disabled']	= 't';
		
		$ido	= array();
		$iproduct	= array();
		$ndeliver	= array();
		$nresidual	= array();
		$iproduct2	= array();
		$nquantity	= array();
		
		$inde	= 0;

		$qdoitem	= $this->db->query(" SELECT i_do_item, i_do, i_product, n_deliver, f_faktur_created, n_residual FROM tm_do_item WHERE n_residual is not null AND length(e_product_name) > 9 AND f_faktur_created='f' ");

		/*** bad-incorect f_faktur_created
		$qdoitem	= $this->db->query(" SELECT i_do_item, i_do, i_product, n_deliver, f_faktur_created, n_residual FROM tm_do_item WHERE n_residual is not null AND length(e_product_name) > 9 AND f_faktur_created='t' ");
		***/
		
		if($qdoitem->num_rows()>0){
			
			$diupdate	= 0;
			
			foreach($qdoitem->result() as $row1){

				$ido[$inde]	= $row1->i_do;
				$iproduct[$inde]	= $row1->i_product;
				$ndeliver[$inde]	= $row1->n_deliver;
				$nresidual[$inde]	= $row1->n_residual;

				$qitembarang	= $this->db->query(" SELECT i_faktur_item, i_faktur, i_do, n_quantity, i_product FROM tm_faktur_do_item_t WHERE i_do='$ido[$inde]' AND i_product='$iproduct[$inde]' ");
				
				if($qitembarang->num_rows()>0) {
					$row2	= $qitembarang->row();
					
					$nquantity[$inde]	= $row2->n_quantity;
					
					$sisa	= $ndeliver[$inde]-$nquantity[$inde];
					
					if($sisa<0 || $sisa==0){
						$sisanya	= 0;
						$ffakturcreated	= 't';
					}else{
						$sisanya	= $sisa;
						$ffakturcreated	= 'f';
					}
					
					//if($diupdate==0){
						$this->db->query(" UPDATE tm_do SET f_faktur_created='TRUE' WHERE f_do_cancel='f' AND i_do='$ido[$inde]' AND f_faktur_created='f' ");
						$diupdate=1;
					//}
					
					$this->db->query(" UPDATE tm_do_item SET n_residual='$sisanya', f_faktur_created='$ffakturcreated' WHERE i_do_item='$row1->i_do_item' AND i_do='$ido[$inde]' AND i_product='$iproduct[$inde]' AND f_faktur_created='f' ");

				}
				
				$inde+=1;
			}
		}	
		$this->load->view('listpenjualanperdo/vmainform',$data);	
	}	
	
	function undo() {
		
		$i_faktur_code	= $this->uri->segment(4);
		$i_faktur	= $this->uri->segment(5);
		
		$this->load->model('listpenjualanperdo/mclass');
		
		$this->mclass->mbatal($i_faktur,$i_faktur_code);		
	}
}
?>
