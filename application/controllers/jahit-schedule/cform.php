<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('jahit-schedule/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================

	$data['isi'] = 'jahit-schedule/vmainform';
	$data['msg'] = '';
	$data['unit_jahit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);

  }
  
  function edit(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$id_sc 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_schedule_jahit($id_sc);
	$data['msg'] = '';
	$data['isi'] = 'jahit-schedule/veditform';
	$data['unit_jahit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);

  }
  
  function updatedata() {
			$id_sc 	= $this->input->post('id_sc', TRUE);
			$unit_jahit 	= $this->input->post('unit_jahit', TRUE);
			$operator_jahit 	= $this->input->post('operator_jahit', TRUE);  
			/*$pisah1 = explode("-", $tgl_request);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_request = $thn1."-".$bln1."-".$tgl1; */
			
			$no 	= $this->input->post('no', TRUE);
			$tgl = date("Y-m-d");

			//update headernya
			$this->db->query(" UPDATE tt_schedule_jahit SET kode_unit = '$unit_jahit', operator_jahit = '$operator_jahit',
							tgl_update= '$tgl' where id= '$id_sc' ");
			//hapus dulu detailnya
			$this->db->delete('tt_schedule_jahit_detail', array('id_schedule_jahit' => $id_sc));
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{	
						$pisah1 = explode("-", $this->input->post('tgl_'.$i, TRUE));
						$tgl1= $pisah1[0];
						$bln1= $pisah1[1];
						$thn1= $pisah1[2];
						$tgl_jahit = $thn1."-".$bln1."-".$tgl1;
						
						$this->db->query(" INSERT INTO tt_schedule_jahit_detail (id_schedule_jahit, kode_brg_jadi, 
						tgl_jahit, qty, jam_kerja, keterangan) 
						VALUES ('$id_sc', '".$this->input->post('kode_'.$i, TRUE)."', '".$tgl_jahit."', 
						'".$this->input->post('qty_'.$i, TRUE)."', 
						'".$this->input->post('jam_'.$i, TRUE)."', 
						'".$this->input->post('keterangan_'.$i, TRUE)."') ");
					}
					redirect('jahit-schedule/cform/view');
				
  }

  function submit(){
		$no_schedule 	= $this->input->post('no_schedule', TRUE);
		$unit_jahit 	= $this->input->post('unit_jahit', TRUE);  
		$operator_jahit 	= $this->input->post('operator_jahit', TRUE);  
	/*	$pisah1 = explode("-", $tgl_request);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_request = $thn1."-".$bln1."-".$tgl1; */
					
		$no 	= $this->input->post('no', TRUE);
			
			$jumlah_input=$no-1;
					
			for ($i=1;$i<=$jumlah_input;$i++)
			{
				$this->mmaster->save($no_schedule,$unit_jahit, $operator_jahit,
					$this->input->post('tgl_'.$i, TRUE), $this->input->post('kode_'.$i, TRUE),
					$this->input->post('nama_'.$i, TRUE), $this->input->post('qty_'.$i, TRUE), 
					$this->input->post('jam_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE) );
			}
			redirect('jahit-schedule/cform/view');

  }
  
  function view(){
    $data['isi'] = 'jahit-schedule/vformview';
    $date_from = "all";
    $date_to = "all";
    $unit_jahit = '0';
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	
    $jum_total = $this->mmaster->getAlltanpalimit($date_from, $date_to, $unit_jahit);
							$config['base_url'] = base_url().'index.php/jahit-schedule/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '5';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $date_from, $date_to, $unit_jahit);	
	
	$data['jum_total'] = count($jum_total);
	$data['date_from'] = '';
	$data['date_to'] = '';
	$data['unit_jahit'] = $unit_jahit;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$unit_jahit 	= $this->input->post('unit_jahit', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE);  
	$date_to 	= $this->input->post('date_to', TRUE);  
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
    
    if ($unit_jahit == '') {
		$unit_jahit 	= $this->uri->segment(4);
	}
	if ($date_from == '') {
		$date_from 	= $this->uri->segment(5);
	}
	if ($date_to == '') {
		$date_to 	= $this->uri->segment(6);
	}
	
	if ($unit_jahit == '') {
		$unit_jahit 	= '0';
	}
	if ($date_from == '') {
		$date_from 	= "all";
	}
	if ($date_to == '') {
		$date_to 	= "all";
	}
    $jum_total = $this->mmaster->getAlltanpalimit($date_from, $date_to, $unit_jahit);
					$config['base_url'] = base_url().'index.php/jahit-schedule/cform/cari/'.$unit_jahit.'/'.$date_from.'/'.$date_to.'/';
					$config['total_rows'] = count($jum_total); 
					$config['per_page'] = '5';
					$config['first_link'] = 'Awal';
					$config['last_link'] = 'Akhir';
					$config['next_link'] = 'Selanjutnya';
					$config['prev_link'] = 'Sebelumnya';
					$config['cur_page'] = $this->uri->segment(5);
					$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $date_from, $date_to, $unit_jahit);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'jahit-schedule/vformview';
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['unit_jahit'] = $unit_jahit;
	
	$this->load->view('template',$data);
  }

  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================

	$posisi 	= $this->uri->segment(4);
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || $posisi == '' ) {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari);

				$config['base_url'] = base_url()."index.php/jahit-schedule/cform/show_popup_brg/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('jahit-schedule/vpopupbrg',$data);
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('jahit-schedule/cform/view');
  }
  
  function generate_nomor() {
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
}
