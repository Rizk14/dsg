<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('listopperitembrg/mclass');
	}
	
	function index() {
		
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}	
			$data['disabled']	= 'f';
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$data['customer']	= $this->mclass->lcustomer();
			$data['isi']	= 'listopperitembrg/vmainform';
			$this->load->view('template',$data);
			
			
	}
	
	function view() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$i_product	= $this->input->post('i_product');
		$d_op_first	= $this->input->post('d_op_first');
		$d_op_last	= $this->input->post('d_op_last');
		
		$data['tglopmulai']	= $d_op_first;
		$data['tglopakhir']	= $d_op_last;
		$data['kproduksi']	= $i_product;
		
		$e_d_do_first	= explode("/",$d_op_first,strlen($d_op_first));
		$e_d_do_last	= explode("/",$d_op_last,strlen($d_op_last));
		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";

		$data['var_iproduct']	= $i_product;
		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;
		
		//$data['customer']	= $this->mclass->lcustomer();
		
		$data['query']	= $this->mclass->get_op_peritem($i_product,$n_d_do_first,$n_d_do_last);
			$data['isi']	= 'listopperitembrg/vlistform';
			$this->load->view('template',$data);
	
	}
	
	function listbarangjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listopvsdo_testing/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('listopvsdo_testing/vlistformbrgjadi',$data);	
	}

	function listbarangjadinext() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listopvsdo_testing/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('listopvsdo_testing/vlistformbrgjadi',$data);	
	}	
	
	function flistbarangjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		if(!empty($key)) {
			$query	= $this->mclass->flbarangjadi($key);
			$jml	= $query->num_rows();
		} else {
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->iproduct."</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->imotif."</a></td>
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->motifname."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}
	
	function refreshpemenuhan() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_opvsdo']	= $this->lang->line('page_title_opvsdo');
		$data['form_title_detail_opvsdo']	= $this->lang->line('form_title_detail_opvsdo');
		$data['list_opvsdo_tgl_mulai_op']	= $this->lang->line('list_opvsdo_tgl_mulai_op');
		$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
		$data['list_opvsdo_kd_brg']	= $this->lang->line('list_opvsdo_kd_brg');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');
		$data['disabled']	= 't';
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$jml=1;
		
		$qorder	= $this->db->query(" SELECT * FROM tm_op WHERE f_op_cancel='f' AND f_op_close='f' AND f_do_created='t' AND (i_customer!='0' AND i_customer!='') ");
		if($qorder->num_rows()>0){
			foreach($qorder->result() as $rorder){
				$qorderitem	= $this->db->query(" SELECT i_op_item, i_op, i_product, n_count, n_residual, f_do_created FROM tm_op_item WHERE i_op='$rorder->i_op' AND length(e_product_name) > 9 AND length(i_product) > 8 AND f_koreksi_order='f' ORDER BY i_op_item ASC ");
	
				if($qorderitem->num_rows()>0){
					foreach($qorderitem->result() as $row){

						$qdeliveritem	= $this->db->query(" SELECT i_do, i_op, i_product, n_deliver FROM tm_do_item WHERE i_op='$rorder->i_op' AND i_product='$row->i_product' AND length(e_product_name) > 9 AND length(cast(i_op AS character varying)) > 6 ORDER BY i_do_item ASC ");
						
						$sisa=0;
						$antar=0;
						
						foreach($qdeliveritem->result() as $rdeliveritem){
							$antar	= $antar+$rdeliveritem->n_deliver;
						}
						
						$sisa	= (($row->n_count)-$antar);
						if($sisa<0 || $sisa==0){
							$status	= 't';
						}else{
							$status	= 'f';
						}						
						
						$this->db->query(" UPDATE tm_op_item SET n_residual='$sisa', f_do_created='$status', f_koreksi_order='t' WHERE i_op_item='$row->i_op_item' AND f_koreksi_order='f' ");
						$jml+=1;
													
					}
				}
			}	
		}
		
		print "<script>alert(\"Jml Order terupdate: '\"+$jml+\"' \");show(\"listopvsdo/cform\",\"#content\");</script>";

	}
}
?>
