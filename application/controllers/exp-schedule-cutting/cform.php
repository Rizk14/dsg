<?php
class Cform extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('exp-schedule-cutting/mmaster');
  }

  function index(){
	  
	$proses_submit	= $this->input->post('submit', TRUE); 
	$tgl	= $this->input->post('tgl'); // d-m-Y
	
	if ($proses_submit=='Proses' && $tgl!=''){
		
		$exp_tgl	= explode("-",$tgl,strlen($tgl));
		$tgl_new	= $exp_tgl[2]."-".$exp_tgl[1]."-".$exp_tgl[0];
		
		$data['tgl']= $tgl;
		
		$data['detail'] = $this->mmaster->detail_realisasi($tgl_new);
		$data['msg']	= '';
		$data['go_proses'] = '1';
	}else{
		$data['msg'] = '';
		$data['go_proses'] = '';
	}
	
	$data['isi'] = 'exp-schedule-cutting/vmainform';
	$this->load->view('template',$data);
  }
  
  
  function export(){
	  
	$tglrealisasi	= $this->input->post('tglrealisasi');

	$exp_tgl	= explode("-",$tglrealisasi,strlen($tglrealisasi));
	$tgl_new	= $exp_tgl[2]."-".$exp_tgl[1]."-".$exp_tgl[0];

	/* Excel */
	$ObjPHPExcel = new PHPExcel();
	
	$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
	$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
	$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
	$ObjPHPExcel->getProperties()
		->setTitle("Laporan Realisasi Cutting")
		->setSubject("Realisasi Cutting")
		->setDescription("Laporan Realisasi Cutting per-tanggal")
		->setKeywords("Laporan")
		->setCategory("Laporan");

	$ObjPHPExcel->setActiveSheetIndex(0);

	$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
	$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
	$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
	$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
	$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
	$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
	$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
	
	$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'No');
	$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'Operator');
	$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Realisasi');
	$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'Kode Barang');
	$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'Kode Barang Jadi');
	$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Nama Barang Jadi');
	$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'Hasil');
	$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'JK');

  	$qrealisasi	= $this->db->query(" SELECT a.operator_cutting FROM tt_realisasi_cutting_detail a
INNER JOIN tt_realisasi_cutting b ON b.id=a.id_realisasi_cutting WHERE b.tgl_realisasi='$tgl_new' GROUP BY a.operator_cutting  ");

	$data_realisasi	= array();
	$data_realisasi_detail = array();
	
	//$indek1 = 0;
	//$indek2 = 0;
	
	if($qrealisasi->num_rows()>0){
		$result	= $qrealisasi->result();
		foreach($result as $row){
			
			$qrealisasi_detail	= $this->db->query(" SELECT b.no_realisasi, a.kode_brg_jadi, a.kode_brg 
								FROM tt_realisasi_cutting_detail a INNER JOIN tt_realisasi_cutting b ON b.id=a.id_realisasi_cutting 
								WHERE a.operator_cutting='$row->operator_cutting' AND b.tgl_realisasi = '$tgl_new' 
								GROUP BY b.no_realisasi, a.kode_brg_jadi, a.kode_brg ORDER BY a.kode_brg_jadi DESC ");
			if($qrealisasi_detail->num_rows()>0){
				
				$result2 = $qrealisasi_detail->result();
				
				foreach($result2 as $row2){
					
					$qnmbrgjd	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
					$rnmbrgjd	= $qnmbrgjd->row();
					
					$kodebrgjadi = $row2->kode_brg_jadi;
					$nmbrgjadi	 = $rnmbrgjd->e_product_motifname;
					$kodebrg	 = $row2->kode_brg;
					$norealisasi = $row2->no_realisasi;
					
					$qjk	= $this->db->query(" SELECT jam_mulai, jam_selesai FROM tt_realisasi_cutting_detail WHERE kode_brg_jadi='$kodebrgjadi' AND kode_brg='$kodebrg' ");
					if($qjk->num_rows()>0){
						
						$total = 0;
						$result3 = $qjk->result();
						
						foreach($result3 as $row3){
							
						  list($h,$m,$s) = explode(":",$row3->jam_mulai);
						  $dtawal = mktime($h,$m,$s,"1","1","1");
						  list($h,$m,$s) = explode(":",$row3->jam_selesai);
						  $dtakhir = mktime($h,$m,$s,"1","1","1");
						  $dtselisih = $dtakhir-$dtawal;
						  $total = $total+$dtselisih;
						}

						$totalmenit = $total/60;
						$jam = explode(".",$totalmenit/60);
						$sisamenit  = ($totalmenit/60)-$jam[0];
						$sisamenit2 = $sisamenit*60;	
						if($sisamenit2==0)
							$sisamenit2 = '00';
						$jk	= $jam[0].".".$sisamenit2;
					}else{
						$jk	= 0;
					}
					
					$qhslrealisasi	= $this->db->query(" SELECT SUM(hasil_potong) AS hsl FROM tt_realisasi_cutting_detail WHERE kode_brg_jadi='$kodebrgjadi' AND kode_brg='$kodebrg' ");
					$rhslrealisasi	= $qhslrealisasi->row();
					$hsl	= str_replace(".",",",$rhslrealisasi->hsl);
					$data_realisasi_detail[] = array(
						'no_realisasi' => $norealisasi,
						'kode_brg_jadi' => $kodebrgjadi,
						'nama_brg_jadi' => $nmbrgjadi,
						'kode_brg' => $kodebrg,
						'hasil' => $hsl,
						'jk' => $jk);
				//	$indek2+=1;
				}
			}
			
			$data_realisasi[]	= array(
				'operator_cutting' => $row->operator_cutting,
				'detail' => $data_realisasi_detail);
			$data_realisasi_detail = array();
						
			//$indek1++;
		}
	}
	
	$cel = 2;
	$noarr = 0;
	$jml = 1;
	
	for($k=0; $k<count($data_realisasi); $k++){
		
		$temp_k = '';
		$opt	= $data_realisasi[$k]['operator_cutting'];
		$detail_realisasi = $data_realisasi[$k]['detail'];
		
		for($kk=0;$kk<count($detail_realisasi);$kk++){
			
			if($temp_k==''){
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$cel, $jml);
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$cel)->applyFromArray(array('font' => array('name'	=> 'Arial','bold'  => false,'italic'=> false,'size'  => 10)));
									
				$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$cel, $opt);
				$ObjPHPExcel->getActiveSheet()->getStyle('B'.$cel)->applyFromArray(array('font' => array('name'	=> 'Arial','bold'  => false,'italic'=> false,'size'  => 10)));
									
				$temp_k='isi';
				$jml+=1;
			}
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$cel, $detail_realisasi[$kk]['no_realisasi']);
			$ObjPHPExcel->getActiveSheet()->getStyle('C'.$cel)->applyFromArray(array('font' => array('name'	=> 'Arial','bold'  => false,'italic'=> false,'size'  => 10)));
								
			$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$cel, $detail_realisasi[$kk]['kode_brg']);
			$ObjPHPExcel->getActiveSheet()->getStyle('D'.$cel)->applyFromArray(array('font' => array('name'	=> 'Arial','bold'  => false,'italic'=> false,'size'  => 10)));
							
			$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$cel, $detail_realisasi[$kk]['kode_brg_jadi']);
			$ObjPHPExcel->getActiveSheet()->getStyle('E'.$cel)->applyFromArray(array('font' => array('name'	=> 'Arial','bold'  => false,'italic'=> false,'size'  => 10)));
							
			$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$cel, $detail_realisasi[$kk]['nama_brg_jadi']);
			$ObjPHPExcel->getActiveSheet()->getStyle('F'.$cel)->applyFromArray(array('font' => array('name'	=> 'Arial','bold'  => false,'italic'=> false,'size'  => 10)));
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$cel, $detail_realisasi[$kk]['hasil']);
			$ObjPHPExcel->getActiveSheet()->getStyle('G'.$cel)->applyFromArray(array('font' => array('name'	=> 'Arial','bold'  => false,'italic'=> false,'size'  => 10)));
			
			$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$cel, $detail_realisasi[$kk]['jk'], Cell_DataType::TYPE_STRING);							
			$ObjPHPExcel->getActiveSheet()->getStyle('H'.$cel)->applyFromArray(array('font' => array('name'	=> 'Arial','bold'  => false,'italic'=> false,'size'  => 10)));
			
			/************
			if($tempk==$k){
				$arrcel[$k] = $cel;
			}else{
				$arrcel[$k] = $cel+1;
			}
			************/
			$cel+=1;
		}
		/***********
		$temp_k = '';
		
		if($temp_k!=$k){
			//$arrcel[$noarr] = $cel;
			$arrkk[$noarr] = $kk;
			$noarr+=1;
			$temp_k = $k;
		}
		*************/
	}
	
	$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');

	$files	= "laporan_realisasi"." ".".xls";
	$ObjWriter->save("files/".$files);
											
	$data['tgl']= $tglrealisasi;
	$data['detail'] = $this->mmaster->detail_realisasi($tgl_new);	  
	$data['isi'] = 'exp-schedule-cutting/vformview';
	
	$this->load->view('template',$data);	
  }
  
}
