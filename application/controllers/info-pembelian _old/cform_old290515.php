<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-pembelian/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['isi'] = 'info-pembelian/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-pembelian/vformview';
    $jenis_beli = $this->input->post('jenis_beli', TRUE);
    $supplier = $this->input->post('supplier', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
    $jum_total = $this->mmaster->get_all_pembeliantanpalimit($jenis_beli, $date_from, $date_to, $supplier);
						/*	$config['base_url'] = base_url().'index.php/info-pembelian/cform/view/index/'.$date_from.'/'.$date_to.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						*/
	//$data['query'] = $this->mmaster->get_all_pembelian($config['per_page'],$this->uri->segment(7), $jenis_beli, $date_from, $date_to);						
	$data['query'] = $this->mmaster->get_all_pembelian($jenis_beli, $date_from, $date_to, $supplier);
	$data['jum_total'] = count($jum_total);
	//$data['cari'] = $keywordcari;
	//$data['list_supplier'] = $this->mmaster->get_supplier();
	//$data['csupplier'] = $csupplier;
	
	// ambil data nama supplier
	$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$supplier' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_supplier	= $hasilrow->nama;
	}
	else
		$nama_supplier = '';
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$data['supplier'] = $supplier;
	$data['nama_supplier'] = $nama_supplier;
	$this->load->view('template',$data);
  }
  
  // 12-04-2012
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$supplier = $this->input->post('kode_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_pembelian_for_print($jenis_beli, $date_from, $date_to, $supplier);
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		// 12-04-2012, coba php excel (blm beres, ditunda dulu. sementara pake header, dibikin 2 aja utk excel dan ods)
		
		/* Excel */
	/*	$ObjPHPExcel = new PHPExcel();
		
		$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
		$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2012");
		$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2012");
		
		$ObjPHPExcel->getProperties()
			->setTitle("Laporan Pembelian")
			->setSubject("Laporan Pembelian")
			->setDescription("Laporan Pembelian per bulan")
			->setKeywords("Laporan")
			->setCategory("Laporan");

		$ObjPHPExcel->setActiveSheetIndex(0);

		$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		
		$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'Supplier');
		$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'No SJ');
		$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Tgl SJ');
		$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'List Barang');
		$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'No Perk');
		$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Harga Satuan');
		$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'Qty');
		$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'Satuan');
		$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'Jumlah');
		$ObjPHPExcel->getActiveSheet()->setCellValue('J1', 'Tot Hutang Dagang');
		$ObjPHPExcel->getActiveSheet()->setCellValue('K1', 'PPN');
		$ObjPHPExcel->getActiveSheet()->setCellValue('L1', 'Bahan Baku');
		$ObjPHPExcel->getActiveSheet()->setCellValue('M1', 'Bahan Pembantu');
		*/
		// ===================================================================================================
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='13' align='center'>LAPORAN PEMBELIAN</th>
		 </tr>
		 <tr>
			<th colspan='13' align='center'>Jenis Pembelian: $nama_jenis</th>
		 </tr>
		 <tr>
			<th colspan='13' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='30%'>Supplier</th>
			 <th width='20%'>No SJ</th>
			 <th width='20%'>Tgl SJ</th>
			 <th width='40%'>List Barang</th>
			 <th width='20%'>No Perk</th>
			 <th>Harga Satuan</th>
			 <th>Qty</th>
			 <th>Satuan</th>
			 <th>Jumlah</th>
			 <th>Tot Hutang Dagang</th>
			 <th>PPN</th>
			 <th>Bahan Baku</th>
			 <th>Bahan Pembantu</th>
		 </tr>
		</thead>
		<tbody>";
		if (is_array($query)) {
			$tot_jumlah_detail = 0;
			$tot_hutang = 0;
			$tot_ppn = 0;
			$tot_baku = 0;
			$tot_pembantu = 0;
			$no_sj_temp = "";
			for($j=0;$j<count($query);$j++){
				// hitung jumlah total masing2 field
				if ($no_sj_temp != $query[$j]['no_sj']) {
					$no_sj_temp = $query[$j]['no_sj'];
					$tot_hutang += $query[$j]['jumlah'];
					$tot_ppn += $query[$j]['pajaknya'];
				}

				 $tot_jumlah_detail += $query[$j]['total'];
				 if ($query[$j]['kode_perk'] == "511.100")
					$tot_baku += $query[$j]['total'];
				 else
					$tot_pembantu += $query[$j]['total'];
			} // end header
		}
		else {
			$tot_jumlah_detail = 0;
			$tot_hutang = 0;
			$tot_ppn = 0;
			$tot_baku = 0;
			$tot_pembantu = 0;
		}
		
		if (is_array($query)) {
			$no_sj_temp = "";
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.= "<tr class=\"record\">";
				 //<td width= '50px'>".$query[$j]['nama_supplier']."</td>";
				 if ($no_sj_temp != $query[$j]['no_sj']) {
					 $html_data.= "<td>".$query[$j]['nama_supplier']."</td>";
					 $html_data.="<td>".$query[$j]['no_sj']."</td>
					  <td>".$query[$j]['tgl_sj']."</td>";
				 }
				 else {
					 $html_data.="<td>&nbsp;</td><td>&nbsp;</td>
					 <td>&nbsp;</td>";
				 }
				 
				 $html_data.="<td nowrap>";
				 $html_data.= $query[$j]['nama_brg'];
				 $html_data.= "</td>
				 <td>";
				 $html_data.= $query[$j]['kode_perk'];
				 $html_data.= "</td>
				 <td align='right' nowrap>";
				 $html_data.= number_format($query[$j]['harga'], 2,',','.');
				 
				 $html_data.= "</td>
				 <td align='right'>";
				 $html_data.= $query[$j]['qty'];
				 $html_data.= "</td>
				 <td>";
				 $html_data.= $query[$j]['satuan'];
				 $html_data.= "</td>";
				 $html_data.= "<td align='right'>";
				 $html_data.= number_format($query[$j]['total'],2,',','.');				 
				 $html_data.= "</td>";
				 
				 if ($no_sj_temp != $query[$j]['no_sj']) {
					 $no_sj_temp = $query[$j]['no_sj'];
					$html_data.="<td align='right'>".number_format($query[$j]['jumlah'],2,',','.')."</td>
					 <td align='right'>".number_format($query[$j]['pajaknya'],2,',','.')."</td>";
				 }
				 else {
					 $html_data.="<td>&nbsp;</td>
					 <td>&nbsp;</td>";
				 }
				 				 
				 $html_data.= "<td align='right'>";
				 if ($query[$j]['kode_perk'] == "511.100") 
					$html_data.= number_format($query[$j]['total'],2,',','.');
				else
					$html_data.= "&nbsp;";
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right'>";
				 if ($query[$j]['kode_perk'] == "512.100") 
					$html_data.= number_format($query[$j]['total'],2,',','.');
				else
					$html_data.= "&nbsp;";
				 
				 $html_data.= "</td>";				 
				 $html_data.=  "</tr>";
		 	}
		 }
		 $html_data.= "<tr>
			<td colspan='8' align='right'>TOTAL</td>
			<td align='right'>".number_format($tot_jumlah_detail,2,',','.')." </td>
			<td align='right'>".number_format($tot_hutang,2,',','.')."</td>
			<td align='right'>".number_format($tot_ppn,2,',','.')."</td>
			<td align='right'>".number_format($tot_baku,2,',','.')."</td>
			<td align='right'>".number_format($tot_pembantu,2,',','.')."</td>
		 </tr></tbody>
		</table>";

		$nama_file = "laporan_pembelian";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../dutaproduksi200/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  //03-04-2012, ini utk cek sj pembelian yg tidak sinkron antara total di detail dgn di header
  function cek_sj_nonsinkron(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$data['query'] = $this->mmaster->get_sj_nonsinkron();
		$data['jum_total'] = count($data['query']);
		$data['isi'] = 'info-pembelian/vformviewsjnonsinkron';
		$this->load->view('template',$data);
  }

}
