<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-rekap-pembelian-peritem/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-rekap-pembelian-peritem/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-rekap-pembelian-peritem/vformview';
    $jenis_beli = $this->input->post('jenis_beli', TRUE);
    $jenis_tampilan = $this->input->post('jenis_tampilan', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$acuan = $this->input->post('acuan', TRUE);
	$gudang = $this->input->post('gudang', TRUE);
	
	/*if ($date_from == '' && $date_to == '') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
	} */

   // $jum_total = $this->mmaster->get_all_pembeliantanpalimit($jenis_beli, $date_from, $date_to);
	if ($jenis_tampilan == '1')
		$data['query'] = $this->mmaster->get_all_pembelian($jenis_beli, $acuan, $date_from, $date_to, $gudang);
	else
		$data['query'] = $this->mmaster->get_all_pembelian_bulanan($jenis_beli, $date_from, $date_to);
	//$data['jum_total'] = count($jum_total);
	
	$pisah1 = explode("-", $date_from);
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$pisah2 = explode("-", $date_to);
	$bln2= $pisah2[1];
	$thn2= $pisah2[2];
	
	if ($thn1 == $thn2) {
		$jum_bln = 0;
		for ($xx=$bln1; $xx<=$bln2; $xx++) {
			$jum_bln++;
		}
	}
	else
		$jum_bln = 0;
	
	// 10-11-2015
	$data['id_gudang'] = $gudang;
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
									FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
		$data['nama_gudang'] = "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang;
	}
	else {
		$data['nama_gudang'] = 'Semua';
	}
	
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$data['acuan'] = $acuan;
	$data['jenis_tampilan'] = $jenis_tampilan;
	$data['jum_bln'] = $jum_bln;
	$data['bln1'] = $bln1;
	$data['thn1'] = $thn1;
	$data['bln2'] = $bln2;
	$data['thn2'] = $thn2;
	$this->load->view('template',$data);
  }
  
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$acuan = $this->input->post('acuan', TRUE);
		$jenis_tampilan = $this->input->post('jenis_tampilan', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$id_gudang = $this->input->post('id_gudang', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		// 10-11-2015
		if ($id_gudang != '0') {
			$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
										FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$id_gudang' ");
			$hasilrow = $query3->row();
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
			$nama_lokasi	= $hasilrow->nama_lokasi;
			$nama_gudang = "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang;
		}
		else {
			$nama_gudang = 'Semua';
		}
		
		if ($jenis_tampilan == '1')
			$query = $this->mmaster->get_all_pembelian($jenis_beli, $acuan, $date_from, $date_to, $id_gudang);
		else
			$query = $this->mmaster->get_all_pembelian_bulanan($jenis_beli, $date_from, $date_to);
		
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
			
		$pisah1 = explode("-", $date_from);
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$pisah2 = explode("-", $date_to);
		$bln2= $pisah2[1];
		$thn2= $pisah2[2];
		
		if ($thn1 == $thn2) {
			$jum_bln = 0;
			for ($xx=$bln1; $xx<=$bln2; $xx++) {
				$jum_bln++;
			}
		}
		else
			$jum_bln = 0;
		$jumcolspan = $jum_bln+5;
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//31-08-2012
		if ($jenis_tampilan == '2') {
			$html_data = "<table border='1' cellpadding= '1' cellspacing = '1'"; 
			if ($jum_bln <=5) { $html_data.=" width='80%' > "; } else { $html_data.= " width='100%' >"; }
			
			$html_data.= " <thead>
			<tr>
				<th colspan='$jumcolspan' align='center'> REKAP PENERIMAAN BARANG PER ITEM </th>
			</tr>
			<tr>
				<th colspan='$jumcolspan' align='center'>Jenis Pembelian: $nama_jenis</th>
			</tr>
			<tr>
				<th colspan='$jumcolspan' align='center'>Periode: $date_from s.d $date_to</th>
			</tr>
			
		 <tr class='judulnya'>
			<th rowspan='2'>No</th>
			 <th rowspan='2'>No. Perk</th>
			 <th rowspan='2'>Nama Barang</th>
			 <th rowspan='2'>Satuan</th>
			 <th colspan='$jum_bln'>Total Qty Per Bulan</th>
			 <th rowspan='2'>Total</th>
		 </tr> 
		 <tr class='judulnya'>";
			$gabbln= '';
			for($xx=1; $xx<=$jum_bln; $xx++) {
				if($xx ==1) {
					$html_data.= "<th>".$bln1."/".$thn1."</th>";
					$blnnext = $bln1+1;
				}
				else {
					if ($blnnext<10)
						$gabbln = "0".$blnnext;
					else
						$gabbln = $blnnext;
					$html_data.= "<th>".$gabbln."/".$thn1."</th>";
					$blnnext++;
				}
			}
			$html_data.="</tr>";
		 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $nomor = $j+1;
				 $totalnya = 0;
				 $html_data.= "<tr class=\"record\">
						<td align='center'>".$nomor."</td>
						<td>".$query[$j]['kode_perk']."</td>
						<td>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>
						<td>".$query[$j]['satuan']."</td>";
					
					$var_detail = $query[$j]['detail_bulanan'];
					for($k=0;$k<count($var_detail); $k++){
						 $html_data.= "<td align='right'>". $var_detail[$k]['qty']."</td>";
						 $totalnya+= $var_detail[$k]['qty'];
					}
					$html_data.="<td align='right'>".number_format($totalnya,2,'.','')."</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		}
		else {
			$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'> ";
			$html_data.= " <thead>
			<tr>
				<th colspan='8' align='center'> REKAP PENERIMAAN BARANG PER ITEM </th>
			</tr>
			<tr>
				<th colspan='8' align='center'>Jenis Pembelian: $nama_jenis</th>
			</tr>
			<tr>
				<th colspan='8' align='center'>Periode: $date_from s.d $date_to</th>
			</tr>
			<tr>
				<th colspan='8' align='center'>Lokasi Gudang: $nama_gudang</th>
			</tr>

		 <tr class='judulnya'>
			<th>No</th>
			 <th>No. Perk</th>
			 <th>Nama Barang</th>
			 <th>Harga</th>
			 <th>Satuan</th>
			 <th>Total Qty</th>
			 <th>Nilai</th>
			 <th>Supplier</th>
		 </tr> ";
		 
			if (is_array($query)) {
				$totalnya = 0;
			 for($j=0;$j<count($query);$j++){
				 $nomor = $j+1;
				 $html_data.= "<tr class=\"record\">
						<td align='center'>".$nomor."</td>
						<td>'".$query[$j]['kode_perk']."</td>
						<td>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>
						<td align='right'>".$query[$j]['harga']."</td>
						<td>".$query[$j]['satuan']."</td>
						<td align='right'>".$query[$j]['qty']."</td>
						<td align='right'>".$query[$j]['nilai']."</td>
						<td>".$query[$j]['supplier']."</td>";
				 $html_data.=  "</tr>";					
				 $totalnya+= $query[$j]['nilai'];
		 	}
		   }
		   $html_data.= "<tr><td colspan='6' align='right'><b>Grand Total Nilai (Rp.)</b></td>
						<td align='right'><b>".$totalnya."</b></td></tr>
		   ";
		} // end if
		
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		$nama_file = "rekap_pembelian_peritem";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  // 10-11-2015
  function formgudang(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-rekap-pembelian-peritem/vmainformgudang';
	$this->load->view('template',$data);
  }
  
  function viewgudang(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-rekap-pembelian-peritem/vformviewgudang';
    $jenis_beli = $this->input->post('jenis_beli', TRUE);;
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$acuan = $this->input->post('acuan', TRUE);
	$gudang = $this->input->post('gudang', TRUE);
	
	$data['query'] = $this->mmaster->get_all_pembelian_gudang($jenis_beli, $acuan, $date_from, $date_to, $gudang);
	
	$pisah1 = explode("-", $date_from);
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$pisah2 = explode("-", $date_to);
	$bln2= $pisah2[1];
	$thn2= $pisah2[2];
		
	// 10-11-2015
	$data['id_gudang'] = $gudang;
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
									FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
		$data['nama_gudang'] = "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang;
	}
	else {
		$data['nama_gudang'] = 'Semua';
	}
	
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$data['acuan'] = $acuan;
	$data['bln1'] = $bln1;
	$data['thn1'] = $thn1;
	$data['bln2'] = $bln2;
	$data['thn2'] = $thn2;
	$this->load->view('template',$data);
  }
  
  function export_excel_gudang() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$acuan = $this->input->post('acuan', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$id_gudang = $this->input->post('id_gudang', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE); 
		
		// 10-11-2015
		if ($id_gudang != '0') {
			$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
										FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$id_gudang' ");
			$hasilrow = $query3->row();
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
			$nama_lokasi	= $hasilrow->nama_lokasi;
			$nama_gudang = "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang;
		}
		else {
			$nama_gudang = 'Semua';
		}
		
		$query = $this->mmaster->get_all_pembelian_gudang($jenis_beli, $acuan, $date_from, $date_to, $id_gudang); 
				
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else if ($jenis_beli == '2')
			$nama_jenis = "Kredit";
		else
			$nama_jenis = "Semua";
			
		$pisah1 = explode("-", $date_from);
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$pisah2 = explode("-", $date_to);
		$bln2= $pisah2[1];
		$thn2= $pisah2[2];
		
		if ($thn1 == $thn2) {
			$jum_bln = 0;
			for ($xx=$bln1; $xx<=$bln2; $xx++) {
				$jum_bln++;
			}
		}
		else
			$jum_bln = 0;
		$jumcolspan = $jum_bln+5;
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

			$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'> ";
			$html_data.= " <thead>
			<tr>
				<th colspan='8' align='center'> REKAP PENERIMAAN BARANG PER ITEM BERDASARKAN HARGA</th>
			</tr>
			<tr>
				<th colspan='8' align='center'>Jenis Pembelian: $nama_jenis</th>
			</tr>
			<tr>
				<th colspan='8' align='center'>Periode: $date_from s.d $date_to</th>
			</tr>
			<tr>
				<th colspan='8' align='center'>Lokasi Gudang: $nama_gudang</th>
			</tr>

		 <tr class='judulnya'>
			<th>No</th>
			 <th>Kode Barang</th>
			 <th>Nama Barang</th>
			 <th>Harga</th>
			 <th>Satuan</th>
			 <th>Satuan<br>Konversi</th>
			 <th>Total Qty</th>
			 <th>Nilai</th>
			 <th>Supplier</th>
		 </tr> ";
		 
			if (is_array($query)) {
				$totalnya = 0;
			 for($j=0;$j<count($query);$j++){
				 $nomor = $j+1;
				 $html_data.= "<tr class=\"record\">
						<td align='center'>".$nomor."</td>
						<td>".$query[$j]['kode_brg']."</td>
						<td>".$query[$j]['nama_brg']."</td>
						<td align='right'>".$query[$j]['harga']."</td>
						<td>".$query[$j]['satuan']."</td>
						<td>".$query[$j]['satuan_konversi']."</td>
						<td align='right'>".$query[$j]['qty']."</td>
						<td align='right'>".$query[$j]['nilai']."</td>
						<td>".$query[$j]['supplier']."</td>";
				 $html_data.=  "</tr>";					
				 $totalnya+= $query[$j]['nilai'];
		 	}
		   }
		   $html_data.= "<tr><td colspan='6' align='right'><b>Grand Total Nilai (Rp.)</b></td>
						<td align='right'><b>".$totalnya."</b></td></tr>
		   ";
		
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		$nama_file = "rekap_pembelian_peritem";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }

}
