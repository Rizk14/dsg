<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('akunting/mmaster');
  }
  
  function addcoa(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_coa 	= $this->uri->segment(4);
	$id_areax =$this->mmaster->getArea();
	if ($id_coa != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$carinya 	= $this->uri->segment(7);
		
		
		$hasil = $this->mmaster->getcoa($id_coa);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid_coa = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
			$ekode_group =$row->i_coa_group;
			$ekode_subledger =$row->i_coa_subledger;
			$ekode_ledger =$row->i_coa_ledger;
			$ekode_generalledger =$row->i_coa_generalledger;
			$eid_area =$row->id_area;
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
	}
	else {
			$eid_coa = '';
			$ekode = '';
			$enama = '';
			$edit = '';
			$ekode_group = '';
			$ekode_subledger ='';
			$ekode_ledger ='';
			$ekode_generalledger ='';
			$eid_area='';
	}
	$data['eid_coa'] = $eid_coa;
	$data['ekode'] = $ekode;
	$data['eid_area'] = $eid_area;
	$data['enama'] = $enama;
	$data['ekode_group'] = $ekode_group;	
	$data['ekode_subledger'] = $ekode_subledger;	
	$data['ekode_ledger'] = $ekode_ledger;	
	$data['ekode_generalledger'] = $ekode_generalledger;
	$data['id_areax'] = $id_areax;
	$data['edit'] = $edit;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/vmainformcoa';
    
	$this->load->view('template',$data);

  }
 function addcoagroup(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_coa 	= $this->uri->segment(4);
	
	if ($id_coa != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$carinya 	= $this->uri->segment(7);
		
		$hasil = $this->mmaster->getcoagroup($id_coa);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid_coa = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
	}
	else {
			$eid_coa = '';
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['eid_coa'] = $eid_coa;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/vmainformcoagroup';
    
	$this->load->view('template',$data);

  }
  
   function addcoageneralledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_coa 	= $this->uri->segment(4);
	
	if ($id_coa != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$carinya 	= $this->uri->segment(7);
		
		$hasil = $this->mmaster->getcoageneralledger($id_coa);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid_coa = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
	}
	else {
			$eid_coa = '';
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['eid_coa'] = $eid_coa;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/vmainformcoageneralledger';
    
	$this->load->view('template',$data);

  }
  
     function addcoasubledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_coa 	= $this->uri->segment(4);
	
	if ($id_coa != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$carinya 	= $this->uri->segment(7);
		
		$hasil = $this->mmaster->getcoasubledgercoa($id_coa);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid_coa = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
	}
	else {
			$eid_coa = '';
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['eid_coa'] = $eid_coa;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/vmainformcoasubledger';
    
	$this->load->view('template',$data);

  }
  
       function addcoaledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_coa 	= $this->uri->segment(4);
	
	if ($id_coa != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$carinya 	= $this->uri->segment(7);
		
		$hasil = $this->mmaster->getcoaledger($id_coa);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid_coa = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
	}
	else {
			$eid_coa = '';
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['eid_coa'] = $eid_coa;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/vmainformcoaledger';
    
	$this->load->view('template',$data);

  }
  
  function submitcoa(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$goedit 	= $this->input->post('goedit', TRUE);		
		$kode 	= $this->input->post('kode', TRUE);
		$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$id_coa 	= $this->input->post('id_coa', TRUE); 
		$id_area 	= $this->input->post('id_area', TRUE); 
		$nama 	= $this->input->post('nama', TRUE);
		$kode_group 	= $this->input->post('kode_group', TRUE);
		$kode_subledger 	= $this->input->post('kode_subledger', TRUE);
		$kode_ledger 	= $this->input->post('kode_ledger', TRUE);
		$kode_generalledger 	= $this->input->post('kode_generalledger', TRUE);
		
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data_coa($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'akunting/vmainformcoa';
					$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
					
					$ekode = '';
					$enama = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;					
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {//$item,
					$this->mmaster->savecoa($kode,$nama, $id_coa, $goedit,$kode_group,$kode_subledger,$kode_ledger,$kode_generalledger,$id_area);
					redirect('akunting/cform/addcoa');
				}
			} // end if goedit == ''
			else {
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
				
				if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data_coa($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'akunting/vmainformcoa';
						$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama'] = $nama;
						$data['edit'] = $edit;
						
						$this->load->view('template',$data);
					}
					else {//$item,
						$this->mmaster->savecoa($kode,$nama, $id_coa, $goedit,$kode_group,$kode_subledger,$kode_ledger,$kode_generalledger,$id_area);
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoa/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoa/".$carinya."/".$cur_page;
					
						redirect($url_redirectnya);
					}
				}
				else {//$item,
					$this->mmaster->savecoa($kode,$nama, $id_coa,$goedit,$kode_group,$kode_subledger,$kode_ledger,$kode_generalledger,$id_area);
					if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoa/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoa/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
				}
			}
			
  }
  
  function submitcoaledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$goedit 	= $this->input->post('goedit', TRUE);		
		$kode 	= $this->input->post('kode', TRUE);
		$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$id_coa 	= $this->input->post('id_coa', TRUE); 
		$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data_coaledger($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'akunting/vmainformcoaledger';
					$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
					
					$ekode = '';
					$enama = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;					
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {//$item,
					$this->mmaster->savecoaledger($kode,$nama, $id_coa, $goedit);
					redirect('akunting/cform/addcoaledger');
				}
			} // end if goedit == ''
			else {
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
				
				if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data_coaledger($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'akunting/vmainformcoaledger';
						$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama'] = $nama;
						$data['edit'] = $edit;
						
						$this->load->view('template',$data);
					}
					else {//$item,
						$this->mmaster->savecoaledger($kode,$nama, $id_coa, $goedit);
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoaledger/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoaledger/".$carinya."/".$cur_page;
					
						redirect($url_redirectnya);
					}
				}
				else {//$item,
					$this->mmaster->savecoaledger($kode,$nama, $id_coa, $goedit);
					if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoaledger/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoaledger/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
				}
			}
			
  }
  
  function submitcoageneralledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$goedit 	= $this->input->post('goedit', TRUE);		
		$kode 	= $this->input->post('kode', TRUE);
		$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$id_coa 	= $this->input->post('id_coa', TRUE); 
		$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data_coa_generalledger($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'akunting/vmainformcoageneralledger';
					$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
					
					$ekode = '';
					$enama = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;					
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {//$item,
					$this->mmaster->savecoa_generalledger($kode,$nama, $id_coa, $goedit);
					redirect('akunting/cform/addcoageneralledger');
				}
			} // end if goedit == ''
			else {
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
				
				if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data_coa_generalledger($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'akunting/vmainformcoageneralledger';
						$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama'] = $nama;
						$data['edit'] = $edit;
						
						$this->load->view('template',$data);
					}
					else {//$item,
						$this->mmaster->savecoa_generalledger($kode,$nama, $id_coa, $goedit);
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoageneralledger/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoageneralledger/".$carinya."/".$cur_page;
					
						redirect($url_redirectnya);
					}
				}
				else {//$item,
					$this->mmaster->savecoa_generalledger($kode,$nama, $id_coa, $goedit);
					if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoageneralledger/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoageneralledger/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
				}
			}
			
  }
  
  function submitcoagroup(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$goedit 	= $this->input->post('goedit', TRUE);		
		$kode 	= $this->input->post('kode', TRUE);
		$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$id_coa 	= $this->input->post('id_coa', TRUE); 
		$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data_coa_group($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'akunting/vmainformcoagroup';
					$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
					
					$ekode = '';
					$enama = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;					
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {//$item,
					$this->mmaster->savecoa_group($kode,$nama, $id_coa, $goedit);
					redirect('akunting/cform/addcoagroup');
				}
			} // end if goedit == ''
			else {
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
				
				if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data_coa_group($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'akunting/vmainformcoagroup';
						$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama'] = $nama;
						$data['edit'] = $edit;
						
						$this->load->view('template',$data);
					}
					else {//$item,
						$this->mmaster->savecoa_group($kode,$nama, $id_coa, $goedit);
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoagroup/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoagroup/".$carinya."/".$cur_page;
					
						redirect($url_redirectnya);
					}
				}
				else {//$item,
					$this->mmaster->savecoa_group($kode,$nama, $id_coa, $goedit);
					if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoagroup/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoagroup/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
				}
			}
			
  }
  
  function submitcoasubledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$goedit 	= $this->input->post('goedit', TRUE);		
		$kode 	= $this->input->post('kode', TRUE);
		$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$id_coa 	= $this->input->post('id_coa', TRUE); 
		$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data_coa_subledger($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'akunting/vmainformcoasubledger';
					$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
					
					$ekode = '';
					$enama = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;					
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {//$item,
					$this->mmaster->savecoa_subledger($kode,$nama, $id_coa, $goedit);
					redirect('akunting/cform/addcoasubledger');
				}
			} // end if goedit == ''
			else {
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
				
				if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data_coa_subledger($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'akunting/vmainformcoasubledger';
						$data['msg'] = "Data kode CoA ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama'] = $nama;
						$data['edit'] = $edit;
						
						$this->load->view('template',$data);
					}
					else {//$item,
						$this->mmaster->savecoa_subledger($kode,$nama, $id_coa, $goedit);
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoasubledger/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoasubledger/".$carinya."/".$cur_page;
					
						redirect($url_redirectnya);
					}
				}
				else {//$item,
					$this->mmaster->savecoa_subledger($kode,$nama, $id_coa, $goedit);
					if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewcoasubledger/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caricoasubledger/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
				}
			}
			
  }
  
  function viewcoa(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoa';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoatanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewcoa/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '40';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllcoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function viewcoagroup(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoagroup';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoagrouptanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewcoa/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllgroupcoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function caricoageneralledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoageneralledger';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoageneralledgertanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewcoageneralledger/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllgeneralledgercoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }  
  
    function viewcoasubledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoasubledger';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoasubledgertanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewcoasubledger/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllsubledgercoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
   function caricoagroup(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoagroup';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoagrouptanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/vformviewcoagroup/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllgroupcoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function caricoaledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoaledger';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoaledgertanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewcoaledger/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllledgercoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  

  
    function caricoasubledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoasubledger';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoasubledgertanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewcoa/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllsubledgercoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function viewcoaledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoaledger';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoaledgertanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewcoa/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllledgercoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function viewcoageneralledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewcoageneralledger';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllcoageneralledgertanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewcoa/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllgeneralledgercoa($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function caricoa(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAllcoatanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/akunting/cform/caricoa/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllcoa($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'akunting/vformviewcoa';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }

  function deletecoa(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_coa 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletecoa($id_coa);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewcoa/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/caricoa/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
   function deletecoageneralledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_coa 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletecoageneralledger($id_coa);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewcoageneralledger/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/caricoageneralledger/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  function deletecoagroup(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_coa 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletecoagroup($id_coa);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewcoagroup/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/caricoagroup/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  function deletecoasubledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_coa 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletecoasubledger($id_coa);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewcoasubledger/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/caricoasubledger/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  function deletecoaledger(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_coa 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletecoaledger($id_coa);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewcoaledger/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/caricoaledger/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  // 17-12-2015
  function addbank(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_bank 	= $this->uri->segment(4);
	
	if ($id_bank != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$carinya 	= $this->uri->segment(7);
		
		$hasil = $this->mmaster->getbank($id_bank);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid_bank = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
			$eid_coa = $row->id_coa;
			$ekode_coa = $row->kode_coa;
			$enama_coa = $row->nama_coa;
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
	}
	else {
			$eid_bank = '';
			$ekode = '';
			$enama = '';
			$eid_coa = '';
			$ekode_coa = '';
			$enama_coa = '';
			$edit = '';
	}
	$data['eid_bank'] = $eid_bank;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;	
	$data['eid_coa'] = $eid_coa;
	$data['ekode_coa'] = $ekode_coa;
	$data['enama_coa'] = $enama_coa;	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/vmainformbank';
    
	$this->load->view('template',$data);

  }



  function submitbank(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$goedit 	= $this->input->post('goedit', TRUE);		
		$kode 	= $this->input->post('kode', TRUE);
		$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$id_bank 	= $this->input->post('id_bank', TRUE); 
		$nama 	= $this->input->post('nama', TRUE);
		$id_coa_lama 	= $this->input->post('id_coa_lama', TRUE); 
		$id_coa 	= $this->input->post('id_coa_0', TRUE); 
		$kode_coa 	= $this->input->post('kode_coa', TRUE); 
		$nama_coa 	= $this->input->post('nama_coa_0', TRUE); 
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data_bank($kode, $id_coa);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'akunting/vmainformbank';
					$data['msg'] = "Data Bank dengan CoA ".$kode_coa." sudah ada..!";
					
					$eid_bank = '';
					$ekode = '';
					$enama = '';
					$eid_coa = '';
					$ekode_coa = '';
					$enama_coa = '';
					$edit = '';
					
					$data['eid_bank'] = $eid_bank;
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;	
					$data['eid_coa'] = $eid_coa;
					$data['ekode_coa'] = $ekode_coa;
					$data['enama_coa'] = $enama_coa;					
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {//$item,
					$this->mmaster->savebank($kode,$nama, $id_bank, $id_coa, $goedit,$kode_coa);
					
					redirect('akunting/cform/addbank');
				}
			} // end if goedit == ''
			else { // UPDATE DATA
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
				
				$data['cur_page'] = $cur_page;
				$data['is_cari'] = $is_cari;
				$data['carinya'] = $carinya;
				
				if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data_bank($kode, $id_coa);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'akunting/vmainformbank';
						$data['msg'] = "Data Bank kode ".$kode." dengan CoA ".$kode_coa." sudah ada..!";
						
						$edit = '1';
						$data['eid_bank'] = $id_bank;
						$data['ekode'] = $kode;
						$data['enama'] = $nama;
						$data['eid_coa'] = $id_coa;
						$data['ekode_coa'] = $kode_coa;
						$data['enama_coa'] = $nama_coa;
						$data['edit'] = $edit;
						
						$this->load->view('template',$data);
					}
					else {//$item,
						$this->mmaster->savebank($kode,$nama, $id_bank, $id_coa, $goedit,$kode_coa);
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewbank/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caribank/".$carinya."/".$cur_page;
					
						redirect($url_redirectnya);
					}
				}
				else {
					$this->mmaster->savebank($kode,$nama, $id_bank, $id_coa, $goedit,$kode_coa);
					if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "akunting/cform/viewbank/".$cur_page;
						else
							$url_redirectnya = "akunting/cform/caribank/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
				}
			} //END ELSE UPDATE DATA
			
  }
  
  function viewbank(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewbank';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllbanktanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewbank/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbank($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function caribank(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAllbanktanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/akunting/cform/caribank/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbank($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'akunting/vformviewbank';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }

  function deletebank(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_bank 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletebank($id_bank);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewbank/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/caribank/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  // 28-12-2015
  function addsaldoakun(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$data['isi'] = 'akunting/vmainformsaldoakun';
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function viewsaldoakun(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewsaldoakun';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllsaldoakuntanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewsaldoakun/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllsaldoakun($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function carisaldoakun(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAllsaldoakuntanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/akunting/cform/carisaldoakun/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllsaldoakun($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'akunting/vformviewsaldoakun';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function show_popup_coa(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);
			
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	$posisi 	= $this->uri->segment(4);
	if($posisi==''){
		$posisi 	= $this->input->post('posisi');
		}
	$qjum_total = $this->mmaster->get_listcoatanpalimit($keywordcari);

			$config['base_url'] = base_url()."index.php/akunting/cform/show_popup_coa/".$posisi."/";
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_listcoa($config['per_page'],$config['cur_page'], $keywordcari);
	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['posisi'] = $posisi;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('akunting/vpopupcoa',$data);
  }
  
   function show_popup_alokasi_bm(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	$posisi 	= $this->uri->segment(4);
	if($posisi==''){
		$posisi='1';
		}
		
	$coa=$this->uri->segment(5);
	$kode_coa=$this->uri->segment(6);		
	
	$qjum_total = $this->mmaster->get_listalotanpalimit($keywordcari,$coa,$kode_coa);

			$config['base_url'] = base_url()."index.php/akunting/cform/show_popup_alokasi_bm/".$posisi."/";
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 50;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_listalo($config['per_page'],$config['cur_page'], $keywordcari,$coa,$kode_coa);
	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['posisi'] = $posisi;
	$data['coa'] = $coa;
	$data['kode_coa'] = $kode_coa;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('akunting/vpopupalo',$data);
  }
  
  // 08-01-2016
  function submitsaldoakun(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$bulan 	= $this->input->post('bulan', TRUE);
		$tahun 	= $this->input->post('tahun', TRUE);
		$id_coa 	= $this->input->post('id_coa_1', TRUE);
		$nama_coa 	= $this->input->post('nama_coa_1', TRUE);
		$kode_coa 	= $this->input->post('kode_coa_1', TRUE);
		$saldo_awal 	= $this->input->post('saldo_awal', TRUE); 
		$debet 	= $this->input->post('debet', TRUE);
		$kredit 	= $this->input->post('kredit', TRUE);
		$saldo_akhir 	= $this->input->post('saldo_akhir', TRUE);
			
		$cek_data = $this->mmaster->cek_data_saldo($id_coa, $bulan, $tahun);
		if (count($cek_data) > 0) { 
			$data['isi'] = 'akunting/vmainformsaldoakun';
			$data['msg'] = "Data saldo akun untuk periode ".$bulan." - ".$tahun." sudah ada..!";
			$this->load->view('template',$data);
		}
		else {
			$this->mmaster->savesaldoakun($id_coa, $nama_coa, $bulan, $tahun, $saldo_awal, $debet, $kredit, $saldo_akhir);
			redirect('akunting/cform/addsaldoakun');
		}	
  }
  
  function editsaldoakun(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_saldo 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
		
	$hasil = $this->mmaster->getsaldoakun($id_saldo);
		
	foreach ($hasil as $row) {
		$eid_saldo = $row->id;
		$eid_coa = $row->id_coa;
		$enama_coa = $row->nama_coa;
		$ebulan = $row->bulan;
		$etahun = $row->tahun;
		$esaldo_awal = $row->saldo_awal;
		$edebet = $row->debet;
		$ekredit = $row->kredit;
		$esaldo_akhir = $row->saldo_akhir;
		
		// ambil kode coa
		$query3	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$eid_coa' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_coa	= $hasilrow->kode; 
		}
		else {
			$kode_coa = '';
		}
	}
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;
	
	$data['eid_saldo'] = $eid_saldo;
	$data['eid_coa'] = $eid_coa;
	$data['ekode_coa'] = $kode_coa;
	$data['enama_coa'] = $enama_coa;
	$data['ebulan'] = $ebulan;
	$data['etahun'] = $etahun;
	$data['esaldo_awal'] = $esaldo_awal;
	$data['edebet'] = $edebet;
	$data['ekredit'] = $ekredit;
	$data['esaldo_akhir'] = $esaldo_akhir;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/veditformsaldoakun';
    
	$this->load->view('template',$data);
  }
  
  function updatedatasaldoakun() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_saldo 	= $this->input->post('id_saldo', TRUE);
			$bulan 	= $this->input->post('bulan', TRUE);
			$tahun 	= $this->input->post('tahun', TRUE);
			$id_coa 	= $this->input->post('id_coa', TRUE);
			$id_coa_lama 	= $this->input->post('id_coa_lama', TRUE);
			$nama_coa 	= $this->input->post('nama_coa', TRUE);
			$saldo_awal 	= $this->input->post('saldo_awal', TRUE); 
			$debet 	= $this->input->post('debet', TRUE);
			$kredit 	= $this->input->post('kredit', TRUE);
			$saldo_akhir 	= $this->input->post('saldo_akhir', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
												
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			if ($id_coa != $id_coa_lama) {
				$cek_data = $this->mmaster->cek_data_saldo($id_coa, $bulan, $tahun);
				if (count($cek_data) == 0) {
					$this->db->query(" UPDATE tt_saldo_akun SET id_coa = '$id_coa', 
									nama_coa = '$nama_coa', saldo_awal = '$saldo_awal', 
									debet = '$debet', kredit = '$kredit', saldo_akhir = '$saldo_akhir',
									tgl_update= '$tgl', 
									uid_update_by='$uid_update_by' where id= '$id_saldo' "); 
									
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "akunting/cform/viewsaldoakun/".$cur_page;
					else
						$url_redirectnya = "akunting/cform/carisaldoakun/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
				}
				else {
					$hasil = $this->mmaster->getsaldoakun($id_saldo);
		
					foreach ($hasil as $row) {
						$eid_saldo = $row->id;
						$eid_coa = $row->id_coa;
						$enama_coa = $row->nama_coa;
						$ebulan = $row->bulan;
						$etahun = $row->tahun;
						$esaldo_awal = $row->saldo_awal;
						$edebet = $row->debet;
						$ekredit = $row->kredit;
						$esaldo_akhir = $row->saldo_akhir;
						
						// ambil kode coa
						$query3	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$eid_coa' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_coa	= $hasilrow->kode; 
						}
						else {
							$kode_coa = '';
						}
					}
					$data['cur_page'] = $cur_page;
					$data['is_cari'] = $is_cari;
					$data['carinya'] = $carinya;
					
					$data['eid_saldo'] = $eid_saldo;
					$data['eid_coa'] = $eid_coa;
					$data['ekode_coa'] = $kode_coa;
					$data['enama_coa'] = $enama_coa;
					$data['ebulan'] = $ebulan;
					$data['etahun'] = $etahun;
					$data['esaldo_awal'] = $esaldo_awal;
					$data['edebet'] = $edebet;
					$data['ekredit'] = $ekredit;
					$data['esaldo_akhir'] = $esaldo_akhir;
					
					$data['isi'] = 'akunting/veditformsaldoakun';
					$data['msg'] = "Update gagal. Data CoA sudah ada..!";
					$this->load->view('template',$data);
				}
			}
			else { // jika sama
				$this->db->query(" UPDATE tt_saldo_akun SET saldo_awal = '$saldo_awal', 
									debet = '$debet', kredit = '$kredit', saldo_akhir = '$saldo_akhir',
									tgl_update= '$tgl', 
									uid_update_by='$uid_update_by' where id= '$id_saldo' "); 
									
				if ($carinya == '') $carinya = "all";
				if ($is_cari == 0)
					$url_redirectnya = "akunting/cform/viewsaldoakun/".$cur_page;
				else
					$url_redirectnya = "akunting/cform/carisaldoakun/".$carinya."/".$cur_page;
					
				redirect($url_redirectnya);
			}	
  }
  
  function deletesaldoakun(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_saldo 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletesaldoakun($id_saldo);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewsaldoakun/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/carisaldoakun/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  // --------------- 09-01-2016 -------------------------------------------
  function addkasbesarin(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	$data['list_bank'] = $this->mmaster->get_bank_by_coa();
	$data['list_area'] = $this->mmaster->get_area();
	$data['isi'] = 'akunting/vmainformkasbesarin';
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function viewkasbesarin(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$data['isi'] = 'akunting/vformviewkasbesarin';
	// $data['list_bank'] = $this->mmaster->get_bank();
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllkasbesarintanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewkasbesarin/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllkasbesarin($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }

  function cetakvoucher(){
	// function cetakvoucher(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	
  $data['isi'] = 'akunting/vformviewkasbesarincetak';
  $data['list_bank'] = $this->mmaster->get_bank();
  $this->load->view('template',$data);
  }

  function cetakvoucherkbin(){
	// function cetakvoucher(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	
  $data['isi'] = 'akunting/vformviewbankincetak';
  $data['list_bank'] = $this->mmaster->get_bank();
  $this->load->view('template',$data);
  }

  function cetakvoucherbkout(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	
  $data['isi'] = 'akunting/vformviewbankoutcetak';
  $data['list_bank'] = $this->mmaster->get_bank();
  $this->load->view('template',$data);
  }
  
  function cetakvoucherkbout(){
	// function cetakvoucher(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	
  $data['isi'] = 'akunting/vformviewkasbesaroutcetak';
  $data['list_bank'] = $this->mmaster->get_bank();
  $this->load->view('template',$data);
}


	function cetak(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }

		$irv	  = $this->uri->segment(4);
		$iarea	  = $this->uri->segment(5);
		$irvtype  = $this->uri->segment(6);
		$jenis    = $this->uri->segment(7);
			if($irvtype == '01'){
				//UNTUK KAS BESAR
				if($jenis == '01'){
					$data['icetak']= '01';
					$data['iarea']= $iarea;
					$data['irv']  = $irv;
					$data['page_title'] = $this->lang->line('printrv');
					$data['isi']=$this->mmaster->bacarvkbin($irv,$iarea,$irvtype);
				}else if($jenis == '02'){
					$data['icetak']= '02';
					$data['iarea']= $iarea;
					$data['irv']  = $irv;
					$data['page_title'] = $this->lang->line('printrv');
					$data['isi']=$this->mmaster->bacarvkbout($irv,$iarea,$irvtype);
				}
			}else if($irvtype = '02'){
				//UNTUK BANK
				if($jenis == '01'){
					$data['icetak']= '03';
					$data['iarea']= $iarea;
					$data['irv']  = $irv;
					$data['page_title'] = $this->lang->line('printrv');
					$data['isi']=$this->mmaster->bacarvbkin($irv,$iarea,$irvtype);
				}else if($jenis == '02'){
					$data['icetak']= '04';
					$data['iarea']= $iarea;
					$data['irv']  = $irv;
					$data['page_title'] = $this->lang->line('printrv');
					$data['isi']=$this->mmaster->bacarvbkout($irv,$iarea,$irvtype);
				}
			}
			$this->load->view('akunting/vformrptkbin', $data);
	}

	function cetakkbout(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		  if (!isset($is_logged_in) || $is_logged_in!= true) {
			  redirect('loginform');
		  }
	
				$irv	= $this->uri->segment(4);
				$iarea	  = $this->uri->segment(5);
				$ipvtype	  = $this->uri->segment(6);
				// $this->load->model('akt-kbin-multi/mmaster');
				$data['iarea']= $iarea;
				$data['irv']  = $irv;
				$data['page_title'] = $this->lang->line('printrv');
				$data['isi']=$this->mmaster->bacarvkbout($irv,$iarea,$ipvtype);
				$this->load->view('akunting/vformrptkbout', $data);
		}

function printvoucher(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	$bulan 		= $this->input->post('bulan', TRUE);  
	$tahun 		= $this->input->post('tahun', TRUE);  
	$bank 		= $this->input->post('ibank', TRUE);  
	$periode	= $bulan."-".$tahun;
  $data['isi'] = 'akunting/vformviewkasbesarinprint';
  $data['list_bank'] = $this->mmaster->get_bank();
  $keywordcari = "all";
  $jum_total = $this->mmaster->getvouchertanpalimit($keywordcari,$periode);	
						  $config['base_url'] = base_url().'index.php/akunting/cform/printvoucher/';
						  $config['total_rows'] = count($jum_total); 
						  $config['per_page'] = '20';
						  $config['first_link'] = 'Awal';
						  $config['last_link'] = 'Akhir';
						  $config['next_link'] = 'Selanjutnya';
						  $config['prev_link'] = 'Sebelumnya';
						  $config['cur_page'] = $this->uri->segment(4);
						  $this->pagination->initialize($config);		
  $data['query'] = $this->mmaster->getvoucher($config['per_page'],$this->uri->segment(4), $keywordcari,$periode);
  $data['jum_total'] = count($jum_total);
//   $data['bank'] = $bank;
  $data['periode'] = $periode;
  
  
  if ($config['cur_page'] == '')
	  $cur_page = 0;
  else
	  $cur_page = $config['cur_page'];
  $data['cur_page'] = $cur_page;
  $data['is_cari'] = 0;
  
  if ($keywordcari == "all")
	  $data['cari'] = '';
  else
	  $data['cari'] = $keywordcari;
  $data['startnya'] = $config['cur_page'];
  $this->load->view('template',$data);
}

function printvoucherkbin(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	$bulan 		= $this->input->post('bulan', TRUE);  
	$tahun 		= $this->input->post('tahun', TRUE);  
	$bank 		= $this->input->post('ibank', TRUE);  
	
  		$periode	= $bulan."-".$tahun;
  		$data['isi'] = 'akunting/vformviewkbinprint';
  		$data['list_bank'] = $this->mmaster->get_bank();
  		$keywordcari = "all";
  		$jum_total = $this->mmaster->getvoucherkbintanpalimit($keywordcari,$periode,$bank);	
								  $config['base_url'] = base_url().'index.php/akunting/cform/printvoucher/';
								  $config['total_rows'] = count($jum_total); 
								  $config['per_page'] = '20';
								  $config['first_link'] = 'Awal';
								  $config['last_link'] = 'Akhir';
								  $config['next_link'] = 'Selanjutnya';
								  $config['prev_link'] = 'Sebelumnya';
								  $config['cur_page'] = $this->uri->segment(4);
								  $this->pagination->initialize($config);		
  		$data['query'] = $this->mmaster->getvoucherkbin($config['per_page'],$this->uri->segment(4), $keywordcari,$periode,$bank);
  		$data['jum_total'] = count($jum_total);
  		$data['bank'] = $bank;
  		$data['periode'] = $periode;
  
  
  if ($config['cur_page'] == '')
	  $cur_page = 0;
  else
	  $cur_page = $config['cur_page'];
  $data['cur_page'] = $cur_page;
  $data['is_cari'] = 0;
  
  if ($keywordcari == "all")
	  $data['cari'] = '';
  else
	  $data['cari'] = $keywordcari;
  $data['startnya'] = $config['cur_page'];
  $this->load->view('template',$data);
}

function printvoucherbkout(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	$bulan 		= $this->input->post('bulan', TRUE);  
	$tahun 		= $this->input->post('tahun', TRUE);  
	$bank 		= $this->input->post('ibank', TRUE);  
	
  		$periode	= $bulan."-".$tahun;
  		$data['isi'] = 'akunting/vformviewkboutprint';
  		$data['list_bank'] = $this->mmaster->get_bank();
  		$keywordcari = "all";
  		$jum_total = $this->mmaster->getvouchertanpalimitbkout($keywordcari,$periode,$bank);	
								  $config['base_url'] = base_url().'index.php/akunting/cform/printvoucher/';
								  $config['total_rows'] = count($jum_total); 
								  $config['per_page'] = '20';
								  $config['first_link'] = 'Awal';
								  $config['last_link'] = 'Akhir';
								  $config['next_link'] = 'Selanjutnya';
								  $config['prev_link'] = 'Sebelumnya';
								  $config['cur_page'] = $this->uri->segment(4);
								  $this->pagination->initialize($config);		
  		$data['query'] = $this->mmaster->getvoucherbkout($config['per_page'],$this->uri->segment(4), $keywordcari,$periode,$bank);
  		$data['jum_total'] = count($jum_total);
  		$data['bank'] = $bank;
  		$data['periode'] = $periode;
  
  
  if ($config['cur_page'] == '')
	  $cur_page = 0;
  else
	  $cur_page = $config['cur_page'];
  $data['cur_page'] = $cur_page;
  $data['is_cari'] = 0;
  
  if ($keywordcari == "all")
	  $data['cari'] = '';
  else
	  $data['cari'] = $keywordcari;
  $data['startnya'] = $config['cur_page'];
  $this->load->view('template',$data);
}

function printvoucherkbout(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	$bulan 		= $this->input->post('bulan', TRUE);  
	$tahun 		= $this->input->post('tahun', TRUE);  
	// $bank 		= $this->input->post('ibank', TRUE);  
	$periode	= $bulan."-".$tahun;
  $data['isi'] = 'akunting/vformviewkasbesaroutprint';
  $data['list_bank'] = $this->mmaster->get_bank();
  $keywordcari = "all";
  $jum_total = $this->mmaster->getvouchertanpalimitkbout($keywordcari,$periode);	
						  $config['base_url'] = base_url().'index.php/akunting/cform/printvoucherkbout/';
						  $config['total_rows'] = count($jum_total); 
						  $config['per_page'] = '20';
						  $config['first_link'] = 'Awal';
						  $config['last_link'] = 'Akhir';
						  $config['next_link'] = 'Selanjutnya';
						  $config['prev_link'] = 'Sebelumnya';
						  $config['cur_page'] = $this->uri->segment(4);
						  $this->pagination->initialize($config);		
  $data['query'] = $this->mmaster->getvoucherkbout($config['per_page'],$this->uri->segment(4), $keywordcari,$periode);
  $data['jum_total'] = count($jum_total);
//   $data['bank'] = $bank;
  $data['periode'] = $periode;
  
  
  if ($config['cur_page'] == '')
	  $cur_page = 0;
  else
	  $cur_page = $config['cur_page'];
  $data['cur_page'] = $cur_page;
  $data['is_cari'] = 0;
  
  if ($keywordcari == "all")
	  $data['cari'] = '';
  else
	  $data['cari'] = $keywordcari;
  $data['startnya'] = $config['cur_page'];
  $this->load->view('template',$data);
}
  
  function carikasbesarin(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(6);
	}
	if ($keywordcari == '')
		$keywordcari 	= "all";
	$periode 	= $this->uri->segment(4);
	// $bank 		= $this->uri->segment(5);
	// $jum_total = $this->mmaster->getAllkasbesarintanpalimit($keywordcari);
	$jum_total = $this->mmaster->getvouchertanpalimitcari($keywordcari,$periode);	
							$config['base_url'] = base_url().'index.php/akunting/cform/carikasbesarin/'.$periode.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	// $data['query'] = $this->mmaster->getAllkasbesarin($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['query'] = $this->mmaster->getvouchercari($config['per_page'],$this->uri->segment(4), $keywordcari,$periode);
	$data['jum_total'] = count($jum_total);
	// $data['bank'] = $bank;
  	$data['periode'] = $periode;
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'akunting/vformviewkasbesarinprint';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }

  function caribkin(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	
  $keywordcari 	= $this->input->post('cari', TRUE);  
  if ($keywordcari == '') {
	  $keywordcari 	= $this->uri->segment(6);
  }
  if ($keywordcari == '')
	  $keywordcari 	= "all";
  $periode 	= $this->uri->segment(4);
  $bank 	= $this->uri->segment(5);
  
  $jum_total = $this->mmaster->getvouchertanpalimitcaribkin($keywordcari,$periode, $bank);	
						  $config['base_url'] = base_url().'index.php/akunting/cform/carikasbesarin/'.$periode.'/'.$bank.'/'.$keywordcari.'/';
						  $config['total_rows'] = count($jum_total); 
						  $config['per_page'] = '20';
						  $config['first_link'] = 'Awal';
						  $config['last_link'] = 'Akhir';
						  $config['next_link'] = 'Selanjutnya';
						  $config['prev_link'] = 'Sebelumnya';
						  $config['cur_page'] = $this->uri->segment(5);
						  $this->pagination->initialize($config);		
  $data['query'] 	 = $this->mmaster->getvouchercaribkin($config['per_page'],$this->uri->segment(4), $keywordcari,$periode,$bank);
  $data['jum_total'] = count($jum_total);
  $data['bank'] 	 = $bank;
	$data['periode'] = $periode;
  
  if ($config['cur_page'] == '')
	  $cur_page = 0;
  else
	  $cur_page = $config['cur_page'];
  $data['cur_page'] = $cur_page;
  $data['is_cari'] = 1;
  
  $data['isi'] = 'akunting/vformviewkbinprint';
  if ($keywordcari == "all")
	  $data['cari'] = '';
  else
	  $data['cari'] = $keywordcari;
  $data['startnya'] = $config['cur_page'];
  $this->load->view('template',$data);
}

  function carikasbesarout(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	}
	
  $keywordcari 	= $this->input->post('cari', TRUE);  
  if ($keywordcari == '') {
	  $keywordcari 	= $this->uri->segment(6);
  }
  if ($keywordcari == '')
	  $keywordcari 	= "all";
  $periode 	= $this->uri->segment(4);
//   $bank 		= $this->uri->segment(5);
  $jum_total = $this->mmaster->getvouchertanpalimitcarikbout($keywordcari,$periode);	
						  $config['base_url'] = base_url().'index.php/akunting/cform/carikasbesarout/'.$periode.'/'.$keywordcari.'/';
						  $config['total_rows'] = count($jum_total); 
						  $config['per_page'] = '20';
						  $config['first_link'] = 'Awal';
						  $config['last_link'] = 'Akhir';
						  $config['next_link'] = 'Selanjutnya';
						  $config['prev_link'] = 'Sebelumnya';
						  $config['cur_page'] = $this->uri->segment(5);
						  $this->pagination->initialize($config);		
  // $data['query'] = $this->mmaster->getAllkasbesarin($config['per_page'],$this->uri->segment(5), $keywordcari);
  $data['query'] = $this->mmaster->getvouchercarikbout($config['per_page'],$this->uri->segment(4), $keywordcari, $periode);
  $data['jum_total'] = count($jum_total);
//   $data['bank'] = $bank;
	$data['periode'] = $periode;
  
  if ($config['cur_page'] == '')
	  $cur_page = 0;
  else
	  $cur_page = $config['cur_page'];
  $data['cur_page'] = $cur_page;
  $data['is_cari'] = 1;
  
  $data['isi'] = 'akunting/vformviewkasbesaroutprint';
  if ($keywordcari == "all")
	  $data['cari'] = '';
  else
	  $data['cari'] = $keywordcari;
  $data['startnya'] = $config['cur_page'];
  $this->load->view('template',$data);
}
  
  function caricoa2(){
		$kode_coa 	= $this->input->post('kode_coa', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel tm_coa utk ambil id, nama
		$queryxx = $this->db->query(" SELECT id, nama FROM tm_coa
									WHERE kode = '".$kode_coa."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_coa = $hasilxx->id;
			$nama_coa = $hasilxx->nama;
		}
		else {
			$id_coa = 0;
			$nama_coa = '';
		}
		
		$data['nama_coa'] = $nama_coa;
		$data['kode_coa'] = $kode_coa;
		$data['id_coa'] = $id_coa;
		$data['posisi'] = $posisi;
		$this->load->view('akunting/vinfocoa', $data); 
		return true;
  }
  
  // 12-01-2016
  function submitkasbesarin(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		
		$bulan 	= $this->input->post('bulan', TRUE);  
		$tahun 	= $this->input->post('tahun', TRUE);
		$iperiode =$bulan."-".$tahun;
		$no 	= $this->input->post('no', TRUE);
		$id_area= $this->input->post('id_area', TRUE);
		$irvtype  = $this->input->post('irvtype', TRUE);
		$bank  = $this->input->post('bank', TRUE);
		// $exbank = explode(';',$bank);
		// $id_coa_bank =$exbank[0];
		// $kode_bank =$exbank[1];
		// $kode_coa_bank =$exbank[2];
		$dbank		  = $this->input->post('dbank', TRUE);	
		if($dbank!=''){
				$tmp=explode("-",$dbank);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkb=$th."-".$bl."-".$hr;
				$dpv=$th."-".$bl."-".$hr;
			}
		$tot=0;$fdebet='f';
		$jumlah_input=$no-1;
		$irv=$this->mmaster->runningnumberrvkbin($tahun,$bulan,$id_area,$irvtype);
		// var_dump($irv);
		// die();
		for ($i=1;$i<=$jumlah_input;$i++) {
			$iareax       = $this->input->post('iarea_'.$i, TRUE);
			$icoa         = $this->input->post('kode_coa_'.$i, TRUE);
			$deskripsi     = $this->input->post('deskripsi_'.$i, TRUE);
			$ecoaname     = $this->input->post('nama_coa_'.$i, TRUE);
			$id_coa		 = $this->input->post('id_coa_'.$i, TRUE);
			
			  if($fdebet=='t'){
				    $accdebet		  = $icoa;
				    $namadebet		= $ecoaname;
				    $acckredit		= KasBesar;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
				    $iddebet		= $this->mmaster->idacc($accdebet);
				     $idkredit		= $id_coa_bank;
			    }else{
				    $accdebet		  = KasBesar;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $iddebet		= $this->mmaster->idacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
				     $idkredit		= $id_coa;
			    }
			
			$fclose			= 'f';
			$tot=$tot+$this->input->post('jumlah_'.$i, TRUE);
			
			$kbin=$this->mmaster->savekasbesarin($bulan, $tahun, 
							$id_coa,$ecoaname, 
							$this->input->post('tgl_'.$i, TRUE), $deskripsi,
							$this->input->post('jumlah_'.$i, TRUE),$iareax);	
						
			 $this->mmaster->inserttransitemdebet($accdebet,$kbin['i_kb'],$namadebet,'t','t',$iareax, $deskripsi,
			 $this->input->post('jumlah_'.$i, TRUE),$this->input->post('tgl_'.$i, TRUE),$kbin['id_kb']);
			 $this->mmaster->inserttransitemkredit($kbin['kode_coa'],$kbin['i_kb'],$ecoaname,'f','t',$iareax, $deskripsi,
			 $this->input->post('jumlah_'.$i, TRUE),$this->input->post('tgl_'.$i, TRUE),$kbin['id_kb']);
			 				
			$this->mmaster->insertgldebetkb ($accdebet,$kbin['i_kb'],$namadebet,'t',$iareax,
			$this->input->post('jumlah_'.$i, TRUE),$this->input->post('tgl_'.$i, TRUE),$deskripsi,$iddebet,$kbin['id_kb']);
			$this->mmaster->insertglkreditkbin($kbin['kode_coa'],$kbin['i_kb'],$namakredit,'f',$iareax,
			$this->input->post('jumlah_'.$i, TRUE),$this->input->post('tgl_'.$i, TRUE),$deskripsi,$id_coa,$kbin['id_kb']);		
			$this->mmaster->inserttransheader($kbin['i_kb'],$iareax,$deskripsi,$fclose,$this->input->post('tgl_'.$i, TRUE),$kbin['id_kb']);		
			$this->mmaster->insertrvitemkbin( $irv['norv'],$id_area,$icoa,$ecoaname,$this->input->post('jumlah_'.$i, TRUE)
			,$deskripsi,$kbin['i_kb'],$irvtype,$iareax,$irv['kode_area']);
		}
		$icoa=KasBesar;
				$this->mmaster->insertrvkbin( $irv['norv'],$id_area,$iperiode,$icoa,$dpv,$tot,$deskripsi,$irvtype,$irv['kode_area']);
		
		redirect('akunting/cform/viewkasbesarin');
		// ---------------------------------------------------------------------------------
  }
  
  // 14-01-2016
  function deletekasbesarin(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_kb 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletekasbesarin($id_kb);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewkasbesarin/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/carikasbesarin/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  // --------------- 18-01-2016 -------------------------------------------
  function editkasbesarin(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_kb 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
		
	$hasil = $this->mmaster->getkasbesarin($id_kb);
		
	foreach ($hasil as $row) {
		$eid_kb = $row->id;
		$ebulan = $row->bulan;
		$etahun = $row->tahun;
		$etgl_trans = $row->tgl;
		$eid_coa = $row->id_coa;
		$enama_coa = $row->nama_coa;
		$edeskripsi = $row->deskripsi;
		$ejumlah = $row->jumlah;
		
		$pisah1 = explode("-", $etgl_trans);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$etgl_trans = $tgl1."-".$bln1."-".$thn1;
		
		// ambil kode coa
		$query3	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$eid_coa' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_coa	= $hasilrow->kode; 
		}
		else {
			$kode_coa = '';
		}
	}
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;
	
	$data['eid_kb'] = $eid_kb;
	$data['eid_coa'] = $eid_coa;
	$data['ekode_coa'] = $kode_coa;
	$data['enama_coa'] = $enama_coa;
	$data['ebulan'] = $ebulan;
	$data['etahun'] = $etahun;
	$data['etgl_trans'] = $etgl_trans;
	$data['edeskripsi'] = $edeskripsi;
	$data['ejumlah'] = $ejumlah;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/veditformkasbesarin';
    
	$this->load->view('template',$data);
  }
  
  function updatedatakasbesarin() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_kb 	= $this->input->post('id_kb', TRUE);
			$id_coa 	= $this->input->post('id_coa_0', TRUE);
			$id_coa_lama 	= $this->input->post('id_coa_lama', TRUE);
			$nama_coa 	= $this->input->post('nama_coa_0', TRUE);
			$tgl_trans 	= $this->input->post('tgl', TRUE); 
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$jumlah 	= $this->input->post('jumlah', TRUE);
			$jumlah_lama 	= $this->input->post('jumlah_lama', TRUE);
			$bulan 	= $this->input->post('bulan', TRUE);
			$tahun 	= $this->input->post('tahun', TRUE);
			//echo $bulan." ".$tahun; die();
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
												
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		// ----------------
		//update saldo akun utk coa lama, reset di debet, saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, debet, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa_lama' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$sdebet	= $hasilrow->debet;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$sdebet-= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET debet = '$sdebet', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update saldo akun utk coa baru, saldo akhir sesuai coa baru
		$query3	= $this->db->query(" SELECT id, debet, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$sdebet	= $hasilrow->debet;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$sdebet+= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET debet = '$sdebet', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update kas besar
		$this->db->query(" UPDATE tt_kas_besar SET id_coa = '$id_coa', 
									nama_coa = '$nama_coa', tgl = '$tgl_trans', 
									deskripsi = '$deskripsi', jumlah = '$jumlah', 
									tgl_update= '$tgl', 
									uid_update_by='$uid_update_by' where id= '$id_kb' "); 
									
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "akunting/cform/viewkasbesarin/".$cur_page;
		else
			$url_redirectnya = "akunting/cform/carikasbesarin/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
		// ----------------
  }
  
  function addkasbesarout(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	$data['list_area'] = $this->mmaster->get_area();
	$data['isi'] = 'akunting/vmainformkasbesarout';
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function viewkasbesarout(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewkasbesarout';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllkasbesarouttanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewkasbesarout/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllkasbesarout($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
//   function carikasbesarout(){
// 	  $is_logged_in = $this->session->userdata('is_logged_in');
// 		if (!isset($is_logged_in) || $is_logged_in!= true) {
// 			redirect('loginform');
// 		}
	  
// 	$keywordcari 	= $this->input->post('cari', TRUE);  
	
// 	if ($keywordcari == '') {
// 		$keywordcari 	= $this->uri->segment(4);
// 	}
	
// 	if ($keywordcari == '')
// 		$keywordcari 	= "all";
	
//     $jum_total = $this->mmaster->getAllkasbesarouttanpalimit($keywordcari);
// 							$config['base_url'] = base_url().'index.php/akunting/cform/carikasbesarout/'.$keywordcari.'/';
// 							$config['total_rows'] = count($jum_total); 
// 							$config['per_page'] = '20';
// 							$config['first_link'] = 'Awal';
// 							$config['last_link'] = 'Akhir';
// 							$config['next_link'] = 'Selanjutnya';
// 							$config['prev_link'] = 'Sebelumnya';
// 							$config['cur_page'] = $this->uri->segment(5);
// 							$this->pagination->initialize($config);		
// 	$data['query'] = $this->mmaster->getAllkasbesarout($config['per_page'],$this->uri->segment(5), $keywordcari);
// 	$data['jum_total'] = count($jum_total);
	
// 	if ($config['cur_page'] == '')
// 		$cur_page = 0;
// 	else
// 		$cur_page = $config['cur_page'];
// 	$data['cur_page'] = $cur_page;
// 	$data['is_cari'] = 1;
	
// 	$data['isi'] = 'akunting/vformviewkasbesarout';
// 	if ($keywordcari == "all")
// 		$data['cari'] = '';
// 	else
// 		$data['cari'] = $keywordcari;
// 	$data['startnya'] = $config['cur_page'];
// 	$this->load->view('template',$data);
//   }
  
  function submitkasbesarout(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		$ipvtype  = $this->input->post('ipvtype', TRUE);
		$bulan 	= $this->input->post('bulan', TRUE);  
		$tahun 	= $this->input->post('tahun', TRUE);
		$no 	= $this->input->post('no', TRUE);
		$id_area= $this->input->post('id_area', TRUE);
		$dkb		  = $this->input->post('dkb', TRUE);
		$iperiode =$bulan."-".$tahun;
			if($dkb!=''){
				$tmp=explode("-",$dkb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkb=$th."-".$bl."-".$hr;
				$dpv=$th."-".$bl."-".$hr;
				$drv=$th."-".$bl."-".$hr;
			}
			$tot=0;$fdebet='t';
			$jumlah_input=$no-1;
			
			$ipv=$this->mmaster->runningnumberpvkbout($tahun,$bulan,$id_area,$ipvtype);
		for ($i=1;$i<=$jumlah_input;$i++) {
			
			$icoa			=$this->input->post('kode_coa_'.$i, TRUE);
			$id_coa			=$this->input->post('id_coa_'.$i, TRUE);
			$vkb			= $this->input->post('jumlah_'.$i, TRUE);
			$iareax      	= $this->input->post('iarea_'.$i, TRUE);
			$icoa			= $this->input->post('kode_coa_'.$i, TRUE);
			$deskripsi			= $this->input->post('deskripsi_'.$i, TRUE);
			$ecoaname			= $this->input->post('nama_coa_'.$i, TRUE);
			$dkbitem			= $this->input->post('tgl_'.$i, TRUE);
			
			$fclose			= 'f';
			$tot=$tot+$vkb;
			if($fdebet=='t'){
				    $accdebet		  = $icoa;
				    $namadebet		= $ecoaname;
				    $iddebet		= $id_coa;
				    $acckredit		= KasBesar;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
				    $idkredit		= $this->mmaster->idacc($acckredit);
			    }else{
				    $accdebet		  = KasBesar;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $iddebet		= $this->mmaster->idacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
				    $idkredit		= $id_coa;
				    
			    }
			
			$kbout=$this->mmaster->savekasbesarout($bulan, $tahun, 
							$id_coa,$ecoaname, 
							$dkbitem, $deskripsi,
							$vkb,$id_area );		
			
			 $this->mmaster->inserttransitemdebetout($accdebet,$kbout['i_kb'],$namadebet,'t','t',$id_area, $deskripsi,
			 $vkb,$dkb,$kbout['id_kb']);
			 $this->mmaster->inserttransitemkreditout($icoa,$kbout['i_kb'],$namakredit,'f','t',$id_area, $deskripsi,
			 $vkb,$dkb,$kbout['id_kb']);
			
				$this->mmaster->insertgldebetkbout($accdebet,$kbout['i_kb'],$namadebet,'t',$iareax,$vkb,$dkb,$deskripsi,
				$iddebet,$kbout['id_kb']);
			    $this->mmaster->insertglkreditkbout($acckredit,$kbout['i_kb'],$namakredit,'f',$iareax,$vkb,$dkb,$deskripsi,
			    $idkredit,$accdebet,$kbout['id_kb']);
						
			//~ $this->mmaster->insertglkreditkbout($accdebet,$kbout['i_kb'],$namadebet,'t',$id_area,
			//~ $vkb,$dkbitem,$deskripsi,$iddebet,$accdebet,$kbout['id_kb']);
			//~ $this->mmaster->insertgldebetkbout($kbout['kode_coa'],$kbout['i_kb'],$ecoaname,'f',$id_area,
			//~ $vkb,$dkbitem,$deskripsi,$id_coa,$kbout['id_kb']);				
			
			$this->mmaster->inserttransheaderkbout($kbout['i_kb'],$id_area,$deskripsi,$fclose,$dkb,$kbout['id_kb']);		
			$this->mmaster->insertpvitemkbout( $ipv['nopv'],$id_area,$icoa,$ecoaname,
			$vkb,$deskripsi,$kbout['i_kb'],$ipvtype,$iareax,$ipv['kode_area']);
		}
			$icoakb=KasBesar;
				$this->mmaster->insertpvkbout(  $ipv['nopv'],$id_area,$iperiode,$icoa,$dpv,$tot,$deskripsi,$ipvtype,$ipv['kode_area']);
		
		
		$icoax=substr($icoa);
		$icoakk=substr($icoa);
		$accdebet= KasBesar;
		if($icoax==Bank){
			$irvtype	= "02";
			$ireff		=$eremark;
			$fdebetx	='f';
			$namakredit		= $this->mmaster->namaacc($accdebet);
		    $ecoaname		= $namakredit;
		
		$ikodebm=$this->mmaster->runningnumberbankmasuk($tahun,$bulan,$iareax,$id_coa);
		$this->mmaster->insertx( $iareax,$ikodebm,$iperiode,$icoakb,$vkb,$dkbitem,
		$ecoaname,$deskripsi,$fdebetx,$icoa);
		
		$irvb=$this->mmaster->runningnumberrvb($tahun,$bulan,$id_coa,$iareax);
				$irv=$this->mmaster->runningnumberrv($tahun,$bulan,$iareax,$irvtype);
				$this->mmaster->insertrvb($irvb,$icoa,$irv,$iareax,$irvtype);
				$this->mmaster->insertrv($irv['norv'],$iareax,$iperiode,$icoa,$drv,$tot,$eremark,$irvtype,$irv['kode_area']);
				$this->mmaster->insertrvitem($irv['norv'],$iareax,$icoakb,$ecoaname,$vkb,$ireff,$ikodebm,$irvtype,$iarea,$icoa,$irv['kode_area']);
		}
		elseif($icoakk==KasKecil){
			$irvtypekk	= "00";
			$ireff=$eremark;
			$fdebetx	='f';
			$namakredit		= $this->mmaster->namaacc($accdebet);
		    $ecoaname		= $namakredit;
#----------------------------------------------------------------------------------------------------------------------------------#
				$ikodekk=$this->mmaster->runningnumberkk($tah,$bul,$iareax,$icoakb);
				$this->mmaster->insertkk($iareax,$ikodekk,$iperiode,$icoakb,$vkb,$dbukti,$ecoaname,$edescription,$fdebetx,$icoakb);
#----------------------------------------------------------------------------------------------------------------------------------#
				$irv=$this->mmaster->runningnumberrv($tah,$bul,$iareax,$irvtypekk);
				$this->mmaster->insertrv($irv['norv'],$iareax,$iperiode,$icoa,$drv,$tot,$eremark,$irvtypekk,$irv['kode_area']);
				$this->mmaster->insertrvitem( $irv['norv'],$iareax,$icoakb,$ecoaname,$vkb,$ireff,$ikodekk,$irvtypekk,$iarea,$icoa,$irv['kode_area']);	
				}
		redirect('akunting/cform/viewkasbesarout');
		// ---------------------------------------------------------------------------------
  }
  
  function deletekasbesarout(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_kb 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletekasbesarout($id_kb);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewkasbesarout/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/carikasbesarout/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  // 19-01-2016
  function editkasbesarout(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_kb 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
		
	$hasil = $this->mmaster->getkasbesarout($id_kb);
		
	foreach ($hasil as $row) {
		$eid_kb = $row->id;
		$ebulan = $row->bulan;
		$etahun = $row->tahun;
		$etgl_trans = $row->tgl;
		$eid_coa = $row->id_coa;
		$enama_coa = $row->nama_coa;
		$edeskripsi = $row->deskripsi;
		$ejumlah = $row->jumlah;
		
		$pisah1 = explode("-", $etgl_trans);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$etgl_trans = $tgl1."-".$bln1."-".$thn1;
		
		// ambil kode coa
		$query3	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$eid_coa' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_coa	= $hasilrow->kode; 
		}
		else {
			$kode_coa = '';
		}
	}
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;
	
	$data['eid_kb'] = $eid_kb;
	$data['eid_coa'] = $eid_coa;
	$data['ekode_coa'] = $kode_coa;
	$data['enama_coa'] = $enama_coa;
	$data['ebulan'] = $ebulan;
	$data['etahun'] = $etahun;
	$data['etgl_trans'] = $etgl_trans;
	$data['edeskripsi'] = $edeskripsi;
	$data['ejumlah'] = $ejumlah;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/veditformkasbesarout';
    
	$this->load->view('template',$data);
  }
  
  function updatedatakasbesarout() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_kb 	= $this->input->post('id_kb', TRUE);
			$id_coa 	= $this->input->post('id_coa_0', TRUE);
			$id_coa_lama 	= $this->input->post('id_coa_lama', TRUE);
			$nama_coa 	= $this->input->post('nama_coa_0', TRUE);
			$tgl_trans 	= $this->input->post('tgl', TRUE); 
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$jumlah 	= $this->input->post('jumlah', TRUE);
			$jumlah_lama 	= $this->input->post('jumlah_lama', TRUE);
			$bulan 	= $this->input->post('bulan', TRUE);
			$tahun 	= $this->input->post('tahun', TRUE);
			//echo $bulan." ".$tahun; die();
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
												
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		// ----------------
		//update saldo akun utk coa lama, reset di kredit, saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa_lama' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit-= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update saldo akun utk coa baru, saldo akhir sesuai coa baru
		$query3	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit+= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update kas besar
		$this->db->query(" UPDATE tt_kas_besar SET id_coa = '$id_coa', 
									nama_coa = '$nama_coa', tgl = '$tgl_trans', 
									deskripsi = '$deskripsi', jumlah = '$jumlah', 
									tgl_update= '$tgl', 
									uid_update_by='$uid_update_by' where id= '$id_kb' "); 
									
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "akunting/cform/viewkasbesarout/".$cur_page;
		else
			$url_redirectnya = "akunting/cform/carikasbesarout/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
		// ----------------
  }
  
  // ------------- KAS BANK 19-01-2016 ----------------------------
  function addbankout(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$data['isi'] = 'akunting/vmainformbankout';
	$data['list_bank'] = $this->mmaster->get_bank_by_coa();
	$data['list_area'] = $this->mmaster->get_area();
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function viewbankout(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewbankout';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllbankouttanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewbankout/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbankout($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$data['list_bank'] = $this->mmaster->get_bank_by_coa();
	
	$this->load->view('template',$data);
  }
  
  function caribankout(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAllbankouttanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/akunting/cform/caribankout/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbankout($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'akunting/vformviewbankout';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$data['list_bank'] = $this->mmaster->get_bank_by_coa();
	$this->load->view('template',$data);
  }
  
  function submitbankout(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		
		$bulan 	= $this->input->post('bulan', TRUE);  
		$tahun 	= $this->input->post('tahun', TRUE);
		$bank	= $this->input->post('bank', TRUE);
		$exbank = explode(';',$bank);
		$id_coa_bank =$exbank[0];
		$kode_bank =$exbank[1];
		$kode_coa_bank =$exbank[2];
		$ipvtype  	= $this->input->post('ipvtype', TRUE);
		$exbank = explode(';',$bank);
		$id_coa_bank =$exbank[0];
		$kode_bank =$exbank[1];
		$kode_coa_bank =$exbank[2];
		$no 	= $this->input->post('no', TRUE);
		$id_area= $this->input->post('id_area', TRUE);
		$iperiode =$bulan."-".$tahun;
		$dbank	  	= $this->input->post('dbank', TRUE);
		$fdebet='t';
		if($dbank!=''){
				$tmp=explode("-",$dbank);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbank=$th."-".$bl."-".$hr;
				$dpv=$th."-".$bl."-".$hr;
				$drv=$th."-".$bl."-".$hr;
			}
			$tot=0;
		$jumlah_input=$no-1;
		
		$ipvb=$this->mmaster->runningnumberpvb($tahun,$bulan,$id_coa_bank,$id_area);
		// $ipv=$this->mmaster->runningnumberpv($tahun,$bulan,$id_area,$ipvtype);
		$ipv=$this->mmaster->runningnumberppv($tahun,$bulan,$id_area,$ipvtype,$kode_coa_bank,$id_coa_bank);
		
		
		for ($i=1;$i<=$jumlah_input;$i++) {
			 $icoa         = $this->input->post('kode_coa_'.$i, TRUE);
			 $edescription = $this->input->post('deskripsi_'.$i, TRUE);
			 $iareax 		= $this->input->post('iarea_'.$i, TRUE);
			 $vbank 		= $this->input->post('jumlah_'.$i, TRUE);
			 $dbukti       = $this->input->post('tgl_'.$i, TRUE);
			 if ($dbukti != ''){
				 $tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkk=$th."-".$bl."-".$hr;
							 
				 }
			 $id_coa        = $this->input->post('id_coa_'.$i, TRUE); 
			 $ecoaname		= $this->input->post('nama_coa_'.$i, TRUE);
			  
			$tot=$tot+$vbank;
			$kbout=$this->mmaster->savebankout($bulan, $tahun, $id_coa_bank,
							$id_coa,$ecoaname, 
							$dbukti, $edescription,
							$vbank, $id_area );		
			
			 if($fdebet=='t'){
				    $accdebet		= $icoa;
				    $acckredit		= $kode_coa_bank;
				    $idkredit		= $id_coa_bank;				    
				    $namakredit		= $this->mmaster->namaacc($acckredit);
				    $namadebet		= $ecoaname;
				    $iddebet		= $this->mmaster->idacc($accdebet);
			    }else{
				    $accdebet		= $icoa;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $iddebet		= $this->mmaster->idacc($accdebet);
				    $acckredit		= $kode_coa_bank;
				    $namakredit		= $ecoaname;
				    $idkredit		= $id_coa;
			    }

			
			$fclose			= 'f';
			
			 $this->mmaster->inserttransitemdebet($accdebet,$kbout['i_kb'],$namadebet,'t','t',$id_area, $edescription,
			 $vbank,$dbukti,$kbout['id_kb']);
			 $this->mmaster->inserttransitemkredit($kbout['kode_coa'],$kbout['i_kb'],$ecoaname,
			 'f','t',$id_area, $edescription,$vbank,$dbukti,$kbout['id_kb']);
							
			$this->mmaster->insertgldebetbk($accdebet,$kbout['i_kb'],$namadebet,'f',$id_area,$vbank,
			$dbukti,$edescription,$iddebet,$kode_bank,$kbout['id_kb'], $acckredit);
			$this->mmaster->insertglkreditbk($acckredit,$kbout['kode_coa'],$kbout['i_kb'],$namakredit,'t',$id_area,
			$vbank,$dbukti,$edescription,$idkredit,$kode_bank,$kbout['id_kb']);				
		
			$this->mmaster->inserttransheader($kbout['i_kb'],$id_area,$edescription,$fclose,$dbukti,$kbout['id_kb']);		
			$this->mmaster->insertpvitem( $ipv['nopv'],$id_area,$icoa,$ecoaname,
			$vbank,$edescription,$kbout['i_kb'],$ipvtype,$iareax,$id_coa_bank,$ipv['kode_area']);
		}
			$this->mmaster->insertpv( $ipv['nopv'],$id_area,$iperiode,$icoa,$dpv,$tot,$edescription,$ipvtype,$ipv['kode_area']);
				$this->mmaster->insertpvb( $ipvb,$id_coa_bank,$ipv['nopv'],$id_area,$ipvtype);	
				#------lawan posting ------------
		$icoax=substr($icoa,0,5);
		$icoakk=substr($icoa,0,6);
		$icoakb=substr($icoa,0,8);
		$fdebetx		='f';
		if($icoax==Bank){
			$irvtype	= "02";
			$ireff		=$edescription;
		    $accdebet		= $icoabank;
		    $namakredit		= $this->mmaster->namaacc($accdebet);
		    $ecoaname		= $namakredit;

				$ikodebm=$this->mmaster->runningnumberkasbankin($tahun,$bulan,$id_coa_bank);
				$this->mmaster->insertx( $id_area,$ikodebm,$iperiode,$id_coa_bank,$vbank,$dbukti,$ecoaname,$edescription,$fdebetx,$icoa);

				$irvb=$this->mmaster->runningnumberrvb($tahun,$bulan,$icoa,$iarea);
				$irv=$this->mmaster->runningnumberrv($tahun,$bulan,$iarea,$irvtype);
				$this->mmaster->insertrvb($irvb,$icoa,$irv,$iarea,$irvtype);
				$this->mmaster->insertrv($irv['norv'],$iarea,$iperiode,$icoa,$drv,$tot,$eremark,$irvtype,$irv['kode_area']);
				$this->mmaster->insertrvitem( $irv['norv'],$iarea,$icoabank,$ecoaname,$vbank,$ireff,$ikodebm,$irvtype,$iareax,$icoa,$irv['kode_area']);
}
		elseif($icoakb==KasBesar){
			$irvtypekb	= "01";
			$ireff=$edescription;
			$accdebet		= $icoa;
		    $namakredit		= $this->mmaster->namaacc($accdebet);
		    $ecoaname		= $namakredit;
				$ikodekb=$this->mmaster->runningnumberkb($bulan,$tahun);
				$this->mmaster->insertkb( $id_area,$ikodekb,$iperiode,$kode_coa_bank,$vbank,$dbukti,$namakredit,$edescription,$fdebetx,$id_coa);

				$irv=$this->mmaster->runningnumberrv($tahun,$bulan,$iareax,$irvtypekb);
			
				$this->mmaster->insertrv($irv['norv'],$iareax,$iperiode,$icoa,$drv,$tot,$eremark,$irvtypekb,$irv['kode_area']);
				$this->mmaster->insertrvitem( $irv['norv'],$iareax,$id_coa_bank,$ecoaname,$vbank,$ireff,$ikodekb,$irvtypekb,$iarea,$icoa,$irv['kode_area'],$id_area);	
				}
		elseif($icoakk==KasKecil){
			$irvtypekk	= "00";
			$ireff=$edescription;
			//$accdebet		= $icoabank;
		    $namakredit		= $this->mmaster->namaacc($accdebet);
		    $ecoaname		= $namakredit;
				$ikodekk=$this->mmaster->runningnumberkk($bulan,$tahun,$iareax,$icoa);
				$this->mmaster->insertkk($iareax,$ikodekk,$iperiode,$icoa,$vbank,$dkk,$ecoaname,$edescription,$fdebetx,$id_coa,$bulan,$tahun);

				$irv=$this->mmaster->runningnumberrv($tahun,$bulan,$iareax,$irvtypekk);

				$this->mmaster->insertrv($irv['norv'],$iareax,$iperiode,$icoa,$drv,$tot,$edescription,$irvtypekk,$irv['kode_area']);
				$this->mmaster->insertrvitem( $irv['norv'],$iareax,$id_coa_bank,$ecoaname ,$vbank,$ireff,$ikodekk,$irvtypekk,$id_area,$icoa,$irv['kode_area']);	
				}
		redirect('akunting/cform/viewbankout');
		// ---------------------------------------------------------------------------------
  }
  
  // 20-01-2016
  function deletebankout(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_kbank 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletebankout($id_kbank);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewbankout/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/caribankout/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  function editbankout(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_kbank 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
		
	$hasil = $this->mmaster->getbankout($id_kbank);
		
	foreach ($hasil as $row) {
		$eid_kbank = $row->id;
		$eid_coa_bank = $row->id_coa_bank;
		$ebulan = $row->bulan;
		$etahun = $row->tahun;
		$etgl_trans = $row->tgl;
		$eid_coa = $row->id_coa;
		$enama_coa = $row->nama_coa;
		$edeskripsi = $row->deskripsi;
		$ejumlah = $row->jumlah;
		
		$pisah1 = explode("-", $etgl_trans);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$etgl_trans = $tgl1."-".$bln1."-".$thn1;
		
		// ambil kode coa
		$query3	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$eid_coa' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_coa	= $hasilrow->kode; 
		}
		else {
			$kode_coa = '';
		}
	}
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;
	
	$data['eid_kbank'] = $eid_kbank;
	$data['eid_coa_bank'] = $eid_coa_bank;
	$data['eid_coa'] = $eid_coa;
	$data['ekode_coa'] = $kode_coa;
	$data['enama_coa'] = $enama_coa;
	$data['ebulan'] = $ebulan;
	$data['etahun'] = $etahun;
	$data['etgl_trans'] = $etgl_trans;
	$data['edeskripsi'] = $edeskripsi;
	$data['ejumlah'] = $ejumlah;
	$data['msg'] = '';
	$data['list_bank'] = $this->mmaster->get_bank_by_coa();
    $data['isi'] = 'akunting/veditformbankout';
    
	$this->load->view('template',$data);
  }
  
  function updatedatabankout() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_kbank 	= $this->input->post('id_kbank', TRUE);
			$id_coa 	= $this->input->post('id_coa_0', TRUE);
			$id_coa_lama 	= $this->input->post('id_coa_lama', TRUE);
			$nama_coa 	= $this->input->post('nama_coa_0', TRUE);
			$tgl_trans 	= $this->input->post('tgl', TRUE); 
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$jumlah 	= $this->input->post('jumlah', TRUE);
			$jumlah_lama 	= $this->input->post('jumlah_lama', TRUE);
			$bulan 	= $this->input->post('bulan', TRUE);
			$tahun 	= $this->input->post('tahun', TRUE);
			$id_coa_bank 	= $this->input->post('id_coa_bank', TRUE);
			$id_coa_bank_lama 	= $this->input->post('id_coa_bank_lama', TRUE);
			//echo $bulan." ".$tahun; die();
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
												
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		// ----------------
		//update saldo akun utk coa lama, reset di kredit, saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa_lama' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit-= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update saldo akun utk coa baru, saldo akhir sesuai coa baru
		$query3	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit+= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update kas bank
		$this->db->query(" UPDATE tt_kas_bank SET id_coa_bank='$id_coa_bank', id_coa = '$id_coa', 
									nama_coa = '$nama_coa', tgl = '$tgl_trans', 
									deskripsi = '$deskripsi', jumlah = '$jumlah', 
									tgl_update= '$tgl', 
									uid_update_by='$uid_update_by' where id= '$id_kbank' "); 
									
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "akunting/cform/viewbankout/".$cur_page;
		else
			$url_redirectnya = "akunting/cform/caribankout/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
		// ----------------
  }
  
  // BANK MASUK
  function addbankin(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$data['isi'] = 'akunting/vmainformbankin';
	$data['list_bank'] = $this->mmaster->get_bank_by_coa();
	$data['list_area'] = $this->mmaster->get_area();
	//print_r($data['list_area']);
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function viewbankin(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewbankin';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllbankintanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewbankin/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbankin($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function caribankin(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAllbankintanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/akunting/cform/caribankin/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbankin($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'akunting/vformviewbankin';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function submitbankin(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		
		$bulan 	= $this->input->post('bulan', TRUE);  
		$tahun 	= $this->input->post('tahun', TRUE);
		$bank	= $this->input->post('bank', TRUE);
		$exbank = explode(';',$bank);
		$id_coa_bank =$exbank[0];
		$kode_bank =$exbank[1];
		$kode_coa_bank =$exbank[2];
		//$id_coa_bank 	= $this->input->post('id_coa_bank', TRUE);
		$id_area 	= $this->input->post('id_area', TRUE);
		$irvtype  = $this->input->post('irvtype', TRUE);
		$iperiode =$bulan."-".$tahun;
		$dbank	  = $this->input->post('dbank', TRUE);
		$no 	= $this->input->post('no', TRUE);

		if($dbank!=''){
				$tmp=explode("-",$dbank);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbank=$th."-".$bl."-".$hr;
				$drv=$th."-".$bl."-".$hr;
			}
		
		$jumlah_input=$no-1;
		$tot=0;	$fdebet='f';
		
		$irvb=$this->mmaster->runningnumberrvb($tahun,$bulan,$id_coa_bank,$id_area);	
		$irv=$this->mmaster->runningnumberrv($tahun,$bulan,$id_area,$irvtype,$kode_coa_bank,$id_coa_bank);
		for ($i=1;$i<=$jumlah_input;$i++) {	
			$deskripsi	 = $this->input->post('deskripsi_'.$i, TRUE);
			$vbank		 = $this->input->post('jumlah_'.$i, TRUE);
			$icoa		 = $this->input->post('kode_coa_'.$i, TRUE);
			$ecoaname	 = $this->input->post('nama_coa_'.$i, TRUE);
			$id_coa		 = $this->input->post('id_coa_'.$i, TRUE);
			$tot=$tot+$vbank;
			
			$kboin=$this->mmaster->savebankin($bulan, $tahun, $id_coa_bank,
							$id_coa,$ecoaname, 
							$this->input->post('tgl_'.$i, TRUE), $deskripsi,
							$vbank, $this->input->post('id_area', TRUE) ,
							$this->input->post('kode_alo_'.$i, TRUE), $this->input->post('id_alo_'.$i, TRUE) 
							);		
								
					 if($fdebet=='t'){
				    $accdebet		= $icoa;
				    $namadebet		= $ecoaname;
				    $iddebet		= $this->mmaster->idacc($accdebet);
				    $acckredit		= $kode_coa_bank;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
				    $idkredit		= $id_coa_bank;
			    }else{
				    $accdebet		= $kode_coa_bank;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $iddebet		= $this->mmaster->idacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
				    $idkredit		= $id_coa;
			    }		
			
			$fclose			= 'f';

			 $this->mmaster->inserttransitemdebet($accdebet,$kboin['i_kb'],$namadebet,'t','t',$id_area, $deskripsi,
			 $vbank,$this->input->post('tgl_'.$i, TRUE),$kboin['id_kb']);
			 $this->mmaster->inserttransitemkredit($kboin['kode_coa'],$kboin['i_kb'],$ecoaname,'f','t',$id_area, $deskripsi,
			 $vbank,$this->input->post('tgl_'.$i, TRUE),$kboin['id_kb']);
							
			$this->mmaster->insertgldebetbm($accdebet,$kboin['i_kb'],$namadebet,'t',$this->input->post('id_area', TRUE),
			$vbank,$this->input->post('tgl_'.$i, TRUE),$deskripsi,$iddebet,$kode_bank,$kboin['id_kb']);
			$this->mmaster->insertglkreditbm($accdebet,$kboin['kode_coa'],$kboin['i_kb'],$ecoaname,'f',$this->input->post('id_area', TRUE),
			$vbank,$this->input->post('tgl_'.$i, TRUE),$deskripsi,$id_coa,$kode_bank,$kboin['id_kb']);				
		
			$this->mmaster->inserttransheader($kboin['i_kb'],$id_area,$deskripsi,$fclose,$this->input->post('tgl_'.$i, TRUE),$kboin['id_kb']);		
			$this->mmaster->insertrvitembm( $irv['norv'],$id_area,$icoa,$ecoaname,$vbank,
			$deskripsi,$kboin['i_kb'],$irvtype,$this->input->post('iarea_'.$i, TRUE),$id_coa_bank,$irv['kode_area']);
		// ---------------------------------------------------------------------------------
  }
				$this->mmaster->insertrvbm( $irv['norv'],$id_area,$iperiode,$kode_coa_bank,$drv,$tot,$deskripsi,$irvtype,$irv['kode_area']);
				$this->mmaster->insertrvbbm( $irvb,$id_coa_bank,$irv['norv'],$id_area,$irvtype);
  redirect('akunting/cform/viewbankin');
 } 
  // 22-01-2016
  function deletebankin(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_kbank 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletebankin($id_kbank);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewbankin/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/caribankin/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
  function editbankin(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_kbank 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
		
	$hasil = $this->mmaster->getbankin($id_kbank);
		
	foreach ($hasil as $row) {
		$eid_kbank = $row->id;
		$ebulan = $row->bulan;
		$etahun = $row->tahun;
		$eid_coa_bank = $row->id_coa_bank;
		$etgl_trans = $row->tgl;
		$eid_coa = $row->id_coa;
		$enama_coa = $row->nama_coa;
		$edeskripsi = $row->deskripsi;
		$ejumlah = $row->jumlah;
		
		$pisah1 = explode("-", $etgl_trans);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$etgl_trans = $tgl1."-".$bln1."-".$thn1;
		
		// ambil kode coa
		$query3	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$eid_coa' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_coa	= $hasilrow->kode; 
		}
		else {
			$kode_coa = '';
		}
	}
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;
	
	$data['eid_kbank'] = $eid_kbank;
	$data['eid_coa_bank'] = $eid_coa_bank;
	$data['eid_coa'] = $eid_coa;
	$data['ekode_coa'] = $kode_coa;
	$data['enama_coa'] = $enama_coa;
	$data['ebulan'] = $ebulan;
	$data['etahun'] = $etahun;
	$data['etgl_trans'] = $etgl_trans;
	$data['edeskripsi'] = $edeskripsi;
	$data['ejumlah'] = $ejumlah;
	$data['msg'] = '';
	$data['list_bank'] = $this->mmaster->get_bank_by_coa();
    $data['isi'] = 'akunting/veditformbankin';
    
	$this->load->view('template',$data);
  }
  
  function updatedatabankin() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_kbank 	= $this->input->post('id_kbank', TRUE);
			$id_coa 	= $this->input->post('id_coa_0', TRUE);
			$id_coa_lama 	= $this->input->post('id_coa_lama', TRUE);
			$nama_coa 	= $this->input->post('nama_coa_0', TRUE);
			$tgl_trans 	= $this->input->post('tgl', TRUE); 
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$jumlah 	= $this->input->post('jumlah', TRUE);
			$jumlah_lama 	= $this->input->post('jumlah_lama', TRUE);
			$bulan 	= $this->input->post('bulan', TRUE);
			$tahun 	= $this->input->post('tahun', TRUE);
			//echo $bulan." ".$tahun; die();
			$id_coa_bank 	= $this->input->post('id_coa_bank', TRUE);
			$id_coa_bank_lama 	= $this->input->post('id_coa_bank_lama', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
												
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		// ----------------
		//update saldo akun utk coa lama, reset di debet, saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, debet, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa_lama' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$sdebet	= $hasilrow->debet;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$sdebet-= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET debet = '$sdebet', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update saldo akun utk coa baru, saldo akhir sesuai coa baru
		$query3	= $this->db->query(" SELECT id, debet, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$sdebet	= $hasilrow->debet;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$sdebet+= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET debet = '$sdebet', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update kas bank
		$this->db->query(" UPDATE tt_kas_bank SET id_coa_bank='$id_coa_bank', id_coa = '$id_coa', 
									nama_coa = '$nama_coa', tgl = '$tgl_trans', 
									deskripsi = '$deskripsi', jumlah = '$jumlah', 
									tgl_update= '$tgl', 
									uid_update_by='$uid_update_by' where id= '$id_kbank' "); 
									
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "akunting/cform/viewbankin/".$cur_page;
		else
			$url_redirectnya = "akunting/cform/caribankin/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
		// ----------------
  }
   function viewkaskecil(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewkaskecil';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllkaskeciltanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewkaskecil/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllkaskecil($config['per_page'],$this->uri->segment(4), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  function addkaskecil(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	$data['list_area'] = $this->mmaster->get_area();
	$data['isi'] = 'akunting/vmainformkaskecil';
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  function submitkaskecil(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		
		$area		= $this->input->post('area', TRUE);
		$exarea		= explode(";",$area);
		$iarea		= $exarea[0];
		$kodearea	= $exarea[1];
		$ipvtype  	= $this->input->post('ipvtype', TRUE);
		$bulan 		= $this->input->post('bulan', TRUE);  
		$tahun 		= $this->input->post('tahun', TRUE);
		$iperiode	= $bulan."-".$tahun;
		$dkk		= $this->input->post('dkk', TRUE);
		if($dkk!=''){
				$tmp=explode("-",$dkk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkk=$th."-".$bl."-".$hr;
				$dpv=$th."-".$bl."-".$hr;
			}
		
		$no 	= $this->input->post('no', TRUE);
		
		$jumlah_input=$no-1;
		$fdebet='t';
		$tot=0;	
		
			for ($i=1;$i<=$jumlah_input;$i++) 
			{	
				$ipv=$this->mmaster->runningnumberpv($tahun,$bulan,$iarea,$ipvtype);
				$id_coa=$this->input->post('id_coa_'.$i, TRUE);
				if($id_coa!=''){
					$icoa= $this->input->post('kode_coa_'.$i, TRUE);	
					$ecoaname= $this->input->post('nama_coa_'.$i, TRUE);	
					$edescription= $this->input->post('deskripsi_'.$i, TRUE);
					 $vkk          = $this->input->post('jumlah_'.$i, TRUE);
					 $tot=$tot+$vkk;
					 
			$kkkin=$this->mmaster->savekaskecil($bulan, $tahun, 
							$id_coa,$ecoaname, 
							$this->input->post('tgl_'.$i, TRUE), $edescription,
							$vkk , $iarea,$iperiode);	
			
			if($fdebet=='t'){
				    $accdebet		= $icoa;
				    $iddebet		= $id_coa;
				    $namadebet		= $ecoaname;
				    $acckredit		= KasKecil.$kodearea;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
				    $idkredit		= $this->mmaster->idacc($acckredit);
			    }else{
				    $accdebet		  = KasKecil.$kodearea;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $iddebet		= $this->mmaster->idacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
				    $idkredit		= $id_coa;
			    }
			
			$fclose			= 'f';
		
			 $this->mmaster->inserttransitemdebet($accdebet,$kkkin['i_kk'],$namadebet,'t','t',$iarea, $edescription,
			 $vkk,$this->input->post('tgl_'.$i, TRUE),$kkkin['id_kk']);
			 $this->mmaster->inserttransitemkredit($acckredit,$kkkin['i_kk'],$namakredit,'f','t',$iarea, $edescription,
			 $vkk,$this->input->post('tgl_'.$i, TRUE),$kkkin['id_kk']);
						
			$this->mmaster->insertgldebetkk($accdebet,$kkkin['i_kk'],$ecoaname,'t',$iarea,
			$vkk,$this->input->post('tgl_'.$i, TRUE),$edescription,$iddebet,$kkkin['id_kk']);				
			$this->mmaster->insertglkreditkk($acckredit,$kkkin['i_kk'],$namakredit,'f',$iarea,
			$vkk,$this->input->post('tgl_'.$i, TRUE),$edescription,$idkredit,$kkkin['id_kk']);				
		
			$this->mmaster->inserttransheader($kkkin['i_kk'],$iarea,$edescription,$fclose,$this->input->post('tgl_'.$i, TRUE),$kkkin['id_kk']);		
		  $this->mmaster->insertpvitemkk( $ipv['nopv'],$iarea,$icoa,$ecoaname,$vkk,$edescription,$kkkin['i_kk'],$ipvtype,$iarea,$ipv['kode_area']);
		  }
		  				$icoa=KasKecil.$kodearea;
				$this->mmaster->insertpv( $ipv['nopv'],$iarea,$iperiode,$icoa,$dpv,$tot,$edescription,$ipvtype,$ipv['kode_area']);
	}
		// ---------------------------------------------------------------------------------

  redirect('akunting/cform/viewkaskecil');
}
  function deletekaskecil(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_kk 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletekaskecil($id_kk);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewkaskecil/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/carikaskecil/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
  }
  
    function editkaskecil(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_kk 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
		
	$hasil = $this->mmaster->getkaskecil($id_kk);
		
	foreach ($hasil as $row) {
		$eid_kk = $row->id;
		$ebulan = $row->bulan;
		$etahun = $row->tahun;
		$etgl_trans = $row->tgl;
		$eid_coa = $row->id_coa;
		$enama_coa = $row->nama_coa;
		$edeskripsi = $row->deskripsi;
		$ejumlah = $row->jumlah;
		
		$pisah1 = explode("-", $etgl_trans);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$etgl_trans = $tgl1."-".$bln1."-".$thn1;
		
		// ambil kode coa
		$query3	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$eid_coa' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_coa	= $hasilrow->kode; 
		}
		else {
			$kode_coa = '';
		}
	}
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;
	
	$data['eid_kk'] = $eid_kk;
	$data['eid_coa'] = $eid_coa;
	$data['ekode_coa'] = $kode_coa;
	$data['enama_coa'] = $enama_coa;
	$data['ebulan'] = $ebulan;
	$data['etahun'] = $etahun;
	$data['etgl_trans'] = $etgl_trans;
	$data['edeskripsi'] = $edeskripsi;
	$data['ejumlah'] = $ejumlah;
	$data['msg'] = '';
	
    $data['isi'] = 'akunting/veditformkaskecil';
    
	$this->load->view('template',$data);
  }
  
  function updatedatakaskecil() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_kk 	= $this->input->post('id_kk', TRUE);
			$id_coa 	= $this->input->post('id_coa_0', TRUE);
			$id_coa_lama 	= $this->input->post('id_coa_lama', TRUE);
			$nama_coa 	= $this->input->post('nama_coa_0', TRUE);
			$tgl_trans 	= $this->input->post('tgl', TRUE); 
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$jumlah 	= $this->input->post('jumlah', TRUE);
			$jumlah_lama 	= $this->input->post('jumlah_lama', TRUE);
			$bulan 	= $this->input->post('bulan', TRUE);
			$tahun 	= $this->input->post('tahun', TRUE);
			//echo $bulan." ".$tahun; die();
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
												
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		// ----------------
		//update saldo akun utk coa lama, reset di kredit, saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa_lama' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit-= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update saldo akun utk coa baru, saldo akhir sesuai coa baru
		$query3	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit+= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
		// update kas besar
		$this->db->query(" UPDATE tt_kas_kecil SET id_coa = '$id_coa', 
									nama_coa = '$nama_coa', tgl = '$tgl_trans', 
									deskripsi = '$deskripsi', jumlah = '$jumlah', 
									tgl_update= '$tgl', 
									uid_update_by='$uid_update_by' where id= '$id_kk' "); 
									
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "akunting/cform/viewkaskecil/".$cur_page;
		else
			$url_redirectnya = "akunting/cform/carikaskecil/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
		// ----------------
  }
  
  function addjurnalumum (){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['list_area'] = $this->mmaster->get_area();
	$data['isi'] = 'akunting/vmainformjurnalumum';
	$data['msg'] = '';
	$this->load->view('template',$data);
		
	  }
	  
	  function submitjurnalumum(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		
		$no_jurnal 	= $this->input->post('no_jurnal', TRUE);  
		$tgl_jurnal 	= $this->input->post('tgl_jurnal', TRUE);
		$id_area 	= $this->input->post('id_area', TRUE);
		$keterangan 	= $this->input->post('keterangan', TRUE);
		$debet 	= $this->input->post('debet', TRUE);  
		$kredit 	= $this->input->post('kredit', TRUE);
		$no 	= $this->input->post('no', TRUE);
		$jumlah_input=$no-1;
		
			
			
	
		$id_ju=	$this->mmaster->savejurnalumum($no_jurnal, $tgl_jurnal, $id_area,$keterangan, $debet, $kredit,$uid_insert_by);
				for ($i=1;$i<=$jumlah_input;$i++) {
			$this->mmaster->savejurnalumum_detail($id_ju,
							$this->input->post('kode_coa_'.$i, TRUE),$this->input->post('nama_coa_'.$i, TRUE), 
							$this->input->post('id_coa_'.$i, TRUE), $this->input->post('deskripsi_'.$i, TRUE),
							$this->input->post('debet_'.$i, TRUE), $this->input->post('kredit_'.$i, TRUE) );						
		}
			
		redirect('akunting/cform/viewjurnalumum');
		// ---------------------------------------------------------------------------------
  }
	 function viewjurnalumum(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $data['isi'] = 'akunting/vformviewjurnalumum';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAlljurnalumum($keywordcari);	
							$config['base_url'] = base_url().'index.php/akunting/cform/viewkasbesarout/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAlljurnalumumtanpalimit($config['per_page'],$this->uri->segment(4), $keywordcari);
	
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
   function editjurnalumum(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_jurnal 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	
		
	$hasil = $this->mmaster->getjurnalumum($id_jurnal);
		
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	
	$data['hasil'] = $hasil;
	$data['id_jurnal'] = $id_jurnal;
	$data['msg'] = '';
	$data['list_area'] = $this->mmaster->get_area();
    $data['isi'] = 'akunting/veditformjurnalumum';
    
	$this->load->view('template',$data);
  }
	  
	  
	  function updatedatajurnalumum() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$no_jurnal 	= $this->input->post('no_jurnal', TRUE);  
			$tgl_jurnal 	= $this->input->post('tgl_jurnal', TRUE);
			$id_area 	= $this->input->post('id_area', TRUE);
			$keterangan 	= $this->input->post('keterangan', TRUE);
			$totdebet 	= $this->input->post('totdebet', TRUE);  
			$totkredit 	= $this->input->post('totkredit', TRUE);
			$no 	= $this->input->post('no', TRUE);
			$id_jurnal= $this->input->post('id_jurnal', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
												
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			$jumlah_input=$no-1;
			$update_data_header=$this->mmaster->update_data_header($id_jurnal,$no_jurnal,$id_area,$keterangan,$totdebet,$totkredit,$tgl,$uid_update_by,$tgl_jurnal );
			
			for ($i=1;$i<=$jumlah_input;$i++) {
			$this->mmaster->updatedatadetail(
			$id_jurnal,
			$this->input->post('id_coa_'.$i, TRUE),
			$this->input->post('nama_coa_'.$i, TRUE),
			$this->input->post('kode_coa_'.$i, TRUE),
			$this->input->post('deskripsi_'.$i, TRUE),
			$this->input->post('debet_'.$i, TRUE),
			$this->input->post('kredit_'.$i, TRUE),
			$this->input->post('id_detail_'.$i, TRUE)
			);
		}
		redirect ('akunting/cform/viewjurnalumum');
		}
		
		function deletejurnalumum(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_jurnal 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	
	$this->mmaster->deljurnalumum($id_jurnal);
	
	if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "akunting/cform/viewjurnalumum/".$cur_page;
	else
		$url_redirectnya = "akunting/cform/viewjurnalumum/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);	

    
	$this->load->view('template',$data);
  }
}
