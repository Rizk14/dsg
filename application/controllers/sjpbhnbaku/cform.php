<?php

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_sjp_bhnbaku']			= $this->lang->line('page_title_sjp_bhnbaku');
			$data['form_title_detail_sjp_bhnbaku']	= $this->lang->line('form_title_detail_sjp_bhnbaku');
			$data['form_nomor_sjp_bhnbaku']			= $this->lang->line('form_nomor_sjp_bhnbaku');
			$data['form_tanggal_sjp_bhnbaku']		= $this->lang->line('form_tanggal_sjp_bhnbaku');
			$data['form_nm_gdg_sjp_bhnbaku']		= $this->lang->line('form_nm_gdg_sjp_bhnbaku');
			$data['form_kode_produk_sjp_bhnbaku']	= $this->lang->line('form_kode_produk_sjp_bhnbaku');
			$data['form_nm_produk_sjp_bhnbaku']		= $this->lang->line('form_nm_produk_sjp_bhnbaku');
			$data['form_hjp_sjp_bhnbaku']			= $this->lang->line('form_hjp_sjp_bhnbaku');
			$data['form_unit_sjp_bhnbaku']			= $this->lang->line('form_unit_sjp_bhnbaku');
			$data['form_total_sjp_bhnbaku']			= $this->lang->line('form_total_sjp_bhnbaku');
			$data['form_total_nilai_brg_sjp_bhnbaku']	= $this->lang->line('form_total_nilai_brg_sjp_bhnbaku');
			$data['form_ket_sjp_bhnbaku']			= $this->lang->line('form_ket_sjp_bhnbaku');
			$data['form_cabang_sjp_bhnbaku']		= $this->lang->line('form_cabang_sjp_bhnbaku');
			$data['form_option_pel_sjp_bhnbaku']	= $this->lang->line('form_option_pel_sjp_bhnbaku');
			$data['form_satuan_sjp_bhnbaku']		= $this->lang->line('form_satuan_sjp_bhnbaku');
			
			$data['lpelanggan']	= "";
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$tahun	= date("Y");
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");

			$data['dateTime']	= date("m/d/Y",time());
			$data['tsj']	= $tgl."/".$bln."/".$thn;
				
			$this->load->model('sjpbhnbaku/mclass');
			
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			$qthn	= $this->mclass->get_thn();
			$qnomor	= $this->mclass->get_nomor();

			if($qthn->num_rows() > 0) {
				$th		= $qthn->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}

			if($thn==$tahun) {
				if($qnomor->num_rows() > 0)  {
					$row	= $qnomor->row_array();
					$isjcode		= $row['isjcode']+1;		
					switch(strlen($isjcode)) {
						case "1": $nomor	= "0000".$isjcode;
						break;
						case "2": $nomor	= "000".$isjcode;
						break;	
						case "3": $nomor	= "00".$isjcode;
						break;
						case "4": $nomor	= "0".$isjcode;
						break;
						case "5": $nomor	= $isjcode;
						break;	
					}
				} else {
					$nomor		= "00001";
				}
				$nomor	= $tahun.$nomor;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['nomorisj']	= $nomor;
			
			$qiterasisjpbhnbaku = $this->mclass->iterasionsjpbhnbaku();
			if($qiterasisjpbhnbaku->num_rows()>0) {
				$riterasisjpbhnbaku = $qiterasisjpbhnbaku->row();
				$noiterasisjpbhnbaku = $riterasisjpbhnbaku->iiterasicode;
			}else{
				$noiterasisjpbhnbaku = 1;
			}
			/*
			$iter = 1;
			$noiterasisjpbhnbaku2 = '';
			
			while($iter<15) {
				if(strlen($noiterasisjpbhnbaku)<15){
					$noiterasisjpbhnbaku2 .= '0';
				}
				$iter++;
			}
			
			$data['noiterasisjpbhnbaku'] = $noiterasisjpbhnbaku2.$noiterasisjpbhnbaku;
			*/
			$data['noiterasisjpbhnbaku'] = $noiterasisjpbhnbaku;
			$data['isi']	='sjpbhnbaku/vmainform';
			$this->load->view('template',$data);
		
	}

	function carisj() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$nsj	= $this->input->post('nsj')?$this->input->post('nsj'):$this->input->get_post('nsj');
		$this->load->model('sjpbhnbaku/mclass');
		$qnsj	= $this->mclass->carisj($nsj);
		if($qnsj->num_rows()>0) {
			echo "Maaf, No. SJ sdh ada!";
		} else {
		}
	}	

	function cari_cabang() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_customer	= $this->input->post('ibranch')?$this->input->post('ibranch'):$this->input->get_post('ibranch');
		$this->load->model('sjpbhnbaku/mclass');
		
		$query	= $this->mclass->getcabang($i_customer);
		if($query->num_rows()>0) {
			$c	= "";
			$cabang	= $query->result();
			foreach($cabang as $row) {
				$c.="<option value=".$row->i_branch_code." >".$row->e_branch_name." ( ".$row->e_initial." ) "."</option>";
			}
			echo "<select name=\"i_branch\" id=\"i_branch\" >
					<option value=\"\">[Pilih Cabang Pelanggan]</option>".$c."</select>";
		}
	}

	function listbhnbaku() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		//$db2 = $this->load->database('db_external', TRUE);
		
		$iterasi	= $this->uri->segment(4,0);
		$i_gudang	= $this->input->post('i_gudang')?$this->input->post('i_gudang'):$this->uri->segment(5,0);
		
		if(isset($i_gudang) && $i_gudang!=''){
			$igudang	= $i_gudang;
			$filter	= " WHERE id_gudang='$igudang' ";
		}else{
			$igudang	= 'null';
			$filter	= "";
		}
			
		$data['iterasi']	= $iterasi;

		$data['page_title']	= "ITEM BAHAN BAKU";
		$data['list']		= "";
		$data['lurl']		= base_url();
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['igudang']	= $igudang;
		
		$qtmbarang	= $this->db->query(" SELECT * FROM tm_barang ".$filter." ORDER BY nama_brg ASC ");
		$jml		= $qtmbarang->num_rows();
		
		/* Table 0f References 

		CREATE TABLE tm_gudang
		(
		  id serial NOT NULL,
		  kode_gudang character varying(3) NOT NULL,
		  nama character varying(60),
		  kode_lokasi character varying(3) NOT NULL,
		  tgl_input date,
		  tgl_update date,
		  CONSTRAINT pk_gudang PRIMARY KEY (id)
		)
		WITH (
		  OIDS=FALSE
		);
		ALTER TABLE tm_gudang OWNER TO postgres; 


		CREATE TABLE tm_barang
		(
		  kode_brg character varying(20) NOT NULL,
		  nama_brg character varying(100),
		  tgl_input date,
		  tgl_update date,
		  deskripsi character varying(100),
		  id_jenis_bahan integer,
		  satuan integer,
		  id_gudang integer,
		  kode_item character varying(5),
		  kode_motif character varying(5),
		  kode_warna character varying(5),
		  status_aktif boolean DEFAULT true,
		  nama_brg_supplier character varying(100),
		  CONSTRAINT pk_barang PRIMARY KEY (kode_brg)
		)
		WITH (
		  OIDS=FALSE
		);
		ALTER TABLE tm_barang OWNER TO postgres;		

		*/

		$qstrtmgudang	= " SELECT * FROM tm_gudang ORDER BY kode_gudang ASC LIMIT 100 ";
		$qtmgudang = $this->db->query($qstrtmgudang);

		if($qtmgudang->num_rows() > 0) {
			$result2 = $qtmgudang->result();
		}
				
		$pagination['base_url'] = base_url().'index.php/sjpbhnbaku/cform/listbhnbakunext/'.$iterasi.'/'.$igudang.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['lgudang']	= $result2;
		
		$qtmbarang2	=  $this->db->query(" SELECT * FROM tm_barang ".$filter." ORDER BY nama_brg ASC LIMIT ".$pagination['per_page']." OFFSET ".$pagination['cur_page']." ");
		if($qtmbarang2->num_rows() > 0) {
			$resulttmbarang2	= $qtmbarang2->result();
			
			$data['isi']			= $resulttmbarang2;
		}else{
			$data['isi']			= array();
		}
		
		
		$this->load->view('sjpbhnbaku/vlistformbrgjadi',$data);
	}

	function listbhnbakunext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$db2 = $this->load->database('db_external', TRUE);
				
		$iterasi	= $this->uri->segment(4,0);
		$i_gudang	= $this->uri->segment(5,0);

		if($i_gudang!='null'){
			$igudang	= $i_gudang;
			$filter	= " WHERE id_gudang='$igudang' ";
		}else{
			$igudang	= 'null';
			$filter	= "";
		}
				
		$data['iterasi']	= $iterasi;

		$data['page_title']	= "ITEM BAHAN BAKU";
		$data['list']		= "";
		$data['lurl']		= base_url();
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['igudang']	= $igudang;
		
		$qtmbarang	= $db2->query(" SELECT * FROM tm_barang ".$filter." ORDER BY nama_brg ASC ");
		$jml		= $qtmbarang->num_rows();

		$qstrtmgudang	= " SELECT * FROM tm_gudang ORDER BY kode_gudang ASC LIMIT 100 ";
		$qtmgudang = $db2->query($qstrtmgudang);

		if($qtmgudang->num_rows() > 0) {
			$result2 = $qtmgudang->result();
		}
				
		$pagination['base_url'] = base_url().'index.php/sjpbhnbaku/cform/listbhnbakunext/'.$iterasi.'/'.$igudang.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['lgudang']	= $result2;
		
		$qtmbarang2	=  $db2->query(" SELECT * FROM tm_barang ".$filter." ORDER BY nama_brg ASC LIMIT ".$pagination['per_page']." OFFSET ".$pagination['cur_page']." ");
		if($qtmbarang2->num_rows() > 0) {
		
			$resulttmbarang2	= $qtmbarang2->result();
			
			$data['isi']			= $resulttmbarang2;
		}else{
			$data['isi']			= array();
		}
		
		$this->load->view('sjpbhnbaku/vlistformbrgjadi',$data);
	}

	function flistbhnbaku() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		//$db2 = $this->load->database('db_external', TRUE);
		
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
				
		$iterasi	= $this->uri->segment(4);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "ITEM BAHAN BAKU";
		$data['lurl']		= base_url();

		$query	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg='$key' OR UPPER(nama_brg) LIKE '$key%' ORDER BY nama_brg ASC ");
		$jml	= $query->num_rows();
		
		$list	= "";
		
		if($jml>0) {
			
			$cc	= 1;
			
			foreach($query->result() as $row) {
				
				$kode_brg = trim($row->kode_brg);
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td><a href=\"javascript:settextfield('$kode_brg','$row->nama_brg','$row->id_gudang');\">".$row->kode_brg."</a></td>
				  <td><a href=\"javascript:settextfield('$kode_brg','$row->nama_brg','$row->id_gudang');\">".$row->nama_brg."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	
	function listsatuanbhnbaku() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;

		$data['page_title']	= "DAFTAR SATUAN";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('sjpbhnbaku/mclass');
		
		$query	= $this->mclass->lsatuanbhnbaku();
		$jml	= $query->num_rows();
		
		$result	= $query->result();
				
		$pagination['base_url'] = base_url().'index.php/sjpbhnbaku/cform/listsatuanbhnbaku/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		 
		$data['isi']		= $this->mclass->lsatuanbhnbakuperpages($pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('sjpbhnbaku/vlistformsatuan',$data);		
	}
		
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iteration	= $this->input->post('iteration');
		$iterasi	= $iteration;
		$i_sj		= $this->input->post('i_sj');
		$d_sj		= $this->input->post('d_sj');
		
		$ex_d_sj	= explode("/",$d_sj,strlen($d_sj));
		$dsj		= $ex_d_sj[2]."-".$ex_d_sj[1]."-".$ex_d_sj[0];
		
		$i_customer	= $this->input->post('i_customer');
		$i_branch	= $this->input->post('i_branch');
		$v_sj_total	= $this->input->post('v_sj_total');
		$e_note		= $this->input->post('e_note');
		$i_product_0	= $this->input->post('i_product'.'_'.'tblItem'.'_'.'0');
		
		$i_product	= array();
		$e_product_name	= array();
		$v_product_price= array();
		$n_unit	= array();
		$v_unit_price = array();
		$e_satuan	= array();
		$i_satuan_hidden = array();
		$i_gudang = array();
		$i_code_references = array();
		
		for($cacah=0;$cacah<=$iteration;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$i_code_references[$cacah]	= $this->input->post('i_code_references'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_product_price[$cacah]	= $this->input->post('v_product_price'.'_'.'tblItem'.'_'.$cacah);
			$n_unit[$cacah]	= $this->input->post('n_unit'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah] = $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
			$e_satuan[$cacah]	= $this->input->post('e_satuan'.'_'.'tblItem'.'_'.$cacah);
			$i_satuan_hidden[$cacah]	= $this->input->post('i_satuan_hidden'.'_'.'tblItem'.'_'.$cacah);
			$i_gudang[$cacah]	= $this->input->post('i_gudang'.'_'.'tblItem'.'_'.$cacah);
		}

		if ( !empty($i_sj) &&
		     !empty($d_sj) && 
		     !empty($v_sj_total) ) {
			 	if(!empty($i_product_0)) {
					$this->load->model('sjpbhnbaku/mclass');
					$qnsj	= $this->mclass->carisj($i_sj);
					if($qnsj->num_rows()==0) {	
						$this->mclass->msimpan($i_sj,$dsj,$v_sj_total,$e_note,$i_product,$i_code_references,$e_product_name,$v_product_price,$n_unit,$v_unit_price,$iterasi,$i_customer,$i_branch,$e_satuan,$i_satuan_hidden,$i_gudang);
					} else {
						print "<script>alert(\"Maaf, Nomor SJ tsb sebelumnya telah diinput. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
					}	
				} else {
					print "<script>alert(\"Maaf, item SJ harus terisi. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
				}
		} else {
			$data['page_title_sjp_bhnbaku']			= $this->lang->line('page_title_sjp_bhnbaku');
			$data['form_title_detail_sjp_bhnbaku']	= $this->lang->line('form_title_detail_sjp_bhnbaku');
			$data['form_nomor_sjp_bhnbaku']			= $this->lang->line('form_nomor_sjp_bhnbaku');
			$data['form_tanggal_sjp_bhnbaku']		= $this->lang->line('form_tanggal_sjp_bhnbaku');
			$data['form_nm_gdg_sjp_bhnbaku']		= $this->lang->line('form_nm_gdg_sjp_bhnbaku');
			$data['form_kode_produk_sjp_bhnbaku']	= $this->lang->line('form_kode_produk_sjp_bhnbaku');
			$data['form_nm_produk_sjp_bhnbaku']		= $this->lang->line('form_nm_produk_sjp_bhnbaku');
			$data['form_hjp_sjp_bhnbaku']			= $this->lang->line('form_hjp_sjp_bhnbaku');
			$data['form_unit_sjp_bhnbaku']			= $this->lang->line('form_unit_sjp_bhnbaku');
			$data['form_total_sjp_bhnbaku']			= $this->lang->line('form_total_sjp_bhnbaku');
			$data['form_total_nilai_brg_sjp_bhnbaku']	= $this->lang->line('form_total_nilai_brg_sjp_bhnbaku');
			$data['form_ket_sjp_bhnbaku']			= $this->lang->line('form_ket_sjp_bhnbaku');
			$data['form_cabang_sjp_bhnbaku']		= $this->lang->line('form_cabang_sjp_bhnbaku');
			$data['form_option_pel_sjp_bhnbaku']	= $this->lang->line('form_option_pel_sjp_bhnbaku');
			$data['form_satuan_sjp_bhnbaku']		= $this->lang->line('form_satuan_sjp_bhnbaku');
			
			$data['lpelanggan']	= "";		
			$data['opt_pelanggan']	= "";			
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
	
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tsj']	= $tgl."/".$bln."/".$thn;
					
			$this->load->model('sjpbhnbaku/mclass');
					
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			$qsj	= $this->mclass->get_nomorsj();
			if($qsj->num_rows() > 0 ) {
				$row_isj	= $qsj->row();
				if(strlen($row_isj->i_sj_code) > 4 ) {
					$getth	= substr($row_isj->i_sj_code,0,4);
					if($getth==date("Y")) {
						$now_thn	= $getth;
						$getnum		= substr($row_isj->i_sj_code,4,strlen($row_isj->i_sj_code)-4);
						$ch_isj	= $getnum+1;
					} else {
						$now_thn	= date("Y");
						$ch_isj	= 1;
					}
				} else {
					$now_thn	= date("Y");
					$ch_isj	= 1;
				}
			} else {
				$now_thn	= date("Y");
				$ch_isj	= 1;
			}
			
			$data['nomorisj']	= $now_thn.$ch_isj;
			
			print "<script>alert(\"Maaf, data SJ gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			
			$this->load->view('sjpbhnbaku/vmainform',$data);			
		}	
	}
	
}

?>
