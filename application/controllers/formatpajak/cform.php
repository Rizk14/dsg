<?php

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
		$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
		$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
		$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
		$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('formatpajak/mclass');		
		$query	= $this->mclass->viewformatpajak();
		if ($query->num_rows() > 0) {
			$hasil = $query->row();
			$data['segmen1'] = $hasil->segmen1;
			$data['nourut_awal'] = $hasil->nourut_awal;
			$data['nourut_akhir'] = $hasil->nourut_akhir;
		}
		else {
			$data['segmen1'] = '';
			$data['nourut_awal'] = '';
			$data['nourut_akhir'] = '';
		}
		$data['isi'] = 'formatpajak/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$segmen1	= $this->input->post('segmen1');
		$nourut_awal	= $this->input->post('nourut_awal');
		$nourut_akhir	= $this->input->post('nourut_akhir');
		
		$this->load->model('formatpajak/mclass');

		$this->mclass->msimpan($segmen1, $nourut_awal, $nourut_akhir);
		
	}
}
?>
