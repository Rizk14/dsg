<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('exportpenjualandoo/mclass');
	}
	
	function fakturdo() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
			$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
			$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
			$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
			$data['list_penjperdo_tgl_faktur_mulai']	= $this->lang->line('list_penjperdo_tgl_faktur_mulai');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
				$data['isi'] = 'exportpenjualandoo/vmainform';
	$this->load->view('template',$data);
		}
	
	
	// ===================================== 12-06-2015 ========================================================
	function exportcsvfakturdo(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			
	//$date_from = $this->input->post('d_do_first', TRUE);
	//$date_to = $this->input->post('d_do_last', TRUE);  
	// 06-07-2015
	$jenis_faktur = $this->input->post('jenis_faktur', TRUE);
	$faktur_from = $this->input->post('nofaktur_first', TRUE);
	$faktur_to = $this->input->post('nofaktur_last', TRUE);  
	$fpengganti = $this->input->post('fpengganti', TRUE);  
	
	if ($fpengganti == 'y')
		$fg = '1';
	else
		$fg = '0';
	
	$query = $this->mclass->get_fakturdo($faktur_from, $faktur_to, $fpengganti, $jenis_faktur);
	
	/*if (is_array($query)) {
		$jumdata = count($query)-1;
		$fakturterakhir = $query[$jumdata]['i_faktur_code'];
	}
	else {
		$jumdata = 0;
		$fakturterakhir = 'Tidak Ada';
	} */
	
	//print "<script>alert(\"Nomor faktur penjualan terakhir yang sudah dibikin nomor pajaknya = ".$fakturterakhir." dari nomor faktur akhir ".$faktur_to.".\");</script>";
	
	// utk download filenya
	if ($jenis_faktur == '1')
		$filename = "faktur_do_".$faktur_from."_".$faktur_to.".csv";
	else if ($jenis_faktur == '2')
		$filename = "faktur_nondo_".$faktur_from."_".$faktur_to.".csv";
	else if ($jenis_faktur == '3')
		$filename = "faktur_bhnbaku_".$faktur_from."_".$faktur_to.".csv";
	
	$now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
    
    // contoh kalo ambil dari mysql
    //$result = mysql_query($sql,$conecction);
/*	$fp = fopen('file.csv', 'w');
	//while($row = mysql_fetch_assoc($result)){
	//for($j=0;$j<count($query);$j++){
		fputcsv($fp, $query, ',');
	//}
	fclose($fp);
	*/
	
	$out = fopen('php://output', 'w');
	fputcsv($out, array("FK","KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "MASA_PAJAK", "TAHUN_PAJAK", "TANGGAL_FAKTUR", "NPWP", "NAMA", "ALAMAT_LENGKAP", "JUMLAH_DPP", "JUMLAH_PPN", "JUMLAH_PPNMBM", "ID_KETERANGAN_TAMBAHAN", "FG_UANG_MUKA", "UANG_MUKA_DPP", "UANG_MUKA_PPN", "UANG_MUKA_PPNBM", "REFERENSI", "KODE_DOKUMEN_PENDUKUNG"));
	fputcsv($out, array("LT","NPWP", "NAMA", "JALAN", "BLOK", "NOMOR", "RT", "RW", "KECAMATAN", "KELURAHAN", "KABUPATEN", "PROPINSI", "KODE_POS", "NOMOR_TELEPON"));
	fputcsv($out, array("OF","KODE_OBJEK", "NAMA", "HARGA_SATUAN", "JUMLAH_BARANG", "HARGA_TOTAL", "DISKON", "DPP", "PPN", "TARIF_PPNBM", "PPNBM"));
	
	// tambahkan coding perulangan data dari variabel $query
	if (is_array($query)) {
		//$i_faktur_code_temp = "";
		for($j=0;$j<count($query);$j++){
			fputcsv($out, array("FK","01", $fg, $query[$j]['nomor_pajak'], $query[$j]['masa_pajak'], $query[$j]['tahun_pajak'], $query[$j]['d_faktur'], $query[$j]['e_customer_npwp'], $query[$j]['e_customer_name'], $query[$j]['e_customer_address'], $query[$j]['v_total_faktur'], $query[$j]['ppn'], "0","","0","0","0","0", $query[$j]['i_faktur_code'], "0" ));
				//var_dump($query[$j]['nomor_pajak']);
				//break;
			//fputcsv($out, array("FAPR",$query[$j]['e_initial_npwp'], $query[$j]['e_initial_name'], "DANIEL SUSILO", "", "", "", "", "", "", "", "", "", $query[$j]['e_initial_phone'] ));
			
			//edit 4/3/2016
			//fputcsv($out, array("FAPR",$query[$j]['e_initial_name'], $query[$j]['e_initial_address'], "DANIEL SUSILO", "", "", "", "", "", "", "", "", "", $query[$j]['e_initial_phone'] ));
			
			fputcsv($out, array("FAPR",$query[$j]['e_initial_name'], $query[$j]['e_initial_address'], "NAOMI CHRISTIANA KUSMANTO", "", "", "", "", "", "", "", "", "", $query[$j]['e_initial_phone'] ));
			// $query[$j]['e_initial_address']
			$detailnya = $query[$j]['detail_faktur'];
			
			for($k=0;$k<count($detailnya);$k++){
				fputcsv($out, array("OF",$detailnya[$k]['i_product'], $detailnya[$k]['e_product_name'], $detailnya[$k]['v_unit_price'], $detailnya[$k]['n_quantity'], $detailnya[$k]['subtotalnondiskon'], $detailnya[$k]['diskon'], $detailnya[$k]['subtotal'], $detailnya[$k]['ppnitem'], "0", "0"));
			}
		} // end for
	}

	
	fclose($out);
	
	die();
  }
	
	// ==========================================================================================================
}
?>
