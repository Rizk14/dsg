<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('exportpenjualan/mmaster');
  }

  function fakturdo(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	//$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['isi'] = 'exportpenjualan/vmainformfakturdo';
	$this->load->view('template',$data);

  }
  
  function exportcsvfakturdo(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$fpengganti = $this->input->post('fpengganti', TRUE);  
	
	if ($fpengganti == 'y')
		$fg = '1';
	else
		$fg = '0';
	
	$query = $this->mmaster->get_fakturdo($date_from, $date_to);
	
	// utk download filenya
	$filename = "faktur_do_".$date_from."_".$date_to.".csv";
	
	//print_r($query); die();
	
	$now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
    
    // contoh kalo ambil dari mysql
    //$result = mysql_query($sql,$conecction);
/*	$fp = fopen('file.csv', 'w');
	//while($row = mysql_fetch_assoc($result)){
	//for($j=0;$j<count($query);$j++){
		fputcsv($fp, $query, ',');
	//}
	fclose($fp);
	*/
	
	$out = fopen('php://output', 'w');
	fputcsv($out, array('FK','KD_JENIS_TRANSAKSI', 'FG_PENGGANTI', 'NOMOR_FAKTUR', 'MASA_PAJAK', 'TAHUN_PAJAK', 'TANGGAL_FAKTUR', 'NPWP', 'NAMA', 'ALAMAT_LENGKAP', 'JUMLAH_DPP', 'JUMLAH_PPN', 'JUMLAH_PPNMBM', 'ID_KETERANGAN_TAMBAHAN', 'FG_UANG_MUKA', 'UANG_MUKA_DPP', 'UANG_MUKA_PPN', 'UANG_MUKA_PPNBM', 'REFERENSI'));
	fputcsv($out, array('LT','NPWP', 'NAMA', 'JALAN', 'BLOK', 'NOMOR', 'RT', 'RW', 'KECAMATAN', 'KELURAHAN', 'KABUPATEN', 'PROPINSI', 'KODE_POS', 'NOMOR_TELEPON'));
	fputcsv($out, array('OF','KODE_OBJEK', 'NAMA_BARANG', 'HARGA_SATUAN', 'JUMLAH_BARANG', 'HARGA_TOTAL', 'DISKON', 'DPP', 'PPN', 'TARIF_PPNBM', 'PPNBM'));
	
	// tambahkan coding perulangan data dari variabel $query
	if (is_array($query)) {
		$i_faktur_code_temp = "";
		for($j=0;$j<count($query);$j++){
			if ($i_faktur_code_temp != $query[$j]['i_faktur_code']) {
				$i_faktur_code_temp = $query[$j]['i_faktur_code'];
				//fputcsv($out, array('FK','\'01', $fg, '\''.$query[$j]['nomor_pajak'], $query[$j]['masa_pajak'], $query[$j]['tahun_pajak'], $query[$j]['d_faktur'], '\''.$query[$j]['e_customer_npwp'], $query[$j]['e_customer_name'], $query[$j]['e_customer_address'], $query[$j]['v_total_faktur'], $query[$j]['ppn'], '0','','0','0','0','0', $query[$j]['i_faktur_code'] ));
				//fputcsv($out, array('LT','\''.$query[$j]['e_initial_npwp'], $query[$j]['e_initial_name'], $query[$j]['e_initial_address'], '-', '-', '-', '-', '-', '-', '-', '-', '-', $query[$j]['e_initial_phone'] ));
				fputcsv($out, array('FK','01', $fg, $query[$j]['nomor_pajak'], $query[$j]['masa_pajak'], $query[$j]['tahun_pajak'], $query[$j]['d_faktur'], $query[$j]['e_customer_npwp'], $query[$j]['e_customer_name'], $query[$j]['e_customer_address'], $query[$j]['v_total_faktur'], $query[$j]['ppn'], '0','','0','0','0','0', $query[$j]['i_faktur_code'] ));
				fputcsv($out, array('LT',$query[$j]['e_initial_npwp'], $query[$j]['e_initial_name'], $query[$j]['e_initial_address'], '-', '-', '-', '-', '-', '-', '-', '-', '-', $query[$j]['e_initial_phone'] ));
				fputcsv($out, array('OF',$query[$j]['i_product'], $query[$j]['e_product_name'], $query[$j]['v_unit_price'], $query[$j]['n_quantity'], $query[$j]['subtotal'], '0', $query[$j]['subtotal'], $query[$j]['ppnitem'], '0', '0'));
			}
			else {
				fputcsv($out, array('OF',$query[$j]['i_product'], $query[$j]['e_product_name'], $query[$j]['v_unit_price'], $query[$j]['n_quantity'], $query[$j]['subtotal'], '0', $query[$j]['subtotal'], $query[$j]['ppnitem'], '0', '0'));
			}
		}
	}
	
	fclose($out);
	
	die();
  }
  
  //====================================================================================================

}
