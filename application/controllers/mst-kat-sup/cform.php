<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-kat-sup/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode_kategory = $row->kode_kategory;
			$enama = $row->nama;
		}
	}
	else {
			$eid = '';
			$ekode_kategory = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode_kategory'] = $ekode_kategory;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	$data['msg'] = '';
	$data['query'] = $this->mmaster->getAll();
	
    $data['isi'] = 'mst-kat-sup/vmainform';
	$this->load->view('template',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		//if ($goedit == 1) {
			$this->form_validation->set_rules('kode_kategory', 'Kode kategory', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		//}
		
		if ($this->form_validation->run() == FALSE)
		{
			//redirect('mst-kat-sup/cform');
			$data['isi'] = 'mst-kat-sup/vmainform';
			$data['msg'] = 'Kode Gudang dan Nama harus diisi..!';
			
			$eid = '';
			$ekode_kategory = '';
			$enama = '';
			
			$edit = '';
			$data['eid'] = $eid;
			$data['ekode_kategory'] = $ekode_kategory;
			$data['enama'] = $enama;
			
			$data['edit'] = $edit;
			$data['query'] = $this->mmaster->getAll();
			$this->load->view('template',$data);
		}
		else
		{
			$kode_kategory 	= $this->input->post('kode_kategory', TRUE);
			$kodeeditkat 	= $this->input->post('kodeeditkat', TRUE); 
			//~ $kodeeditgud 	= $this->input->post('kodeeditgud', TRUE); 
			$id_kategory	 	= $this->input->post('id_kategory', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$jenis 	= $this->input->post('jenis', TRUE);
			
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($kode_kategory);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-kat-sup/vmainform';
					$data['msg'] = 'Data sudah ada..!';
					
					$eid = '';
					$ekode_kategory = '';
					$enama = '';
					$eid_lokasi = '';
					$ejenis = '';
					$edit = '';
					$data['eid'] = $eid;
					$data['ekode_kategory'] = $ekode_kategory;
					$data['enama'] = $enama;
					$data['ejenis'] = $ejenis;
					$data['edit'] = $edit;
					$data['query'] = $this->mmaster->getAll();
					
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save( $kode_kategory, $id_kategory, $nama, $goedit);
					/*$data['isi'] = 'mst-kat-sup/vmainform';
					$data['msg'] = 'Simpan data berhasil';
					$this->load->view('template',$data); */
					redirect('mst-kat-sup/cform');
				}
			} // end if goedit == ''
			else {
					$cek_data2 = $this->mmaster->cek_data($kode_kategory);
				if ($kode_kategory != $kodeeditkat) {
					if (count($cek_data2) == 0) {
						$this->mmaster->save( $kode_kategory, $id_kategory, $nama, $goedit);
						redirect('mst-kat-sup/cform');
					}
					else {
						$data['isi'] = 'mst-kat-sup/vmainform';
						$data['msg'] = 'Data sudah ada..!';
						
						$edit = '1';
						$data['eid'] = $id_kategory;
						$data['ekode_kategory'] = $kode_kategory;
						$data['enama'] = $nama;
						$data['eid_lokasi'] = $id_lokasi;
						$data['ejenis'] = $jenis;
						$data['edit'] = $edit;
						$data['query'] = $this->mmaster->getAll();
						$this->load->view('template',$data);
					}
				}
				else {
					$this->mmaster->save($kode_kategory, $id_kategory, $nama, $goedit);
					redirect('mst-kat-sup/cform');
				}
			}
		}
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-kat-sup/cform');
  }
  
  
}
