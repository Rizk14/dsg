<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			$data['page_title_bmk']	= $this->lang->line('page_title_bmk');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['form_kepada_bmk']	= $this->lang->line('form_kepada_bmk');
			$data['form_option_kepada_bmk']	= $this->lang->line('form_option_kepada_bmk');
			$data['form_nomor_bmk']	= $this->lang->line('form_nomor_bmk');
			$data['form_tgl_bmk']	= $this->lang->line('form_tgl_bmk');
			$data['form_title_detail_bmk']	= $this->lang->line('form_title_detail_bmk');
			$data['form_kode_produk_bmk']	= $this->lang->line('form_kode_produk_bmk');
			$data['form_nm_produk_bmk']	= $this->lang->line('form_nm_produk_bmk');
			$data['form_jml_bmk']	= $this->lang->line('form_jml_bmk');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['lkepada']	= "";
		
			$tahun	= date("Y");

			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");

			$data['dateTime']	= date("m/d/Y",time());
			$data['tbonmkeluar']	= $tgl."/".$bln."/".$thn;
			$data['dbonmkeluar'] = $thn."-".$bln."-".$tgl;
				
			$this->load->model('bonmkeluargd/mclass');

			$qthn	= $this->mclass->get_thnbonmkeluar();
			$qbonmkeluar	= $this->mclass->get_nomorbonmkeluar();

			if($qthn->num_rows() > 0) {
				$th		= $qthn->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}
		
			if($thn==$tahun) {
				if($qbonmkeluar->num_rows() > 0)  {
					$row	= $qbonmkeluar->row_array();
					$ibonmkeluar	= $row['icode']+1;
				
					switch(strlen($ibonmkeluar)) {
						case "1":
							$nomorbonmkeluar	= "0000".$ibonmkeluar;
						break;
						case "2":
							$nomorbonmkeluar	= "000".$ibonmkeluar;
						break;	
						case "3":
							$nomorbonmkeluar	= "00".$ibonmkeluar;
						break;
						case "4":
							$nomorbonmkeluar	= "0".$ibonmkeluar;
						break;
						case "5":
							$nomorbonmkeluar	= $ibonmkeluar;
						break;	
					}
				} else {
					$nomorbonmkeluar		= "00001";
				}
				$nomor	= $tahun.$nomorbonmkeluar;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['no']	= $nomor;
			$data['opt_kepada']	= $this->mclass->lkepada();
		
			$data['isi']	= 'bonmkeluargd/vmainform';	
			$this->load->view('template',$data);
			
		}
	

	function listbarangjadi(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iterasi	= $this->uri->segment(4,0);
		$doutbonm = $this->uri->segment(5,0); // YYYY-mm-dd

		$expdoutbonm	= explode("-",$doutbonm,strlen($doutbonm));
		$bulan	= $expdoutbonm[1];
		$tahun	= $expdoutbonm[0];
				
		$data['doutbonm']	= $doutbonm;
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('bonmkeluargd/mclass');

		$query	= $this->mclass->lbarangjadi($bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->lbarangjadiopsi($bulan,$tahun);
			$jml	= $query->num_rows();
		}
		$result	= $query->result();
				
		$pagination['base_url'] = base_url().'index.php/bonmkeluargd/cform/listbarangjadinext/'.$iterasi.'/'.$doutbonm.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']		= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpagesopsi($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
			
			$this->load->view('bonmkeluargd/vlistformbrgjadi',$data);
		
	}

	function listbarangjadinext(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iterasi	= $this->uri->segment(4,0);
		$doutbonm	= $this->uri->segment(5,0);

		$expdoutbonm	= explode("-",$doutbonm,strlen($doutbonm));
		$bulan	= $expdoutbonm[1];
		$tahun	= $expdoutbonm[0];
				
		$data['doutbonm']	= $doutbonm;			
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('bonmkeluargd/mclass');

		$query	= $this->mclass->lbarangjadi($bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->lbarangjadiopsi($bulan,$tahun);
			$jml	= $query->num_rows();
		}
		$result	= $query->result();
				
		$pagination['base_url'] = base_url().'index.php/bonmkeluargd/cform/listbarangjadinext/'.$iterasi.'/'.$doutbonm.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']		= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpagesopsi($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		
		$this->load->view('bonmkeluargd/vlistformbrgjadi',$data);
	}	

	function flistbarangjadi(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$key		= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$doutbonm	= $this->input->post('doutbonm')?$this->input->post('doutbonm'):$this->input->get_post('doutbonm');

		$expdoutbonm	= explode("-",$doutbonm,strlen($doutbonm));
		$bulan	= $expdoutbonm[1];
		$tahun	= $expdoutbonm[0];
				
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('bonmkeluargd/mclass');

		$query	= $this->mclass->flbarangjadi($key,$bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->flbarangjadiopsi($key,$bulan,$tahun);
			$jml	= $query->num_rows();
		}
		
		$list	= "";
		if($jml>0) {
			$db2=$this->load->database('db_external', TRUE);
			$cc	= 1; 
			foreach($query->result() as $row){
				
				//--------------08-01-2014 23-10-2014 ----------------------------
				// ambil data warna dari tr_product_color
				$sqlxx	= $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b 
										 WHERE a.i_color=b.i_color AND a.i_product_motif = '$row->imotif' ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0){
					$hasilxx = $sqlxx->result();
					
					foreach ($hasilxx as $rownya) {
						$listwarna.= $rownya->e_color_name."<br>";
					}
				}
				
				//-----------------------------------------------------
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->qty','$row->stp','$row->iso')\">".$row->imotif."</a></td>
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->qty','$row->stp','$row->iso')\">".$row->motifname."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->qty','$row->stp','$row->iso')\">".$row->qty."</a></td>	
				  <td>".$listwarna."</td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;		
	}
		
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration;
		$e_to_outbonm	= $this->input->post('e_to_outbonm');
		$i_outbonm	= $this->input->post('i_outbonm');
		$d_outbonm	= $this->input->post('d_outbonm');
		
		$exp_d_outbonm	= explode("/",$d_outbonm,strlen($d_outbonm));
		$doutbonm	= $exp_d_outbonm[2]."-".$exp_d_outbonm[1]."-".$exp_d_outbonm[0];
		
		if($exp_d_outbonm[2]=='' || $exp_d_outbonm[1]=='' || $exp_d_outbonm[0]=='')
			$doutbonm	= date("Y-m-d");
		
		$i_product	= array();
		$e_product_name	= array();
		$n_count_product	= array();
		$qty_product	= array();
		$f_stp	= array();
		$iso	= array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah);
			$n_count_product[$cacah]	= $this->input->post('n_count_product_'.'tblItem'.'_'.$cacah);
			$qty_product[$cacah]	= $this->input->post('qty_product_'.'tblItem'.'_'.$cacah);
			
			$qty_warna[$cacah]	= $this->input->post('qty_warna_'.$cacah);
			$i_product_color[$cacah]	= $this->input->post('i_product_color_'.$cacah);
			$i_color[$cacah]	= $this->input->post('i_color_'.$cacah);
			
			$f_stp[$cacah]	= $this->input->post('f_stp_'.'tblItem'.'_'.$cacah);
			$iso[$cacah]	= $this->input->post('iso_'.'tblItem'.'_'.$cacah);
		}
		
		$this->load->model('bonmkeluargd/mclass');
		
		if( !empty($e_to_outbonm) && 
		    !empty($i_outbonm) && 
		    !empty($d_outbonm) ) {
			if(!empty($i_product_0)) {
				$qcekbonmkeluar	= $this->mclass->caribonmkeluar($i_outbonm);
				if($qcekbonmkeluar->num_rows()==0) {	
					$this->mclass->msimpan($e_to_outbonm,$i_outbonm,$doutbonm,$i_product,$e_product_name,
					$qty_warna, $i_product_color, $i_color, $n_count_product,$qty_product,$iterasi,$f_stp,$iso);
				}else{
					print "<script>alert(\"Maaf, Data Bon M Keluar gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
				}
			}else{
				print "<script>alert(\"Maaf, item utk Bon M Keluar harus terisi. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}
		}else{
			$data['page_title_bmk']	= $this->lang->line('page_title_bmk');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['form_kepada_bmk']	= $this->lang->line('form_kepada_bmk');
			$data['form_option_kepada_bmk']	= $this->lang->line('form_option_kepada_bmk');
			$data['form_nomor_bmk']	= $this->lang->line('form_nomor_bmk');
			$data['form_tgl_bmk']	= $this->lang->line('form_tgl_bmk');
			$data['form_title_detail_bmk']	= $this->lang->line('form_title_detail_bmk');
			$data['form_kode_produk_bmk']	= $this->lang->line('form_kode_produk_bmk');
			$data['form_nm_produk_bmk']	= $this->lang->line('form_nm_produk_bmk');
			$data['form_jml_bmk']	= $this->lang->line('form_jml_bmk');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['lkepada']	= "";
			
			$tahun	= date("Y");
	
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tbonmkeluar']	= $tgl."/".$bln."/".$thn;
	
			$qthn	= $this->mclass->get_thnbonmkeluar();
			$qbonmkeluar	= $this->mclass->get_nomorbonmkeluar();
	
			if($qthn->num_rows() > 0) {
				$th		= $qthn->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}
			
			if($thn==$tahun) {
				if($qbonmkeluar->num_rows() > 0)  {
					$row	= $qbonmkeluar->row_array();
					$ibonmkeluar	= $row['icode']+1;
					
					switch(strlen($ibonmkeluar)) {
						case "1":
							$nomorbonmkeluar	= "0000".$ibonmkeluar;
						break;
						case "2":
							$nomorbonmkeluar	= "000".$ibonmkeluar;
						break;	
						case "3":
							$nomorbonmkeluar	= "00".$ibonmkeluar;
						break;
						case "4":
							$nomorbonmkeluar	= "0".$ibonmkeluar;
						break;
						case "5":
							$nomorbonmkeluar	= $ibonmkeluar;
						break;	
					}
				} else {
					$nomorbonmkeluar		= "00001";
				}
				$nomor	= $tahun.$nomorbonmkeluar;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['no']	= $nomor;
			$data['opt_kepada']	= $this->mclass->lkepada();
			
			print "<script>alert(\"Maaf, Data Bon M Keluar gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			$data['isi']	= 'bonmkeluargd/vmainform';	
			$this->load->view('template',$data);
		}		
	}

	function simpan_old() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration;
		$e_to_outbonm	= $this->input->post('e_to_outbonm')?$this->input->post('e_to_outbonm'):$this->input->get_post('e_to_outbonm');
		$i_outbonm	= $this->input->post('i_outbonm')?$this->input->post('i_outbonm'):$this->input->get_post('i_outbonm');
		$d_outbonm	= $this->input->post('d_outbonm')?$this->input->post('d_outbonm'):$this->input->get_post('d_outbonm');
		
		$exp_d_outbonm	= explode("/",$d_outbonm,strlen($d_outbonm)); // dd/mm/YYYY
		$doutbonm	= $exp_d_outbonm[2]."-".$exp_d_outbonm[1]."-".$exp_d_outbonm[0];
		
		$i_product	= array();
		$e_product_name	= array();
		$n_count_product	= array();
		$qty_product	= array();
		$f_stp	= array();
		$iso	= array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem'.'_'.$cacah)?$this->input->post('i_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah)?$this->input->post('e_product_name_'.'tblItem'.'_'.$cacah):$this->input->get_post('e_product_name_'.'tblItem'.'_'.$cacah);
			$n_count_product[$cacah]	= $this->input->post('n_count_product_'.'tblItem'.'_'.$cacah)?$this->input->post('n_count_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('n_count_product_'.'tblItem'.'_'.$cacah);
			$qty_product[$cacah]	= $this->input->post('qty_product_'.'tblItem'.'_'.$cacah)?$this->input->post('qty_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('qty_product_'.'tblItem'.'_'.$cacah);
			$f_stp[$cacah]	= $this->input->post('f_stp_'.'tblItem'.'_'.$cacah)?$this->input->post('f_stp_'.'tblItem'.'_'.$cacah):$this->input->get_post('f_stp_'.'tblItem'.'_'.$cacah);
			$iso[$cacah]	= $this->input->post('iso_'.'tblItem'.'_'.$cacah)?$this->input->post('iso_'.'tblItem'.'_'.$cacah):$this->input->get_post('iso_'.'tblItem'.'_'.$cacah);
		}

		if( !empty($e_to_outbonm) && 
		    !empty($i_outbonm) && 
		    !empty($d_outbonm) ) {
			if(!empty($i_product_0)) {
				
				$this->load->model('bonmkeluargd/mclass');
				
				$qcekbonmkeluar	= $this->mclass->caribonmkeluar($i_outbonm);
				if($qcekbonmkeluar->num_rows()<0 || $qcekbonmkeluar->num_rows()==0) {	
					$this->mclass->msimpan($e_to_outbonm,$i_outbonm,$doutbonm,$i_product,$e_product_name,$n_count_product,$qty_product,$iterasi,$f_stp,$iso);
				}else{
					print "<script>alert(\"Maaf, Data Bon M Keluar gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
				}
			} else {
				print "<script>alert(\"Maaf, item utk Bon M Keluar harus terisi. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}
		} else {
			$data['page_title_bmk']	= $this->lang->line('page_title_bmk');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['form_kepada_bmk']	= $this->lang->line('form_kepada_bmk');
			$data['form_option_kepada_bmk']	= $this->lang->line('form_option_kepada_bmk');
			$data['form_nomor_bmk']	= $this->lang->line('form_nomor_bmk');
			$data['form_tgl_bmk']	= $this->lang->line('form_tgl_bmk');
			$data['form_title_detail_bmk']	= $this->lang->line('form_title_detail_bmk');
			$data['form_kode_produk_bmk']	= $this->lang->line('form_kode_produk_bmk');
			$data['form_nm_produk_bmk']	= $this->lang->line('form_nm_produk_bmk');
			$data['form_jml_bmk']	= $this->lang->line('form_jml_bmk');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['lkepada']	= "";
			
			$tahun	= date("Y");
	
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tbonmkeluar']	= $tgl."/".$bln."/".$thn;
					
			$this->load->model('bonmkeluargd/mclass');
	
			$qthn	= $this->mclass->get_thnbonmkeluar();
			$qbonmkeluar	= $this->mclass->get_nomorbonmkeluar();
	
			if($qthn->num_rows() > 0) {
				$th		= $qthn->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}
			
			if($thn==$tahun) {
				if($qbonmkeluar->num_rows() > 0)  {
					$row	= $qbonmkeluar->row_array();
					$ibonmkeluar	= $row['icode']+1;
					
					switch(strlen($ibonmkeluar)) {
						case "1":
							$nomorbonmkeluar	= "0000".$ibonmkeluar;
						break;
						case "2":
							$nomorbonmkeluar	= "000".$ibonmkeluar;
						break;	
						case "3":
							$nomorbonmkeluar	= "00".$ibonmkeluar;
						break;
						case "4":
							$nomorbonmkeluar	= "0".$ibonmkeluar;
						break;
						case "5":
							$nomorbonmkeluar	= $ibonmkeluar;
						break;	
					}
				} else {
					$nomorbonmkeluar		= "00001";
				}
				$nomor	= $tahun.$nomorbonmkeluar;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['no']	= $nomor;
			$data['opt_kepada']	= $this->mclass->lkepada();
			
			print "<script>alert(\"Maaf, Data Bon M Keluar gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			$data['isi']	= 'bonmkeluargd/vmainform';	
			$this->load->view('template',$data);
		}		
	}
		
	function caribonmkeluar() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$nbonmkeluar	= $this->input->post('nbonmkeluar')?$this->input->post('nbonmkeluar'):$this->input->get_post('nbonmkeluar');
		$this->load->model('bonmkeluargd/mclass');
		$qnkeluar	= $this->mclass->caribonmkeluar($nbonmkeluar);
		if($qnkeluar->num_rows()>0) {
			echo "Maaf, No. Bon M sdh ada!";
		}	
	}
	
	// 23-10-2014
	function additemwarna(){
		$is_logged_in = $this->session->userdata('is_logged_in');
if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$db2=$this->load->database('db_external', TRUE);
		$iproductmotif 	= $this->input->post('iproductmotif', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
									WHERE a.i_color = b.i_color AND a.i_product_motif = '".$iproductmotif."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'i_product_color'=> $rowxx->i_product_color,
										'i_color'=> $rowxx->i_color,
										'e_color_name'=> $rowxx->e_color_name
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['iproductmotif'] = $iproductmotif;
		$data['posisi'] = $posisi;
		$this->load->view('bonmkeluargd/vlistwarna', $data); 
		//print_r($detailwarna); die();
		//echo $iproductmotif; die();
		return true;
  }
}
?>
