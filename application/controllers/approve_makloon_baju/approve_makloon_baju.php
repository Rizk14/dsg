<?php
class approve_makloon_baju extends CI_Controller
{
    public $data = array(
        'halaman' => 'approve_makloon_baju',
        'isi' => 'approve_makloon_baju/approve_makloon_baju_list',
        'title' => 'Data Approve Makloon Baju',
    );

    
     public function __construct()
    {
        parent::__construct();
        $this->load->model('approve_makloon_baju/approve_makloon_baju_model', 'approve_makloon_baju');
    }
    
     public function view($offset = 0)
    {
        $approve_makloon_baju = $this->approve_makloon_baju->get_all_paged_custom($offset);
        if ($approve_makloon_baju) {
            $this->data['approve_makloon_baju'] = $approve_makloon_baju;
            $this->data['paging'] = $this->approve_makloon_baju->paging('biasa', site_url('approve_makloon_baju/approve_makloon_baju/halaman/'), 4);
        } else {
            $this->data['approve_makloon_baju'] = 'Tidak ada data Approve Makloon Baju.';
        }
        $this->load->view('template', $this->data);
    }

    public function cari($offset = 0)
    {
        $approve_makloon_baju = $this->approve_makloon_baju->cari($offset);
        if ($approve_makloon_baju) {
            $this->data['approve_makloon_baju'] = $approve_makloon_baju;
            $this->data['paging'] = $this->approve_makloon_baju->paging('pencarian', site_url('approve_makloon_baju/approve_makloon_baju/cari/'), 4);
        } else {
            $this->data['approve_makloon_baju'] = 'Data tidak ditemukan.'. anchor('approve_makloon_baju/approve_makloon_baju', ' Tampilkan semua approve_makloon_baju.', 'class="alert-link"');
        }
        $this->load->view('template', $this->data);
    }

   
    public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data Approve Makloon Baju';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data Approve Makloon Baju';
        $this->load->view('template', $this->data);
    }

   

    // Ubah status verifikasi
    public function ubah_status_verifikasi($id)
    {
        // Pastikan data approve_makloon_baju ada.
        $approve_makloon_baju = $this->approve_makloon_baju->get($id);
        if (! $approve_makloon_baju) {
            $this->session->set_flashdata('pesan_error', 'Data Approve Makloon Baju tidak ada. Kembali ke halaman ' . anchor('approve_makloon_baju/approve_makloon_baju/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju/approve_makloon_baju/error');
        }

        // Ubah status verifikasi
        if ($this->approve_makloon_baju->ubah_status_approve($id, $approve_makloon_baju->status_approve)) {
            $this->session->set_flashdata('pesan', 'Status verifikasi berhasil diubah. Kembali ke halaman ' . anchor('approve_makloon_baju/approve_makloon_baju/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju/approve_makloon_baju/sukses');
        } else {
            $this->session->set_flashdata('pesan', 'Status verifikasi gagal diubah. Kembali ke halaman ' . anchor('approve_makloon_baju/approve_makloon_baju/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju/approve_makloon_baju/error');
        }
    }

    
    public function link_detail()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $approve_makloon_baju = $this->approve_makloon_baju->getAllDetail($id);
        if (! $approve_makloon_baju) {
            $this->session->set_flashdata('pesan_error', 'Data Detail Makloon Baju Gudang Jadi Wip tidak ada. Kembali ke halaman ' . anchor('approve_makloon_baju/approve_makloon_baju/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju/master-barang/error');
        }
		$this->data['list_unit_jahit'] = $this->approve_makloon_baju->get_unit_jahit(); 
		$this->data['list_unit_packing'] = $this->approve_makloon_baju->get_unit_packing(); 
		$this->data['list_gudang'] = $this->approve_makloon_baju->get_gudang(); 
        $this->data['values'] = $approve_makloon_baju;
		$this->data['isi'] = 'approve_makloon_baju/approve_makloon_baju_form_edit';
        $this->load->view('template', $this->data);
	 }

    
}
