<?php
include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0) 
		{	
			$data['page_title_fpajakndo']	= $this->lang->line('page_title_fpajakndo');
			$data['form_title_detail_fpajakndo']	= $this->lang->line('form_title_detail_fpajakndo');
			$data['list_fpajakndo_faktur']	= $this->lang->line('list_fpajakndo_faktur');
			$data['list_fpajakndo_kd_brg']	= $this->lang->line('list_fpajakndo_kd_brg');
			$data['list_fpajakndo_jenis_brg']	= $this->lang->line('list_fpajakndo_jenis_brg');
			$data['list_fpajakndo_s_produksi'] = $this->lang->line('list_fpajakndo_s_produksi');
			$data['list_fpajakndo_no_do']	= $this->lang->line('list_fpajakndo_no_do');
			$data['list_fpajakndo_nm_brg']	= $this->lang->line('list_fpajakndo_nm_brg');
			$data['list_fpajakndo_qty']	= $this->lang->line('list_fpajakndo_qty');
			$data['list_fpajakndo_hjp']	= $this->lang->line('list_fpajakndo_hjp');
			$data['list_fpajakndo_amount']	= $this->lang->line('list_fpajakndo_amount');
			$data['list_fpajakndo_total_pengiriman']	= $this->lang->line('list_fpajakndo_total_pengiriman');
			$data['list_fpajakndo_total_penjualan']	= $this->lang->line('list_fpajakndo_total_penjualan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			$this->load->model('prntfpajakndo_test/mclass');
			$data['opt_jns_brg']	= $this->mclass->lklsbrg();
			$this->load->view('prntfpajakndo_test/vmainform',$data);
		}
	}

	function listbarangjadi() {
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpajakndo_test/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/prntfpajakndo_test/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('prntfpajakndo_test/vlistformbrgjadi',$data);			
	}
	
	function listbarangjadinext() {
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntfpajakndo_test/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntfpajakndo_test/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('prntfpajakndo_test/vlistformbrgjadi',$data);			
	}

	function flistbarangjadi() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('prntfpajakndo_test/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}		

	function carilistpenjualanndo() {	
		
		$nofaktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->input->get_post('no_faktur');
		$ecategory	= $this->input->post('e_category')?$this->input->post('e_category'):$this->input->get_post('e_category');
		$fstopproduksi	= $this->input->post('f_stop_produksi')?$this->input->post('f_stop_produksi'):$this->input->get_post('f_stop_produksi');

		$data['nofaktur']	= $nofaktur;
		$data['kategori']	= $ecategory;
		$data['sproduksi']	= $fstopproduksi=1?" checked ":"";
		
		$data['page_title_fpajakndo']	= $this->lang->line('page_title_fpajakndo');
		$data['form_title_detail_fpajakndo']	= $this->lang->line('form_title_detail_fpajakndo');
		$data['list_fpajakndo_faktur']	= $this->lang->line('list_fpajakndo_faktur');
		$data['list_fpajakndo_kd_brg']	= $this->lang->line('list_fpajakndo_kd_brg');
		$data['list_fpajakndo_jenis_brg']	= $this->lang->line('list_fpajakndo_jenis_brg');
		$data['list_fpajakndo_s_produksi'] = $this->lang->line('list_fpajakndo_s_produksi');
		$data['list_fpajakndo_no_do']	= $this->lang->line('list_fpajakndo_no_do');
		$data['list_fpajakndo_nm_brg']	= $this->lang->line('list_fpajakndo_nm_brg');
		$data['list_fpajakndo_qty']	= $this->lang->line('list_fpajakndo_qty');
		$data['list_fpajakndo_hjp']	= $this->lang->line('list_fpajakndo_hjp');
		$data['list_fpajakndo_amount']	= $this->lang->line('list_fpajakndo_amount');
		$data['list_fpajakndo_total_pengiriman']	= $this->lang->line('list_fpajakndo_total_pengiriman');
		$data['list_fpajakndo_total_penjualan']	= $this->lang->line('list_fpajakndo_total_penjualan');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";
		$data['limages']	= base_url();
				
		$this->load->model('prntfpajakndo_test/mclass');
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		$data['isi']	= $this->mclass->clistpenjualanndo($nofaktur);
		$this->load->view('prntfpajakndo_test/vlistform',$data);
	}
	
	function cpenjualanndo() {			
		$data['page_title_fpajakndo']	= $this->lang->line('page_title_fpajakndo');
		$data['form_title_detail_fpajakndo']	= $this->lang->line('form_title_detail_fpajakndo');
		$data['list_fpajakndo_faktur']	= $this->lang->line('list_fpajakndo_faktur');
		$data['list_fpajakndo_kd_brg']	= $this->lang->line('list_fpajakndo_kd_brg');
		$data['list_fpajakndo_jenis_brg']	= $this->lang->line('list_fpajakndo_jenis_brg');
		$data['list_fpajakndo_s_produksi'] = $this->lang->line('list_fpajakndo_s_produksi');
		$data['list_fpajakndo_no_do']	= $this->lang->line('list_fpajakndo_no_do');
		$data['list_fpajakndo_nm_brg']	= $this->lang->line('list_fpajakndo_nm_brg');
		$data['list_fpajakndo_qty']	= $this->lang->line('list_fpajakndo_qty');
		$data['list_fpajakndo_hjp']	= $this->lang->line('list_fpajakndo_hjp');
		$data['list_fpajakndo_amount']	= $this->lang->line('list_fpajakndo_amount');
		$data['list_fpajakndo_total_pengiriman']	= $this->lang->line('list_fpajakndo_total_pengiriman');
		$data['list_fpajakndo_total_penjualan']	= $this->lang->line('list_fpajakndo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();
		
		$nofaktur	= $this->uri->segment(4); 
		$data['nofaktur']	= $nofaktur;
				
		$this->load->model('prntfpajakndo_test/mclass');
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		$data['isi']	= $this->mclass->clistpenjualanndo($nofaktur);
		$this->load->view('prntfpajakndo_test/vprintform',$data);
	}	

	function cpopup() {

		/*
		Nomor Seri Faktur Pajak : 010.000.10.00000001
		011	= pada digit titik pertama jensi faktur, 011 jika faktur pengganti
		10	= pada digit titik ke-tiga adalah tahun sekarang
		*/

		$iduserid	= $this->session->userdata('user_idx');	
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.194';
		//$host		= '192.168.0.134';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;	
			
		$data['page_title_fpajakndo']	= $this->lang->line('page_title_fpajakndo');
		$data['form_title_detail_fpajakndo']	= $this->lang->line('form_title_detail_fpajakndo');
		$data['list_fpajakndo_faktur']	= $this->lang->line('list_fpajakndo_faktur');
		$data['list_fpajakndo_kd_brg']	= $this->lang->line('list_fpajakndo_kd_brg');
		$data['list_fpajakndo_jenis_brg']	= $this->lang->line('list_fpajakndo_jenis_brg');
		$data['list_fpajakndo_s_produksi'] = $this->lang->line('list_fpajakndo_s_produksi');
		$data['list_fpajakndo_no_do']	= $this->lang->line('list_fpajakndo_no_do');
		$data['list_fpajakndo_nm_brg']	= $this->lang->line('list_fpajakndo_nm_brg');
		$data['list_fpajakndo_qty']	= $this->lang->line('list_fpajakndo_qty');
		$data['list_fpajakndo_hjp']	= $this->lang->line('list_fpajakndo_hjp');
		$data['list_fpajakndo_amount']	= $this->lang->line('list_fpajakndo_amount');
		$data['list_fpajakndo_total_pengiriman']	= $this->lang->line('list_fpajakndo_total_pengiriman');
		$data['list_fpajakndo_total_penjualan']	= $this->lang->line('list_fpajakndo_total_penjualan');
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();

		$data['TtdPajak01']	= $this->lang->line('TtdPajak01');
		$data['TtdPajak02']	= $this->lang->line('TtdPajak02');
		$data['falamat']	= $this->lang->line('AddInisial');
				
		$nofaktur	= $this->uri->segment(4); 
		
		$this->load->model('prntfpajakndo_test/mclass');
		
		$this->mclass->fprinted($nofaktur);
	
		//$qry_source_remote	= $this->mclass->remote($remote);
		$qry_source_remote	= $this->mclass->remote($iduserid);

		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;
				
		$qry_infoheader		= $this->mclass->clistfpenjndo_header($nofaktur);
		
		if($qry_infoheader->num_rows() > 0){
			$bglobal	= array(
						'01'=>'Januari',
						'02'=>'Februari',
						'03'=>'Maret',
						'04'=>'April',
						'05'=>'Mei',
						'06'=>'Juni',
						'07'=>'Juli',
						'08'=>'Agustus',
						'09'=>'September',
						'10'=>'Oktober',
						'11'=>'Nopember',
						'12'=>'Desember');

			$tanggal= date("d");
			$bulan	= date("m");
			$tahun	= date("Y");
			
			$tgl	= substr($tanggal,0,1)=='0'?substr($tanggal,1,1):$tanggal;
			$nw_tgl	= $tgl;
			$bln	= $bglobal[$bulan];
			
			$data['ftgl']	= $nw_tgl." ".$bln." ".$tahun;
			
			$row_infoheader	= $qry_infoheader->row();
			$nofaktur		= $row_infoheader->ifakturcode;
			$tglfaktur		= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$tglpajak		= $row_infoheader->dpajak;
											
			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$exp_tpajak		= explode("-",$tglpajak,strlen($tglpajak));	
			
			$tahunpajak	= !empty($exp_tpajak[0])?substr($exp_tpajak[0],2,2):date("y");
			
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];

			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']		= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;			
		}
		
		$qry_jml	= $this->mclass->clistfpenjndo_jml($nofaktur);
		
		if($qry_jml->num_rows() > 0 ) {
			$row_jml = $qry_jml->row_array();
			$total	 = $row_jml['total'];
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			$total_pls_disc	= $total - $v_disc; // DPP
			$nilai_ppn		= (($total_pls_disc*10) / 100); // PPN
			$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			
			$data['jumlah']		= $total;
			$data['dpp']		= $total_pls_disc;
			$data['diskon']		= $v_disc;
			$data['nilai_ppn']	= $nilai_ppn;
			$data['nilai_faktur']	= round($nilai_faktur);		
		}

		$ititas		= $this->mclass->ititas();
		if($ititas->num_rows() > 0) {
			$row_ititas	= $ititas->row();
			$data['nminisial']	= $row_ititas->e_initial_name;
			$data['almtperusahaan']	= $row_ititas->e_initial_address;
			$data['npwpperusahaan']	= $row_ititas->e_initial_npwp;
		}

		$nopajak	= $this->mclass->pajak($nofaktur);
		if($nopajak->num_rows() > 0) {
			$row_pajak	= $nopajak->row();			
			$nomorpajak	= $row_pajak->ifakturpajak;			
			
			switch(strlen($nomorpajak)) {
				case "1":
					$nfaktur	= '0000000'.$nomorpajak;
				break;
				case "2":
					$nfaktur	= '000000'.$nomorpajak;
				break;
				case "3":
					$nfaktur	= '00000'.$nomorpajak;
				break;
				case "4":
					$nfaktur	= '0000'.$nomorpajak;
				break;
				case "5":
					$nfaktur	= '000'.$nomorpajak;
				break;
				case "6":
					$nfaktur	= '00'.$nomorpajak;
				break;
				case "7":
					$nfaktur	= '0'.$nomorpajak;
				break;
				default:
					$nfaktur	= $nomorpajak;
			}
			$data['nomorpajak']	= '010'.'.'.'000'.'-'.$tahunpajak.'.'.$nfaktur;		
		} else {
			$data['nomorpajak']	= "";		
		}
		
		$qrypelanggan	= $this->mclass->pelanggan($nofaktur);

		if($qrypelanggan->num_rows() >0) {
			$row_pelanggan		= $qrypelanggan->row_array();
			
			$data['nmkostumer']	= $row_pelanggan['customername'];
			$data['almtkostumer']	= $row_pelanggan['customeraddress'];
			$data['npwp']		= $row_pelanggan['npwp'];
		}
		
		$data['opt_jns_brg']= $this->mclass->lklsbrg();
		$data['isi']		= $this->mclass->clistpenjualanndo2($nofaktur);
		
		$this->load->view('prntfpajakndo_test/vtestform',$data);
		//$this->load->view('prntfpajakndo_test/vpopupform',$data);
	}	
}
?>
