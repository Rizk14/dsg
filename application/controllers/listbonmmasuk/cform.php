<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() { 
		
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$data['page_title_inbonm']	= $this->lang->line('page_title_inbonm');
			$data['list_tgl_mulai_inbonm']	= $this->lang->line('list_tgl_mulai_inbonm');
			$data['form_title_detail_inbonm']	= $this->lang->line('form_title_detail_inbonm');
			$data['list_no_inbonm']	= $this->lang->line('list_no_inbonm');
			$data['list_tgl_inbonm']	= $this->lang->line('list_tgl_inbonm');
			$data['list_kd_brg_inbonm']	= $this->lang->line('list_kd_brg_inbonm');
			$data['list_nm_brg_inbonm']	= $this->lang->line('list_nm_brg_inbonm');
			$data['list_qty_brg_inbonm']	= $this->lang->line('list_qty_brg_inbonm');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$this->load->model('listbonmmasuk/mclass');
			$data['isi']	= 'listbonmmasuk/vmainform';
			$this->load->view('template',$data);
			
		
	}
	
	function carilistbonmmasuk() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_inbonm']	= $this->lang->line('page_title_inbonm');
		$data['list_tgl_mulai_inbonm']	= $this->lang->line('list_tgl_mulai_inbonm');
		$data['form_title_detail_inbonm']	= $this->lang->line('form_title_detail_inbonm');
		$data['list_no_inbonm']	= $this->lang->line('list_no_inbonm');
		$data['list_no_insj']	= $this->lang->line('list_no_insj');
		$data['list_tgl_inbonm']	= $this->lang->line('list_tgl_inbonm');
		$data['list_kd_brg_inbonm']	= $this->lang->line('list_kd_brg_inbonm');
		$data['list_nm_brg_inbonm']	= $this->lang->line('list_nm_brg_inbonm');
		$data['list_qty_brg_inbonm']	= $this->lang->line('list_qty_brg_inbonm');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lbonmmasuk']	= "";

		$d_bonmmasuk_first	= $this->input->post('d_bonmmasuk_first');
		$d_bonmmasuk_last	= $this->input->post('d_bonmmasuk_last');
		$bonmmasuk			= $this->input->post('bonmmasuk');
		$imotif				= $this->input->post('imotif');
		
		$data['tglbonmmasukmulai']	= $d_bonmmasuk_first;
		$data['tglbonmmasukakhir']	= $d_bonmmasuk_last;
		$data['bonmmasuk']		= $bonmmasuk;
		$data['imotif_x']		= $imotif;
		
		$dbonmmasukfirst	= explode("/",$d_bonmmasuk_first,strlen($d_bonmmasuk_first)); // dd/mm/YYYY
		$dbonmmasuklast	= explode("/",$d_bonmmasuk_last,strlen($d_bonmmasuk_last)); // dd/mm/YYYY
		
		$n_d_bonmmasuk_first= (!empty($dbonmmasukfirst[2]) && strlen($dbonmmasukfirst[2])==4)?$dbonmmasukfirst[2].'-'.$dbonmmasukfirst[1].'-'.$dbonmmasukfirst[0]:"";
		$n_d_bonmmasuk_last	= (!empty($dbonmmasuklast[2]) && strlen($dbonmmasuklast[2])==4)?$dbonmmasuklast[2].'-'.$dbonmmasuklast[1].'-'.$dbonmmasuklast[0]:"";
				
		$this->load->model('listbonmmasuk/mclass');
		
		if($n_d_bonmmasuk_first=='' || $n_d_bonmmasuk_last=='') {
			$data['template']	= '1';
		}else{
			$data['template']	= '0';
		}
		
		if($n_d_bonmmasuk_first!='' && $n_d_bonmmasuk_last!='') {
			$data['query']	= $this->mclass->clistbonmmasuk2($bonmmasuk,$n_d_bonmmasuk_first,$n_d_bonmmasuk_last,strtoupper($imotif));
		}elseif($bonmmasuk!='' || $imotif!='') {
			$data['query']	= $this->mclass->clistbonmmasuk($bonmmasuk,$n_d_bonmmasuk_first,$n_d_bonmmasuk_last,strtoupper($imotif));
		}else{
			$data['query']	= $this->mclass->clistbonmmasuk2($bonmmasuk,$n_d_bonmmasuk_first,$n_d_bonmmasuk_last,strtoupper($imotif));
		}
			$data['isi']	= 'listbonmmasuk/vlistform';
			$this->load->view('template',$data);
	
	}
	
	function edit() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ibonm	= $this->uri->segment(4);
		
		$data['page_title_bmm']	= $this->lang->line('page_title_bmm');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']= "";
		$data['limages']	= base_url();
		$data['form_title_detail_bmm']	= $this->lang->line('form_title_detail_bmm');
		$data['form_nomor_bmm']	= $this->lang->line('form_nomor_bmm');
		$data['form_tanggal_bmm']	= $this->lang->line('form_tanggal_bmm');
		$data['form_sj_bmm']		= $this->lang->line('form_sj_bmm');
		$data['form_title_detail_bmm']	= $this->lang->line('form_title_detail_bmm');
		$data['form_kode_produk_bmm']	= $this->lang->line('form_kode_produk_bmm');
		$data['form_nm_produk_bmm']	= $this->lang->line('form_nm_produk_bmm');
		$data['form_jml_produk_bmm']	= $this->lang->line('form_jml_produk_bmm');
		$data['button_update']	= $this->lang->line('button_update');
		$data['button_batal']	= $this->lang->line('button_batal');

		$this->load->model('listbonmmasuk/mclass');
		
		$qbonmmasuk	= $this->mclass->getbonmmasuk($ibonm);

		if($qbonmmasuk->num_rows()>0) {
			$row_bonmmsuk	= $qbonmmasuk->row();
			$ibonmmasukcode	= $row_bonmmsuk->i_inbonm_code;
			$ibonmmasuk	= $row_bonmmsuk->i_inbonm;
			$data['ibonmmasukcode']	= $ibonmmasukcode;
			$data['ibonmmasuk']	= $ibonmmasuk;
			$tglbonmmasuk	= explode("-",$row_bonmmsuk->d_inbonm,strlen($row_bonmmsuk->d_inbonm)); // YYYY-mm-dd
			$data['tBonMmasuk']	= $tglbonmmasuk[2]."/".$tglbonmmasuk[1]."/".$tglbonmmasuk[0];
			$data['tBonMmasukH']= $row_bonmmsuk->d_inbonm;
			$data['isjmanual']	= $row_bonmmsuk->i_sj_manual;
			
			/* 20092011
			$data['bonmmasukitem']	= $this->mclass->getbonmmasukitem($ibonmmasukcode);
			*/
			
			$data['bonmmasukitem']	= $this->mclass->getbonmmasukitem($ibonm);

		}
				$data['isi']	= 'listbonmmasuk/veditform';
			$this->load->view('template',$data);		
		
		
	}
	
	function actedit() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration; 
		
		$i_inbonm_code			= $this->input->post('i_inbonm');
		$i_inbonm_hidden		= $this->input->post('i_inbonm_hidden');
		$i_inbonm_code_hidden	= $this->input->post('i_inbonm_code_hidden');
		$d_inbonm	= $this->input->post('d_inbonm');
		$i_sj		= $this->input->post('i_sj');		
		
		$i_product	= array();
		$e_product_name	= array();
		//$n_count_product= array();
		$qty_product	= array();
		$f_stp	= array();
		$iso	= array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem'.'_'.'0');
		$ex_d_inbonm	= explode("/",$d_inbonm,strlen($d_inbonm)); // dd/mm/YYYY
		$tglbaru		= $ex_d_inbonm[2]."-".$ex_d_inbonm[1]."-".$ex_d_inbonm[0];
		
		if($ex_d_inbonm[2]=='' || $ex_d_inbonm[1]=='' || $ex_d_inbonm[0]=='')
			$tglbaru	= date("Y-m-d");
		
		$this->load->model('listbonmmasuk/mclass');
		
		for($cacah=0; $cacah<=$iterasi; $cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah);
			
			//$n_count_product[$cacah]	= $this->input->post('n_count_product_'.'tblItem'.'_'.$cacah);
			$qty_warna[$cacah]	= $this->input->post('qty_warna_'.$cacah);
			$i_product_color[$cacah]	= $this->input->post('i_product_color_'.$cacah);
			$i_color[$cacah]	= $this->input->post('i_color_'.$cacah);
			
			$qty_product[$cacah]	= $this->input->post('qty_product_'.'tblItem'.'_'.$cacah);
			$f_stp[$cacah]	= $this->input->post('f_stp_'.'tblItem'.'_'.$cacah);
			$iso[$cacah]	= $this->input->post('iso_'.'tblItem'.'_'.$cacah);
		}

		if (!empty($i_inbonm_code) &&
		    !empty($d_inbonm)) {
			if(!empty($i_product_0)) {
				// $n_count_product
				$this->mclass->mupdate($i_inbonm_code,$i_inbonm_hidden,$i_inbonm_code_hidden,$tglbaru,$i_product,$e_product_name,
				$qty_warna, $i_product_color, $i_color, $qty_product,$iterasi,$f_stp,$iso,$i_sj);
			}else{
				print "<script>alert(\"Maaf, item utk Bon M Masuk harus terisi. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}
		} 		
	}

	function actedit_old() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration; 
		
		$i_inbonm_code			= $this->input->post('i_inbonm');
		$i_inbonm_hidden		= $this->input->post('i_inbonm_hidden')?$this->input->post('i_inbonm_hidden'):$this->input->get_post('i_inbonm_hidden');
		$i_inbonm_code_hidden	= $this->input->post('i_inbonm_code_hidden')?$this->input->post('i_inbonm_code_hidden'):$this->input->get_post('i_inbonm_code_hidden');
		$d_inbonm	= $this->input->post('d_inbonm')?$this->input->post('d_inbonm'):$this->input->get_post('d_inbonm');
		$i_sj		= $this->input->post('i_sj')?$this->input->post('i_sj'):$this->input->get_post('i_sj');		
		
		$i_product	= array();
		$e_product_name	= array();
		$n_count_product= array();
		$qty_product	= array();
		$f_stp	= array();
		$iso	= array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem'.'_'.'0');
		$ex_d_inbonm	= explode("/",$d_inbonm,strlen($d_inbonm)); // dd/mm/YYYY
		$tglbaru		= $ex_d_inbonm[2]."-".$ex_d_inbonm[1]."-".$ex_d_inbonm[0];
		
		for($cacah=0; $cacah<=$iterasi; $cacah++) {

			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem'.'_'.$cacah)?$this->input->post('i_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah)?$this->input->post('e_product_name_'.'tblItem'.'_'.$cacah):$this->input->get_post('e_product_name_'.'tblItem'.'_'.$cacah);
			$n_count_product[$cacah]	= $this->input->post('n_count_product_'.'tblItem'.'_'.$cacah)?$this->input->post('n_count_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('n_count_product_'.'tblItem'.'_'.$cacah);
			$qty_product[$cacah]	= $this->input->post('qty_product_'.'tblItem'.'_'.$cacah)?$this->input->post('qty_product_'.'tblItem'.'_'.$cacah):$this->input->get_post('qty_product_'.'tblItem'.'_'.$cacah);
			$f_stp[$cacah]	= $this->input->post('f_stp_'.'tblItem'.'_'.$cacah)?$this->input->post('f_stp_'.'tblItem'.'_'.$cacah):$this->input->get_post('f_stp_'.'tblItem'.'_'.$cacah);
			$iso[$cacah]	= $this->input->post('iso_'.'tblItem'.'_'.$cacah)?$this->input->post('iso_'.'tblItem'.'_'.$cacah):$this->input->get_post('iso_'.'tblItem'.'_'.$cacah);
		}

		if (!empty($i_inbonm_code) &&
		    !empty($d_inbonm)) {
			if(!empty($i_product_0)) {
				$this->load->model('listbonmmasuk/mclass');
				$this->mclass->mupdate($i_inbonm_code,$i_inbonm_hidden,$i_inbonm_code_hidden,$tglbaru,$i_product,$e_product_name,$n_count_product,$qty_product,$iterasi,$f_stp,$iso,$i_sj);
			} else {
				print "<script>alert(\"Maaf, item utk Bon M Masuk harus terisi. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}
		} 		
	}
	
	function cari_bonmmasuk() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$nobonmmasuk	= $this->input->post('nobonmmasuk');
		$i_inbonm_code_hidden = $this->input->post('i_inbonm_code_hidden');
		
		$this->load->model('listbonmmasuk/mclass');
		$qbonmmasuk	= $this->mclass->cari_bonmmasuk($nobonmmasuk);
		$num = $qbonmmasuk->num_rows();
		if($num>0) {
			$rbonmmasuk = $qbonmmasuk->row();
			if(($rbonmmasuk->i_inbonm_code!=$i_inbonm_code_hidden) && ($num>0)){
				echo "Maaf, No. Bon M sudah ada!";
			}
		}
	}	
	
	function undo() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ibonm	= $this->uri->segment(4);
		$this->load->model('listbonmmasuk/mclass');
		$this->mclass->mbatal($ibonm);
	}
	
	function listbarangjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "BON M MASUK";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbonmmasuk/mclass');

		$query	= $this->mclass->lbonmmasuk();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbonmmasuk/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbonmmasukperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listbonmmasuk/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "BON M MASUK";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbonmmasuk/mclass');

		$query	= $this->mclass->lbonmmasuk();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbonmmasuk/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbonmmasukperpages($pagination['per_page'],$pagination['cur_page']);

		$this->load->view('listbonmmasuk/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "BON M MASUK";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbonmmasuk/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->flbonmmasuk($key);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
					'01'=>'Januari', 
					'02'=>'Februari', 
					'03'=>'Maret', 
					'04'=>'April', 
					'05'=>'Mei', 
					'06'=>'Juni', 
					'07'=>'Juli', 
					'08'=>'Agustus', 
					'09'=>'September', 
					'10'=>'Oktober', 
					'11'=>'Nopember', 
					'12'=>'Desember' );
							
			$cc	= 1; 			
			foreach($query->result() as $row) {
					$tgl			= (!empty($row->dinbonm) || strlen($row->dinbonm)!=0)?@explode("-",$row->dinbonm,strlen($row->dinbonm)):"";
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
								
				$list .= "
				 <tr>
				  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->inbonmcode')\">".$row->inbonmcode."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->inbonmcode')\">".$tanggal[$cc]."</a></td>
					  <td><a href=\"javascript:settextfield('$row->inbonmcode')\">".$row->isjmanual."</a></td>	 
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}

	function listbarangjadi2() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iterasi = $this->uri->segment(4,0);
		$dbonmmasuk = $this->uri->segment(5,0); // YYYY-mm-dd
		
		$expdbonmmasuk	= explode("-",$dbonmmasuk,strlen($dbonmmasuk));
		$bulan	= $expdbonmmasuk[1];
		$tahun	= $expdbonmmasuk[0];
		
		$data['dbonmmasuk']	= $dbonmmasuk;
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('listbonmmasuk/mclass');

		$query	= $this->mclass->lbarangjadi2($bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->lbarangjadi2opsi($bulan,$tahun);
			$jml	= $query->num_rows();			
		}
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbonmmasuk/cform/listbarangjadinext2/'.$iterasi.'/'.$dbonmmasuk.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages2($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpages2opsi($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$this->load->view('listbonmmasuk/vlistformbrgjadi2',$data);
	}

	function listbarangjadinext2(){
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iterasi = $this->uri->segment(4,0);
		$dbonmmasuk = $this->uri->segment(5,0); // YYYY-mm-dd
		
		$expdbonmmasuk	= explode("-",$dbonmmasuk,strlen($dbonmmasuk));
		$bulan	= $expdbonmmasuk[1];
		$tahun	= $expdbonmmasuk[0];
		
		$data['dbonmmasuk']	= $dbonmmasuk;		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('listbonmmasuk/mclass');

		$query	= $this->mclass->lbarangjadi2($bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
			$query	= $this->mclass->lbarangjadi2opsi($bulan,$tahun);
			$jml	= $query->num_rows();
		}
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listbonmmasuk/cform/listbarangjadinext2/'.$iterasi.'/'.$dbonmmasuk.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages2($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpages2opsi($pagination['per_page'],$pagination['cur_page'],$bulan,$tahun);
		
		$this->load->view('listbonmmasuk/vlistformbrgjadi2',$data);
	}	

	function flistbarangjadi2(){
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key');
		$dbonmmasuk	= $this->input->post('dbonmmasuk');
		
		$iterasi = $this->uri->segment(4,0);
		//$dbonmmasuk = $this->uri->segment(5,0); // YYYY-mm-dd
		
		$expdbonmmasuk	= explode("-",$dbonmmasuk,strlen($dbonmmasuk));
		$bulan	= $expdbonmmasuk[1];
		$tahun	= $expdbonmmasuk[0];
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('listbonmmasuk/mclass');

		$query	= $this->mclass->flbarangjadi2($key,$bulan,$tahun);
		$jml	= $query->num_rows();
		if($jml==0){
		$query	= $this->mclass->flbarangjadi2opsi($key,$bulan,$tahun);
		$jml	= $query->num_rows();			
		}
		
		$list	= "";
		if($jml>0) {
			$db2=$this->load->database('db_external', TRUE);
			$cc	= 1; 
			foreach($query->result() as $row){
				
				//--------------08-01-2014 ----------------------------
				// ambil data warna dari tr_product_color
				$sqlxx	= $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b 
										 WHERE a.i_color=b.i_color AND a.i_product_motif = '$row->imotif' ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0){
					$hasilxx = $sqlxx->result();
					
					foreach ($hasilxx as $rownya) {
						$listwarna.= $rownya->e_color_name."<br>";
					}
				}
				
				//-----------------------------------------------------
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->stp','$row->iso')\">".$row->imotif."</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->stp','$row->iso')\">".$row->motifname."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->stp','$row->iso')\">".$row->qty."</a></td>
				  <td>".$listwarna."</td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;		
	}	
	
	// 08-01-2014
	function additemwarna(){
 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproductmotif 	= $this->input->post('iproductmotif', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$db2=$this->load->database('db_external', TRUE);
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
									WHERE a.i_color = b.i_color AND a.i_product_motif = '".$iproductmotif."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'i_product_color'=> $rowxx->i_product_color,
										'i_color'=> $rowxx->i_color,
										'e_color_name'=> $rowxx->e_color_name
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['iproductmotif'] = $iproductmotif;
		$data['posisi'] = $posisi;
		$this->load->view('listbonmmasuk/vlistwarna', $data); 
		//print_r($detailwarna); die();
		//echo $iproductmotif; die();
		return true;
  }
}
?>
