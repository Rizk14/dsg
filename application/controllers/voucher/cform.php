<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_voucher']			= $this->lang->line('page_title_voucher');
			$data['form_title_detail_voucher']	= $this->lang->line('form_title_detail_voucher');
			$data['list_voucher_no_kontrabon']	= $this->lang->line('list_voucher_no_kontrabon');
			$data['list_voucher_tgl_kontrabon']	= $this->lang->line('list_voucher_tgl_kontrabon');
			$data['list_voucher_total_kontrabon']	= $this->lang->line('list_voucher_total_kontrabon');
			$data['list_voucher_nilai_voucher_detail']	= $this->lang->line('list_voucher_nilai_voucher_detail');
			$data['list_voucher_tgl_faktur']	= $this->lang->line('list_voucher_tgl_faktur');
			$data['list_voucher_kd_sumber']		= $this->lang->line('list_voucher_kd_sumber');
			$data['list_voucher_no']			= $this->lang->line('list_voucher_no');
			$data['list_voucher_tgl']			= $this->lang->line('list_voucher_tgl');
			$data['list_voucher_kd_perusahaan']	= $this->lang->line('list_voucher_kd_perusahaan');
			$data['list_voucher_deskripsi']		= $this->lang->line('list_voucher_deskripsi');
			$data['list_voucher_total']			= $this->lang->line('list_voucher_total');
			$data['list_voucher_received']		= $this->lang->line('list_voucher_received');
			$data['list_voucher_approved']		= $this->lang->line('list_voucher_approved');
			$data['list_voucher_approved_dept']	= $this->lang->line('list_voucher_approved_dept');
			$data['list_voucher_approved_serv']	= $this->lang->line('list_voucher_approved_serv');
			$data['list_voucher_manual_input']	= $this->lang->line('list_voucher_manual_input');
			$data['list_voucher_piutang']	= $this->lang->line('list_voucher_piutang');	
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			$this->load->model('voucher/mclass');
			$data['isi']	= 'voucher/vmainform';
			$this->load->view('template',$data);
		}	
	

	function listkontrabon() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('voucher/mclass');

		$query	= $this->mclass->lkontrabon();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/voucher/cform/listkontrabonnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 500;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lkontrabonperpages($pagination['per_page'],$pagination['cur_page']);
				
		$this->load->view('voucher/vlistformbrgjadi',$data);			
	}

	function listkontrabonnext() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('voucher/mclass');

		$query	= $this->mclass->lkontrabon();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/voucher/cform/listkontrabonnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 500;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lkontrabonperpages($pagination['per_page'],$pagination['cur_page']);
				
		$this->load->view('voucher/vlistformbrgjadi',$data);			
	}

	function flistkontrabon() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "NOMOR KONTRA BON";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('voucher/mclass');

		$query	= $this->mclass->flkontrabon($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row) {
				
				$f_nota_sederhana = ($row->f_nota_sederhana=='t')?'Faktur Non DO':'';
				
				$list .= "
				
				<tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt','$row->f_nota_sederhana')\">".$row->i_dt_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt','$row->f_nota_sederhana')\">".$row->d_dt."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt','$row->f_nota_sederhana')\">".$f_nota_sederhana."</td>
				  <td>&nbsp;</td>
				</tr>";
				 
				$cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	
	function carikontrabon() {	
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_voucher']			= $this->lang->line('page_title_voucher');
		$data['form_title_detail_voucher']	= $this->lang->line('form_title_detail_voucher');
		$data['list_voucher_no_kontrabon']	= $this->lang->line('list_voucher_no_kontrabon');
		$data['list_voucher_tgl_kontrabon']	= $this->lang->line('list_voucher_tgl_kontrabon');
		$data['list_voucher_total_kontrabon']	= $this->lang->line('list_voucher_total_kontrabon');
		$data['list_voucher_nilai_voucher_detail']	= $this->lang->line('list_voucher_nilai_voucher_detail');
		$data['list_voucher_tgl_faktur']	= $this->lang->line('list_voucher_tgl_faktur');
		$data['list_voucher_kd_sumber']		= $this->lang->line('list_voucher_kd_sumber');
		$data['list_voucher_no']			= $this->lang->line('list_voucher_no');
		$data['list_voucher_tgl']			= $this->lang->line('list_voucher_tgl');
		$data['list_voucher_kd_perusahaan']	= $this->lang->line('list_voucher_kd_perusahaan');
		$data['list_voucher_deskripsi']		= $this->lang->line('list_voucher_deskripsi');
		$data['list_voucher_total']			= $this->lang->line('list_voucher_total');
		$data['list_voucher_received']		= $this->lang->line('list_voucher_received');
		$data['list_voucher_approved']		= $this->lang->line('list_voucher_approved');
		$data['list_voucher_approved_dept']	= $this->lang->line('list_voucher_approved_dept');
		$data['list_voucher_approved_serv']	= $this->lang->line('list_voucher_approved_serv');
		$data['list_voucher_manual_input']	= $this->lang->line('list_voucher_manual_input');
		$data['list_voucher_piutang']		= $this->lang->line('list_voucher_piutang');	
		$data['list_voucher_total_nilai']	= $this->lang->line('list_voucher_total_nilai');
		
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['limages']	= base_url();
		
		$this->load->model('voucher/mclass');
		
		$no_kontra_bon		= $this->input->post('no_kontra_bon');
		$i_kontra_bon		= $this->input->post('i_kontra_bon');
		$f_nota_sederhana	= $this->input->post('f_nota_sederhana');
		$data['no_kontra_bon']	= $no_kontra_bon;
		
		$expikontrabon	= explode("#",$i_kontra_bon);
		$fnotasederhana = explode("#",$f_nota_sederhana);
		
		$list	= '';
		
		if(sizeof($expikontrabon)>0 && $no_kontra_bon!='') {
			
			$wh		= 0;
			$iter	= 0;
			$totalkontrabon = 0;
			$totalpiutang = 0;
			
			while($wh<count($expikontrabon)) {
				
				$query	= $this->mclass->clistkontrabon($expikontrabon[$wh]);
				$row	= $query->row();
				
				$exp_d_dt = explode("-",$row->d_dt,strlen($row->d_dt)); // YYYY-mm-dd
				
				$list	.= "
					<tr>
						<td><div style=\"font:24px;text-align:right;width:13px;margin-right:0px;\">".($iter*1+1)."</div></td>
						
						<td>
						<DIV ID=\"ajax_i_kontrabon_code_tblItem_".$iter."\" style=\"width:85px;\">
						<input type=\"text\" ID=\"i_kontrabon_code_tblItem_".$iter."\" name=\"i_kontrabon_code_tblItem_".$iter."\" style=\"width:85px;\" value=\"".trim($row->i_dt_code)."\" readonly >
						<input type=\"hidden\" ID=\"i_kontrabon_tblItem_".$iter."\" name=\"i_kontrabon_tblItem_".$iter."\" value=\"".$row->i_dt."\">
						</DIV>
						</td>
						
						<td>
						<DIV ID=\"ajax_tgl_kontrabon_tblItem_".$iter."\" style=\"width:85px;\" >
						<input type=\"text\" ID=\"tgl_kontrabon_tblItem_".$iter."\" name=\"tgl_kontrabon_tblItem_".$iter."\" style=\"width:83px;text-align:left;\" value=\"".$exp_d_dt[2]."/".$exp_d_dt[1]."/".$exp_d_dt[0]."\" readonly >
						<input type=\"hidden\" name=\"tgl_kontrabonhidden_tblItem_".$iter."\" id=\"tgl_kontrabonhidden_tblItem_".$iter."\" value=\"".$row->d_dt."\">
						</DIV>
						</td>
								
						<td><DIV ID=\"ajax_total_kontrabon_tblItem_".$iter."\" style=\"width:130px;\" >
						<input type=\"text\" ID=\"total_kontrabon_tblItem_".$iter."\" name=\"total_kontrabon_tblItem_".$iter."\" style=\"width:128px;text-align:right;\" value=\"".$row->v_total_grand."\" readonly >
						<input type=\"hidden\" ID=\"total_kontrabonhidden_tblItem_".$iter."\" name=\"total_kontrabonhidden_tblItem_".$iter."\" value=\"".$row->v_total_grand."\" ></DIV></td>

						<td><DIV ID=\"ajax_piutang_tblItem_".$iter."\" style=\"width:130px;\" >
						<input type=\"text\" ID=\"piutang_tblItem_".$iter."\" name=\"piutang_tblItem_".$iter."\" style=\"width:128px;text-align:right;\" value=\"".$row->v_grand_sisa."\" readonly >
						<input type=\"hidden\" ID=\"piutanghidden_tblItem_".$iter."\" name=\"piutanghidden_tblItem_".$iter."\" value=\"".$row->v_grand_sisa."\"></DIV></td>
						
						<td><DIV ID=\"ajax_nilai_voucher_tblItem_".$iter."\" style=\"width:145px;text-align:right;\" >
						<input type=\"text\" ID=\"nilai_voucher_tblItem_".$iter."\" name=\"nilai_voucher_tblItem_".$iter."\" style=\"width:143px;text-align:right;\" disabled onkeyup=\"hitung(".$iter.");\"></DIV>
						<input type=\"hidden\" id=\"f_nota_sederhana".$iter."\" name=\"f_nota_sederhana".$iter."\" value=\"".$fnotasederhana[$wh]."\">
						<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\">
						</td>
					</tr>";
				
				$totalkontrabon = $totalkontrabon+$row->v_total_grand;
				$totalpiutang = $totalpiutang+$row->v_grand_sisa;
				
				$iter++;	
				$wh+=1;
			}
			$list	.= "<input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"".($iter-1)."\">";
			$list	.= "<input type=\"hidden\" id=\"totalpiutang\" name=\"totalpiutang\" value=\"".$totalpiutang."\">";
		}else{
		}
		
		$data['totalkontrabon']	= $totalkontrabon;
		$data['totalpiutang']	= $totalpiutang;
		
		$data['query'] = $list;
			$data['isi']	= 'voucher/vlistform';
			$this->load->view('template',$data);
	}
	
	function listkodesumbervoucher(){
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "KODE SUMBER";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('voucher/mclass');

		$query	= $this->mclass->lkodesumber();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/voucher/cform/listkodesumbervouchernext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 500;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lkodesumberperpages($pagination['per_page'],$pagination['cur_page']);
				
		$this->load->view('voucher/vlistformkodesumber',$data);			
	}

	function listkodesumbervouchernext(){
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "KODE SUMBER";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('voucher/mclass');

		$query	= $this->mclass->lkodesumber();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/voucher/cform/listkodesumbervouchernext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 500;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lkodesumberperpages($pagination['per_page'],$pagination['cur_page']);
				
		$this->load->view('voucher/vlistformkodesumber',$data);			
	}

	function flistkodesumbervoucher() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key');
		
		$data['page_title']	= "KODE SUMBER";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('voucher/mclass');

		$query	= $this->mclass->flkodesumber($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_voucher','$row->i_voucher_code')\">".$row->i_voucher_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_voucher','$row->i_voucher_code')\">".$row->e_voucher_name."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_voucher','$row->i_voucher_code')\">".$row->e_description."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
			
			
	function simpan() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_kontrabon_code	= array();
		$i_kontrabon = array();
		$tgl_kontrabonhidden = array();
		$tglkontrabon = array();
		$total_kontrabonhidden = array();
		$piutanghidden = array();
		$nilai_voucher = array();
		$fnotasederhana = array();
		
		$kode_sumber = $this->input->post('kode_sumber');
		$i_kode_sumber = $this->input->post('i_kode_sumber');
		$no_voucher = $this->input->post('no_voucher');
		$tgl_voucher = $this->input->post('tgl_voucher');
		$kode_perusahaan = $this->input->post('kode_perusahaan');
		$deskripsi_voucher = $this->input->post('deskripsi_voucher');
		$total_nilai_voucher = $this->input->post('total_nilai_voucher');
		$recieved_voucher = $this->input->post('recieved_voucher');
		$approve_voucher = $this->input->post('approve_voucher');
		$app_dept = $this->input->post('app_dept');
		$f_nilai_manual = $this->input->post('f_nilai_manual');
		
		$iteration	= $this->input->post('iteration');  		
		
		$exptgl_voucher	= explode("/",$tgl_voucher,strlen($tgl_voucher)); // dd/mm/YYYY
		$tglvoucher	= $exptgl_voucher[2]."-".$exptgl_voucher[1]."-".$exptgl_voucher[0];
		
		$i_kontrabon_code_tblItem_0 = $this->input->post('i_kontrabon_code_tblItem_0');
		
		for($jumlah=0; $jumlah<=$iteration; $jumlah++) {
			
			$i_kontrabon_code[$jumlah]	= $this->input->post('i_kontrabon_code_tblItem_'.$jumlah);
			$i_kontrabon[$jumlah] = $this->input->post('i_kontrabon_tblItem_'.$jumlah);
			$tgl_kontrabonhidden[$jumlah] = $this->input->post('tgl_kontrabonhidden_tblItem_'.$jumlah);
			$total_kontrabonhidden[$jumlah] = $this->input->post('total_kontrabonhidden_tblItem_'.$jumlah);
			$piutanghidden[$jumlah] = $this->input->post('piutanghidden_tblItem_'.$jumlah);
			$nilai_voucher[$jumlah] = $this->input->post('nilai_voucher_tblItem_'.$jumlah);
			$fnotasederhana[$jumlah] = $this->input->post('f_nota_sederhana'.$jumlah);
			
		}
		
		if(!empty($kode_sumber) && 
		   !empty($no_voucher) && 
		   !empty($i_kontrabon_code_tblItem_0)
		) {
			$this->load->model('voucher/mclass');
			$this->mclass->msimpan($kode_sumber,$i_kode_sumber,$no_voucher,$tglvoucher,$kode_perusahaan,$deskripsi_voucher,$total_nilai_voucher,$recieved_voucher,$approve_voucher,$app_dept,$f_nilai_manual,$iteration,$i_kontrabon_code,$i_kontrabon,$tgl_kontrabonhidden,$total_kontrabonhidden,$piutanghidden,$nilai_voucher,$fnotasederhana);			
		}else{
			print "<script>alert(\"Maaf, Voucher '\"+$no_voucher+\"' gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
	}
	
}
?>
