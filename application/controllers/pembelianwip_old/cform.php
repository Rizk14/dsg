<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('pembelianwip/mmaster');
  }
  
  // 22-08-2013, FAKTUR WIP JAHIT ++++++++++++++++++++
  function fakturwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}

	$keywordcari 	= $this->input->post('cari', TRUE);  
	$cunit 	= $this->input->post('cunit', TRUE);
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE); 
	
	if ($cunit == '')
		$cunit = $this->uri->segment(4);
	if ($date_from == '')
		$date_from = $this->uri->segment(5);
	if ($date_to == '')
		$date_to = $this->uri->segment(6);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(7);
		
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($cunit == '')
		$cunit = "xx";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAllfakturwiptanpalimit($cunit, $date_from, $date_to, $keywordcari);
							$config['base_url'] = base_url().'index.php/pembelianwip/cform/fakturwip/'.$cunit.'/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllfakturwip($config['per_page'],$this->uri->segment(8), $cunit, $date_from, $date_to, $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['isi'] = 'pembelianwip/vformviewfakturwip';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['cunit'] = $cunit;
	$data['startnya'] = $config['cur_page'];
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	$data['list_unit_jahit'] = $this->mmaster->getlistunitjahit();
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function fakturwipadd(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['list_unit_jahit'] = $this->mmaster->getlistunitjahit();	
		
		$data['isi'] = 'pembelianwip/vmainformfakturwip';
		$this->load->view('template',$data);
	//}
  }
  
  function show_popup_brgjadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$unit_jahit 	= $this->input->post('kode_unit', TRUE);
	$posisi 	= $this->input->post('posisi', TRUE);  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' && $posisi == '' && $unit_jahit == '') {
		$unit_jahit 	= $this->uri->segment(4);
		$posisi 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->getbrgjaditanpalimit($keywordcari, $unit_jahit);
			$config['base_url'] = base_url()."index.php/pembelianwip/cform/show_popup_brgjadi/".$unit_jahit."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getbrgjadi($config['per_page'],$this->uri->segment(7), $keywordcari, $unit_jahit);
	
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
	if ($query3->num_rows() > 0){
		$hasil3 = $query3->row();
		$namaunit = $hasil3->nama;
	}
	else {
		$namaunit = "";
	}
		
	$data['kodeunit'] = $unit_jahit;
	$data['namaunit'] = $namaunit;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('pembelianwip/vpopupbrgjadi',$data);
  }
  
  function fakturwipsubmit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
		$no_faktur 	= $this->input->post('no_faktur', TRUE);
		$tgl_faktur 	= $this->input->post('tgl_faktur', TRUE);  
		$kode_unit 	= $this->input->post('kode_unit', TRUE);  
		$gtotal 	= $this->input->post('gtotal', TRUE);
		$ket 	= $this->input->post('ket', TRUE);  
		
		$pisah1 = explode("-", $tgl_faktur);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_faktur = $thn1."-".$bln1."-".$tgl1;
									
		$no 	= $this->input->post('no', TRUE);
		$jumlah_input=$no-1;
			
		$cek_data = $this->mmaster->cek_data_fakturwip($no_faktur, $kode_unit, $thn1);
			if (count($cek_data) > 0) {
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$namaunit	= $hasilrow->nama;
				}
				else {
					$namaunit = '';
				}

				$data['isi'] = 'pembelianwip/vmainformfakturwip';
				$data['msg'] = "Data no Faktur ".$no_faktur." untuk unit jahit ".$kode_unit." - ".$namaunit." di tahun ".$thn1."sudah ada..!";
				$data['go_proses'] = '';
				$data['list_unit_jahit'] = $this->mmaster->getunitjahit();	
				$this->load->view('template',$data);
			}
			else {
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->savefakturwip($no_faktur,$tgl_faktur, $kode_unit, $gtotal, $ket, 
							$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
							$this->input->post('qty_'.$i, TRUE),  
							$this->input->post('harga_'.$i, TRUE),  
							$this->input->post('harga_lama_'.$i, TRUE),  
							$this->input->post('diskon_'.$i, TRUE),  
							$this->input->post('subtotal_'.$i, TRUE),
							$this->input->post('no_sj_'.$i, TRUE),
							$this->input->post('tgl_sj_'.$i, TRUE));
				}
				redirect('pembelianwip/cform/fakturwip');
			}
  }
  
  function fakturwipdelete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $curpage 	= $this->uri->segment(5);
    $cunit 	= $this->uri->segment(6);
    $tgl_awal 	= $this->uri->segment(7);
    $tgl_akhir 	= $this->uri->segment(8);
    $carinya 	= $this->uri->segment(9);
    
    $this->mmaster->deletefakturwip($kode);
    
    if ($carinya == '') $carinya = "all"; 
	$url_redirectnya = "pembelianwip/cform/fakturwip/".$cunit."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$curpage;
	
    redirect($url_redirectnya);
  }
  
  function fakturwipedit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$id_faktur 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cunit 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['cur_page'] = $cur_page;
	$data['cunit'] = $cunit;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_fakturwip($id_faktur); 
			
	$data['list_unit_jahit'] = $this->mmaster->getlistunitjahit();
	$data['msg'] = '';
	$data['isi'] = 'pembelianwip/veditformfakturwip';
	$this->load->view('template',$data);
  }
  
  function fakturwipupdate() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$id_faktur 	= $this->input->post('id_faktur', TRUE);
			$no_faktur 	= $this->input->post('no_faktur', TRUE);
			$tgl_faktur 	= $this->input->post('tgl_faktur', TRUE);  
			$gtotal 	= $this->input->post('gtotal', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$kode_unit 	= $this->input->post('kode_unit', TRUE);  
			
			$pisah1 = explode("-", $tgl_faktur);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_faktur = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$cunit = $this->input->post('cunit', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");
			$uid = $this->session->userdata('uid');
			
			$sql = "UPDATE tm_fakturmasukwip SET no_faktur='".$this->db->escape_str($no_faktur)."', 
					tgl = '$tgl_faktur', grandtotal = '$gtotal', 
					tgl_update='$tgl', keterangan = '$ket' where id= '$id_faktur'";
			
			//update headernya
			$this->db->query($sql);
			$this->db->delete('tm_fakturmasukwip_detail', array('id_fakturmasukwip' => $id_faktur));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$kode_brg_jadi = $this->input->post('kode_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$harga = $this->input->post('harga_'.$i, TRUE);
						$harga_lama = $this->input->post('harga_lama_'.$i, TRUE);
						$diskon = $this->input->post('diskon_'.$i, TRUE);
						$subtotal = $this->input->post('subtotal_'.$i, TRUE);
						$no_sj = $this->input->post('no_sj_'.$i, TRUE);
						$tgl_sj = $this->input->post('tgl_sj_'.$i, TRUE);
						$id_faktur_detail = $this->input->post('id_faktur_detail_'.$i, TRUE);
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[0];
						$bln1= $pisah1[1];
						$thn1= $pisah1[2];
						$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
						// jika semua data tdk kosong, insert ke tm_fakturmasukwip_detail
						$data_detail = array(
									'id_fakturmasukwip'=>$id_faktur,
									'kode_brg_jadi'=>$kode_brg_jadi,
									'qty'=>$qty,
									'harga'=>$harga,
									'diskon'=>$diskon,
									'subtotal'=>$subtotal,
									'no_sj'=>$no_sj,
									'tgl_sj'=>$tgl_sj
						);
						$this->db->insert('tm_fakturmasukwip_detail',$data_detail);
							// ================ end insert item detail ===========
							
							// update harganya jika harga != harga_lama
							if ($harga_lama != 0) {
								if ($harga != $harga_lama) {
									$query31	= $this->db->query(" SELECT id FROM tm_harga_hasil_jahit WHERE kode_unit = '$kode_unit'
																AND kode_brg_jadi = '$kode_brg_jadi' ");
									if ($query31->num_rows() > 0){
										$hasilrow31 = $query31->row();
										$id_harga	= $hasilrow31->id;
									}
									
									$this->db->query(" UPDATE tm_harga_hasil_jahit SET harga='$harga',
														tgl_update='$tgl' WHERE id='$id_harga' ");
								}
							}
							else {
								$tgl2 = date("Y-m-d");
								$data_harga = array(
									'kode_brg_jadi'=>$kode_brg_jadi,
									'kode_unit'=>$kode_unit,
									'harga'=>$harga,
									'tgl_input'=>$tgl2,
									'tgl_update'=>$tgl2
								);
								$this->db->insert('tm_harga_hasil_jahit',$data_harga);
							}
				} // end perulangan

			if ($carinya == '') $carinya = "all"; 
			$url_redirectnya = "pembelianwip/cform/fakturwip/".$cunit."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
			redirect($url_redirectnya);
				
  }
  
  // =================================== END 22-08-2013 ============================================
  
	function importfaktur(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$data['list_unit_jahit'] = $this->mmaster->getlistunitjahit();	
	$data['isi'] = 'pembelianwip/vformimportfaktur';
	$this->load->view('template',$data);
  }
  
  function doimport() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$kode_unit 	= $this->input->post('kode_unit', TRUE); 
		
		set_time_limit(36000);
		ini_set("memory_limit","512M");
		ini_set("max_execution_time","36000");
		//ini_set("display_errors", 1);
		
		$filenya = $_FILES['userfile']['name'];
		
		$config['upload_path'] = './files/';
		$config['allowed_types'] = '*';
		$config['max_size']	= '0';
		$config['overwrite']	= 'TRUE';
		
		$this->load->library('upload', $config);	
		//$this->upload->set_allowed_types('*');  
		$listdata = "";
		if ( !$this->upload->do_upload()) {
			$hasilnya = array('error' => $this->upload->display_errors());
			$data['isi'] = 'pembelianwip/vimportgagal';
			$data['pesan'] = "Upload file excel gagal. Silahkan cek permission file anda, atau folder tujuan";
			$data['hasilnya'] = $hasilnya;
			$this->load->view('template', $data);
		}
		else {
			$hasilnya = array('upload_data' => $this->upload->data());
				
			$file= './files/'.$filenya;
			$tgl = date("Y-m-d H:i:s");
			//$uid = $this->session->userdata('uid');
			
			require_once './php/excelreader/reader.php';
			$data = new Spreadsheet_Excel_Reader();
			// Set output Encoding.
			$data->setOutputEncoding('CP1251');
			$data->read($file);
			
			error_reporting(E_ALL ^ E_NOTICE);
			
			$temp_no_sj = ''; $temp_tgl_sj = '';
			$tgl = date("Y-m-d H:i:s");
			$jmlimport = 0;
			
			for($i=1;$i<=$data->sheets[0]['numRows'];$i++){ // pengulangan sebanyak jumlah baris
				
				$tgl_sj= trim($data->sheets[0]['cells'][$i][1]);
				
				if ($tgl_sj != '')
					$tgl_sj = date("Y-m-d",($tgl_sj-25569) * 86400);
				if ($tgl_sj != '' && $tgl_sj != $temp_tgl_sj)
					$temp_tgl_sj = $tgl_sj;
				if ($tgl_sj == '')
					$tgl_sj = $temp_tgl_sj;
				
				//echo $tgl_sj."<br>";
				
				$no_sj=trim($data->sheets[0]['cells'][$i][2]);				
				if ($no_sj != '' && $no_sj != $temp_no_sj)
					$temp_no_sj = $no_sj;
				if ($no_sj == '')
					$no_sj = $temp_no_sj;
				
				$kode_brg_jadi= trim($data->sheets[0]['cells'][$i][3]);
				
				if ($kode_brg_jadi != '') {
					$pisah1 = explode(" ", $kode_brg_jadi);
					$kode1= $pisah1[0];
					$kode2= $pisah1[1];
					$kode_brg_jadi = $kode1.$kode2."00";
					
					$nama_brg_jadi=$data->sheets[0]['cells'][$i][4];
					$qty=$data->sheets[0]['cells'][$i][5];
					$harga=$data->sheets[0]['cells'][$i][6];
					$harga = round($harga, 2);
					//$total=$data->sheets[0]['cells'][$i][7];
					$no_faktur=trim($data->sheets[0]['cells'][$i][8]);
					//$grandtotal=$data->sheets[0]['cells'][$i][9];
					//echo $tgl_sj." ".$no_sj." ".$kode_brg_jadi." ".$nama_brg_jadi." ".$qty." " .$harga." ".$no_faktur."<br>";
					
					$subtotal = $qty*$harga;
					$subtotal = round($subtotal, 2);
					
					// 27-08-2013 cek apakah udh pernah ada datanya
					$queryxx	= $this->db->query(" SELECT b.id FROM tm_fakturmasukwip a, tm_fakturmasukwip_detail b 
					 			WHERE a.id = b.id_fakturmasukwip AND a.kode_unit_jahit = '$kode_unit' AND b.no_sj = '$no_sj' 
								AND b.tgl_sj = '$tgl_sj' LIMIT 1 ");
					if ($queryxx->num_rows() == 0){
						$data_faktur_detail	= array(
											 'id_fakturmasukwip'=>'0',
											 'kode_brg_jadi'=>$kode_brg_jadi,
											 'qty'=>$qty,
											 'harga'=>$harga,
											 'diskon'=>'0',
											 'subtotal'=>$subtotal,
											 'no_sj'=>$no_sj,
											 'tgl_sj'=>$tgl_sj
											);
						$this->db->insert('tm_fakturmasukwip_detail',$data_faktur_detail);
						$jmlimport++;
						
						if ($no_faktur != '') {
							$data_faktur	= array(
										 'no_faktur'=>$no_faktur,
										 'tgl'=>$tgl_sj,
										 'kode_unit_jahit'=>$kode_unit,
										// 'grandtotal'=>$grandtotal,
										 'tgl_input'=>$tgl,
										 'tgl_update'=>$tgl
										);
							$this->db->insert('tm_fakturmasukwip',$data_faktur);
							
							$query3	= $this->db->query(" SELECT id FROM tm_fakturmasukwip ORDER BY id DESC LIMIT 1 ");
							if ($query3->num_rows() > 0){
								$hasilrow3 = $query3->row();
								$last_id	= $hasilrow3->id;
								
								$this->db->query(" UPDATE tm_fakturmasukwip_detail SET id_fakturmasukwip = '$last_id'
												WHERE id_fakturmasukwip = '0' ");
												
								$query3	= $this->db->query(" SELECT sum(subtotal) as jumnya FROM tm_fakturmasukwip_detail 
										WHERE id_fakturmasukwip='$last_id' ");
								$hasilrow3 = $query3->row();
								$grandtotal	= $hasilrow3->jumnya;
								
								$this->db->query(" UPDATE tm_fakturmasukwip SET grandtotal = '$grandtotal'
												WHERE id = '$last_id' ");
							} // end if
						} // end if $no_faktur != ''
					}  // end if pengecekan
				} // end if kode_brg_jadi != ''
			} // end for
			// ====================================================================================================

				$datax['isi'] = 'pembelianwip/vimportsukses';
				$datax['pesan'] = "Import file faktur pembelian WIP berhasil. Data yang ditransfer sebanyak: ".$jmlimport;
				$datax['outputnya'] = "";
				$this->load->view('template', $datax);
				
		}
	}
}
