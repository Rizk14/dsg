<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_penjualanndo']	= $this->lang->line('page_title_penjualanndo');
			$data['form_title_detail_penjualanndo']	= $this->lang->line('form_title_detail_penjualanndo');
			$data['list_penjualanndo_kd_brg']	= $this->lang->line('list_penjualanndo_kd_brg');
			$data['list_penjualanndo_no_faktur']	= $this->lang->line('list_penjualanndo_no_faktur');
			$data['list_penjualanndo_jenis_brg']	= $this->lang->line('list_penjualanndo_jenis_brg');
			$data['list_penjualanndo_s_produksi'] = $this->lang->line('list_penjualanndo_s_produksi');
			$data['list_penjualanndo_no_do']	= $this->lang->line('list_penjualanndo_no_do');
			$data['list_penjualanndo_nm_brg']	= $this->lang->line('list_penjualanndo_nm_brg');
			$data['list_penjualanndo_qty']	= $this->lang->line('list_penjualanndo_qty');
			$data['list_penjualanndo_hjp']	= $this->lang->line('list_penjualanndo_hjp');
			$data['list_penjualanndo_amount']	= $this->lang->line('list_penjualanndo_amount');
			$data['list_penjualanndo_total_pengiriman']	= $this->lang->line('list_penjualanndo_total_pengiriman');
			$data['list_penjualanndo_total_penjualan']	= $this->lang->line('list_penjualanndo_total_penjualan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			$this->load->model('listpenjualandolar/mclass');
			$data['opt_jns_brg']	= $this->mclass->lklsbrg();
			$data['isi']	= 'listpenjualandolar/vmainform';
			$this->load->view('template',$data);
			
			
	}

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualandolar/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/listpenjualandolar/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
				
		$this->load->view('listpenjualandolar/vlistformbrgjadi',$data);			
	}	

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanndo/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/listpenjualanndo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
				
		$this->load->view('listpenjualanndo/vlistformbrgjadi',$data);			
	}

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('listpenjualandolar/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	

	function listsjp() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4);
		
		$data['iterasi']	= $iterasi;

		$data['page_title']	= "DATA MASTER BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualandolar/mclass');

		$query	= $this->mclass->lsjp();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listpenjualandolar/cform/listsjpnext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		//$this->pagination->initialize($pagination);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lsjpperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('listpenjualandolar/vlistformsjp',$data);
	}

	function listsjpnext() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4);
		
		$data['iterasi']	= $iterasi;
		
		$data['page_title']	= "DATA MASTER BARANG JADI";
		
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualandolar/mclass');

		$query	= $this->mclass->lsjp();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listpenjualandolar/cform/listsjpnext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lsjpperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('listpenjualandolar/vlistformsjp',$data);
	}

	function flistsjp() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		//$ibranchhidden	= $this->input->post('ibranchhidden')?$this->input->post('ibranchhidden'):$this->input->get_post('ibranchhidden');
		
		$iterasi	= $this->uri->segment(4);
		
		$data['iterasi']	= $iterasi;
		
		$data['page_title']	= "DATA MASTER BARANG JADI";
		$data['isi']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualandolar/mclass');

		$query	= $this->mclass->flsjp($key);
		$jml	= $query->num_rows();

		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				$list .= "
				 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qty','$row->nilai','$row->isj','$row->qty');\">".$row->imotif."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qty','$row->nilai','$row->isj','$row->qty');\">".$row->productname."</a></td>
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qty','$row->nilai','$row->isj','$row->qty');\">".$row->qty."</a></td>
				 </tr>";
				 $cc+=1;
			}
		} else {
			$list.="";
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
 	
	function carilistpenjualandolar() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$nofaktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->input->get_post('no_faktur');
				
		$data['nofaktur']	= $nofaktur;
		
		$data['page_title_penjualanndo']	= $this->lang->line('page_title_penjualanndo');
		$data['form_title_detail_penjualanndo']	= $this->lang->line('form_title_detail_penjualanndo');
		$data['list_penjualanndo_kd_brg']	= $this->lang->line('list_penjualanndo_kd_brg');
		$data['list_penjualanndo_no_faktur']	= $this->lang->line('list_penjualanndo_no_faktur');		
		$data['list_penjualanndo_jenis_brg']	= $this->lang->line('list_penjualanndo_jenis_brg');
		$data['list_penjualanndo_s_produksi'] = $this->lang->line('list_penjualanndo_s_produksi');
		$data['list_penjualanndo_no_do']	= $this->lang->line('list_penjualanndo_no_do');
		$data['list_penjualanndo_nm_brg']	= $this->lang->line('list_penjualanndo_nm_brg');
		$data['list_penjualanndo_qty']	= $this->lang->line('list_penjualanndo_qty');
		$data['list_penjualanndo_hjp']	= $this->lang->line('list_penjualanndo_hjp');
		$data['list_penjualanndo_amount']	= $this->lang->line('list_penjualanndo_amount');
		$data['list_penjualanndo_total_pengiriman']	= $this->lang->line('list_penjualanndo_total_pengiriman');
		$data['list_penjualanndo_total_penjualan']	= $this->lang->line('list_penjualanndo_total_penjualan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();
		
		$this->load->model('listpenjualandolar/mclass');
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		$data['query']	= $this->mclass->clistpenjualandolar($nofaktur);
		$data['isi']	= 'listpenjualandolar/vlistform';
			$this->load->view('template',$data);
		
	}
	
	function edit() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
			$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
			$data['form_nomor_f_fpenjualanndo']	= $this->lang->line('form_nomor_f_fpenjualanndo');
			$data['form_tgl_f_fpenjualanndo']	= $this->lang->line('form_tgl_f_fpenjualanndo');
			$data['form_cabang_fpenjualanndo']	= $this->lang->line('form_cabang_fpenjualanndo');
			$data['form_pilih_cab_fpenjualanndo']	= $this->lang->line('form_pilih_cab_fpenjualanndo');
			$data['form_pilih_cab_manual_fpenjualanndo'] = $this->lang->line('form_pilih_cab_manual_fpenjualanndo');
			$data['form_detail_f_fpenjualanndo']	= $this->lang->line('form_detail_f_fpenjualanndo');
			$data['form_kd_brg_fpenjualanndo']	= $this->lang->line('form_kd_brg_fpenjualanndo');
			$data['form_nm_brg_fpenjualanndo']	= $this->lang->line('form_nm_brg_fpenjualanndo');
			$data['form_hjp_fpenjualanndo']	= $this->lang->line('form_hjp_fpenjualanndo');
			$data['form_qty_fpenjualanndo']	= $this->lang->line('form_qty_fpenjualanndo');
			$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
			$data['form_ket_f_fpenjualanndo']	= $this->lang->line('form_ket_f_fpenjualanndo');
			$data['form_tgl_jtempo_fpenjualanndo']	= $this->lang->line('form_tgl_jtempo_fpenjualanndo');
			$data['form_no_fpajak_fpenjualanndo']	= $this->lang->line('form_no_fpajak_fpenjualanndo');
			$data['form_tgl_fpajak_fpenjualanndo']	= $this->lang->line('form_tgl_fpajak_fpenjualanndo');
			$data['form_ket_cetak_fpenjualanndo']	= $this->lang->line('form_ket_cetak_fpenjualanndo');
			$data['form_tnilai_fpenjualanndo']	= $this->lang->line('form_tnilai_fpenjualanndo');
			$data['form_diskon_fpenjualanndo']	= $this->lang->line('form_diskon_fpenjualanndo');
			$data['form_dlm_fpenjualanndo']	= $this->lang->line('form_dlm_fpenjualanndo');
			$data['form_total_fpenjualanndo']	= $this->lang->line('form_total_fpenjualanndo');
			$data['form_ppn_fpenjualanndo']		= $this->lang->line('form_ppn_fpenjualanndo');
			$data['form_grand_t_fpenjualanndo']	= $this->lang->line('form_grand_t_fpenjualanndo');
			
			$data['button_update']	= $this->lang->line('button_update');
			$data['button_batal']	= $this->lang->line('button_batal');
			
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$tahun	= date("Y");
			$data['tjthtempo']	= "";
		
			$ifakturcode	= $this->uri->segment('4');
			$data['ifakturcode']	= $ifakturcode;
			
			$this->load->model('listpenjualandolar/mclass');
			
			$qryfakheader	= $this->mclass->getfakheader($ifakturcode);
			
			if($qryfakheader->num_rows()>0) {
				
				$row_fakheader	= $qryfakheader->row();
				
				$i_faktur_code	= $row_fakheader->i_faktur_code;
				$data['i_faktur']		= $row_fakheader->i_faktur;
				$data['i_faktur_code']	= $row_fakheader->i_faktur_code;
				$tglfaktur	= explode("-",$row_fakheader->d_faktur,strlen($row_fakheader->d_faktur)); // YYYY-mm-dd
				$data['f_printed']	= $row_fakheader->f_printed=='t'?'checked':'';
				$data['tgFAKTUR']	= $tglfaktur[2]."/".$tglfaktur[1]."/".$tglfaktur[0];
				$ibranchname		= $row_fakheader->e_branch_name;		
				$data['ibranchname']	= $ibranchname;
				$data['e_note_faktur']	= $row_fakheader->e_note_faktur;
				$data['v_total_faktur']	= $row_fakheader->v_total_faktur;
				
				/*$qicustomer	= $this->mclass->geticustomer($ibranchname);
				if($qicustomer->num_rows()>0) {
					
					$row_icustomer	= $qicustomer->row();
					
					$icustomer	= $row_icustomer->i_customer;
					$ibranch	= $row_icustomer->i_branch;
					
					$data['icustomer']	= $icustomer;
					$data['ibranch']	= $ibranch;
				} */
				
				$data['fakturitem']	= $this->mclass->lfakturitem($row_fakheader->i_faktur);
				//$data['opt_cabang']	= $this->mclass->lcabang($icustomer);
			}
				
			
				$data['isi']=	'listpenjualandolar/veditform';	
		$this->load->view('template',$data);
			
	}

	function cari_fpenjualan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$foriginal	= $this->input->post('foriginal')?$this->input->post('foriginal'):$this->input->get_post('foriginal');
		$fpenj	= $this->input->post('fpenj')?$this->input->post('fpenj'):$this->input->get_post('fpenj');
		
		$this->load->model('listpenjualanndo/mclass');
		
		$qnsop	= $this->mclass->cari_fpenjualan($fpenj);
		
		if($qnsop->num_rows()>0 && $foriginal!=$fpenj) {
			echo "Maaf, No. Faktur sudah ada!".$foriginal;
		}		
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanndo']	= $this->lang->line('page_title_penjualanndo');
		$data['form_title_detail_penjualanndo']	= $this->lang->line('form_title_detail_penjualanndo');
		$data['list_penjualanndo_kd_brg']	= $this->lang->line('list_penjualanndo_kd_brg');
		$data['list_penjualanndo_no_faktur']	= $this->lang->line('list_penjualanndo_no_faktur');
		$data['list_penjualanndo_jenis_brg']	= $this->lang->line('list_penjualanndo_jenis_brg');
		$data['list_penjualanndo_s_produksi'] = $this->lang->line('list_penjualanndo_s_produksi');
		$data['list_penjualanndo_no_do']	= $this->lang->line('list_penjualanndo_no_do');
		$data['list_penjualanndo_nm_brg']	= $this->lang->line('list_penjualanndo_nm_brg');
		$data['list_penjualanndo_qty']	= $this->lang->line('list_penjualanndo_qty');
		$data['list_penjualanndo_hjp']	= $this->lang->line('list_penjualanndo_hjp');
		$data['list_penjualanndo_amount']	= $this->lang->line('list_penjualanndo_amount');
		$data['list_penjualanndo_total_pengiriman']	= $this->lang->line('list_penjualanndo_total_pengiriman');
		$data['list_penjualanndo_total_penjualan']	= $this->lang->line('list_penjualanndo_total_penjualan');
		
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('listpenjualandolar/mclass');
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();

		$i_product	= array();
		$e_product_name	= array();
		$v_hjp	= array();
		$n_quantity	= array();
		$v_unit_price	= array();
		
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$i_faktur	= $this->input->post('i_faktur');
		$d_faktur	= $this->input->post('d_faktur');
		$nama_pelanggan	= $this->input->post('nama_pelanggan');
		$ifakturcode	= $this->input->post('ifakturcodehiden');
		$ifaktur	= $this->input->post('ifakturhiden');
		
		$i_product_0	= $this->input->post('i_product'.'_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iteration;$cacah++) {

			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_hjp[$cacah]	= $this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah);
			$n_quantity[$cacah]	= $this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
						
		}
		
		$e_note_faktur	= $this->input->post('e_note_faktur');
		$v_total_faktur	= $this->input->post('v_total_faktur');
		
		 // dd/mm/YYYY
		$ex_d_faktur	= explode("/",$d_faktur,strlen($d_faktur));
		$nw_d_faktur	= $ex_d_faktur[2]."-".$ex_d_faktur[1]."-".$ex_d_faktur[0]; //YYYY-mm-dd
		
		$ex_v_total_faktur	= explode(".",$v_total_faktur,strlen($v_total_faktur));		
		$nw_v_total_faktur	= $ex_v_total_faktur[0];
		
		if(!empty($i_faktur) &&
		   !empty($d_faktur)
		) {
			if(!empty($i_product_0)) {
				
				$qnsop	= $this->mclass->cari_fpenjualan($i_faktur);	
					
				if(($qnsop->num_rows() < 0 || $qnsop->num_rows()==0) || ($ifakturcode==$i_faktur)) {
					$this->mclass->mupdate($i_faktur,$nw_d_faktur,$nama_pelanggan,$e_note_faktur,$nw_v_total_faktur,$i_product,$e_product_name,
					$v_hjp,$n_quantity,$v_unit_price,$iteration,$ifakturcode,$ifaktur);
				}else{
					print "<script>alert(\"Maaf, Nomor Faktur tsb sebelumnya telah diinput. Terimakasih.\");show(\"listpenjualandolar/cform\",\"#content\");</script>";
				}
				
			}else{
				print "<script>alert(\"Maaf, item Faktur Penjualan hrs terisi. Terimakasih.\");show(\"listpenjualandolar/cform\",\"#content\");</script>";
			}
				
		}else{
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}	
	}
	
	function undo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_faktur_code	= $this->uri->segment(4);
		$i_faktur	= $this->uri->segment(5);
		
		$this->load->model('listpenjualandolar/mclass');
		
		$this->mclass->mbatal($i_faktur,$i_faktur_code);			
	}
	
}

?>
