<?php
class cform extends CI_controller {
	function __construct(){
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('info-btb-pck/mmaster');
	}
	
		function index(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if	(!isset($is_logged_in) || $is_logged_in!=true){
			redirect ('loginform');
			}
			$data['list_unit_packing'] = $this->mmaster->getunitpacking();
			$data['isi'] = 'info-btb-pck/vmainform';
			$this->load->view('template',$data);
			}
			
		function viewlapbtb(){
			$is_logged_in = $this->session->userdata('is_logged_in');
			if (!isset($is_logged_in) || $is_logged_in!=true){	
			redirect ('loginform');
		}
		$data['isi']='info-btb-pck/vviewform';
		$id_unit_packing = $this->input->post('id_unit_packing',TRUE);
		$date_from = $this->input->post('date_from',TRUE);
		$date_to = $this->input->post('date_to',TRUE);
		
		$querynya= $this->mmaster->getallviewlapbtb($id_unit_packing,$date_from,$date_to);
		$data['query']=$querynya;
		
		if (is_array($querynya)){
			$data['jumlah']=count($querynya);
			}
		else{
			$data['jumlah']= 0;
			}
			//print_r($id_unit_packing);
			
		$query4 = $this->db->query ("select * from tm_unit_packing where id='$id_unit_packing'");
		if($query4->num_rows() > 0){
		$hasil4 = $query4->row();
		$kode_unit_packing = $hasil4->kode_unit;
		$nama_unit_packing = $hasil4->nama;
	}
	else{
		$kode_unit_packing = '';
		$nama_unit_packing = '';
	}
	//print_r($nama_unit_packing );
	$data['id_unit_packing'] = $id_unit_packing;
	$data['kode_unit_packing'] = $kode_unit_packing;
	$data['nama_unit_packing'] = $nama_unit_packing;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$this->load->view('template',$data);
	}
	function export() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$kode_unit = $this->input->post('kode_unit', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);
		
		$id_unit_packing = $this->input->post('id_unit_packing', TRUE);  	
		
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster-> getallviewlapbtb($id_unit_packing,$date_from,$date_to);
			
	$html_data = "
	<table border='1 cellpadding= '1' cellspacing = '1' width='100%'>
	<thead>
	<tr>
			<th colspan='11' align='center'> Laporan Harga Unit packing Berdasarkan BTB packing </th>
		 </tr>
		 
		 <tr>
			<th colspan='11' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
	 <tr class='judulnya'>
	<th>No</th>

	<th>No SJ Pembelian</th>
	<th>Tanggal SJ</th>
	<th>Unit packing</th>
	<th>Kode Barang</th>
	<th>Nama Barang</th>
	<th>Qty</th>
	<th>Harga</th>
	<th>Diskon</th>
	<th>Sub total</th>
	<th>Jumlah Total</th>
</tr>
</thead>
<tbody>";
	

			if (is_array($query)) {
				$tot_grandtotal = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					//$tot_grandtotal += $query[$j]['grandtotal'];
				} // end header
				
			}
			else {
				$tot_grandtotal = 0;
			}
		 
			if (is_array($query)) {
				$no = 1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.= "<tr class=\"record\">";
				 $html_data.=    "<td align='center'>".$no."</td>";

				 
				 
				$html_data.=    "<td>".$query[$j]['no_sjmasukpembelianpack']."</td>";
	
				 $html_data.=    "<td>".$query[$j]['tgl_sj']."</td>";
				  $html_data.=    "<td>".$query[$j]['nama_unit_packing']."</td>";
				 
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_brg_wip'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_brg_wip'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['qty'];
						  //$html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				// $html_data.=    "<td></td>";
				
				
				
				 
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['harga'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 
				 $html_data.= "</td>";
			//	 $html_data.=    "<td></td>";
				 $html_data.= "<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 $html_data.= $var_detail[$k]['diskon'];
						$html_data.=  "" ;
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 $html_data.= $var_detail[$k]['subtotal'];
						 $html_data.= ""   ;
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 


				

				 $html_data.=    "<td align='center'>".$query[$j]['total']."</td>";
				$tot_grandtotal+=$query[$j]['total'];
				
				 
				 $html_data.=  "</tr>";

				$no++;
		 	} // end for
		   }

		 $html_data.=" <tr>
			<td colspan='10' align='right'><b>TOTAL</b></td>
			<td align='right'><b> ".number_format($tot_grandtotal,2,',','.'). "</b></td>
		 </tr>
 	</tbody>
</table><br>";
		// ====================================================================

		$nama_file = "laporan_faktur_pembelian_retur_wip";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
}
?>
