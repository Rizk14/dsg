<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('exp-htg-dgg/mmaster');
  }
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	//$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['isi'] = 'exp-htg-dgg/vmainform';
	$this->load->view('template',$data);
	}
	
	
	// ===================================== 12-06-2015 ========================================================
	function exportcsv(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$kategori = $this->input->post('kategori', TRUE);
		$supplier = $this->input->post('id_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		
		$query = $this->mmaster->get_all_pembelian($jenis_beli, $kategori, $date_from, $date_to, $supplier);
		
		$filename = "tm_payment_pembelian".$jenis_beli.".csv";
	
	
	$now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
  
	
	$out = fopen('php://output', 'w');
		
	fputcsv($out, array('i_dtap','i_area','i_supplier','i_pajak',
	'd_dtap','d_bayar','d_due_date','d_pajak','f_pkp',
	'n_discount','v_gross','v_discount','v_ppn','v_netto',
	'v_sisa','f_dtap_cancel','n_print','n_dtap_year','d_entry','d_update',
	
	'i_dtap' , 'i_do' ,'i_op','i_supplier', 'i_area' , 
	'i_product' , 'i_product_motif', 'e_product_name', 'd_dtap', 
	'd_do', 'd_op', 'n_jumlah', 'v_pabrik', 'v_gross', 'v_discount', 
	'v_netto', 'n_item_no' ,  'i_price_group', 'n_dtap_year' ));
	
	
	
	if (is_array($query)) {
		
		for($j=0;$j<count($query);$j++){
			
		$tgl_faktur = explode('-', $query[$j]['tgl_faktur']);
		
			//print_r($tgl_faktur[0]);
		
			fputcsv($out, array($query[$j]['no_faktur'],"00",$query[$j]['kode_supplier'],$query[$j]['no_faktur_pajak'],
			$query[$j]['tgl_faktur'] ,"","",$query[$j]['tgl_faktur_pajak'],$query[$j]['pkp'],
			"",$query[$j]['dpp'],"",$query[$j]['total_pajak'],$query[$j]['total'],
			$query[$j]['sisa_hutang'],"f","0",$tgl_faktur[0],$query[$j]['tgl_input'],$query[$j]['tgl_update'],
			
			$query[$j]['no_faktur'],$query[$j]['no_sj'],"",$query[$j]['kode_supplier'],"00",
			$query[$j]['kode_brg'],$query[$j]['kode_brg'],$query[$j]['nama_brg'],$query[$j]['tgl_faktur'],
			$query[$j]['tgl_sj'],"",$query[$j]['qty'],"","",$query[$j]['diskon'],
			"","","",""
			));
			
		} // end for
	}
	
	fclose($out);
	
	die();
	$this->load->view('template',$data);
  }
	
	// ==========================================================================================================
}
?>
