<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-lok-gudang/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode_lokasi;
			$enama = $row->nama;
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-lok-gudang/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode_lokasi', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('mst-lok-gudang/cform');
		}
		else
		{
			$kode 	= $this->input->post('kode_lokasi', TRUE);
			$id_lokasi 	= $this->input->post('id_lokasi', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit != 1) {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					redirect('mst-lok-gudang/cform');
				}
				else {
					$this->mmaster->save($kode, $id_lokasi, $nama, $goedit);
					redirect('mst-lok-gudang/cform');
				}
			}
			else {
				$this->mmaster->save($kode, $id_lokasi, $nama, $goedit);
				redirect('mst-lok-gudang/cform');
			}
		}
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete($id);
    redirect('mst-lok-gudang/cform');
  }
  
  
}
