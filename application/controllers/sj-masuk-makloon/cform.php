<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('sj-masuk-makloon/mmaster');
  }

function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$no_sj_keluar = $this->input->post('sj_keluar', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	$id_sj_detail = $this->input->post('id_brg', TRUE);  
	$unit_makloon = $this->input->post('unit_makloon', TRUE);  
	$is_no_sjkeluar = $this->input->post('is_no_sjkeluar', TRUE);    
	
	$list_brg = explode(";", $id_sj_detail);
	$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
	  if ($is_no_sjkeluar == '') { // jika ambil dari SJ keluar
		if ($no_sj_keluar !='') {
			$data['sj_detail'] = $this->mmaster->get_detail_sj_keluar($list_brg, $unit_makloon);
			$data['msg'] = '';
			$data['no_sj_keluar'] = $no_sj_keluar;
		}
		/*else {
			$data['msg'] = 'SJ Keluar harus dipilih';
			$data['no_sj_keluar'] = '';
			$data['list_quilting'] = $this->mmaster->get_supplier();
		}*/
		$data['go_proses'] = '1';
		
		$query3	= $this->db->query(" SELECT nama, pkp, tipe_pajak FROM tm_supplier WHERE kode_supplier = '$unit_makloon' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) 
			$nama_unit	= $hasilrow->nama;
				
		$data['nama_unit'] = $nama_unit;
		$data['pkp'] = $hasilrow->pkp;
		$data['tipe_pajak'] = $hasilrow->tipe_pajak;
		$data['unit_makloon'] = $unit_makloon;
		
		$data['isi'] = 'sj-masuk-makloon/vmainform';
		$this->load->view('template',$data);
	  } // end if is_no_sjkeluar
	  else { // jika is_no_sjkeluar == 't'
		$query3	= $this->db->query(" SELECT nama, pkp, tipe_pajak FROM tm_supplier WHERE kode_supplier = '$unit_makloon' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) 
			$nama_unit	= $hasilrow->nama;
				
			$data['nama_unit'] = $nama_unit;
			$data['pkp'] = $hasilrow->pkp;
			$data['tipe_pajak'] = $hasilrow->tipe_pajak;
			$data['unit_makloon'] = $unit_makloon;
				
		$data['go_proses'] = '1';
		$data['msg'] = '';
		$data['no_sj_keluar'] = '';
		$data['is_no_sjkeluar'] = $is_no_sjkeluar;
		$data['isi'] = 'sj-masuk-makloon/vmainformnosjkeluar';
		$this->load->view('template',$data);
	  }
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['list_quilting'] = $this->mmaster->get_supplier();
		$data['isi'] = 'sj-masuk-makloon/vmainform';
		$this->load->view('template',$data);
	}

	// ====== tes
	/*	$contoh_data = "3;2;1;";
		$explode_contoh_data = explode(";", $contoh_data);
		
			foreach($explode_contoh_data as $rownya) {
				if ($rownya != '') {
					$nilai_yard = $rownya / 2;
					$contoh_data_yard[] = $nilai_yard;
				}
			}
			$comma_separated = implode(";", $contoh_data_yard);
			echo $comma_separated; die();
		*/
	//==============
  }
  
  function edit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	$id_sj 	= $this->uri->segment(4);
	$is_no_sjkeluar = $this->uri->segment(5);
	$cur_page 	= $this->uri->segment(6);
	$is_cari 	= $this->uri->segment(7);
	$unit_makloon 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['query'] = $this->mmaster->get_sj($id_sj, $is_no_sjkeluar);
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['cunit_makloon'] = $unit_makloon;
	$data['carinya'] = $carinya;
	$data['is_no_sjkeluar'] = $is_no_sjkeluar;

	if ($is_no_sjkeluar == 0)
		$data['isi'] = 'sj-masuk-makloon/veditform';
	else
		$data['isi'] = 'sj-masuk-makloon/veditformnosjkeluar';
	$this->load->view('template',$data);

  }

  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj_keluar 	= $this->input->post('no_sj_keluar', TRUE);
			$unit_makloon = $this->input->post('unit_makloon', TRUE);  
			$is_no_sjkeluar = $this->input->post('is_no_sjkeluar', TRUE);  
			
			if ($is_no_sjkeluar == 't') {
				$dpp = $this->input->post('dpp', TRUE);
				$tot_pajak = $this->input->post('tot_pajak', TRUE);
				$gtotal = $this->input->post('gtotal', TRUE);
				$uang_muka = $this->input->post('uang_muka', TRUE);
				$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			}
			else {
				$dpp = 0;
				$tot_pajak = 0;
				$gtotal = 0;
				$uang_muka = 0;
				$sisa_hutang = 0;
			}
			
			//05-04-2012
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			$tgl_sj = $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			
			$ket = $this->input->post('ket', TRUE);  
			$no 	= $this->input->post('no', TRUE);
						
			// jika edit, var ini ada isinya
			$id_sj_makloon 	= $this->input->post('id_sj_masuk', TRUE);
			
			if ($id_sj_makloon == '') {
				$cek_data = $this->mmaster->cek_data($no_sj, $unit_makloon);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'sj-masuk-makloon/vmainform';
					$data['msg'] = "Data no SJ ".$no_sj." untuk supplier ".$unit_makloon." sudah ada..!";
					$data['list_quilting'] = $this->mmaster->get_supplier();
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						if ($is_no_sjkeluar == 't') {
							$kode_brg_jadi = '';
							$detail_yard = '';
							$qty_meter = 0;
							$qty_hasil = $this->input->post('qty_'.$i, TRUE);
							$kode_bhn_baku = $this->input->post('kode_bhn_baku_'.$i, TRUE);
							$kode_bhn_quilting = $this->input->post('kode_bhn_quilting_'.$i, TRUE);
							$harga = $this->input->post('harga_'.$i, TRUE);
							$pajak = $this->input->post('pajak_'.$i, TRUE);
							$total = $this->input->post('total_'.$i, TRUE);
						}
						else {
							$kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
							$detail_yard = $this->input->post('detail_yard_'.$i, TRUE);
							$qty_meter = $this->input->post('qty_meter_'.$i, TRUE);
							$qty_hasil = $this->input->post('qty_hasil_'.$i, TRUE);
							$kode_bhn_baku = $this->input->post('kode_brg_'.$i, TRUE);
							$kode_bhn_quilting = $this->input->post('kode_brg_makloon_'.$i, TRUE);
							$harga = 0;
							$pajak = 0;
							$total = 0;
						}
						$this->mmaster->save($jenis_pembelian, $no_sj_keluar, $no_sj, $tgl_sj, $unit_makloon, $ket, $is_no_sjkeluar,
								$dpp, $tot_pajak, $gtotal, $uang_muka, $sisa_hutang,
								$kode_bhn_baku, $kode_bhn_quilting, $kode_brg_jadi,
								$this->input->post('id_sj_detail_'.$i, TRUE), $this->input->post('id_sj_proses_quilting_'.$i, TRUE), 
								$qty_meter, $qty_hasil,
								$detail_yard, $harga, $pajak, $total );
					}
					redirect('sj-masuk-makloon/cform/view');
				}
			} // end if id_sj_makloon == ''
			else { // update
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$cunit_makloon = $this->input->post('cunit_makloon', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
			
				$tgl = date("Y-m-d");
				
				/*if ($is_no_sjkeluar == 't') {
					$dpp = $this->input->post('dpp', TRUE);
					$tot_pajak = $this->input->post('tot_pajak', TRUE);
					$gtotal = $this->input->post('gtotal', TRUE);
					$uang_muka = $this->input->post('uang_muka', TRUE);
					$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
				}*/
				
				$sql = "UPDATE tm_sj_hasil_makloon SET no_sj = '$no_sj', tgl_sj = '$tgl_sj', jenis_pembelian = '$jenis_pembelian',
									keterangan = '$ket', tgl_update = '$tgl' ";
				
				if ($is_no_sjkeluar == 't') {
					$sql.= " ,dpp = '$dpp', total_pajak = '$tot_pajak', total = '$gtotal', uang_muka = '$uang_muka',
							sisa_hutang = '$sisa_hutang' ";
				}
				$sql.="where id= '$id_sj_makloon'";
				
				// update di tabel tm_sj_hasil_makloon
					$this->db->query($sql);
				
				$jumlah_input=$no-1;
				for ($i=1;$i<=$jumlah_input;$i++)
				{ // detailnya
					
					if ($is_no_sjkeluar == 't') {
						if ($this->input->post('id_sj_detail_'.$i, TRUE) == '') {
							// a. insert item detail
							$data_detail = array(
								'kode_brg'=>$this->input->post('kode_bhn_baku_'.$i, TRUE),
								'kode_brg_makloon'=>$this->input->post('kode_bhn_quilting_'.$i, TRUE),
								'qty_makloon'=>$this->input->post('qty_'.$i, TRUE),
								'harga'=>$this->input->post('harga_'.$i, TRUE),
								'pajak'=>$this->input->post('pajak_'.$i, TRUE),
								'biaya'=>$this->input->post('total_'.$i, TRUE),
								'id_sj_hasil_makloon'=>$id_sj_makloon
							);
							$this->db->insert('tm_sj_hasil_makloon_detail',$data_detail);
						}
						else {
							//echo $this->input->post('kode_bhn_quilting_'.$i, TRUE); die();
							$sql = " UPDATE tm_sj_hasil_makloon SET 
									status_stok = 'f' WHERE id = '".$id_sj_makloon."' ";
							$this->db->query($sql);
							
							$sql = " UPDATE tm_sj_hasil_makloon_detail SET 
									kode_brg = '".$this->input->post('kode_bhn_baku_'.$i, TRUE)."',
									kode_brg_makloon = '".$this->input->post('kode_bhn_quilting_'.$i, TRUE)."', 
									qty_makloon = '".$this->input->post('qty_'.$i, TRUE)."',
									harga = '".$this->input->post('harga_'.$i, TRUE)."',
									pajak = '".$this->input->post('pajak_'.$i, TRUE)."',
									biaya = '".$this->input->post('total_'.$i, TRUE)."'
									WHERE id = '".$this->input->post('id_sj_detail_'.$i, TRUE)."' ";
							$this->db->query($sql);
						}
					} // 
					
					// ============ update stok =====================
					//if ($is_no_sjkeluar == 'f') {

						$qty_makloon_lama = $this->input->post('qty_lama_'.$i, TRUE);
						$qty_makloon = $this->input->post('qty_'.$i, TRUE);
						$harga = $this->input->post('harga_'.$i, TRUE);
						
					if ($is_no_sjkeluar == 'f') {
						$kode_brg_makloon = $this->input->post('kode_brg_makloon_'.$i, TRUE);
						$kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
						$id_sj_proses_makloon_detail = $this->input->post('id_sj_proses_makloon_detail_'.$i, TRUE);
						$detail_yard = $this->input->post('detail_yard_'.$i, TRUE);
					}
					else {
						$kode_brg_makloon = $this->input->post('kode_bhn_quilting_'.$i, TRUE);
						$kode_brg_jadi = '';
						$id_sj_proses_makloon_detail = 0;
						$detail_yard = '';
					}
						
						$id_sj_detail = $this->input->post('id_sj_detail_'.$i, TRUE);
						if ($id_sj_detail == '')
							$id_sj_detail = 0;
						
						if ($qty_makloon != $qty_makloon_lama) {
							// ambil id_bonm
							$query3	= $this->db->query(" SELECT no_bonm, status_stok FROM tm_sj_hasil_makloon_detail 
											WHERE id = '".$id_sj_detail."' ");
							if ($query3->num_rows() == 0){
								$no_bonm = '';
								$status_stok = 'f';
							}
							else {
								$hasilrow = $query3->row();
								$no_bonm = $hasilrow->no_bonm;
								$status_stok = $hasilrow->status_stok;
							}
							
							if ($no_bonm != '' && $status_stok == 't') {
								//cek stok terakhir tm_stok, dan reset stoknya
								// 16 NOV 2011: KAYAKNYA STOK HASIL QUILTING INI GA PERLU ADA ACUAN KODE BRG JADI
								/*if ($is_no_sjkeluar == 'f')
									$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon 
											WHERE kode_brg = '".$kode_brg_makloon."' AND kode_brg_jadi = '$kode_brg_jadi' ");
								else */
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon 
											WHERE kode_brg = '".$kode_brg_makloon."' ");
								
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama-$qty_makloon_lama; // berkurang stok karena reset dari SJ masuk utk makloon
								
								// 17 nov 2011 START
								//cek stok terakhir tm_stok_harga, dan update stoknya
								if ($is_no_sjkeluar == 't') {
									$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga 
														WHERE kode_brg_quilting = '".$this->input->post('kode_bhn_quilting_lama_'.$i, TRUE)."' 
														AND harga = '$harga' AND quilting = 't' ");
								}
								else {
									$query3	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga 
														WHERE kode_brg_quilting = '$kode_brg_makloon' 
														AND quilting = 't' ORDER BY id DESC LIMIT 1 ");
								}
														
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
									$harga_nosjkeluar = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
									$harga_nosjkeluar	= $hasilrow->harga;
								}
								$stokreset2 = $stok_lama-$qty_makloon_lama;
								
								if ($qty_makloon_lama > 0) {
									if ($is_no_sjkeluar == 'f') {
										$harga = $harga_nosjkeluar;
										$kode_lama = $kode_brg_makloon;
									}
									else {
										$kode_lama = $this->input->post('kode_bhn_quilting_lama_'.$i, TRUE);
									}
									// if ini tidak perlu karena di bon M masuk pembelian pada bag update stok itu udh ada validasi, jika harga 0 maka tidak bisa update stok
									//if ($harga != '' || $harga != 0) {
										//insert ke tabel tt_stok_hasil_makloon dgn tipe keluar, utk membatalkan data brg masuk
										$this->db->query(" INSERT INTO tt_stok_hasil_makloon (kode_brg, kode_unit, no_bukti, keluar, 
											saldo, tgl_input, harga)
											VALUES ('$kode_lama', '$unit_makloon', '$no_bonm', '$qty_makloon_lama', '$stokreset2', 
											'$tgl', '$harga' ) ");
									//}
																						
									//4. update stok di tm_stok
									$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', tgl_update_stok = '$tgl'
														where kode_brg= '$kode_lama' ");
									
									//if ($harga != '' || $harga != 0) {	
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl' 
														where kode_brg_quilting= '$kode_lama' AND harga = '$harga' ");
									//}
								}

							} // end if no_bonm != '' && status_stok == 't'	
								// ==============================================
							
							if ($is_no_sjkeluar == 'f') {
								// update di tabel tm_sj_hasil_makloon_detail
								$sql = " UPDATE tm_sj_hasil_makloon SET 
									status_stok = 'f' WHERE id = '".$id_sj_makloon."' ";
								$this->db->query($sql);
								
								$this->db->query(" UPDATE tm_sj_hasil_makloon_detail SET qty_makloon = '$qty_makloon',
												detail_pjg_quilting = '$detail_yard', status_stok = 'f'
												where id= '$id_sj_detail' ");
							}
							else {
								//$this->input->post('id_sj_detail_'.$i, TRUE)
								$sql = " UPDATE tm_sj_hasil_makloon_detail SET 
									status_stok = 'f' WHERE id = '".$id_sj_detail."' ";
								$this->db->query($sql);
							}
								
								//ambil id_sj_proses_quilting
							/*	$query3	= $this->db->query(" SELECT id_sj_proses_quilting FROM tm_sj_proses_quilting_detail 
											WHERE id = '".$id_sj_proses_makloon_detail."' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$id_sj_proses_quilting = $hasilrow->id_sj_proses_quilting;
								}
								else
									$id_sj_proses_quilting = 0;
													
									//cek di tabel tm_sj_proses_quilting_detail, apakah status_makloon sudah 't' semua?
										$this->db->select(" id from tm_sj_proses_quilting_detail WHERE status_makloon = 'f' 
															AND id_sj_proses_quilting = '$id_sj_proses_quilting' ", false);
										$query = $this->db->get();
										//jika sudah t semua, maka update tabel tm_sj_proses_quilting di field status_makloon menjadi t
										if ($query->num_rows() == 0){
											$this->db->query(" UPDATE tm_sj_proses_quilting SET status_makloon = 't' 
															where id= '$id_sj_proses_quilting' ");
										}
								*/
										// =======+xxxx
								
								//cek stok terakhir tm_stok, dan update stoknya dgn yg baru
								// KOREKSI 16 NOV 2011: UPDATE STOK TERBARU BUKAN DISINI, TAPI DI FITUR BON M MASUK PEMBELIAN
								
							/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon 
										WHERE kode_brg = '$kode_brg_makloon' AND kode_brg_jadi= '$kode_brg_jadi' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty_makloon; // bertambah stok karena dari SJ masuk utk makloon
												
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$kode_brg_makloon,
										'kode_brg_jadi'=>$kode_brg_jadi,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_makloon',$data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$kode_brg_makloon' AND kode_brg_jadi='$kode_brg_jadi' ");
								}
												
								$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_makloon (kode_brg, kode_brg_jadi, no_bukti, kode_unit, masuk, saldo, tgl_input) 
											VALUES ('$kode_brg_makloon', '$kode_brg_jadi', '$no_sj', '$unit_makloon', '$qty_makloon', '$new_stok', '$tgl') ");	
							*/
							
						} // end if

				} // end for
					
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "sj-masuk-makloon/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "sj-masuk-makloon/cform/cari/".$cunit_makloon."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('sj-masuk-makloon/cform/view');
			}

  }
  
  function view(){ 
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		
    $data['isi'] = 'sj-masuk-makloon/vformview';
    $keywordcari = "all";
    $unit_makloon = '0';
	//$kode_bagian = '';
	
   $jum_total = $this->mmaster->getAlltanpalimit($unit_makloon, $keywordcari); 
							$config['base_url'] = base_url().'index.php/sj-masuk-makloon/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $unit_makloon, $keywordcari);		
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['list_quilting'] = $this->mmaster->get_unit_quilting();
	$data['list_unit'] = $this->mmaster->get_supplier();
	$data['unit_makloon'] = $unit_makloon;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$unit_makloon = $this->input->post('unit_makloon', TRUE);  
	
	if ($keywordcari == '' && $unit_makloon == '') {
		$unit_makloon 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($unit_makloon == '')
		$unit_makloon = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($unit_makloon, $keywordcari);
							$config['base_url'] = base_url().'index.php/sj-masuk-makloon/cform/cari/'.$unit_makloon.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $unit_makloon, $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'sj-masuk-makloon/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_unit'] = $this->mmaster->get_supplier();
	$data['unit_makloon'] = $unit_makloon;
	$this->load->view('template',$data);
  }

  function delete(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $id_sj 	= $this->uri->segment(4);
    $list_brg 	= $this->uri->segment(5);
    $is_no_sjkeluar = $this->uri->segment(6);
    $cur_page 	= $this->uri->segment(7);
    $is_cari 	= $this->uri->segment(8);
    $cunit_makloon 	= $this->uri->segment(9);
    $carinya 	= $this->uri->segment(10);
    
    $this->mmaster->delete($id_sj, $list_brg, $is_no_sjkeluar);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "sj-masuk-makloon/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "sj-masuk-makloon/cform/cari/".$cunit_makloon."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('sj-masuk-makloon/cform/view');
  }
  
  function generate_nomor() {
		//$jenis_brg 	= $this->uri->segment(4);
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================

	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		
	$posisi	= $this->uri->segment(4);
	$kode_supplier 	= $this->uri->segment(5);
	$jenisnya 	= $this->uri->segment(6);
	//echo $kode_supplier." ". $jenisnya." 1<br>";

	if ($posisi == '' || $kode_supplier == '' || $jenisnya == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kode_supplier 	= $this->input->post('kode_supplier', TRUE);  
		$jenisnya 	= $this->input->post('jenisnya', TRUE);  
		//echo $kode_supplier." ". $jenisnya." 2<br>";
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
	if ($keywordcari == '' || ($kode_supplier == '' && $posisi == '' && $jenisnya == '') ) {
		$posisi	= $this->uri->segment(4);
		$kode_supplier 	= $this->uri->segment(5);
		$jenisnya 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
		//echo $kode_supplier." ". $jenisnya." 3<br>";
	}
		
		if ($keywordcari == '')
			$keywordcari = "all";
		
		$submitnya 	= $this->input->post('submit', TRUE);  
		if ($submitnya == "Cari") {
			$jenisnya 	= $this->input->post('jenisnya', TRUE);  
			$posisi 	= $this->input->post('posisi', TRUE);  
			$kode_supplier 	= $this->input->post('kode_supplier', TRUE);   
		}

		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kode_supplier, $jenisnya);
		
		$config['base_url'] = base_url()."index.php/sj-masuk-makloon/cform/show_popup_brg/".$posisi."/".$kode_supplier."/".$jenisnya."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kode_supplier, $jenisnya);

	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['kode_supplier'] = $kode_supplier;
	$data['jenisnya'] = $jenisnya; //echo $kode_supplier." ". $jenisnya."4<br>";
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('sj-masuk-makloon/vpopupbrg',$data);
  }
  
  // popup sj keluar
  function show_popup_sj_keluar(){
	// =======================
	// disini coding utk pengecekan user login
	//========================

	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$unit_makloon	= $this->uri->segment(4);

	if ($unit_makloon == '') {
		$unit_makloon 	= $this->input->post('unit_makloon', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $unit_makloon == '' ) {
			$unit_makloon 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";

		$qjum_total = $this->mmaster->get_sj_keluartanpalimit($keywordcari, $unit_makloon);
		
		$config['base_url'] = base_url()."index.php/sj-masuk-makloon/cform/show_popup_sj_keluar/".$unit_makloon."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_sj_keluar($config['per_page'],$config['cur_page'], $keywordcari, $unit_makloon);

	$data['jum_total'] = count($qjum_total);
	$data['unit_makloon'] = $unit_makloon;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$unit_makloon' ");
	$hasilrow = $query3->row();
	$nama_unit	= $hasilrow->nama;
	$data['nama_unit'] = $nama_unit;

	$this->load->view('sj-masuk-makloon/vpopupsjkeluar',$data);
	
  }
  
}
