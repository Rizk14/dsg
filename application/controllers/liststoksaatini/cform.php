<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			
			$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
			$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
			$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
			$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
			$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			$data['isi']		= "";
			
			$this->load->model('liststoksaatini/mclass');
			
			$data['opt_jns_brg']	= $this->mclass->lklsbrg();
			$data['isi']	= 'liststoksaatini/vmainform';
			$this->load->view('template',$data);
			
		
	}
	
	function carilistbrgjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
		$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
		$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
		$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
		$data['list_stoksaatini_kd_brg']	= $this->lang->line('list_stoksaatini_kd_brg');
		$data['list_stoksaatini_nm_brg']	= $this->lang->line('list_stoksaatini_nm_brg');
		$data['list_stoksaatini_s_awal']	= $this->lang->line('list_stoksaatini_s_awal');
		$data['list_stoksaatini_s_akhir']	= $this->lang->line('list_stoksaatini_s_akhir');
		$data['list_stoksaatini_bmm']	= $this->lang->line('list_stoksaatini_bmm');
		$data['list_stoksaatini_bmk']	= $this->lang->line('list_stoksaatini_bmk');
		$data['list_stoksaatini_bbm']	= $this->lang->line('list_stoksaatini_bbm');
		$data['list_stoksaatini_bbk']	= $this->lang->line('list_stoksaatini_bbk');
		$data['list_stoksaatini_do']	= $this->lang->line('list_stoksaatini_do');
		$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
		
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lstokopname']= "";
		$data['limages']	= base_url();
		$data['ljnsbrg']	= "";
		
		$ddofirst	= $this->input->post('d_do_first');
		$ddolast	= $this->input->post('d_do_last');
		$sstop		= $this->input->post('f_stop_produksi');
		$iclass		= $this->input->post('i_class');
		
		$stopproduk	= ($sstop==1)?("TRUE"):("FALSE");
		
		$data['tgldomulai']	= ($ddofirst!="")?($ddofirst):"01"."/".date("m/Y");
		$data['tgldoakhir']	= ($ddolast!="")?($ddolast):date("d/m/Y");
		$data['sproduksi']	= $sstop==1?" checked ":"";
		$data['kproduksi']	= $iclass;
		
		$e_d_do_first	= explode("/",$ddofirst,strlen($ddofirst));
		$e_d_do_last	= explode("/",$ddolast,strlen($ddolast)); // dd/mm/YYYY
		
		$nddofirst	= ($ddofirst!='')?($e_d_do_first[2]."-".$e_d_do_first[1]."-".$e_d_do_first[0]):$def_dofirst=date("Y")."-".date("m")."-"."01";
		$nddolast	= ($ddolast!='')?($e_d_do_last[2]."-".$e_d_do_last[1]."-".$e_d_do_last[0]):$def_dolast=date("Y-m-d");
		
		/*
		if(!isset($ddofirst)){
			$def_dofirst	= "01"."/".date("m/Y");
			$def_dolast		= date("d/m/Y");
			
			$nddofirst	= $this->uri->segment(4);
			$nddolast	= $this->uri->segment(5);
			$stopproduk	= $this->uri->segment(6);
			
			$data['sproduksi']	= ($stopproduk=='TRUE')?(" checked "):("");
			$ndopfirst2	= explode("-",$nddofirst,strlen($nddofirst)); // YYYY-mm-dd
			$ndoplast2	= explode("-",$nddolast,strlen($nddolast));
			$data['tgldomulai']	= ($ndopfirst2[2]!='')?($ndopfirst2[2]."/".$ndopfirst2[1]."/".$ndopfirst2[0]):($def_dofirst);
			$data['tgldoakhir']	= ($ndoplast2[2]!='')?($ndoplast2[2]."/".$ndoplast2[1]."/".$ndoplast2[0]):($def_dolast);
		}else{
		}
		*/
		
		$tahun	= substr($nddofirst,0,4); // YYYY-mm-dd
		$bulan	= substr($nddofirst,5,2);
		
		$bln_sekarang	= date("m");
		$thn_sekarang	= date("Y");
		
		if($bln_sekarang==$bulan && $thn_sekarang==$tahun){
			$filter_tgl_stokopname	= " AND d.i_status_so='0' ";
		}else{
			$filter_tgl_stokopname	= " AND SUBSTRING(CAST(d.d_so AS character varying),1,4)='$tahun' AND SUBSTRING(CAST(d.d_so AS character varying),6,2)='$bulan'";
		}
		
		$this->load->model('liststoksaatini/mclass');
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_product_motif, 
				a.i_product, 
				a.e_product_motifname, 
				b.v_unitprice,
				c.n_quantity_awal,
				c.n_quantity_akhir
				
			FROM tr_product_motif a
			
			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so
			
			WHERE b.f_stop_produksi='$stopproduk' AND a.n_active='1' ".$filter_tgl_stokopname."
			
			ORDER BY a.e_product_motifname ASC ");
		
		$jml	= $query->num_rows();

		$pagination['base_url'] 	= '/liststoksaatini/cform/carilistbrgjadinext/'.$nddofirst.'/'.$nddolast.'/'.$stopproduk.'/'.$iclass.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		
		$data['create_link']	= $this->pagination->create_links();
		
		$qbrgjadi		= $this->mclass->lbrgjadi($stopproduk,$pagination['per_page'],$pagination['cur_page'],$filter_tgl_stokopname);
		$lstokopname	= "";
		
		$no	= 1;
		$j	= 0;
		
		$bonmkeluar	= array();
		$bonmmasuk	= array();
		$bbk	= array();
		$bbm	= array();
		$do	= array();
		$sj = array();
		
		/* Deklarasi variabel : Mencari saldo akhir */
		$x11	= array();
		$x22	= array();
		$z11	= array();
		$saldoakhirnya	= array();
		/* End 0f saldo akhir */
		
		if($qbrgjadi->num_rows()>0) {
			$cc	= 1;
			foreach($qbrgjadi->result() as $field) {
				$so_warna = ""; $bonmkeluar_warna=""; $bonmmasuk_warna=""; $do_warna="";
				$saldo_akhir_warna=""; 
				
				// 22-12-2015
				$sqlxx = " SELECT a.*, c.e_color_name FROM tr_product_color a
							INNER JOIN tr_color c ON a.i_color = c.i_color
							WHERE a.i_product_motif = '$field->i_product_motif' ORDER BY c.e_color_name ";
				$queryxx	= $db2->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
									
					foreach ($hasilxx as $rowxx) {			
						$jum_saldo_akhir = 0;				
						/*$detail_warna[] = array(
									'i_color'=> $rowxx->i_color,
									'e_color_name'=> $rowxx->e_color_name
								); */
						
						// ================== 22-12-2015, SO per warna ========================================
						$sqlxx = " SELECT a.*, c.e_color_name FROM tm_stokopname_item_color a
									INNER JOIN tr_color c ON a.i_color = c.i_color
									WHERE a.i_so_item = '$field->i_so_item' AND a.i_color='$rowxx->i_color' ";
						$queryxx	= $db2->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->result();
								
							foreach ($hasilxx as $rowxx) {
								$so_warna.= $rowxx->e_color_name." : ".$rowxx->n_quantity_awal."<br>";
								$jum_saldo_akhir+= $rowxx->n_quantity_awal;
							}
						}
						else {
							$so_warna.= $rowxx->e_color_name." : 0<br>";
						}
				
						// =================================================================================
						
						// ambil total bonm keluar per warna
						$sqlxx2 = " SELECT sum(c.qty) AS jum 
								FROM tm_outbonm a
								INNER JOIN tm_outbonm_item b ON a.i_outbonm=b.i_outbonm 
								INNER JOIN tm_outbonm_item_color c ON b.i_outbonm_item = c.i_outbonm_item
									
								WHERE (a.d_outbonm BETWEEN '$nddofirst' AND '$nddolast') 
								AND b.i_product='$field->i_product_motif' 
								AND a.f_outbonm_cancel='f'
								AND c.i_color = '$rowxx->i_color' ";
						$queryxx2	= $db2->query($sqlxx2);
						if ($queryxx2->num_rows() > 0){
							$hasilxx2 = $queryxx2->row();
							$jumwarna	= $hasilxx2->jum;
							
							if ($jumwarna == '')
								$jumwarna = 0;
							
							$bonmkeluar_warna.= $rowxx->e_color_name." : ".$jumwarna."<br>";
							$jum_saldo_akhir-= $jumwarna;
						}
						else {
							$bonmkeluar_warna.= $rowxx->e_color_name." : 0<br>";
						}
						
						// bonm masuk per warna
						$sqlxx2 = " SELECT sum(c.qty) AS jum 
								FROM tm_inbonm a
								INNER JOIN tm_inbonm_item b ON a.i_inbonm=b.i_inbonm 
								INNER JOIN tm_inbonm_item_color c ON b.i_inbonm_item = c.i_inbonm_item
									
								WHERE (a.d_inbonm BETWEEN '$nddofirst' AND '$nddolast') 
								AND b.i_product='$field->i_product_motif' 
								AND a.f_inbonm_cancel='f'
								AND c.i_color = '$rowxx->i_color' ";
						$queryxx2	= $db2->query($sqlxx2);
						if ($queryxx2->num_rows() > 0){
							$hasilxx2 = $queryxx2->row();
							$jumwarna	= $hasilxx2->jum;
							
							if ($jumwarna == '')
								$jumwarna = 0;
							
							$bonmmasuk_warna.= $rowxx->e_color_name." : ".$jumwarna."<br>";
							$jum_saldo_akhir+= $jumwarna;
						}
						else {
							$bonmmasuk_warna.= $rowxx->e_color_name." : 0<br>";
						}
						
						// do per warna
						$sqlxx2 = " SELECT sum(c.qty) AS jum 
								FROM tm_do a
								INNER JOIN tm_do_item b ON a.i_do=b.i_do 
								INNER JOIN tm_do_item_color c ON b.i_do_item = c.i_do_item
									
								WHERE (a.d_do BETWEEN '$nddofirst' AND '$nddolast') 
								AND b.i_product='$field->i_product_motif' 
								AND a.f_do_cancel='f'
								AND c.i_color = '$rowxx->i_color' ";
						$queryxx2	= $db2->query($sqlxx2);
						if ($queryxx2->num_rows() > 0){
							$hasilxx2 = $queryxx2->row();
							$jumwarna	= $hasilxx2->jum;
							
							if ($jumwarna == '')
								$jumwarna = 0;
							
							$do_warna.= $rowxx->e_color_name." : ".$jumwarna."<br>";
							$jum_saldo_akhir-= $jumwarna;
						}
						else {
							$do_warna.= $rowxx->e_color_name." : 0<br>";
						}
						
						$saldo_akhir_warna.= $rowxx->e_color_name." : ".$jum_saldo_akhir."<br>";
					}
				}
				
				$qbonmkeluar	= $this->mclass->lbonkeluar($nddofirst,$nddolast,$field->i_product_motif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}
				
				$qbonmmasuk	= $this->mclass->lbonmasuk($nddofirst,$nddolast,$field->i_product_motif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk;
				} else {
					$bonmmasuk[$j] = 0;
				}

				$qbbk	= $this->mclass->lbbk($nddofirst,$nddolast,$field->i_product_motif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk;
				} else {
					$bbk[$j] = 0;
				}

				$qbbm	= $this->mclass->lbbm($nddofirst,$nddolast,$field->i_product_motif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm;
				} else {
					$bbm[$j] = 0;
				}

				$qdo	= $this->mclass->ldo($nddofirst,$nddolast,$field->i_product_motif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}
				
				$qsj	= $this->mclass->lsj($nddofirst,$nddolast,$field->i_product_motif);
				if($qsj->num_rows()>0) {
					$row_sj	= $qsj->row();
					$sj[$j]	= $row_sj->jsj; 
				} else {
					$sj[$j] = 0;
				}
				
				$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
				$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
				
				/* Mencari saldo akhir */
				$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
				//$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
				$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j];
				$z11[$j]	= $field->n_quantity_awal+$x11[$j];
				$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
				/* End 0f saldo akhir */
				
				/*
				<td align=\"right\">".$field->n_quantity_akhir."</td>
				*/
				
				$lstokopname	.=	
				"<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\">
				  <td height=\"20px;\" bgcolor=\"$bgcolor\">".$no."</td>
				  <td>".$field->i_product_motif."</td>
				  <td>".$field->e_product_motifname."</td>
				  <td align=\"right\">".$field->n_quantity_awal."</td>
				  <td align='right' style='white-space:nowrap;'>$so_warna<hr></td>
				  <td align=\"right\">".$saldoakhirnya[$j]."</td>
				  <td align='right' style='white-space:nowrap;'>$saldo_akhir_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bonmmasuk[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbonmmasuk('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bon M Masuk\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bon M Masuk\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$bonmmasuk_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bbm[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbbm('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bukti Barang Masuk\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bukti Barang Masuk\"><a/>"."</td>
				  <td align=\"right\" style='white-space:nowrap;'>".$do[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shdo('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Delivery Order (DO)\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Delivery Order (DO)\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$do_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bonmkeluar[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbonmkeluar('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bon M Keluar\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bon M Keluar\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$bonmkeluar_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bbk[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbbk('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bukti Barang Keluar\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bukti Barang Keluar\"><a/>"."</td>
				</tr>";	
				$j++;
				$no++;
				$cc++;
			}
		}		
		$data['query']	= $lstokopname;		
		
		$data['isi']	= 'liststoksaatini/vlistform';
			$this->load->view('template',$data);
	}

	function carilistbrgjadinext() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_stoksaatini']		= $this->lang->line('page_title_stoksaatini');
		$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
		$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
		$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
		$data['list_stoksaatini_kd_brg']	= $this->lang->line('list_stoksaatini_kd_brg');
		$data['list_stoksaatini_nm_brg']	= $this->lang->line('list_stoksaatini_nm_brg');
		$data['list_stoksaatini_s_awal']	= $this->lang->line('list_stoksaatini_s_awal');
		$data['list_stoksaatini_s_akhir']	= $this->lang->line('list_stoksaatini_s_akhir');
		$data['list_stoksaatini_bmm']	= $this->lang->line('list_stoksaatini_bmm');
		$data['list_stoksaatini_bmk']	= $this->lang->line('list_stoksaatini_bmk');
		$data['list_stoksaatini_bbm']	= $this->lang->line('list_stoksaatini_bbm');
		$data['list_stoksaatini_bbk']	= $this->lang->line('list_stoksaatini_bbk');
		$data['list_stoksaatini_do']	= $this->lang->line('list_stoksaatini_do');
		$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
		
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lstokopname']= "";
		$data['limages']	= base_url();
		$data['ljnsbrg']	= "";
		
		$def_dofirst	= "01"."/".date("m/Y");
		$def_dolast		= date("d/m/Y");
			
		$nddofirst	= $this->uri->segment(4);
		$nddolast	= $this->uri->segment(5);
		$stopproduk	= $this->uri->segment(6);
		$iclass		= $this->uri->segment(7);
		
		$data['sproduksi']	= ($stopproduk=='TRUE')?(" checked "):("");
		$ndopfirst2	= explode("-",$nddofirst,strlen($nddofirst)); // YYYY-mm-dd
		$ndoplast2	= explode("-",$nddolast,strlen($nddolast));
		$data['tgldomulai']	= ($ndopfirst2[2]!='')?($ndopfirst2[2]."/".$ndopfirst2[1]."/".$ndopfirst2[0]):($def_dofirst);
		$data['tgldoakhir']	= ($ndoplast2[2]!='')?($ndoplast2[2]."/".$ndoplast2[1]."/".$ndoplast2[0]):($def_dolast);
		$data['kproduksi']	= $iclass;
		
		$tahun	= substr($nddofirst,0,4); // YYYY-mm-dd
		$bulan	= substr($nddofirst,5,2);
		
		$bln_sekarang	= date("m");
		$thn_sekarang	= date("Y");
		
		if($bln_sekarang==$bulan && $thn_sekarang==$tahun){
			$filter_tgl_stokopname	= " AND d.i_status_so='0' ";
		}else{
			$filter_tgl_stokopname	= " AND SUBSTRING(CAST(d.d_so AS character varying),1,4)='$tahun' AND SUBSTRING(CAST(d.d_so AS character varying),6,2)='$bulan'";
		}
		
		$this->load->model('liststoksaatini/mclass');
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		
		$query	= $db2->query(" SELECT a.i_product_motif, 
				a.i_product, 
				a.e_product_motifname, 
				b.v_unitprice,
				c.n_quantity_awal,
				c.n_quantity_akhir
				
			FROM tr_product_motif a
			
			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so
			
			WHERE b.f_stop_produksi='$stopproduk' AND a.n_active='1' ".$filter_tgl_stokopname."
			
			ORDER BY a.e_product_motifname ASC ");
		
		$jml	= $query->num_rows();

		$pagination['base_url'] = '/liststoksaatini/cform/carilistbrgjadinext/'.$nddofirst.'/'.$nddolast.'/'.$stopproduk.'/'.$iclass.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		
		$data['create_link']	= $this->pagination->create_links();
					
		$qbrgjadi	= $this->mclass->lbrgjadi($stopproduk,$pagination['per_page'],$pagination['cur_page'],$filter_tgl_stokopname);
		$lstokopname	= "";
		
		$no	= 1;
		$j	= 0;
		
		$bonmkeluar	= array();
		$bonmmasuk	= array();
		$bbk	= array();
		$bbm	= array();
		$do	= array();
		$sj = array();
		
		$x11	= array();
		$x22	= array();
		$z11	= array();
		$saldoakhirnya	= array();
		
		if($qbrgjadi->num_rows()>0) {
			$cc	= 1;
			foreach($qbrgjadi->result() as $field) {
				
				$so_warna = ""; $bonmkeluar_warna=""; $bonmmasuk_warna=""; $do_warna="";
				$saldo_akhir_warna=""; 
				
				// 22-12-2015
				$sqlxx = " SELECT a.*, c.e_color_name FROM tr_product_color a
							INNER JOIN tr_color c ON a.i_color = c.i_color
							WHERE a.i_product_motif = '$field->i_product_motif' ORDER BY c.e_color_name ";
				$queryxx	= $db2->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
									
					foreach ($hasilxx as $rowxx) {			
						$jum_saldo_akhir = 0;				
						/*$detail_warna[] = array(
									'i_color'=> $rowxx->i_color,
									'e_color_name'=> $rowxx->e_color_name
								); */
						
						// ================== 22-12-2015, SO per warna ========================================
						$sqlxx = " SELECT a.*, c.e_color_name FROM tm_stokopname_item_color a
									INNER JOIN tr_color c ON a.i_color = c.i_color
									WHERE a.i_so_item = '$field->i_so_item' AND a.i_color='$rowxx->i_color' ";
						$queryxx	= $db2->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->result();
								
							foreach ($hasilxx as $rowxx) {
								$so_warna.= $rowxx->e_color_name." : ".$rowxx->n_quantity_awal."<br>";
								$jum_saldo_akhir+= $rowxx->n_quantity_awal;
							}
						}
						else {
							$so_warna.= $rowxx->e_color_name." : 0<br>";
						}
				
						// =================================================================================
						
						// ambil total bonm keluar per warna
						$sqlxx2 = " SELECT sum(c.qty) AS jum 
								FROM tm_outbonm a
								INNER JOIN tm_outbonm_item b ON a.i_outbonm=b.i_outbonm 
								INNER JOIN tm_outbonm_item_color c ON b.i_outbonm_item = c.i_outbonm_item
									
								WHERE (a.d_outbonm BETWEEN '$nddofirst' AND '$nddolast') 
								AND b.i_product='$field->i_product_motif' 
								AND a.f_outbonm_cancel='f'
								AND c.i_color = '$rowxx->i_color' ";
						$queryxx2	= $db2->query($sqlxx2);
						if ($queryxx2->num_rows() > 0){
							$hasilxx2 = $queryxx2->row();
							$jumwarna	= $hasilxx2->jum;
							
							if ($jumwarna == '')
								$jumwarna = 0;
							
							$bonmkeluar_warna.= $rowxx->e_color_name." : ".$jumwarna."<br>";
							$jum_saldo_akhir-= $jumwarna;
						}
						else {
							$bonmkeluar_warna.= $rowxx->e_color_name." : 0<br>";
						}
						
						// bonm masuk per warna
						$sqlxx2 = " SELECT sum(c.qty) AS jum 
								FROM tm_inbonm a
								INNER JOIN tm_inbonm_item b ON a.i_inbonm=b.i_inbonm 
								INNER JOIN tm_inbonm_item_color c ON b.i_inbonm_item = c.i_inbonm_item
									
								WHERE (a.d_inbonm BETWEEN '$nddofirst' AND '$nddolast') 
								AND b.i_product='$field->i_product_motif' 
								AND a.f_inbonm_cancel='f'
								AND c.i_color = '$rowxx->i_color' ";
						$queryxx2	= $db2->query($sqlxx2);
						if ($queryxx2->num_rows() > 0){
							$hasilxx2 = $queryxx2->row();
							$jumwarna	= $hasilxx2->jum;
							
							if ($jumwarna == '')
								$jumwarna = 0;
							
							$bonmmasuk_warna.= $rowxx->e_color_name." : ".$jumwarna."<br>";
							$jum_saldo_akhir+= $jumwarna;
						}
						else {
							$bonmmasuk_warna.= $rowxx->e_color_name." : 0<br>";
						}
						
						// do per warna
						$sqlxx2 = " SELECT sum(c.qty) AS jum 
								FROM tm_do a
								INNER JOIN tm_do_item b ON a.i_do=b.i_do 
								INNER JOIN tm_do_item_color c ON b.i_do_item = c.i_do_item
									
								WHERE (a.d_do BETWEEN '$nddofirst' AND '$nddolast') 
								AND b.i_product='$field->i_product_motif' 
								AND a.f_do_cancel='f'
								AND c.i_color = '$rowxx->i_color' ";
						$queryxx2	= $db2->query($sqlxx2);
						if ($queryxx2->num_rows() > 0){
							$hasilxx2 = $queryxx2->row();
							$jumwarna	= $hasilxx2->jum;
							
							if ($jumwarna == '')
								$jumwarna = 0;
							
							$do_warna.= $rowxx->e_color_name." : ".$jumwarna."<br>";
							$jum_saldo_akhir-= $jumwarna;
						}
						else {
							$do_warna.= $rowxx->e_color_name." : 0<br>";
						}
						
						$saldo_akhir_warna.= $rowxx->e_color_name." : ".$jum_saldo_akhir."<br>";
					}
				}
				
				$qbonmkeluar	= $this->mclass->lbonkeluar($nddofirst,$nddolast,$field->i_product_motif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}

				$qbonmmasuk	= $this->mclass->lbonmasuk($nddofirst,$nddolast,$field->i_product_motif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk;
				} else {
					$bonmmasuk[$j] = 0;
				}

				$qbbk	= $this->mclass->lbbk($nddofirst,$nddolast,$field->i_product_motif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk;
				} else {
					$bbk[$j] = 0;
				}

				$qbbm	= $this->mclass->lbbm($nddofirst,$nddolast,$field->i_product_motif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm;
				} else {
					$bbm[$j] = 0;
				}

				$qdo	= $this->mclass->ldo($nddofirst,$nddolast,$field->i_product_motif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}
				
				$qsj	= $this->mclass->lsj($nddofirst,$nddolast,$field->i_product_motif);
				if($qsj->num_rows()>0) {
					$row_sj	= $qsj->row();
					$sj[$j]	= $row_sj->jsj; 
				} else {
					$sj[$j] = 0;
				}
				
				$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
				$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
				
				$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
				$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
				$z11[$j]	= $field->n_quantity_awal+$x11[$j];
				$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
				
				/*
				 * <td align='right' style='white-space:nowrap;'>$so_warna<hr></td>
				  <td align=\"right\">".$saldoakhirnya[$j]."</td>
				  <td align='right' style='white-space:nowrap;'>$saldo_akhir_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bonmmasuk[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbonmmasuk('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bon M Masuk\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bon M Masuk\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$bonmmasuk_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bbm[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbbm('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bukti Barang Masuk\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bukti Barang Masuk\"><a/>"."</td>
				  <td align=\"right\" style='white-space:nowrap;'>".$do[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shdo('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Delivery Order (DO)\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Delivery Order (DO)\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$do_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bonmkeluar[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbonmkeluar('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bon M Keluar\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bon M Keluar\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$bonmkeluar_warna<hr></td>
				 */
				
				// old 22-12-2015							
				/*$lstokopname	.=	
				"<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\">
				  <td height=\"20px;\" bgcolor=\"$bgcolor\">".$no."</td>
				  <td>".$field->i_product_motif."</td>
				  <td>".$field->e_product_motifname."</td>
				  <td align=\"right\">".$field->n_quantity_awal."</td>
				  <td align=\"right\">".$saldoakhirnya[$j]."</td>
				  <td align=\"right\">".$bonmmasuk[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbonmmasuk('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bon M Masuk\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bon M Masuk\"><a/>"."</td>
				  <td align=\"right\">".$bbm[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbbm('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bukti Barang Masuk\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bukti Barang Masuk\"><a/>"."</td>
				  <td align=\"right\">".$do[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shdo('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Delivery Order (DO)\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Delivery Order (DO)\"><a/>"."</td>
				  <td align=\"right\">".$bonmkeluar[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbonmkeluar('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bon M Keluar\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bon M Keluar\"><a/>"."</td>
				  <td align=\"right\">".$bbk[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbbk('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bukti Barang Keluar\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bukti Barang Keluar\"><a/>"."</td>
				</tr>";	*/
				
				// new 22-12-2015
				$lstokopname	.=	
				"<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\">
				  <td height=\"20px;\" bgcolor=\"$bgcolor\">".$no."</td>
				  <td>".$field->i_product_motif."</td>
				  <td>".$field->e_product_motifname."</td>
				  <td align=\"right\">".$field->n_quantity_awal."</td>
				  <td align='right' style='white-space:nowrap;'>$so_warna<hr></td>
				  <td align=\"right\">".$saldoakhirnya[$j]."</td>
				  <td align='right' style='white-space:nowrap;'>$saldo_akhir_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bonmmasuk[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbonmmasuk('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bon M Masuk\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bon M Masuk\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$bonmmasuk_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bbm[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbbm('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bukti Barang Masuk\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bukti Barang Masuk\"><a/>"."</td>
				  <td align=\"right\" style='white-space:nowrap;'>".$do[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shdo('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Delivery Order (DO)\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Delivery Order (DO)\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$do_warna<hr></td>
				  <td align=\"right\" style='white-space:nowrap;'>".$bonmkeluar[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbonmkeluar('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bon M Keluar\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bon M Keluar\"><a/>"."</td>
				  <td align='right' style='white-space:nowrap;'>$bonmkeluar_warna<hr></td>
				  <td align=\"right\">".$bbk[$j]."&nbsp;&nbsp;<a href=\"#\" onclick=\"shbbk('$nddofirst','$nddolast','$field->i_product_motif');\" alt=\"Lihat Bukti Barang Keluar\"><img src=\"".base_url()."asset/images/theme/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"Lihat Bukti Barang Keluar\"><a/>"."</td>
				</tr>";
				$j++;
				$no++;
				$cc++;
			}
		}		
		$data['isi']	= $lstokopname;		
		
		$this->load->view('liststoksaatini/vlistform',$data);
	}
		
	function lbonmmasuk() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI BON M";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
		
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupbonmmasuk($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/lbonmmasuknext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupbonmmasukperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformbonmmasuk',$data);
	}

	function lbonmmasuknext() {
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI BON M";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
				
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupbonmmasuk($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/lbonmmasuknext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupbonmmasukperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformbonmmasuk',$data);
	}	
	
	function flbonmmasuk() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$d		= $this->input->post('d')?$this->input->post('d'):$this->input->get_post('d');
		$d2		= $this->input->post('d2')?$this->input->post('d2'):$this->input->get_post('d2');
		$code	= $this->input->post('code')?$this->input->post('code'):$this->input->get_post('code');
		
		$key_upper	= strtoupper($code);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI BON M";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('liststoksaatini/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->fbonmmasuk($key_upper,$d,$d2);
			//$query	= $this->mclass->fbonmmasuk($code);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			$tanggal	= array();			
			$cc	= 1;
			
			foreach($query->result() as $row){
					$tgl	= (!empty($row->dinbonm) || strlen($row->dinbonm)!=0)?@explode("-",$row->dinbonm,strlen($row->dinbonm)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
										
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td>".$row->ibonmcode."</td>
					  <td>".$row->iproduct."</td>
					  <td>".$row->eproductname."</td>
					  <td>".$row->jbonmasuk."</td> 
					  <td>".$tanggal[$cc]."</td>
					 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;			
	}

	function lbonmkeluar() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI BON M";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
		
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupbonmkeluar($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/lbonmkeluarnext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupbonmkeluarperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformbonmkeluar',$data);
	}

	function lbonmkeluarnext() {
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI BON M";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
				
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupbonmkeluar($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/lbonmkeluarnext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupbonmkeluarperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformbonmkeluar',$data);
	}	
	
	function flbonmkeluar() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$d		= $this->input->post('d')?$this->input->post('d'):$this->input->get_post('d');
		$d2		= $this->input->post('d2')?$this->input->post('d2'):$this->input->get_post('d2');
		$code	= $this->input->post('code')?$this->input->post('code'):$this->input->get_post('code');
		
		$key_upper	= strtoupper($code);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI BON M";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('liststoksaatini/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->fbonmkeluar($key_upper,$d,$d2);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			$tanggal	= array();			
			$cc	= 1;
			
			foreach($query->result() as $row){
					$tgl	= (!empty($row->doutbonm) || strlen($row->doutbonm)!=0)?@explode("-",$row->doutbonm,strlen($row->doutbonm)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
										
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td>".$row->ioutbonmcode."</td>
					  <td>".$row->iproduct."</td>
					  <td>".$row->eproductname."</td>
					  <td>".$row->jbonkeluar."</td> 
					  <td>".$tanggal[$cc]."</td>
					 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;			
	}

	function ldo() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI DO";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
		
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupdo($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/ldonext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupdoperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformdo',$data);
	}

	function ldonext() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI DO";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
		
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupdo($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/ldonext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupdoperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformdo',$data);
	}

	function fldo() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$d		= $this->input->post('d')?$this->input->post('d'):$this->input->get_post('d');
		$d2		= $this->input->post('d2')?$this->input->post('d2'):$this->input->get_post('d2');
		$code	= $this->input->post('code')?$this->input->post('code'):$this->input->get_post('code');
		
		$key_upper	= strtoupper($code);

		$data['page_title']	= "DAFTAR ITEM BRG YG DI DO";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('liststoksaatini/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->fdo($key_upper,$d,$d2);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			$tanggal	= array();			
			$cc	= 1;
			
			foreach($query->result() as $row){
					$tgl	= (!empty($row->ddo) || strlen($row->ddo)!=0)?@explode("-",$row->ddo,strlen($row->ddo)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
					
					$list .= "
					<tr>
					  <td>".$cc."</td>
					  <td>".$row->idocode."</td>
					  <td>".$row->iproduct."</td>
					  <td>".$row->eproductname."</td>
					  <td align=\"right\" >".$row->jdo."</td> 
					  <td align=\"right\" >".$tanggal[$cc]."</td>
					 </tr>					
					";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;			
	}
	
	function lbbm() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
		
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupbbm($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/lbbmnext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupbbmperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformbbm',$data);
	}

	function lbbmnext() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
		
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupbbm($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/lbbmnext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupbbmperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformbbm',$data);
	}

	function flbbm() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$d		= $this->input->post('d')?$this->input->post('d'):$this->input->get_post('d');
		$d2		= $this->input->post('d2')?$this->input->post('d2'):$this->input->get_post('d2');
		$code	= $this->input->post('code')?$this->input->post('code'):$this->input->get_post('code');
		
		$key_upper	= strtoupper($code);

		$data['page_title']	= "DAFTAR ITEM BRG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('liststoksaatini/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->fbbm($key_upper,$d,$d2);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			$tanggal	= array();			
			$cc	= 1;
			
			foreach($query->result() as $row){
					$tgl	= (!empty($row->dbbm) || strlen($row->dbbm)!=0)?@explode("-",$row->dbbm,strlen($row->dbbm)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
					
					$list .= "
					<tr>
					  <td>".$cc."</td>
					  <td>".$row->ibbm."</td>
					  <td>".$row->iproduct."</td>
					  <td>".$row->eproductname."</td>
					  <td align=\"right\" >".$row->jbbm."</td> 
					  <td align=\"right\" >".$tanggal[$cc]."</td>
					 </tr>										
					";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;			
	}
	
//
	function lbbk() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
		
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupbbk($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/lbbknext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['isi']	= $this->mclass->popupbbkperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformbbk',$data);
	}

	function lbbknext() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$d		= $this->uri->segment(4,0);
		$d2		= $this->uri->segment(5,0);
		$code	= $this->uri->segment(6,0);

		$data['page_title']	= "DAFTAR ITEM BRG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$data['d']		= $d;
		$data['d2']		= $d2;
		$data['code']	= $code;
		
		$this->load->model('liststoksaatini/mclass');
		
		$query	= $this->mclass->popupbbk($d,$d2,$code);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/liststoksaatini/cform/lbbknext/'.$d.'/'.$d2.'/'.$code.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->popupbbkperpages($d,$d2,$code,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('liststoksaatini/vlistformbbk',$data);
	}

	function flbbk() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$d		= $this->input->post('d')?$this->input->post('d'):$this->input->get_post('d');
		$d2		= $this->input->post('d2')?$this->input->post('d2'):$this->input->get_post('d2');
		$code	= $this->input->post('code')?$this->input->post('code'):$this->input->get_post('code');
		
		$key_upper	= strtoupper($code);

		$data['page_title']	= "DAFTAR ITEM BRG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('liststoksaatini/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->fbbk($key_upper,$d,$d2);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			$tanggal	= array();			
			$cc	= 1;
			
			foreach($query->result() as $row){
					$tgl	= (!empty($row->dbbk) || strlen($row->dbbk)!=0)?@explode("-",$row->dbbk,strlen($row->dbbk)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
					
					$list .= "
					<tr>
					  <td>".$cc."</td>
					  <td>".$row->ibbk."</td>
					  <td>".$row->iproduct."</td>
					  <td>".$row->eproductname."</td>
					  <td align=\"right\" >".$row->jbbk."</td> 
					  <td align=\"right\" >".$tanggal[$cc]."</td>
					 </tr>
					";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;			
	}					
}
?>
