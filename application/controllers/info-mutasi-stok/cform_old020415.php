<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-mutasi-stok/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-mutasi-stok/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vformview';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }
  
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='9' align='center'>LAPORAN MUTASI STOK BAHAN BAKU/PEMBANTU</th>
		 </tr>
		 <tr>
			<th colspan='9' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='9' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%'>No</th>
			 <th width='20%'>Kode Brg</th>
			 <th width='30%'>Nama Barang</th>
			 <th width='15%'>Sat Awal</th>
			 <th width='7%'>Saldo Awal</th>
			 <th width='7%'>Masuk</th>
			 <th width='7%'>Masuk Lain</th>
			 <th width='7%'>Keluar</th>
			 <th width='7%'>Keluar Lain</th>
			 <th width='7%'>Saldo Akhir</th>
			 <th width='7%'>SO</th>
			 <th width='7%'>Selisih</th>
		 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td>".$query[$j]['satuan']."</td>
				 <td align='right'>".$query[$j]['saldo_awal']."</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain']."</td>
				 <td align='right'>".$query[$j]['jum_keluar']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain']."</td> ";
				$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				$html_data.= "<td align='right'>".$saldo_akhir."</td>";
				$html_data.= "<td align='right'>".$query[$j]['jum_stok_opname']."</td> ";
				$selisih = abs($query[$j]['jum_stok_opname']-$saldo_akhir);
				$html_data.= "<td align='right'>".$selisih."</td> ";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		//include("../tiraiproduksi/system/application/libraries/generateExcelFile.php"); 
		include("../dutaproduksi200/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // ++++++++++++++++++ 14-03-2013, mutasi brg WIP ++++++++++++++++++++++++++++
  function wip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-mutasi-stok/vmainformwip';
	$this->load->view('template',$data);

  }
  
  function viewwip(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vformviewwip';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }
  
  function export_excel_wip() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='16' align='center'>LAPORAN MUTASI STOK BARANG WIP</th>
		 </tr>
		 <tr>
			<th colspan='16' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='16' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%' rowspan='2'>No</th>
			 <th width='15%' rowspan='2'>Kode Brg</th>
			 <th width='25%' rowspan='2'>Nama Brg</th>
			 <th width='8%' rowspan='2'>Satuan</th>
			 <th width='8%' rowspan='2'>S.Awal</th>
			 <th width='8%' rowspan='2'>Masuk</th>
			 <th colspan='4'>Masuk Lain</th>
			 <th colspan='2'>Keluar</th>
			 <th>Keluar Lain</th>
			 <th width='8%' rowspan='2'>Saldo Akhir</th>
			 <th width='8%' rowspan='2'>Stok Opname</th>
			 <th width='8%' rowspan='2'>Selisih</th>
		 </tr>
		 <tr>
			 <th width='8%'>Perbaikan Unit Jht</th>
			 <th width='8%'>Retur dr Packing</th>
			 <th width='8%'>Retur dr Gd Jadi</th>
			 <th width='8%'>Lainnya</th>
			 <th width='8%'>Packing</th>
			 <th width='8%'>Gd Jadi</th>
			 <th width='8%'>Retur ke Unit Jht</th>
		 </tr>
		</thead>
		<tbody>";
		// 12-09-2014, kolom setelah retur ke unit jahit dihilangkan
		// <th width='8%'>Lainnya</th>
		
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td>Pieces</td>
				 <td align='right'>".$query[$j]['saldo_awal']."</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain1']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain2']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain3']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain4']."</td>
				 <td align='right'>".$query[$j]['jum_keluar1']."</td>
				 <td align='right'>".$query[$j]['jum_keluar2']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain1']."</td>";
				//$saldo_akhir = $query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain1']+$query[$j]['jum_masuk_lain2']+$query[$j]['jum_masuk_lain3']+$query[$j]['jum_masuk_lain4']-$query[$j]['jum_keluar1']-$query[$j]['jum_keluar2']-$query[$j]['jum_keluar_lain1']-$query[$j]['jum_keluar_lain2'];
				$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain1']+$query[$j]['jum_masuk_lain2']+$query[$j]['jum_masuk_lain3']+$query[$j]['jum_masuk_lain4']-$query[$j]['jum_keluar1']-$query[$j]['jum_keluar2']-$query[$j]['jum_keluar_lain1']-$query[$j]['jum_keluar_lain2'];
				$selisih = $query[$j]['jum_stok_opname']-$saldo_akhir;
				$html_data.= "<td align='right'>".$saldo_akhir."</td>
				<td align='right'>".$query[$j]['jum_stok_opname']."</td>
				<td align='right'>".$selisih."</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok_wip";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		//include("../tiraiproduksi/system/application/libraries/generateExcelFile.php"); 
		include("../dutaproduksi200/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  //============= 26-09-2013, mutasi brg HASIL CUTTING ===============================
  function hasilcutting(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'info-mutasi-stok/vmainformhasilcutting';
	$this->load->view('template',$data);

  }
  
  function viewhasilcutting(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vformviewhasilcutting';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok_hasilcutting($date_from, $date_to);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	/*$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi; */
	$this->load->view('template',$data);
  }
  
  function export_excel_hasilcutting() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_hasilcutting($date_from, $date_to);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='11' align='center'>LAPORAN MUTASI STOK BARANG HASIL CUTTING</th>
		 </tr>
		 <tr>
			<th colspan='11' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
		 <th width='3%' rowspan='2'>No</th>
		 <th width='30%' rowspan='2'>Barang Jadi</th>
		 <th width='30%' rowspan='2'>Nama Bahan Baku</th>
		 <th width='8%' rowspan='2'>S.Awal</th>
		 <th colspan='3'>Masuk</th>
		 <th colspan='2'>Keluar</th>
		 <th width='8%' rowspan='2'>Saldo Akhir</th>
		 <th width='8%' rowspan='2'>Stok Opname</th>
	 </tr>
	 <tr>
			 <th width='8%'>Masuk Bagus</th>
			 <th width='8%'>Perbaikan</th>
			 <th width='8%'>Lain-Lain</th>
			 <th width='8%'>Keluar Bagus</th>
			 <th width='8%'>Lain-Lain</th>
	 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td align='right'>".$query[$j]['saldo_awal']."</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain1']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain2']."</td>
				 <td align='right'>".$query[$j]['jum_keluar']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain']."</td>";
				$saldo_akhir = $query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain1']+$query[$j]['jum_masuk_lain2']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				$html_data.= "<td align='right'>".$saldo_akhir."</td>
				<td align='right'>".$query[$j]['jum_stok_opname']."</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok_hasil_cutting";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		//include("../tiraiproduksi/system/application/libraries/generateExcelFile.php"); 
		include("../dutaproduksi200/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // 04-06-2014
  function viewsobhnbaku(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
    $data['isi'] = 'info-mutasi-stok/vformviewsobhnbaku';
	$id_gudang 	= $this->input->post('gudang', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);  
	
	if ($id_gudang == '' && $bulan=='' && $tahun=='') {
		$id_gudang 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	if ($id_gudang == '')
		$id_gudang = '0';

    $jum_total = $this->mmaster->get_sobhnbakutanpalimit($id_gudang, $bulan, $tahun);

			$config['base_url'] = base_url().'index.php/info-mutasi-stok/cform/viewsobhnbaku/'.$id_gudang.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mmaster->get_sobhnbaku($config['per_page'],$this->uri->segment(7), $id_gudang, $bulan, $tahun);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['cid_gudang'] = $id_gudang;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  function editsobhnbaku(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cid_gudang 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cid_gudang'] = $cid_gudang;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mmaster->get_detail_sobhnbaku($id_so); 
	$data['jum_total'] = count($data['query']);
	$data['isi'] = 'info-mutasi-stok/veditsobhnbaku';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  function updatesobhnbaku() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cid_gudang = $this->input->post('cid_gudang', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  //$tgl = date("Y-m-d H:i:s"); 
	  $tgl = date("Y-m-d"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET tgl_update = '$tgl', status_approve='f' where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mmaster->savesobhnbaku($id_so, 
			 $this->input->post('kode_brg_'.$i, TRUE), 
			 $this->input->post('stok_fisik_'.$i, TRUE));
		 }
			  
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "info-mutasi-stok/cform/viewsobhnbaku/".$cid_gudang."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }

}
