<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}		
			$data['page_title_op']	= $this->lang->line('page_title_op');
			$data['form_title_detail_op']	= $this->lang->line('form_title_detail_op');
			$data['list_tgl_mulai_op']	= $this->lang->line('list_tgl_mulai_op');
			$data['list_tgl_akhir_op']	= $this->lang->line('list_tgl_akhir_op');
			$data['list_no_op']	= $this->lang->line('list_no_op');
			$data['list_nm_cab_op']	= $this->lang->line('list_nm_cab_op');
			$data['list_kd_brg_op']	= $this->lang->line('list_kd_brg_op');
			$data['list_nm_brg_op']	= $this->lang->line('list_nm_brg_op');
			$data['list_qty_op']	= $this->lang->line('list_qty_op');
			$data['list_sisa_order_op']	= $this->lang->line('list_sisa_order_op');
			$data['list_limit_date_op']	= $this->lang->line('list_limit_date_op');
			$data['list_no_sop_op']	= $this->lang->line('list_no_sop_op');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');		
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$this->load->model('listop/mclass');
			
			$data['customer']	= $this->mclass->lcustomer();
			
			$data['isi']	= 'listop/vmainform';	
			$this->load->view('template',$data);
						
	}
	
	function carilistop() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$data['page_title_op']	= $this->lang->line('page_title_op');
		$data['form_title_detail_op']	= $this->lang->line('form_title_detail_op');
		$data['list_tgl_mulai_op']	= $this->lang->line('list_tgl_mulai_op');
		$data['list_tgl_akhir_op']	= $this->lang->line('list_tgl_akhir_op');
		$data['list_no_op']	= $this->lang->line('list_no_op');
		$data['list_nm_cab_op']	= $this->lang->line('list_nm_cab_op');
		$data['list_kd_brg_op']	= $this->lang->line('list_kd_brg_op');
		$data['list_nm_brg_op']	= $this->lang->line('list_nm_brg_op');
		$data['list_qty_op']	= $this->lang->line('list_qty_op');
		$data['list_sisa_order_op']	= $this->lang->line('list_sisa_order_op');
		$data['list_limit_date_op']	= $this->lang->line('list_limit_date_op');
		$data['list_no_sop_op']	= $this->lang->line('list_no_sop_op');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopbrg']	= "";

		$d_op_first	= $this->input->post('d_op_first');
		$d_op_last	= $this->input->post('d_op_last');
		$kdop		= $this->input->post('nomor_op');
		$icustomer	= $this->input->post('customer');
		
		if($d_op_first!='' || $d_op_last!='') {
		
			$e_d_op_first	= explode("/",$d_op_first,strlen($d_op_first)); // dd/mm/YYYY
			$e_d_op_last	= explode("/",$d_op_last,strlen($d_op_last)); // dd/mm/YYYY		
			$n_d_op_first	= (!empty($e_d_op_first[2]) && strlen($e_d_op_first[2])==4)?$e_d_op_first[2].'-'.$e_d_op_first[1].'-'.$e_d_op_first[0]:'';
			$n_d_op_last	= (!empty($e_d_op_last[2]) && strlen($e_d_op_last[2])==4)?$e_d_op_last[2].'-'.$e_d_op_last[1].'-'.$e_d_op_last[0]:'';
			
			$data['tglopmulai']	= $d_op_first;
			$data['tglopakhir']	= $d_op_last;
			$data['nomorop']	= '';
			
		}else{
		
			$tawal	= $this->uri->segment(4);
			$takhir	= $this->uri->segment(5);
			$nomer	= $this->uri->segment(6);
			
			if(($tawal!='empty' || $takhir!='empty') && ($tawal!='' || $takhir!='')){
				$n_d_op_first	= $tawal;
				$n_d_op_last	= $takhir;
				$e_d_op_first	= explode("-",$n_d_op_first,strlen($n_d_op_first)); // YYYY-mm-dd
				$e_d_op_last	= explode("-",$n_d_op_last,strlen($n_d_op_last)); // YYYY-mm-dd
				$data['tglopmulai']	= $e_d_op_first[2].'/'.$e_d_op_first[1].'/'.$e_d_op_first[0];
				$data['tglopakhir']	= $e_d_op_last[2].'/'.$e_d_op_last[1].'/'.$e_d_op_last[0];
				$data['nomorop']	= '';
			}else{
				$n_d_op_first	= '';
				$n_d_op_last	= '';			
				$data['tglopmulai']	= '';
				$data['tglopakhir']	= '';
				$data['nomorop']	= $kdop;
			}
		}
		
		$icustomer	= !empty($icustomer)?$icustomer:$this->uri->segment(7);
		
		$turi1	= ($n_d_op_first!='' && $n_d_op_first!='empty')?$n_d_op_first:'empty';
		$turi2	= ($n_d_op_last!='' && $n_d_op_last!='empty')?$n_d_op_last:'empty';
		$turi3	= $kdop==''?'empty':$kdop;
		$turi4	= $icustomer;
					
		$this->load->model('listop/mclass');
		
		$flag	= 	$turi1=='empty'?'f':'t';
		$data['flag']	= $flag;
		
		if($flag=='t') {
			
			$query	= $this->mclass->clistopbrg_t($n_d_op_first,$n_d_op_last,$kdop,$icustomer);
			$jml	= $query->num_rows();		
			
			$pagination['base_url'] 	= '/listop/cform/carilistopperpages/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(8,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			$data['isi_t']	= $this->mclass->viewperpages_t($pagination['per_page'],$pagination['cur_page'],$n_d_op_first,$n_d_op_last,$kdop,$icustomer);
		}elseif($flag=='f'){			
			$data['isi_f']	= $this->mclass->clistopbrg_f($n_d_op_first,$n_d_op_last,$kdop,$icustomer);
		}
		
		$data['icustomer']	= $icustomer;
				
		$data['isi']	= 'listop/vlistform';	
			$this->load->view('template',$data);		
		
	}

	function carilistopperpages() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$data['page_title_op']	= $this->lang->line('page_title_op');
		$data['form_title_detail_op']	= $this->lang->line('form_title_detail_op');
		$data['list_tgl_mulai_op']	= $this->lang->line('list_tgl_mulai_op');
		$data['list_tgl_akhir_op']	= $this->lang->line('list_tgl_akhir_op');
		$data['list_no_op']	= $this->lang->line('list_no_op');
		$data['list_nm_cab_op']	= $this->lang->line('list_nm_cab_op');
		$data['list_kd_brg_op']	= $this->lang->line('list_kd_brg_op');
		$data['list_nm_brg_op']	= $this->lang->line('list_nm_brg_op');
		$data['list_qty_op']	= $this->lang->line('list_qty_op');
		$data['list_sisa_order_op']	= $this->lang->line('list_sisa_order_op');
		$data['list_limit_date_op']	= $this->lang->line('list_limit_date_op');
		$data['list_no_sop_op']	= $this->lang->line('list_no_sop_op');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopbrg']	= "";
		
		$tawal	= $this->uri->segment(4);
		$takhir	= $this->uri->segment(5);
		$nomer	= $this->uri->segment(6);
		$icustomer	= $this->uri->segment(7);
		
		$e_d_op_first	= explode("-",$tawal,strlen($tawal)); // YYYY-mm-dd
		$e_d_op_last	= explode("-",$takhir,strlen($takhir)); // YYYY-mm-dd
		$data['tglopmulai']	= $e_d_op_first[2].'/'.$e_d_op_first[1].'/'.$e_d_op_first[0];
		$data['tglopakhir']	= $e_d_op_last[2].'/'.$e_d_op_last[1].'/'.$e_d_op_last[0];
		$data['nomorop']	= '';
					
		$this->load->model('listop/mclass');
		
		$data['flag']	= 't';
		
		$query	= $this->mclass->clistopbrg_t($tawal,$takhir,"empty",$icustomer);
		$jml	= $query->num_rows();		

		$pagination['base_url'] 	= '/listop/cform/carilistopperpages/'.$tawal.'/'.$takhir.'/empty/'.$icustomer.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();		
		$data['isi_t']	= $this->mclass->viewperpages_t($pagination['per_page'],$pagination['cur_page'],$tawal,$takhir,"empty",$icustomer);
		
		$data['icustomer']	= $icustomer;
				
		$this->load->view('listop/vlistform',$data);
	}
		
	function listbarangjadi() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listop/mclass');

		$query	= $this->mclass->lop();
		$jml	= $query->num_rows();
		//$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listop/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lopperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listop/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listop/mclass');

		$query	= $this->mclass->lop();
		$jml	= $query->num_rows();
		//$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listop/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lopperpages($pagination['per_page'],$pagination['cur_page']);

		$this->load->view('listop/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listop/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->flop($key);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 			
			foreach($query->result() as $row){
				$list .= "
				 <tr>
				  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->op')\">".$row->op."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->op')\">".$row->iproduct."</a></td>
					  <td><a href=\"javascript:settextfield('$row->op')\">".$row->productname."</a></td>
					  <td><a href=\"javascript:settextfield('$row->op')\">".$row->qty."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	
	function undo() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$iopcode	= $this->uri->segment(4);
		$icustomer	= $this->uri->segment(5);
		
		$this->load->model('listop/mclass');
		
		$this->mclass->mbatal($iopcode,$icustomer);
	}
	
	function edit() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$iopcode	= $this->uri->segment(4);
		$icustomer	= $this->uri->segment(5);
		
		$data['form_nomor_op']	= $this->lang->line('form_nomor_op');
		$data['form_tgl_op']	= $this->lang->line('form_tgl_op');	
		$data['form_cabang_op']	= $this->lang->line('form_cabang_op');
		$data['form_option_pel_op']	= $this->lang->line('form_option_pel_op');
		$data['form_option_cab_op']	= $this->lang->line('form_option_cab_op');
		$data['form_tgl_bts_kirim_op']	= $this->lang->line('form_tgl_bts_kirim_op');
		$data['form_nomor_sop_op']	= $this->lang->line('form_nomor_sop_op');
		$data['form_title_detail_op']	= $this->lang->line('form_title_detail_op');
		$data['form_jml_op']	= $this->lang->line('form_jml_op');
		$data['form_kode_produk_op']	= $this->lang->line('form_kode_produk_op');
		$data['form_nm_produk_op']	= $this->lang->line('form_nm_produk_op');
		$data['form_ket_op']	= $this->lang->line('form_ket_op');
		$data['button_update']	= $this->lang->line('button_update');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['page_title']	= $this->lang->line('page_title_op');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lpelanggan']	= "";
		$data['lcabang']	= "";
		$data['selected_cab']	= "";
		
		$this->load->model('listop/mclass');

		$qryopheader	= $this->mclass->getopheader($iopcode,$icustomer);
		
		if($qryopheader->num_rows()>0) {
			$row_opheader	= $qryopheader->row();
			$iop			= $row_opheader->i_op;
			$data['iop']	= $iop;
			$data['no']		= $row_opheader->i_op_code;
			$data['sop']	= $row_opheader->i_sop;
			$data['f_op_dropforcast']	= $row_opheader->f_op_dropforcast;
			
			//13-04-2015
			$data['jenis_op']	= $row_opheader->jenis_op;
			
			$tglop	= explode("-",$row_opheader->d_op); // YYYY-mm-dd
			$tglbtskirim	= explode("-",$row_opheader->d_delivery_limit);
			$data['tgOP']	= $tglop[2]."/".$tglop[1]."/".$tglop[0];			
			$data['tBtsKirim']	= $tglbtskirim[2]."/".$tglbtskirim[1]."/".$tglbtskirim[0];
			$icustomer	= $row_opheader->i_customer;
			$ibranch	= $row_opheader->i_branch;
			$qcustomer	= $this->mclass->getpelanggan($icustomer);
			$qcabang	= $this->mclass->getcabang($ibranch);
			$data['opitem']	= $this->mclass->lopitem($iop);
			$data['opt_cabang']	= $this->mclass->lcabang($icustomer);
			
			if($qcustomer->num_rows()>0) {
				$row_customer	= $qcustomer->row();
				$icust	= $row_customer->i_customer;
				$data['icust']	= $icust;
			} else {
				$data['icust']	= "";
			}
			
			if($qcabang->num_rows()>0) {
				$row_cabang	= $qcabang->row();
				$ibranch	= $row_cabang->i_branch;
				$data['ibranch']	= $ibranch;
			} else {
				$data['ibranch']	= "";
			}	
		} else {
			$data['iop']	= "";
			$data['no']		= "";
			$data['sop']	= "";
			$data['f_op_dropforcast']	= "";
			$data['dop']	= "";
			$data['tBtsKirim']	= "";
			$icustomer	= "";
			$ibranch	= "";
			$data['icust']	= "";
		}
		
		$data['opt_pelanggan']	= $this->mclass->lpelanggan();
		$data['isi']	= 'listop/veditform';	
			$this->load->view('template',$data);
						
		
	}

	function cari_cabang() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$i_customer	= $this->input->post('ibranch');
		$this->load->model('listop/mclass');
		$query	= $this->mclass->getcabang($i_customer);
		if($query->num_rows()>0) {
			$cabang	= $query->result();
			foreach ($cabang as $row) {
				$data['option_cabang'][$row->i_branch_code]=$row->e_branch_name." ( ".$row->e_initial." ) ";
			}
			$c	= form_dropdown('i_branch', $data['option_cabang']);
			echo $c;		
		}
	}	

	function cari_op() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$nop		= $this->input->post('nop');
		$noplama	= $this->input->post('i');
		$icustomer	= $this->input->post('icustomer');
		
		$this->load->model('listop/mclass');
		$qnop	= $this->mclass->cari_op($nop,$noplama,$icustomer);
		if($qnop->num_rows()>0) {
			$rop	= $qnop->row();
			$iopcode= $rop->i_op_code;
			if(trim($iopcode)!=trim($noplama)){
				echo "Maaf, No. OP sdh ada!";
			}else{
			}
		}else{
		}
	}

	function cari_sop() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$nsop	= $this->input->post('nsop')?$this->input->post('nsop'):$this->input->get_post('nsop');
		$this->load->model('listop/mclass');
		$qnsop	= $this->mclass->cari_sop($nsop);
		if($qnsop->num_rows()>0) {
			echo "Maaf, No. SOP tdk dpt diubah!";
		}
	}
	
	function actedit() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$iterasii	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iterasii; // 0+1 = 2
		
		$v_count	= array();
		$i_product	= array();
		$e_product_name	= array();
		$e_note		= array();
		
		$i_op		= $this->input->post('i_op');
		$i_op_code_hidden	= $this->input->post('i_op_code_hidden');
		$i_op_hidden	= $this->input->post('i_op_hidden');
		$i_customer_hidden	= $this->input->post('i_customer_hidden');
		$i_sop_hidden	= $this->input->post('i_sop_hidden');
		$i_branch	= $this->input->post('i_branch');
		$d_op		= $this->input->post('d_op');
		$d_delivery_limit	= $this->input->post('d_delivery_limit');
		$f_op_dropforcast	= $this->input->post('f_op_dropforcast')=='1'?'TRUE':'FALSE';
		$v_count_0	= $this->input->post('v_count_'.'tblItem'.'_'.'0');
		// 13-04-2015
		$jenis_op	= $this->input->post('jenis_op');
				
		$ex_d_op	= explode("/",$d_op,strlen($d_op)); // dd/mm/YYYY
		$ex_d_delivery_limit	= explode("/",$d_delivery_limit,strlen($d_delivery_limit)); // dd/mm/YYYY
		
		$nw_d_op	= $ex_d_op[2]."-".$ex_d_op[1]."-".$ex_d_op[0];
		$nw_d_delivery_limit	= $ex_d_delivery_limit[2]."-".$ex_d_delivery_limit[1]."-".$ex_d_delivery_limit[0];
		
		for($cacah=0; $cacah<=$iterasi; $cacah++) { // iterasi=2, 0<2, 1<2			
			$v_count[$cacah]	= $this->input->post('v_count_'.'tblItem'.'_'.$cacah);
			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah);
			$e_note[$cacah]		= $this->input->post('e_note_'.'tblItem'.'_'.$cacah);
		}

		if( !empty($i_op_code_hidden) && 
		    !empty($i_sop_hidden) && 
		    !empty($i_customer_hidden) && 
		    !empty($i_branch)) {

			if(!empty($v_count_0)) {
				$this->load->model('listop/mclass');
				$this->mclass->mupdate($i_op,$i_op_code_hidden,$i_op_hidden,$i_customer_hidden,$i_branch,$nw_d_op,$nw_d_delivery_limit,$i_sop_hidden,$f_op_dropforcast,$v_count,$i_product,$e_product_name,$e_note, $jenis_op, $iterasi);
			}else{
				print "<script>alert(\"Maaf, item OP harus terisi. Terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
			
			}
		}else{
			print "";
		}
	}
	
	
	function koreksijmlorder() {
		
		$iop		= $this->uri->segment(4);
		$iopcode	= $this->uri->segment(5);
		$icustomer	= $this->uri->segment(6);
		
		$qOP	= $this->db->query(" SELECT i_op_item, i_op, i_product, n_count, n_residual FROM tm_op_item WHERE i_op='$iop' AND n_residual < 0 ");
		if($qOP->num_rows()>0) {
			foreach($qOP->result() as $row1) {
				$qDO = $this->db->query(" SELECT SUM(a.n_deliver) AS jmlpemenuhan FROM tm_do_item a, tm_do b 
						WHERE a.i_op='$row1->i_op' AND a.i_product='$row1->i_product' AND b.f_do_cancel='f' AND a.i_do=b.i_do ");
				if($qDO->num_rows()>0) {
					$rDO	= $qDO->row();
					if($rDO->jmlpemenuhan!='') {
						$sisa	= (($row1->n_count)-($rDO->jmlpemenuhan));
						$this->db->query(" UPDATE tm_op_item SET n_residual='$sisa' WHERE i_op_item='$row1->i_op_item' AND i_op='$row1->i_op' AND i_product='$row1->i_product' ");
					}
				}
			}
		}
		
		print "<script>alert(\" Nilai pemenuhan OP $iopcode telah dikoreksi. Terimakasih.\");show(\"listop/cform\",\"#content\");</script>";
		
	}

	function koreksijmlorder2() {
		
		$iopcode	= $this->uri->segment(4);
		$icustomer	= $this->uri->segment(5);
		
		$qop		= $this->db->query(" SELECT i_op FROM tm_op WHERE i_op_code='$iopcode' AND i_customer='$icustomer' AND f_op_cancel='false' ");
		
		if($qop->num_rows()>0) {
			
			$rop = $qop->row();
			
			$qOPdetail	= $this->db->query(" SELECT i_op_item, i_op, i_product, n_count, n_residual FROM tm_op_item WHERE i_op='$rop->i_op' AND n_residual < 0 ");
			
			if($qOPdetail->num_rows()>0) {
				foreach($qOPdetail->result() as $row1) {
					$qDO = $this->db->query(" SELECT SUM(a.n_deliver) AS jmlpemenuhan FROM tm_do_item a, tm_do b 
						WHERE a.i_op='$row1->i_op' AND a.i_product='$row1->i_product' AND b.f_do_cancel='f' AND a.i_do=b.i_do ");									
					if($qDO->num_rows()>0) {
						$rDO	= $qDO->row();
						if($rDO->jmlpemenuhan!='') {
							$sisa	= (($row1->n_count)-($rDO->jmlpemenuhan));
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisa' WHERE i_op_item='$row1->i_op_item' AND i_op='$row1->i_op' AND i_product='$row1->i_product' ");
						}
					}
				}
			}
			
		}
		
		print "<script>alert(\" Nilai pemenuhan OP $iopcode telah dikoreksi. Terimakasih.\");show(\"listop/cform\",\"#content\");</script>";
		
	}
	
}
?>
