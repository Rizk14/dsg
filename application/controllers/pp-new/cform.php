<?php
class Cform extends CI_Controller {

  function __construct () {
    parent::__construct();
    $this->load->library('pagination'); 
    $this->load->model('pp-new/mmaster');
  }
  
  /*function tesjquery() {
	  $data['isi'] = 'pp-new/tesjquerypush';
	
	$this->load->view('template',$data);
  } */
  
  function index() {
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_pp 	= $this->uri->segment(4);
	
	$curpage 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
	
	$jenis_brg	= 1;
	$th_now	= date("Y");
	
	if ($id_pp != '') {
		$hasil = $this->mmaster->get_header_pp($id_pp);
		$edit = 1;
				
		foreach ($hasil as $row) {
			$e_tgl_pp = explode("-",$row->tgl_pp,strlen($row->tgl_pp)); // Y-m-d
			$n_tgl_pp = $e_tgl_pp[2]."-".$e_tgl_pp[1]."-".$e_tgl_pp[0];
			
			$eid = $row->id;
			$eno_pp = $row->no_pp;
			$eid_bagian = $row->id_bagian;
			$etgl_pp = $n_tgl_pp;
		}
		$data['pp_detail'] = $this->mmaster->get_detail_pp($id_pp);
		$data['curpage'] = $curpage;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
	}
	else {
		// generate no PP
		// =======================================================
		
			$query3	= $this->db->query(" SELECT no_pp FROM tm_pp ORDER BY no_pp DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_pp	= $hasilrow->no_pp;
			else
				$no_pp = '';
			if(strlen($no_pp)==12) {
				$nopp = substr($no_pp, 3, 9);
				$n_pp	= (substr($nopp,4,5))+1;
				$th_pp	= substr($nopp,0,4);
				if($th_now==$th_pp) {
						$jml_n_pp	= $n_pp;
						switch(strlen($jml_n_pp)) {
							case "1": $kodepp	= "0000".$jml_n_pp;
							break;
							case "2": $kodepp	= "000".$jml_n_pp;
							break;	
							case "3": $kodepp	= "00".$jml_n_pp;
							break;
							case "4": $kodepp	= "0".$jml_n_pp;
							break;
							case "5": $kodepp	= $jml_n_pp;
							break;	
						}
						$nomorpp = $th_now.$kodepp;
				}
				else {
					$nomorpp = $th_now."00001";
				}
			}
			else {
				$nomorpp	= $th_now."00001";
			}
			$nomorpp = "PP-".$nomorpp;
			$data['no_pp'] = $nomorpp;
		
		// =======================================================
					
		$eid = '';
		$eno_pp = $nomorpp;
		$etgl_pp = '';
		$eid_bagian = '';
		$edit = '';
	}
	
	$data['edit'] = $edit;
	$data['eid'] = $eid;
	$data['etgl_pp'] = $etgl_pp;
	$data['eno_pp'] = $eno_pp;
	$data['eid_bagian'] = $eid_bagian;
	$data['msg'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['satuan'] = $this->mmaster->get_satuan();
	$data['bagian'] = $this->mmaster->get_bagian();
    $data['isi'] = 'pp-new/vmainform';
    $data['jenis_brg'] = $jenis_brg;
	
	$this->load->view('template',$data);
  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
	//$departemen = ambil infonya dari tabel user
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);

	if ($posisi == '' || $kel_brg =='') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg = $this->input->post('kel_brg', TRUE);  
	}	
	
		$keywordcari 	= $this->input->post('cari', TRUE);
		$id_jenis_brg = $this->input->post('id_jenis_brg', TRUE);    
		
		if ($keywordcari == '' && ($id_jenis_brg == '' || $posisi == '' || $kel_brg == '') ) {
			$kel_brg 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$id_jenis_brg 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		$keywordcari = str_replace("%20"," ", $keywordcari);	
			
		if ($id_jenis_brg == '')
			$id_jenis_brg 	= '0';
		
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $id_jenis_brg);
		//$jum_total	= $qjum_total->num_rows(); 

			$config['base_url'] = base_url().'index.php/pp-new/cform/show_popup_brg/'.$kel_brg.'/'.$posisi.'/'.$id_jenis_brg.'/'.$keywordcari.'/';
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $id_jenis_brg);						
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	$data['kel_brg'] = $kel_brg;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;
	$data['jenis_brg'] = $this->mmaster->get_jenis_brg($kel_brg);
	$data['cjenis_brg'] = $id_jenis_brg;
	$data['startnya'] = $config['cur_page'];
	
	$this->load->view('pp-new/vpopupbrg',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	//$this->load->library('form_validation');
		 
	/*		$this->form_validation->set_rules('no_faktur', 'Nomor PP', 'required');
			$this->form_validation->set_rules('tgl_retur', 'Tanggal PP', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$data['isi'] = 'pp-bb/vmainform';
			$data['msg'] = 'Field2 nomor PP, tanggal PP tidak boleh kosong..!';
			$eno_pp = '';
			$etgl_pp = '';
			$edit = '';
			$eid = '';
			
			$data['eno_pp'] = $eno_pp;
			$data['etgl_pp'] = $etgl_pp;
			$data['edit'] = $edit;
			$data['eid'] = $eid;
			$this->load->view('template',$data);
		} */
		//else
	//	{
			$goedit 	= $this->input->post('goedit', TRUE);
			$id_bagian = $this->input->post('bagian', TRUE);
			$no_pp 	= $this->input->post('no_faktur', TRUE);
			$tgl_pp 	= $this->input->post('tgl_retur', TRUE);  // d-m-Y
			$kodeedit 	= $this->input->post('kodeeditnopp', TRUE); 
			$no 	= $this->input->post('no', TRUE);
			$id_pp 	= $this->input->post('id_pp', TRUE);
			$e_tgl_pp = explode("-",$tgl_pp,strlen($tgl_pp)); // d-m-Y
			$n_tgl_pp = $e_tgl_pp[2]."-".$e_tgl_pp[1]."-".$e_tgl_pp[0]; // Y-m-d
			 
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($no_pp);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'pp-new/vmainform';
					$data['msg'] = "Data PP nomor ".$no_pp." sudah ada..!";
					$eno_pp = '';
					$etgl_pp = '';
					$edit = '';
					$data['eno_pp'] = $no_pp;
					$data['etgl_pp'] = $tgl_pp;
					$data['eid_bagian'] = $id_bagian;
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
					// inspired from proIT inventory
					// 25-06-2015: $this->input->post('is_satuan_lain_'.$i, TRUE), $this->input->post('satuan_lain_'.$i, TRUE) dikeluarin
						$this->mmaster->save($no_pp,$n_tgl_pp, $id_bagian, $this->input->post('id_brg_'.$i, TRUE),
						$this->input->post('nama_'.$i, TRUE), $this->input->post('id_satuan_'.$i, TRUE),
									$this->input->post('jumlah_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE), $goedit);
					}
					redirect('pp-new/cform/view');
				}
			} // end if goedit == ''
			else { // UPDATE DATA
				$cek_data = $this->mmaster->cek_data($no_pp);
				$curpage 	= $this->input->post('curpage', TRUE);
				$carinya 	= $this->input->post('carinya', TRUE);
				$is_cari 	= $this->input->post('is_cari', TRUE);
				
				if ($no_pp != $kodeedit) {
					if (count($cek_data) > 0) { 
						$data['isi'] = 'pp-new/vmainform';
						$data['msg'] = "Data PP Nomor ".$no_pp." sudah ada..!";
						
						$edit = '1';
						$data['eno_pp'] = $no_pp;
						$data['etgl_pp'] = $tgl_pp;
						$data['eid_bagian'] = $id_bagian;
						$data['edit'] = $edit;
						$this->load->view('template',$data);
					}
					else {
						$uid_update_by = $this->session->userdata('uid');
						$jumlah_input=$no-1;
					
						// ambil id di tabel tm_pp sesuai dgn no_pp yg lama
						//$query2	= $this->db->query(" SELECT id FROM tm_pp WHERE no_pp = '$kodeedit' AND kode_bagian= '$kode_bagian' ");
						//$hasilrow = $query2->row();
						//$id_pp_lama	= $hasilrow->id;
						$this->db->delete('tm_pp_detail', array('id_pp' => $id_pp));
						$tgl = date("Y-m-d H:i:s");
						$data_header = array(
						  'no_pp'=>$no_pp,
						  'tgl_pp'=>$n_tgl_pp,
						  'id_bagian'=>$id_bagian,
						  'tgl_update'=>$tgl,
						  'uid_update_by'=>$uid_update_by
						);
						$this->db->where('id',$id_pp);
						$this->db->update('tm_pp',$data_header);
										
						for ($i=1;$i<=$jumlah_input;$i++)
						{
							// 25-06-2015 $this->input->post('is_satuan_lain_'.$i, TRUE), $this->input->post('satuan_lain_'.$i, TRUE) GA DIPAKE LG
							$this->mmaster->update_pp($id_pp,$this->input->post('id_brg_'.$i, TRUE),
							$this->input->post('nama_'.$i, TRUE), $this->input->post('id_satuan_'.$i, TRUE),
							$this->input->post('jumlah_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE), $goedit);
						}
						
						// hapus semua OP! (koreksi: ini ga perlu dipake lagi, karena PP boleh diedit kalo ga ada data OPnya)
						/*$qop	= $this->db->query(" SELECT id FROM tm_op WHERE id_pp ='$id_pp' ");
						$hasil = $qop->result();
						foreach ($hasil as $row1) {			
							$this->db->delete('tm_op_detail', array('id_op' => $row1->id));
						}
						$this->db->delete('tm_op', array('id_pp' => $id_pp)); */
						
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "pp-new/cform/view/index/".$curpage;
						else
							$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;
						
						redirect($url_redirectnya);
						//redirect('pp-new/cform/view');
					}
				}
				else {
						$jumlah_input=$no-1;
						$tgl = date("Y-m-d H:i:s");
						$this->db->delete('tm_pp_detail', array('id_pp' => $id_pp));
						$data_header = array(
						  'no_pp'=>$no_pp,
						  'tgl_pp'=>$n_tgl_pp,
						  'id_bagian'=>$id_bagian,
						  'tgl_update'=>$tgl
						);
						$this->db->where('id',$id_pp);
						$this->db->update('tm_pp',$data_header);
										
						for ($i=1;$i<=$jumlah_input;$i++)
						{
							// 25-06-2015 $this->input->post('is_satuan_lain_'.$i, TRUE), $this->input->post('satuan_lain_'.$i, TRUE) GA DIPAKE LG
							$this->mmaster->update_pp($id_pp,$this->input->post('id_brg_'.$i, TRUE),
							$this->input->post('nama_'.$i, TRUE), $this->input->post('id_satuan_'.$i, TRUE),
							$this->input->post('jumlah_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE), $goedit);
						}
												
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "pp-new/cform/view/index/".$curpage;
						else
							$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;
						
						redirect($url_redirectnya);
				}
			}
			
	//	} end if kosong
  }
  
  function view(){	
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'pp-new/vformview';
    $keywordcari = "all";
	
    $qjum_total = $this->mmaster->getAlltanpalimit($keywordcari);
	//$jum_total	= $qjum_total->num_rows();
	
							$config['base_url'] = base_url().'index.php/pp-new/cform/view/index/';
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$config['cur_page'], $keywordcari);	
	$data['jum_total'] = count($qjum_total);
	$data['cur_page'] = $config['cur_page'];
	$data['is_cari'] = 0;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $qjum_total = $this->mmaster->getAlltanpalimit($keywordcari);
	//$jum_total	= $qjum_total->num_rows();
	
							$config['base_url'] = base_url().'index.php/pp-new/cform/cari/index/'.$keywordcari.'/';
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	$data['cur_page'] = $config['cur_page'];
	$data['is_cari'] = 1;
	$data['isi'] = 'pp-new/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    
    $curpage 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "pp-new/cform/view/index/".$curpage;
	else
		$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;
    redirect($url_redirectnya);
    //redirect('pp-new/cform/view');
  }
  
  function edititem() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $iddetail 	= $this->uri->segment(4);  
    $curpage 	= $this->uri->segment(5);  
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($iddetail == '' && $curpage == '' && $is_cari == '' && $carinya == '') {
		$iddetail 	= $this->input->post('iddetail', TRUE);
		$curpage 	= $this->input->post('curpage', TRUE);
		$carinya 	= $this->input->post('carinya', TRUE);
		$is_cari 	= $this->input->post('is_cari', TRUE);
	}
    
    if ($go_edit == '') {
		$data['query'] = $this->mmaster->get_item_pp($iddetail);
		$data['kel_brg'] = $this->mmaster->get_kel_brg();
		$data['isi'] = 'pp-new/vedititemform';
		$data['iddetail'] = $iddetail;
		$data['curpage'] = $curpage;
		$data['carinya'] = $carinya;
		$data['is_cari'] = $is_cari;
		$this->load->view('template',$data);
	}
	else { // update data detail
		$id_brg 	= $this->input->post('id_brg_0', TRUE);
		$qty 	= $this->input->post('jumlah_0', TRUE);
		$qty_lama 	= $this->input->post('qty_lama', TRUE);
		$id_pp 	= $this->input->post('id_pp', TRUE);
		$tgl_pp 	= $this->input->post('tgl_pp', TRUE);
		
		$pisah1 = explode("-", $tgl_pp);
		$thn1= $pisah1[2];
		$bln1= $pisah1[1];
		$tgl1= $pisah1[0];
		
		$tgl_pp = $thn1."-".$bln1."-".$tgl1;
		
		$tgl = date("Y-m-d H:i:s");
		
		//if ($qty != $qty_lama) {
			$this->db->query(" UPDATE tm_pp_detail SET id_brg = '$id_brg', qty = '$qty' WHERE id = '$iddetail' ");    
			$this->db->query(" UPDATE tm_pp SET tgl_pp = '$tgl_pp', tgl_update = '$tgl' WHERE id = '$id_pp' ");    
		//}
		//$url_redirectnya = "pp-new/cform/view/index/".$curpage;
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "pp-new/cform/view/index/".$curpage;
		else
			$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;
		
		redirect($url_redirectnya);
	}
  }
  
  function deleteitem(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $iddetail 	= $this->uri->segment(4);
    $curpage 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    $this->mmaster->deleteitem($iddetail);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "pp-new/cform/view/index/".$curpage;
	else
		$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;
    redirect($url_redirectnya);
  }
  
  function cari_pp() {
  	$nomorpp	= $this->input->post('nomorpp')?$this->input->post('nomorpp'):$this->input->post('nomorpp');
	$query	= $this->mmaster->getpp($nomorpp);
	if($query->num_rows()>0) {
		echo "Maaf, No. PP sudah ada!";
	}
  }
  
  // 21 okt 2011
  function tambahitem(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_pp 	= $this->uri->segment(4);
	$curpage 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
	
	$hasil = $this->mmaster->get_header_pp($id_pp);
				
	foreach ($hasil as $row) {
		$e_tgl_pp = explode("-",$row->tgl_pp,strlen($row->tgl_pp)); // Y-m-d
		$n_tgl_pp = $e_tgl_pp[2]."-".$e_tgl_pp[1]."-".$e_tgl_pp[0];
		
		$eid = $row->id;
		$eno_pp = $row->no_pp;
		$eid_bagian = $row->id_bagian;
		$etgl_pp = $n_tgl_pp;
	}
	//$data['pp_detail'] = $this->mmaster->get_detail_pp($id_pp);
	$data['curpage'] = $curpage;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;
	
	$data['eid'] = $eid;
	$data['etgl_pp'] = $etgl_pp;
	$data['eno_pp'] = $eno_pp;
	$data['eid_bagian'] = $eid_bagian;
	$data['msg'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['bagian'] = $this->mmaster->get_bagian();

	$data['isi'] = 'pp-new/vadditemform';
	$this->load->view('template',$data);
  }
  
  function tambahitemsubmit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$no 	= $this->input->post('no', TRUE);
			$id_pp 	= $this->input->post('id_pp', TRUE);
			$id_bagian = $this->input->post('bagian', TRUE);
			
			$curpage 	= $this->input->post('curpage', TRUE);
			$carinya 	= $this->input->post('carinya', TRUE);
			$is_cari 	= $this->input->post('is_cari', TRUE);
			 
				$cek_data = $this->mmaster->cek_data($no_pp);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'pp-new/vmainform';
					$data['msg'] = "Data PP nomor ".$no_pp." sudah ada..!";
					$eno_pp = '';
					$etgl_pp = '';
					$edit = '';
					$data['eno_pp'] = $no_pp;
					$data['etgl_pp'] = $tgl_pp;
					$data['eid_bagian'] = $id_bagian;
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						// simpan item brgnya
						$query3	= $this->db->query(" SELECT id FROM tm_pp_detail 
						WHERE id_brg = '".$this->input->post('id_brg_'.$i, TRUE)."' AND id_pp = '$id_pp' ");
						if ($query3->num_rows() == 0){
							$data_detail = array(
							  'id_brg'=>$this->input->post('id_brg_'.$i, TRUE),
							  'qty'=>$this->input->post('jumlah_'.$i, TRUE),
							  'keterangan'=>$this->input->post('keterangan_'.$i, TRUE),
							  'id_pp'=>$id_pp
							);
							$this->db->insert('tm_pp_detail',$data_detail);
						}
						
					}
					
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "pp-new/cform/view/index/".$curpage;
					else
						$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;
					
					redirect($url_redirectnya);
				}
	}
	
	// 02-02-2013
	function updatestatus(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $aksi = $this->uri->segment(5);
    $cur_page 	= $this->uri->segment(6);
    $is_cari 	= $this->uri->segment(7);
    $csupplier 	= $this->uri->segment(8);
    $carinya 	= $this->uri->segment(9);
    
    $this->mmaster->updatestatus($id, $aksi);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "pp-new/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('op/cform/view');
  }
  
  // 01-10-2015
  function migrasinamabrg() {
	  $sql = " SELECT id, id_brg FROM tm_pp_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.nama_brg, a.satuan FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
			$id_satuan	= $hasilrow->satuan;
						
			$this->db->query(" UPDATE tm_pp_detail SET nama_brg = '$nama_brg', id_satuan='$id_satuan' WHERE id = '$row1->id' ");
		}
	  }
	  echo "sukses migrasi nama_brg di tm_pp_detail <br>";
  }
}
