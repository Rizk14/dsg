<?php
class makloon_unit_baju_wip extends CI_Controller
{
    public $data = array(
        'halaman' => 'makloon_unit_baju_wip',        
        'title' => 'Makloon Unit Baju Wip',
        'isi' => 'makloon_unit_baju_wip/makloon_unit_baju_wip_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('makloon_unit_baju_wip/makloon_unit_baju_wip_model', 'makloon_unit_baju_wip');
    }

   
    public function index()
    {
		$no=$this->input->post('no');
		
        $this->data['values'] = (object) $this->makloon_unit_baju_wip->default_values;
        $this->data['unit_jahit'] =  $this->makloon_unit_baju_wip->get_unit_jahit();
        $this->data['unit_packing'] =  $this->makloon_unit_baju_wip->get_unit_packing();
        $this->data['gudang'] =  $this->makloon_unit_baju_wip->get_gudang();
		$this->load->view('template', $this->data);
			
    }
     public function sukses_input()
    {	
		
        $this->data['isi'] = 'makloon_unit_baju_wip/makloon_unit_baju_wip-sukses';
        $this->load->view('template', $this->data);
    }

    // Jika pendaftaran error, tampilkan informasi mengenai error.
    public function error_input()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Penginputan Makloon Unit Baju Wip Error';
        $this->load->view('template', $this->data);
    }
   public function submit()
    {

		$no=$this->input->post('no',TRUE);
		$id_gudang= $this->input->post('gudang_masuk',TRUE);
		$id_unit_jahit= $this->input->post('unit_jahit',TRUE);
		
		$id = $this->makloon_unit_baju_wip->input_header(
       $this->input->post('no_sj',TRUE),
       $this->input->post('jenis_masuk',TRUE),
       $id_gudang,$id_unit_jahit,
       $this->input->post('keterangan_header',TRUE),
        $this->input->post('tanggal_sj',TRUE)
       );
       
      
       foreach ($id as $id_makloon_unit_baju_wip){
		 $jumlah_input=$no-1;
		 for($i=1; $i<=$jumlah_input; $i++){
		$this->makloon_unit_baju_wip->input_detail(
		$id_makloon_unit_baju_wip->id,	
		$this->input->post('id_barang_bb_'.$i,TRUE),
		$this->input->post('qty_'.$i,TRUE),
		 $this->input->post('keterangan_detail_'.$i,TRUE),$id_gudang,$id_unit_jahit,
		 $this->input->post('id_barang_wip_'.$i,TRUE)
		);
	}
		}
		if(true)
		//redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/sukses_input');
		redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/view');
		else 
		redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/error_input');
    }
    
    
   public function view($offset= null)
    {	
		
		$page = $this->uri->segment(4);
		$per_page=10;
		
		if (empty($page)) {
		$offset = 0;
		} else {
		$offset = ($page * $per_page - $per_page);
		}
		
       $makloon_unit_baju_wip = $this->makloon_unit_baju_wip->get_all_inner_paged($offset);
        if ($makloon_unit_baju_wip) {
            $this->data['makloon_unit_baju_wip'] = $makloon_unit_baju_wip;
            $this->data['paging'] = $this->makloon_unit_baju_wip->paging_barang('biasa', site_url('makloon_unit_baju_wip/makloon_unit_baju_wip/halaman/'), 4);
        } else {
            $this->data['makloon_unit_baju_wip'] = 'Tidak ada data Makloon Unit Baju Wip, Silahkan Melakukan '.anchor('/makloon_unit_baju_wip/makloon_unit_baju_wip/', 'Proses penginputan.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('makloon_unit_baju_wip/makloon_unit_baju_wip/cari');
        $this->data['isi'] = 'makloon_unit_baju_wip/makloon_unit_baju_wip_list';
        $this->load->view('template', $this->data);
    }
    public function cari($offset = 0)
    {
        $makloon_unit_baju_wip = $this->makloon_unit_baju_wip->cari($offset);
        if ($makloon_unit_baju_wip) {
            $this->data['makloon_unit_baju_wip'] = $makloon_unit_baju_wip;
            $this->data['paging'] = $this->makloon_unit_baju_wip->paging('pencarian', site_url('/makloon_unit_baju_wip/makloon_unit_baju_wip/cari/'), 4);
        } else {
            $this->data['makloon_unit_baju_wip'] = 'Data tidak ditemukan.'. anchor('/makloon_unit_baju_wip/makloon_unit_baju_wip/view', ' Tampilkan semua Makloon Unit Baju Wip.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('/makloon_unit_baju_wip/makloon_unit_baju_wip/cari');
        $this->data['isi'] = 'makloon_unit_baju_wip/makloon_unit_baju_wip_list';
        $this->load->view('template', $this->data);
    }
    
   
    
     public function hapus($id,$id_gudang,$id_unit_jahit)
    {
       
      
        if ($this->session->userdata('user_bagian') != '2') {
            $this->session->set_flashdata('pesan_error', 'Anda tidak berhak menghapus data Makloon Unit Baju Wip. Kembali ke halaman ' . anchor('makloon_unit_baju_wip/makloon_unit_baju_wip', 'makloon_unit_baju_wip.', 'class="alert-link"'));
            redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/error');
        }

        if (! $this->makloon_unit_baju_wip->get($id)) {
            $this->session->set_flashdata('pesan_error', 'Data Makloon Unit Baju Wip tidak ada. Kembali ke halaman ' . anchor('makloon_unit_baju_wip/makloon_unit_baju_wip', 'makloon_unit_baju_wip.', 'class="alert-link"'));
            redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/error');
        }
		 if($id_gudang == 0){
		  $id_gudang = $this->uri->segment(5);
		  }
		if($id_unit_jahit == 0){
		  $id_unit_jahit = $this->uri->segment(6);
		  }	
        // Hapus
        if ($this->makloon_unit_baju_wip->delete_aktif($id,$id_gudang,$id_unit_jahit)) {
            $this->session->set_flashdata('pesan', 'Data berhasil dihapus. Kembali ke halaman '. anchor('makloon_unit_baju_wip/makloon_unit_baju_wip/view', 'View Makloon Unit Baju Wip.', 'class="alert-link"'));
            redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/sukses');
        } else {
			
            $this->session->set_flashdata('pesan_error', 'Data gagal dihapus. Kembali ke halaman '. anchor('makloon_unit_baju_wip/makloon_unit_baju_wip/view', 'View Makloon Unit Baju Wip.', 'class="alert-link"'));
            redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/error');
        }
    }
     public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }
    
    public function edit()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $makloon_unit_baju_wip = $this->makloon_unit_baju_wip->getAllDetail($id);
        if (! $makloon_unit_baju_wip) {
            $this->session->set_flashdata('pesan_error', 'Data Makloon Unit Baju Wip tidak ada. Kembali ke halaman ' . anchor('makloon_unit_baju_wip/master-barang/view', 'Makloon Unit Baju Wip.', 'class="alert-link"'));
            redirect('makloon_unit_baju_wip/master-barang/error');
        }
		$this->data['list_unit_jahit'] = $this->makloon_unit_baju_wip->get_unit_jahit(); 
		$this->data['list_gudang'] = $this->makloon_unit_baju_wip->get_gudang(); 
		
        $this->data['values'] = $makloon_unit_baju_wip;
		$this->data['isi'] = 'makloon_unit_baju_wip/makloon_unit_baju_wip_form_edit';
        $this->load->view('template', $this->data);
	 }
	 
	 
	  public function updatedata(){
			$id =  $this->input->post('id',TRUE);
	        $no_sj =  $this->input->post('no_sj',TRUE);
			$tanggal_sj =  $this->input->post('tanggal_sj',TRUE);
			$jenis_masuk =  $this->input->post('jenis_masuk',TRUE);
			$id_unit_jahit =  $this->input->post('id_unit_jahit',TRUE);
			
			$id_gudang =  $this->input->post('id_gudang',TRUE);
			$keterangan_header =  $this->input->post('keterangan_header',TRUE);
			
			$jum_data_detail =  $this->input->post('jum_data_detail',TRUE);
			$jum_data_motif =  $this->input->post('jum_data_motif',TRUE);			
	  
		
		$update_header = $this->makloon_unit_baju_wip->update_header($id,$no_sj,$tanggal_sj,$jenis_masuk,
		$id_gudang,$id_unit_jahit,$keterangan_header);
		
		foreach ($update_header as $row){
    	 for($i=1;$i<=$jum_data_detail-1;$i++){	
    	 $this->makloon_unit_baju_wip->update_detail(
    	$id_unit_jahit, $row->id,
    	 $this->input->post('id_barang_wip_'.$i,TRUE),
    	 $this->input->post('qty_'.$i,TRUE),
    	 $this->input->post('qty_lama_'.$i,TRUE),
    	 $this->input->post('keterangan_detail_'.$i,TRUE),
    	 $this->input->post('id_detail_'.$i,TRUE),$id_gudang
    	);    
		$this->makloon_unit_baju_wip->update_detail_motif($this->input->post('id_detail_motif_'.$i,TRUE),
		 $this->input->post('qty_'.$i,TRUE),$id_unit_jahit,$id_gudang, $this->input->post('qty_lama_'.$i,TRUE),
		 $this->input->post('id_barang_bb_'.$i,TRUE),$this->input->post('id_detail_'.$i,TRUE),$jum_data_detail);
	 
	 }
		}
		if(true){
		$this->session->set_flashdata('pesan', 'Data berhasil diupdate. Kembali ke halaman ' . anchor('makloon_unit_baju_wip/makloon_unit_baju_wip/view', 'Makloon Baju Gudang Jadi WIP.', 'class="alert-link"'));
		redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/view');
		}
		else {
		$this->session->set_flashdata('pesan_error', 'Data tidak berhasil diupdate. Kembali ke halaman ' . anchor('makloon_unit_baju_wip/makloon_unit_baju_wip/view', 'Makloon Baju Gudang Jadi WIP.', 'class="alert-link"'));
		redirect('makloon_unit_baju_wip/makloon_unit_baju_wip/error_input');
	}
}

public function cari_barang($offset = 0)
    {
        $kata_kunci =  $this->input->post('kata_kunci');
        $posisi =  $this->input->post('posisi');
		if($posisi == ''){
			$posisi =$this->uri->segment(4);
		}
		$makloon_unit_baju_wip_barang = $this->makloon_unit_baju_wip->cari_barang($offset,$kata_kunci);
        if ($makloon_unit_baju_wip_barang) {
            $this->data['makloon_unit_baju_wip_barang'] = $makloon_unit_baju_wip_barang;
            $this->data['paging'] = $this->makloon_unit_baju_wip->paging_barang('pencarian', site_url('/makloon_unit_baju_wip/makloon_unit_baju_wip/cari_barang/'.$posisi), 5);
        } else {
            $this->data['makloon_unit_baju_wip_barang'] = 'Data tidak ditemukan.'. anchor('/makloon_unit_baju_wip/makloon_unit_baju_wip/barang_bb/'.$posisi, ' Tampilkan semua data di master barang wip.', 'class="alert-link"');
        }
		
        $this->data['posisi'] = $posisi;
        $this->data['form_action'] = site_url('/makloon_unit_baju_wip/makloon_unit_baju_wip/cari_barang/'.$posisi);
        $this->data['isi'] = 'makloon_unit_baju_wip/makloon_unit_baju_wip_list_barang';
        $this->load->view('template', $this->data);
    }
     
}

