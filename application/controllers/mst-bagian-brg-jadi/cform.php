<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-bagian-brg-jadi/mmaster');
  }

  function index(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->id;
			$enama = $row->nama_bagian;
		}
	}
	else {
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-bagian-brg-jadi/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		
			$id_bagian 	= $this->input->post('id_bagian', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == 1) {
				$this->mmaster->save($id_bagian, $nama, $goedit);
			}
			else {
				$cek_data = $this->mmaster->cek_data($nama);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id_bagian, $nama, $goedit);
				}
			}
			
			redirect('mst-bagian-brg-jadi/cform');
		
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-bagian-brg-jadi/cform');
  }
}
