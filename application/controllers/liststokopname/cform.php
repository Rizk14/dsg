<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

			$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
			$data['form_title_detail_laporansok']	= $this->lang->line('form_title_detail_laporansok');
			$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
			$data['list_laporansok_kd_brg']			= $this->lang->line('list_laporansok_kd_brg');
			$data['list_laporansok_nm_brg']			= $this->lang->line('list_laporansok_nm_brg');
			$data['list_laporansok_awalstok_brg']	= $this->lang->line('list_laporansok_awalstok_brg');
			$data['list_laporansok_akhirstok_brg']	= $this->lang->line('list_laporansok_akhirstok_brg');
			$data['list_laporansok_ket_brg']		= $this->lang->line('list_laporansok_ket_brg');
			$data['list_laporansok_sop_brg']		= $this->lang->line('list_laporansok_sop_brg');
			$data['list_laporansok_saldoakhirbln_brg']	= $this->lang->line('list_laporansok_saldoakhirbln_brg');
			$data['list_laporansok_sop_brg_warna']	= $this->lang->line('list_laporansok_sop_brg_warna');
		
			
			$data['button_batal']					= $this->lang->line('button_batal');
			$data['button_detail']					= $this->lang->line('button_detail');
			$data['button_update_opname']			= $this->lang->line('button_update_opname');
			
			$data['detail']							= "";
			$data['list']							= "";
			$data['limages']						= base_url();
			
			$blnarr	= array(
				"01"=>"Januari",
				"02"=>"Februari",
				"03"=>"Maret",
				"04"=>"April",
				"05"=>"Mei",
				"06"=>"Juni",
				"07"=>"Juli",
				"08"=>"Agustus",
				"09"=>"September",
				"10"=>"Oktober",
				"11"=>"Nopember",
				"12"=>"Desember");
				
			$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->uri->segment(4);
			$stp	= $this->input->post('stp')?$this->input->post('stp'):$this->uri->segment(5);
			$tglopname	= $this->input->post('tglopname')?$this->input->post('tglopname'):$this->uri->segment(6);
			
			$stp	= ($stp=='t')?('t'):('f');
			
			$uri1	= ($cari!='')?($cari):('kosong');
			$uri2	= ($this->uri->segment(5)=='')?($stp):($this->uri->segment(5));
			
			$data['stp']	= $uri2;
			
			$bln_skrng = date("m");
			$thn_skrng = date("Y");
			
			$this->load->model('liststokopname/mclass');
			
			$data['listtanggalopname']	= $this->mclass->tgl_opname();
			
			$tglactive	= $this->mclass->tglopnameactive($uri2);
			
			if($tglopname=='') {
				$rtglactive	= $tglactive->row();
				$tglopnamenya	= $rtglactive->d_so;
			}else{
				$tglopnamenya	= $tglopname;
			}
			
			$query	= $this->mclass->view($cari,$tglopnamenya,$uri2);
			$jml	= $query->num_rows();
			
			$exp_tgl= explode("-",$tglopnamenya); // YYYY-mm-dd

			$thn	= $exp_tgl[0];
			$bln	= $exp_tgl[1];
			
			$totalhari			= cal_days_in_month(CAL_GREGORIAN, $bln, $thn); 
			//echo $exp_tgl[0]." ".$exp_tgl[1]." ".$tglopname." skrg = ".$thn_skrng." ".$bln_skrng; die();
			if(($exp_tgl[0]!=$thn_skrng || $exp_tgl[1]!=$bln_skrng) && $tglopnamenya=='') {
				$thn	= $thn_skrng;
				$bln	= $bln_skrng;
			}
			
			$data['tglopnamenya']	= $tglopnamenya;
			
			$data['checked']	= ($uri2=='t')?(' checked '):('');
			$data['cari']		= ($cari!='' && $cari!='kosong')?($cari):('');
			
			//$totalhari			= cal_days_in_month(CAL_GREGORIAN, $bln, $thn); 
			
			$data['tglawalbln']	= $thn.'-'.$bln.'-'.'01';
			$data['tglakhirbln']= $thn.'-'.$bln.'-'.$totalhari;
			$data['totalhari']	= $totalhari;
			
			$pagination['base_url'] 	= '/liststokopname/cform/index/'.$uri1.'/'.$uri2.'/'.$tglopnamenya.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(7,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page'],$cari,$tglopnamenya,$uri2);
			
			$this->load->model('liststokopname/mclass');
			$data['isi']	= 'liststokopname/vlistform';
			$this->load->view('template',$data);
			
		}
	
	
	/*
	function back() {
		$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
		$data['form_title_detail_laporansok']	= $this->lang->line('form_title_detail_laporansok');
		$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
		$data['list_laporansok_kd_brg']			= $this->lang->line('list_laporansok_kd_brg');
		$data['list_laporansok_nm_brg']			= $this->lang->line('list_laporansok_nm_brg');
		$data['list_laporansok_awalstok_brg']	= $this->lang->line('list_laporansok_awalstok_brg');
		$data['list_laporansok_akhirstok_brg']	= $this->lang->line('list_laporansok_akhirstok_brg');
		$data['list_laporansok_ket_brg']		= $this->lang->line('list_laporansok_ket_brg');
		$data['button_batal']					= $this->lang->line('button_batal');
		$data['button_detail']					= $this->lang->line('button_detail');	
		$data['button_update_opname']			= $this->lang->line('button_update_opname');	
		$data['detail']							= "";
		$data['list']							= "";
		$data['limages']						= base_url();
		
		$blnarr	= array(
			"01"=>"Januari",
			"02"=>"Februari",
			"03"=>"Maret",
			"04"=>"April",
			"05"=>"Mei",
			"06"=>"Juni",
			"07"=>"Juli",
			"08"=>"Agustus",
			"09"=>"September",
			"10"=>"Oktober",
			"11"=>"Nopember",
			"12"=>"Desember");
			
		$cari	= $this->input->post('cari');
		
		$uri1	= ($cari!='')?($cari):('kosong');
		$uri2	= $this->uri->segment(4);
		$tglopnamenya = $this->uri->segment(5);
		
		$data['stp']		= $uri2;
		
		$this->load->model('liststokopname/mclass');
		
		$qnomorso	= $this->mclass->statusSO($uri2);
		if($qnomorso->num_rows()>0){
			$rnomorso		= $qnomorso->row();
			$iso			= $rnomorso->i_so;
			$istatusso		= $rnomorso->i_status_so; // hrs yang statusnya 0
			$stopproduct	= $rnomorso->f_stop_produksi; // true jika telah STP
			$dso			= explode("-",$rnomorso->d_so,strlen($rnomorso->d_so)); // Y-m-d
			$blso			= $dso[1];
			$data['blnso']	= $blnarr[$blso];
			$data['thnso']	= $dso[0];
		}else{
		}
		
		$query	= $this->mclass->view($cari,$iso,$uri2);
		$jml	= $query->num_rows();
		
		$data['checked']	= ($uri2=='t')?(' checked '):('');
		$data['cari']		= ($cari!='' && $cari!='kosong')?($cari):('');

		$thnawalbln	= date("Y");
		$blnawalbln	= date("m");
		$data['tglawalbln']	= $thnawalbln.'-'.$blnawalbln.'-'.'01';
		$data['tglakhirbln']	= date("Y-m-d");
		
		$pagination['base_url'] 	= '/liststokopname/cform/index/'.$uri1.'/'.$uri2.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();		
		$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page'],$cari,$iso,$uri2);
				
		$this->load->model('liststokopname/mclass');
		$this->load->view('liststokopname/vlistform',$data);
	}
	*/
		
	function updateopname() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$stp	= $this->uri->segment(4,0);
		$tglopnamenya = $this->uri->segment(5,0);
		$totalhari	= $this->uri->segment(6,0);
		
		$data['page_intitle_stokopname']		= $this->lang->line('page_intitle_stokopname');
		$data['page_title_stokopnamemutasi']	= $this->lang->line('page_title_stokopnamemutasi');
		$data['form_tanggal_stokopname']		= $this->lang->line('form_tanggal_stokopname');
		$data['form_title_detail_stokopname']	= $this->lang->line('form_title_detail_stokopname');
		$data['form_kode_product_opname']		= $this->lang->line('form_kode_product_opname');
		$data['form_nm_product_opname']			= $this->lang->line('form_nm_product_opname');
		$data['form_jml_product_opname']		= $this->lang->line('form_jml_product_opname');
		$data['form_jml_fisik_product_opname']	= $this->lang->line('form_jml_fisik_product_opname');
		$data['form_note_opname']				= $this->lang->line('form_note_opname');
		
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['button_mutasi']	= $this->lang->line('button_mutasi');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['detail']			= "";
		$data['list']			= "";
		$data['limages']		= base_url();
		
		$data['checked']	= $stp=='t'?' checked ':'';
		$data['stp']		= $stp;
			
		$tgl	= date("d");
		$bln	= date("m");
		$thn	= date("Y");
		$data['dateTime']	= date("m/d/Y",time());
		$data['tgOpname']	= $tgl."/".$bln."/".$thn;
		$data['tglopnamenya']	= $tglopnamenya;
		$data['totalhari']	= $totalhari;
		
		$this->load->model('liststokopname/mclass');
		
		$query	= $this->mclass->statusopname($stp,$tglopnamenya);
		
		if($query->num_rows()>0){
			$row1	= $query->row();
			$tglso	= explode("-",$row1->d_so,strlen($row1->d_so)); // Y-m-d
			
			$data['blnso']	= $tglso[1];
			$data['thnso']	= $tglso[0];
			$data['iso_lama']	= $row1->i_so_lama;
			$data['disabled']	= '';
		}else{
			$data['blnso']	= '';
			$data['thnso']	= '';
			$data['iso_lama']	= '';
			$data['disabled']	= 'disabled';
		}
		
		$data['query']	= $this->mclass->listopname($stp,$tglopnamenya);
			$data['isi']	= 'liststokopname/vformopname';
			$this->load->view('template',$data);
			
		
	}

	function update() {
 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproduct	= array();
		$nquantityakhir	= array();
		$isoitem	= array();
		$iso	= array();
		$enote	= array();
		$stok_fisik	= array();
		$i_color	= array();
		
		$jml	= 0;
		$cacah = 0;
		$success= 0;
				
		$stp	= $this->input->post('stp');
		$blnso	= $this->input->post('blnso');
		$thnso	= $this->input->post('thnso');
		$iteration		= $this->input->post('iteration');
		$tglopnamenya	= $this->input->post('tglopnamenya');
		$totalhari	= $this->input->post('totalhari');
		$iso_lama	= $this->input->post('iso_lama');
		
		while($jml<=$iteration) {
			$iproduct[$jml]			= $this->input->post('i_product_tblItem_'.$jml);
			$nquantityakhir[$jml]	= $this->input->post('n_quantity_akhir_tblItem_'.$jml);
			$enote[$jml]	= $this->input->post('e_note_tblItem_'.$jml);
			$isoitem[$jml]	= $this->input->post('isoitem_'.$jml);
			$iso[$jml]		= $this->input->post('iso_'.$jml);
			$stok_fisik[$jml] = $this->input->post('stok_fisik_'.$jml);
			$i_color[$jml] = $this->input->post('i_color_'.$jml);
			
			
			$jml+=1;
			
			if(($jml >= $iteration) || ($jml==$iteration)) {
				$success=1;
			}
		}
		
		$this->load->model('liststokopname/mclass');
		
		$this->mclass->mupdate($stp,$blnso,$thnso,$iproduct,$nquantityakhir,$isoitem,$iso,$iteration,$stp,$stok_fisik, $i_color,$enote,$tglopnamenya,$totalhari,$iso_lama);
		
		if($success==1) {
			
			$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
			$data['form_title_detail_laporansok']	= $this->lang->line('form_title_detail_laporansok');
			$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
			$data['list_laporansok_kd_brg']			= $this->lang->line('list_laporansok_kd_brg');
			$data['list_laporansok_nm_brg']			= $this->lang->line('list_laporansok_nm_brg');
			$data['list_laporansok_awalstok_brg']	= $this->lang->line('list_laporansok_awalstok_brg');
			$data['list_laporansok_akhirstok_brg']	= $this->lang->line('list_laporansok_akhirstok_brg');
			$data['list_laporansok_ket_brg']		= $this->lang->line('list_laporansok_ket_brg');
			$data['list_laporansok_sop_brg']		= $this->lang->line('list_laporansok_sop_brg');
			$data['list_laporansok_sop_brg_warna']		= $this->lang->line('list_laporansok_sop_brg_warna');
			$data['list_laporansok_saldoakhirbln_brg']	= $this->lang->line('list_laporansok_saldoakhirbln_brg');			
			$data['button_batal']					= $this->lang->line('button_batal');
			$data['button_detail']					= $this->lang->line('button_detail');	
			$data['button_update_opname']			= $this->lang->line('button_update_opname');	
			$data['detail']							= "";
			$data['list']							= "";
			$data['limages']						= base_url();
			
			$blnarr	= array(
				"01"=>"Januari",
				"02"=>"Februari",
				"03"=>"Maret",
				"04"=>"April",
				"05"=>"Mei",
				"06"=>"Juni",
				"07"=>"Juli",
				"08"=>"Agustus",
				"09"=>"September",
				"10"=>"Oktober",
				"11"=>"Nopember",
				"12"=>"Desember");
				
			$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->uri->segment(4);
			$stp	= $this->input->post('stp')?$this->input->post('stp'):$this->uri->segment(5);
			$tglopname	= $this->input->post('tglopname')?$this->input->post('tglopname'):$this->uri->segment(6);
			
			$stp	= ($stp=='t')?('t'):('f');
			
			$uri1	= ($cari!='')?($cari):('kosong');
			$uri2	= ($this->uri->segment(5)=='')?($stp):($this->uri->segment(5));
			
			$data['stp']	= $uri2;

			$bln_skrng = date("m");
			$thn_skrng = date("Y");
						
			$data['listtanggalopname']	= $this->mclass->tgl_opname();
			
			$tglactive	= $this->mclass->tglopnameactive($uri2);
			
			if($tglopname=='') {
				$rtglactive	= $tglactive->row();
				$tglopnamenya	= $rtglactive->d_so;
			}else{
				$tglopnamenya	= $tglopname;
			}
			
			$query	= $this->mclass->view($cari,$tglopnamenya,$uri2);
			$jml	= $query->num_rows();
			
			$exp_tgl	= explode("-",$tglopnamenya);
			
			$thn	= $exp_tgl[0];
			$bln	= $exp_tgl[1];

			if(($exp_tgl[0]!=$thn_skrng || $exp_tgl[1]!=$bln_skrng) && $tglopname=='') {
				$thn	= $thn_skrng;
				$bln	= $bln_skrng;
			}
						
			$data['tglopnamenya']	= $tglopnamenya;

			$data['checked']	= ($uri2=='t')?(' checked '):('');
			$data['cari']		= ($cari!='' && $cari!='kosong')?($cari):('');
			
			$totalhari=cal_days_in_month(CAL_GREGORIAN, $bln, $thn); 
						
			$data['tglawalbln']	= $thn.'-'.$bln.'-'.'01';
			$data['tglakhirbln']= $thn.'-'.$bln.'-'.$totalhari;
			$data['totalhari']	= $totalhari;
			
			$pagination['base_url'] 	= '/liststokopname/cform/index/'.$uri1.'/'.$uri2.'/'.$tglopnamenya.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(7,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page'],$cari,$tglopnamenya,$uri2);
			
			$data['isi']	= 'liststokopname/vlistform';
			$this->load->view('template',$data);
			
		}		
	}
		
	function update_old() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$stp	= $this->input->post('stp');
		$blnso	= $this->input->post('blnso');
		$thnso	= $this->input->post('thnso');
		$iteration		= $this->input->post('iteration');
		$tglopnamenya	= $this->input->post('tglopnamenya');
		$totalhari	= $this->input->post('totalhari');
		$iso_lama	= $this->input->post('iso_lama');
		
		$iproduct	= array();
		//$eproductname	= array();
		$nquantityakhir	= array();
		$isoitem	= array();
		$iso	= array();
		$enote	= array();
		
		$jml	= 0;
		$success= 0;
		while($jml<=$iteration){
			$iproduct[$jml]			= $this->input->post('i_product_tblItem_'.$jml);
			//$eproductname[$jml]	= $this->input->post('e_product_name_tblItem_'.$jml);
			$nquantityakhir[$jml]	= $this->input->post('n_quantity_akhir_tblItem_'.$jml);
			$enote[$jml]	= $this->input->post('e_note_tblItem_'.$jml);
			$isoitem[$jml]	= $this->input->post('isoitem_'.$jml);
			$iso[$jml]		= $this->input->post('iso_'.$jml);
			
			$jml+=1;
			
			if(($jml >= $iteration) || ($jml==$iteration)){
				$success=1;
			}
		}
		
		$this->load->model('liststokopname/mclass');
		$this->mclass->mupdate($stp,$blnso,$thnso,$iproduct,$nquantityakhir,$isoitem,$iso,$iteration,$stp,$enote,$tglopnamenya,$totalhari,$iso_lama);
		
		if($success==1){
			$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
			$data['form_title_detail_laporansok']	= $this->lang->line('form_title_detail_laporansok');
			$data['page_title_laporansok']			= $this->lang->line('page_title_laporansok');
			$data['list_laporansok_kd_brg']			= $this->lang->line('list_laporansok_kd_brg');
			$data['list_laporansok_nm_brg']			= $this->lang->line('list_laporansok_nm_brg');
			$data['list_laporansok_awalstok_brg']	= $this->lang->line('list_laporansok_awalstok_brg');
			$data['list_laporansok_akhirstok_brg']	= $this->lang->line('list_laporansok_akhirstok_brg');
			$data['list_laporansok_ket_brg']		= $this->lang->line('list_laporansok_ket_brg');
			$data['list_laporansok_sop_brg']		= $this->lang->line('list_laporansok_sop_brg');
			$data['list_laporansok_saldoakhirbln_brg']	= $this->lang->line('list_laporansok_saldoakhirbln_brg');	

			$data['list_laporansok_sop_brg_warna']	= $this->lang->line('list_laporansok_sop_brg_warna');	
			
			$data['button_batal']					= $this->lang->line('button_batal');
			$data['button_detail']					= $this->lang->line('button_detail');	
			$data['button_update_opname']			= $this->lang->line('button_update_opname');	
			$data['detail']							= "";
			$data['list']							= "";
			$data['limages']						= base_url();
			
			$blnarr	= array(
				"01"=>"Januari",
				"02"=>"Februari",
				"03"=>"Maret",
				"04"=>"April",
				"05"=>"Mei",
				"06"=>"Juni",
				"07"=>"Juli",
				"08"=>"Agustus",
				"09"=>"September",
				"10"=>"Oktober",
				"11"=>"Nopember",
				"12"=>"Desember");
				
			$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->uri->segment(4);
			$stp	= $this->input->post('stp')?$this->input->post('stp'):$this->uri->segment(5);
			$tglopname	= $this->input->post('tglopname')?$this->input->post('tglopname'):$this->uri->segment(6);
			
			$stp	= ($stp=='t')?('t'):('f');
			
			$uri1	= ($cari!='')?($cari):('kosong');
			$uri2	= ($this->uri->segment(5)=='')?($stp):($this->uri->segment(5));
			
			$data['stp']	= $uri2;
			
			$this->load->model('liststokopname/mclass');
			
			$data['listtanggalopname']	= $this->mclass->tgl_opname();
			$tglactive	= $this->mclass->tglopnameactive($uri2);
			if($tglopname==''){
				$rtglactive	= $tglactive->row();
				$tglopnamenya	= $rtglactive->d_so;
			}else{
				$tglopnamenya	= $tglopname;
			}
			
			$query	= $this->mclass->view($cari,$tglopnamenya,$uri2);
			$jml	= $query->num_rows();
			
			$exp_tgl	= explode("-",$tglopnamenya); // YYYY-mm-dd
			$thn	= $exp_tgl[0];
			$bln	= $exp_tgl[1];
			
			$data['tglopnamenya']	= $tglopnamenya;

			$data['checked']	= ($uri2=='t')?(' checked '):('');
			$data['cari']		= ($cari!='' && $cari!='kosong')?($cari):('');
			
			$totalhari=cal_days_in_month(CAL_GREGORIAN, $bln, $thn); 
						
			$data['tglawalbln']	= $thn.'-'.$bln.'-'.'01';
			$data['tglakhirbln']= $thn.'-'.$bln.'-'.$totalhari;
			$data['totalhari']	= $totalhari;
			
			$pagination['base_url'] 	= '/liststokopname/cform/index/'.$uri1.'/'.$uri2.'/'.$tglopnamenya.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(7,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page'],$cari,$tglopnamenya,$uri2);
			
			$this->load->model('liststokopname/mclass');
			$data['isi']	= 'liststokopname/vlistform';
			$this->load->view('template',$data);
				
		}		
	}
}
?>
