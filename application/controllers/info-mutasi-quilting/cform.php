<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-mutasi-quilting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['isi'] = 'info-mutasi-quilting/vmainform';
	//$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$this->load->view('template',$data);

  }
  
  function view(){
    $data['isi'] = 'info-mutasi-quilting/vformview';
    $kode_brg = $this->input->post('kode_brg', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$brg_baku = $this->input->post('brg_baku', TRUE);  
	
	if ($date_from == '' && $date_to == '' && $kode_brg == '') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
		$kode_brg 	= $this->uri->segment(7);
	}

    $jum_total = $this->mmaster->get_mutasi_stoktanpalimit($date_from, $date_to, $kode_brg);
			$config['base_url'] = base_url().'index.php/info-mutasi-quilting/cform/view/index/'.$date_from.'/'.$date_to.'/'.$kode_brg;
			//$config['total_rows'] = $query->num_rows(); 
			$config['total_rows'] = count($jum_total); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_mutasi_stok($config['per_page'],$this->uri->segment(8), $date_from, $date_to, $kode_brg);
	$data['jum_total'] = count($jum_total);
	$data['kode_brg'] = $kode_brg;
	
	$data['brg_baku'] = $brg_baku;
				// ambil data satuan
				$query3	= $this->db->query(" SELECT c.nama as satuan 
					FROM tm_brg_hasil_makloon b, tm_satuan c 
					WHERE b.satuan = c.id AND b.kode_brg= '$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->satuan;
				}
				else {
					$satuan	= '';
				}
	$data['satuan'] = $satuan;
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$this->load->view('template',$data);
  }
  
  // popup bhn quilting
function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================

		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' ) {
			$keywordcari 	= $this->uri->segment(4);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
					  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari);
		
		$config['base_url'] = base_url()."index.php/info-mutasi-quilting/cform/show_popup_brg/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari);

	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['startnya'] = $config['cur_page'];

	$this->load->view('info-mutasi-quilting/vpopupbrg',$data);
  }
  
}
