<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-makloon/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================

	$data['isi'] = 'faktur-makloon/vmainform';
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['jenis_makloon'] = $this->mmaster->get_jenis_makloon(); 
	$data['msg'] = '';
	$this->load->view('template',$data);

  }
  
  function edit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$id_faktur 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_faktur($id_faktur);
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['jenis_makloon'] = $this->mmaster->get_jenis_makloon(); 
	$data['msg'] = '';
	$data['isi'] = 'faktur-makloon/veditform';
	$data['id_faktur'] = $id_faktur;
	$this->load->view('template',$data);

  }
  
  // updatedata
  function updatedata() {
			$id_faktur 	= $this->input->post('id_faktur', TRUE);
			
			$unit_makloon 	= $this->input->post('unit_makloon', TRUE);
			$jenis_makloon 	= $this->input->post('jenis_makloon', TRUE);
			$jum 	= $this->input->post('jum', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj_lama = $this->input->post('no_sj_lama', TRUE);
			$no_sj = trim($no_sj);
			$no_sj_lama = trim($no_sj_lama);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;
						
			$no_faktur_lama = $this->input->post('no_faktur_lama', TRUE);
			$jum = $this->input->post('jum', TRUE);
									
			$tgl = date("Y-m-d");			
			
			if ($no_fp != $no_faktur_lama) {
				$cek_data = $this->mmaster->cek_data($no_fp, $unit_makloon);
				if (count($cek_data) == 0) {
					$this->db->query(" UPDATE tm_faktur_makloon SET kode_unit = '$unit_makloon', 
									jenis_makloon = '$jenis_makloon', no_faktur = '$no_fp', 
									tgl_faktur = '$tgl_fp', tgl_update= '$tgl', jumlah = '$jum' where id= '$id_faktur' "); 
									
					// reset dulu status_faktur menjadi FALSE --------------------------------
					$list_sj_lama = explode(",", $no_sj_lama);
					foreach($list_sj_lama as $row2) {
						$row2 = trim($row2);
						if ($row2 != '') {
							/*$this->db->query(" UPDATE tm_pembelian SET status_faktur = 'f', no_faktur = '' 
							WHERE kode_supplier = '$supplier' AND no_sj = '$row2' "); */
							
							if ($jenis_makloon == 1)
								$nama_tabel = "tm_sj_hasil_makloon";
							else if ($jenis_makloon == 2)
								$nama_tabel = "tm_sj_hasil_bisbisan";
							else if ($jenis_makloon == 3)
								$nama_tabel = "tm_sj_hasil_bordir";
							else if ($jenis_makloon == 4)
								$nama_tabel = "tm_sj_hasil_print";
							else if ($jenis_makloon == 5)
								$nama_tabel = "tm_sj_hasil_asesoris";
								
							$this->db->query(" UPDATE ".$nama_tabel." SET status_faktur = 'f', no_faktur = '' 
									WHERE kode_unit = '$unit_makloon' AND no_sj = '$row2' ");
						}
					}
					//-------------------------------------------------------------------------
					
					$this->db->query(" DELETE FROM tm_faktur_makloon_sj where id_faktur_makloon = '$id_faktur' ");
					
					// insert tabel detail sj-nya
					$list_sj = explode(",", $no_sj); 
					foreach($list_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') { //echo $row1."<br>";
							$data_detail = array(
							  'id_faktur_makloon'=>$id_faktur,
							  'no_sj_masuk'=>$row1
							);
							$this->db->insert('tm_faktur_makloon_sj',$data_detail);
							
							// update status_faktur di tabel sj masuk makloon
							
							/*$this->db->query(" UPDATE tm_pembelian SET no_faktur = '$no_fp', status_faktur = 't' 
								WHERE no_sj = '$row1' AND kode_supplier = '$supplier' "); */
							
							if ($jenis_makloon == 1)
								$nama_tabel = "tm_sj_hasil_makloon";
							else if ($jenis_makloon == 2)
								$nama_tabel = "tm_sj_hasil_bisbisan";
							else if ($jenis_makloon == 3)
								$nama_tabel = "tm_sj_hasil_bordir";
							else if ($jenis_makloon == 4)
								$nama_tabel = "tm_sj_hasil_print";
							else if ($jenis_makloon == 5)
								$nama_tabel = "tm_sj_hasil_asesoris";
							
							$this->db->query(" UPDATE ".$nama_tabel." SET no_faktur = '$no_fp', status_faktur = 't' 
								WHERE no_sj = '$row1' AND kode_unit = '$unit_makloon' ");
							
						}
					}
				
					redirect('faktur-makloon/cform/view');
				}
				else {
					$data['query'] = $this->mmaster->get_faktur($id_faktur);
					$data['list_supplier'] = $this->mmaster->get_supplier(); 
					$data['jenis_makloon'] = $this->mmaster->get_jenis_makloon(); 
					$data['isi'] = 'faktur-makloon/veditform';
					$data['id_faktur'] = $id_faktur;
					$data['msg'] = "Update gagal. Data no faktur ".$no_fp." untuk unit makloon ".$supplier." sudah ada..!";
					$this->load->view('template',$data);
				}
			}
			else { // jika sama
				$this->db->query(" UPDATE tm_faktur_makloon SET kode_unit = '$unit_makloon', 
									jenis_makloon = '$jenis_makloon', no_faktur = '$no_fp', 
									tgl_faktur = '$tgl_fp', tgl_update= '$tgl', jumlah = '$jum' where id= '$id_faktur' "); 
				
					// reset dulu status_faktur menjadi FALSE --------------------------------
					$list_sj_lama = explode(",", $no_sj_lama);
					foreach($list_sj_lama as $row2) {
						$row2 = trim($row2);
						if ($row2 != '') {
							/*$this->db->query(" UPDATE tm_pembelian SET status_faktur = 'f', no_faktur = '' 
							WHERE kode_supplier = '$supplier' AND no_sj = '$row2' "); */
							
							if ($jenis_makloon == 1)
								$nama_tabel = "tm_sj_hasil_makloon";
							else if ($jenis_makloon == 2)
								$nama_tabel = "tm_sj_hasil_bisbisan";
							else if ($jenis_makloon == 3)
								$nama_tabel = "tm_sj_hasil_bordir";
							else if ($jenis_makloon == 4)
								$nama_tabel = "tm_sj_hasil_print";
							else if ($jenis_makloon == 5)
								$nama_tabel = "tm_sj_hasil_asesoris";
								
							$this->db->query(" UPDATE ".$nama_tabel." SET status_faktur = 'f', no_faktur = '' 
									WHERE kode_unit = '$unit_makloon' AND no_sj = '$row2' ");
						}
					}
					//-------------------------------------------------------------------------
				
					$this->db->query(" DELETE FROM tm_faktur_makloon_sj where id_faktur_makloon = '$id_faktur' ");
					
					// insert tabel detail sj-nya
					$list_sj = explode(",", $no_sj); 
					foreach($list_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') { //echo $row1."<br>";
							$data_detail = array(
							  'id_faktur_makloon'=>$id_faktur,
							  'no_sj_masuk'=>$row1
							);
							$this->db->insert('tm_faktur_makloon_sj',$data_detail);
							
							// update status_faktur di tabel sj masuk makloon
							
							/*$this->db->query(" UPDATE tm_pembelian SET no_faktur = '$no_fp', status_faktur = 't' 
								WHERE no_sj = '$row1' AND kode_supplier = '$supplier' "); */
							
							if ($jenis_makloon == 1)
								$nama_tabel = "tm_sj_hasil_makloon";
							else if ($jenis_makloon == 2)
								$nama_tabel = "tm_sj_hasil_bisbisan";
							else if ($jenis_makloon == 3)
								$nama_tabel = "tm_sj_hasil_bordir";
							else if ($jenis_makloon == 4)
								$nama_tabel = "tm_sj_hasil_print";
							else if ($jenis_makloon == 5)
								$nama_tabel = "tm_sj_hasil_asesoris";
							
							$this->db->query(" UPDATE ".$nama_tabel." SET no_faktur = '$no_fp', status_faktur = 't' 
								WHERE no_sj = '$row1' AND kode_unit = '$unit_makloon' ");
							
						}
					}
				
					redirect('faktur-makloon/cform/view');
			}	
  }
  
  function show_popup_makloon(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	// ini popup sj masuk quilting
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jnsaction = $this->uri->segment(4); 
	$csupplier = $this->uri->segment(5); 
	$jnsmakloon = $this->uri->segment(6); 
	$no_fakturnya = $this->uri->segment(7); 
	
	if ($jnsaction == '')
		$jnsaction = $this->input->post('jnsaction', TRUE); 
	if ($csupplier == '')
		$csupplier = $this->input->post('csupplier', TRUE);  
		
	if ($jnsmakloon == '')
		$jnsmakloon = $this->input->post('jnsmakloon', TRUE);  
	
	if ($no_fakturnya == '')
		$no_fakturnya = $this->input->post('no_fakturnya', TRUE); 
		
	if ($keywordcari == '' && ($csupplier == '' || $jnsaction == '' || $jnsmakloon == '' || $no_fakturnya == '') ) {
		$jnsaction 	= $this->uri->segment(4);
		$csupplier 	= $this->uri->segment(5);
		$jnsmakloon = $this->uri->segment(6); 
		$no_fakturnya 	= $this->uri->segment(7);
		$keywordcari 	= $this->uri->segment(8);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
	if ($jnsmakloon == '')
		$jnsmakloon = '0';
	
	$jum_total = $this->mmaster->get_sjmasukmakloontanpalimit($jnsaction, $jnsmakloon, $csupplier, $no_fakturnya, $keywordcari);
		/*		$config['base_url'] = base_url()."index.php/faktur-jual/cform/show_popup_pembelian/".$jnsaction."/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		*/
	//$data['query'] = $this->mmaster->get_pembelian($config['per_page'],$this->uri->segment(7), $jnsaction, $csupplier, $keywordcari);						
	$data['query'] = $this->mmaster->get_sjmasukmakloon($jnsaction, $jnsmakloon, $csupplier, $no_fakturnya, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$data['jnsaction'] = $jnsaction;
	$data['jnsmakloon'] = $jnsmakloon;
	$data['no_fakturnya'] = $no_fakturnya;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_supplier
				WHERE kode_supplier = '$csupplier' ");
				$hasilrow3 = $query3->row();
				$data['nama_supplier']	= $hasilrow3->nama;

	$this->load->view('faktur-makloon/vpopupsjmakloon',$data);

  }

  function submit(){
	//$this->load->library('form_validation');

			$unit_makloon 	= $this->input->post('unit_makloon', TRUE);
			$jenis_makloon 	= $this->input->post('jenis_makloon', TRUE);
			
			$jum 	= $this->input->post('jum', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj = trim($no_sj);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;

			$cek_data = $this->mmaster->cek_data($no_fp, $unit_makloon, $jenis_makloon);
			if (count($cek_data) > 0) { 
				$data['isi'] = 'faktur-makloon/vmainform';
				$data['list_supplier'] = $this->mmaster->get_supplier(); 
				$data['jenis_makloon'] = $this->mmaster->get_jenis_makloon(); 
				$data['msg'] = "Data no faktur ".$no_fp." dari unit makloon ".$unit_makloon." sudah ada..!";
				$this->load->view('template',$data);
			}
			else {
				$this->mmaster->save($no_fp, $tgl_fp, $unit_makloon, $jenis_makloon, $jum, $no_sj);
				redirect('faktur-makloon/cform/view');
			}
  }
  
  function view(){
    $data['isi'] = 'faktur-makloon/vformview';
    $keywordcari = "all";
    $csupplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-makloon/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);		
				
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
		
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-makloon/cform/cari/'.$csupplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'faktur-makloon/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('faktur-makloon/cform/view');
  }
}
