<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-hrg-quilting/mmaster');
  }
  
  // function ini utk keperluan ekspor data yg disebabkan lupa masukin skrip insert ke tt_harga pada saat save
  /*function eksporharga() {
	  $tgl = date("Y-m-d");
	  $sql = " select * from tm_harga_brg_supplier "; 
	  $query	= $this->db->query($sql);    
	  $i = 0;
	
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$kode_brg = $row1->kode_brg;
			$kode_supplier = $row1->kode_supplier;
			$harga = $row1->harga;
			
			// cek di tt_harga apa udh ada data harga. kalo blm maka insert
			$query3	= $this->db->query(" SELECT id FROM tt_harga WHERE kode_brg = '$kode_brg'
									AND kode_supplier = '$kode_supplier' ");
			if ($query3->num_rows() == 0){
				$this->db->query(" INSERT INTO tt_harga (kode_brg, kode_supplier, harga, tgl_input) 
						VALUES ('$kode_brg','$kode_supplier', '$harga', '$tgl') ");
				$i++;
			}
		}
		echo "sukses ".$i." data";
	}
  } */

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['msg'] = '';
	$data['go_proses'] = '';
	$data['supplier'] = $this->mmaster->get_supplier();
    $data['isi'] = 'mst-hrg-quilting/vmainform';
    
	$this->load->view('template',$data);

  }
  
  function edit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $idharga 	= $this->uri->segment(4);  
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $esupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($idharga == '')
		$idharga 	= $this->input->post('idharga', TRUE);
    
    if ($go_edit == '') {
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['esupplier'] = $esupplier;
		$data['carinya'] = $carinya;
		
		$data['query'] = $this->mmaster->get_harga($idharga);
		$data['isi'] = 'mst-hrg-quilting/veditform';
		$data['idharga'] = $idharga;
		$this->load->view('template',$data);
	}
	else { // update data harga
		$harga 	= $this->input->post('harga', TRUE);
		$harga_lama 	= $this->input->post('harga_lama', TRUE);
		$id_brg 	= $this->input->post('id_brg', TRUE);
		$id_supplier 	= $this->input->post('id_supplier', TRUE);
		
		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$esupplier = $this->input->post('esupplier', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		
		$tgl = date("Y-m-d H:i:s");
		
		if ($harga != $harga_lama) {
			$this->db->query(" UPDATE tm_harga_quilting SET harga = '$harga', tgl_update = '$tgl' WHERE id = '$idharga' ");    
			
			/*$this->db->query(" INSERT INTO tt_harga (kode_brg, kode_supplier, harga, tgl_input) 
											VALUES ('".$kode_brg."','$kode_supplier', '$harga', '$tgl') "); */
		}
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "mst-hrg-quilting/cform/view/index/".$cur_page;
		else
			$url_redirectnya = "mst-hrg-quilting/cform/cari/index/".$esupplier."/".$carinya."/".$cur_page;
		
		redirect($url_redirectnya);
		//redirect('mst-bb-sup/cform/view');
	}
  }
  
  function proseslistbrg() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$supplier 	= $this->input->post('supplier', TRUE);
		
		$data['list_brg'] = $this->mmaster->get_barang($supplier);
		$data['msg'] = '';	
		$data['jum'] = count($data['list_brg']);	
		//nm supplier
		$query	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$supplier' ");    
		$hasilrow = $query->row();
		$kode_sup	= $hasilrow->kode_supplier;
		$nama_sup	= $hasilrow->nama;
				
		$data['isi'] = 'mst-hrg-quilting/vmainform';
		$data['go_proses'] = '1';
		
		$data['id_supplier'] = $supplier;
		$data['kode_supplier'] = $kode_sup;
		$data['nama_supplier'] = $nama_sup;		
		$this->load->view('template',$data);  
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$jum = $this->input->post('jum', TRUE);
		$id_supplier = $this->input->post('id_supplier', TRUE);  

		for ($i=1;$i<=$jum;$i++)
		{
			if ($this->input->post('cek_'.$i, TRUE) == 'y') {
				// cek apakah datanya udh ada
				$cek_data = $this->mmaster->cek_data($id_supplier, $this->input->post('id_brg_'.$i, TRUE));
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id_supplier, $this->input->post('id_brg_'.$i, TRUE), 
								$this->input->post('harga_'.$i, TRUE) );
				}
				else {
					if ($this->input->post('harga_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0)
						$this->db->query(" UPDATE tm_harga_quilting SET harga = '".$this->input->post('harga_'.$i, TRUE)."'
								WHERE id_unit = '$id_supplier' AND id_brg_quilting = '".$this->input->post('id_brg_'.$i, TRUE)."' ");
				}
			}
		}
		redirect('mst-hrg-quilting/cform');
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'mst-hrg-quilting/vformview';
    $keywordcari = "all";
    $supplier = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $supplier);	
							$config['base_url'] = base_url().'index.php/mst-hrg-quilting/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $supplier);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['esupplier'] = $supplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$supplier 	= $this->input->post('supplier', TRUE);  

	if ($keywordcari == '' && $supplier == '') {
		$supplier 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($supplier == '')
		$supplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $supplier);
							$config['base_url'] = base_url().'index.php/mst-hrg-quilting/cform/cari/index/'.$supplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $supplier);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'mst-hrg-quilting/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['esupplier'] = $supplier;
	$this->load->view('template',$data);
  }
  
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg 	= $this->uri->segment(4);
	if ($kel_brg == '')
		$kel_brg = $this->input->post('kel_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $kel_brg == '') {
		$kel_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barangtanpalimit($kel_brg, $keywordcari);
					$config['base_url'] = base_url()."index.php/mst-bb-sup/cform/show_popup_jenis/".$kel_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang($config['per_page'],$this->uri->segment(6), $kel_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['kel_brg'] = $kel_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('mst-bb-sup/vpopupjenis',$data);

  }
  
  function show_popup_jenis_bhn(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jns_brg 	= $this->uri->segment(4);
	if ($jns_brg == '')
		$jns_brg = $this->input->post('jns_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $jns_brg == '') {
		$jns_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_bahantanpalimit($jns_brg, $keywordcari);
							$config['base_url'] = base_url()."index.php/mst-bb-sup/cform/show_popup_jenis_bhn/".$jns_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_bahan($config['per_page'],$this->uri->segment(6), $jns_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['jns_brg'] = $jns_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		$hasilrow = $query3->row();
		$nama_jns_brg	= $hasilrow->nama;
	$data['nama_jns_brg'] = $nama_jns_brg;

	$this->load->view('mst-bb-sup/vpopupjenisbhn',$data);

  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $esupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "mst-hrg-quilting/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-hrg-quilting/cform/cari/index/".$esupplier."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('mst-bb-sup/cform/view');
  }
  
  
}
