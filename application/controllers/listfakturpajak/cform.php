<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('listfakturpajak/mclass');
	}
	
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
	$data['isi']	= 'listfakturpajak/vmainform';
			$this->load->view('template',$data);
			
		
	}
	
	function view() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
		
		$i_faktur_code	= $this->input->post('i_faktur_code');
		$d_first	= $this->input->post('d_first');
		$d_last	= $this->input->post('d_last');
		
		$data['d_first']	= $d_first;
		$data['d_last']	= $d_last;
		
		$e_d_first	= explode("/",$d_first,strlen($d_first));
		$e_d_last	= explode("/",$d_last,strlen($d_last));
		
		$n_d_first	= !empty($e_d_first[2])?$e_d_first[2].'-'.$e_d_first[1].'-'.$e_d_first[0]:'0';
		$n_d_last	= !empty($e_d_last[2])?$e_d_last[2].'-'.$e_d_last[1].'-'.$e_d_last[0]:'0';
		
		$data['iproduct']	= $i_faktur_code;
		$data['tdfirst']	= $n_d_first;
		$data['tdlast']	= $n_d_last;
		$data['query']	= $this->mclass->getnomorfakturpajak($i_faktur_code,$n_d_first,$n_d_last);
		//print_r($data['isi']); die();
		$data['isi']	= 'listfakturpajak/vlistform';
			$this->load->view('template',$data);
		
		
	}
	
	
	function show_popup_faktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "DAFTAR NOMOR FAKTUR PENJUALAN";
		
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->getfakturpenjualan();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listfakturpajak/cform/show_popup_faktur/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$isi= $this->mclass->getfakturpenjualanperpages($pagination['per_page'],$pagination['cur_page']);
	
	$data['isi']=$isi;
	
		$this->load->view('listfakturpajak/vlistfakturpenjualan',$data);	
	}
	
	function fshow_popup_faktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "DAFTAR NOMOR FAKTUR PENJUALAN";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		if(!empty($key)) {
			$query	= $this->mclass->fgetfakturpenjualan($key);
			$jml	= $query->num_rows();
		}else{
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('".trim($row->i_faktur_code)."')\">".$row->i_faktur_code."</a></td>	 
				  <td><a href=\"javascript:settextfield('".trim($row->i_faktur_code)."')\">".$row->d_faktur."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}
	

	
}
?>
