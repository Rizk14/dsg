<?php
class master_motif_bb extends CI_Controller
{
    public $data = array(
        'halaman' => 'master_motif_bb',        
        'title' => 'Master motif BB',
        'isi' => 'master_motif_bb/master_motif_bb_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_motif_bb/master_motif_bb_model', 'master_motif_bb');
    }

   
    public function index()
    {
		
        $this->data['values'] = (object) $this->master_motif_bb->default_values;
        $this->data['list_data_wip']=$this->master_motif_bb->master_motif_bb();
		$this->load->view('template', $this->data);
			
    }
     public function sukses_input()
    {	
		
        $this->data['isi'] = 'master_motif_bb/master_motif_bb-sukses';
        $this->load->view('template', $this->data);
    }

    // Jika pendaftaran error, tampilkan informasi mengenai error.
    public function error_input()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Penginputan Master motif BB Error';
        $this->load->view('template', $this->data);
    }
   public function submit()
    {
		
		$no=$this->input->post('no',TRUE);
        
       
		 $jumlah_input=$no-1;
		// print_r($jumlah_input);
		 for($i=1; $i<=$jumlah_input; $i++){
		$this->master_motif_bb->input(	
		$this->input->post('kode_motif_bb_m_'.$i,TRUE),
		$this->input->post('nama_motif_bb_'.$i,TRUE)
		);
		
		}
		if(true)
		redirect('master_motif_bb/master_motif_bb/sukses_input');
		else 
		redirect('master_motif_bb/master_motif_bb/error_input');
    }
    
    
   public function view($offset= null)
    {	
		
       $master_motif_bb = $this->master_motif_bb->master_motif_bb($offset);
        if ($master_motif_bb) {
            $this->data['master_motif_bb'] = $master_motif_bb;
            $this->data['paging'] = $this->master_motif_bb->paging('biasa', site_url('master_motif_bb/master_motif_bb/halaman/'), 4);
        } else {
            $this->data['master_motif_bb'] = 'Tidak ada data Master motif BB, Silahkan Melakukan '.anchor('/master_motif_bb/master_motif_bb/', 'Proses penginputan.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('master_motif_bb/master_motif_bb/cari');
        $this->data['isi'] = 'master_motif_bb/master_motif_bb_list';
        $this->load->view('template', $this->data);
    }
    public function cari($offset = 0)
    {
        $master_motif_bb = $this->master_motif_bb->cari($offset);
        if ($master_motif_bb) {
            $this->data['master_motif_bb'] = $master_motif_bb;
            $this->data['paging'] = $this->master_motif_bb->paging('pencarian', site_url('/master_motif_bb/master_motif_bb/cari/'), 4);
        } else {
            $this->data['master_motif_bb'] = 'Data tidak ditemukan.'. anchor('/master_motif_bb/master_motif_bb/view', ' Tampilkan semua Master motif BB.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('/master_motif_bb/master_motif_bb/cari');
        $this->data['isi'] = 'master_motif_bb/master_motif_bb_list';
        $this->load->view('template', $this->data);
    }
    
   
    
     public function hapus($id)
    {
        
        //~ if ($this->session->userdata('user_bagian') != '2') {
            //~ $this->session->set_flashdata('pesan_error', 'Anda tidak berhak menghapus data Master motif BB. Kembali ke halaman ' . anchor('master_motif_bb/master_motif_bb', 'master_motif_bb.', 'class="alert-link"'));
            //~ redirect('master_motif_bb/master_motif_bb/error');
        //~ }

      
        if (! $this->master_motif_bb->get($id)) {
            $this->session->set_flashdata('pesan_error', 'Data Master motif BB tidak ada. Kembali ke halaman ' . anchor('master_motif_bb/master_motif_bb', 'master_motif_bb.', 'class="alert-link"'));
            redirect('master_motif_bb/master_motif_bb/error');
        }

        // Hapus
        if ($this->master_motif_bb->delete($id)) {
            $this->session->set_flashdata('pesan', 'Data berhasil dihapus. Kembali ke halaman '. anchor('master_motif_bb/master_motif_bb/view', 'View Master motif BB.', 'class="alert-link"'));
            redirect('master_motif_bb/master_motif_bb/sukses');
        } else {
            $this->session->set_flashdata('pesan_error', 'Data gagal dihapus. Kembali ke halaman '. anchor('master_motif_bb/master_motif_bb/view', 'View Master motif BB.', 'class="alert-link"'));
            redirect('master_motif_bb/master_motif_bb/error');
        }
    }
     public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }
    
    public function edit()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $master_motif_bb = $this->master_motif_bb->get($id);
        if (! $master_motif_bb) {
            $this->session->set_flashdata('pesan_error', 'Data Master motif BB tidak ada. Kembali ke halaman ' . anchor('master_motif_bb/master-motif/view', 'Master motif BB.', 'class="alert-link"'));
            redirect('master_motif_bb/master-motif/error');
        }

        // Data untuk form.
        if (!$_POST) {
            $data = (object) $master_motif_bb;     
        } else {
            $data = (object) $this->input->post(null, true);
        }
        $this->data['values'] = $data;
        
		$this->data['isi'] = 'master_motif_bb/master_motif_bb_form_edit';
        $this->load->view('template', $this->data);
	 }
	    public function updatedata(){
			$id =  $this->input->post('id',TRUE);
	        $nama_motif_bb =  $this->input->post('nama_motif_bb',TRUE);
			$kode_motif_bb =  $this->input->post('kode_motif_bb',TRUE);
	    
	     if (! $this->master_motif_bb->validate('form_rules')) {
            $this->data['isi'] = 'master_motif_bb/master_motif_bb_form_edit';
            $this->data['form_action'] = site_url('master_motif_bb/master_motif_bb/view');
            return;     
		}
    	 
		if ($this->master_motif_bb->edit($id,$nama_motif_bb,$kode_motif_bb)) {
            $this->session->set_flashdata('pesan', 'Data berhasil diupdate. Kembali ke halaman ' . anchor('master_motif_bb/master_motif_bb/view', 'Master motif BB.', 'class="alert-link"'));
            redirect('master_motif_bb/master_motif_bb/sukses');
        } else {
            $this->session->set_flashdata('pesan_error', 'Data tidak berhasil diupdate. Kembali ke halaman ' . anchor('master_motif_bb/master_motif_bb/view', 'Master motif BB.', 'class="alert-link"'));
            redirect('/master_motif_bb/master_motif_bb/error');
	}
}
     
}

