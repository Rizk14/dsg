<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();			
	}
	
	function index(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
		$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
		$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
		$data['form_cari_pelanggan']	= $this->lang->line('form_cari_pelanggan');
		$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
		$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
		$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
		$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');
		$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
		$data['form_alamat_pelanggan']	= $this->lang->line('form_alamat_pelanggan');
		$data['form_pkp_pelanggan']	= $this->lang->line('form_pkp_pelanggan');
		$data['form_npwp_pelanggan']	= $this->lang->line('form_npwp_pelanggan');
		$data['form_top_pelanggan']	= $this->lang->line('form_top_pelanggan');
		$data['form_konsinyasi_pelanggan'] = $this->lang->line('form_konsinyasi_pelanggan');
		$data['form_telpon_pelanggan']	= $this->lang->line('form_telpon_pelanggan');
		$data['form_fax_pelanggan']	= $this->lang->line('form_fax_pelanggan');
		$data['form_telp_fax_pelanggan']	= $this->lang->line('form_telp_fax_pelanggan');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclass');
		
		$list	= "";
		$q_grp_pel	= $this->mclass->listgrouppelanggan();
		foreach($q_grp_pel as $row_grp_pel) {
			$list.="<option value=".$row_grp_pel->i_group.">".$row_grp_pel->e_group_name."</option>";
		}
		$data['opt_grp_pel']	= $list;
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
			$pagination['base_url'] 	=   base_url()."index.php/pelanggan/cform/pagesnext/";
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'pelanggan/vform';
		$this->load->view('template',$data);
		
	}
function index_cari(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
		$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
		$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
		$data['form_cari_pelanggan']	= $this->lang->line('form_cari_pelanggan');
		$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
		$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
		$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
		$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');
		$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
		$data['form_alamat_pelanggan']	= $this->lang->line('form_alamat_pelanggan');
		$data['form_pkp_pelanggan']	= $this->lang->line('form_pkp_pelanggan');
		$data['form_npwp_pelanggan']	= $this->lang->line('form_npwp_pelanggan');
		$data['form_top_pelanggan']	= $this->lang->line('form_top_pelanggan');
		$data['form_konsinyasi_pelanggan'] = $this->lang->line('form_konsinyasi_pelanggan');
		$data['form_telpon_pelanggan']	= $this->lang->line('form_telpon_pelanggan');
		$data['form_fax_pelanggan']	= $this->lang->line('form_fax_pelanggan');
		$data['form_telp_fax_pelanggan']	= $this->lang->line('form_telp_fax_pelanggan');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclass');
		
		$list	= "";
		$q_grp_pel	= $this->mclass->listgrouppelanggan();
		foreach($q_grp_pel as $row_grp_pel) {
			$list.="<option value=".$row_grp_pel->i_group.">".$row_grp_pel->e_group_name."</option>";
		}
		$data['opt_grp_pel']	= $list;
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
			$pagination['base_url'] 	=   base_url()."index.php/pelanggan/cform/pagesnext/";
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'pelanggan/vformcari';
		$this->load->view('template',$data);
		
	}
function tambah(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
		$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
		$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
		$data['form_cari_pelanggan']	= $this->lang->line('form_cari_pelanggan');
		$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
		$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
		$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
		$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');
		$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
		$data['form_alamat_pelanggan']	= $this->lang->line('form_alamat_pelanggan');
		$data['form_pkp_pelanggan']	= $this->lang->line('form_pkp_pelanggan');
		$data['form_npwp_pelanggan']	= $this->lang->line('form_npwp_pelanggan');
		$data['form_top_pelanggan']	= $this->lang->line('form_top_pelanggan');
		$data['form_konsinyasi_pelanggan'] = $this->lang->line('form_konsinyasi_pelanggan');
		$data['form_telpon_pelanggan']	= $this->lang->line('form_telpon_pelanggan');
		$data['form_fax_pelanggan']	= $this->lang->line('form_fax_pelanggan');
		$data['form_telp_fax_pelanggan']	= $this->lang->line('form_telp_fax_pelanggan');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclass');
		
		$list	= "";
		$q_grp_pel	= $this->mclass->listgrouppelanggan();
		foreach($q_grp_pel as $row_grp_pel) {
			$list.="<option value=".$row_grp_pel->i_group.">".$row_grp_pel->e_group_name."</option>";
		}
		$data['opt_grp_pel']	= $list;
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
			$pagination['base_url'] 	=   base_url()."index.php/pelanggan/cform/pagesnext/";
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'pelanggan/vmainformadd';
		$this->load->view('template',$data);
		
	}
	function carikodecustomers() {
		$kodecustomer	= $this->input->post('kodecustomer')?$this->input->post('kodecustomer'):$this->input->get_post('kodecustomer');
		
		$kodecustomer	= strtoupper($kodecustomer);
		
		$this->load->model('pelanggan/mclass');
		
		$qkode	= $this->mclass->caricustomer($kodecustomer);
		if($qkode->num_rows()>0) {
			echo "Maaf, Kode Pelanggan sdh ada!";
		}		
	}

	function pagesnext(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
		$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
		$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
		$data['form_cari_pelanggan']	= $this->lang->line('form_cari_pelanggan');
		$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
		$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
		$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
		$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');		
		$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
		$data['form_alamat_pelanggan']	= $this->lang->line('form_alamat_pelanggan');
		$data['form_pkp_pelanggan']	= $this->lang->line('form_pkp_pelanggan');
		$data['form_npwp_pelanggan']	= $this->lang->line('form_npwp_pelanggan');
		$data['form_top_pelanggan']	= $this->lang->line('form_top_pelanggan');
		$data['form_konsinyasi_pelanggan'] = $this->lang->line('form_konsinyasi_pelanggan');
		$data['form_telpon_pelanggan']	= $this->lang->line('form_telpon_pelanggan');
		$data['form_fax_pelanggan']	= $this->lang->line('form_fax_pelanggan');
		$data['form_telp_fax_pelanggan']	= $this->lang->line('form_telp_fax_pelanggan');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclass');

		$list	= "";
		$q_grp_pel	= $this->mclass->listgrouppelanggan();
		foreach($q_grp_pel as $row_grp_pel) {
			$list.="<option value=".$row_grp_pel->i_group.">".$row_grp_pel->e_group_name."</option>";
		}
		$data['opt_grp_pel']	= $list;
				
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
	
		$pagination['base_url'] 	=   base_url()."index.php/pelanggan/cform/pagesnext/";
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'pelanggan/vform';
		$this->load->view('template',$data);
	}
	
	function detail(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}	
		$this->load->view('pelanggan/vform',$data);
	}

	function simpan(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$i_customer_code	= @$this->input->post('i_customer_code')?@$this->input->post('i_customer_code'):@$this->input->get_post('i_customer_code');
		$e_customer_name	= @$this->input->post('e_customer_name')?@$this->input->post('e_customer_name'):@$this->input->get_post('e_customer_name');
		$e_customer_address	= @$this->input->post('e_customer_address')?@$this->input->post('e_customer_address'):@$this->input->get_post('e_customer_address');
		$f_customer_pkp2	= @$this->input->post('f_customer_pkp')?@$this->input->post('f_customer_pkp'):@$this->input->get_post('f_customer_pkp');
		$e_customer_npwp	= @$this->input->post('e_customer_npwp')?@$this->input->post('e_customer_npwp'):@$this->input->get_post('e_customer_npwp');
		$n_customer_top	= @$this->input->post('n_customer_top')?@$this->input->post('n_customer_top'):@$this->input->get_post('n_customer_top');
		$f_customer_konsinyasi2	= @$this->input->post('f_customer_konsinyasi')?@$this->input->post('f_customer_konsinyasi'):@$this->input->get_post('f_customer_konsinyasi');
		$e_customer_phone	= @$this->input->post('e_customer_phone')?@$this->input->post('e_customer_phone'):@$this->input->get_post('e_customer_phone');
		$e_customer_fax	= @$this->input->post('e_customer_fax')?@$this->input->post('e_customer_fax'):@$this->input->get_post('e_customer_fax');
		$e_customer_contact 	= @$this->input->post('e_customer_contact')?@$this->input->post('e_customer_contact'):@$this->input->get_post('e_customer_contact');
		$igroup 	= @$this->input->post('igroup')?@$this->input->post('igroup'):@$this->input->get_post('igroup');
		/*
		$fcustomerkonsinyasi	= $f_customer_konsinyasi2==1?'TRUE':'FALSE';
		$fcustomerpkp	= $f_customer_pkp2==1?'TRUE':'FALSE';
		*/
		if( !empty($i_customer_code) && 
		    !empty($e_customer_name) && 
			!empty($igroup) ) {
			$this->load->model('pelanggan/mclass');
			
			$q_ckpelanggan	= $this->mclass->ckpelanggan($i_customer_code);
			if($q_ckpelanggan->num_rows()>0) {
				$ada=1;
			} else {
				$ada=0;
			}
			if($ada==0) {
				$this->mclass->msimpan($i_customer_code,$e_customer_name,$e_customer_address,$f_customer_pkp2,$e_customer_npwp,$n_customer_top,$f_customer_konsinyasi2,$e_customer_phone,$e_customer_fax,$e_customer_contact,$igroup);
			} else {
				print "<script>alert(\"Maaf, Kode Pelanggan tsb sudah ada!\");show(\"pelanggan/cform\",\"#content\");</script>";
			}
		} else {
			$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
			$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
			$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
			$data['form_cari_pelanggan']	= $this->lang->line('form_cari_pelanggan');
			$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
			$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
			$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
			$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');			
			$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
			$data['form_alamat_pelanggan']	= $this->lang->line('form_alamat_pelanggan');
			$data['form_pkp_pelanggan']	= $this->lang->line('form_pkp_pelanggan');
			$data['form_npwp_pelanggan']	= $this->lang->line('form_npwp_pelanggan');
			$data['form_top_pelanggan']	= $this->lang->line('form_top_pelanggan');
			$data['form_konsinyasi_pelanggan'] = $this->lang->line('form_konsinyasi_pelanggan');
			$data['form_telpon_pelanggan']	= $this->lang->line('form_telpon_pelanggan');
			$data['form_fax_pelanggan']	= $this->lang->line('form_fax_pelanggan');
			$data['form_telp_fax_pelanggan']	= $this->lang->line('form_telp_fax_pelanggan');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['link_aksi']	= $this->lang->line('link_aksi');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
	
			$this->load->model('pelanggan/mclass');

			$list	= "";
			$q_grp_pel	= $this->mclass->listgrouppelanggan();
			foreach($q_grp_pel as $row_grp_pel) {
				$list.="<option value=".$row_grp_pel->i_group.">".$row_grp_pel->e_group_name."</option>";
			}
			$data['opt_grp_pel']	= $list;
					
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= '/pelanggan/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['isi']	='pelanggan/vmainform';
			$this->load->view('template',$data);
		}
	}
	
	function edit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$id	= $this->uri->segment(4);
		$data['id']	= $id;

		$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
		$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
		$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
		$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
		$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
		$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
		$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');		
		$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
		$data['form_alamat_pelanggan']	= $this->lang->line('form_alamat_pelanggan');
		$data['form_pkp_pelanggan']	= $this->lang->line('form_pkp_pelanggan');
		$data['form_npwp_pelanggan']	= $this->lang->line('form_npwp_pelanggan');
		$data['form_top_pelanggan']	= $this->lang->line('form_top_pelanggan');
		$data['form_konsinyasi_pelanggan'] = $this->lang->line('form_konsinyasi_pelanggan');
		$data['form_telpon_pelanggan']	= $this->lang->line('form_telpon_pelanggan');
		$data['form_fax_pelanggan']	= $this->lang->line('form_fax_pelanggan');
		$data['form_telp_fax_pelanggan']	= $this->lang->line('form_telp_fax_pelanggan');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclass');
				
		$qry_brg_pel	= $this->mclass->medit($id);
		
		if($qry_brg_pel->num_rows() > 0) {
		
			$arr	= $qry_brg_pel->row_array();
			
			$data['icustomer']		= $arr['i_customer'];
			$data['icustomercode']	= $arr['i_customer_code'];
			$data['ecustomername']	= $arr['e_customer_name'];
			$data['ecustomeraddress']	= $arr['e_customer_address'];
			$fcustomerpkp	= ($arr['f_customer_pkp']==true)?"checked":"";
			$data['fcustpkp']	= $fcustomerpkp;
			$data['ecustomernpwp']	= $arr['e_customer_npwp'];
			$data['ncustomertop']		= $arr['n_customer_top'];
			$fcustomerkonsinyasi		= ($arr['f_customer_konsinyasi']==true)?"checked":"";
			$data['fcustkonsinyasi']	= $fcustomerkonsinyasi;
			$data['ecustomerphone']	= $arr['e_customer_phone'];
			$data['ecustomerfax']		= $arr['e_customer_fax'];
			$data['ecustomercontact']	= $arr['e_customer_contact'];
			$igrouppel	= $arr['i_group_code'];

			$list	= "";
			$q_grp_pel	= $this->mclass->listgrouppelanggan();
			foreach($q_grp_pel as $row_grp_pel) {
				$sel = $igrouppel==$row_grp_pel->i_group?"selected":"";
				$list.="<option value=".$row_grp_pel->i_group." ".$sel.">".$row_grp_pel->e_group_name."</option>";
			}
			$data['opt_grp_pel']	= $list;
			$data['isi']	='pelanggan/veditform';
			$this->load->view('template',$data);
		
		}
	}
	
	function actedit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$i_customer_code	= @$this->input->post('i_customer_code')?@$this->input->post('i_customer_code'):@$this->input->get_post('i_customer_code');
		$i_customer_code2	= @$this->input->post('i_customer_code2')?@$this->input->post('i_customer_code2'):@$this->input->get_post('i_customer_code2');
		$i_customer2	= @$this->input->post('i_customer2')?@$this->input->post('i_customer2'):@$this->input->get_post('i_customer2');		
		$e_customer_name	= @$this->input->post('e_customer_name')?@$this->input->post('e_customer_name'):@$this->input->get_post('e_customer_name');
		$e_customer_address	= @$this->input->post('e_customer_address')?@$this->input->post('e_customer_address'):@$this->input->get_post('e_customer_address');
		$f_customer_pkp2	= @$this->input->post('f_customer_pkp')?@$this->input->post('f_customer_pkp'):@$this->input->get_post('f_customer_pkp');
		$e_customer_npwp	= @$this->input->post('e_customer_npwp')?@$this->input->post('e_customer_npwp'):@$this->input->get_post('e_customer_npwp');
		$n_customer_top	= @$this->input->post('n_customer_top')?@$this->input->post('n_customer_top'):@$this->input->get_post('n_customer_top');
		$f_customer_konsinyasi2	= @$this->input->post('f_customer_konsinyasi')?@$this->input->post('f_customer_konsinyasi'):@$this->input->get_post('f_customer_konsinyasi');
		$e_customer_phone	= @$this->input->post('e_customer_phone')?@$this->input->post('e_customer_phone'):@$this->input->get_post('e_customer_phone');
		$e_customer_fax	= @$this->input->post('e_customer_fax')?@$this->input->post('e_customer_fax'):@$this->input->get_post('e_customer_fax');
		$e_customer_contact 	= @$this->input->post('e_customer_contact')?@$this->input->post('e_customer_contact'):@$this->input->get_post('e_customer_contact');
		$igroup 	= @$this->input->post('igroup')?@$this->input->post('igroup'):@$this->input->get_post('igroup');
		
		if( !empty($i_customer_code) && 
		    !empty($e_customer_name) &&
			!empty($igroup) ) {
			$this->load->model('pelanggan/mclass');
			$this->mclass->mupdate($i_customer_code,$e_customer_name,$e_customer_address,$f_customer_pkp2,$e_customer_npwp,$n_customer_top,$f_customer_konsinyasi2,$e_customer_phone,$e_customer_fax,$e_customer_contact,$i_customer2,$i_customer_code2,$igroup);
		} else {
			$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
			$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
			$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
			$data['form_cari_pelanggan']	= $this->lang->line('form_cari_pelanggan');
			$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
			$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
			$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
			$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');			
			$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
			$data['form_alamat_pelanggan']	= $this->lang->line('form_alamat_pelanggan');
			$data['form_pkp_pelanggan']	= $this->lang->line('form_pkp_pelanggan');
			$data['form_npwp_pelanggan']	= $this->lang->line('form_npwp_pelanggan');
			$data['form_top_pelanggan']	= $this->lang->line('form_top_pelanggan');
			$data['form_konsinyasi_pelanggan'] = $this->lang->line('form_konsinyasi_pelanggan');
			$data['form_telpon_pelanggan']	= $this->lang->line('form_telpon_pelanggan');
			$data['form_fax_pelanggan']	= $this->lang->line('form_fax_pelanggan');
			$data['form_telp_fax_pelanggan']	= $this->lang->line('form_telp_fax_pelanggan');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['link_aksi']	= $this->lang->line('link_aksi');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
	
			$this->load->model('pelanggan/mclass');

			$list	= "";
			$q_grp_pel	= $this->mclass->listgrouppelanggan();
			foreach($q_grp_pel as $row_grp_pel) {
				$list.="<option value=".$row_grp_pel->i_group.">".$row_grp_pel->e_group_name."</option>";
			}
			$data['opt_grp_pel']	= $list;
					
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= '/pelanggan/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);

			$this->load->view('pelanggan/vmainform',$data);
		}		
	}
	/* 20052011
	function actdelete() {
		$id			= $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_customer',array('i_customer'=>$id));
		redirect('pelanggan/cform/');
	}
	*/

	function actdelete(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('pelanggan/mclass');
		$this->mclass->delete($id);
	}
		
	function cari(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
		$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
		$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
		$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
		$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
		$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
		$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');		
		$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
	
		$txt_i_customer_code	= $this->input->post('txt_i_customer_code')?$this->input->post('txt_i_customer_code'):$this->input->get_post('txt_i_customer_code');
		$txt_e_customer_name	= $this->input->post('txt_e_customer_name')?$this->input->post('txt_e_customer_name'):$this->input->get_post('txt_e_customer_name');
		$txt_e_cust_phonefax	= $this->input->post('txt_e_cust_phonefax')?$this->input->post('txt_e_cust_phonefax'):$this->input->get_post('txt_e_cust_phonefax');
		
		$this->load->model('pelanggan/mclass');

		$query	= $this->mclass->viewcari($txt_i_customer_code,$txt_e_customer_name,$txt_e_cust_phonefax);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= '/pelanggan/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
	
		$data['query']	= $this->mclass->mcari($txt_i_customer_code,$txt_e_customer_name,$txt_e_cust_phonefax,$pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'pelanggan/vcariform';
		$this->load->view('template',$data);
	}

	function carinext(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_pelanggan']	= $this->lang->line('page_title_pelanggan');
		$data['form_panel_daftar_pelanggan']	= $this->lang->line('form_panel_daftar_pelanggan');
		$data['form_form_pelanggan']	= $this->lang->line('form_form_pelanggan');
		$data['form_kode_pelanggan']	= $this->lang->line('form_kode_pelanggan');
		$data['form_nm_pelanggan']	= $this->lang->line('form_nm_pelanggan');
		$data['form_nm_group_pelanggan']	= $this->lang->line('form_nm_group_pelanggan');
		$data['form_option_group_pelanggan'] = $this->lang->line('form_option_group_pelanggan');		
		$data['form_kontak_pelanggan']	= $this->lang->line('form_kontak_pelanggan');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
			
		$txt_i_customer_code	= $this->input->post('txt_i_customer_code')?$this->input->post('txt_i_customer_code'):$this->input->get_post('txt_i_customer_code');
		$txt_e_customer_name	= $this->input->post('txt_e_customer_name')?$this->input->post('txt_e_customer_name'):$this->input->get_post('txt_e_customer_name');
		$txt_e_cust_phonefax	= $this->input->post('txt_e_cust_phonefax')?$this->input->post('txt_e_cust_phonefax'):$this->input->get_post('txt_e_cust_phonefax');
		
		$this->load->model('pelanggan/mclass');

		$query	= $this->mclass->viewcari($txt_i_customer_code,$txt_e_customer_name,$txt_e_cust_phonefax);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= '/pelanggan/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
	
		$data['isi']	= $this->mclass->mcari($txt_i_customer_code,$txt_e_customer_name,$txt_e_cust_phonefax,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('pelanggan/vcariform',$data);
	}	
}
?>
