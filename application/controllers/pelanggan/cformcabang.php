<?php
class Cformcabang extends CI_Controller {

	function Cformcabang() {
		parent::__construct();			
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
		$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
		$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
		$data['form_panel_cari_pelcab']	= $this->lang->line('form_panel_cari_pelcab');
		$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
		$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
		$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
		$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
		$data['form_inisial_cab'] = $this->lang->line('form_inisial_cab');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('pelanggan/mclasscabang');
		$data['buatkode']	= $this->mclasscabang->buatkode();
		
		$query	= $this->mclasscabang->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url()."index.php/pelanggan/cformcabang/pagesnext/";
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclasscabang->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
		$data['isi']	= 'pelanggan/vmainformcabang';
		$this->load->view('template',$data);	
	}
	function index_cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
		$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
		$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
		$data['form_panel_cari_pelcab']	= $this->lang->line('form_panel_cari_pelcab');
		$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
		$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
		$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
		$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
		$data['form_inisial_cab'] = $this->lang->line('form_inisial_cab');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('pelanggan/mclasscabang');
		$data['buatkode']	= $this->mclasscabang->buatkode();
		
		$query	= $this->mclasscabang->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url()."index.php/pelanggan/cformcabang/pagesnext/";
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclasscabang->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
		$data['isi']	= 'pelanggan/vformcabangcari';
		$this->load->view('template',$data);	
	}
function tambah() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
		$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
		$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
		$data['form_panel_cari_pelcab']	= $this->lang->line('form_panel_cari_pelcab');
		$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
		$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
		$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
		$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
		$data['form_inisial_cab'] = $this->lang->line('form_inisial_cab');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('pelanggan/mclasscabang');
		$data['buatkode']	= $this->mclasscabang->buatkode();
		
		$query	= $this->mclasscabang->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url()."index.php/pelanggan/cformcabang/pagesnext/";
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclasscabang->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
		$data['isi']	= 'pelanggan/vmainformcabangadd';
		$this->load->view('template',$data);	
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
		$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
		$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
		$data['form_panel_cari_pelcab']	= $this->lang->line('form_panel_cari_pelcab');
		$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
		$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
		$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
		$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
		$data['form_inisial_cab'] = $this->lang->line('form_inisial_cab');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('pelanggan/mclasscabang');
		
		$data['buatkode']	= $this->mclasscabang->buatkode();
		
		$query	= $this->mclasscabang->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url()."index.php/pelanggan/cformcabang/pagesnext/";
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclasscabang->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
		$data['isi']	= 'pelanggan/vmainformcabang';
		$this->load->view('template',$data);
		
	}

	function detail() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$this->load->view('pelanggan/vmainformcabang',$data);
	}
	
	function listpelanggan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title']	= "PELANGGAN PUSAT";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('pelanggan/mclasscabang');

		$query	= $this->mclasscabang->listkonstumer();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/pelanggan/cformcabang/listpelanggannext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['isi']	= $this->mclasscabang->listkonstumerperpages($pagination['per_page'],$pagination['cur_page']);		
	
		//$data['isi']	= $this->mclasscabang->listpquery($tabel="tr_customer",$order="ORDER BY e_customer_name ",$filter="");	
		$this->load->view('pelanggan/vlistformpelanggan',$data);
	}

	function listpelanggannext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title']	= "PELANGGAN PUSAT";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('pelanggan/mclasscabang');

		$query	= $this->mclasscabang->listkonstumer();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/pelanggan/cformcabang/listpelanggannext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['isi']	= $this->mclasscabang->listkonstumerperpages($pagination['per_page'],$pagination['cur_page']);		
	
		//$data['isi']	= $this->mclasscabang->listpquery($tabel="tr_customer",$order="ORDER BY e_customer_name ",$filter="");	
		$this->load->view('pelanggan/vlistformpelanggan',$data);
	}
	
	function simpan() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$vi_branch		= $this->input->post('i_branch');
		$iarea			= $this->input->post('i_area');
		$vi_customer	= $this->input->post('i_customer');
		$icustomer		= $this->input->post('icustomer');
		$ve_branch_name	= $this->input->post('e_branch_name');
		$ve_branch_address	= $this->input->post('e_branch_address');
		$ve_branch_city	= $this->input->post('e_branch_city');
		$ve_initial		= $this->input->post('e_initial');
		
		if(!empty($vi_branch) && 
		   !empty($vi_customer) && 
		   !empty($ve_branch_name) ) {
			$this->load->model('pelanggan/mclasscabang');

			$q_ckpelanggan	= $this->mclasscabang->ckpelanggan($vi_branch);
			if($q_ckpelanggan->num_rows()>0) {
				$kodeada=1;
			} else {
				$kodeada=0;
			}
			
			$q_einisial	= $this->mclasscabang->ckpelangganinisial($ve_initial);
			if($q_einisial->num_rows()>0) {
				$einisalada=1;
			} else {
				$einisalada=0;
			}
			
			if($kodeada==0 && $einisalada==0) {
				$this->mclasscabang->msimpan($vi_branch,$iarea,$vi_customer,$ve_branch_name,$ve_branch_address,$ve_branch_city,$ve_initial,$icustomer);
			} else {
				print "<script>alert(\"Maaf, Kode Pelanggan tsb sudah ada!\"); window.open(\"tambah\", \"_self\");</script>";
			}
			
		} else {
			$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
			$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
			$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
			$data['form_panel_cari_pelcab']	= $this->lang->line('form_panel_cari_pelcab');
			$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
			$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
			$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
			$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
			$data['form_inisial_cab'] = $this->lang->line('form_inisial_cab');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['link_aksi']	= $this->lang->line('link_aksi');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('pelanggan/mclasscabang');
			
			$data['buatkode']	= $this->mclasscabang->buatkode();
			
			$query	= $this->mclasscabang->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= '/pelanggan/cformcabang/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclasscabang->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
			
			$this->load->view('pelanggan/vmainformcabang',$data);			
		}
	}
	
	function edit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		
		$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
		$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
		$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
		$data['form_panel_cari_pelcab']	= $this->lang->line('form_panel_cari_pelcab');
		$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
		$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
		$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
		$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
		$data['form_inisial_cab'] = $this->lang->line('form_inisial_cab');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model("pelanggan/mclasscabang");
		$qry_brg_pel	= $this->mclasscabang->medit($id);
		
		if($qry_brg_pel->num_rows() > 0) {
			
			$arr	= $qry_brg_pel->row_array();
			$qry_customer	= $this->mclasscabang->icustomer($arr['i_customer']);
			
			if($qry_customer->num_rows() > 0) {
				$row_customer	= $qry_customer->row();
				$data['customername']	= $row_customer->i_customer_code;
			} else {
				$data['customername']	= "";
			}
			
			$data['ibranch']		= $arr['i_branch'];
			$data['iarea']			= $arr['i_code'];
			$data['icustomer']		= $arr['i_customer'];
			$data['ibranchcode']	= $arr['i_branch_code'];
			$data['ebranchname']	= $arr['e_branch_name'];
			$data['ebranchaddress']	= $arr['e_branch_address'];
			$data['ebranchcity']	= $arr['e_branch_city'];
			$data['einitial']		= $arr['e_initial'];
			$data['isi']	= 'pelanggan/veditformcabang';
			$this->load->view('template',$data);
			
		}				
	}
	
	function actedit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$ibranchcode	= $this->input->post('i_branch');
		$iarea			= $this->input->post('i_area');
		$icustomer	= $this->input->post('i_customer');
		$ibranch	= $this->input->post('ibranch');
		$icustomer2	= $this->input->post('icustomer');
		$ebranchname	= $this->input->post('e_branch_name');
		$ebranchaddress	= $this->input->post('e_branch_address');
		$ebranchcity	= $this->input->post('e_branch_city');
		$einitial	= $this->input->post('e_initial');
		
		if(!empty($ibranchcode) && 
		   !empty($icustomer) && 
		   !empty($ebranchname) ) {   
		   
			$this->load->model('pelanggan/mclasscabang');		
			$this->mclasscabang->mupdate($ibranchcode,$iarea,$icustomer,$ebranchname,$ebranchaddress,$ebranchcity,$einitial,$ibranch,$icustomer2);				
		} else {
			$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
			$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
			$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
			$data['form_panel_cari_pelcab']	= $this->lang->line('form_panel_cari_pelcab');
			$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
			$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
			$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
			$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
			$data['form_inisial_cab'] = $this->lang->line('form_inisial_cab');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['link_aksi']	= $this->lang->line('link_aksi');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('pelanggan/mclasscabang');
			
			$data['buatkode']	= $this->mclasscabang->buatkode();
			
			$query	= $this->mclasscabang->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= '/pelanggan/cformcabang/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclasscabang->viewperpages($pagination['per_page'],$this->uri->segment(4,0));

			$this->load->view('pelanggan/vmainformcabang',$data);
		}
	}
	
	/* 20052011
	function actdelete() {
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_branch',array('i_branch'=>$id));
	}
	*/

	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('pelanggan/mclasscabang');
		$this->mclasscabang->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
		$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
		$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
		$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
		$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
		$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
		$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$txtibranch	= $this->input->post('txt_i_branch');
		$txticustomercode	= $this->input->post('txt_i_customer_code');
		$txtebranchname	= $this->input->post('txt_e_branch_name');

		$text1	= $txtibranch;
		$text2	= $txticustomercode;
		$text3	= $txtebranchname;
				
		$this->load->model('pelanggan/mclasscabang');

		$query	= $this->mclasscabang->viewcari($text1,$text2,$text3);
		$jml	= $query->num_rows();
		
		if($txtibranch=='')
			$txtibranch	= "kosong";
		
		if($txticustomercode=='')
			$txticustomercode	= "kosong";	
		
		if($txtebranchname=='')
			$txtebranchname	= "kosong";
		
		$pagination['base_url'] 	= '/pelanggan/cformcabang/carinext/'.$txtibranch.'/'.$txticustomercode.'/'.$txtebranchname.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclasscabang->mcari($text1,$text2,$text3,$pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'pelanggan/vcariformcabang';
		$this->load->view('template',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_pelcab']	= $this->lang->line('page_title_pelcab');
		$data['form_panel_daftar_pelcab']	= $this->lang->line('form_panel_daftar_pelcab');
		$data['form_panel_form_pelcab']	= $this->lang->line('form_panel_form_pelcab');
		$data['form_kode_cab']	= $this->lang->line('form_kode_cab');
		$data['form_kode_pelcab']	= $this->lang->line('form_kode_pelcab');
		$data['form_nm_cab']	= $this->lang->line('form_nm_cab');
		$data['form_kota_cab']	= $this->lang->line('form_kota_cab');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$txtibranch			= $this->uri->segment(4);
		$txticustomercode	= $this->uri->segment(5);
		$txtebranchname		= $this->uri->segment(6);
		
		$this->load->model('pelanggan/mclasscabang');
		
		$uri1	= $txtibranch;
		$uri2	= $txticustomercode;
		$uri3	= $txtebranchname;
		
		if($txtibranch=='kosong')
			$txtibranch	= "";
		
		if($txticustomercode=='kosong')
			$txticustomercode	= "";
		
		if($txtebranchname=='kosong')
			$txtebranchname	= "";
					
		$query	= $this->mclasscabang->viewcari($txtibranch,$txticustomercode,$txtebranchname);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= '/pelanggan/cformcabang/carinext/'.$uri1.'/'.$uri2.'/'.$uri3.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclasscabang->mcari($txtibranch,$txticustomercode,$txtebranchname,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('pelanggan/vcariformcabang',$data);
	}
}
?>
