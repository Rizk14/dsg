<?php
class Cformcodetransfer extends CI_Controller {
	function Cformcodetransfer(){
		parent::__construct();
	}
	
	function index(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_codetransfer']	= $this->lang->line('page_title_codetransfer');
		$data['form_panel_daftar_codetransfer']	= $this->lang->line('form_panel_daftar_codetransfer');
		$data['form_panel_cari_codetransfer']	= $this->lang->line('form_panel_cari_codetransfer');
		$data['form_keyword_pencarian']	= $this->lang->line('form_keyword_pencarian');
		$data['form_kode_codetransfer']	= $this->lang->line('form_kode_codetransfer');
		$data['form_kode_pelcodetransfer']	= $this->lang->line('form_kode_pelcodetransfer');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi');	
		$data['detail']			= "";
		$data['list']			= "";
		$data['not_defined']	= "";
		$data['limages']		= base_url();
		
		$this->load->model('pelanggan/mclasscodetransfer');
		$query	= $this->mclasscodetransfer->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= '/pelanggan/cformcodetransfer/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclasscodetransfer->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
		$data['isi']	= 'pelanggan/vmainformcodetransfer';
			$this->load->view('template',$data);
			
		
	}

function tambah(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_codetransfer']	= $this->lang->line('page_title_codetransfer');
		$data['form_panel_daftar_codetransfer']	= $this->lang->line('form_panel_daftar_codetransfer');
		$data['form_panel_cari_codetransfer']	= $this->lang->line('form_panel_cari_codetransfer');
		$data['form_keyword_pencarian']	= $this->lang->line('form_keyword_pencarian');
		$data['form_kode_codetransfer']	= $this->lang->line('form_kode_codetransfer');
		$data['form_kode_pelcodetransfer']	= $this->lang->line('form_kode_pelcodetransfer');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi');	
		$data['detail']			= "";
		$data['list']			= "";
		$data['not_defined']	= "";
		$data['limages']		= base_url();
		
		$this->load->model('pelanggan/mclasscodetransfer');
		$query	= $this->mclasscodetransfer->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= '/pelanggan/cformcodetransfer/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclasscodetransfer->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
		$data['isi']	= 'pelanggan/vmainformcodetransferadd';
			$this->load->view('template',$data);
			
		
	}

	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_codetransfer']	= $this->lang->line('page_title_codetransfer');
		$data['form_panel_daftar_codetransfer']	= $this->lang->line('form_panel_daftar_codetransfer');
		$data['form_panel_cari_codetransfer']	= $this->lang->line('form_panel_cari_codetransfer');
		$data['form_keyword_pencarian']	= $this->lang->line('form_keyword_pencarian');
		$data['form_kode_codetransfer']	= $this->lang->line('form_kode_codetransfer');
		$data['form_kode_pelcodetransfer']	= $this->lang->line('form_kode_pelcodetransfer');
		$data['text_cari']	= $this->lang->line('text_cari');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('pelanggan/mclasscodetransfer');
		$query	= $this->mclasscodetransfer->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= '/pelanggan/cformcodetransfer/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclasscodetransfer->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
		$this->load->view('pelanggan/vmainformcodetransfer',$data);		}

	function detail() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$this->load->view('pelanggan/vmainformcodetransfer',$data);
	}
	
	function listpelanggan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title']	= "PELANGGAN PUSAT";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('pelanggan/mclasscodetransfer');

		$query	= $this->mclasscodetransfer->listkonstumer();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/pelanggan/cformcodetransfer/listpelanggannext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['isi']	= $this->mclasscodetransfer->listkonstumerperpages($pagination['per_page'],$pagination['cur_page']);		
		$this->load->view('pelanggan/vlistformpelanggantrans',$data);
	}

	function listpelanggannext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title']	= "PELANGGAN PUSAT";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('pelanggan/mclasscodetransfer');

		$query	= $this->mclasscodetransfer->listkonstumer();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/pelanggan/cformcodetransfer/listpelanggannext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$data['isi']	= $this->mclasscodetransfer->listkonstumerperpages($pagination['per_page'],$pagination['cur_page']);		
		$this->load->view('pelanggan/vlistformpelanggantrans',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		//$irefftransfer	= @$this->input->post('i_reff_transfer')?@$this->input->post('i_reff_transfer'):@$this->input->get_post('i_reff_transfer');
		$icustomer	= @$this->input->post('i_customer')?@$this->input->post('i_customer'):@$this->input->get_post('i_customer');
		$itransfer	= @$this->input->post('i_transfer')?@$this->input->post('i_transfer'):@$this->input->get_post('i_transfer');

		if(!empty($icustomer) && 
		   !empty($itransfer)) {
			$this->load->model('pelanggan/mclasscodetransfer');

			$q_ckpelanggan	= $this->mclasscodetransfer->ckpelanggan($itransfer);
			if($q_ckpelanggan->num_rows()>0) {
				$ada=1;
			} else {
				$ada=0;
			}
			
			if($ada==0) {
				$this->mclasscodetransfer->msimpan($icustomer,$itransfer);
			} else {
				print "<script>alert(\"Maaf, Kode Pelanggan tsb sudah ada!\");show(\"pelanggan/cformcodetransfer\",\"#content\");</script>";
			}			
		} else {
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['page_title_codetransfer']	= $this->lang->line('page_title_codetransfer');
			$data['form_panel_daftar_codetransfer']	= $this->lang->line('form_panel_daftar_codetransfer');
			$data['form_panel_cari_codetransfer']	= $this->lang->line('form_panel_cari_codetransfer');
			$data['form_keyword_pencarian']	= $this->lang->line('form_keyword_pencarian');
			$data['form_kode_codetransfer']	= $this->lang->line('form_kode_codetransfer');
			$data['form_kode_pelcodetransfer']	= $this->lang->line('form_kode_pelcodetransfer');
			$data['text_cari']	= $this->lang->line('text_cari');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['link_aksi']	= $this->lang->line('link_aksi');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$this->load->model('pelanggan/mclasscodetransfer');
			$query	= $this->mclasscodetransfer->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= '/pelanggan/cformcodetransfer/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclasscodetransfer->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
			$this->load->view('pelanggan/vmainformcodetransfer',$data);				
		}
	}
	
	function edit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		
		$data['page_title_codetransfer']	= $this->lang->line('page_title_codetransfer');
		$data['form_panel_daftar_codetransfer']	= $this->lang->line('form_panel_daftar_codetransfer');
		$data['form_panel_cari_codetransfer']	= $this->lang->line('form_panel_cari_codetransfer');
		$data['form_keyword_pencarian']	= $this->lang->line('form_keyword_pencarian');
		$data['form_kode_codetransfer']	= $this->lang->line('form_kode_codetransfer');
		$data['form_kode_pelcodetransfer']	= $this->lang->line('form_kode_pelcodetransfer');
		$data['text_cari']	= $this->lang->line('text_cari');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model("pelanggan/mclasscodetransfer");
		$qry_brg_pel	= $this->mclasscodetransfer->medit($id);
		
		if($qry_brg_pel->num_rows() > 0) {
			$arr	= $qry_brg_pel->row_array();
			$qry_customer	= $this->mclasscodetransfer->icustomer($arr['i_customer_from']);
						
			if($qry_customer->num_rows() > 0) {
				$row_customer	= $qry_customer->row();
				$data['customername']	= $row_customer->i_customer_code;
			} else {
				$data['customername']	= "";
			}
			
			$data['icustomer']	= $arr['i_customer_from'];
			$data['itransfer']	= $arr['i_customer_transfer'];
			$data['irefftransfer']	= $arr['i_reff_transfer'];
			$data['isi']='pelanggan/veditformcodetransfer';	
			$this->load->view('template',$data);	
		}				
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$irefftransfer	= @$this->input->post('i_reff_transfer')?@$this->input->post('i_reff_transfer'):@$this->input->get_post('i_reff_transfer');
		$icustomer	= @$this->input->post('i_customer')?@$this->input->post('i_customer'):@$this->input->get_post('i_customer');
		$itransfer	= @$this->input->post('i_transfer')?@$this->input->post('i_transfer'):@$this->input->get_post('i_transfer');
		$itrans		= @$this->input->post('itrans')?@$this->input->post('itrans'):@$this->input->get_post('itrans');
				
		if(!empty($icustomer) && 
		   !empty($itransfer)) {   
		   
			$this->load->model('pelanggan/mclasscodetransfer');		
			$q_ckpelanggan	= $this->mclasscodetransfer->ckpelanggan($itransfer);
			
			if(($q_ckpelanggan->num_rows()>0)) {
				$r_ckpelanggan	= $q_ckpelanggan->row();
				$x	= $r_ckpelanggan->i_customer_transfer;		
				if($x!=$itrans){
					$ada=1;
				}else{
					$ada=0;
				}
			} else {
				$ada=0;
			}

			if($ada==0)	{		
				$this->mclasscodetransfer->mupdate($irefftransfer,$icustomer,$itransfer);
			}else{
				print "<script>alert(\"Maaf, gagal diupdate! \");show(\"pelanggan/cformcodetransfer\",\"#content\");</script>";
			}
			
		} else {
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['page_title_codetransfer']	= $this->lang->line('page_title_codetransfer');
			$data['form_panel_daftar_codetransfer']	= $this->lang->line('form_panel_daftar_codetransfer');
			$data['form_panel_cari_codetransfer']	= $this->lang->line('form_panel_cari_codetransfer');
			$data['form_keyword_pencarian']	= $this->lang->line('form_keyword_pencarian');
			$data['form_kode_codetransfer']	= $this->lang->line('form_kode_codetransfer');
			$data['form_kode_pelcodetransfer']	= $this->lang->line('form_kode_pelcodetransfer');
			$data['text_cari']	= $this->lang->line('text_cari');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['link_aksi']	= $this->lang->line('link_aksi');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$this->load->model('pelanggan/mclasscodetransfer');
			$query	= $this->mclasscodetransfer->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= '/pelanggan/cformcodetransfer/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclasscodetransfer->viewperpages($pagination['per_page'],$this->uri->segment(4,0));
			$this->load->view('pelanggan/vmainformcodetransfer',$data);				
		}
	}

	function actdelete() {
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('pelanggan/mclasscodetransfer');
		$this->mclasscodetransfer->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_codetransfer']	= $this->lang->line('page_title_codetransfer');
		$data['form_panel_daftar_codetransfer']	= $this->lang->line('form_panel_daftar_codetransfer');
		$data['form_panel_cari_codetransfer']	= $this->lang->line('form_panel_cari_codetransfer');
		$data['form_keyword_pencarian']	= $this->lang->line('form_keyword_pencarian');
		$data['form_kode_codetransfer']	= $this->lang->line('form_kode_codetransfer');
		$data['form_kode_pelcodetransfer']	= $this->lang->line('form_kode_pelcodetransfer');
		$data['text_cari']	= $this->lang->line('text_cari');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$txtcari	= $this->input->post('txtcari')?$this->input->post('txtcari'):$this->input->get_post('txtcari');
		
		$this->load->model('pelanggan/mclasscodetransfer');

		$query	= $this->mclasscodetransfer->viewcari($txtcari);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/pelanggan/cformcodetransfer/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclasscodetransfer->mcari($txtcari,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('pelanggan/vcariformcodetransfer',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_codetransfer']	= $this->lang->line('page_title_codetransfer');
		$data['form_panel_daftar_codetransfer']	= $this->lang->line('form_panel_daftar_codetransfer');
		$data['form_panel_cari_codetransfer']	= $this->lang->line('form_panel_cari_codetransfer');
		$data['form_keyword_pencarian']	= $this->lang->line('form_keyword_pencarian');
		$data['form_kode_codetransfer']	= $this->lang->line('form_kode_codetransfer');
		$data['form_kode_pelcodetransfer']	= $this->lang->line('form_kode_pelcodetransfer');
		$data['text_cari']	= $this->lang->line('text_cari');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$txtcari	= $this->input->post('txtcari')?$this->input->post('txtcari'):$this->input->get_post('txtcari');
		
		$this->load->model('pelanggan/mclasscodetransfer');

		$query	= $this->mclasscodetransfer->viewcari($txtcari);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/pelanggan/cformcodetransfer/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclasscodetransfer->mcari($txtcari,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('pelanggan/vcariformcodetransfer',$data);
	}
}
?>
