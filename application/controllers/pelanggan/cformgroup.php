<?php
class Cformgroup extends CI_Controller {

	function Cformgroup()
	{
		parent::__construct();			
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_group_pel']	= $this->lang->line('page_title_group_pel');
		$data['form_panel_daftar_group_pel']	= $this->lang->line('form_panel_daftar_group_pel');
		$data['form_form_group_pel']	= $this->lang->line('form_form_group_pel');
		$data['form_cari_group_pel']	= $this->lang->line('form_cari_group_pel');
		$data['form_kode_group_pel']	= $this->lang->line('form_kode_group_pel');
		$data['form_nm_group_pel']	= $this->lang->line('form_nm_group_pel');
		$data['form_note_group_pel']	= $this->lang->line('form_note_group_pel');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclassgroup');
		
		$q_codegrouppel	= $this->mclassgroup->codegrouppelanggan();
		if($q_codegrouppel->num_rows() > 0) {
			$row_codegrouppel	=  $q_codegrouppel->row();
			//$igroup	= $row_codegrouppel->igroup;
			$igroupcode	= $row_codegrouppel->igroupcode;
			
			switch(strlen($igroupcode)) {
				case 1:
					$groupcode	= '0000'.$igroupcode;
				break;
				case 2:
					$groupcode	= '000'.$igroupcode;
				break;
				case 3:
					$groupcode	= '00'.$igroupcode;
				break;
				case 4:
					$groupcode	= '0'.$igroupcode;
				break;
				case 5:
					$groupcode	= $igroupcode;
				break;
			}
			
		} else {
			//$igroup	= 1;
			$groupcode	= '00001';
		}
		$data['igrouppelanggan'] = $groupcode==""?"00001":$groupcode;
		
		$query	= $this->mclassgroup->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= '/pelanggan/cformgroup/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclassgroup->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'pelanggan/vformgroup';
	//	$this->load->view('pelanggan/vformgroup',$data);	
		$this->load->view('template',$data);
	}

	function tambah() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_group_pel']	= $this->lang->line('page_title_group_pel');
		$data['form_panel_daftar_group_pel']	= $this->lang->line('form_panel_daftar_group_pel');
		$data['form_form_group_pel']	= $this->lang->line('form_form_group_pel');
		$data['form_cari_group_pel']	= $this->lang->line('form_cari_group_pel');
		$data['form_kode_group_pel']	= $this->lang->line('form_kode_group_pel');
		$data['form_nm_group_pel']	= $this->lang->line('form_nm_group_pel');
		$data['form_note_group_pel']	= $this->lang->line('form_note_group_pel');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclassgroup');
		
		$q_codegrouppel	= $this->mclassgroup->codegrouppelanggan();
		if($q_codegrouppel->num_rows() > 0) {
			$row_codegrouppel	=  $q_codegrouppel->row();
			//$igroup	= $row_codegrouppel->igroup;
			$igroupcode	= $row_codegrouppel->igroupcode;
			
			switch(strlen($igroupcode)) {
				case 1:
					$groupcode	= '0000'.$igroupcode;
				break;
				case 2:
					$groupcode	= '000'.$igroupcode;
				break;
				case 3:
					$groupcode	= '00'.$igroupcode;
				break;
				case 4:
					$groupcode	= '0'.$igroupcode;
				break;
				case 5:
					$groupcode	= $igroupcode;
				break;
			}
			
		} else {
			//$igroup	= 1;
			$groupcode	= '00001';
		}
		$data['igrouppelanggan'] = $groupcode==""?"00001":$groupcode;
		
		$query	= $this->mclassgroup->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= '/pelanggan/cformgroup/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclassgroup->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'pelanggan/vformgroupadd';
	//	$this->load->view('pelanggan/vformgroup',$data);	
		$this->load->view('template',$data);
	}

	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_group_pel']	= $this->lang->line('page_title_group_pel');
		$data['form_panel_daftar_group_pel']	= $this->lang->line('form_panel_daftar_group_pel');
		$data['form_form_group_pel']	= $this->lang->line('form_form_group_pel');
		$data['form_cari_group_pel']	= $this->lang->line('form_cari_group_pel');
		$data['form_kode_group_pel']	= $this->lang->line('form_kode_group_pel');
		$data['form_nm_group_pel']	= $this->lang->line('form_nm_group_pel');
		$data['form_note_group_pel']	= $this->lang->line('form_note_group_pel');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclassgroup');

		$q_codegrouppel	= $this->mclassgroup->codegrouppelanggan();
		if($q_codegrouppel->num_rows() > 0) {
			$row_codegrouppel	=  $q_codegrouppel->row();
			//$igroup	= $row_codegrouppel->igroup;
			$igroupcode	= $row_codegrouppel->igroupcode;

			switch(strlen($igroupcode)) {
				case 1:
					$groupcode	= '0000'.$igroupcode;
				break;
				case 2:
					$groupcode	= '000'.$igroupcode;
				break;
				case 3:
					$groupcode	= '00'.$igroupcode;
				break;
				case 4:
					$groupcode	= '0'.$igroupcode;
				break;
				case 5:
					$groupcode	= $igroupcode;
				break;
			}
			
		} else {
			//$igroup	= 1;
			$groupcode	= '00001';
		}
		$data['igrouppelanggan'] = $groupcode==""?"00001":$groupcode;
		
		$query	= $this->mclassgroup->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= '/pelanggan/cformgroup/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclassgroup->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$this->load->view('pelanggan/vformgroup',$data);	
	}
	
	function detail(){	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$this->load->view('pelanggan/vformgroup',$data);
	}

	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$i_group_code	= @$this->input->post('i_group_code')?@$this->input->post('i_group_code'):@$this->input->get_post('i_group_code');
		$e_group_name	= @$this->input->post('e_group_name')?@$this->input->post('e_group_name'):@$this->input->get_post('e_group_name');
		$e_note	= @$this->input->post('e_note')?@$this->input->post('e_note'):@$this->input->get_post('e_note');

		if( !empty($i_group_code) && 
		    !empty($e_group_name) ) {
			$this->load->model('pelanggan/mclassgroup');
			$this->mclassgroup->msimpan($i_group_code,$e_group_name,$e_note);
		} else {
			$data['page_title_group_pel']	= $this->lang->line('page_title_group_pel');
			$data['form_panel_daftar_group_pel']	= $this->lang->line('form_panel_daftar_group_pel');
			$data['form_form_group_pel']	= $this->lang->line('form_form_group_pel');
			$data['form_cari_group_pel']	= $this->lang->line('form_cari_group_pel');
			$data['form_kode_group_pel']	= $this->lang->line('form_kode_group_pel');
			$data['form_nm_group_pel']	= $this->lang->line('form_nm_group_pel');
			$data['form_note_group_pel']	= $this->lang->line('form_note_group_pel');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['link_aksi']	= $this->lang->line('link_aksi');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
	
			$this->load->model('pelanggan/mclassgroup');

			$q_codegrouppel	= $this->mclassgroup->codegrouppelanggan();
			if($q_codegrouppel->num_rows() > 0) {
				$row_codegrouppel	=  $q_codegrouppel->row();
				//$igroup	= $row_codegrouppel->igroup;
				$igroupcode	= $row_codegrouppel->igroupcode;
	
				switch(strlen($igroupcode)) {
					case 1:
						$groupcode	= '0000'.$igroupcode;
					break;
					case 2:
						$groupcode	= '000'.$igroupcode;
					break;
					case 3:
						$groupcode	= '00'.$igroupcode;
					break;
					case 4:
						$groupcode	= '0'.$igroupcode;
					break;
					case 5:
						$groupcode	= $igroupcode;
					break;
				}
				
			} else {
				//$igroup	= 1;
				$groupcode	= '00001';
			}
			$data['igrouppelanggan'] = $groupcode==""?"00001":$groupcode;
						
			$query	= $this->mclassgroup->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= '/pelanggan/cformgroup/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclassgroup->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
			$this->load->view('pelanggan/vmainformgroup',$data);
		}
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$id	= $this->uri->segment(4);
		$data['id']	= $id;

		$data['page_title_group_pel']	= $this->lang->line('page_title_group_pel');
		$data['form_panel_daftar_group_pel']	= $this->lang->line('form_panel_daftar_group_pel');
		$data['form_form_group_pel']	= $this->lang->line('form_form_group_pel');
		$data['form_cari_group_pel']	= $this->lang->line('form_cari_group_pel');
		$data['form_kode_group_pel']	= $this->lang->line('form_kode_group_pel');
		$data['form_nm_group_pel']	= $this->lang->line('form_nm_group_pel');
		$data['form_note_group_pel']	= $this->lang->line('form_note_group_pel');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();

		$this->load->model('pelanggan/mclassgroup');
		$qry_group_pel	= $this->mclassgroup->medit($id);
		
		if($qry_group_pel->num_rows() > 0) {
		
			$arr	= $qry_group_pel->row_array();
			
			$data['igroup']		= $arr['i_group'];
			$data['igroupcode']	= $arr['i_group_code'];
			$data['egroupname']	= $arr['e_group_name'];
			$data['enote']		= $arr['e_note'];
		$data['isi']	= 'pelanggan/veditformgroup';

		$this->load->view('template',$data);
			//$this->load->view('pelanggan/veditformgroup',$data);	
		}
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$i_group	= @$this->input->post('igroup')?@$this->input->post('igroup'):@$this->input->get_post('igroup');	
		$i_group_code	= @$this->input->post('i_group_code')?@$this->input->post('i_group_code'):@$this->input->get_post('i_group_code');
		$e_group_name	= @$this->input->post('e_group_name')?@$this->input->post('e_group_name'):@$this->input->get_post('e_group_name');
		$e_note	= @$this->input->post('e_note')?@$this->input->post('e_note'):@$this->input->get_post('e_note');
		
		if( !empty($i_group_code) && 
		    !empty($e_group_name) ) {
			$this->load->model('pelanggan/mclassgroup');
			$this->mclassgroup->mupdate($i_group,$i_group_code,$e_group_name,$e_note);
		} else {
			$data['page_title_group_pel']	= $this->lang->line('page_title_group_pel');
			$data['form_panel_daftar_group_pel']	= $this->lang->line('form_panel_daftar_group_pel');
			$data['form_form_group_pel']	= $this->lang->line('form_form_group_pel');
			$data['form_cari_group_pel']	= $this->lang->line('form_cari_group_pel');
			$data['form_kode_group_pel']	= $this->lang->line('form_kode_group_pel');
			$data['form_nm_group_pel']	= $this->lang->line('form_nm_group_pel');
			$data['form_note_group_pel']	= $this->lang->line('form_note_group_pel');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['link_aksi']	= $this->lang->line('link_aksi');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
	
			$this->load->model('pelanggan/mclassgroup');

			$q_codegrouppel	= $this->mclassgroup->codegrouppelanggan();
			if($q_codegrouppel->num_rows() > 0) {
				$row_codegrouppel	=  $q_codegrouppel->row();
				//$igroup	= $row_codegrouppel->igroup;
				$igroupcode	= $row_codegrouppel->igroupcode;
	
				switch(strlen($igroupcode)) {
					case 1:
						$groupcode	= '0000'.$igroupcode;
					break;
					case 2:
						$groupcode	= '000'.$igroupcode;
					break;
					case 3:
						$groupcode	= '00'.$igroupcode;
					break;
					case 4:
						$groupcode	= '0'.$igroupcode;
					break;
					case 5:
						$groupcode	= $igroupcode;
					break;
				}
				
			} else {
				//$igroup	= 1;
				$groupcode	= '00001';
			}
			$data['igrouppelanggan'] = $groupcode==""?"00001":$groupcode;
					
			$query	= $this->mclassgroup->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= '/pelanggan/cformgroup/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclassgroup->viewperpages($pagination['per_page'],$pagination['cur_page']);

			$this->load->view('pelanggan/vmainformgroup',$data);
		}		
	}
	
	/* 20052011
	function actdelete() {
		$id			= $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_group',array('i_group'=>$id));
	}
	*/
	
	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('pelanggan/mclassgroup');
		$this->mclassgroup->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_group_pel']	= $this->lang->line('page_title_group_pel');
		$data['form_panel_daftar_group_pel']	= $this->lang->line('form_panel_daftar_group_pel');
		$data['form_form_group_pel']	= $this->lang->line('form_form_group_pel');
		$data['form_cari_group_pel']	= $this->lang->line('form_cari_group_pel');
		$data['form_kode_group_pel']	= $this->lang->line('form_kode_group_pel');
		$data['form_nm_group_pel']	= $this->lang->line('form_nm_group_pel');
		$data['form_note_group_pel']	= $this->lang->line('form_note_group_pel');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
	
		$txt_i_group_code	= $this->input->post('txt_i_group_code')?$this->input->post('txt_i_group_code'):$this->input->get_post('txt_i_group_code');
		$txt_group_name	= $this->input->post('txt_group_name')?$this->input->post('txt_group_name'):$this->input->get_post('txt_group_name');
		
		$this->load->model('pelanggan/mclassgroup');

		$query	= $this->mclassgroup->viewcari($txt_i_group_code,$txt_group_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= '/pelanggan/cformgroup/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
	
		$data['isi']	= $this->mclassgroup->mcari($txt_i_group_code,$txt_group_name,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('pelanggan/vcariformgroup',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		$data['page_title_group_pel']	= $this->lang->line('page_title_group_pel');
		$data['form_panel_daftar_group_pel']	= $this->lang->line('form_panel_daftar_group_pel');
		$data['form_form_group_pel']	= $this->lang->line('form_form_group_pel');
		$data['form_cari_group_pel']	= $this->lang->line('form_cari_group_pel');
		$data['form_kode_group_pel']	= $this->lang->line('form_kode_group_pel');
		$data['form_nm_group_pel']	= $this->lang->line('form_nm_group_pel');
		$data['form_note_group_pel']	= $this->lang->line('form_note_group_pel');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
	
		$txt_i_group_code	= $this->input->post('txt_i_group_code')?$this->input->post('txt_i_group_code'):$this->input->get_post('txt_i_group_code');
		$txt_group_name	= $this->input->post('txt_group_name')?$this->input->post('txt_group_name'):$this->input->get_post('txt_group_name');
		
		$this->load->model('pelanggan/mclassgroup');

		$query	= $this->mclassgroup->viewcari($txt_i_group_code,$txt_group_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= '/pelanggan/cformgroup/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
	
		$data['isi']	= $this->mclassgroup->mcari($txt_i_group_code,$txt_group_name,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('pelanggan/vcariformgroup',$data);
	}	
}
?>
