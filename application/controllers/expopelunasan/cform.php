<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expopelunasan/mclass');
	}
	
	function index() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			$data['page_title_pelunasan']		= $this->lang->line('page_title_pelunasan');
			$data['form_title_detail_pelunasan']= $this->lang->line('form_title_detail_pelunasan');
			$data['list_pelunasan_no_voucher']	= $this->lang->line('list_pelunasan_no_voucher');
			$data['list_pelunasan_tgl_voucher']	= $this->lang->line('list_pelunasan_tgl_voucher');
			$data['list_pelunasan_tgl_pelunasan']	= $this->lang->line('list_pelunasan_tgl_pelunasan');
			$data['list_pelunasan_recieve']		= $this->lang->line('list_pelunasan_recieve');
			$data['list_pelunasan_approve']		= $this->lang->line('list_pelunasan_approve');
			$data['list_pelunasan_piutang']		= $this->lang->line('list_pelunasan_piutang');
			$data['list_pelunasan_total_voucher']	= $this->lang->line('list_pelunasan_total_voucher');
			$data['list_pelunasan_nilai_voucher']	= $this->lang->line('list_pelunasan_nilai_voucher');
			$data['list_pelunasan_nilai_pelunasan']	= $this->lang->line('list_pelunasan_nilai_pelunasan');
			$data['list_pelunasan_no_kontrabon']	= $this->lang->line('list_pelunasan_no_kontrabon');
			$data['list_pelunasan_tgl_kontrabon']	= $this->lang->line('list_pelunasan_tgl_kontrabon');
			$data['list_pelunasan_total_pelunasan']	= $this->lang->line('list_pelunasan_total_pelunasan');
			$data['list_pelunasan_nilai_faktur']	= $this->lang->line('list_pelunasan_nilai_faktur');
			$data['list_pelunasan_tgl_faktur']		= $this->lang->line('list_pelunasan_tgl_faktur');
			$data['list_pelunasan_pelangga_faktur']	= $this->lang->line('list_pelunasan_pelangga_faktur');
			$data['list_pelunasan_total_faktur']	= $this->lang->line('list_pelunasan_total_faktur');
			$data['list_pelunasan_total_kontrabon'] = $this->lang->line('list_pelunasan_total_kontrabon');
			$data['list_pelunasan_keterangan']	= $this->lang->line('list_pelunasan_keterangan');
			
			$data['button_laporan']	= $this->lang->line('button_laporan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
				
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi'] = 'expopelunasan/vmainform';
			$this->load->view('template',$data);
		
		
	}
	
	function carilistvoucher() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title_pelunasan']		= $this->lang->line('page_title_pelunasan');
		$data['form_title_detail_pelunasan']= $this->lang->line('form_title_detail_pelunasan');
		$data['list_pelunasan_no_voucher']	= $this->lang->line('list_pelunasan_no_voucher');
		$data['list_pelunasan_tgl_voucher']	= $this->lang->line('list_pelunasan_tgl_voucher');
		$data['list_pelunasan_tgl_pelunasan']	= $this->lang->line('list_pelunasan_tgl_pelunasan');
		$data['list_pelunasan_recieve']		= $this->lang->line('list_pelunasan_recieve');
		$data['list_pelunasan_approve']		= $this->lang->line('list_pelunasan_approve');
		$data['list_pelunasan_piutang']		= $this->lang->line('list_pelunasan_piutang');
		$data['list_pelunasan_total_voucher']	= $this->lang->line('list_pelunasan_total_voucher');
		$data['list_pelunasan_nilai_voucher']	= $this->lang->line('list_pelunasan_nilai_voucher');
		$data['list_pelunasan_nilai_pelunasan']	= $this->lang->line('list_pelunasan_nilai_pelunasan');
		$data['list_pelunasan_no_kontrabon']	= $this->lang->line('list_pelunasan_no_kontrabon');
		$data['list_pelunasan_tgl_kontrabon']	= $this->lang->line('list_pelunasan_tgl_kontrabon');
		$data['list_pelunasan_total_pelunasan']	= $this->lang->line('list_pelunasan_total_pelunasan');
		$data['list_pelunasan_nilai_faktur']	= $this->lang->line('list_pelunasan_nilai_faktur');
		$data['list_pelunasan_tgl_faktur']		= $this->lang->line('list_pelunasan_tgl_faktur');
		$data['list_pelunasan_pelangga_faktur']	= $this->lang->line('list_pelunasan_pelangga_faktur');
		$data['list_pelunasan_total_faktur']	= $this->lang->line('list_pelunasan_total_faktur');
		$data['list_pelunasan_total_kontrabon'] = $this->lang->line('list_pelunasan_total_kontrabon');
		$data['list_pelunasan_keterangan']	= $this->lang->line('list_pelunasan_keterangan');
		$data['list_pelunasan_sumber_kontrabon'] = $this->lang->line('list_pelunasan_sumber_kontrabon');
		
		$data['button_laporan']	= $this->lang->line('button_laporan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpelunasan']	= "";
		$data['limages']	= base_url();
		
		$no_voucher	= $this->input->post('no_voucher');
		$i_voucher	= $this->input->post('i_voucher');
		$dvoucherfirst= $this->input->post('d_voucher_first');
		$dvoucherlast	= $this->input->post('d_voucher_last');
		
		$data['tglvouchermulai']	= (!empty($dvoucherfirst) && $dvoucherfirst!='0')?$dvoucherfirst:'';
		$data['tglvoucherakhir']	= (!empty($dvoucherlast) && $dvoucherlast!='0')?$dvoucherlast:'';
		$data['novoucher']			= (!empty($no_voucher) && $no_voucher!='0')?$no_voucher:'';
		$data['ivoucher']			= (!empty($i_voucher) && $i_voucher!='0')?$i_voucher:'';
		
		$e_d_voucher_first= explode("/",$dvoucherfirst,strlen($dvoucherfirst));
		$e_d_voucher_last	= explode("/",$dvoucherlast,strlen($dvoucherlast));
		
		$ndvoucherfirst	= !empty($dvoucherfirst)?$e_d_voucher_first[2].'-'.$e_d_voucher_first[1].'-'.$e_d_voucher_first[0]:'0';
		$ndvoucherlast	= !empty($dvoucherlast)?$e_d_voucher_last[2].'-'.$e_d_voucher_last[1].'-'.$e_d_voucher_last[0]:'0';
		
		$turi1	= ($no_voucher!='' && $no_voucher!='0')?$no_voucher:'0';
		$turi2	= ($i_voucher!='' && $i_voucher!='0')?$i_voucher:'0';
		$turi3	= $ndvoucherfirst;
		$turi4	= $ndvoucherlast;
		
		$data['turi1'] = $turi1;
		$data['turi2'] = $turi2;
		$data['turi3'] = $turi3;
		$data['turi4'] = $turi4;
				
		/*** $this->load->model('expopelunasan/mclass'); ***/
		
		$pagination['base_url'] 	= 'expopelunasan/cform/carilistvouchernext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/';
		
		if($dvoucherfirst!='' && $dvoucherlast!='') {
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage1($i_voucher,$ndvoucherfirst,$ndvoucherlast);
			$data['template'] = 1;
		}else{
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage2($i_voucher,$ndvoucherfirst,$ndvoucherlast);
			$data['template'] = 2;
		}
		
		$pagination['total_rows']	= $qlistvoucherallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dvoucherfirst!='' && $dvoucherlast!='') {
			$data['query']	= $this->mclass->clistvoucher1($pagination['per_page'],$pagination['cur_page'],$i_voucher,$ndvoucherfirst,$ndvoucherlast);
		}else{
			$data['query']	= $this->mclass->clistvoucher2($pagination['per_page'],$pagination['cur_page'],$i_voucher,$ndvoucherfirst,$ndvoucherlast);
		}
		$data['isi'] = 'expopelunasan/vlistform';
			$this->load->view('template',$data);
		
	}

	function carilistvouchernext() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title_pelunasan']		= $this->lang->line('page_title_pelunasan');
		$data['form_title_detail_pelunasan']= $this->lang->line('form_title_detail_pelunasan');
		$data['list_pelunasan_no_voucher']	= $this->lang->line('list_pelunasan_no_voucher');
		$data['list_pelunasan_tgl_voucher']	= $this->lang->line('list_pelunasan_tgl_voucher');
		$data['list_pelunasan_tgl_pelunasan']	= $this->lang->line('list_pelunasan_tgl_pelunasan');
		$data['list_pelunasan_recieve']		= $this->lang->line('list_pelunasan_recieve');
		$data['list_pelunasan_approve']		= $this->lang->line('list_pelunasan_approve');
		$data['list_pelunasan_piutang']		= $this->lang->line('list_pelunasan_piutang');
		$data['list_pelunasan_total_voucher']	= $this->lang->line('list_pelunasan_total_voucher');
		$data['list_pelunasan_nilai_voucher']	= $this->lang->line('list_pelunasan_nilai_voucher');
		$data['list_pelunasan_nilai_pelunasan']	= $this->lang->line('list_pelunasan_nilai_pelunasan');
		$data['list_pelunasan_no_kontrabon']	= $this->lang->line('list_pelunasan_no_kontrabon');
		$data['list_pelunasan_tgl_kontrabon']	= $this->lang->line('list_pelunasan_tgl_kontrabon');
		$data['list_pelunasan_total_pelunasan']	= $this->lang->line('list_pelunasan_total_pelunasan');
		$data['list_pelunasan_nilai_faktur']	= $this->lang->line('list_pelunasan_nilai_faktur');
		$data['list_pelunasan_tgl_faktur']		= $this->lang->line('list_pelunasan_tgl_faktur');
		$data['list_pelunasan_pelangga_faktur']	= $this->lang->line('list_pelunasan_pelangga_faktur');
		$data['list_pelunasan_total_faktur']	= $this->lang->line('list_pelunasan_total_faktur');
		$data['list_pelunasan_total_kontrabon'] = $this->lang->line('list_pelunasan_total_kontrabon');
		$data['list_pelunasan_keterangan']	= $this->lang->line('list_pelunasan_keterangan');
		$data['list_pelunasan_sumber_kontrabon'] = $this->lang->line('list_pelunasan_sumber_kontrabon');
		
		$data['button_laporan']	= $this->lang->line('button_laporan');	
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpelunasan']	= "";		
		$data['limages']	= base_url();

		$no_voucher	= $this->uri->segment(4);
		$i_voucher	= $this->uri->segment(5);
		$dvoucherfirst	= $this->uri->segment(6);
		$dvoucherlast	= $this->uri->segment(7);
		
		$e_d_voucher_first = ($dvoucherfirst!='0')?explode("-",$dvoucherfirst,strlen($dvoucherfirst)):'';
		$e_d_voucher_last  = ($dvoucherlast!='0')?explode("-",$dvoucherlast,strlen($dvoucherlast)):'';

		$ndvoucherfirst	= $dvoucherfirst!='0'?$e_d_voucher_first[2].'/'.$e_d_voucher_first[1].'/'.$e_d_voucher_first[0]:'0';
		$ndvoucherlast	= $dvoucherlast!='0'?$e_d_voucher_last[2].'/'.$e_d_voucher_last[1].'/'.$e_d_voucher_last[0]:'0';
						
		$data['tglvouchermulai']= (!empty($ndvoucherfirst) && $ndvoucherfirst!='0')?$ndvoucherfirst:'';
		$data['tglvoucherakhir']= (!empty($ndvoucherlast) && $ndvoucherlast!='0')?$ndvoucherlast:'';
		$data['novoucher']		= (!empty($no_voucher) && $no_voucher!='0')?$no_voucher:'';
		$data['ivoucher']		= (!empty($i_voucher) && $i_voucher!='0')?$i_voucher:'';
		
		$turi1	= ($no_voucher!='' && $no_voucher!='0')?$no_voucher:'0';
		$turi2	= ($i_voucher!='' && $i_voucher!='0')?$i_voucher:'0';
		$turi3	= $dvoucherfirst;
		$turi4	= $dvoucherlast;

		$data['turi1'] = $turi1;
		$data['turi2'] = $turi2;
		$data['turi3'] = $turi3;
		$data['turi4'] = $turi4;
		
		 $this->load->model('expopelunasan/mclass'); 
		
		$pagination['base_url'] 	= 'expopelunasan/cform/carilistvouchernext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/';
		
		if($dvoucherfirst!='0' && $dvoucherlast!='0') {
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage1($i_voucher,$dvoucherfirst,$dvoucherlast);
		}else{
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage2($i_voucher,$dvoucherfirst,$dvoucherlast);
		}
		
		$pagination['total_rows']	= $qlistvoucherallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dvoucherfirst!='0' && $dvoucherlast!='0') {
			$data['isi']	= $this->mclass->clistvoucher1($pagination['per_page'],$pagination['cur_page'],$i_voucher,$dvoucherfirst,$dvoucherlast);
			$data['template'] = 1;
		}else{
			$data['isi']	= $this->mclass->clistvoucher2($pagination['per_page'],$pagination['cur_page'],$i_voucher,$dvoucherfirst,$dvoucherlast);
			$data['template'] = 2;
		}
		
		$this->load->view('expopelunasan/vlistform',$data);
					
	}
	
	function gexportpelunasan() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title_pelunasan']		= $this->lang->line('page_title_pelunasan');
		$data['form_title_detail_pelunasan']= $this->lang->line('form_title_detail_pelunasan');
		$data['list_pelunasan_no_voucher']	= $this->lang->line('list_pelunasan_no_voucher');
		$data['list_pelunasan_tgl_voucher']	= $this->lang->line('list_pelunasan_tgl_voucher');
		$data['list_pelunasan_tgl_pelunasan']	= $this->lang->line('list_pelunasan_tgl_pelunasan');
		$data['list_pelunasan_recieve']		= $this->lang->line('list_pelunasan_recieve');
		$data['list_pelunasan_approve']		= $this->lang->line('list_pelunasan_approve');
		$data['list_pelunasan_piutang']		= $this->lang->line('list_pelunasan_piutang');
		$data['list_pelunasan_total_voucher']	= $this->lang->line('list_pelunasan_total_voucher');
		$data['list_pelunasan_nilai_voucher']	= $this->lang->line('list_pelunasan_nilai_voucher');
		$data['list_pelunasan_nilai_pelunasan']	= $this->lang->line('list_pelunasan_nilai_pelunasan');
		$data['list_pelunasan_no_kontrabon']	= $this->lang->line('list_pelunasan_no_kontrabon');
		$data['list_pelunasan_tgl_kontrabon']	= $this->lang->line('list_pelunasan_tgl_kontrabon');
		$data['list_pelunasan_total_pelunasan']	= $this->lang->line('list_pelunasan_total_pelunasan');
		$data['list_pelunasan_nilai_faktur']	= $this->lang->line('list_pelunasan_nilai_faktur');
		$data['list_pelunasan_tgl_faktur']		= $this->lang->line('list_pelunasan_tgl_faktur');
		$data['list_pelunasan_pelangga_faktur']	= $this->lang->line('list_pelunasan_pelangga_faktur');
		$data['list_pelunasan_total_faktur']	= $this->lang->line('list_pelunasan_total_faktur');
		$data['list_pelunasan_total_kontrabon'] = $this->lang->line('list_pelunasan_total_kontrabon');
		$data['list_pelunasan_keterangan']	= $this->lang->line('list_pelunasan_keterangan');
		$data['list_pelunasan_sumber_kontrabon'] = $this->lang->line('list_pelunasan_sumber_kontrabon');
		
		$data['button_laporan']	= $this->lang->line('button_laporan');	
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpelunasan']	= "";		
		$data['limages']	= base_url();

		$no_voucher	= $this->uri->segment(4);
		$i_voucher	= $this->uri->segment(5);
		$dvoucherfirst	= $this->uri->segment(6);
		$dvoucherlast	= $this->uri->segment(7);
				
		$e_d_voucher_first = ($dvoucherfirst!='0')?explode("-",$dvoucherfirst,strlen($dvoucherfirst)):'';
		$e_d_voucher_last  = ($dvoucherlast!='0')?explode("-",$dvoucherlast,strlen($dvoucherlast)):'';

		$ndvoucherfirst	= $dvoucherfirst!='0'?$e_d_voucher_first[2].'/'.$e_d_voucher_first[1].'/'.$e_d_voucher_first[0]:'0';
		$ndvoucherlast	= $dvoucherlast!='0'?$e_d_voucher_last[2].'/'.$e_d_voucher_last[1].'/'.$e_d_voucher_last[0]:'0';
		
		$periode	= $ndvoucherfirst." s.d ".$ndvoucherlast;
						
		$data['tglvouchermulai']= (!empty($ndvoucherfirst) && $ndvoucherfirst!='0')?$ndvoucherfirst:'';
		$data['tglvoucherakhir']= (!empty($ndvoucherlast) && $ndvoucherlast!='0')?$ndvoucherlast:'';
		$data['novoucher']		= (!empty($no_voucher) && $no_voucher!='0')?$no_voucher:'';
		$data['ivoucher']		= (!empty($i_voucher) && $i_voucher!='0')?$i_voucher:'';
		$data['ivouchercode']	= (!empty($i_voucher_code) && $i_voucher_code!='0')?$i_voucher_code:'';
		
		$turi1	= $no_voucher;
		$turi2	= $i_voucher;
		$turi3	= $dvoucherfirst;
		$turi4	= $dvoucherlast;

		$data['turi1'] = $turi1;
		$data['turi2'] = $turi2;
		$data['turi3'] = $turi3;
		$data['turi4'] = $turi4;
		
		$this->load->model('expopelunasan/mclass'); 
		
		$pagination['base_url'] 	= '/expopelunasan/cform/carilistvouchernext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/';
		
		if($dvoucherfirst!='0' && $dvoucherlast!='0') {
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage1($i_voucher,$dvoucherfirst,$dvoucherlast);
		}else{
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage2($i_voucher,$dvoucherfirst,$dvoucherlast);
		}
		
		$pagination['total_rows']	= $qlistvoucherallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dvoucherfirst!='0' && $dvoucherlast!='0') {
			$data['isi']	= $this->mclass->clistvoucher1($pagination['per_page'],$pagination['cur_page'],$i_voucher,$dvoucherfirst,$dvoucherlast);
			$data['template'] = 1;
		}else{
			$data['isi']	= $this->mclass->clistvoucher2($pagination['per_page'],$pagination['cur_page'],$i_voucher,$dvoucherfirst,$dvoucherlast);
			$data['template'] = 2;
		}	

		$ObjPHPExcel = new PHPExcel();
		
		$qexppelunasan	= $this->mclass->explistpelunasan($i_voucher,$dvoucherfirst,$dvoucherlast);
		
		if($qexppelunasan->num_rows()>0) {
		
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Pelunasan")
				->setSubject("Laporan Pelunasan")
				->setDescription("Laporan Pelunasan")
				->setKeywords("Laporan Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);
			
			/*** 02012012
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12); // recieve
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12); // approve
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:J2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN PELUNASAN');
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:J3');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:J4');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),													
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'No Kontra Bon');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl. Kontra Bon');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'Total Kontra Bon');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Nomor Voucher');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Ket. Voucher');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Tgl. Voucher');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Penerima');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Di Approve');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
												
			$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'Nilai Pelunasan');
			$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
												
			if($qexppelunasan->num_rows()>0) {
				
				$j	= 7;
				$nomor	= 1;
				$jml=1;
									
				foreach($qexppelunasan->result() as $row) {
					
					$row->d_dt	= explode("-",$row->d_dt,strlen($row->d_dt)); // Y-m-d
					$row->d_voucher	= explode("-",$row->d_voucher,strlen($row->d_voucher)); // Y-m-d
					
					$tgldt		= $row->d_dt[2]."/".$row->d_dt[1]."/".$row->d_dt[0];
					$tgldvoucher	= $row->d_voucher[2]."/".$row->d_voucher[1]."/".$row->d_voucher[0];

					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->i_dt_code);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $tgldt);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, "-");
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->i_voucher_code." - ".$row->i_voucher_no);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->e_description);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $tgldvoucher);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->e_recieved);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->e_approved);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
																		
					$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $row->v_voucher);
					$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$jml++;													
					$j++;																																																							
					$nomor++;
				}
			}
			***/

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12); // recieve
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12); // approve
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:I2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN PELUNASAN');
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:I3');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:I4');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'No Kontra Bon');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl. Kontra Bon');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'Nomor Voucher');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Ket. Voucher');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Tgl. Voucher');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Penerima');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Di Approve');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
												
			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Nilai Pelunasan');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
												
			if($qexppelunasan->num_rows()>0) {
				
				$j	= 7;
				$nomor	= 1;
				$jml=1;
									
				foreach($qexppelunasan->result() as $row) {
					
					$row->d_dt	= explode("-",$row->d_dt,strlen($row->d_dt)); // Y-m-d
					$row->d_voucher	= explode("-",$row->d_voucher,strlen($row->d_voucher)); // Y-m-d
					
					$tgldt		= $row->d_dt[2]."/".$row->d_dt[1]."/".$row->d_dt[0];
					$tgldvoucher	= $row->d_voucher[2]."/".$row->d_voucher[1]."/".$row->d_voucher[0];

					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->i_dt_code);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $tgldt);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->i_voucher_code." - ".$row->i_voucher_no);
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->e_description);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $tgldvoucher);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->e_recieved);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->e_approved);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
																		
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->v_voucher);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$jml++;
					$j++;																																																				
					$nomor++;
				}
			}
									
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."laporan_pelunasan_".$dvoucherfirst."_".$dvoucherlast.".xls";
			$ObjWriter->save("files/".$files);

			$efilename = @substr($files,1,strlen($files));
	
			$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
			
			$this->load->view('expopelunasan/vexpform',$data);																					
		}
	}
	
	function listvoucher() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title']	= "VOUCHER";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expopelunasan/mclass'); ***/

		$query	= $this->mclass->lvoucher();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expopelunasan/cform/listvouchernext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lvoucherperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('expopelunasan/vlistvoucher',$data);			
	}

	function listvouchernext() {
		$data['page_title']	= "VOUCHER";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expopelunasan/mclass'); ***/

		$query	= $this->mclass->lvoucher();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expopelunasan/cform/listvouchernext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lvoucherperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('expopelunasan/vlistvoucher',$data);			
	}		

	function flistvoucher() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');

		$data['page_title']	= "VOUCHER";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		/*** $this->load->model('expopelunasan/mclass'); ***/

		$query	= $this->mclass->flvoucher($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				$exp_d_voucher = explode('-',$row->d_voucher,strlen($row->d_voucher));
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_voucher_code','$row->i_voucher')\">".$row->i_voucher_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_voucher_code','$row->i_voucher')\">".$exp_d_voucher[2].'/'.$exp_d_voucher[1].'/'.$exp_d_voucher[0]."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
}

?>
