<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "KELAS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('klsbrg/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'klsbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		$qry_class				= $this->mclass->classcode();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['iclass']	= $row_class->classcode;
		} else {
			$data['iclass']	= 1;
		}
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	='klsbrg/vmainform';
		$this->load->view('template',$data);
	}

	function tambah() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "KELAS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('klsbrg/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'klsbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		$qry_class				= $this->mclass->classcode();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['iclass']	= $row_class->classcode;
		} else {
			$data['iclass']	= 1;
		}
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	='klsbrg/vmainformadd';
		$this->load->view('template',$data);
	}

	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "KELAS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('klsbrg/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'klsbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		$qry_class				= $this->mclass->classcode();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['iclass']	= $row_class->classcode;
		} else {
			$data['iclass']	= "";
		}		
		$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$this->load->view('klsbrg/vmainform',$data);
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$this->load->view('klsbrg/vmainform',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$viclass	= @$this->input->post('iclass')?@$this->input->post('iclass'):@$this->input->get_post('iclass');
		$veclassname= @$this->input->post('eclassname')?@$this->input->post('eclassname'):@$this->input->get_post('eclassname');
		
		if((isset($viclass) || !empty($viclass)) && 
		    (isset($veclassname) || !empty($veclassname)) &&
		    (($viclass!=0 || $viclass!="")) && ($veclassname!=0 || $veclassname!="")) {
			$this->load->model('klsbrg/mclass');
			$this->mclass->msimpan($viclass,$veclassname);
		} else {
			$data['page_title']	= "KELAS BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('klsbrg/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'klsbrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();
			$qry_class				= $this->mclass->classcode();
			if($qry_class->num_rows()>0) {
				$row_class	= $qry_class->row();
				$data['iclass']	= $row_class->classcode;
			} else {
				$data['iclass']	= "";
			}
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
			
			$this->load->view('klsbrg/vmainform',$data);
		}
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		$data['page_title']	= "KELAS BARANG";
		$limages			= base_url();
		$data['list']		= "";
		$this->load->model('klsbrg/mclass');
		$qry_klsbrg		= $this->mclass->medit($id);
		if( $qry_klsbrg->num_rows() > 0 ) {
			$row_klsbrg	= $qry_klsbrg->row();
			$data['i_class']	= $row_klsbrg->i_class;
			$data['e_class_name']	= $row_klsbrg->e_class_name;
		} else {
			$data['i_class']	= "";
			$data['e_class_name']	= "";		
		}
		$data['isi']='klsbrg/veditform';
		$this->load->view('template',$data);
	}
	
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iclass	= @$this->input->post('iclass')?@$this->input->post('iclass'):@$this->input->get_post('iclass');
		$eclassname= @$this->input->post('eclassname')?@$this->input->post('eclassname'):@$this->input->get_post('eclassname');
		
		if((isset($iclass) || !empty($iclass)) && 
		    (isset($eclassname) || !empty($eclassname)) &&
		    (($iclass!=0 || $iclass!="")) && ($eclassname!=0 || $eclassname!="")) {
			$this->load->model('klsbrg/mclass');
			$this->mclass->mupdate($iclass,$eclassname);
		} else {
			$data['page_title']	= "KELAS BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('klsbrg/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'klsbrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();
			$qry_class				= $this->mclass->classcode();
			if($qry_class->num_rows()>0) {
				$row_class	= $qry_class->row();
				$data['iclass']	= $row_class->classcode;
			} else {
				$data['iclass']	= "";
			}
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
			
			$this->load->view('klsbrg/vmainform',$data);
		}
	}
	
	/* 20052011
	function actdelete() {
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_class',array('i_class'=>$id));
	}
	*/

	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('klsbrg/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_class	= $this->input->post('txt_i_class')?$this->input->post('txt_i_class'):$this->input->get_post('txt_i_class');
		$txt_e_class_name	= $this->input->post('txt_e_class_name')?$this->input->post('txt_e_class_name'):$this->input->get_post('txt_e_class_name');

		$data['page_title']	= "KELAS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('klsbrg/mclass');
		$query	= $this->mclass->viewcari($txt_i_class,$txt_e_class_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'klsbrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txt_i_class,$txt_e_class_name,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('klsbrg/vcariform',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_class	= $this->input->post('txt_i_class')?$this->input->post('txt_i_class'):$this->input->get_post('txt_i_class');
		$txt_e_class_name	= $this->input->post('txt_e_class_name')?$this->input->post('txt_e_class_name'):$this->input->get_post('txt_e_class_name');

		$data['page_title']	= "KELAS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('klsbrg/mclass');
		$query	= $this->mclass->viewcari($txt_i_class,$txt_e_class_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'klsbrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txt_i_class,$txt_e_class_name,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('klsbrg/vcariform',$data);
	}	
}
?>
