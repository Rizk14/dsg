<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-area/mmaster');
    $this->load->library('pagination');
  }
  
 /*
 /*
  function updateidunit() {
	  $sql = " SELECT * FROM tm_area ORDER BY tgl_input ";
	  $query	= $this->db->query($sql);
	  
	  if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil id tertinggi
			$sql2 = " SELECT id FROM tm_area ORDER BY id DESC LIMIT 1 ";
			$query2	= $this->db->query($sql2);
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->row();
				$idlama	= $hasil2->id;
				$idbaru = $idlama+1;
				
				$this->db->query(" UPDATE tm_area SET id='$idbaru' WHERE kode_area='$row1->kode_area' ");
			}
		}
	}
	echo "sukses";
  }
*/
  function index(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode_area;
			$elokasi = $row->lokasi;
			$enama = $row->nama;
			
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$elokasi = '';
			$enama = '';
			$edit = '';
			
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['elokasi'] = $elokasi;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-area/vmainform';
    
	$this->load->view('template',$data);
 //   $this->load->view('mst-kel-barang/input',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		// 22-09-2014, form validation ga perlu. skrg pake jquery aja
		
			$kode 	= $this->input->post('kode_area', TRUE);
			$id_area 	= $this->input->post('id_area', TRUE); 
			$lokasi = $this->input->post('lokasi', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			
			
			$this->mmaster->save($kode, $id_area, $lokasi, $nama, $goedit);
			redirect('mst-area/cform');
		//}
  }

  function delete(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete($id);
    redirect('mst-area/cform');
  }
  

 
}
