<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expostoksaatinimrkt/mclass');
	}
	
	function index() {
		
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
			$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
			$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
			$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
			$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			$data['isi']		= "";
			
			/*** $this->load->model('expostoksaatini/mclass'); ***/
			$data['opt_jns_brg']	= $this->mclass->lklsbrg();
			$data['isi']	= 'expostoksaatinimrkt/vmainform';	
			$this->load->view('template',$data);	
		
		
	}
	
	function carilistbrgjadi() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
		$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
		$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
		$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
		$data['list_stoksaatini_kd_brg']	= $this->lang->line('list_stoksaatini_kd_brg');
		$data['list_stoksaatini_nm_brg']	= $this->lang->line('list_stoksaatini_nm_brg');
		$data['list_stoksaatini_s_awal']	= $this->lang->line('list_stoksaatini_s_awal');
		$data['list_stoksaatini_s_akhir']	= $this->lang->line('list_stoksaatini_s_akhir');
		$data['list_stoksaatini_bmm']	= $this->lang->line('list_stoksaatini_bmm');
		$data['list_stoksaatini_bmk']	= $this->lang->line('list_stoksaatini_bmk');
		$data['list_stoksaatini_bbm']	= $this->lang->line('list_stoksaatini_bbm');
		$data['list_stoksaatini_bbk']	= $this->lang->line('list_stoksaatini_bbk');
		$data['list_stoksaatini_do']	= $this->lang->line('list_stoksaatini_do');
		$data['list_stoksaatini_sj']	= $this->lang->line('list_stoksaatini_sj');
		$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lstokopname']	= "";
		$data['ljnsbrg']	= "";
		$data['limages']	= base_url();
			
		$ddofirst	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->input->get_post('d_do_first');
		$ddolast	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->input->get_post('d_do_last');
		$stop		= $this->input->post('stop');
		$bulanan	= $this->input->post('bulanan');
		$iclass		= $this->input->post('i_class')?$this->input->post('i_class'):$this->input->get_post('i_class');
		
		$stopproduk	= $stop==1?'TRUE':'FALSE';
		
		$data['tgldomulai']	= ($ddofirst!="")?($ddofirst):$def_dofirst="01"."/".date("m/Y");
		$data['tgldoakhir']	= ($ddolast!="")?($ddolast):$def_dolast=date("d/m/Y");		
		$data['sproduksi']	= $stop==1?" checked ":"";
		$data['kproduksi']	= $iclass;
		
		$e_d_do_first	= explode("/",$ddofirst,strlen($ddofirst));
		$e_d_do_last	= explode("/",$ddolast,strlen($ddolast)); // dd/mm/YYYY
		$nddofirst		= ($ddofirst!='')?($e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]):($def_dofirst=date('Y-m')."-"."01");
		$nddolast		= ($ddolast!='')?($e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]):($def_dolast=date('Y-m-d'));

		$tahun	= substr($nddofirst,0,4); // YYYY-mm-dd
		$bulan	= substr($nddofirst,5,2);
		
		$bln_sekarang	= date("m");
		$thn_sekarang	= date("Y");
		
		if($bln_sekarang==$bulan && $thn_sekarang==$tahun){
			$filter_tgl_stokopname	= " AND d.i_status_so=0 ";
		}else{
			$filter_tgl_stokopname	= " AND SUBSTRING(CAST(d.d_so AS character varying),1,4)='$tahun' AND SUBSTRING(CAST(d.d_so AS character varying),6,2)='$bulan'";
		}
				
		$data['tdofirst']	= $nddofirst;
		$data['tdolast']	= $nddolast;
		$data['sproduct']	= $stopproduk;
		$data['class']		= $iclass;
		$data['bulanan']	= $bulanan;
		
		/*** $this->load->model('expostoksaatini/mclass'); ***/
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		
		$qbrgjadi	= $this->mclass->lbrgjadi($stopproduk,$filter_tgl_stokopname);
		$lstokopname	= "";
		
		$no	= 1;
		$j	= 0;
		
		$bonmkeluar	= array();
		$bonmmasuk	= array();
		$bbk	= array();
		$bbm	= array();
		$do	= array();
		$sj	= array();
		
		$x11	= array();
		$x22	= array();
		$z11	= array();
		$saldoakhirnya	= array();
				
		if($qbrgjadi->num_rows()>0) {
			$cc	= 1;
			foreach($qbrgjadi->result() as $field) {
				$qbonmkeluar	= $this->mclass->lbonkeluar($nddofirst,$nddolast,$field->i_product_motif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}

				$qbonmmasuk	= $this->mclass->lbonmasuk($nddofirst,$nddolast,$field->i_product_motif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk; 
				} else {
					$bonmmasuk[$j] = 0;
				}

				$qbbk	= $this->mclass->lbbk($nddofirst,$nddolast,$field->i_product_motif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk; 
				} else {
					$bbk[$j] = 0;
				}

				$qbbm	= $this->mclass->lbbm($nddofirst,$nddolast,$field->i_product_motif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm; 
				} else {
					$bbm[$j] = 0;
				}

				$qdo	= $this->mclass->ldo($nddofirst,$nddolast,$field->i_product_motif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}

				$qsj	= $this->mclass->lsj($nddofirst,$nddolast,$field->i_product_motif);
				if($qsj->num_rows()>0) {
					$row_sj	= $qsj->row();
					$sj[$j]	= $row_sj->jsj;
				} else {
					$sj[$j] = 0;
				}
				
				$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
				$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

				$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
				//$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
				$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j];
				$z11[$j]	= $field->n_quantity_awal+$x11[$j];
				$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
																								
				$lstokopname	.=	
				"<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\">
				  <td height=\"20px;\" bgcolor=\"$bgcolor\">".$no."</td>
				  <td>".$field->i_product_motif."</td>
				  <td>".$field->e_product_motifname."</td>
				  <td align=\"right\">".$field->n_quantity_awal."</td>
				  <td align=\"right\">".$saldoakhirnya[$j]."</td>
				  <td align=\"right\">".$bonmmasuk[$j]."</td>
				  <td align=\"right\">".$bbm[$j]."</td>
				  <td align=\"right\">".$do[$j]."</td>";
				  
				  //<td align=\"right\">".$sj[$j]."</td>
				  $lstokopname	.="<td align=\"right\">".$bonmkeluar[$j]."</td>
				  <td align=\"right\">".$bbk[$j]."</td>
				</tr>";	
				$j++;
				$no++;
				$cc++;
			}
		}		
		$data['query']	= $lstokopname;
		$data['isi']	= 'expostoksaatinimrkt/vlistform';	
			$this->load->view('template',$data);	
	}
	
	function gexportstoksaatini() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
		$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
		$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
		$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
		$data['list_stoksaatini_kd_brg']	= $this->lang->line('list_stoksaatini_kd_brg');
		$data['list_stoksaatini_nm_brg']	= $this->lang->line('list_stoksaatini_nm_brg');
		$data['list_stoksaatini_s_awal']	= $this->lang->line('list_stoksaatini_s_awal');
		$data['list_stoksaatini_s_akhir']	= $this->lang->line('list_stoksaatini_s_akhir');
		$data['list_stoksaatini_bmm']	= $this->lang->line('list_stoksaatini_bmm');
		$data['list_stoksaatini_bmk']	= $this->lang->line('list_stoksaatini_bmk');
		$data['list_stoksaatini_bbm']	= $this->lang->line('list_stoksaatini_bbm');
		$data['list_stoksaatini_bbk']	= $this->lang->line('list_stoksaatini_bbk');
		$data['list_stoksaatini_do']	= $this->lang->line('list_stoksaatini_do');
		$data['list_stoksaatini_sj']	= $this->lang->line('list_stoksaatini_sj');
		$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lstokopname']	= "";
		$data['ljnsbrg']	= "";
		$data['limages']	= base_url();
					
		$tdofirst	= $this->uri->segment(4); // yyyy-mm-dd
		$tdolast	= $this->uri->segment(5);
		$sproduct	= $this->uri->segment(6);
		$class	= $this->uri->segment(7);
		$bulanan	= $this->uri->segment(8);
		
		$extdofirst	= explode("-",$tdofirst,strlen($tdofirst));
		$extdolast	=  explode("-",$tdolast,strlen($tdolast));
		
		$nwdofirst	= $extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0];
		$nwdolast	= $extdolast[2]."/".$extdolast[1]."/".$extdolast[0];

		$tahun	= substr($tdofirst,0,4); // YYYY-mm-dd
		$bulan	= substr($tdofirst,5,2);
		
		$bln_sekarang	= date("m");
		$thn_sekarang	= date("Y");
		
		if($bln_sekarang==$bulan && $thn_sekarang==$tahun){
			$filter_tgl_stokopnames	= " AND d.i_status_so=0 ";
		}else{
			$filter_tgl_stokopnames	= " AND SUBSTRING(CAST(d.d_so AS character varying),1,4)='$tahun' AND SUBSTRING(CAST(d.d_so AS character varying),6,2)='$bulan' ";
		}
		
        $ObjPHPExcel = new PHPExcel();
				
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= $nwdofirst;
		$data['tgldoakhir']	= $nwdolast;
		$data['sproduksi']	= $sproduct=='TRUE'?" checked ":"";
		$data['kproduksi']	= $class;
				
		/*** $this->load->model('expostoksaatini/mclass'); ***/
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		
		$qbrgjadi	= $this->mclass->lbrgjadi($sproduct,$filter_tgl_stokopnames);
		$lstokopname	= "";
		
		$no	= 1;
		$j	= 0;
		
		$bonmkeluar	= array();
		$bonmmasuk	= array();
		$bbk	= array();
		$bbm	= array();
		$do	= array();
		$sj	= array();
		
		$x11	= array();
		$x22	= array();
		$z11	= array();
		$saldoakhirnya	= array();
				
		if($qbrgjadi->num_rows()>0) {

			$cc	= 1;
			
			foreach($qbrgjadi->result() as $field) {
				$qbonmkeluar	= $this->mclass->lbonkeluar($tdofirst,$tdolast,$field->i_product_motif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}

				$qbonmmasuk	= $this->mclass->lbonmasuk($tdofirst,$tdolast,$field->i_product_motif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk; 
				} else {
					$bonmmasuk[$j] = 0;
				}

				$qbbk	= $this->mclass->lbbk($tdofirst,$tdolast,$field->i_product_motif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk; 
				} else {
					$bbk[$j] = 0;
				}

				$qbbm	= $this->mclass->lbbm($tdofirst,$tdolast,$field->i_product_motif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm;
				} else {
					$bbm[$j] = 0;
				}

				$qdo	= $this->mclass->ldo($tdofirst,$tdolast,$field->i_product_motif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}

				$qsj	= $this->mclass->lsj($tdofirst,$tdolast,$field->i_product_motif);
				if($qsj->num_rows()>0) {
					$row_sj	= $qsj->row();
					$sj[$j]	= $row_sj->jsj;
				} else {
					$sj[$j] = 0;
				}
				
				$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
				$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

				$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
				//$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
				$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j];
				$z11[$j]	= $field->n_quantity_awal+$x11[$j];
				$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
																												
				$lstokopname	.=	
				"<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\" onMouseOut=\"this.className='$Classnya'\">
				  <td height=\"20px;\" bgcolor=\"$bgcolor\">".$no."</td>
				  <td>".$field->i_product_motif."</td>
				  <td>".$field->e_product_motifname."</td>
				  <td align=\"right\">".$field->n_quantity_awal."</td>
				  <td align=\"right\">".$saldoakhirnya[$j]."</td>
				  <td align=\"right\">".$bonmmasuk[$j]."</td>
				  <td align=\"right\">".$bbm[$j]."</td>
				  <td align=\"right\">".$do[$j]."</td>";
				  
				  //<td align=\"right\">".$sj[$j]."</td>
				  $lstokopname	.=	"<td align=\"right\">".$bonmkeluar[$j]."</td>
				  <td align=\"right\">".$bbk[$j]."</td>
				</tr>";
			
				$j++;
				$no++;
				$cc++;
				
			}
		}		
		
		$data['query']	= $lstokopname;
		
		
		$qbrgjadi2	= $this->mclass->lbrgjadi($sproduct,$filter_tgl_stokopnames);
		
		$no2	= 1;
		$j2		= 0;
		$nomor2	= 1;
		$cel	= 8;
		
		$bonmkeluar2= array();
		$bonmmasuk2	= array();
		$bbk2	= array();
		$bbm2	= array();
		$do2	= array();
		$sj2	= array();
		
		$saldosfk	= array();

		$x11excel	= array();
		$x22excel	= array();
		$z11excel	= array();
		$saldoakhirnyaexcel	= array();	
		
		$nilai_order = '';
		
		$total_order	= 0;
		$total_qty_awal	= 0;
		$total_qty_akhir = 0;
		$total_bonmmasuk = 0;
		$total_bbm = 0;
		$total_do = 0;
		$total_bonmkeluar = 0;
		$total_bbk = 0;
		$total_sj = 0;
									
		if($qbrgjadi2->num_rows()>0) {
		
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Mutasi Stok Gudang Jadi")
				->setSubject("Mutasi Stok")
				->setDescription("Laporan Mutasi Stok Gudang Jadi")
				->setKeywords("Laporan Bulanan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			//$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);			
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:S2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'CV. DUTA SETIA GARMEN');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:S3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', 'Laporan Mutasi Stok Gudang Jadi');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:S4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A6:A7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('B6:B7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Kode Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),						
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('C6:C7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Nama Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('E6:F6');
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Penerimaan');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			$ObjPHPExcel->getActiveSheet()->mergeCells('D6:D7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'S.Awal');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E7', 'B.Masuk');
			$ObjPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F7', 'BBM');
			$ObjPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('G6:J6');
			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Pengeluaran');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				
			$ObjPHPExcel->getActiveSheet()->setCellValue('G7', 'DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H7', 'BBK');
			$ObjPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I7', 'B.Keluar');
			$ObjPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			/*$ObjPHPExcel->getActiveSheet()->setCellValue('J7', 'SJ');
			$ObjPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				); */

			$ObjPHPExcel->getActiveSheet()->mergeCells('K6:K7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'S.Akhir');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
								
			// tambahan
			$ObjPHPExcel->getActiveSheet()->mergeCells('L6:L7');	
			$ObjPHPExcel->getActiveSheet()->setCellValue('L6', 'S.O.P');
			$ObjPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			$ObjPHPExcel->getActiveSheet()->mergeCells('M6:M7');	
			$ObjPHPExcel->getActiveSheet()->setCellValue('M6', 'Selisih');
			$ObjPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);						

			$ObjPHPExcel->getActiveSheet()->mergeCells('N6:S6');
			$ObjPHPExcel->getActiveSheet()->setCellValue('N6', 'Nilai');
			$ObjPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('N7', 'HJP');
			$ObjPHPExcel->getActiveSheet()->getStyle('N7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('O7', 'S.Awal');
			$ObjPHPExcel->getActiveSheet()->getStyle('O7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('P7', 'DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('P7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('Q7', 'S.Akhir');
			$ObjPHPExcel->getActiveSheet()->getStyle('Q7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('R7', 'S.O.P');
			$ObjPHPExcel->getActiveSheet()->getStyle('R7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('S7', 'Selisih');
			$ObjPHPExcel->getActiveSheet()->getStyle('S7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');		
			$ObjPHPExcel->getActiveSheet()->getStyle('E6:J6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('E7:J7')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6:M7')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('N6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('N7:S7')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
																												
			foreach($qbrgjadi2->result() as $field2) {
				$qbonmkeluar2	= $this->mclass->lbonkeluar($tdofirst,$tdolast,$field2->i_product_motif);
				if($qbonmkeluar2->num_rows()>0) {
					$row_bmkeluar2	= $qbonmkeluar2->row();
					$bonmkeluar2[$j2]	= $row_bmkeluar2->jbonkeluar;
				} else {
					$bonmkeluar2[$j2] = 0;
				}

				$qbonmmasuk2	= $this->mclass->lbonmasuk($tdofirst,$tdolast,$field2->i_product_motif);
				if($qbonmmasuk2->num_rows()>0) {
					$row_bmmasuk2	= $qbonmmasuk2->row();
					$bonmmasuk2[$j2]	= $row_bmmasuk2->jbonmasuk; 
				} else {
					$bonmmasuk2[$j2] = 0;
				}

				$qbbk2	= $this->mclass->lbbk($tdofirst,$tdolast,$field2->i_product_motif);
				if($qbbk2->num_rows()>0) {
					$row_bbk2	= $qbbk2->row();
					$bbk2[$j2]	= $row_bbk2->jbbk; 
				} else {
					$bbk2[$j2] = 0;
				}

				$qbbm2	= $this->mclass->lbbm($tdofirst,$tdolast,$field2->i_product_motif);
				if($qbbm2->num_rows()>0) {
					$row_bbm2	= $qbbm2->row();
					$bbm2[$j2]	= $row_bbm2->jbbm; 
				} else {
					$bbm2[$j2] = 0;
				}

				$qdo2	= $this->mclass->ldo($tdofirst,$tdolast,$field2->i_product_motif);
				if($qdo2->num_rows()>0) {
					$row_do2	= $qdo2->row();
					$do2[$j2]	= $row_do2->jdo; 
				} else {
					$do2[$j2] = 0;
				}

				$qsj2	= $this->mclass->lsj($tdofirst,$tdolast,$field2->i_product_motif);
				if($qsj2->num_rows()>0) {
					$row_sj2	= $qsj2->row();
					$sj2[$j2]	= $row_sj2->jsj; 
				} else {
					$sj2[$j2] = 0;
				}
								
				$qdatafsk	= $this->mclass->ljumlahfsk($field2->i_product_motif,$field2->f_stop_produksi);
				if($qdatafsk->num_rows()>0) {
					$row_datafsk	= $qdatafsk->row();
					$saldosfk[$j2]	= $row_datafsk->n_quantity_fisik;
				} else {
					$saldosfk[$j2]	= 0;
				}

				$x11excel[$j2]	= $bonmmasuk2[$j2]+$bbm[$j2];
				//$x22excel[$j2]	= $do2[$j2]+$bonmkeluar2[$j2]+$bbk2[$j2]+$sj2[$j2];
				$x22excel[$j2]	= $do2[$j2]+$bonmkeluar2[$j2]+$bbk2[$j2];
				$z11excel[$j2]	= $field2->n_quantity_awal+$x11excel[$j2];
				$saldoakhirnyaexcel[$j2]	= $z11excel[$j2]-$x22excel[$j2];
												
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$cel, $nomor2);
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
						
				$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$cel, $field2->i_product_motif);
				$ObjPHPExcel->getActiveSheet()->getStyle('B'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$cel, $field2->e_product_motifname);
				$ObjPHPExcel->getActiveSheet()->getStyle('C'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$cel, $field2->n_quantity_awal);
				$ObjPHPExcel->getActiveSheet()->getStyle('D'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$cel, $bonmmasuk2[$j2]);
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
							
				$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$cel, $bbm2[$j2]);
				$ObjPHPExcel->getActiveSheet()->getStyle('F'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$cel, $do2[$j2]);
				$ObjPHPExcel->getActiveSheet()->getStyle('G'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$cel, $bbk2[$j2]);
				$ObjPHPExcel->getActiveSheet()->getStyle('H'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$cel, $bonmkeluar2[$j2]);
				$ObjPHPExcel->getActiveSheet()->getStyle('I'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				//$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$cel, $field2->n_quantity_akhir);
			/*	$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$cel, $sj2[$j2]);
				$ObjPHPExcel->getActiveSheet()->getStyle('J'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */

				//$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$cel, $field2->n_quantity_akhir);
				$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$cel, $saldoakhirnyaexcel[$j2]);
				$ObjPHPExcel->getActiveSheet()->getStyle('K'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				
				if($bulanan==1){
					$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$cel, $saldosfk[$j2]);
				}else{
					$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$cel, "");
				}
				$ObjPHPExcel->getActiveSheet()->getStyle('L'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
																	
				if($bulanan==1){
					$ObjPHPExcel->getActiveSheet()->setCellValue('M'.$cel, ($saldosfk[$j2]-$saldoakhirnyaexcel[$j2]));
				}else{
					$ObjPHPExcel->getActiveSheet()->setCellValue('M'.$cel, "");
				}
				
				$ObjPHPExcel->getActiveSheet()->getStyle('M'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('N'.$cel, $field2->v_unitprice);
				$ObjPHPExcel->getActiveSheet()->getStyle('N'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				  
				$ObjPHPExcel->getActiveSheet()->setCellValue('O'.$cel, ($field2->v_unitprice * $field2->n_quantity_awal));
				$ObjPHPExcel->getActiveSheet()->getStyle('O'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('P'.$cel, ($field2->v_unitprice * $do2[$j2]));
				$ObjPHPExcel->getActiveSheet()->getStyle('P'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				  
				//$ObjPHPExcel->getActiveSheet()->setCellValue('P'.$cel, ($field2->v_unitprice * $field2->n_quantity_akhir));
				$ObjPHPExcel->getActiveSheet()->setCellValue('Q'.$cel, ($field2->v_unitprice * $saldoakhirnyaexcel[$j2]));
				$ObjPHPExcel->getActiveSheet()->getStyle('Q'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),						
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				
				if($bulanan==1){
					$ObjPHPExcel->getActiveSheet()->setCellValue('R'.$cel, ($field2->v_unitprice * $saldosfk[$j2]));
				}else{
					$ObjPHPExcel->getActiveSheet()->setCellValue('R'.$cel, "");
				}
				$ObjPHPExcel->getActiveSheet()->getStyle('R'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				
				if($bulanan==1){
					$ObjPHPExcel->getActiveSheet()->setCellValue('S'.$cel, ($field2->v_unitprice * ($saldosfk[$j2]-$saldoakhirnyaexcel[$j2])));
				}else{
					$ObjPHPExcel->getActiveSheet()->setCellValue('S'.$cel, "");
				}
				$ObjPHPExcel->getActiveSheet()->getStyle('S'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				
				$nilai_order= ($field2->v_unitprice * $do2[$j2]);
				$total_order= $total_order + $nilai_order;
				
				$total_qty_awal	= $total_qty_awal+$field2->n_quantity_awal;
				$total_qty_akhir = $total_qty_akhir+$saldoakhirnyaexcel[$j2];
				$total_bonmmasuk = $total_bonmmasuk+$bonmmasuk2[$j2];
				$total_bbm = $total_bbm+$bbm2[$j2];
				$total_do = $total_do+$do2[$j2];
				$total_bonmkeluar = $total_bonmkeluar+$bonmkeluar2[$j2];
				$total_bbk = $total_bbk+$bbk2[$j2];				
				//$total_sj = $total_sj+$sj2[$j2];
				
				$nomor2++;	
				$cel++;
				$j2++;
				$no2++;	
				
				$nilai_order = '';
			}
			
			$cel2=$cel+1;
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$cel2, 'TOTAL :');			
			$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$cel2, $total_qty_awal);			
			$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$cel2, $total_bonmmasuk);			
			$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$cel2, $total_bbm);
			$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$cel2, $total_do);
			$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$cel2, $total_bbk);					
			$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$cel2, $total_bonmkeluar);	
			//$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$cel2, $total_sj);		
			$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$cel2, $total_qty_akhir);
			$ObjPHPExcel->getActiveSheet()->setCellValue('P'.$cel2, $total_order);
											
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			if($sproduct=='TRUE'){
					$stp = 'stp';
			}else{
					$stp = 'nonstp';
			}	
							
			$files	= $this->session->userdata('gid')."laporan_stok_".$stp."_".$tdofirst."-".$tdolast.".xls";
			$ObjWriter->save("files/".$files);
			
		} else {
			
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Mutasi Stok Gudang Jadi")
				->setSubject("Mutasi Stok")
				->setDescription("Laporan Mutasi Stok Gudang Jadi")
				->setKeywords("Laporan Bulanan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			//$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);			
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:S2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'CV. DUTA SETIA GARMEN');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:S3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', 'Laporan Mutasi Stok Gudang Jadi');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:S4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A6:A7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('B6:B7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Kode Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),						
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('C6:C7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Nama Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('E6:F6');
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Penerimaan');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			$ObjPHPExcel->getActiveSheet()->mergeCells('D6:D7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'S.Awal');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E7', 'B.Masuk');
			$ObjPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F7', 'BBM');
			$ObjPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->mergeCells('G6:J6');
			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Pengeluaran');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				
			$ObjPHPExcel->getActiveSheet()->setCellValue('G7', 'DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H7', 'BBK');
			$ObjPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I7', 'B.Keluar');
			$ObjPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		/*	$ObjPHPExcel->getActiveSheet()->setCellValue('J7', 'SJ');
			$ObjPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				); */

			$ObjPHPExcel->getActiveSheet()->mergeCells('K6:K7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'S.Akhir');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
								
			// tambahan
			$ObjPHPExcel->getActiveSheet()->mergeCells('L6:L7');	
			$ObjPHPExcel->getActiveSheet()->setCellValue('L6', 'S.O.P');
			$ObjPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			$ObjPHPExcel->getActiveSheet()->mergeCells('M6:M7');	
			$ObjPHPExcel->getActiveSheet()->setCellValue('M6', 'Selisih');
			$ObjPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);				
			// End 0f tambahan				

			$ObjPHPExcel->getActiveSheet()->mergeCells('N6:S6');
			$ObjPHPExcel->getActiveSheet()->setCellValue('N6', 'Nilai');
			$ObjPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
					array(
						'font' => array(

						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('N7', 'HJP');
			$ObjPHPExcel->getActiveSheet()->getStyle('N7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('O7', 'S.Awal');
			$ObjPHPExcel->getActiveSheet()->getStyle('O7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('P7', 'DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('P7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('Q7', 'S.Akhir');
			$ObjPHPExcel->getActiveSheet()->getStyle('Q7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('R7', 'S.O.P');
			$ObjPHPExcel->getActiveSheet()->getStyle('R7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('S7', 'Selisih');
			$ObjPHPExcel->getActiveSheet()->getStyle('S7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 12
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');		
			$ObjPHPExcel->getActiveSheet()->getStyle('E6:J6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('E7:J7')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6:M7')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('N6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
			$ObjPHPExcel->getActiveSheet()->getStyle('N7:S7')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEBD');
																								
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');

			if($sproduct=='TRUE'){
					$stp	= 'stp';
			}else{
					$stp	= 'nonstp';
			}
						
			$files	= $this->session->userdata('gid')."laporan_stok_".$stp."_".$tdofirst."-".$tdolast.".xls";
			$ObjWriter->save("files/".$files);			
		}

		$efilename = @substr($files,1,strlen($files));
	
		$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
		$data['isi']		= 'expostoksaatinimrkt/vexpform';
		$this->load->view('template',$data);
	}

	/* Export nya pake Popup Windows 
	di remark
	*/	
	function gexportstoksaatini_pake_popupwindows() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
		$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
		$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
		$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
		$data['list_stoksaatini_kd_brg']	= $this->lang->line('list_stoksaatini_kd_brg');
		$data['list_stoksaatini_nm_brg']	= $this->lang->line('list_stoksaatini_nm_brg');
		$data['list_stoksaatini_s_awal']	= $this->lang->line('list_stoksaatini_s_awal');
		$data['list_stoksaatini_s_akhir']	= $this->lang->line('list_stoksaatini_s_akhir');
		$data['list_stoksaatini_bmm']	= $this->lang->line('list_stoksaatini_bmm');
		$data['list_stoksaatini_bmk']	= $this->lang->line('list_stoksaatini_bmk');
		$data['list_stoksaatini_bbm']	= $this->lang->line('list_stoksaatini_bbm');
		$data['list_stoksaatini_bbk']	= $this->lang->line('list_stoksaatini_bbk');
		$data['list_stoksaatini_do']	= $this->lang->line('list_stoksaatini_do');
		$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lstokopname']	= "";
		$data['limages']	= base_url();
					
		$tdofirst	= $this->uri->segment(4); // yyyy-mm-dd
		$tdolast	= $this->uri->segment(5);
		$sproduct	= $this->uri->segment(6);
		$class	= $this->uri->segment(7);
		
		$extdofirst	= explode("-",$tdofirst,strlen($tdofirst));
		$extdolast	=  explode("-",$tdolast,strlen($tdolast));
		
		$nwdofirst	= $extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0];
		$nwdolast	= $extdolast[2]."/".$extdolast[1]."/".$extdolast[0];
				
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= $nwdofirst;
		$data['tgldoakhir']	= $nwdolast;
		$data['sproduksi']	= $sproduct=='TRUE'?" checked ":"";
		$data['kproduksi']	= $class;
				
		$this->load->model('expostoksaatinimrkt/mclass');
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		
		$qbrgjadi	= $this->mclass->lbrgjadi($sproduct);
		$lstokopname	= "";
		$lstokopname2	= "";
		$no	= 1;
		$j	= 0;
		
		$bonmkeluar	= array();
		$bonmmasuk	= array();
		$bbk	= array();
		$bbm	= array();
		$do	= array();
		
		if($qbrgjadi->num_rows()>0) {
			
			$cc = 1;
			
			foreach($qbrgjadi->result() as $field) {
				$qbonmkeluar	= $this->mclass->lbonkeluar($tdofirst,$tdolast,$field->i_product_motif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}

				$qbonmmasuk	= $this->mclass->lbonmasuk($tdofirst,$tdolast,$field->i_product_motif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk; 
				} else {
					$bonmmasuk[$j] = 0;
				}

				$qbbk	= $this->mclass->lbbk($tdofirst,$tdolast,$field->i_product_motif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk; 
				} else {
					$bbk[$j] = 0;
				}

				$qbbm	= $this->mclass->lbbm($tdofirst,$tdolast,$field->i_product_motif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm; 
				} else {
					$bbm[$j] = 0;
				}

				$qdo	= $this->mclass->ldo($tdofirst,$tdolast,$field->i_product_motif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}

				$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
				$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
																										
				$lstokopname	.=	
				"<tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
				  <td height=\"20px;\" bgcolor=\"$bgcolor\">".$no."</td>
				  <td>".$field->i_product_motif."</td>
				  <td>".$field->e_product_motifname."</td>
				  <td>".$field->n_quantity_awal."</td>
				  <td>".$field->n_quantity_akhir."</td>
				  <td>".$bonmmasuk[$j]."</td>
				  <td>".$bbm[$j]."</td>
				  <td>".$do[$j]."</td>
				  <td>".$bonmkeluar[$j]."</td>
				  <td>".$bbk[$j]."</td>
				</tr>";
				$j++;
				$no++;
				$cc++;
			}
		}		
		
		$data['isi']	= $lstokopname;
		
		$this->load->view('expostoksaatinimrkt/vexpform',$data);
	}	
	
	/* Export nya pake Popup Windows 
	di remark
	*/
	function cpopup() {
					
		$tdofirst	= $this->uri->segment(4); // yyyy-mm-dd
		$tdolast	= $this->uri->segment(5);
		$sproduct	= $this->uri->segment(6);
		$class	= $this->uri->segment(7);
		
		$extdofirst	= explode("-",$tdofirst,strlen($tdofirst));
		$extdolast	=  explode("-",$tdolast,strlen($tdolast));
		
		$nwdofirst	= $extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0];
		$nwdolast	= $extdolast[2]."/".$extdolast[1]."/".$extdolast[0];
				
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= $nwdofirst;
		$data['tgldoakhir']	= $nwdolast;
		$data['sproduksi']	= $sproduct=='TRUE'?" checked ":"";
		$data['kproduksi']	= $class;
				
		/*** $this->load->model('expostoksaatini/mclass'); ***/
			
		// Excel
		$qbrgjadi2	= $this->mclass->lbrgjadi($sproduct);
		
		$no2	= 1;
		$j2		= 0;
		$nomor2	= 1;
		
		$lstokopname2= "";
		$bonmkeluar2= array();
		$bonmmasuk2	= array();
		$bbk2	= array();
		$bbm2	= array();
		$do2	= array();
				
		if($qbrgjadi2->num_rows()>0) {
			
			$cc = 1;
																		
			foreach($qbrgjadi2->result() as $field2) {

				$qbonmkeluar2	= $this->mclass->lbonkeluar($tdofirst,$tdolast,$field2->i_product_motif);
				if($qbonmkeluar2->num_rows()>0) {
					$row_bmkeluar2	= $qbonmkeluar2->row();
					$bonmkeluar2[$j2]	= $row_bmkeluar2->jbonkeluar;
				} else {
					$bonmkeluar2[$j2] = 0;
				}

				$qbonmmasuk2	= $this->mclass->lbonmasuk($tdofirst,$tdolast,$field2->i_product_motif);
				if($qbonmmasuk2->num_rows()>0) {
					$row_bmmasuk2	= $qbonmmasuk2->row();
					$bonmmasuk2[$j2]	= $row_bmmasuk2->jbonmasuk; 
				} else {
					$bonmmasuk2[$j2] = 0;
				}

				$qbbk2	= $this->mclass->lbbk($tdofirst,$tdolast,$field2->i_product_motif);
				if($qbbk2->num_rows()>0) {
					$row_bbk2	= $qbbk2->row();
					$bbk2[$j2]	= $row_bbk2->jbbk; 
				} else {
					$bbk2[$j2] = 0;
				}

				$qbbm2	= $this->mclass->lbbm($tdofirst,$tdolast,$field2->i_product_motif);
				if($qbbm2->num_rows()>0) {
					$row_bbm2	= $qbbm2->row();
					$bbm2[$j2]	= $row_bbm2->jbbm; 
				} else {
					$bbm2[$j2] = 0;
				}

				$qdo2	= $this->mclass->ldo($tdofirst,$tdolast,$field2->i_product_motif);
				if($qdo2->num_rows()>0) {
					$row_do2	= $qdo2->row();
					$do2[$j2]	= $row_do2->jdo; 
				} else {
					$do2[$j2] = 0;
				}
				
				$lstokopname2 .= 
				  "<tr>
					<td>".$no2."</td>
					<td>".$field2->i_product_motif."</td>
					<td>".$field2->e_product_motifname."</td>
					<td>".$field2->n_quantity_awal."</td>
					<td>".$bonmmasuk2[$j2]."</td>
					<td>".$bbm2[$j2]."</td>
					<td>".$do2[$j2]."</td>
					<td>".$bbk2[$j2]."</td>
					<td>".$bonmkeluar2[$j2]."</td>
					<td>".$field2->n_quantity_akhir."</td>
					<td>".number_format($field2->v_unitprice,'2','.',',')."</td>
					<td>".number_format($field2->n_quantity_awal * $field2->v_unitprice,'2','.',',')."</td>
					<td>".number_format($do2[$j2] * $field2->v_unitprice,'2','.',',')."</td>
					<td>".number_format($field2->n_quantity_akhir * $field2->v_unitprice,'2','.',',')."</td>
				  </tr> ";
	  			
				$no2++;
				$j2++;
				$nomor2++;
				$cc++;
			}
		}
		
		$data['periode'] = $periode;
		$data['lstokopname2']	= $lstokopname2;
		
		$this->load->view('expostoksaatinimrkt/header_excelfiles',$data);	
	}
}
?>	
