<?php
class Cform extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('schedule-cutting/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$no_item= $this->input->post('no_item', TRUE);  //
	$id_apply_stok_item = $this->input->post('id_apply_stok_item', TRUE); // tm_apply_stok_proses_cutting = id
	$id 	= $this->input->post('id', TRUE);
	$no_pb_cutting	= $this->input->post('no_pb_cutting', TRUE);
	
	$proses_submit	= $this->input->post('submit', TRUE); 
	$idapply_item		= explode(";", $id_apply_stok_item);
	$idapply	= explode(";",$id);
	//$no_pb_cutting	= explode(";",$no_pb_cutting);
	$data['no_pb_cutting']	= $no_pb_cutting;
	$is_dacron = $this->input->post('is_dacron', TRUE);
	    
	$is_realisasi = $this->input->post('is_realisasi', TRUE);    
	$item_brg = $this->input->post('item_brg', TRUE);
	$list_item_brg	= explode(";", $item_brg);
	
	$id_realisasi_detail = $this->input->post('id_realisasi_detail', TRUE);
	$list_id_realisasi_detail	= explode(";", $id_realisasi_detail);
	
	if ($proses_submit == "Proses"){
		$data['msg'] = '';
		$data['go_proses'] = '1';
		
		$thn	= date("Y");
		$t	= $this->mmaster->thnschedule();
		$n	= $this->mmaster->nomorschedule();
			
			if($t->num_rows()>0){
				$row_t	= $t->row();
				$row_n	= $n->row();

				if($thn==$row_t->tahun){
					$nomer		= $row_n->nomor+1;
					
					switch(strlen($nomer)){
						case "1":
							$nomor	= "0000".$nomer;
						break;
						case "2":
							$nomor	= "000".$nomer;
						break;
						case "3":
							$nomor	= "00".$nomer;
						break;
						case "4":
							$nomor	= "0".$nomer;
						break;
						case "5":
							$nomor	= $nomer;
						break;
					}
					$nomernya	= $nomor;
					$tahunnya	= $row_t->tahun;
				}else{
					$nomernya	= "00001";
					$tahunnya	= $thn;
				}
				$data['noschedule']	= "SC"."-".$tahunnya.$nomernya;
			}else{
				$data['noschedule']	= "SC"."-".$thn."00001";
			}
			
		if ($is_dacron == 't') {
			$data['is_dacron'] = $is_dacron;
			$data['bagian_brg_jadi'] = $this->mmaster->get_bagian_brg_jadi();
			$data['isi'] = 'schedule-cutting/vmainformdacron';
			$this->load->view('template',$data);
		}
		else if ($is_realisasi == 't') {
			$data['is_realisasi'] = $is_realisasi;
			$data['realisasi_detail'] = $this->mmaster->get_detail_realisasi($list_id_realisasi_detail);
			$data['isi'] = 'schedule-cutting/vmainformrealisasi';
			$this->load->view('template',$data);
		}
		else {
			//if ($idapply!='' && $idapply!=';'){
				//$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting WHERE id='$id_apply_stok[0]' ");
				//$hasilrow = $query2->row();
				$non_bonm = $this->input->post('non_bonm', TRUE);
				
				if ($non_bonm == '' && $idapply!='' && $idapply!=';') {
					$data['pemenuhan_detail'] = $this->mmaster->get_detail_pemenuhan($idapply, $idapply_item,$no_pb_cutting);
				}
				else
					$data['pemenuhan_detail'] = '';
				
				$data['non_bonm'] = $non_bonm;
				$data['isi'] = 'schedule-cutting/vmainform';
				$this->load->view('template',$data);
			//}
		} // end if
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['isi'] = 'schedule-cutting/vmainform';
		$this->load->view('template',$data);
	}
	
  }
  
  /*function show_popup_brg(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$query	= array();
  		$keywordcari	= $this->input->post('cari')?$this->input->post('cari'):"0";
		$posisi 	= $this->uri->segment(4);
		
		if($posisi=='')
			$posisi 	= $this->uri->segment(4);
			
		$qjum_total = $this->mmaster->barangjditanpalimit($keywordcari);
		$jum_total	= $qjum_total->num_rows();
		
		$config['base_url'] 	= base_url()."index.php/schedule-cutting/cform/show_popup_brg/".$keywordcari."/";
		$config['total_rows'] 	= $jum_total; 
		$config['per_page'] 	= '10';
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $this->uri->segment(5);
		$this->pagination->initialize($config);		
		$data['query'] 		= $this->mmaster->barangjdi($config['per_page'],$config['cur_page'], $keywordcari);
		$data['jum_total']	= count($jum_total);
		$data['posisi']	= $posisi;
		
		if ($keywordcari=='0')
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$this->load->view('schedule-cutting/vpopupbrg',$data);  	
  } */
  
  // khusus dacron
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================

	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	// ini popup bhn dacron yg bukan di detail
	//$posisi	= $this->uri->segment(4);

	/*if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	} */
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
	if ($keywordcari == '') {
		//$posisi	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(4);
	}
		
		if ($keywordcari == '')
			$keywordcari = "all";
		
		$submitnya 	= $this->input->post('submit', TRUE);  
		/*if ($submitnya == "Cari") {
			$posisi 	= $this->input->post('posisi', TRUE);  
		} */

		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari);
		
		//$config['base_url'] = base_url()."index.php/schedule-cutting/cform/show_popup_brg/".$posisi."/";
		$config['base_url'] = base_url()."index.php/schedule-cutting/cform/show_popup_brg/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari);

	$data['jum_total'] = count($qjum_total);
	//$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('schedule-cutting/vpopupbrg',$data);
  }
  
  // utk non-dacron
  function show_popup_brg2(){
	// =======================
	// disini coding utk pengecekan user login
//========================

	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi	= $this->uri->segment(4);

	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
	if ($keywordcari == '' && $posisi == '') {
		$posisi	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
		
	if ($keywordcari == '')
		$keywordcari = "all";
		
	$submitnya 	= $this->input->post('submit', TRUE);  
	if ($submitnya == "Cari") {
		$posisi 	= $this->input->post('posisi', TRUE);  
	}

		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari);
		
		$config['base_url'] = base_url()."index.php/schedule-cutting/cform/show_popup_brg2/".$posisi."/".$keywordcari;
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari);

	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('schedule-cutting/vpopupbrg2',$data);
  }
  
  function edit(){ 
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	$id 	= $this->uri->segment(4);
	$query3	= $this->db->query(" SELECT is_dacron FROM tt_schedule_cutting WHERE id = '$id' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$is_dacron	= $hasilrow->is_dacron;
	}
	
	$data['query'] = $this->mmaster->get_schedule_cutting($id);
	$data['bagian_brg_jadi'] = $this->mmaster->get_bagian_brg_jadi();
	$data['msg'] = '';
	
	if ($is_dacron == 'f')
		$data['isi'] = 'schedule-cutting/veditform';
	else
		$data['isi'] = 'schedule-cutting/veditformdacron';
	$this->load->view('template',$data);
  }
  
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	/*$this->load->library('form_validation');
	$this->form_validation->set_rules('tglschedule', 'Nomor Schedule', 'required');
	$this->form_validation->set_rules('noschedule', 'Tanggal Schedule', 'required');

	if ($this->form_validation->run() == FALSE) {
	} else {   */
		$tglschedule 	= $this->input->post('tglschedule', TRUE);
		$noschedule 	= $this->input->post('noschedule', TRUE);
		//$no_pb_cutting 	= $this->input->post('no_pb_cutting', TRUE);  
		$idheader		= $this->input->post('idheader', TRUE);
		if ($idheader == '')
			$idheader = $this->input->post('id_schedule', TRUE);
		
		$iterasi		= $this->input->post('iterasi', TRUE);
		$no		= $this->input->post('no', TRUE);
		$is_dacron		= $this->input->post('is_dacron', TRUE);
		$kode_bhn_baku		= $this->input->post('kode_bhn_baku', TRUE);
		$is_realisasi		= $this->input->post('is_realisasi', TRUE);
		
		$pisah1 = explode("-", $tglschedule);
		$tgl1	= $pisah1[0];
		$bln1	= $pisah1[1];
		$thn1	= $pisah1[2];
		$tglschedule = $thn1."-".$bln1."-".$tgl1;
		
		if ($is_dacron == '')
			$jumlah_input	= $iterasi;
		else
			$jumlah_input = $no-1;
			
			$tgl = date("Y-m-d");
			$this->db->query(" UPDATE tt_schedule_cutting SET tgl_cutting='$tglschedule', 
								tgl_update = '$tgl' WHERE id='$idheader' ");
			if ($is_dacron == '') {
				$iddetail	= array();	
				//$nmbrgjadi	= array();
				//$imotif	= array();
				$qty	= array();
				$qty_lama	= array();
				//$nmbrg	= array();
				//$kode	= array();
				$jammulai	= array();
				$jamselesai	= array();
				$keterangan	= array();
				$operator	= array();
				$id_detail	= array();
				//$idapplystok_detail	= array();
				//$no_pb_cutting	= array();
				$ii=1;
				for($i=0;$i<$jumlah_input;$i++){
					$iddetail[$i]	= $this->input->post('i_'.$ii, TRUE);
					//$nmbrgjadi[$i]	= $this->input->post('nmbrgjadi_'.$ii, TRUE);
					//$imotif[$i]	= $this->input->post('imotif_'.$ii, TRUE);
					$qty[$i]		= $this->input->post('qty_'.$ii, TRUE);
					$qty_lama[$i]		= $this->input->post('qty_lama_'.$ii, TRUE);
					//$nmbrg[$i]	= $this->input->post('nmbrg_'.$ii, TRUE);
					//$kode[$i]		= $this->input->post('kode_'.$ii, TRUE);
					$jammulai[$i]	= $this->input->post('jammulai_'.$ii, TRUE);
					$jamselesai[$i]	= $this->input->post('jamselesai_'.$ii, TRUE);
					$keterangan[$i]	= $this->input->post('keterangan_'.$ii, TRUE);
					$operator[$i]		= $this->input->post('operator_'.$ii, TRUE);
					$id_detail[$i]		= $this->input->post('id_detail_'.$ii, TRUE);
					//$idapplystok_detail[$i]	= $this->input->post('idapplystok_detail_'.$ii, TRUE);
					//$no_pb_cutting[$i]	= $this->input->post('no_pb_cutting_'.$ii, TRUE);
					
					if($qty[$i]!='' && $jammulai[$i]!='' && $jamselesai[$i]!='' && $operator[$i]!=''){
						/*echo "UPDATE tt_schedule_cutting_detail SET 
							qty_bhn='$qty[$i]',
							jam_mulai='$jammulai[$i]',
							jam_selesai='$jamselesai[$i]',
							operator_cutting='$operator[$i]',
							keterangan='$keterangan[$i]',
							WHERE id='$iddetail[$i]' $idapplystok_detail[$i]<br>"; */
						
						$this->db->query(" UPDATE tt_schedule_cutting_detail SET 
							qty_bhn='$qty[$i]',
							jam_mulai='$jammulai[$i]',
							jam_selesai='$jamselesai[$i]',
							operator_cutting='$operator[$i]',
							keterangan='$keterangan[$i]'
							WHERE id='$iddetail[$i]' ");	
						
						// cek qty di pemenuhan
						if ($id_detail[$i] != 0) {
							// 16-01-2012,cek apakah sum qty_bhn di schedule_cutting = yg di pemenuhan. kalo udh =, maka update status cutting = 't'
							$query3 = $this->db->query(" select sum(qty_bhn) as jum FROM tt_schedule_cutting_detail 
											WHERE id_apply_stok_detail = '$id_detail[$i]' ");
							if ($query3->num_rows()>0){
								$hasilrow = $query3->row();
								$jum_dischedule = $hasilrow->jum;
							}
							else
								$jum_dischedule = 0;
							
							// ambil data plan_qty dan id_apply_stok
							$query3 = $this->db->query(" select id_apply_stok, plan_qty_cutting FROM tm_apply_stok_proses_cutting_detail 
											WHERE id = '$id_detail[$i]' ");
							if ($query3->num_rows()>0){
								$hasilrow = $query3->row();
								$plan_qty_cutting = $hasilrow->plan_qty_cutting;
								$id_apply_stok = $hasilrow->id_apply_stok;
							}
							else {
								$plan_qty_cutting = 0;
								$id_apply_stok = 0;
							}
							
							$selisihnya = $plan_qty_cutting-$jum_dischedule;
							if ($selisihnya <= 0)
								$this->db->query(" UPDATE tm_apply_stok_proses_cutting_detail SET status_cutting='t' WHERE id='$id_detail[$i]' ");
							else
								$this->db->query(" UPDATE tm_apply_stok_proses_cutting_detail SET status_cutting='f' WHERE id='$id_detail[$i]' ");
							
							//cek di tabel tm_apply_stok_proses_cutting_detail, apakah status_cutting sudah 't' semua?
							$this->db->select("id from tm_apply_stok_proses_cutting_detail WHERE status_cutting = 'f' 
										AND id_apply_stok = '$id_apply_stok' ", false);
							$query = $this->db->get();
							//jika sudah t semua, maka update tabel tm_apply_stok_proses_cutting di field status_cutting menjadi t
							if ($query->num_rows() == 0){
								$this->db->query(" UPDATE tm_apply_stok_proses_cutting SET status_cutting='t' WHERE id='$id_apply_stok' ");
							}
						} // end if id_detail !=0
						
					}
					$ii+=1;	
				}
				//die();

			}// end if dacron
			else {
				// DACROON
				$kode_brg_jadi	= array();
				$qty	= array();
				//$kode_bhn_baku	= array();
				$jammulai	= array();
				$jamselesai	= array();
				$bagian_brg_jadi = array();
				$uk_pola = array();
				$ket	= array();
				//$operator	= array();
				$id_detail	= array();

				$ii=1;
				for($i=0;$i<$jumlah_input;$i++){
					$kode_brg_jadi[$i]	= $this->input->post('kode_brg_jadi_'.$ii, TRUE);
					$qty[$i]		= $this->input->post('qty_'.$ii, TRUE);
					$jammulai[$i]	= $this->input->post('jammulai_'.$ii, TRUE);
					$jamselesai[$i]	= $this->input->post('jamselesai_'.$ii, TRUE);
					$ket[$i]	= $this->input->post('ket_'.$ii, TRUE);
					$bagian_brg_jadi[$i]		= $this->input->post('bagian_brg_jadi_'.$ii, TRUE);
					$uk_pola[$i]		= $this->input->post('uk_pola_'.$ii, TRUE);
					//$operator[$i]		= $this->input->post('operator_'.$ii, TRUE);
					$id_detail[$i]		= $this->input->post('id_detail_'.$ii, TRUE);
					
					//if($kode_brg_jadi[$i]!='' && $qty[$i]!='' && $kode[$i]!='' && $jammulai[$i]!='' && $jamselesai[$i]!='' && $operator[$i]!=''){
						$this->db->query(" UPDATE tt_schedule_cutting_detail SET 
							kode_brg_jadi='$kode_brg_jadi[$i]',
							kode_brg='$kode_bhn_baku',
							qty_bhn='$qty[$i]',
							jam_mulai='$jammulai[$i]',
							jam_selesai='$jamselesai[$i]',
							keterangan='$ket[$i]',
							id_bagian_brg_jadi ='$bagian_brg_jadi[$i]' ,
							ukuran_pola ='$uk_pola[$i]' 
							WHERE id='$id_detail[$i]' ");	
					//}
					
					$ii+=1;	
				}
			}
	//}	
	redirect('schedule-cutting/cform/view');		
  }
  
  function show_popup_pemenuhan() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$query	= array();
		
  		$keywordcari	= $this->input->post('cari');
		if($keywordcari=='')
			$keywordcari	= "all";
		
		//$jum_total = $this->mmaster->get_pemenuhantanpalimit($keywordcari);
		
		/*$config['base_url'] 	= base_url()."index.php/schedule-cutting/cform/show_popup_pemenuhan/".$keywordcari."/";
		$config['total_rows'] 	= count($jum_total);
		$config['per_page'] 	= '10';
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $this->uri->segment(5);
		$this->pagination->initialize($config); */
		//$data['query'] 		= $this->mmaster->get_pemenuhan($config['per_page'],$this->uri->segment(5), $keywordcari);
		$data['query'] 		= $this->mmaster->get_pemenuhan($keywordcari);
		//$data['jum_total']	= count($jum_total);
		//print_r($data['query']); die();
		
		if ($keywordcari=="all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$this->load->view('schedule-cutting/vpopuppemenuhan',$data);
  }
  
  // 18-01-2012
  function show_popup_realisasi() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
				
		$data['query'] 		= $this->mmaster->get_item_realisasi();
		//print_r($data['query']);
		$this->load->view('schedule-cutting/vpopuprealisasi',$data);
  }

  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	/*$this->load->library('form_validation');
	$this->form_validation->set_rules('tglschedule', 'Nomor Schedule', 'required');
	$this->form_validation->set_rules('noschedule', 'Tanggal Schedule', 'required'); */

	//if ($this->form_validation->run()==FALSE) {
	//} else {
		$tglschedule 	= $this->input->post('tglschedule', TRUE);
		$noschedule 	= $this->input->post('noschedule', TRUE);
		//$no_pb_cutting 	= $this->input->post('no_pb_cutting', TRUE);  
		$no		= $this->input->post('no', TRUE);
		//$iterasi		= $this->input->post('iterasi', TRUE);
		$is_realisasi	= $this->input->post('is_realisasi', TRUE);
		//29-03-2012
		$non_bonm 	= $this->input->post('non_bonm', TRUE);
		$eksternal 	= $this->input->post('eksternal', TRUE);
		if ($eksternal == '')
			$eksternal = 'f';
		
		$pisah1 = explode("-", $tglschedule);
		$tgl1	= $pisah1[0];
		$bln1	= $pisah1[1];
		$thn1	= $pisah1[2];
		$tglschedule = $thn1."-".$bln1."-".$tgl1;
			
		$cek_data = $this->mmaster->cek_data($noschedule);
		if($cek_data->num_rows()>0) { 
			$data['isi'] 	= 'schedule-cutting/vmainform';
			$data['msg'] 	= "Data no Schedule ".$noschedule." sudah ada..!";
			$data['go_proses'] = '';
			$this->load->view('template',$data);
		}else{
			//$jumlah_input	= $iterasi;
			$jumlah_input	= $no-1;
			
			$nmbrgjadi	= array();
			$imotif	= array();
			$qty	= array();
			
			if ($is_realisasi == '') {
				$plan_qty	= array();
				$qty_pjg_kain	= array();
				$id_detail	= array();
				$idapplystok_detail	= array();
				$no_pb_cutting	= array();
			}
				
			$diprint	= array();
			$nmbrg	= array();
			$kode	= array();
			$jammulai	= array();
			$jamselesai	= array();
			$keterangan	= array();
			$operator	= array();
			
			if ($is_realisasi == 't') {
				$id_realisasi_detail = array();
				$id_realisasi_cutting = array();
				$qty_schedule = array();
				$qty_potong = array();
			}
			
			//for($i=0;$i<=$jumlah_input;$i++){
			for($i=1;$i<=$jumlah_input;$i++){
				$nmbrgjadi[$i]	= $this->input->post('brg_jadi_'.$i, TRUE);
				$imotif[$i]	= $this->input->post('kode_brg_jadi_'.$i, TRUE);
				$qty[$i]		= $this->input->post('qty_'.$i, TRUE);
				
				if ($is_realisasi == '') {
					$plan_qty[$i]		= $this->input->post('plan_qty_'.$i, TRUE);
					$qty_pjg_kain[$i]		= $this->input->post('qty_pjg_kain_'.$i, TRUE); // ini utk pengurangan stok hasil quilting, sampe sini 16-02-2012 4:03
					$id_detail[$i]		= $this->input->post('id_detail_'.$i, TRUE);
					$idapplystok_detail[$i]	= $this->input->post('idapplystok_detail_'.$i, TRUE);	
					$no_pb_cutting[$i]	= $this->input->post('no_pb_cutting_'.$i, TRUE);
				}
				
				if ($is_realisasi == 't') {
					$id_realisasi_detail[$i] = $this->input->post('id_realisasi_detail_'.$i, TRUE);
					$id_realisasi_cutting[$i] = $this->input->post('id_realisasi_cutting_'.$i, TRUE);
					$qty_schedule[$i] = $this->input->post('qty_schedule_'.$i, TRUE);
					$qty_potong[$i] = $this->input->post('qty_potong_'.$i, TRUE);
				}
					
				$diprint[$i]	= $this->input->post('diprint_'.$i, TRUE);
				$nmbrg[$i]	= $this->input->post('nmbrg_'.$i, TRUE);
				$kode[$i]		= $this->input->post('kode_'.$i, TRUE);
				$kode_brg_quilting[$i] = $this->input->post('kode_brg_quilting_'.$i, TRUE);
				$jammulai[$i]	= $this->input->post('jammulai_'.$i, TRUE);
				$jamselesai[$i]	= $this->input->post('jamselesai_'.$i, TRUE);
				$keterangan[$i]	= $this->input->post('keterangan_'.$i, TRUE);
				$operator[$i]		= $this->input->post('operator_'.$i, TRUE);
				
			}
			if ($is_realisasi == '') {
				// 29-03-2012, function save ini blm beres, lanjut besok (yg schedule_cutting_detail)
				$this->mmaster->save($eksternal, $non_bonm, $noschedule, $tglschedule, $no_pb_cutting, $nmbrgjadi, $imotif, $qty, 
								$plan_qty, $qty_pjg_kain, $nmbrg, $kode, $diprint, $kode_brg_quilting, $jammulai, $jamselesai, 
								$keterangan, $operator, $id_detail, $idapplystok_detail, $jumlah_input);
			}
			else
				$this->mmaster->saverealisasi($eksternal, $noschedule, $tglschedule, $nmbrgjadi, $imotif, $qty, 
								$nmbrg, $kode, $diprint, $kode_brg_quilting, $jammulai, $jamselesai, 
								$keterangan, $operator, $id_realisasi_detail, $id_realisasi_cutting, 
								$qty_schedule, $qty_potong, $jumlah_input);
		}
	//}
	redirect('schedule-cutting/cform/view');
  }
  
  // 2 jan 2012
  function submitdacron(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$tglschedule 	= $this->input->post('tglschedule', TRUE);
	$noschedule 	= $this->input->post('noschedule', TRUE);
	$kode_bhn_baku 	= $this->input->post('kode_bhn_baku', TRUE);
	$no		= $this->input->post('no', TRUE);
	$eksternal 	= $this->input->post('eksternal', TRUE);
	if ($eksternal == '')
		$eksternal = 'f';
		
	$pisah1 = explode("-", $tglschedule);
	$tgl1	= $pisah1[0];
	$bln1	= $pisah1[1];
	$thn1	= $pisah1[2];
	$tglschedule = $thn1."-".$bln1."-".$tgl1;
		
	$cek_data = $this->mmaster->cek_data($noschedule);
		if($cek_data->num_rows()>0) { 
			$data['isi'] 	= 'schedule-cutting/vmainform';
			$data['msg'] 	= "Data no Schedule ".$noschedule." sudah ada..!";
			$data['go_proses'] = '';
			$this->load->view('template',$data);
		}else{
			$jumlah_input	= $no-1;
			
			$kode_brg_jadi	= array();
			$qty	= array();
			//$qty_bhn	= array();
			//$kode_bhn_baku	= array();
			$jammulai	= array();
			$jamselesai	= array();
			$bagian_brg_jadi = array();
			$uk_pola = array();
			$ket	= array();
			//$operator	= array();
			
			for($i=1;$i<=$jumlah_input;$i++){
				$kode_brg_jadi[$i]	= $this->input->post('kode_brg_jadi_'.$i, TRUE);
				$qty[$i]		= $this->input->post('qty_'.$i, TRUE);
				//$qty_bhn[$i]		= $this->input->post('qty_bhn_'.$i, TRUE);
				//$kode_bhn_baku[$i]		= $this->input->post('kode_bhn_baku_'.$i, TRUE);
				$jammulai[$i] = $this->input->post('jammulai_'.$i, TRUE);
				$jamselesai[$i]	= $this->input->post('jamselesai_'.$i, TRUE);
				$bagian_brg_jadi[$i]	= $this->input->post('bagian_brg_jadi_'.$i, TRUE);
				$uk_pola[$i]	= $this->input->post('uk_pola_'.$i, TRUE);
				$ket[$i]	= $this->input->post('ket_'.$i, TRUE);
				//$operator[$i]		= $this->input->post('operator_'.$i, TRUE);
			}

			$this->mmaster->savedacron($eksternal, $noschedule, $tglschedule, $kode_brg_jadi, $qty, $kode_bhn_baku,
					$jammulai, $jamselesai, $bagian_brg_jadi, $uk_pola, $ket, $jumlah_input);
		}
	
	redirect('schedule-cutting/cform/view');
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'schedule-cutting/vformview';
	$keywordcari	= $this->input->post('keywordcari');
	if($keywordcari=='')
		$keywordcari = '0';
		
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/schedule/cform/view/index/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	
	if ($keywordcari=='0')
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE)?$this->input->post('cari', TRUE):$this->uri->segment(4);  
	
	if ($keywordcari=='')
		$keywordcari	= '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/schedule-cutting/cform/cari/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] 		= $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] 	= count($jum_total);
	$data['isi'] 		= 'schedule-cutting/vformview';
	
	if ($keywordcari=='0')
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('schedule-cutting/cform/view');
  }
  
  //28 des 2011
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		
	$posisi 	= $this->uri->segment(4);
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || $posisi == '' ) {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari);

				$config['base_url'] = base_url()."index.php/schedule-cutting/cform/show_popup_brg_jadi/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('schedule-cutting/vpopupbrgjadi',$data);
  }
  
}
