<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-pembelian-packing/mmaster');
  }

  function lapfakturwip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_unit_packing'] = $this->mmaster->getlistunitpacking(); 
	$data['isi'] = 'info-pembelian-packing/vmainformlapfakturwip';
	$this->load->view('template',$data);
  }
  
  function lapfakturwipview(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
    $data['isi'] = 'info-pembelian-packing/vformviewlapfakturwip';
	$id_unit = $this->input->post('id_unit', TRUE);  
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE); 
	$jenis_masuk = $this->input->post('jenis_masuk', TRUE);

	$querynya = $this->mmaster->get_all_fakturwip($date_from, $date_to, $id_unit, $jenis_masuk);
	$data['query'] = $querynya;
	if (is_array($querynya))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
		
	// ambil data nama unit packing
	$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE id = '$id_unit' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
		$kode_unit	= $hasilrow->nama;
	}
	else
		$nama_unit = 'ALL';
		$kode_unit='';
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
	$data['jenis_masuk'] = $jenis_masuk;
	$this->load->view('template',$data);
  }
  
   function export_excel_lapfakturwip() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_unit = $this->input->post('id_unit', TRUE);  
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE); 
	$jenis_masuk = $this->input->post('jenis_masuk', TRUE);;  		
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_fakturwip_for_print($date_from, $date_to, $id_unit,$jenis_masuk);
		
	$html_data = "
	<table border='1 cellpadding= '1' cellspacing = '1' width='100%'>
	<thead>
	<tr>
			<th colspan='12' align='center'>Laporan Faktur Pembelian Makloon Hasil packing (WIP)</th>
		 </tr>
		 
		 <tr>
			<th colspan='12' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
	 <tr class='judulnya'>
		 <th rowspan='2'>No</th>
		 <th rowspan='2'>Unit packing</th>
		 <th rowspan='2'>No Faktur</th>
		 <th rowspan='2'>Tgl Faktur</th>
		
		 <th colspan='7'>List Brg Jadi</th>		 
		 <th rowspan='2'>Grand Total </th>
	 </tr>
	 <tr class='judulnya'>
		<th>Kode Brg Jadi</th>
		<th>Nama Brg Jadi</th>
		<th>Satuan</th>
		<th>Quantity</th>
		<th>Harga (Rp.)</th>
		<th>Diskon</th>
		<th>Subtotal</th>
		
	 </tr>
	</thead>
	<tbody>";
			if (is_array($query)) {
				$tot_grandtotal = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					//$tot_grandtotal += $query[$j]['grandtotal'];
				} // end header
			}
			else {
				$tot_grandtotal = 0;
			}
			if (is_array($query)) {
				$no = 1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.="<tr class=\"record\">";
				 $html_data.= "<td align='center'>".$no."</td>";
				 $html_data.="<td>".$query[$j]['nama_unit']."</td>";
				  $html_data.=  "<td>".$query[$j]['no_faktur']."</td>";

				 $html_data.=  "<td>".$query[$j]['tgl_faktur']."</td>";
				
				  $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['kode_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
			 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				
				 
				 $html_data.="<td align='center'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				$html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
				$html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data.= $var_detail[$k]['harga'];
				$html_data.=  "" ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data.= $var_detail[$k]['diskon'];
				 $html_data.= ""   ;
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";
				 
				  $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['subtotal'];
				 $html_data.= ""   ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";

				

			  $html_data.=    "<td align='right'>".$query[$j]['grandtotal']."</td>";
				 
				$tot_grandtotal+=$query[$j]['grandtotal'];
				 
				  $html_data.=  "</tr>";

				$no++;
		 	} // end for
		   }

		 $html_data.=" <tr>
			<td colspan='11' align='center'><b>TOTAL</b></td>
			<td align='center'><b> ".number_format($tot_grandtotal,2,',','.'). "</b></td>
		 </tr>
 	</tbody>
</table><br>";
		// ====================================================================

		$nama_file = "laporan_faktur_pembelian_wip_packing";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
}
