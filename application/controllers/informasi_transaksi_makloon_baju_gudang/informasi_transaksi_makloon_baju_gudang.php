<?php
class informasi_transaksi_makloon_baju_gudang extends CI_Controller
{
    public $data = array(
        'halaman' => 'informasi_transaksi_makloon_baju_gudang',        
        'title' => 'Informasi Transaksi Makloon Baju QC',
        'isi' => 'informasi_transaksi_makloon_baju_gudang/informasi_transaksi_makloon_baju_gudang_form'
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('informasi_transaksi_makloon_baju_gudang/informasi_transaksi_makloon_baju_gudang_model', 'informasi_transaksi_makloon_baju_gudang');
    }

   
    public function index()
    {
		$no=$this->input->post('no');
		
        $this->data['values'] = (object) $this->informasi_transaksi_makloon_baju_gudang->default_values;
        $this->data['gudang'] =  $this->informasi_transaksi_makloon_baju_gudang->get_gudang();
		$this->load->view('template', $this->data);
			
    }
    
 public function view($offset=null)
    {
		$no=$this->input->post('no');
		$tanggal_sj_dari = $this->input->post('tanggal_sj_dari',TRUE);
		$tanggal_sj_ke = $this->input->post('tanggal_sj_ke',TRUE);
		$gudang = $this->input->post('gudang',TRUE);
		$id_barang_bb = $this->input->post('id_barang_bb_1',TRUE);
		
		$informasi_transaksi_makloon_baju_gudang = $this->informasi_transaksi_makloon_baju_gudang->get_all_inner_paged($tanggal_sj_dari,$tanggal_sj_ke,$gudang,$id_barang_bb);
		
		if ($informasi_transaksi_makloon_baju_gudang) {
            $this->data['informasi_transaksi_makloon_baju_gudang'] = $informasi_transaksi_makloon_baju_gudang;  
        } else {
            $this->data['informasi_transaksi_makloon_baju_gudang'] = 'Tidak ada data Makloon Baju Gudang Jadi Wip, Silahkan Melakukan '.anchor('/informasi_transaksi_makloon_baju_gudang/informasi_transaksi_makloon_baju_gudang/view', 'Pencarian kembali.', 'class="alert-link"');
        }
		$this->data['tanggal_sj_dari'] = $tanggal_sj_dari;
		$this->data['tanggal_sj_ke'] = $tanggal_sj_ke;
		
		$this->data['gudang'] = $gudang;
		$this->data['isi'] = 'informasi_transaksi_makloon_baju_gudang/informasi_transaksi_makloon_baju_gudang_list';
		$this->load->view('template', $this->data);		
    }
     
}

