<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MEREK BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('merekbrg/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/merekbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']='merekbrg/vmainform';	
		$this->load->view('template',$data);	
	}
function tambah() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MEREK BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('merekbrg/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/merekbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']='merekbrg/vmainformadd';	
		$this->load->view('template',$data);	
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MEREK BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('merekbrg/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/merekbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);			
		
		$data['isi']='merekbrg/vmainform';	
		$this->load->view('template',$data);	
	}

	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']='merekbrg/vmainform';	
		$this->load->view('template',$data);	
	}

	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$vi_brand	= @$this->input->post('ibrand')?@$this->input->post('ibrand'):@$this->input->get_post('ibrand');
		$ve_brand_name	= @$this->input->post('ebrandname')?@$this->input->post('ebrandname'):@$this->input->get_post('ebrandname');
		if( (isset($ve_brand_name) && !empty($ve_brand_name)) && 
		    (isset($vi_brand) && !empty($vi_brand)) ) {
			$this->load->model('merekbrg/mclass');
			$this->mclass->msimpan($vi_brand,$ve_brand_name);
		} else {
			$data['page_title']	= "MEREK BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			$this->load->model('merekbrg/mclass');
	
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= base_url().'index.php/merekbrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		

			$data['isi']='merekbrg/vmainform';	
		$this->load->view('template',$data);	
		}
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
				
		$this->load->model('merekbrg/mclass');

		$qry_merekbrg		= $this->mclass->medit($id);
		if($qry_merekbrg->num_rows() > 0 ) {
			$row_merekbrg	= $qry_merekbrg->row();
			$data['i_brand'] = $row_merekbrg->i_brand;
			$data['e_brand_name'] = $row_merekbrg->e_brand_name;
			$data['c_brand_code'] = $row_merekbrg->c_brand_code;		
		} else {
			$data['i_brand'] = "";
			$data['e_brand_name'] = "";
			$data['c_brand_code'] = "";
		}
		$data['isi']='merekbrg/veditform';	
		$this->load->view('template',$data);	
		
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ibrandcode	= $this->input->post('ibrand')?$this->input->post('ibrand'):$this->input->get_post('ibrand');
		$ebrandname	= $this->input->post('ebrandname')?$this->input->post('ebrandname'):$this->input->get_post('ebrandname');
		$ibrand		= $this->input->post('i_brand')?$this->input->post('i_brand'):$this->input->get_post('i_brand');
		if( !empty($ibrandcode) && 
			!empty($ebrandname) ) {
			$this->load->model('merekbrg/mclass');
			$this->mclass->mupdate($ibrandcode,$ebrandname,$ibrand);
		} else {
			$data['page_title']	= "MEREK BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			$this->load->model('merekbrg/mclass');
	
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= base_url().'index.php/merekbrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
			$data['isi']='merekbrg/vmainform';	
			$this->load->view('template',$data);	
			
		}
	}
	
	/* 20052011
	function actdelete() {
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_brand',array('i_brand'=>$id));
	}
	*/

	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('merekbrg/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_c_brand_code	= $this->input->post('txt_c_brand_code')?$this->input->post('txt_c_brand_code'):$this->input->get_post('txt_c_brand_code');
		$txt_e_brand_name	= $this->input->post('txt_e_brand_name')?$this->input->post('txt_e_brand_name'):$this->input->get_post('txt_e_brand_name');
		
		$data['page_title']	= "MEREK BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('merekbrg/mclass');

		$query	= $this->mclass->viewcari($txt_c_brand_code,$txt_e_brand_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/merekbrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txt_c_brand_code,$txt_e_brand_name,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('merekbrg/vcariform',$data);			
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_c_brand_code	= $this->input->post('txt_c_brand_code')?$this->input->post('txt_c_brand_code'):$this->input->get_post('txt_c_brand_code');
		$txt_e_brand_name	= $this->input->post('txt_e_brand_name')?$this->input->post('txt_e_brand_name'):$this->input->get_post('txt_e_brand_name');
		
		$data['page_title']	= "MEREK BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('merekbrg/mclass');

		$query	= $this->mclass->viewcari($txt_c_brand_code,$txt_e_brand_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/merekbrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txt_c_brand_code,$txt_e_brand_name,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('merekbrg/vcariform',$data);			
	}	
}
?>
