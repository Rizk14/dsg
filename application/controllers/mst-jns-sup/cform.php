<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-jns-sup/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode_jenis = $row->kode_jenis;
			$enama = $row->nama;
		}
	}
	else {
			$eid = '';
			$ekode_jenis = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode_jenis'] = $ekode_jenis;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	$data['msg'] = '';
	$data['query'] = $this->mmaster->getAll();
	
    $data['isi'] = 'mst-jns-sup/vmainform';
	$this->load->view('template',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		//if ($goedit == 1) {
			$this->form_validation->set_rules('kode_jenis', 'Kode jenis', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		//}
		
		if ($this->form_validation->run() == FALSE)
		{
			//redirect('mst-jns-sup/cform');
			$data['isi'] = 'mst-jns-sup/vmainform';
			$data['msg'] = 'Kode Gudang dan Nama harus diisi..!';
			
			$eid = '';
			$ekode_jenis = '';
			$enama = '';
			
			$edit = '';
			$data['eid'] = $eid;
			$data['ekode_jenis'] = $ekode_jenis;
			$data['enama'] = $enama;
			
			$data['edit'] = $edit;
			$data['query'] = $this->mmaster->getAll();
			$this->load->view('template',$data);
		}
		else
		{
		
			$kode_jenis 	= $this->input->post('kode_jenis', TRUE);
			$kodeeditkat 	= $this->input->post('kodeeditkat', TRUE); 
			//~ $kodeeditgud 	= $this->input->post('kodeeditgud', TRUE); 
			$id_jenis	 	= $this->input->post('id_jenis', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$jenis 	= $this->input->post('jenis', TRUE);
			
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($kode_jenis);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-jns-sup/vmainform';
					$data['msg'] = 'Data sudah ada..!';
					
					$eid = '';
					$ekode_jenis = '';
					$enama = '';
					$eid_lokasi = '';
					$ejenis = '';
					$edit = '';
					$data['eid'] = $eid;
					$data['ekode_jenis'] = $ekode_jenis;
					$data['enama'] = $enama;
					$data['ejenis'] = $ejenis;
					$data['edit'] = $edit;
					$data['query'] = $this->mmaster->getAll();
					
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save( $kode_jenis, $id_jenis, $nama, $goedit);
					/*$data['isi'] = 'mst-jns-sup/vmainform';
					$data['msg'] = 'Simpan data berhasil';
					$this->load->view('template',$data); */
					redirect('mst-jns-sup/cform');
				}
			} // end if goedit == ''
			else {
					$cek_data2 = $this->mmaster->cek_data($kode_jenis);
				if ($kode_jenis != $kodeeditkat) {
					if (count($cek_data2) == 0) {
						$this->mmaster->save( $kode_jenis, $id_jenis, $nama, $goedit);
						redirect('mst-jns-sup/cform');
					}
					else {
						$data['isi'] = 'mst-jns-sup/vmainform';
						$data['msg'] = 'Data sudah ada..!';
						
						$edit = '1';
						$data['eid'] = $id_jenis;
						$data['ekode_jenis'] = $kode_jenis;
						$data['enama'] = $nama;
						$data['eid_lokasi'] = $id_lokasi;
						$data['ejenis'] = $jenis;
						$data['edit'] = $edit;
						$data['query'] = $this->mmaster->getAll();
						$this->load->view('template',$data);
					}
				}
				else {
					$this->mmaster->save($kode_jenis, $id_jenis, $nama, $goedit);
					redirect('mst-jns-sup/cform');
				}
			}
		}
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-jns-sup/cform');
  }
  
  
}
