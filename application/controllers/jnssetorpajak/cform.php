<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['page_title_jsetor_pajak']	= $this->lang->line('page_title_jsetor_pajak');
		$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
		$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
		$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
		$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
		$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
		$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
		$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		
		$data['limages']	= base_url();
		
		$this->load->model('jnssetorpajak/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'jnssetorpajak/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']	= 'jnssetorpajak/vmainform';
		$this->load->view('template',$data);
		
	}

function tambah() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['page_title_jsetor_pajak']	= $this->lang->line('page_title_jsetor_pajak');
		$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
		$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
		$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
		$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
		$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
		$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
		$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		
		$data['limages']	= base_url();
		
		$this->load->model('jnssetorpajak/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'jnssetorpajak/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']	= 'jnssetorpajak/vmainformadd';
		$this->load->view('template',$data);
		
	}

	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['page_title_jsetor_pajak']	= $this->lang->line('page_title_jsetor_pajak');
		$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
		$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
		$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
		$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
		$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
		$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
		$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('jnssetorpajak/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'jnssetorpajak/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
$data['isi']	= 'jnssetorpajak/vmainformadd';
		$this->load->view('template',$data);
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']	= 'jnssetorpajak/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$kodeakunpajak1	= @$this->input->post('kodeakunpajak')?@$this->input->post('kodeakunpajak'):@$this->input->get_post('kodeakunpajak');
		$kodejenissetor1	= @$this->input->post('kodejenissetor')?@$this->input->post('kodejenissetor'):@$this->input->get_post('kodejenissetor');
		$uraianpembayaran1	= @$this->input->post('uraianpembayaran')?@$this->input->post('uraianpembayaran'):@$this->input->get_post('uraianpembayaran');
		$e_note1	= @$this->input->post('e_note')?@$this->input->post('e_note'):@$this->input->get_post('e_note');
		
		if( (isset($kodeakunpajak1) && !empty($kodeakunpajak1)) &&
		    (isset($kodejenissetor1) && !empty($kodejenissetor1)) && 
		    (isset($uraianpembayaran1) && !empty($uraianpembayaran1)) ) {
			$this->load->model('jnssetorpajak/mclass');
			$this->mclass->msimpan($kodeakunpajak1,$kodejenissetor1,$uraianpembayaran1,$e_note1);
		} else {
			$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
			$data['page_title_jsetor_pajak']	= $this->lang->line('page_title_jsetor_pajak');
			$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
			$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
			$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
			$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
			$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
			$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
			$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
			$data['txt_cari']	= $this->lang->line('text_cari');
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			
			$data['limages']	= base_url();
			
			$this->load->model('jnssetorpajak/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= 'jnssetorpajak/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);			
$data['isi']	= 'jnssetorpajak/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	function listakunpajak() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
		$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
		$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
		$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
		$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
		$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
		$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('jnssetorpajak/mclass');
		$data['query']	= $this->mclass->listpquery($tabel="tr_akun_pajak",$order="ORDER BY i_akun_pajak ",$filter="");	
		
		$this->load->view('jnssetorpajak/vlistformakunpajak',$data);
		
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
		$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
		$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
		$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
		$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
		$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
		$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
		$data['txt_cari']	= $this->lang->line('text_cari');
			
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
				
		$this->load->model('jnssetorpajak/mclass');

		$qry_jnssetor		= $this->mclass->medit($id);
		if($qry_jnssetor->num_rows() > 0 ) {
			$row_jnssetor	= $qry_jnssetor->row();
			$data['isetoran']	= $row_jnssetor->i_setoran;
			$data['iakunpajak']	= $row_jnssetor->i_akun_pajak;
			$data['ijsetorpajak']	= $row_jnssetor->i_jsetor_pajak;
			$data['ejsetorpajak']	= $row_jnssetor->e_jsetor_pajak;
			$data['keterangan']	= $row_jnssetor->keterangan;
		} else {
			$data['isetoran']	= "";
			$data['iakunpajak']	= "";
			$data['ijsetorpajak']	= "";
			$data['ejsetorpajak']	= "";
			$data['keterangan']	= "";
		}
		$data['isi']	= 'jnssetorpajak/veditform';
		$this->load->view('template',$data);
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$isetoran1	= @$this->input->post('isetoran')?@$this->input->post('isetoran'):@$this->input->get_post('isetoran');
		$kodeakunpajak1	= @$this->input->post('kodeakunpajak')?@$this->input->post('kodeakunpajak'):@$this->input->get_post('kodeakunpajak');
		$kodejenissetor1	= @$this->input->post('kodejenissetor')?@$this->input->post('kodejenissetor'):@$this->input->get_post('kodejenissetor');
		$uraianpembayaran1	= @$this->input->post('uraianpembayaran')?@$this->input->post('uraianpembayaran'):@$this->input->get_post('uraianpembayaran');
		$e_note1	= @$this->input->post('e_note')?@$this->input->post('e_note'):@$this->input->get_post('e_note');
		
		if( (isset($kodeakunpajak1) && !empty($kodeakunpajak1)) &&
		    (isset($kodejenissetor1) && !empty($kodejenissetor1)) && 
		    (isset($uraianpembayaran1) && !empty($uraianpembayaran1)) ) {
			$this->load->model('jnssetorpajak/mclass');
			$this->mclass->mupdate($isetoran1,$kodeakunpajak1,$kodejenissetor1,$uraianpembayaran1,$e_note1);
		} else {
			$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
			$data['page_title_jsetor_pajak']	= $this->lang->line('page_title_jsetor_pajak');
			$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
			$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
			$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
			$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
			$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
			$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
			$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
			$data['txt_cari']	= $this->lang->line('text_cari');
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			
			$data['limages']	= base_url();
			
			$this->load->model('jnssetorpajak/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= 'jnssetorpajak/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
	
			$data['isi']	= 'jnssetorpajak/vmainform';
		$this->load->view('template',$data);
		}		
	}
	
	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('jnssetorpajak/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txtcar	= $this->input->post('txtcari')?$this->input->post('txtcari'):$this->input->get_post('txtcari');
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['page_title_jsetor_pajak']	= $this->lang->line('page_title_jsetor_pajak');
		$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
		$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
		$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
		$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
		$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
		$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
		$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('jnssetorpajak/mclass');
		$query	= $this->mclass->viewcari($txtcar);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'jnssetorpajak/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txtcar,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('jnssetorpajak/vcariform',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txtcar	= $this->input->post('txtcari')?$this->input->post('txtcari'):$this->input->get_post('txtcari');
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['page_title_jsetor_pajak']	= $this->lang->line('page_title_jsetor_pajak');
		$data['form_panel_daftar_jsetor_pajak']	= $this->lang->line('form_panel_daftar_jsetor_pajak');
		$data['form_panel_form_jsetor_pajak']	= $this->lang->line('form_panel_form_jsetor_pajak');
		$data['form_panel_cari_jsetor_pajak']	= $this->lang->line('form_panel_cari_jsetor_pajak');
		$data['form_kode_jsetor_pajak']	= $this->lang->line('form_kode_jsetor_pajak');
		$data['form_kode_kode_jsetor_pajak']	= $this->lang->line('form_kode_kode_jsetor_pajak');
		$data['form_jns_setoran']		= $this->lang->line('form_jns_setoran');
		$data['form_keterangan_jsetor_pajak']	= $this->lang->line('form_keterangan_jsetor_pajak');
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('jnssetorpajak/mclass');
		$query	= $this->mclass->viewcari($txtcar);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'jnssetorpajak/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txtcar,$pagination['per_page'],$pagination['cur_page']);

		$this->load->view('jnssetorpajak/vcariform',$data);
	}	
}
?>
