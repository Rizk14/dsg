<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-rekap-op/mmaster');
  }
  
  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'info-rekap-op/vmainform';
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$this->load->view('template',$data);


  }

  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	
	  
    $data['isi'] = 'info-rekap-op/vformview';
  
	$jenis_beli = $this->input->post('jenis_beli', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$id_supplier = $this->input->post('id_supplier', TRUE);  
	//print_r($id_supplier);
	
	$data['query'] = $this->mmaster->getAll($jenis_beli, $date_from, $date_to,$id_supplier);
	$data['jum_total'] = count($data['query']);
	
	

	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$data['id_supplier'] = $id_supplier;
	$this->load->view('template',$data);
  }
  
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$id_supplier = $this->input->post('id_supplier', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->getAll($jenis_beli, $date_from, $date_to,$id_supplier);

		
		
		$html_data= "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
	<thead>
	<tr>
		 <th>No PP</th>
		 <th>No OP</th>
		 <th>Tgl OP</th>
		 <th>Supplier</th>
		 <th>List Barang</th>
		 <th>Satuan</th>
		 <th>Qty</th>
		 <th>Harga (Rp.)</th>
		 <th>Diskon (Rp.)</th>
		 <th>Subtotal (Rp.)</th>
		 <th>Grandtotal (Rp.)</th>
		 <th>Qty Pemenuhan SJ</th>
		 <th>Keterangan SJ</th>
		 <th>Qty Sisa</th>
		 <th>Status</th> 
		 <th>Keterangan</th> 
	</tr>
	</thead>
	<tbody>";
			// if (is_array($query)) {
			// 	for ($a =0;$b =count($query;$a++){
			// 		$ket = $this->mmaster->getAll($query[$a]['id']);
			// 		$ket = $this->$ket->num_rows();
					
			// 	 $html_data.= "<td align='right'>".$Keterangan."</td>";
			// 	})
			// }
		 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				$qstatusop	= $this->mmaster->statusfakturop($query[$j]['id']);
				$jstatusop	= $qstatusop->num_rows();
				if($jstatusop>0) {
					$jmlop	= $jstatusop;
				} else {
					$jmlop	= 0;
				}
				 
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				$pisah1 = explode("-", $query[$j]['tgl_op']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_op = $tgl1." ".$nama_bln." ".$thn1;
				
				if ($query[$j]['jenis_pembelian'] == '1')
					$jenis = "Cash";
				else if ($query[$j]['jenis_pembelian'] == '2')
					$jenis = "Kredit";
				else
					$jenis = "-";
				 
				 $html_data.=	 "<tr class=\"record\">";
				 $html_data.=   "<td>".$query[$j]['no_pp']."</td>";
				 $html_data.=   "<td style='white-space:nowrap;'>".$query[$j]['no_op']." (".$jenis.")</td>";
				 $html_data.=    "<td>".$tgl_op."</td>";
				 $html_data.=    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				
				 
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					// $total_qty =0;
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.=  $var_detail[$k]['qty'];
						  //	$total_qty += $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 // 22-09-2014
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= 
						  number_format( $var_detail[$k]['harga'],2,',','.');
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 // 02-07-2015
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						   number_format( $var_detail[$k]['diskon'],2,',','.');
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= number_format($var_detail[$k]['subtotal'], 2, ',','.');
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
					$html_data.= number_format($query[$j]['grandtotal'], 2, ',','.');
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= number_format($var_detail[$k]['jum_beli'],2,'.','');
						 
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 
						  if ($var_detail[$k]['count_beli'] > 1)
							 $html_data.= " (".$var_detail[$k]['count_beli']." SJ)";
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= number_format($var_detail[$k]['sisanya'],2,'.','');
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.=    "<td>";
				 if ($query[$j]['status_aktif'] == 't') $html_data.= "Aktif"; else $html_data.= "Non-Aktif";
				 $html_data.= "</td>";

				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					// $total_qty =0;
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.=  $var_detail[$k]['keterangan'];
						  //	$total_qty += $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";



				 
				// $total_pembantu += $query[$j]['jum_total_pembantu'];
				// $total_semua += $totalnya; 		 
				 $html_data.=  "</tr>";
		 	}
		   }
		
				 $html_data.=  "</tr>";
			
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
			if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
											$total_harga[]=$var_detail[$k]['harga'];
											$total_qty[]=$var_detail[$k]['qty'];
											$total_subtotal[]=$var_detail[$k]['subtotal'];
											$total_grandtotal[]= ($query[$j]['grandtotal']);
		  $totalharga = array_sum($total_harga);
		  $totalqty = array_sum($total_qty);
		  $totalsubtotal = array_sum($total_subtotal);
		  $totalgrandtotal = array_sum($total_grandtotal);								
			}
		}
	}
}	 $html_data.= "<tr>
			<td>&nbsp;</td>
			<td colspan='5'><b>TOTAL</b></td>";
	
			if ((isset($totalqty)) ||(isset($totalharga))){
		$html_data.= "<td align='right'>".$totalqty."</td>";
		$html_data.= "<td>&nbsp;</td>";
	}
			
		$html_data.= "<td>&nbsp;</td>";
		if ((isset($totalqty)) ||(isset($totalharga))){
		$html_data.= "<td align='right' colspan='2'>".number_format($totalsubtotal,2,',','.')."</td>";
	 }
		$html_data.= "<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			
		 </tr>
		 </tr>
		 </tr>
		 </tr>
 	</tbody>
</table><br>";
$nama_file = "rekap_op";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		
		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
}
}
  
  ?>
