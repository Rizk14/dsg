<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmkeluar-makloon/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['bonm_detail'] = $this->mmaster->get_detail_bonm();
	$th_now	= date("Y");
	$data['msg'] = '';
	$data['isi'] = 'bonmkeluar-makloon/vmainform';
	$this->load->view('template',$data);
  }
  
  function edit(){
	$id_apply_stok 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_bonm($id_apply_stok);
	$data['msg'] = '';
	$data['id_apply_stok'] = $id_apply_stok;

	$data['isi'] = 'bonmmasuk/veditform';
	$this->load->view('template',$data);

  }
    
  function show_popup_sj(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	// sj pembelian
	
	$id_gudang 	= $this->uri->segment(4);
	$csupplier 	= $this->uri->segment(5);
	if ($csupplier == '')
		$csupplier 	= $this->input->post('csupplier', TRUE);  
	if ($id_gudang == '')
		$id_gudang 	= $this->input->post('id_gudang', TRUE);  
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jum_total = $this->mmaster->get_sjtanpalimit($csupplier, $id_gudang, $keywordcari);
							$config['base_url'] = base_url()."index.php/bonmmasuk/cform/show_popup_sj/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_sj($config['per_page'],$this->uri->segment(5), $csupplier, $id_gudang, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$data['csupplier'] = $csupplier;
	$data['id_gudang'] = $id_gudang;

	$this->load->view('bonmmasuk/vpopupsj',$data);

  }

  function submit(){			
	  $tgl = date("Y-m-d");
			$list_detail 	= $this->input->post('list_detail', TRUE);
			
			$id_detail = explode(",", $list_detail);
			foreach($id_detail as $row1) {
				if ($row1 != '') {
					// update statusnya!
					$this->db->query(" UPDATE tm_apply_stok_proses_quilting_detail SET status_stok = 't' WHERE id = '$row1' ");
					
					// ambil id headernya
					$query3	= $this->db->query(" SELECT id_apply_stok FROM tm_apply_stok_proses_quilting_detail 
							WHERE id = '$row1' ");
					$hasilrow = $query3->row();
					$id_apply_stok	= $hasilrow->id_apply_stok;
					
					// ambil field2 detailnya
					$query3	= $this->db->query(" SELECT kode_brg, qty FROM tm_apply_stok_proses_quilting_detail
							WHERE id = '$row1' ");
					$hasilrow = $query3->row();
					$kode_brg	= $hasilrow->kode_brg;
					$qty	= $hasilrow->qty;
					
					//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
					$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_proses_quilting_detail WHERE status_stok = 'f' 
										AND id_apply_stok = '$id_apply_stok' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" UPDATE tm_apply_stok_proses_quilting SET status_stok = 't' WHERE id= '$id_apply_stok' ");
					}
										
					// ambil no pb quilting
					$query3	= $this->db->query(" SELECT no_pb_quilting, no_bonm FROM tm_apply_stok_proses_quilting 
							WHERE id = '$id_apply_stok' ");
					$hasilrow = $query3->row();
					$no_pb_quilting	= $hasilrow->no_pb_quilting;
					$no_bonm	= $hasilrow->no_bonm;
					
					// ambil id quilting
					$query3	= $this->db->query(" SELECT id FROM tm_pb_quilting 
							WHERE no_pb_quilting = '$no_pb_quilting' ");
					$hasilrow = $query3->row();
					$id_pb_quilting	= $hasilrow->id;
					
					// new 090411
					//$this->db->query(" UPDATE tm_pb_quilting SET status_edit = 't' where id= '$id_pb_quilting' ");
					//ralat: skrip UPDATE diatas tsb dijalankan di fitur SJ keluar aja 
					
					// update lagi di tm_pb_quilting_detail
					$this->db->query(" UPDATE tm_pb_quilting_detail SET status_pb = 't' WHERE id_pb_quilting = '$id_pb_quilting' 
								AND kode_brg = '$kode_brg' ");
								
					//cek apakah status_pb di tabel detailnya sudah t semua, jika sudah maka update statusnya
					$query3	= $this->db->query(" SELECT id FROM tm_pb_quilting_detail WHERE status_pb = 'f' 
										AND id_pb_quilting = '$id_pb_quilting' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" UPDATE tm_pb_quilting SET status_pb = 't' WHERE id = '$id_pb_quilting' ");
					}
					
					//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty; // berkurang stok karena bon M keluar
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg,
							'stok'=>$new_stok,
						//	'id_gudang'=>$id_gudang, //
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg' ");
					}
					
					$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input) 
											VALUES ('$kode_brg','$no_bonm', '$qty', '$new_stok', '$tgl') ");
					}
			} // end foreach row1
			
			redirect('bonmkeluar-makloon/cform/view');
			//=================================================================== END =============================================

  }
  
  function view(){
    $data['isi'] = 'bonmkeluar-makloon/vformview';
    $keywordcari = "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonmkeluar-makloon/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);				
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonmkeluar-makloon/cform/cari/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'bonmkeluar-makloon/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }


/*  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('retur-beli/cform/view');
  } */
  
}
