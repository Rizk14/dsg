<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('kartuhutang-makloon/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit(); 
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['isi'] = 'kartuhutang-makloon/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			
    $unit_jahit = $this->input->post('unit_jahit', TRUE);
    $unit_packing = $this->input->post('unit_packing', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	$data['query'] = $this->mmaster->get_all_sjpembelian($date_from, $date_to, $unit_jahit, $unit_packing);
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
	$data['isi'] = 'kartuhutang-makloon/vformview';
	
	// ambil data nama supplier
	$query3	= $this->db->query(" SELECT kode_unit, nama, pkp FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$kode_unit_jahit	= $hasilrow->kode_unit;
		$nama_unit_jahit	= $hasilrow->nama;
	
	}
	else {
		$nama_unit_jahit = '';
		$kode_unit_jahit = '';
	
	}
	
	// ------- hitung saldo awal berdasarkan supplier dan range tanggalnya (copy dari rekap hutang dagang) --------------------------
	//$saldo_awal = 0;
	$pisah1 = explode("-", $date_from);
	$thn1= $pisah1[2];
	$bln1= $pisah1[1];
	$tgl1= $pisah1[0];
		
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}
		
	if ($bln1 == '07' && $thn1 == '2015') {
	
			$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
								INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
								WHERE a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.id_supplier = '$supplier' ");
			if ($query2->num_rows() > 0){
				$hasilrow = $query2->row();
				$tot_hutang	= $hasilrow->jum_stok_opname;
				if ($tot_hutang == '')
					$tot_hutang = 0;
			}
			else
				$tot_hutang = 0;
		
	}
	else {
		
			$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian_wip a 
								INNER JOIN tm_unit_jahit b ON a.id_unit_jahit = b.id
								WHERE a.status_aktif = 't'
								AND a.tgl_sjpembelian < to_date('$date_from','dd-mm-yyyy') 
								AND a.tgl_sjpembelian >= '2015-07-01'
								AND b.id = '$unit_jahit' ");
												
			$hasilrow = $query2->row();
			$tot_hutang	= $hasilrow->tot_hutang;
			if ($tot_hutang == '')
				$tot_hutang = 0;
			/*					
			// 09-09-2015, tambahkan hutang dari saldo awal hutang yg diinput pertama kali (= saldo akhir juni 2015)
			$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
								INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
								WHERE a.bulan = '06' AND a.tahun = '2015' AND b.id_supplier = '$supplier' ");
			if ($query2->num_rows() > 0){
				$hasilrow = $query2->row();
				$tot_hutang	+= $hasilrow->jum_stok_opname;
			}
			// ---------------------------------------------
		*/
	}
	
	// hitung pelunasannya				
	$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat 
				FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
				WHERE a.tgl < to_date('$date_from','dd-mm-yyyy')
				AND a.tgl >= '2015-07-01'
				AND b.id_unit_jahit = '$unit_jahit' ");
				
	$hasilrow = $query2->row();
	$tot_bayar	= $hasilrow->tot_bayar;
	$tot_bulat	= $hasilrow->tot_bulat;
	if ($tot_bayar == '')
		$tot_bayar = 0;
	if ($tot_bulat == '')
		$tot_bulat = 0;
								
	
	// 27-08-2015 UNTUK SUPPLIER MAKLOON GA PAKE RETUR
	
	
		$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a 
					INNER JOIN tm_retur_beli_detail c ON a.id = c.id_retur_beli WHERE
					a.id_supplier = '$unit_jahit'
					AND a.tgl_retur < to_date('$date_from','dd-mm-yyyy')
					AND a.tgl_retur >= '2015-07-01' ");
						
		$hasilrow = $query2->row();
		$tot_retur	= $hasilrow->tot_retur;	
		if ($tot_retur == '')
			$tot_retur = 0;
	
								
	//ini asli. 30-06-2015 DIMODIF TANPA UANG MUKA
	//$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar-$tot_retur+$tot_bulat;
	$tot_hutang = $tot_hutang-$tot_bayar-$tot_retur+$tot_bulat;
	$saldo_awal = $tot_hutang;
	// ----------------------------------------------------------------------------------------------
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['unit_jahit'] = $unit_jahit;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['nama_unit_jahit'] = $nama_unit_jahit;
	$data['saldo_awal'] = $saldo_awal;
	$this->load->view('template',$data);
  }
 

}
