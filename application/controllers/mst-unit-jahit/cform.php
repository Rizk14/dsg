<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-unit-jahit/mmaster');
    $this->load->library('pagination');
  }
  
  // 15-06-2015, skrg pake field id utk primary key. nanti di tabel2 lain ganti pake id_unit_jahit, bukan kode_unit_jahit
  function updateidunit() {
	  $sql = " SELECT * FROM tm_unit_jahit ORDER BY tgl_input ";
	  $query	= $this->db->query($sql);
	  
	  if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil id tertinggi
			$sql2 = " SELECT id FROM tm_unit_jahit ORDER BY id DESC LIMIT 1 ";
			$query2	= $this->db->query($sql2);
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->row();
				$idlama	= $hasil2->id;
				$idbaru = $idlama+1;
				
				$this->db->query(" UPDATE tm_unit_jahit SET id='$idbaru' WHERE kode_unit='$row1->kode_unit' ");
			}
		}
	}
	echo "sukses";
  }

  function index(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode_unit;
			$elokasi = $row->lokasi;
			$enama = $row->nama;
			$eadmin = $row->administrator;
			$eperusahaan =$row->perusahaan;
			$epenanggungjawab =$row->penanggungjawab;
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$elokasi = '';
			$enama = '';
			$edit = '';
			$eadmin = '';
			$eperusahaan = '';
			$epenanggungjawab ='';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['elokasi'] = $elokasi;
	$data['enama'] = $enama;
	$data['eadmin'] = $eadmin;
	$data['eperusahaan'] = $eperusahaan;
	$data['epenanggungjawab'] = $epenanggungjawab;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-unit-jahit/vmainform';
    
	$this->load->view('template',$data);
 //   $this->load->view('mst-kel-barang/input',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		// 22-09-2014, form validation ga perlu. skrg pake jquery aja
		
			$kode 	= $this->input->post('kode_unit', TRUE);
			$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE); 
			$lokasi = $this->input->post('lokasi', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			$admin 	= $this->input->post('admin', TRUE);
			$perusahaan 	= $this->input->post('perusahaan', TRUE);
			$penanggungjawab 	= $this->input->post('penanggungjawab', TRUE);
			
			$this->mmaster->save($kode, $id_unit_jahit, $lokasi, $nama,$admin,$perusahaan,$penanggungjawab, $goedit);
			redirect('mst-unit-jahit/cform');
		//}
  }

  function delete(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete($id);
    redirect('mst-unit-jahit/cform');
  }
  
  // 05-04-2014
  function viewgrupjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'mst-unit-jahit/vformviewgrupjahit';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllgrupjahittanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/mst-unit-jahit/cform/viewgrupjahit/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllgrupjahit($config['per_page'],$this->uri->segment(4,0), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function carigrupjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
			
    $jum_total = $this->mmaster->getAllgrupjahittanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/mst-unit-jahit/cform/carigrupjahit/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllgrupjahit($config['per_page'],$this->uri->segment(5,0), $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'mst-unit-jahit/vformviewgrupjahit';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function addgrupjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['msg'] = '';
	$data['list_unit'] = $this->mmaster->get_unit_jahit();
    $data['isi'] = 'mst-unit-jahit/vmainformgrupjahit';
	$this->load->view('template',$data);
  }
  
  function submitgrupjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$unit_jahit 	= $this->input->post('unit_jahit', TRUE);
		$jumgrup	= $this->input->post('jumgrup');
		$namagrup = "";
		
		for ($i=1; $i<=$jumgrup; $i++) {
			$namagrup	.= $this->input->post('nama_grup_'.$i).";";
		}
		
		$this->mmaster->savegrupjahit($unit_jahit,$namagrup);
		redirect('mst-unit-jahit/cform/viewgrupjahit');		
  }
  
  function deletegrupjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->deletegrupjahit($kode);
    redirect('mst-unit-jahit/cform/viewgrupjahit');
  }
  
  function editgrupjahit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_jahit	= $this->uri->segment(4);
		$data['unit_jahit']	= $unit_jahit;
		
		//--------------------------------------------------------------
		$sqlnya	= $this->db->query(" SELECT distinct a.kode_unit, b.nama 
							FROM tm_grup_jahit a, tm_unit_jahit b
							WHERE a.kode_unit = b.kode_unit 
							AND a.kode_unit = '$unit_jahit' ");
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailgrup = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 nama grup berdasarkan unit jahit
				$queryxx = $this->db->query(" SELECT id, nama_grup FROM tm_grup_jahit WHERE kode_unit = '".$rownya->kode_unit."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailgrup[] = array(	'id'=> $rowxx->id,
												'nama_grup'=> $rowxx->nama_grup
												);
					}
				}
				else
					$detailgrup = '';
					
				$outputdata[] = array(	'kode_unit'=> $rownya->kode_unit,
										'nama_unit'=> $rownya->nama,
										'detailgrup'=> $detailgrup
								);
				
				$detailgrup = array();
			} // end for
		}
		else
			$outputdata = '';
		// =====================================================================
		$data['msg'] = '';
		$data['list_unit'] = $this->mmaster->get_unit_jahit();
		$data['query'] = $outputdata;
		$data['isi'] = 'mst-unit-jahit/veditformgrupjahit';
		
		$this->load->view('template',$data);
		//--------------------------------------------------------------
	}
	
	function updategrupjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$unit_jahit 	= $this->input->post('unit_jahit', TRUE);
		$jumgrup	= $this->input->post('jumgrup');
		$namagrup = "";
		
		for ($i=1; $i<=$jumgrup; $i++) {
			$namagrup	.= $this->input->post('nama_grup_'.$i).";";
		}
		
		$this->mmaster->updategrupjahit($unit_jahit, $namagrup);
		redirect('mst-unit-jahit/cform/viewgrupjahit');
  }
  
}
