<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-perubahan-harga/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['isi'] = 'info-perubahan-harga/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $data['isi'] = 'info-perubahan-harga/vformview';
	/*$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);   */
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
/*	if ($date_from == '' && $date_to == '') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
	}
	
		$pisah1 = explode("-", $date_from);
		//$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		
		$pisah2 = explode("-", $date_to);
		//$tgl2= $pisah2[0];
		$bln2= $pisah2[1];
		$thn2= $pisah2[2];
		//$tgl_to2 = $thn2."-".$bln2."-".$tgl2; */
	
	// cek di tabel rekap, apakah ada datanya?
	// koreksi 7 okt 2011: ini sebetulnya udh ga perlu lagi, karena tidak perlu ada penyimpanan data rekap perubahan harga
	$cek_rekap = $this->mmaster->cek_rekap_harga($bulan, $tahun);
	// ga pake paging
		/*	$config['base_url'] = base_url().'index.php/info-perubahan-harga/cform/view/index/'.$date_from.'/'.$date_to.'/';
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config); */
			
	if (count($cek_rekap) == 0) {
		// query dari tabel tt_harga
		// KOREKSI 3 okt 2011: data perubahan harga ini ambilnya dari urutan tgl SJ
		//$jum_total = $this->mmaster->get_hargatanpalimit($date_from, $date_to);
		$jum_total = $this->mmaster->get_harga($bulan, $tahun);
		//$data['query'] = $this->mmaster->get_harga($config['per_page'],$this->uri->segment(7), $date_from, $date_to);
		//$data['query'] = $this->mmaster->get_harga($date_from, $date_to);
		$data['query'] = $this->mmaster->get_harga($bulan, $tahun);
		$is_save = 0;
	}
	else {
		// query dari tabel tm_rekap_perubahan_harga
		$jum_total = $this->mmaster->get_rekap_hargatanpalimit($bulan, $tahun);
		//$data['query'] = $this->mmaster->get_rekap_harga($config['per_page'],$this->uri->segment(7), $bln2, $thn2);
		$data['query'] = $this->mmaster->get_rekap_harga($bulan, $tahun);
		$is_save = 1;
			
	} // end else
	//$config['total_rows'] = count($jum_total); 
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	if ($jum_total == '')
		$data['jum_total'] = 0;
	else
		$data['jum_total'] = count($jum_total);
	$data['nama_bulan'] = $nama_bln;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['is_save'] = $is_save;
	//$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$no 	= $this->input->post('no', TRUE);
	$bulan = $this->input->post('bulan', TRUE);  
	$tahun = $this->input->post('tahun', TRUE);  
	$is_save = $this->input->post('is_save', TRUE);  
	
	for ($i=1;$i<=$no;$i++)
	{
		$this->mmaster->save($bulan,$tahun, $is_save,
			$this->input->post('kode_brg_'.$i, TRUE), $this->input->post('kode_supplier_'.$i, TRUE), 
			$this->input->post('harga_lama_'.$i, TRUE), $this->input->post('harga_baru_'.$i, TRUE),
			$this->input->post('tgl_berubah_'.$i, TRUE), $this->input->post('ket_'.$i, TRUE), $this->input->post('idnya_'.$i, TRUE));
	}
	
	redirect('info-perubahan-harga/cform/');
  }
  
}
