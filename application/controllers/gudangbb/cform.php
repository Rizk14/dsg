<?php

class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('gudangbb/mmaster');
  }

	// 07-08-2015
  function kalkulasiulangstok(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'gudangbb/vformkalkulasiulangstok';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);
  }
  
  function dokalkulasiulangstok() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  // 08-08-2015 DILANJUT
	  $gudang = $this->input->post('gudang', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  // query ke tabel tm_barang 
	  $sql1 = " SELECT id, kode_brg, id_satuan_konversi, rumus_konversi, angka_faktor_konversi FROM tm_barang 
				WHERE id_gudang = '$gudang' ORDER BY kode_brg ";
	  $query1	= $this->db->query($sql1);
	  if ($query1->num_rows() > 0){
		  $hasil1=$query1->result();
		  foreach ($hasil1 as $row1) {
			  $id_brg = $row1->id;
			  $kode_brg = $row1->kode_brg;
			  $id_satuan_konversi = $row1->id_satuan_konversi;
			  $rumus_konversi = $row1->rumus_konversi;
			  $angka_faktor_konversi = $row1->angka_faktor_konversi;
			  
			  // QUERY ke tabel tt_stok_opname_bahan_baku utk acuan data SO
			  $sql2 = " SELECT a.tgl_so, b.jum_stok_opname FROM tt_stok_opname_bahan_baku a INNER JOIN tt_stok_opname_bahan_baku_detail b
						ON a.id = b.id_stok_opname_bahan_baku WHERE a.id_gudang = '$gudang' 
						AND b.id_brg = '$id_brg' AND a.status_approve='t' AND b.status_approve='t' ORDER BY a.id DESC LIMIT 1 ";
			  //if ($id_brg == '650')
				//echo $sql2."<br>";
			  
			  $query2	= $this->db->query($sql2);
			  if ($query2->num_rows() > 0){
				  $hasil2 = $query2->row();
				  $tgl_so = $hasil2->tgl_so;
				  $jum_stok_opname = $hasil2->jum_stok_opname;
				  
				  $totalxx = $jum_stok_opname;
				  
				 // if ($id_brg == '650')
					//echo $totalxx."<br>";
					// ===================================08-08-2015=================================================================
					// 1.ambil brg masuk hasil pembelian
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
									INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									INNER JOIN tm_barang c ON c.id = b.id_brg
									WHERE b.tgl_bonm > '$tgl_so'
									AND b.id_brg = '$id_brg'
									AND b.status_stok = 't' AND c.id_gudang = '$gudang' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk = $hasilrow->jum_masuk;
						if ($jum_masuk == '')
							$jum_masuk = 0;
						
						if ($id_satuan_konversi != 0) {
							if ($rumus_konversi == '1') {
								$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '2') {
								$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '3') {
								$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '4') {
								$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
							}
							else
								$jum_masuk_konv = $jum_masuk;
						}
						else
							$jum_masuk_konv = $jum_masuk;
					}
					else
						$jum_masuk_konv = 0;
					
					// 1.2. ambil brg masuk hasil pembelian manual
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukmanual a 
									INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual
									INNER JOIN tm_barang c ON c.id = b.id_brg
									WHERE a.tgl_bonm > '$tgl_so' AND b.id_brg = '$id_brg'
									AND a.id_gudang = '$gudang' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masukmanual = $hasilrow->jum_masuk;
						if ($jum_masukmanual == '')
							$jum_masukmanual = 0;
						
						if ($id_satuan_konversi != 0) {
							if ($rumus_konversi == '1') {
								$jum_masukmanual_konv = $jum_masukmanual*$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '2') {
								$jum_masukmanual_konv = $jum_masukmanual/$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '3') {
								$jum_masukmanual_konv = $jum_masukmanual+$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '4') {
								$jum_masukmanual_konv = $jum_masukmanual-$angka_faktor_konversi;
							}
							else
								$jum_masukmanual_konv = $jum_masukmanual;
						}
						else
							$jum_masukmanual_konv = $jum_masukmanual;
					}
					else
						$jum_masukmanual_konv = 0;
					
					//echo $jum_masuk_konv." ".$jum_masukmanual_konv."<br>";
					
					// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a 
									INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
									WHERE  a.tgl_bonm > '$tgl_so'
									AND b.id_brg = '$id_brg'
									AND a.id_gudang = '$gudang'
									AND b.is_quilting = 'f' AND ((a.tujuan < '3' OR a.tujuan > '5') AND a.keterangan <> 'DIBEBANKAN') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar = 0;
										
						// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
									INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
									WHERE a.tgl_bonm > '$tgl_so'
									AND b.id_brg = '$id_brg'
									AND a.id_gudang = '$gudang'
									AND b.is_quilting = 'f' AND ((a.tujuan >= '3' AND a.tujuan <= '5') OR a.keterangan = 'DIBEBANKAN' ) "); 
									// AND (a.tujuan > '2' OR a.keterangan = 'DIBEBANKAN')
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_lain = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_lain = 0;
							
						// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
									INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
									WHERE a.tgl_bonm  > '$tgl_so'
									AND b.id_brg = '$id_brg'
									AND a.id_gudang = '$gudang'
									AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_lain = $hasilrow->jum_masuk_lain;
						}
						else
							$jum_masuk_lain = 0;
						
						//echo $jum_keluar." ".$jum_keluar_lain." ".$jum_masuk_lain."<br>";
						
						$totmasuk = $jum_masuk_konv + $jum_masukmanual_konv+ $jum_masuk_lain;
						$totkeluar = $jum_keluar+$jum_keluar_lain;
														
						$jum_stok_akhir = $totmasuk-$totkeluar;
						//echo $jum_stok_akhir."<br>";
					//------------------------------- END query cek brg masuk/keluar ---------------------------------------------

						// tambahkan stok_akhir ini ke qty SO
						$totalxx = $totalxx+$jum_stok_akhir;
						//echo $totalxx."<br><br>";
						
					 //cek stok terakhir tm_stok, dan update stoknya stoknya konversikan balik ke sat awal
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$totalxx_konvawal = $totalxx/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$totalxx_konvawal = $totalxx*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$totalxx_konvawal = $totalxx-$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$totalxx_konvawal = $totalxx+$angka_faktor_konversi;
						}
						else
							$totalxx_konvawal = $totalxx;
					}
					else
						$totalxx_konvawal = $totalxx;
						
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok WHERE id_brg = '$id_brg' ");
						if ($query3->num_rows() == 0){						
							$data_stok = array(
									'id_brg'=>$id_brg,
									'stok'=>$totalxx_konvawal,
									'tgl_update_stok'=>$tgl
									);
							$this->db->insert('tm_stok', $data_stok);
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
							
							$this->db->query(" UPDATE tm_stok SET stok = '$totalxx_konvawal', tgl_update_stok = '$tgl' 
							where id_brg= '$id_brg' ");
						}
						// ====================================================================================================
			  } // END IF2
		  }
	  } // END IF1
	  //die();
	  $query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
	  $hasilrow = $query3->row();
	  $kode_gudang	= $hasilrow->kode_gudang;
	  $nama_gudang	= $hasilrow->nama;
		  
		$data['msg'] = "Kalkulasi stok terkini untuk gudang ".$kode_gudang." - ".$nama_gudang." sudah berhasil dan otomatis mengupdate stok";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'gudangbb/vformkalkulasiulangstok';
		$this->load->view('template',$data);
  }
  
}
