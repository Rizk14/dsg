<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-warna/mmaster');
  }

  function index(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$enama = $row->nama;
		}
	}
	else {
			$eid = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-warna/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('mst-warna/cform');
		}
		else 
		{ */
			$id 	= $this->input->post('id', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == 1) {
				$cek_data = $this->mmaster->cek_data($nama);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id, $nama, $goedit);
				}
			}
			else {
				$cek_data = $this->mmaster->cek_data($nama);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id, $nama, $goedit);
				}
			}
			
			redirect('mst-warna/cform');
		//}
  }

  function delete(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete($id);
    redirect('mst-warna/cform');
  }
  
}
