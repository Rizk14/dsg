<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('konversi-kodebrgjadi/mmaster');
  }
  
  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['list_brg'] = $this->mmaster->get_brgjadi_kode_zero();
    $data['isi'] = 'konversi-kodebrgjadi/vmainform';
	$this->load->view('template',$data);

  }
    
  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  
		$jum = $this->input->post('jum_data', TRUE);
		$tgl = date("Y-m-d");

		for ($i=1; $i<=$jum; $i++)
		{
			if ($this->input->post('cek_'.$i, TRUE) == 'y') {
				$this->db->query(" UPDATE tm_stok_hasil_packing SET kode_brg_jadi = '".$this->input->post('kode_'.$i, TRUE)."'
										WHERE id = '".$this->input->post('idnya_'.$i, TRUE)."' ");
				
			}
		}

		//redirect('set-stok-harga/cform');
		$data['isi'] = 'konversi-kodebrgjadi/vformsukses';
		$this->load->view('template',$data);
		
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'mst-bb-sup/vformview';
    $keywordcari = "all";
    $supplier = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $supplier);	
							$config['base_url'] = base_url().'index.php/mst-bb-sup/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $supplier);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['esupplier'] = $supplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$supplier 	= $this->input->post('supplier', TRUE);  
	//echo $supplier; die();
	if ($keywordcari == '' && $supplier == '') {
		$supplier 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($supplier == '')
		$supplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $supplier);
							$config['base_url'] = base_url().'index.php/mst-bb-sup/cform/cari/index/'.$supplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $supplier);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'mst-bb-sup/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['esupplier'] = $supplier;
	$this->load->view('template',$data);
  }
  
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg 	= $this->uri->segment(4);
	if ($kel_brg == '')
		$kel_brg = $this->input->post('kel_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $kel_brg == '') {
		$kel_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barangtanpalimit($kel_brg, $keywordcari);
					$config['base_url'] = base_url()."index.php/mst-bb-sup/cform/show_popup_jenis/".$kel_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang($config['per_page'],$this->uri->segment(6), $kel_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['kel_brg'] = $kel_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('mst-bb-sup/vpopupjenis',$data);

  }
  
  function show_popup_jenis_bhn(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jns_brg 	= $this->uri->segment(4);
	if ($jns_brg == '')
		$jns_brg = $this->input->post('jns_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $jns_brg == '') {
		$jns_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_bahantanpalimit($jns_brg, $keywordcari);
							$config['base_url'] = base_url()."index.php/mst-bb-sup/cform/show_popup_jenis_bhn/".$jns_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_bahan($config['per_page'],$this->uri->segment(6), $jns_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['jns_brg'] = $jns_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		$hasilrow = $query3->row();
		$nama_jns_brg	= $hasilrow->nama;
	$data['nama_jns_brg'] = $nama_jns_brg;

	$this->load->view('mst-bb-sup/vpopupjenisbhn',$data);

  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $esupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "mst-bb-sup/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-bb-sup/cform/cari/index/".$esupplier."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('mst-bb-sup/cform/view');
  }
  
  
}
