<?php
class informasi_transaksi_makloon_baju extends CI_Controller
{
    public $data = array(
        'halaman' => 'informasi_transaksi_makloon_baju',        
        'title' => 'Informasi Transaksi Makloon Baju',
        'isi' => 'informasi_transaksi_makloon_baju/informasi_transaksi_makloon_baju_form'
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('informasi_transaksi_makloon_baju/informasi_transaksi_makloon_baju_model', 'informasi_transaksi_makloon_baju');
    }

   
    public function index()
    {
		$no=$this->input->post('no');
		
        $this->data['values'] = (object) $this->informasi_transaksi_makloon_baju->default_values;
        $this->data['unit_jahit'] =  $this->informasi_transaksi_makloon_baju->get_unit_jahit();
		$this->load->view('template', $this->data);
			
    }
    
 public function view($offset=null)
    {
		$no=$this->input->post('no');
		$tanggal_sj_dari = $this->input->post('tanggal_sj_dari',TRUE);
		$tanggal_sj_ke = $this->input->post('tanggal_sj_ke',TRUE);
		$unit_jahit = $this->input->post('unit_jahit',TRUE);
		$id_barang_bb = $this->input->post('id_barang_bb_1',TRUE);
		
		$informasi_transaksi_makloon_baju = $this->informasi_transaksi_makloon_baju->get_all_inner_paged($tanggal_sj_dari,$tanggal_sj_ke,$unit_jahit,$id_barang_bb);
		
		if ($informasi_transaksi_makloon_baju) {
            $this->data['informasi_transaksi_makloon_baju'] = $informasi_transaksi_makloon_baju;  
        } else {
            $this->data['informasi_transaksi_makloon_baju'] = 'Tidak ada data Makloon Baju Gudang Jadi Wip, Silahkan Melakukan '.anchor('/informasi_transaksi_makloon_baju/informasi_transaksi_makloon_baju/view', 'Pencarian kembali.', 'class="alert-link"');
        }
		$this->data['tanggal_sj_dari'] = $tanggal_sj_dari;
		$this->data['tanggal_sj_ke'] = $tanggal_sj_ke;
		
		$this->data['unit_jahit'] = $unit_jahit;
		$this->data['isi'] = 'informasi_transaksi_makloon_baju/informasi_transaksi_makloon_baju_list';
		$this->load->view('template', $this->data);		
    }
     
}

