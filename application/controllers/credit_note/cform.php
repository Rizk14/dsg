<?php
class cform extends CI_Controller{
	function __construct (){
		parent::__construct();
		$this->load->model('credit_note/mmaster');
		
	}
	
	function index(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']='credit_note/vformviewcn';
		$tanggal='00-00-0000';
		$no_cnote='0';
		
		$total_row=$this->mmaster->get_hitung_row();
		$this->load->view('template',$data);
		
		
		}
		
	function c_addcnote(){
		$is_logged_in=$this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in!=true){
			redirect('loginform');
		}
		$data['list_supplier']=$this->mmaster->get_supplier();
		$data['isi']='credit_note/vformaddcn';
		$this->load->view('template',$data);
		
		
	}
		
		
	
}
?>
