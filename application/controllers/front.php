<?php

class Front extends CI_Controller {

	function Front()
	{
		parent::__construct();
		$this->is_logged_in();
	}

	// --------------------------------------------------------------------	
	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		else
			redirect('welcome');
	}
}
?>
