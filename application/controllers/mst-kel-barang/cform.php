<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-kel-barang/mmaster');
  }

  function index(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->kode;
			$ekode_perkiraan = $row->kode_perkiraan;
			$enama = $row->nama;
			$eket = $row->keterangan;
		}
	}
	else {
			$ekode = '';
			$ekode_perkiraan = '';
			$enama = '';
			$eket = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['ekode_perkiraan'] = $ekode_perkiraan;
	$data['enama'] = $enama;
	$data['eket'] = $eket;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-kel-barang/vmainform';
    
	$this->load->view('template',$data);
 //   $this->load->view('mst-kel-barang/input',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('mst-kel-barang/cform');
		}
		else
		{
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$kode_perkiraan = $this->input->post('kode_perkiraan', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			$ket 	= $this->input->post('ket', TRUE);
			
			if ($goedit == 1) {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) == 0) { 
						$this->mmaster->save($kode, $kodeedit, $kode_perkiraan, $nama, $ket, $goedit);
					}
				}
				else {
					$this->mmaster->save($kode, $kodeedit, $kode_perkiraan, $nama, $ket, $goedit);
				}
			}
			else {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($kode, $kodeedit, $kode_perkiraan, $nama, $ket, $goedit);
				}
			}
			
			redirect('mst-kel-barang/cform');
		}
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-kel-barang/cform');
  }
  
  
}

