<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-jns-bb/mmaster');
  }

  function index(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode;
			$ekode_kel = $row->kode_kel_brg;
			$enama = $row->nama;
		}
	}
	else {
			$eid='';
			$ekode = '';
			$ekode_kel = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['ekode_kel'] = $ekode_kel;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['jumdata'] = count($data['query']);
    $data['isi'] = 'mst-jns-bb/vmainform';
	$this->load->view('template',$data);
  }

  function submit(){
	//	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		//	$this->form_validation->set_rules('kode', 'Kode', 'required');
		//	$this->form_validation->set_rules('nama', 'Nama', 'required');
					
		//if ($this->form_validation->run() == FALSE)
		//{
		//	redirect('mst-jns-bb/cform');
		//}
		//else
	//	{
			$id_jenis 	= $this->input->post('id_jenis', TRUE);
			$kel_brg 	= $this->input->post('kel_brg', TRUE);
			$kode 	= $this->input->post('kode', TRUE);
			$kodeeditjenis 	= $this->input->post('kodeeditjenis', TRUE); 
			$kodeeditkel 	= $this->input->post('kodeeditkel', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == '') { // insert
				$cek_data = $this->mmaster->cek_data($kel_brg, $kode);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id_jenis, $kode, $kel_brg, $nama, $goedit, $kodeeditjenis, $kodeeditkel);
				}
				redirect('mst-jns-bb/cform');
			
			}
			else { // edit
				$cek_data = $this->mmaster->cek_data($kel_brg, $kode);

				if (($kel_brg != $kodeeditkel) || ($kode != $kodeeditjenis)) {
					if (count($cek_data) == 0) {
						$this->mmaster->save($id_jenis, $kode, $kel_brg, $nama, $goedit, $kodeeditjenis, $kodeeditkel);
						redirect('mst-jns-bb/cform');
					}
					else
						redirect('mst-jns-bb/cform');
				}
				else {
					$this->mmaster->save($id_jenis, $kode, $kel_brg, $nama, $goedit, $kodeeditjenis, $kodeeditkel);
					redirect('mst-jns-bb/cform');
				}
			}
	//	}
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-jns-bb/cform');
  }
  
  
}
