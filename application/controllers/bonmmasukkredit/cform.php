<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmmasukkredit/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================

	$no_sj = $this->input->post('no_sj', TRUE);  
	$kode_supplier = $this->input->post('kode_supplier', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	//$id_pb_detail = $this->input->post('id_brg', TRUE);  
	
	$list_sj = explode(",", $no_sj);
	$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
		if ($no_sj !='') {
			$data['sj_detail'] = $this->mmaster->get_detail_sj($kode_supplier, $list_sj);
			//print_r($data['sj_detail']); die();
			$data['msg'] = '';
			$data['kode_supplier'] = $kode_supplier;
		}
		else {
			$data['msg'] = 'SJ pembelian harus dipilih';
			$data['kode_supplier'] = '';
			$data['supplier2'] = $this->mmaster->get_supplier();
		}
		$data['go_proses'] = '1';
			
		// =======================================================
		$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$kode_supplier' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) 
			$nama_supplier	= $hasilrow->nama;
				
			$data['nama_supplier'] = $nama_supplier;
			$data['kode_supplier'] = $kode_supplier;
	}
	else {
		$data['msg'] = '';
		$data['kode_supplier'] = '';
		$data['go_proses'] = '';
		$data['supplier2'] = $this->mmaster->get_supplier();
	}
	$data['isi'] = 'bonmmasukkredit/vmainform';
	$this->load->view('template',$data);

  }
  
  function edit(){
	$id_bonm 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_bonm($id_bonm);
	$data['msg'] = '';

	$data['isi'] = 'bonm-bbc/veditform';
	$this->load->view('template',$data);

  }
  
  function show_popup_sj(){
	// =======================
	// disini coding utk pengecekan user login
//========================

	$kode_supplier 	= $this->uri->segment(4);
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($kode_supplier == '')
		$kode_supplier = $this->input->post('kode_supplier', TRUE); 
	
	if ($keywordcari == '' && $kode_supplier == '') {
		$kode_supplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari = "all";
	
	if ($kode_supplier == '')
		$kode_supplier = '0';
	
	$jum_total = $this->mmaster->get_sjtanpalimit($kode_supplier, $keywordcari);
						/*	$config['base_url'] = base_url()."index.php/bonmmasukkredit/cform/show_popup_sj/".$kode_supplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		 */
	//$data['query'] = $this->mmaster->get_sj($config['per_page'],$this->uri->segment(6), $kode_supplier, $keywordcari);
	$data['query'] = $this->mmaster->get_sj($kode_supplier, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['kode_supplier'] = $kode_supplier;

	$this->load->view('bonmmasukkredit/vpopupsj',$data);

  }
    
  function show_popup_bonm(){
	// =======================
	// disini coding utk pengecekan user login
//========================

	$id_gudang 	= $this->uri->segment(4);
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($id_gudang == '')
		$id_gudang = $this->input->post('gudang', TRUE); 
	
	if ($keywordcari == '' && $id_gudang == '') {
		$kode_supplier 	= $this->uri->segment(4);
		$id_gudang 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari = "all";
	
	if ($id_gudang == '')
		$id_gudang = '0';
	
	$jum_total = $this->mmaster->get_bonmtanpalimit($id_gudang, $keywordcari);
							$config['base_url'] = base_url()."index.php/bonmmasukkredit/cform/show_popup_bonm/".$id_gudang."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		 
	$data['query'] = $this->mmaster->get_bonm($config['per_page'],$this->uri->segment(6), $id_gudang, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['id_gudang'] = $id_gudang;

	$this->load->view('bonmmasukkredit/vpopupbonm',$data);

  }

  function submit(){
		// 1. create bon m otomatis
		// 2. update status stok dan history stok
			
			$tgl = date("Y-m-d");
			
			$kode_supplier 	= $this->input->post('kode_supplier', TRUE);
			$no 	= $this->input->post('no', TRUE);
			$date_stok 	= $this->input->post('date_stok', TRUE);
			$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);
			
			$pisah1 = explode("-", $date_stok);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$date_stok = $thn1."-".$bln1."-".$tgl1;
			
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			
			// save detailnya
					
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						if ($this->input->post('cek_'.$i, TRUE) == 't') {
							$this->mmaster->create_bonm_update_stok($kode_supplier, $date_stok, $no_bonm, $tgl_bonm,
									$this->input->post('kode_'.$i, TRUE), 
									$this->input->post('no_sj_'.$i, TRUE), $this->input->post('id_detail_'.$i, TRUE), 
									$this->input->post('id_pembelian_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE) );
						}
					}
					redirect('bonmmasukkredit/cform/view');

  }
  
  function updatedata(){
		$id_bonm 	= $this->input->post('id_bonm', TRUE);  
		$no_bonm 	= $this->input->post('no_bonm', TRUE);  
	    $tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);
	    $pisah1 = explode("-", $tgl_bonm);
	    $tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
		
		$no 	= $this->input->post('no', TRUE);
		
		$tgl = date("Y-m-d");
		$this->db->query(" UPDATE tm_apply_stok_proses_cutting SET tgl_bonm = '$tgl_bonm', tgl_update = '$tgl'
						where id= '$id_bonm' ");
		
		$jumlah_input=$no-1;
		for ($i=1;$i<=$jumlah_input;$i++)
		{
			if ($this->input->post('pjg_kain_'.$i, TRUE) != $this->input->post('pjg_kain_lama_'.$i, TRUE) ) {
				$pjg_kain = $this->input->post('pjg_kain_'.$i, TRUE);
				$pjg_kain_lama = $this->input->post('pjg_kain_lama_'.$i, TRUE);
				$kode = $this->input->post('kode_'.$i, TRUE);
				
				// update tabel detailnya
				$this->db->query(" UPDATE tm_apply_stok_proses_cutting_detail SET 
							qty_pjg_kain = '".$this->input->post('pjg_kain_'.$i, TRUE)."', 
							 gelar_pjg_kain = '".$this->input->post('gelar_pjg_kain_'.$i, TRUE)."'
							where id= '".$this->input->post('id_bonm_detail_'.$i, TRUE)."' ");
				
				// 1. reset stoknya #############################################
				// cek dulu satuan brgnya. jika satuannya yard, maka qty (m) yg tadi diambil itu konversikan lagi ke yard
					$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$satuan	= '';
						}
						
						if ($satuan == "Yard") {
							$qty_sat_awal = $pjg_kain_lama / 0.91;
						}
						else
							$qty_sat_awal = $pjg_kain_lama;
				
				//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama+$qty_sat_awal; // menambah stok krn reset
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$kode,
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode' ");
				}
				
				$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input) 
										VALUES ('$kode','$no_bonm', '$qty_sat_awal', '$new_stok', '$tgl') ");
				// ###########################		
				
				
				// 2. update lagi stoknya #############################################
				if ($satuan == "Yard") { // konversikan lagi ke yard
					$qty_sat_awal = $pjg_kain / 0.91;
				}
				else
					$qty_sat_awal = $pjg_kain;
				
				//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama-$qty_sat_awal; // mengurangi stok
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$kode,
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode' ");
				}
				
				$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input) 
										VALUES ('$kode','$no_bonm', '$qty_sat_awal', '$new_stok', '$tgl') ");
				// #############################################
				// end update stok
			}
		} // end for
		redirect('bonm-bbc/cform/view');
  }
  
  function view(){
    $data['isi'] = 'bonmmasukkredit/vformview';
    $keywordcari = "all";
	$cgudang = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($cgudang,$keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasukkredit/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $cgudang, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['cgudang'] = $cgudang;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$cgudang = $this->input->post('gudang', TRUE);  
	
	if ($keywordcari == '' && $cgudang == '') {
		$cgudang 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($cgudang == '')
		$cgudang = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($cgudang, $keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasukkredit/cform/cari/'.$cgudang.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $cgudang, $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'bonmmasukkredit/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['cgudang'] = $cgudang;
	$this->load->view('template',$data);
  }
  
  function revisicash(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================

	$no_bonm = $this->input->post('no_bonm', TRUE);  
	$id_gudang = $this->input->post('gudang', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	
	$list_bonm = explode(",", $no_bonm);
	$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
		if ($no_bonm !='') {
			$data['bonm_detail'] = $this->mmaster->get_detail_bonm($id_gudang, $list_bonm);
			$data['msg'] = '';
			$data['id_gudang'] = $id_gudang;
		}
		else {
			$data['msg'] = 'Bon M harus dipilih';
			$data['id_gudang'] = '';
			$data['list_gudang'] = $this->mmaster->get_gudang();
		}
		$data['go_proses'] = '1';
				
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$id_gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
				
			$data['kode_gudang'] = $kode_gudang;
			$data['nama_gudang'] = $nama_gudang;
			$data['nama_lokasi'] = $nama_lokasi;
	}
	else {
		$data['msg'] = '';
		$data['id_gudang'] = '';
		$data['go_proses'] = '';
		$data['list_gudang'] = $this->mmaster->get_gudang();
	}
	$data['isi'] = 'bonmmasukkredit/vmainformrevisicash';
	$this->load->view('template',$data);

  }
  
  function submitrevisicash(){
		// 1. insert ke history stok, urutannya adalah reset dari masuk lain2 ke keluar lain2, kemudian insert ke "masuk"
			$tgl = date("Y-m-d");
			
			$id_gudang 	= $this->input->post('id_gudang', TRUE);
			$no 	= $this->input->post('no', TRUE);
			$date_stok 	= $this->input->post('date_stok', TRUE);
			
			$pisah1 = explode("-", $date_stok);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$date_stok = $thn1."-".$bln1."-".$tgl1;
			
			// insert transaksi stoknya
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{ 
						if ($this->input->post('cek_'.$i, TRUE) == 't') {
							$this->mmaster->update_stok_cash($id_gudang, $date_stok, $this->input->post('kode_'.$i, TRUE), 
									$this->input->post('no_bonm_'.$i, TRUE), $this->input->post('qty_'.$i, TRUE), 
									$this->input->post('harga_'.$i, TRUE),
									$this->input->post('id_detail_'.$i, TRUE), 
									$this->input->post('id_apply_stok_'.$i, TRUE),
									$this->input->post('no_sj_'.$i, TRUE),
									$this->input->post('kode_supplier_'.$i, TRUE) );
						}
					}
					redirect('bonmmasukkredit/cform/view');

  } 
  
  function stokcash(){
	$id_gudang = $this->input->post('gudang', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	if ($proses_submit == "Proses") {
		$data['bonm_detail'] = $this->mmaster->get_detail_bonm_cash($id_gudang);
		$data['go_proses'] = '1';
		$data['msg'] = '';
		
			// nama gudang
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
								WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$id_gudang' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$data['kode_gudang']	= $hasilrow->kode_gudang;
					$data['nama_gudang']	= $hasilrow->nama;
					$data['nama_lokasi']	= $hasilrow->nama_lokasi;
				}
		
		$data['isi'] = 'bonmmasukkredit/vmainformstokcash';
		$this->load->view('template',$data);
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'bonmmasukkredit/vmainformstokcash';
		$this->load->view('template',$data);
	}
  }
  
  function submitstokcash(){			
	  $tgl = date("Y-m-d");
		$jumdetail 	= $this->input->post('jumdetail', TRUE);
		$no_bonm 	= $this->input->post('no_bonm', TRUE);
		$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);
		
		$pisah1 = explode("-", $tgl_bonm);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
	// ======================================================================
	// NEW 16 AGUSTUS 2011
			
	for ($i=1; $i<=$jumdetail; $i++) {
		//echo $this->input->post('cek_'.$i, TRUE)."<br>";
		if ($this->input->post('cek_'.$i, TRUE) == 'y') {
			echo $i."<br>";
				/*	$this->mmaster->save($no_dn, $tgl_retur,$no_faktur, $kode_supplier,
							$ket, $this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
							$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE), 
							$this->input->post('faktur_'.$i, TRUE) ); */
				$id_detail = $this->input->post('id_detail_'.$i, TRUE);
				$id_apply_stok = $this->input->post('id_apply_stok_'.$i, TRUE);
				$id_pembelian = $this->input->post('id_pembelian_'.$i, TRUE);
				$id_pembelian_detail = $this->input->post('id_pembelian_detail_'.$i, TRUE);
				$kode_supplier = $this->input->post('kode_supplier_'.$i, TRUE);
				$kode_brg = $this->input->post('kode_brg_'.$i, TRUE);
				$qty = $this->input->post('qty_'.$i, TRUE);
				$harga = $this->input->post('harga_'.$i, TRUE);
				
				// update statusnya!
				$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET status_stok = 't', no_bonm = '$no_bonm',
									tgl_bonm = '$tgl_bonm' WHERE id = '$id_detail' ");
				
				//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
				$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE status_stok = 'f' 
									AND id_apply_stok = '$id_apply_stok' ");
				if ($query3->num_rows() == 0){
					$this->db->query(" UPDATE tm_apply_stok_pembelian SET status_stok = 't' WHERE id= '$id_apply_stok' ");
				}
				
				// update lagi di tm_pembelian_detail
				$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 't' 
							WHERE id_pembelian_detail = '$id_pembelian_detail' ");
							
				//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
				$query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE status_stok = 'f' 
									AND id_pembelian = '$id_pembelian' ");
				if ($query3->num_rows() == 0){
					$this->db->query(" UPDATE tm_pembelian SET status_stok = 't' WHERE id = '$id_pembelian' ");
				}
				
				//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg,
							'stok'=>$new_stok,
						//	'id_gudang'=>$id_gudang, //
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg' ");
					}
					
					// 14 juni 2011, ada tabel baru, yaitu tm_stok_harga (utk membedakan stok berdasarkan harga)
							//cek stok terakhir tm_stok_harga, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
														AND harga = '$harga' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty; // utk save ke tt_stok, $new_stok ini yg dipake karena per harga
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_harga, insert
								$data_stok = array(
									'kode_brg'=>$kode_brg,
									'stok'=>$new_stok,
									'harga'=>$harga,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_harga',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode_brg' AND harga = '$harga' ");
							}
					// #########################################################################################
					
					// ambil field stok_lain
					$query3	= $this->db->query(" SELECT stok_lain FROM tm_apply_stok_pembelian 
							WHERE id = '$id_apply_stok' ");
					$hasilrow = $query3->row();
					$stok_lain	= $hasilrow->stok_lain;
					
					if ($stok_lain == 'f')
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, kode_supplier, no_bukti, masuk, saldo, tgl_input, harga) 
										VALUES ('$kode_brg', '$kode_supplier', '$no_bonm', '$qty', '$new_stok', '$tgl', '$harga' ) ");
					else
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, kode_supplier, no_bukti, masuk_lain, saldo, tgl_input, harga) 
										VALUES ('$kode_brg', '$kode_supplier','$no_bonm', '$qty', '$new_stok', '$tgl', '$harga' ) ");
				
		}
	} // end for
	redirect('bonmmasukkredit/cform/view');
  }

}
