<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-marker/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['motif_brg_jadi'] = $this->mmaster->get_motif_brg_jadi();
	$data['isi'] = 'mst-marker/vmainform';
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function edit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_marker 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
	
	$data['query'] = $this->mmaster->get_marker($id_marker);
	$data['msg'] = '';
	$data['isi'] = 'mst-marker/veditform';
	$data['id_marker'] = $id_marker;
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;
	$this->load->view('template',$data);
  }
  
  // updatedata
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
			$id_marker 	= $this->input->post('id_marker', TRUE);
			$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
			$kode_brg_jadi_lama 	= $this->input->post('kode_brg_jadi_lama', TRUE);
			$brg_jadi 	= $this->input->post('brg_jadi', TRUE);
			$kode_bhn_baku = $this->input->post('kode_bhn_baku', TRUE);
			$kode_bhn_baku_lama = $this->input->post('kode_bhn_baku_lama', TRUE);
			$bhn_baku = $this->input->post('bhn_baku', TRUE);
			$gelaran 	= $this->input->post('gelaran', TRUE);
			$jum_set = $this->input->post('jum_set', TRUE);  
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
												
			$tgl = date("Y-m-d");			
		/*	if (($kode_brg_jadi != $kode_brg_jadi_lama) || ($kode_bhn_baku != $kode_bhn_baku_lama) ) {
				$cek_data = $this->mmaster->cek_data($kode_brg_jadi, $kode_bhn_baku);
				if (count($cek_data) == 0) {
					$this->db->query(" UPDATE tm_marker_gelaran SET kode_brg_jadi = '$kode_brg_jadi', 
									kode_brg = '$kode_bhn_baku', tgl_update= '$tgl', gelaran = '$gelaran', 
									jum_set = '$jum_set' where id= '$id_marker' ");
					//-------------------------------------------------------------------------
					redirect('mst-marker/cform/view');
				}
				else {
					$data['query'] = $this->mmaster->get_marker($id_marker);
					$data['isi'] = 'mst-marker/veditform';
					$data['id_marker'] = $id_marker;
					$data['msg'] = "Update gagal. Data marker untuk barang jadi ".$brg_jadi." dan bahan ".$bhn_baku." sudah ada..!";
					$this->load->view('template',$data);
				}
			} // end if
			*/
			//else {
				$this->db->query(" UPDATE tm_marker_gelaran SET tgl_update= '$tgl', gelaran = '$gelaran', 
									jum_set = '$jum_set' where id= '$id_marker' ");
									
				if ($carinya == '') $carinya = "all";
				if ($is_cari == 0)
					$url_redirectnya = "mst-marker/cform/view/index/".$cur_page;
				else
					$url_redirectnya = "mst-marker/cform/cari/".$carinya."/".$cur_page;
				
				redirect($url_redirectnya);
				//redirect('mst-marker/cform/view');
			//}
  }
  
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '') {
			$keywordcari 	= $this->uri->segment(4);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari);

				$config['base_url'] = base_url()."index.php/mst-marker/cform/show_popup_brg_jadi/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('mst-marker/vpopupbrgjadi',$data);
  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);
	$quilting 	= $this->uri->segment(6);
	
	if ($posisi == '' || $kel_brg == '' || $quilting == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg 	= $this->input->post('kel_brg', TRUE);  
		$quilting 	= $this->input->post('quilting', TRUE);  
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$id_jenis_bhn = $this->input->post('id_jenis_bhn', TRUE);  
		
		if ($keywordcari == '' && ($id_jenis_bhn == '' || $posisi == '' || $kel_brg == '' || $quilting == '') ) {
			$kel_brg	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$quilting 	= $this->uri->segment(6);
			$id_jenis_bhn 	= $this->uri->segment(7);
			$keywordcari 	= $this->uri->segment(8);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		if ($id_jenis_bhn == '')
			$id_jenis_bhn 	= "0";	
		  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $quilting, $kel_brg, $id_jenis_bhn);		
				$config['base_url'] = base_url()."index.php/mst-marker/cform/show_popup_brg/".$kel_brg."/".$posisi."/".$quilting."/".$id_jenis_bhn."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(9);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $quilting, $kel_brg, $id_jenis_bhn);
	$data['jum_total'] = count($qjum_total);

	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['startnya'] = $config['cur_page'];
	$data['posisi'] = $posisi;
	$data['kel_brg'] = $kel_brg;
	$data['quilting'] = $quilting;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	}
	else {
		$nama_kel	= '';
	}
	
	$data['nama_kel'] = $nama_kel;
	$data['jenis_bhn'] = $this->mmaster->get_jenis_bhn($kel_brg);
	$data['cjenis_bhn'] = $id_jenis_bhn;

	$this->load->view('mst-marker/vpopupbrg',$data);
  }

  function submit(){
	//$this->load->library('form_validation');
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

			$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
			$motif_brg_jadi 	= $this->input->post('motif_brg_jadi', TRUE);
			$no 	= $this->input->post('no', TRUE);
			
			// ===============================================================
			// new 25-08-2011
			$jumlah_input=$no-1;
					
			for ($i=1;$i<=$jumlah_input;$i++)
			{
				$this->mmaster->save($kode_brg_jadi, $motif_brg_jadi,
						$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
						$this->input->post('gelaran_'.$i, TRUE), $this->input->post('set_'.$i, TRUE), 
						$this->input->post('diprint_'.$i, TRUE), $this->input->post('diquilting_'.$i, TRUE),
						$this->input->post('kode_brg_quilting_'.$i, TRUE) );
			}
			redirect('mst-marker/cform/');
			// ===============================================================
			
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'mst-marker/vformview';
    $keywordcari = "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/mst-marker/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);		
				
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' ) {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
		
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/mst-marker/cform/cari/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'mst-marker/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "mst-marker/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-marker/cform/cari/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('mst-marker/cform/view');
  }
  
  // 30 mei 2012, update status aktif
  function updatestatus(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $aksi = $this->uri->segment(5);
    $cur_page 	= $this->uri->segment(6);
    $is_cari 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->updatestatus($kode, $aksi);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "mst-marker/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-marker/cform/cari/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('op/cform/view');
  }
  
}
