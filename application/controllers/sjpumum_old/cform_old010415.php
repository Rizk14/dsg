<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmkeluar/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluar ORDER BY no_bonm DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==9) {
				$nobonm = substr($no_bonm, 0, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm;
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			}

	$data['isi'] = 'bonmkeluar/vmainform';
	$data['msg'] = '';
	//$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['no_bonm'] = $nomorbonm;
	$this->load->view('template',$data);

  }
  
  function edit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_bonm 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_bonm($id_bonm); 
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['msg'] = '';
	$data['isi'] = 'bonmkeluar/veditform';
	$this->load->view('template',$data);

  }
  
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_bonm 	= $this->input->post('id_bonm', TRUE);
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$tujuan 	= $this->input->post('tujuan', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");

			//update headernya
			$this->db->query(" UPDATE tm_bonmkeluar SET tgl_bonm = '$tgl_bonm', tujuan = '$tujuan', tgl_update='$tgl',
							no_manual='$no_bonm_manual', 
							keterangan = '$ket' where id= '$id_bonm' ");
							
				//reset stok, dan hapus dulu detailnya
				//============= 26-12-2012 ====================
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$id_bonm' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					$tgl = date("Y-m-d");
					foreach ($hasil2 as $row2) {
						// cek dulu satuan brgnya. jika satuannya yard/lusin, maka qty di detail ini konversikan lagi ke yard/lusin
							$query3	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$satuan	= $hasilrow->satuan;
								}
								else {
									$satuan	= '';
								}
								
								if ($satuan == '2') {
									$qty_sat_awal = $row2->qty / 0.90; //meter ke yard 07-03-2012, rubah dari 0.91 ke 0.90
								}
								else if ($satuan == '7') {
									$qty_sat_awal = $row2->qty / 12; // pcs ke lusin
								}
								else
									$qty_sat_awal = $row2->qty;
						
						// ============ update stok =====================
						
						//$nama_tabel_stok = "tm_stok";
						if ($row2->is_quilting == 't')
							$nama_tabel_stok = "tm_stok_hasil_makloon";
						else
							$nama_tabel_stok = "tm_stok";
				
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty_sat_awal; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert($nama_tabel_stok, $data_stok);
								}
								else {
									$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' ");
								}
								
								// ========== 24-12-2012 ==============
								$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
								if ($row2->is_quilting == 't')
									$sqlxx.= " kode_brg_quilting = '$row2->kode_brg' ";
								else
									$sqlxx.= " kode_brg = '$row2->kode_brg' ";
								$sqlxx.= " AND quilting = '$row2->is_quilting' ORDER BY id ASC LIMIT 1";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilrow = $queryxx->row();
									$stok_lama	= $hasilrow->stok;
									$harganya	= $hasilrow->harga;
								}
								// ========== end 24-12-2012 ===========
								$stokreset = $stok_lama+$qty_sat_awal;
								if ($row2->is_quilting == 'f')
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg= '$row2->kode_brg' AND harga = '$harganya' "); //
								else
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg_quilting= '$row2->kode_brg' AND harga = '$harganya' "); //
						// ==============================================
					} // end foreach
				} // end reset stok
				//=============================================
				$this->db->delete('tm_bonmkeluar_detail', array('id_bonmkeluar' => $id_bonm));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$is_quilting = $this->input->post('is_quilting_'.$i, TRUE);		
						$kode = $this->input->post('kode_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);

						// ========= 26-12-2012 ==============================
						if ($is_quilting == '')
							$is_quilting = 'f';
						
						// cek satuan barangnya, jika yard (qty = meter) maka konversi balik ke yard
						$queryxx	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$kode' ");
						
						if ($queryxx->num_rows() > 0){
							$hasilrow = $queryxx->row();
							$id_satuan	= $hasilrow->satuan;
						}
						else {
							$id_satuan	= '';
						
						}
						//$hasilrow = $queryxx->row();
						//$id_satuan	= $hasilrow->satuan; 
						
						if ($id_satuan == '2') { // jika satuan awalnya yard
							$qty_sat_awal = $qty / 0.90;
							$konv = 't';
						}
						else if ($id_satuan == '7') { // jika satuan awalnya lusin
							$qty_sat_awal = $qty / 12;
							$konv = 't';
						}
						else {
							$qty_sat_awal = $qty;
							$konv = 'f';
						}
						
						// ======== update stoknya! =============
						if ($is_quilting == 't')
							$nama_tabel_stok = "tm_stok_hasil_makloon";
						else
							$nama_tabel_stok = "tm_stok";
						
						//cek stok terakhir tm_stok, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$kode' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qty_sat_awal;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$kode,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert($nama_tabel_stok, $data_stok);
							}
							else {
								$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode' ");
							}
							
							$selisih = 0;
							$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
							if ($is_quilting == 't')
								$sqlxx.= " kode_brg_quilting = '$kode' ";
							else
								$sqlxx.= " kode_brg = '$kode' ";
							$sqlxx.= " AND quilting = '$is_quilting' ORDER BY id ASC";
								
							$queryxx	= $this->db->query($sqlxx);
							if ($queryxx->num_rows() > 0){
								$hasilxx=$queryxx->result();
									
								$temp_selisih = 0;				
								foreach ($hasilxx as $row2) {
									$stoknya = $row2->stok; 
									$harganya = $row2->harga; 
									
									if ($stoknya > 0) { 
										if ($temp_selisih == 0) 
											$selisih = $stoknya-$qty_sat_awal;
										else
											$selisih = $stoknya+$temp_selisih;
										
										if ($selisih < 0) {
											$temp_selisih = $selisih;
											if ($is_quilting == 'f')
												$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
														where kode_brg= '$kode' AND harga = '$harganya' "); //
											else
												$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
														where kode_brg_quilting= '$kode' AND harga = '$harganya' "); //

										}
											
										if ($selisih > 0) {
											if ($temp_selisih == 0)
												$temp_selisih = $qty_sat_awal;
											break;
										}
									}
								} // end for
							}
							
							if ($selisih != 0) {
								$new_stok = $selisih;
								if ($is_quilting == 'f')
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
												where kode_brg= '$kode' AND harga = '$harganya' ");
								else
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
												where kode_brg_quilting= '$kode' AND harga = '$harganya' ");
							}
				
					// ============================= end 22-12-2012 ============================
					
					// jika semua data tdk kosong, insert ke tm_bonmkeluar_detail
					$data_detail = array(
						'id_bonmkeluar'=>$id_bonm,
						'kode_brg'=>$kode,
						'qty'=>$qty,
						'keterangan'=>$ket_detail,
						'konversi_satuan'=>$konv,
						'is_quilting'=>$is_quilting
					);
					$this->db->insert('tm_bonmkeluar_detail',$data_detail);
							// ================ end insert item detail ===========
				} // end perulangan
					
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "bonmkeluar/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "bonmkeluar/cform/cari/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
				
  }

  function submit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
			$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			$tujuan 	= $this->input->post('tujuan', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data($no_bonm_manual, $thn1);
			if (count($cek_data) > 0) {
				$data['isi'] = 'bonmkeluar/vmainform';
				$data['msg'] = "Data no bon M ".$no_bonm_manual." utk tahun $thn1 sudah ada..!";
				
				$th_now	= date("Y");
				$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluar ORDER BY no_bonm DESC LIMIT 1 ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) 
					$no_bonm	= $hasilrow->no_bonm;
				else
					$no_bonm = '';
				if(strlen($no_bonm)==9) {
					$nobonm = substr($no_bonm, 0, 9);
					$n_bonm	= (substr($nobonm,4,5))+1;
					$th_bonm	= substr($nobonm,0,4);
					if($th_now==$th_bonm) {
							$jml_n_bonm	= $n_bonm;
							switch(strlen($jml_n_bonm)) {
								case "1": $kodebonm	= "0000".$jml_n_bonm;
								break;
								case "2": $kodebonm	= "000".$jml_n_bonm;
								break;	
								case "3": $kodebonm	= "00".$jml_n_bonm;
								break;
								case "4": $kodebonm	= "0".$jml_n_bonm;
								break;
								case "5": $kodebonm	= $jml_n_bonm;
								break;	
							}
							$nomorbonm = $th_now.$kodebonm;
					}
					else {
						$nomorbonm = $th_now."00001";
					}
				}
				else {
					$nomorbonm	= $th_now."00001";
				}
				$data['list_gudang'] = $this->mmaster->get_gudang();
				$data['no_bonm'] = $nomorbonm;
				$this->load->view('template',$data);
			}
			else {
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->save($no_bonm,$tgl_bonm, $no_bonm_manual, $id_gudang, $tujuan, $ket, 
							$this->input->post('is_quilting_'.$i, TRUE),
							$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								$this->input->post('qty_'.$i, TRUE), $this->input->post('stok_'.$i, TRUE),  
								$this->input->post('satuan_'.$i, TRUE), $this->input->post('ket_detail_'.$i, TRUE) );						
				}
				redirect('bonmkeluar/cform/view');
			}
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'bonmkeluar/vformview';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/bonmkeluar/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $date_from, $date_to);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
    
    /*if ($keywordcari == '' && ($date_from == '' || $date_to == '')) {
		$date_from 	= $this->uri->segment(4);
		$date_to 	= $this->uri->segment(5);
		$keywordcari = $this->uri->segment(6);
	} */
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(6);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	
	$jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to);
    
							$config['base_url'] = base_url().'index.php/bonmkeluar/cform/cari/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'bonmkeluar/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }

  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_gudang	= $this->uri->segment(4);
	$is_quilting 	= $this->uri->segment(5);
	$posisi 	= $this->uri->segment(6);
	
	if ($id_gudang == '' || $is_quilting == '' || $posisi == '') {
		$id_gudang = $this->input->post('id_gudang', TRUE);  
		$is_quilting 	= $this->input->post('is_quilting', TRUE);  
		$posisi 	= $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || ($id_gudang == '' && $is_quilting == '' && $posisi == '') ) {
			$id_gudang	= $this->uri->segment(4);
			$is_quilting 	= $this->uri->segment(5);
			$posisi 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		//echo $id_gudang." ".$is_quilting." ".$posisi." ".$keywordcari;			  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $id_gudang, $is_quilting);
		
				$config['base_url'] = base_url()."index.php/bonmkeluar/cform/show_popup_brg/".$id_gudang."/".
							$is_quilting."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $id_gudang, $is_quilting);
	$data['jum_total'] = count($qjum_total);
	$data['id_gudang'] = $id_gudang;
	$data['is_quilting'] = $is_quilting;
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$id_gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$nama_gudang	= '';
	}
	$data['nama_gudang'] = $nama_gudang;

	$this->load->view('bonmkeluar/vpopupbrg',$data);
  }
  
  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
    
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "bonmkeluar/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "bonmkeluar/cform/cari/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  
  function generate_nomor() {
		//$jenis_brg 	= $this->uri->segment(4);
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
    
  // ===================================================== 21-09-2013 =====================================================
  function addbonmkeluarcutting(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluarcutting ORDER BY no_bonm DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==9) {
				$nobonm = substr($no_bonm, 0, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm;
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			}

	$data['isi'] = 'bonmkeluar/vform1bonmkeluarcutting';
	$data['msg'] = '';
	//$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['no_bonm'] = $nomorbonm;
	$this->load->view('template',$data);
  }
  
  function show_popup_brg_jadi(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$posisi		= $this->input->post('posisi', TRUE);   
	$keywordcari	= $this->input->post('cari', TRUE);  
	
	if ($posisi=='' && $keywordcari=='') {
		$posisi 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}	

	if ($keywordcari=='')
		$keywordcari = "all";
			
	$qjum_total = $this->mmaster->getbarangjaditanpalimit($keywordcari);

	$config['base_url'] 	= base_url()."index.php/bonmkeluar/cform/show_popup_brg_jadi/".$posisi."/".$keywordcari."/";
	$config['total_rows'] 	= count($qjum_total); 
	$config['per_page'] 	= 10;
	$config['first_link'] 	= 'Awal';
	$config['last_link'] 	= 'Akhir';
	$config['next_link'] 	= 'Selanjutnya';
	$config['prev_link'] 	= 'Sebelumnya';
	$config['cur_page']		= $this->uri->segment(6,0);
	$this->pagination->initialize($config);
	
	$data['query']		= $this->mmaster->getbarangjadi($config['per_page'],$config['cur_page'], $keywordcari);	
	$data['jum_total']	= count($qjum_total);
	$data['posisi']		= $posisi;
	$data['jumdata'] = $posisi-1;

	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
				
	$data['startnya']	= $config['cur_page'];
	
	$this->load->view('bonmkeluar/vpopupbrgjadi',$data);  
  } 
  
  // 23-09-2013
  function viewbonmkeluarcutting(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($kode_unit_jahit == '0')
		$kode_unit_jahit = $this->uri->segment(6);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(7);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(8);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(9);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($kode_unit_jahit == '')
		$kode_unit_jahit = '0';
	if ($caribrg == '')
		$caribrg = "all";
	if ($filterbrg == '')
		$filterbrg = "n";
	
	$jum_total = $this->mmaster->getAllbonmkeluarcuttingtanpalimit($keywordcari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg);
    
				$config['base_url'] = base_url().'index.php/bonmkeluar/cform/viewbonmkeluarcutting/'.$date_from.'/'.$date_to.'/'.$kode_unit_jahit.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
				$config['total_rows'] = count($jum_total); 
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(10);
				$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbonmkeluarcutting($config['per_page'],$this->uri->segment(10), $keywordcari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['isi'] = 'bonmkeluar/vformviewbonmkeluarcutting';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);
  }
  
  function submitbonmkeluarcutting(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
			$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);  
			$jenis 	= $this->input->post('jenis', TRUE);  
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $kode_unit_jahit, $jenis);
			if (count($cek_data) > 0) {
				$data['isi'] = 'bonmkeluar/vform1bonmkeluarcutting';
				$data['msg'] = "Data no bon M ".$no_bonm_manual." utk tahun $thn1 sudah ada..!";
				
				$th_now	= date("Y");
				$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluarcutting ORDER BY no_bonm DESC LIMIT 1 ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) 
					$no_bonm	= $hasilrow->no_bonm;
				else
					$no_bonm = '';
				if(strlen($no_bonm)==9) {
					$nobonm = substr($no_bonm, 0, 9);
					$n_bonm	= (substr($nobonm,4,5))+1;
					$th_bonm	= substr($nobonm,0,4);
					if($th_now==$th_bonm) {
							$jml_n_bonm	= $n_bonm;
							switch(strlen($jml_n_bonm)) {
								case "1": $kodebonm	= "0000".$jml_n_bonm;
								break;
								case "2": $kodebonm	= "000".$jml_n_bonm;
								break;	
								case "3": $kodebonm	= "00".$jml_n_bonm;
								break;
								case "4": $kodebonm	= "0".$jml_n_bonm;
								break;
								case "5": $kodebonm	= $jml_n_bonm;
								break;	
							}
							$nomorbonm = $th_now.$kodebonm;
					}
					else {
						$nomorbonm = $th_now."00001";
					}
				}
				else {
					$nomorbonm	= $th_now."00001";
				}
				
				$data['no_bonm'] = $nomorbonm;
				$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
				$this->load->view('template',$data);
			}
			else {
				// 11-09-2014
				$tgl = date("Y-m-d H:i:s");
				// insert di tm_bonmkeluarcutting
				$data_header = array(
				  'no_bonm'=>$no_bonm,
				  'no_manual'=>$no_bonm_manual,
				  'tgl_bonm'=>$tgl_bonm,
				  'jenis_keluar'=>$jenis,
				  'kode_unit'=>$kode_unit_jahit,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'keterangan'=>$ket
				);
				$this->db->insert('tm_bonmkeluarcutting',$data_header);
				
				// ambil data terakhir di tabel tm_bonmkeluarcutting
				$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query2->row();
				$id_bonm	= $hasilrow->id; 
											
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					/*$this->mmaster->savebonmkeluarcutting($no_bonm,$tgl_bonm, $no_bonm_manual, $jenis, $kode_unit_jahit, $ket, 
							$this->input->post('kode_brg_jadi_'.$i, TRUE), 
								//$this->input->post('qty_'.$i, TRUE), 
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) ); */
								
					$this->mmaster->savebonmkeluarcutting($id_bonm, $jenis, $kode_unit_jahit, 
							$this->input->post('kode_brg_jadi_'.$i, TRUE), 
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) );
				}
				redirect('bonmkeluar/cform/viewbonmkeluarcutting');
			}
  }
  
  function deletebonmkeluarcutting(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$tgl_awal 	= $this->uri->segment(6);
	$tgl_akhir 	= $this->uri->segment(7);
	$kode_unit_jahit 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
    
    $this->mmaster->deletebonmkeluarcutting($id);
    
    if ($carinya == '') $carinya = "all";

	$url_redirectnya = "bonmkeluar/cform/viewbonmkeluarcutting/".$tgl_awal."/".$tgl_akhir."/".$kode_unit_jahit."/".$carinya."/".$cur_page;
	redirect($url_redirectnya);
  }
  
  function editbonmkeluarcutting(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_bonm 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$tgl_awal 	= $this->uri->segment(6);
	$tgl_akhir 	= $this->uri->segment(7);
	$kode_unit_jahit 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['cur_page'] = $cur_page;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_bonmkeluarcutting($id_bonm); 
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['msg'] = '';
	$data['isi'] = 'bonmkeluar/veditformbonmkeluarcutting';
	$this->load->view('template',$data);

  }
  
  function updatedatabonmkeluarcutting() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_bonm 	= $this->input->post('id_bonm', TRUE);
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$jenis 	= $this->input->post('jenis', TRUE);  
			$kode_unit 	= $this->input->post('kode_unit', TRUE);  
			$kode_unit_lama 	= $this->input->post('kode_unit_lama', TRUE); 
			
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$kode_unit_jahit = $this->input->post('kode_unit_jahit', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");

			//update headernya
			$this->db->query(" UPDATE tm_bonmkeluarcutting SET tgl_bonm = '$tgl_bonm', jenis_keluar = '$jenis', 
							kode_unit = '$kode_unit', tgl_update='$tgl', no_manual='$no_bonm_manual', 
							keterangan = '$ket' where id= '$id_bonm' ");
							
				//reset stok, dan hapus dulu detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail WHERE id_bonmkeluarcutting = '$id_bonm' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					foreach ($hasil2 as $row2) {
						$qty = $row2->qty;
						
						// 1. stok total
						// ============ update stok PABRIK =====================
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
								
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$row2->kode_brg_jadi' ");
						
						//================ update stok UNIT JAHIT ===============================
						if ($kode_unit_lama != '0') {
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit
											WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_unit='$kode_unit_lama' ");
							if ($query3->num_rows() == 0){
								$id_stok_unit = 0;
								$stok_unit_lama = 0;
								$stok_unit_bagus_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								$stok_unit_bagus_lama = $hasilrow->stok_bagus;
							}
							$new_stok_unit = $stok_unit_lama-$row2->qty; // berkurang stok unit karena reset bon M keluar
							$new_stok_unit_bagus = $stok_unit_bagus_lama-$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
										WHERE kode_brg_jadi= '$row2->kode_brg_jadi' AND kode_unit = '$kode_unit_lama' ");
						} // end if
						
						// 2. reset stok per warna dari tabel tm_bonmkeluarcutting_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail_warna 
												WHERE id_bonmkeluarcutting_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND kode_warna='$rowwarna->kode_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari bon M keluar
										
								$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND kode_warna = '$rowwarna->kode_warna' ");
								
								// ====================	15-03-2014, stok hasil cutting bhn baku ==================
								//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
								$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '".$rowwarna->kode_warna."'
										AND id_stok_hasil_cutting='$id_stok' ");
								if ($query3->num_rows() > 0){
									$hasil3 = $query3->result();
									foreach ($hasil3 as $row3) {	
										$new_stok_bhnbaku = $row3->stok+$rowwarna->qty;
										$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
												where id= '".$row3->id."' ");
									}
								}
								// ==========================================================
								
								// ============= update stok unit jahit
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND kode_warna='$rowwarna->kode_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warna_bagus_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama-$rowwarna->qty; // berkurang stok di unit karena reset dari bon M keluar
									$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama-$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna',
												stok_bagus = '$new_stok_unit_warna_bagus',
												tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND kode_warna = '$rowwarna->kode_warna' ");
							
							}
						} // end if detail warna
						
						$this->db->query(" DELETE FROM tm_bonmkeluarcutting_detail_warna WHERE id_bonmkeluarcutting_detail = '$row2->id' ");
						
						// ============ update stok =====================
						// modif 27-01-2014, sementara ga dipake =
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
							/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting 
														WHERE kode_brg = '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'kode_brg_jadi'=>$row2->kode_brg_jadi,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_cutting', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' AND kode_brg_jadi= '$row2->kode_brg_jadi' ");
								} */
						// ==============================================
					} // end foreach detail
				} // end reset stok
				//=============================================
				$this->db->delete('tm_bonmkeluarcutting_detail', array('id_bonmkeluarcutting' => $id_bonm));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{	
						$kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
						//$qty = $this->input->post('qty_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
						
						$temp_qty = $this->input->post('temp_qty_'.$i, TRUE);
						$kode_warna = $this->input->post('kode_warna_'.$i, TRUE);
						$qty_warna = $this->input->post('qty_warna_'.$i, TRUE);
						
						//-------------- hitung total qty dari detail tiap2 warna -------------------
						$qtytotal = 0;
						for ($xx=0; $xx<count($kode_warna); $xx++) {
							$kode_warna[$xx] = trim($kode_warna[$xx]);
							$qty_warna[$xx] = trim($qty_warna[$xx]);
							$qtytotal+= $qty_warna[$xx];
						} // end for
						// ---------------------------------------------------------------------
						
						// 1. stok total
						// ============================= update stok PABRIK ======================================
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() == 0){
								$id_stok = 0;
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok	= $hasilrow->id;
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qtytotal;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
								$data_stok = array(
									'kode_brg_jadi'=>$kode_brg_jadi,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting', $data_stok);
								
								// ambil id_stok utk dipake di stok warna
								$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
								if($sqlxx->num_rows() > 0) {
									$hasilxx	= $sqlxx->row();
									$id_stok	= $hasilxx->id;
								}else{
									$id_stok	= 1;
								}
							}
							else {
								$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$kode_brg_jadi' ");
							}
					
					// ============= update stok UNIT JAHIT ======================
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode_brg_jadi'
											AND kode_unit='$kode_unit' ");
						if ($query3->num_rows() == 0){
							$id_stok_unit = 0;
							$stok_unit_lama = 0;
							$stok_unit_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							$stok_unit_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit = $stok_unit_lama+$qtytotal;
						$new_stok_unit_bagus = $stok_unit_bagus_lama+$qtytotal;
							
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'kode_brg_jadi'=>$kode_brg_jadi,
								'stok'=>$new_stok_unit,
								'stok_bagus'=>$new_stok_unit_bagus,
								'kode_unit'=>$kode_unit,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
								stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
								where kode_brg_jadi= '$kode_brg_jadi' AND kode_unit = '$kode_unit' ");
						}
					// =====================================================================================
						
						// ======== update stoknya! =============

						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
						
						// modif, stok sementara ga dipake
						/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting WHERE kode_brg = '$kode'
													AND kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
								$data_stok = array(
									'kode_brg'=>$kode,
									'kode_brg_jadi'=>$kode_brg_jadi,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting', $data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode' AND kode_brg_jadi= '$kode_brg_jadi' ");
							} */
							
					// jika semua data tdk kosong, insert ke tm_bonmkeluarcutting_detail
					$data_detail = array(
						'id_bonmkeluarcutting'=>$id_bonm,
						'kode_brg_jadi'=>$kode_brg_jadi,
						'qty'=>$qtytotal,
						'keterangan'=>$ket_detail
					);
					$this->db->insert('tm_bonmkeluarcutting_detail',$data_detail);
							// ================ end insert item detail ===========
					
					// ambil id detail tm_bonmkeluarcutting_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;
					
					// ----------------------------------------------
					for ($xx=0; $xx<count($kode_warna); $xx++) {
						$kode_warna[$xx] = trim($kode_warna[$xx]);
						$qty_warna[$xx] = trim($qty_warna[$xx]);
								
						$seq_warna	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail_warna ORDER BY id DESC LIMIT 1 ");
							
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}

						$tm_bonmkeluarcutting_detail_warna	= array(
							 'id'=>$idbaru,
							 'id_bonmkeluarcutting_detail'=>$iddetail,
							 'id_warna_brg_jadi'=>'0',
							 'kode_warna'=>$kode_warna[$xx],
							 'qty'=>$qty_warna[$xx]
						);
						$this->db->insert('tm_bonmkeluarcutting_detail_warna',$tm_bonmkeluarcutting_detail_warna);
						
						// ========================= 04-02-2014, stok per warna ===============================================
						//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() == 0){
							$stok_warna_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_warna_lama	= $hasilrow->stok;
						}
						$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_cutting'=>$id_stok,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_warna,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
						}
						
						// ====================	15-03-2014, stok hasil cutting bhn baku ==================
						//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() == 0){
							//$stok_warna_lama = 0;
							// ambil data2 material brg jadi berdasarkan brg jadi, trus insert langsung ke tabel stok_hasil_cutting_bhnbaku. qty-nya sesuai dgn warnanya
							$sqlxx = " SELECT * FROM tm_material_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' AND kode_warna='".$kode_warna[$xx]."' ";
							$queryxx = $this->db->query($sqlxx);
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->result();
								foreach ($hasilxx as $rowxx) {			
									$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_bhnbaku ORDER BY id DESC LIMIT 1 ");
									if($seq_stokwarna->num_rows() > 0) {
										$seq_stokwarnarow	= $seq_stokwarna->row();
										$id_stok_warna	= $seq_stokwarnarow->id+1;
									}else{
										$id_stok_warna	= 1;
									}
									
									$data_stok = array(
										'id'=>$id_stok_warna,
										'id_stok_hasil_cutting'=>$id_stok,
										'kode_brg'=>$rowxx->kode_brg,
										'kode_warna'=>$kode_warna[$xx],
										'stok'=>$qty_warna[$xx],
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_cutting_bhnbaku', $data_stok);
								} // end xxx
							} // end if
							
						}
						else {
							$hasil3 = $query3->result();
							foreach ($hasil3 as $row3) {	
								$new_stok_bhnbaku = $row3->stok-$qty_warna[$xx];
								$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
										where id= '".$row3->id."' ");
							}
						} // end if
						// ==========================================================
						
						// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warna_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama+$qty_warna[$xx]; // bertambah stok karena masuk ke unit
						$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								'stok_bagus'=>$new_stok_unit_warna_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
					// ------------------------------------------------------------------------------------------
						
					} // end for
					// ----------------------------------------------
						
				} // end perulangan
					//die();
			if ($carinya == '') $carinya = "all";
			$url_redirectnya = "bonmkeluar/cform/viewbonmkeluarcutting/".$tgl_awal."/".$tgl_akhir."/".$kode_unit_jahit."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
  }
  
  // 04-02-2014
  function additemwarna(){

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna_brg_jadi'=> $rowxx->id,
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('bonmkeluar/vlistwarna', $data); 
		return true;
  }
  
  // 25-03-2014
  function caribrgjadi(){
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel brg jadi utk ambil kode, nama, dan stok
		$queryxx = $this->db->query(" SELECT e_product_motifname FROM tr_product_motif
									WHERE i_product_motif = '".$kode_brg_jadi."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$nama_brg_jadi = $hasilxx->e_product_motifname;
		}
		else
			$nama_brg_jadi = '';
		//echo $nama_brg_jadi; die();
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('bonmkeluar/vinfobrgjadi', $data); 
		return true;
  }
  
  // 04-04-2014
  function ceknobonm(){
		$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
		$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);
		$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);
		$jenis 	= $this->input->post('jenis', TRUE);
		
		$pisah1 = explode("-", $tgl_bonm);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		
		$cek_data = $this->mmaster->cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $kode_unit_jahit, $jenis);
		if (count($cek_data) > 0)
			$ada = "ada";	
		else
			$ada = "tidak";
		
		echo $ada; 
		return true;
  }
  
}
