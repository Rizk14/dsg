<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('sjpumum/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluar ORDER BY no_bonm DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==9) {
				$nobonm = substr($no_bonm, 0, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm;
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			}

	$data['isi'] = 'sjpumum/vmainform';
	$data['msg'] = '';
	//$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_satuan'] = $this->mmaster->get_satuan();
	
	// 08-06-2015
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['no_bonm'] = $nomorbonm;

    // area
    $data['list_area'] = $this->mmaster->get_list_area();

	$this->load->view('template',$data);

  }
  
  function edit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_bonm 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$cgudang 	= $this->uri->segment(9);
	$ctujuan 	= $this->uri->segment(10);
	$carinya 	= $this->uri->segment(11);
	$caribrg 	= $this->uri->segment(12);
	$filterbrg 	= $this->uri->segment(13);
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['cgudang'] = $cgudang;
	$data['ctujuan'] = $ctujuan;
	$data['carinya'] = $carinya;
	$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['query'] = $this->mmaster->get_bonm($id_bonm); 
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_satuan'] = $this->mmaster->get_satuan();
	// 08-06-2015
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['msg'] = '';
	$data['isi'] = 'sjpumum/veditform';
	$this->load->view('template',$data);

  }
  
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_bonm 	= $this->input->post('id_bonm', TRUE);
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			// 18-08-2015
			$dibebankan 	= $this->input->post('dibebankan', TRUE);  
			if ($dibebankan != '')
				$ket = $dibebankan;
			$tujuan 	= $this->input->post('tujuan', TRUE);  
			// 08-06-2015
			$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$cgudang = $this->input->post('cgudang', TRUE);
			$ctujuan = $this->input->post('ctujuan', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			// 04-11-2015
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
			
			$submit3 = $this->input->post('submit3', TRUE);
						
			$tgl = date("Y-m-d H:i:s");
			
			// 08-09-2015
			if ($submit3 != '') {
				$jumlah_input=$no-1;
				for ($i=1;$i<=$jumlah_input;$i++) {
					if ($this->input->post('cek_'.$i, TRUE) == 'y') {
						/* algoritma:
						1. Reset stoknya 
						2. Hapus dari tm_bonmkeluar_detail
						*/
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							
							//========= start here ==========
							$id_detailnya = $this->input->post('id_detail_'.$i, TRUE);
																				 
							 // Reset stoknya dan hapus dari tm_bonmkeluar_detail
							 $qty_lama = $this->input->post('qty_lama_'.$i, TRUE); 
							 $id_brg_lama = $this->input->post('id_brg_lama_'.$i, TRUE);
							 
							 //ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg='$id_brg_lama' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							else
								$stok_lama = 0;
										
							$stokreset1 = $stok_lama+$qty_lama;
														
							$this->db->query(" UPDATE tm_stok SET stok = '$stokreset1', tgl_update_stok = '$tgl'
												where id_brg= '$id_brg_lama' ");
																										
							$this->db->query("DELETE FROM tm_bonmkeluar_detail WHERE id='".$id_detailnya."' ");
						} // end if ($this->input->post('id_detail_'.$i, TRUE) != 'n')
					} // end if cek_
				}
			} // end jika hapus per item
			else {
				// 28-08-2015
				$uid_update_by = $this->session->userdata('uid');
				//update headernya
				$this->db->query(" UPDATE tm_bonmkeluar SET tgl_bonm = '$tgl_bonm', tujuan = '$tujuan', tgl_update='$tgl',
								no_manual='$no_bonm_manual', id_unit_jahit='$id_unit_jahit', uid_update_by = '$uid_update_by',
								keterangan = '$ket' where id= '$id_bonm' ");
								
					//reset stok, dan hapus dulu detailnya
					//============= 26-12-2012 ====================
					$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$id_bonm' ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
						
						foreach ($hasil2 as $row2) {
							// cek dulu satuan brgnya. jika satuannya yard/lusin, maka qty di detail ini konversikan lagi ke yard/lusin
								$query3	= $this->db->query(" SELECT satuan FROM tm_barang WHERE id = '$row2->id_brg' ");
									if ($query3->num_rows() > 0){
										$hasilrow = $query3->row();
										$satuan	= $hasilrow->satuan;
									}
									else {
										$satuan	= '';
									}
								
								// 29-10-2014, ini ga dipake, soalnya udh ada setting satuan konversi di master bhn baku
								/*	if ($satuan == '2') {
										$qty_sat_awal = $row2->qty / 0.90; //meter ke yard 07-03-2012, rubah dari 0.91 ke 0.90
									}
									else if ($satuan == '7') {
										$qty_sat_awal = $row2->qty / 12; // pcs ke lusin
									} */
									//else {
										
									if ($row2->id_satuan_konversi == '0')
										$qty_sat_awal = $row2->qty;
									else
										$qty_sat_awal = $row2->qty_satawal;
									//}
							
							// ============ update stok =====================
							
							// 05-09-2015 DIKOMEN
							//if ($row2->is_quilting == 't')
							//	$nama_tabel_stok = "tm_stok_hasil_makloon";
							//else
								$nama_tabel_stok = "tm_stok";
					
							//cek stok terakhir tm_stok, dan update stoknya
									$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE id_brg = '$row2->id_brg' ");
									if ($query3->num_rows() == 0){
										$stok_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_lama	= $hasilrow->stok;
									}
									$new_stok = $stok_lama+$qty_sat_awal; // bertambah stok karena reset dari bon M keluar
									
									if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
										$data_stok = array(
											'id_brg'=>$row2->id_brg,
											'stok'=>$new_stok,
											'tgl_update_stok'=>$tgl
											);
										$this->db->insert($nama_tabel_stok, $data_stok);
									}
									else {
										$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where id_brg= '$row2->id_brg' ");
									}
									
						} // end foreach
					} // end reset stok
					//=============================================
					$this->db->delete('tm_bonmkeluar_detail', array('id_bonmkeluar' => $id_bonm));
					
						$jumlah_input=$no-1; 
						for ($i=1;$i<=$jumlah_input;$i++)
						{
							//$is_quilting = $this->input->post('is_quilting_'.$i, TRUE);		
							$id_brg = $this->input->post('id_brg_'.$i, TRUE);
							$qty = $this->input->post('qty_'.$i, TRUE);
							$satuan_konvx = $this->input->post('satuan_konv_'.$i, TRUE);
							$qty_satawalx = $this->input->post('qty_satawal_'.$i, TRUE);
							$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
							// 13-08-2015
							$untuk_bisbisan = $this->input->post('untuk_bisbisan_'.$i, TRUE);
							// 04-11-2015
							$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);

							// ========= 26-12-2012 ==============================
							//if ($is_quilting == '')
								$is_quilting = 'f';
							
							if ($untuk_bisbisan == '')
								$untuk_bisbisan = 'f';
							
							// cek satuan barangnya, jika yard (qty = meter) maka konversi balik ke yard
							$queryxx	= $this->db->query(" SELECT satuan FROM tm_barang WHERE id = '$id_brg' ");
							
							if ($queryxx->num_rows() > 0){
								$hasilrow = $queryxx->row();
								$id_satuan	= $hasilrow->satuan;
							}
							else {
								$id_satuan	= '';
							
							}
													
							if ($satuan_konvx != 0) {
								$konv = 't';
								$qty_sat_awal = $qty_satawalx;
							}
							else {
								$qty_sat_awal = $qty;
								$konv = 'f';
							}
							
							if ($id_brg_wip == '')
								$id_brg_wip = 0;
							
							// 27-10-2014
							// jika semua data tdk kosong, insert ke tm_bonmkeluar_detail
							$data_detail = array(
								'id_bonmkeluar'=>$id_bonm,
								'id_brg'=>$id_brg,
								'qty'=>$qty,
								'keterangan'=>$ket_detail,
								'konversi_satuan'=>$konv,
								'is_quilting'=>$is_quilting,
								'qty_satawal'=>$qty_satawalx,
								'id_satuan_konversi'=>$satuan_konvx,
								// 13-08-2015
								'untuk_bisbisan'=>$untuk_bisbisan,
								// 04-11-2015
								'id_brg_wip'=>$id_brg_wip
							);
							$this->db->insert('tm_bonmkeluar_detail',$data_detail);
								// ================ end insert item detail ===========
								
							// 27-10-2014, ambil data terakhir di tabel tm_bonmkeluar_detail
							$query3	= $this->db->query(" SELECT id FROM tm_bonmkeluar_detail ORDER BY id DESC LIMIT 1 ");
							$hasilrow = $query3->row();
							$id_bonmkeluar_detail	= $hasilrow->id; 
							
							// ======== update stoknya! =============
							// 05-09-2015 dikomen
							//if ($is_quilting == 't')
							//	$nama_tabel_stok = "tm_stok_hasil_makloon";
							//else
								$nama_tabel_stok = "tm_stok";
							
							//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE id_brg = '$id_brg' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama-$qty_sat_awal;
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'id_brg'=>$id_brg,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert($nama_tabel_stok, $data_stok);
								}
								else {
									$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg= '$id_brg' ");
								}
								
								// ==================== 05-09-2015 DIKOMEN, PROSES KE STOK_HARGA GA DIPAKE LAGI =================================
							/*	$selisih = 0;
								$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
								if ($is_quilting == 't')
									$sqlxx.= " id_brg_quilting = '$id_brg' ";
								else
									$sqlxx.= " id_brg = '$id_brg' ";
								$sqlxx.= " AND quilting = '$is_quilting' AND stok > '0' ORDER BY id ASC";
								
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilxx=$queryxx->result();
										
									$temp_selisih = 0; $list_harga = array();
									foreach ($hasilxx as $row2) {
										$stoknya = $row2->stok; 
										$harganya = $row2->harga; 
										
											// ================= 27-10-2014, skrip baru ================================================
											if ($temp_selisih == 0)
												$selisih = $stoknya-$qty_sat_awal;
											else
												$selisih = $stoknya+$temp_selisih;
											
											if ($selisih < 0) {
												// isinya minus. stok utk harga ini = 0
												$temp_selisih = $selisih;
																							
												if ($is_quilting == 'f') {										
													$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
															where id_brg= '$id_brg' AND harga = '$harganya' "); //
												}
												else {										
													$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
															where id_brg_quilting= '$id_brg' AND harga = '$harganya' "); //
												}
												
												$list_harga[] = array('harganya'=>$harganya,
																	'qty_satawal'=>$stoknya);
											}
											else if ($selisih >= 0) {
												if ($temp_selisih != 0) {
													$qty_tambahan = abs($temp_selisih);
													$temp_selisih = 0;
												}
												else
													$qty_tambahan = $qty_sat_awal;
																							
												if ($is_quilting == 'f') {										
													$this->db->query(" UPDATE tm_stok_harga SET stok = '$selisih', tgl_update_stok = '$tgl' 
															where id_brg= '$id_brg' AND harga = '$harganya' "); //
													
												}
												else {										
													$this->db->query(" UPDATE tm_stok_harga SET stok = '$selisih', tgl_update_stok = '$tgl' 
															where id_brg_quilting= '$id_brg' AND harga = '$harganya' "); //
												}
												
												$list_harga[] = array('harganya'=>$harganya,
															 'qty_satawal'=>$qty_tambahan);
												break;
												
											} // logikanya udh good kayaknya
									//-----------------------------------------------------------------------------------------
									} // end for stok harga
									
									// 28-10-2014
									$jum_data_harga = count($list_harga);
									
									if ($jum_data_harga > 0) {
										// catatan 27-02-2015: qty ini bukan dibagi rata, tapi dari data qty_satawal trus dikonversi
										
										$query3	= $this->db->query(" SELECT a.satuan, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
													a.nama_brg, b.nama as nama_satuan FROM tm_barang a 
													INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
										if ($query3->num_rows() > 0){
											$hasilnya = $query3->row();
											$id_satuan = $hasilnya->satuan;
											$satuan = $hasilnya->nama_satuan;
											
											$id_satuan_konversi = $hasilnya->id_satuan_konversi;
											$rumus_konversi = $hasilnya->rumus_konversi;
											$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
										}
										else {
											$id_satuan_konversi = 0;
											$id_satuan = 0;
											$satuan = '';
										}
										
										//$qty_dipecah = $qty/$jum_data_harga;
										//foreach ($list_harga as $rowharga) {
										//print_r($list_harga); echo "<br>";
										//if (is_array($list_harga)) {
											for ($ii=0; $ii<count($list_harga); $ii++) {
												// 27-02-2015
												if ($id_satuan_konversi != 0) {
													if ($rumus_konversi == '1') {
														$qty_dipecah = $list_harga[$ii]['qty_satawal']*$angka_faktor_konversi;
													}
													else if ($rumus_konversi == '2') {
														$qty_dipecah = $list_harga[$ii]['qty_satawal']/$angka_faktor_konversi;
													}
													else if ($rumus_konversi == '3') {
														$qty_dipecah = $list_harga[$ii]['qty_satawal']+$angka_faktor_konversi;
													}
													else if ($rumus_konversi == '4') {
														$qty_dipecah = $list_harga[$ii]['qty_satawal']-$angka_faktor_konversi;
													}
													else
														$qty_dipecah = $list_harga[$ii]['qty_satawal'];
												}
												else
													$qty_dipecah = $list_harga[$ii]['qty_satawal'];
												
												$detail_harga = array(
														'id_bonmkeluar_detail'=>$id_bonmkeluar_detail,
														'qty'=>$qty_dipecah,
														'harga'=>$list_harga[$ii]['harganya'],
														'qty_satawal'=>$list_harga[$ii]['qty_satawal']
														);
													$this->db->insert('tm_bonmkeluar_detail_harga', $detail_harga);
											}
										//}
									}
								} // end if
								*/
								// ==================== END 05-09-2015  =================================
						
					} // end perulangan
			}
			
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "sjpumum/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "sjpumum/cform/cari/".$tgl_awal."/".$tgl_akhir."/".$cgudang."/".$ctujuan."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
			redirect($url_redirectnya);
  }

  function submit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
			$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			$tujuan 	= $this->input->post('tujuan', TRUE);  
			
			// 08-06-2015
			$id_unit_jahit 	= intval($this->input->post('id_unit_jahit', TRUE));  
			// 22-06-2015 ambil kode dan nama unit
			$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_unit_jahit' ");
			if ($query3->num_rows() > 0){
				$hasilnya = $query3->row();
				$kode_unit = $hasilnya->kode_unit;
				$nama_unit = $hasilnya->nama;
			}
			else {
				$kode_unit = '';
				$nama_unit = '';
			}
			
	
			$id_unit_packing 	= intval($this->input->post('id_unit_packing', TRUE));  
		
			$query5	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$id_unit_packing' ");
			if ($query5->num_rows() > 0){
				$hasilnya5 = $query5->row();
				$kode_unit_p = $hasilnya5->kode_unit;
				$nama_unit_p = $hasilnya5->nama;
			}
			else {
				$kode_unit_p = '';
				$nama_unit_p = '';
			}
			
			$ket 	= $this->input->post('ket', TRUE);  
			// 11-07-2015
			$dibebankan 	= $this->input->post('dibebankan', TRUE);  
			if ($dibebankan != '')
				$ket = $dibebankan;
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			//echo $tgl_bonm;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data($no_bonm_manual, $thn1, $tgl_bonm, $tujuan, $id_unit_jahit);
			if (count($cek_data) > 0) {
				$data['isi'] = 'sjpumum/vmainform';
				if ($id_unit_jahit == '0')
					$data['msg'] = "Data no bon M ".$no_bonm_manual." dgn tgl bon M ".$tgl_bonm." sudah ada..!";
				else
					$data['msg'] = "Data no bon M ".$no_bonm_manual." dgn tgl bon M ".$tgl_bonm." dan unit jahit ".$kode_unit."-".$nama_unit." sudah ada..!";
				// utk tahun $thn1 
				
				/*$th_now	= date("Y");
				$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluar ORDER BY no_bonm DESC LIMIT 1 ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) 
					$no_bonm	= $hasilrow->no_bonm;
				else
					$no_bonm = '';
				if(strlen($no_bonm)==9) {
					$nobonm = substr($no_bonm, 0, 9);
					$n_bonm	= (substr($nobonm,4,5))+1;
					$th_bonm	= substr($nobonm,0,4);
					if($th_now==$th_bonm) {
							$jml_n_bonm	= $n_bonm;
							switch(strlen($jml_n_bonm)) {
								case "1": $kodebonm	= "0000".$jml_n_bonm;
								break;
								case "2": $kodebonm	= "000".$jml_n_bonm;
								break;	
								case "3": $kodebonm	= "00".$jml_n_bonm;
								break;
								case "4": $kodebonm	= "0".$jml_n_bonm;
								break;
								case "5": $kodebonm	= $jml_n_bonm;
								break;	
							}
							$nomorbonm = $th_now.$kodebonm;
					}
					else {
						$nomorbonm = $th_now."00001";
					}
				}
				else {
					$nomorbonm	= $th_now."00001";
				} */
				$data['list_gudang'] = $this->mmaster->get_gudang();
				$data['list_satuan'] = $this->mmaster->get_satuan();
				//$data['no_bonm'] = $nomorbonm;
				$this->load->view('template',$data);
			}
			else {
                $i_sjp = $this->input->post('no_bonm_manual');
                $i_area = $this->input->post('i_area');
                $d_sjp = $tgl_bonm;
                $e_sender = $this->input->post('e_sender');
                
                /** static variable */
                $e_sender_company = 'CV. Duta Setia Garment';

                $e_recipient = $this->input->post('tujuan');

                /** static variable */
                $e_recipient_company = 'PT Dialogue Garmindo Utama';

                /** insert header */
                $return_id = $this->mmaster->insert_header($i_sjp, $i_area, $d_sjp, $e_sender, $e_sender_company, $e_recipient, $e_recipient_company);
                
                $count_detail = $this->input->post('no');
                $no_urut_detail = 1;
                for ($i=1; $i<$count_detail; $i++) {
                    if ($this->input->post("kode_$i") == null) {
                        continue;
                    }
                    
                    $i_product = $this->input->post("kode_$i");
                    $e_product_name = $this->input->post("nama_$i");
                    $n_quantity = $this->input->post("qty_$i");
                    $n_satuan = $this->input->post("satuan_$i");
                    $remark = $this->input->post("remark_$i");
                    
                    $this->mmaster->input_detail($return_id, $i_product, $e_product_name, $n_quantity, $n_satuan, $remark);

                    $no_urut_detail++;
                }

                redirect('sjpumum/cform/view');
                /*
				// cek apa udah ada datanya blm dgn no request tadi
				$tgl = date("Y-m-d H:i:s");
				//$this->db->select("id from tm_bonmkeluar WHERE no_bonm = '$no_bonm' ", false);
				// 31-07-2015 GANTI
				$this->db->select("id from tm_bonmkeluar WHERE no_manual = '$no_bonm_manual' AND tgl_bonm = '$tgl_bonm'
				AND tujuan = '$tujuan' AND id_unit_jahit='$id_unit_jahit' ", false); 
				
				$query = $this->db->get();
				$hasil = $query->result();
							
				// jika data header blm ada 
				if(count($hasil)== 0) {
					$th_now	= date("Y");
					$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluar ORDER BY no_bonm DESC LIMIT 1 ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() != 0) 
						$no_bonm	= $hasilrow->no_bonm;
					else
						$no_bonm = '';
					if(strlen($no_bonm)==9) {
						$nobonm = substr($no_bonm, 0, 9);
						$n_bonm	= (substr($nobonm,4,5))+1;
						$th_bonm	= substr($nobonm,0,4);
						if($th_now==$th_bonm) {
								$jml_n_bonm	= $n_bonm;
								switch(strlen($jml_n_bonm)) {
									case "1": $kodebonm	= "0000".$jml_n_bonm;
									break;
									case "2": $kodebonm	= "000".$jml_n_bonm;
									break;	
									case "3": $kodebonm	= "00".$jml_n_bonm;
									break;
									case "4": $kodebonm	= "0".$jml_n_bonm;
									break;
									case "5": $kodebonm	= $jml_n_bonm;
									break;	
								}
								$nomorbonm = $th_now.$kodebonm;
						}
						else {
							$nomorbonm = $th_now."00001";
						}
					}
					else {
						$nomorbonm	= $th_now."00001";
					}
					
					// 28-08-2015
					$uid_update_by = $this->session->userdata('uid');
					
					// insert di tm_bonm
					$data_header = array(
						  //'no_bonm'=>$no_bonm,
						  'no_bonm'=>$nomorbonm,
						  'no_manual'=>$no_bonm_manual,
						  'tgl_bonm'=>$tgl_bonm,
						  'tujuan'=>$tujuan,
						  // 08-06-2015
						  'id_unit_jahit'=>$id_unit_jahit,
						  'id_unit_packing'=>$id_unit_packing,
						  'id_gudang'=>$id_gudang,
						  'tgl_input'=>$tgl,
						  'tgl_update'=>$tgl,
						  'keterangan'=>$ket,
						  'uid_update_by'=>$uid_update_by
						);
					$this->db->insert('tm_bonmkeluar',$data_header);
				} // end if
				
				// ambil data terakhir di tabel tm_bonmkeluar. 01-08-2015 GANTI JADI ACUAN no_bonm
				//$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluar ORDER BY id DESC LIMIT 1 ");
				$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluar WHERE no_manual = '$no_bonm_manual' AND tgl_bonm = '$tgl_bonm'
				AND tujuan = '$tujuan' AND id_unit_jahit='$id_unit_jahit' AND id_unit_packing='$id_unit_packing' ");
				$hasilrow = $query2->row();
				$id_bonm	= $hasilrow->id; 
				
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->save($id_bonm, $id_gudang, 
							//$this->input->post('is_quilting_'.$i, TRUE),
							$this->input->post('id_brg_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								$this->input->post('qty_'.$i, TRUE), $this->input->post('stok_'.$i, TRUE),  
								$this->input->post('satuan_'.$i, TRUE), 
								$this->input->post('satuan_konv_'.$i, TRUE), $this->input->post('qty_satawal_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE), $this->input->post('untuk_bisbisan_'.$i, TRUE),
								$this->input->post('id_brg_wip_'.$i, TRUE) );						
				}
				redirect('sjpumum/cform/view');
                */
			}
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'sjpumum/vformview';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	// 27-07-2015

	if($this->session->userdata('gid') == 16) {
	$cgudang = '16';
	}
	else
	{
	$cgudang = '0';
	}
	
	// 04-11-2015
	$ctujuan = '0';
	$caribrg = "all";
	$filterbrg = "n";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to, $cgudang, $ctujuan, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/sjpumum/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $date_from, $date_to, $cgudang, $ctujuan, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['cgudang'] = $cgudang;
	$data['ctujuan'] = $ctujuan;
	
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	// 27-07-2015
	$cgudang = $this->input->post('gudang', TRUE);  
	// 04-11-2015
	$ctujuan = $this->input->post('tujuan', TRUE);  
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
    
    /*if ($keywordcari == '' && ($date_from == '' || $date_to == '')) {
		$date_from 	= $this->uri->segment(4);
		$date_to 	= $this->uri->segment(5);
		$keywordcari = $this->uri->segment(6);
	} */
	
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($cgudang == '')
		$cgudang = $this->uri->segment(6);
	if ($ctujuan == '')
		$ctujuan = $this->uri->segment(7);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(8);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(9);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(10);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($cgudang == '')
		$cgudang = '0';
	if ($ctujuan == '')
		$ctujuan = '0';
	if ($filterbrg == '')
		$filterbrg = 'n';
	if ($caribrg == '')
		$caribrg = "all";
	
	$jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to, $cgudang, $ctujuan, $caribrg, $filterbrg);
    
	$config['base_url'] = base_url().'index.php/sjpumum/cform/cari/'.$date_from.'/'.$date_to.'/'.$cgudang.'/'.$ctujuan.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
	$config['total_rows'] = count($jum_total); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(11);
	$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(11), $keywordcari, $date_from, $date_to, $cgudang, $ctujuan, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'sjpumum/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['cgudang'] = $cgudang;
	$data['ctujuan'] = $ctujuan;
	
	$this->load->view('template',$data);
  }

  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_gudang	= $this->uri->segment(4);
	$is_quilting 	= $this->uri->segment(5);
	$posisi 	= $this->uri->segment(6);
	
	if ($id_gudang == '' || $is_quilting == '' || $posisi == '') {
		$id_gudang = $this->input->post('id_gudang', TRUE);  
		$is_quilting 	= $this->input->post('is_quilting', TRUE);  
		$posisi 	= $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || ($id_gudang == '' && $is_quilting == '' && $posisi == '') ) {
			$id_gudang	= $this->uri->segment(4);
			$is_quilting 	= $this->uri->segment(5);
			$posisi 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$keywordcari = str_replace("%20"," ", $keywordcari);
		//echo $id_gudang." ".$is_quilting." ".$posisi." ".$keywordcari;			  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $id_gudang, $is_quilting);
		
				$config['base_url'] = base_url()."index.php/sjpumum/cform/show_popup_brg/".$id_gudang."/".
							$is_quilting."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $id_gudang, $is_quilting);
	$data['jum_total'] = count($qjum_total);
	$data['id_gudang'] = $id_gudang;
	$data['is_quilting'] = $is_quilting;
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$id_gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$nama_gudang	= '';
	}
	$data['nama_gudang'] = $nama_gudang;

	$this->load->view('sjpumum/vpopupbrg',$data);
  }
  
  function show_popup_brg_wip(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	
	$posisi 	= $this->input->post('posisi', TRUE);
	$keywordcari 	= $this->input->post('cari', TRUE);  
	if ( $posisi == '') {
		$posisi 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$keywordcari = str_replace("%20"," ", $keywordcari);
		//echo $id_gudang." ".$is_quilting." ".$posisi." ".$keywordcari;			  
		$qjum_total = $this->mmaster->get_wiptanpalimit($keywordcari);
		
				$config['base_url'] = base_url()."index.php/sjpumum/cform/show_popup_brg_wip/".$posisi.'/'.$keywordcari;
							
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_wip($config['per_page'],$config['cur_page'], $keywordcari);
	$data['jum_total'] = count($qjum_total);
	
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$this->load->view('sjpumum/vpopupbrgwip',$data);
  }
  
  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$cgudang 	= $this->uri->segment(9);
	$ctujuan 	= $this->uri->segment(10);
	$carinya 	= $this->uri->segment(11);
	$caribrg 	= $this->uri->segment(12);
	$filterbrg 	= $this->uri->segment(13);
    
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "sjpumum/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "sjpumum/cform/cari/".$tgl_awal."/".$tgl_akhir."/".$cgudang."/".$ctujuan."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  
  function generate_nomor() {
		//$jenis_brg 	= $this->uri->segment(4);
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
    
  // ===================================================== 21-09-2013 =====================================================
  function addbonmkeluarcutting(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluarcutting ORDER BY no_bonm DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==9) {
				$nobonm = substr($no_bonm, 0, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm;
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			}

	$data['isi'] = 'sjpumum/vform1bonmkeluarcutting';
	$data['msg'] = '';
	//$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['no_bonm'] = $nomorbonm;
	$this->load->view('template',$data);
  }
  
  function show_popup_brg_jadi(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$posisi		= $this->input->post('posisi', TRUE);   
	$keywordcari	= $this->input->post('cari', TRUE);  
	
	if ($posisi=='' && $keywordcari=='') {
		$posisi 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}	

	if ($keywordcari=='')
		$keywordcari = "all";
			
	$qjum_total = $this->mmaster->getbarangjaditanpalimit($keywordcari);

	$config['base_url'] 	= base_url()."index.php/sjpumum/cform/show_popup_brg_jadi/".$posisi."/".$keywordcari."/";
	$config['total_rows'] 	= count($qjum_total); 
	$config['per_page'] 	= 10;
	$config['first_link'] 	= 'Awal';
	$config['last_link'] 	= 'Akhir';
	$config['next_link'] 	= 'Selanjutnya';
	$config['prev_link'] 	= 'Sebelumnya';
	$config['cur_page']		= $this->uri->segment(6,0);
	$this->pagination->initialize($config);
	
	$data['query']		= $this->mmaster->getbarangjadi($config['per_page'],$config['cur_page'], $keywordcari);	
	$data['jum_total']	= count($qjum_total);
	$data['posisi']		= $posisi;
	$data['jumdata'] = $posisi-1;

	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
				
	$data['startnya']	= $config['cur_page'];
	
	$this->load->view('sjpumum/vpopupbrgjadi',$data);  
  } 
  
  // 23-09-2013
  function viewbonmkeluarcutting(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
	
	// 24-02-2015
	$submit2 	= $this->input->post('submit2', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($kode_unit_jahit == '0')
		$kode_unit_jahit = $this->uri->segment(6);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(7);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(8);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(9);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($kode_unit_jahit == '')
		$kode_unit_jahit = '0';
	if ($caribrg == '')
		$caribrg = "all";
	if ($filterbrg == '')
		$filterbrg = "n";
	
	if ($submit2 == "") {
		$jum_total = $this->mmaster->getAllbonmkeluarcuttingtanpalimit($keywordcari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg);
		
					$config['base_url'] = base_url().'index.php/sjpumum/cform/viewbonmkeluarcutting/'.$date_from.'/'.$date_to.'/'.$kode_unit_jahit.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
					$config['total_rows'] = count($jum_total); 
					$config['per_page'] = '10';
					$config['first_link'] = 'Awal';
					$config['last_link'] = 'Akhir';
					$config['next_link'] = 'Selanjutnya';
					$config['prev_link'] = 'Sebelumnya';
					$config['cur_page'] = $this->uri->segment(10);
					$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllbonmkeluarcutting($config['per_page'],$this->uri->segment(10), $keywordcari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		
		$data['isi'] = 'sjpumum/vformviewbonmkeluarcutting';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		if ($date_from=="00-00-0000")
			$data['date_from'] = '';
		else
			$data['date_from'] = $date_from;
		
		if ($date_to=="00-00-0000")
			$data['date_to'] = '';
		else
			$data['date_to'] = $date_to;
		
		if ($caribrg == "all")
			$data['caribrg'] = '';
		else
			$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;
		$data['kode_unit_jahit'] = $kode_unit_jahit;
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$this->load->view('template',$data);
	}
	else {
		// export excel
		$query = $this->mmaster->getAllbonmkeluarcutting_export($keywordcari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='10' align='center'>DATA SJ KELUAR BAHAN BAKU (HASIL CUTTING) KE JAHITAN</th>
				 </tr>
				</table><br>";
				
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th width='3%'>No</th>
					 <th width='10%'>No Bon M / SJ</th>
					 <th width='10%'>No Manual</th>
					 <th width='8%'>Tgl Bon M</th>
					 <th width='10%'>Jenis</th>
					 <th width='10%'>Unit Jahit</th>
					 <th width='15%'>List Barang Jadi</th>
					 <th width='5%'>Qty</th>
					 <th width='15%'>Qty Ket Warna</th>
					 <th>Last Update</th>
				 </tr>
				</thead>
				<tbody>";
				
		if (is_array($query)) {
			$no=1;
			for($j=0;$j<count($query);$j++){

				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";

				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;

				$pisah1 = explode("-", $query[$j]['tgl_bonm']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_bonm = $tgl1." ".$nama_bln." ".$thn1;
				 
				 $html_data.= "<tr>";
				 $html_data.=    "<td>".$no."</td>";
				 $html_data.=    "<td>".$query[$j]['no_bonm']."</td>";
				 $html_data.=    "<td>".$query[$j]['no_manual']."</td>";
				 $html_data.=    "<td>".$tgl_bonm."</td>";
				 $html_data.=    "<td>".$query[$j]['nama_jenis']."</td>";
				 
				$html_data.=    "<td>".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nama_brg_jadi'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 				 
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['ket_qty_warna'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				  
				 $html_data.=    "<td align='center'>".$tgl_update."</td>";
				 				 
				 $html_data.=  "</tr>";
				 $no++;
			} // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "data_sj_keluar_bhnbaku_hasil_cutting_jahitan";
		$export_excel1 = '1';
		//if ($export_excel1 != '')
			$nama_file.= ".xls";
		//else
		//	$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
	}
  }
  
  function submitbonmkeluarcutting(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
			$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);  
			$jenis 	= $this->input->post('jenis', TRUE);  
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $kode_unit_jahit, $jenis);
			if (count($cek_data) > 0) {
				$data['isi'] = 'sjpumum/vform1bonmkeluarcutting';
				$data['msg'] = "Data no bon M ".$no_bonm_manual." utk tahun $thn1 sudah ada..!";
				
				$th_now	= date("Y");
				$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluarcutting ORDER BY no_bonm DESC LIMIT 1 ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) 
					$no_bonm	= $hasilrow->no_bonm;
				else
					$no_bonm = '';
				if(strlen($no_bonm)==9) {
					$nobonm = substr($no_bonm, 0, 9);
					$n_bonm	= (substr($nobonm,4,5))+1;
					$th_bonm	= substr($nobonm,0,4);
					if($th_now==$th_bonm) {
							$jml_n_bonm	= $n_bonm;
							switch(strlen($jml_n_bonm)) {
								case "1": $kodebonm	= "0000".$jml_n_bonm;
								break;
								case "2": $kodebonm	= "000".$jml_n_bonm;
								break;	
								case "3": $kodebonm	= "00".$jml_n_bonm;
								break;
								case "4": $kodebonm	= "0".$jml_n_bonm;
								break;
								case "5": $kodebonm	= $jml_n_bonm;
								break;	
							}
							$nomorbonm = $th_now.$kodebonm;
					}
					else {
						$nomorbonm = $th_now."00001";
					}
				}
				else {
					$nomorbonm	= $th_now."00001";
				}
				
				$data['no_bonm'] = $nomorbonm;
				$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
				$this->load->view('template',$data);
			}
			else {
				// 11-09-2014
				$tgl = date("Y-m-d H:i:s");
				// insert di tm_bonmkeluarcutting
				$data_header = array(
				  'no_bonm'=>$no_bonm,
				  'no_manual'=>$no_bonm_manual,
				  'tgl_bonm'=>$tgl_bonm,
				  'jenis_keluar'=>$jenis,
				  'kode_unit'=>$kode_unit_jahit,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'keterangan'=>$ket
				);
				$this->db->insert('tm_bonmkeluarcutting',$data_header);
				
				// ambil data terakhir di tabel tm_bonmkeluarcutting
				$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query2->row();
				$id_bonm	= $hasilrow->id; 
											
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					/*$this->mmaster->savebonmkeluarcutting($no_bonm,$tgl_bonm, $no_bonm_manual, $jenis, $kode_unit_jahit, $ket, 
							$this->input->post('kode_brg_jadi_'.$i, TRUE), 
								//$this->input->post('qty_'.$i, TRUE), 
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) ); */
								
					$this->mmaster->savebonmkeluarcutting($id_bonm, $jenis, $kode_unit_jahit, 
							$this->input->post('kode_brg_jadi_'.$i, TRUE), 
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) );
				}
				redirect('sjpumum/cform/viewbonmkeluarcutting');
			}
  }
  
  function deletebonmkeluarcutting(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$tgl_awal 	= $this->uri->segment(6);
	$tgl_akhir 	= $this->uri->segment(7);
	$kode_unit_jahit 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
    
    $this->mmaster->deletebonmkeluarcutting($id);
    
    if ($carinya == '') $carinya = "all";

	$url_redirectnya = "sjpumum/cform/viewbonmkeluarcutting/".$tgl_awal."/".$tgl_akhir."/".$kode_unit_jahit."/".$carinya."/".$cur_page;
	redirect($url_redirectnya);
  }
  
  function editbonmkeluarcutting(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_bonm 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$tgl_awal 	= $this->uri->segment(6);
	$tgl_akhir 	= $this->uri->segment(7);
	$kode_unit_jahit 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['cur_page'] = $cur_page;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_bonmkeluarcutting($id_bonm); 
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['msg'] = '';
	$data['isi'] = 'sjpumum/veditformbonmkeluarcutting';
	$this->load->view('template',$data);

  }
  
  function updatedatabonmkeluarcutting() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_bonm 	= $this->input->post('id_bonm', TRUE);
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$jenis 	= $this->input->post('jenis', TRUE);  
			$kode_unit 	= $this->input->post('kode_unit', TRUE);  
			$kode_unit_lama 	= $this->input->post('kode_unit_lama', TRUE); 
			
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$kode_unit_jahit = $this->input->post('kode_unit_jahit', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");

			//update headernya
			$this->db->query(" UPDATE tm_bonmkeluarcutting SET tgl_bonm = '$tgl_bonm', jenis_keluar = '$jenis', 
							kode_unit = '$kode_unit', tgl_update='$tgl', no_manual='$no_bonm_manual', 
							keterangan = '$ket' where id= '$id_bonm' ");
							
				//reset stok, dan hapus dulu detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail WHERE id_bonmkeluarcutting = '$id_bonm' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					foreach ($hasil2 as $row2) {
						$qty = $row2->qty;
						
						// 1. stok total
						// ============ update stok PABRIK =====================
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
								
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$row2->kode_brg_jadi' ");
						
						//================ update stok UNIT JAHIT ===============================
						if ($kode_unit_lama != '0') {
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit
											WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_unit='$kode_unit_lama' ");
							if ($query3->num_rows() == 0){
								$id_stok_unit = 0;
								$stok_unit_lama = 0;
								$stok_unit_bagus_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								$stok_unit_bagus_lama = $hasilrow->stok_bagus;
							}
							$new_stok_unit = $stok_unit_lama-$row2->qty; // berkurang stok unit karena reset bon M keluar
							$new_stok_unit_bagus = $stok_unit_bagus_lama-$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
										WHERE kode_brg_jadi= '$row2->kode_brg_jadi' AND kode_unit = '$kode_unit_lama' ");
						} // end if
						
						// 2. reset stok per warna dari tabel tm_bonmkeluarcutting_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail_warna 
												WHERE id_bonmkeluarcutting_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND kode_warna='$rowwarna->kode_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari bon M keluar
										
								$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND kode_warna = '$rowwarna->kode_warna' ");
								
								// ====================	15-03-2014, stok hasil cutting bhn baku ==================
								//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
								$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '".$rowwarna->kode_warna."'
										AND id_stok_hasil_cutting='$id_stok' ");
								if ($query3->num_rows() > 0){
									$hasil3 = $query3->result();
									foreach ($hasil3 as $row3) {	
										$new_stok_bhnbaku = $row3->stok+$rowwarna->qty;
										$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
												where id= '".$row3->id."' ");
									}
								}
								// ==========================================================
								
								// ============= update stok unit jahit
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND kode_warna='$rowwarna->kode_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warna_bagus_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama-$rowwarna->qty; // berkurang stok di unit karena reset dari bon M keluar
									$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama-$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna',
												stok_bagus = '$new_stok_unit_warna_bagus',
												tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND kode_warna = '$rowwarna->kode_warna' ");
							
							}
						} // end if detail warna
						
						// 05-12-2014, reset stok hasil cutting bhn baku yg kode warnanya 00
						// ====================	==================
						//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '00'
									AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
							foreach ($hasil3 as $row3) {	
								$new_stok_bhnbaku = $row3->stok+$row2->qty;
								$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
										where id= '".$row3->id."' ");
							}
						}
						// ==========================================================
						
						$this->db->query(" DELETE FROM tm_bonmkeluarcutting_detail_warna WHERE id_bonmkeluarcutting_detail = '$row2->id' ");
						
						// ============ update stok =====================
						// modif 27-01-2014, sementara ga dipake =
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
							/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting 
														WHERE kode_brg = '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'kode_brg_jadi'=>$row2->kode_brg_jadi,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_cutting', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' AND kode_brg_jadi= '$row2->kode_brg_jadi' ");
								} */
						// ==============================================
					} // end foreach detail
				} // end reset stok
				//=============================================
				$this->db->delete('tm_bonmkeluarcutting_detail', array('id_bonmkeluarcutting' => $id_bonm));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{	
						$kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
						//$qty = $this->input->post('qty_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
						
						$temp_qty = $this->input->post('temp_qty_'.$i, TRUE);
						$kode_warna = $this->input->post('kode_warna_'.$i, TRUE);
						$qty_warna = $this->input->post('qty_warna_'.$i, TRUE);
						
						//-------------- hitung total qty dari detail tiap2 warna -------------------
						$qtytotal = 0;
						for ($xx=0; $xx<count($kode_warna); $xx++) {
							$kode_warna[$xx] = trim($kode_warna[$xx]);
							$qty_warna[$xx] = trim($qty_warna[$xx]);
							$qtytotal+= $qty_warna[$xx];
						} // end for
						// ---------------------------------------------------------------------
						
						// 1. stok total
						// ============================= update stok PABRIK ======================================
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() == 0){
								$id_stok = 0;
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok	= $hasilrow->id;
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qtytotal;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
								$data_stok = array(
									'kode_brg_jadi'=>$kode_brg_jadi,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting', $data_stok);
								
								// ambil id_stok utk dipake di stok warna
								$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
								if($sqlxx->num_rows() > 0) {
									$hasilxx	= $sqlxx->row();
									$id_stok	= $hasilxx->id;
								}else{
									$id_stok	= 1;
								}
							}
							else {
								$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$kode_brg_jadi' ");
							}
					
					// ============= update stok UNIT JAHIT ======================
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode_brg_jadi'
											AND kode_unit='$kode_unit' ");
						if ($query3->num_rows() == 0){
							$id_stok_unit = 0;
							$stok_unit_lama = 0;
							$stok_unit_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							$stok_unit_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit = $stok_unit_lama+$qtytotal;
						$new_stok_unit_bagus = $stok_unit_bagus_lama+$qtytotal;
							
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'kode_brg_jadi'=>$kode_brg_jadi,
								'stok'=>$new_stok_unit,
								'stok_bagus'=>$new_stok_unit_bagus,
								'kode_unit'=>$kode_unit,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
								stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
								where kode_brg_jadi= '$kode_brg_jadi' AND kode_unit = '$kode_unit' ");
						}
					// =====================================================================================
						
						// ======== update stoknya! =============

						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
						
						// modif, stok sementara ga dipake
						/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting WHERE kode_brg = '$kode'
													AND kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
								$data_stok = array(
									'kode_brg'=>$kode,
									'kode_brg_jadi'=>$kode_brg_jadi,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting', $data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode' AND kode_brg_jadi= '$kode_brg_jadi' ");
							} */
							
					// jika semua data tdk kosong, insert ke tm_bonmkeluarcutting_detail
					$data_detail = array(
						'id_bonmkeluarcutting'=>$id_bonm,
						'kode_brg_jadi'=>$kode_brg_jadi,
						'qty'=>$qtytotal,
						'keterangan'=>$ket_detail
					);
					$this->db->insert('tm_bonmkeluarcutting_detail',$data_detail);
							// ================ end insert item detail ===========
					
					// ambil id detail tm_bonmkeluarcutting_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;
					
					// ----------------------------------------------
					for ($xx=0; $xx<count($kode_warna); $xx++) {
						$kode_warna[$xx] = trim($kode_warna[$xx]);
						$qty_warna[$xx] = trim($qty_warna[$xx]);
								
						$seq_warna	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail_warna ORDER BY id DESC LIMIT 1 ");
							
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}

						$tm_bonmkeluarcutting_detail_warna	= array(
							 'id'=>$idbaru,
							 'id_bonmkeluarcutting_detail'=>$iddetail,
							 'id_warna_brg_jadi'=>'0',
							 'kode_warna'=>$kode_warna[$xx],
							 'qty'=>$qty_warna[$xx]
						);
						$this->db->insert('tm_bonmkeluarcutting_detail_warna',$tm_bonmkeluarcutting_detail_warna);
						
						// ========================= 04-02-2014, stok per warna ===============================================
						//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() == 0){
							$stok_warna_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_warna_lama	= $hasilrow->stok;
						}
						$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_cutting'=>$id_stok,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_warna,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
						}
						
						// ====================	15-03-2014, stok hasil cutting bhn baku ==================
						//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() == 0){
							$stok_bhn_lama = 0;
							$new_stok_bhnbaku = $stok_bhn_lama-$qty_warna[$xx];
							// ambil data2 material brg jadi berdasarkan brg jadi, trus insert langsung ke tabel stok_hasil_cutting_bhnbaku. qty-nya sesuai dgn warnanya
							$sqlxx = " SELECT * FROM tm_material_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' AND kode_warna='".$kode_warna[$xx]."'
										AND kode_brg <> '00' ";
							$queryxx = $this->db->query($sqlxx);
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->result();
								foreach ($hasilxx as $rowxx) {			
									$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_bhnbaku ORDER BY id DESC LIMIT 1 ");
									if($seq_stokwarna->num_rows() > 0) {
										$seq_stokwarnarow	= $seq_stokwarna->row();
										$id_stok_warna	= $seq_stokwarnarow->id+1;
									}else{
										$id_stok_warna	= 1;
									}
									
									$data_stok = array(
										'id'=>$id_stok_warna,
										'id_stok_hasil_cutting'=>$id_stok,
										'kode_brg'=>$rowxx->kode_brg,
										'kode_warna'=>$kode_warna[$xx],
										'stok'=>$new_stok_bhnbaku,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_cutting_bhnbaku', $data_stok);
								} // end xxx
							} // end if
							
						}
						else {
							$hasil3 = $query3->result();
							foreach ($hasil3 as $row3) {	
								$new_stok_bhnbaku = $row3->stok-$qty_warna[$xx];
								$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
										where id= '".$row3->id."' ");
							}
						} // end if
						// ==========================================================
						
						// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warna_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama+$qty_warna[$xx]; // bertambah stok karena masuk ke unit
						$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								'stok_bagus'=>$new_stok_unit_warna_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
					// ------------------------------------------------------------------------------------------
						
					} // end for
					
					// 05-12-2014, update stok hasil cutting bhn baku yg tidak ada warna
					//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE id_stok_hasil_cutting='$id_stok'
					AND kode_warna = '00' ");
					if ($query3->num_rows() == 0){
						$stok_bhn_lama = 0;
						$new_stok_bhnbaku = $stok_bhn_lama-$qtytotal;
						
						// ambil data2 material brg jadi berdasarkan brg jadi, trus insert langsung ke tabel stok_hasil_cutting_bhnbaku. qty-nya sesuai dgn warnanya
						// 05-12-2014, kode brg tidak boleh 00
						$sqlxx = " SELECT * FROM tm_material_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' 
									AND kode_brg <> '00' AND kode_warna = '00' ";
						$queryxx = $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->result();
							foreach ($hasilxx as $rowxx) {			
								$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_bhnbaku ORDER BY id DESC LIMIT 1 ");
								if($seq_stokwarna->num_rows() > 0) {
									$seq_stokwarnarow	= $seq_stokwarna->row();
									$id_stok_warna	= $seq_stokwarnarow->id+1;
								}else{
									$id_stok_warna	= 1;
								}
								
								$data_stok = array(
									'id'=>$id_stok_warna,
									'id_stok_hasil_cutting'=>$id_stok,
									'kode_brg'=>$rowxx->kode_brg,
									'kode_warna'=>'00',
									'stok'=>$new_stok_bhnbaku,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting_bhnbaku', $data_stok);
							} // end xxx
						} // end if
						
					}
					else {
						//$hasilrow = $query3->row();
						//$stok_warna_lama	= $hasilrow->stok;
						$hasil3 = $query3->result();
						foreach ($hasil3 as $row3) {	
							$new_stok_bhnbaku = $row3->stok-$qtytotal;
							$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
									where id= '".$row3->id."' ");
						}
					} // end if
					//DONE
					// ----------------------------------------------
						
				} // end perulangan
					//die();
			if ($carinya == '') $carinya = "all";
			$url_redirectnya = "sjpumum/cform/viewbonmkeluarcutting/".$tgl_awal."/".$tgl_akhir."/".$kode_unit_jahit."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
  }
  
  // 04-02-2014
  function additemwarna(){

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna_brg_jadi'=> $rowxx->id,
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('sjpumum/vlistwarna', $data); 
		return true;
  }
  
  // 25-03-2014
  function caribrgjadi(){
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel brg jadi utk ambil kode, nama, dan stok
		$queryxx = $this->db->query(" SELECT e_product_motifname FROM tr_product_motif
									WHERE i_product_motif = '".$kode_brg_jadi."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$nama_brg_jadi = $hasilxx->e_product_motifname;
		}
		else
			$nama_brg_jadi = '';
		//echo $nama_brg_jadi; die();
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('sjpumum/vinfobrgjadi', $data); 
		return true;
  }
  
  // 04-04-2014
  function ceknobonm(){
		$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
		$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);
		$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);
		$jenis 	= $this->input->post('jenis', TRUE);
		
		$pisah1 = explode("-", $tgl_bonm);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		
		$cek_data = $this->mmaster->cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $kode_unit_jahit, $jenis);
		if (count($cek_data) > 0)
			$ada = "ada";	
		else
			$ada = "tidak";
		
		echo $ada; 
		return true;
  }
  
  // 04-11-2015
  function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		//if ($isedit == '0') {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		//}
				
		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('sjpumum/vinfobrgwip', $data); 
		return true;
  }

  function generate_nomor_dokumen()
  {
        $date = $this->input->post('date');

        /** reformat date */
        $date = date('Y-m-d', strtotime($date));
        
        $i_document = $this->mmaster->generate_nomor_dokumen($date);

        echo $i_document;
  }
  
}
