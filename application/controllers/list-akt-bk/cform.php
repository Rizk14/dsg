<?php
class Cform extends Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') &&  
			$this->session->userdata('isession')!=0) 
		{
			$data['page_title'] = $this->lang->line('listbk');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('list-akt-bk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') &&  
			$this->session->userdata('isession')!=0) 
		{
			/*----------  Ambil Closing Dari Table  ----------*/			
			$tableclosing = $this->db->query("SELECT d_closing_kbank, d_closing_kbankin, d_open_kbank, d_open_kbankin FROM tm_closing_kas_bank", FALSE);
			if ($tableclosing->num_rows()>0) {
				$row = $tableclosing->row();
				$data['d_closing_kbank']   = date('Ymd', strtotime($row->d_closing_kbank));
				$data['d_open_kbank']      = date('Ymd', strtotime($row->d_open_kbank));
				$data['d_closing_kbankin'] = date('Ymd', strtotime($row->d_closing_kbankin));
				$data['d_open_kbankin']    = date('Ymd', strtotime($row->d_open_kbankin));
			}else{
				$data['d_closing_kbank']   = date('Ym').'04';
				$data['d_open_kbank'] 	   = date('Ym').'01';
				$data['d_closing_kbankin'] = date('Ym').'04';
				$data['d_open_kbankin']	   = date('Ym').'01';
			}

			$lepel		= $this->session->userdata('i_area');
			$cari		  = strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		  = $this->input->post('dto');
			$icoabank = $this->input->post('icoa');
			$ibank		= $this->input->post('ibank');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($icoabank=='') $icoabank	= $this->uri->segment(6);
			if($ibank=='') $ibank	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/list-akt-bk/cform/view/'.$dfrom.'/'.$dto.'/'.$icoabank.'/'.$ibank.'/index/';
			$query = $this->db->query(" select	distinct on (i_kbank) vc, f_kbank_cancel, i_kbank, i_area, d_bank, i_coa, e_description, v_bank, v_sisa, i_periode, f_debet, f_posting, 
		                    i_cek, e_area_name, i_bank, i_coa_bank, e_nama_bank
	                    	from(
                  			select x.i_rvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, 
                  			a.v_sisa, a.i_periode, a.f_debet, a.f_posting, 
                  			a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_nama_bank
                  			from tr_area e, tm_kbank a
                  			left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' 
                  			and b.i_coa_bank='$icoabank')
                  			left join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                  			left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv 
                  			and x.i_coa_bank='$icoabank')
                  			left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                  			where (upper(a.i_kbank) like '%$cari%' or upper(c.i_rv) like '%$cari%') and a.i_area=e.i_area
                  			and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
                  			and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_debet = 'f'
                  			union
                    		select x.i_pvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, 
                    		a.v_sisa, a.i_periode, a.f_debet, a.f_posting, 
                    		a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_nama_bank
                    		from tr_area e, tm_kbank a
                    		left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' 
                    		and b.i_coa_bank='$icoabank')
                    		left join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                    		left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv 
                    		and x.i_coa_bank='$icoabank')
                    		left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                    		where (upper(a.i_kbank) like '%$cari%' or upper(c.i_pv) like '%$cari%') and a.i_area=e.i_area 
                    		and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
                    		and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_debet = 't'
                  			) as a
                        ORDER BY i_kbank",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->paginationxx->initialize($config);
			$data['page_title'] = strtoupper("List Daftar Bank");
			$this->load->model('list-akt-bk/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']  = $dfrom;
			$data['dto']		= $dto;
			$data['icoabank']		= $icoabank;
			$data['ibank']  = $ibank;
			$data['lepel']	= $lepel;
			$data['isi']		= $this->mmaster->bacaperiode($icoabank,$dfrom,$dto,$ibank,$config['per_page'],$this->uri->segment(9),$cari);
			/*$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data  Bank '.$ibank.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  */

			$this->load->view('list-akt-bk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') &&  
			$this->session->userdata('isession')!=0) 
		{
			$data['page_title'] = $this->lang->line('listgj');
			$this->load->view('list-akt-bk/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		/*if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		)*/
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') &&  
			$this->session->userdata('isession')!=0) 
		{
			$ikb		= $this->uri->segment(4);
			$iperiode	= $this->uri->segment(5);
			$iarea		= $this->uri->segment(6);
			$dfrom	  	= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$icoabank	= $this->uri->segment(9);
			$ibank		= $this->uri->segment(10);
	    	$lepel		= $this->session->userdata('i_area');
				$this->load->model('list-akt-bk/mmaster');
	      		$this->db->trans_begin();
				$this->mmaster->delete($iperiode,$iarea,$ikb,$icoabank);
	      	if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
			  /*$sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Hapus Kas Kecil No:'.$ikb.' Coa Bank '.$icoabank.'  Area:'.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan ); */
#				$this->db->trans_rollback();
				$this->db->trans_commit();
      }
			$cari		= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/list-akt-bk/cform/view/'.$dfrom.'/'.$dto.'/'.$icoabank.'/'.$ibank.'/index/';
			$query = $this->db->query(" select	distinct on (i_kbank) vc, f_kbank_cancel, i_kbank, i_area, d_bank, i_coa, e_description, v_bank, v_sisa, i_periode, f_debet, f_posting, 
		                    i_cek, e_area_name, i_bank, i_coa_bank, e_nama_bank
	                    	from(
                  			select x.i_rvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, 
                  			a.v_sisa, a.i_periode, a.f_debet, a.f_posting, 
                  			a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_nama_bank
                  			from tr_area e, tm_kbank a
                  			left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' 
                  			and b.i_coa_bank='$icoabank')
                  			left join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                  			left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv 
                  			and x.i_coa_bank='$icoabank')
                  			left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                  			where (upper(a.i_kbank) like '%$cari%' or upper(c.i_rv) like '%$cari%') and a.i_area=e.i_area
                  			and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
                  			and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_debet = 'f'
                  			union
                    		select x.i_pvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, 
                    		a.v_sisa, a.i_periode, a.f_debet, a.f_posting, 
                    		a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_nama_bank
                    		from tr_area e, tm_kbank a
                    		left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' 
                    		and b.i_coa_bank='$icoabank')
                    		left join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                    		left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv 
                    		and x.i_coa_bank='$icoabank')
                    		left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                    		where (upper(a.i_kbank) like '%$cari%' or upper(c.i_pv) like '%$cari%') and a.i_area=e.i_area 
                    		and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
                    		and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_debet = 't'
                  			) as a
                        ORDER BY i_kbank",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(11);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line($icoabank);
			$this->load->model('list-akt-bk/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']  = $dfrom;
			$data['dto']		= $dto;
			$data['icoabank']		= $icoabank;
			$data['ibank']  = $ibank;
			$data['lepel']	= $lepel;
			$data['isi']		= $this->mmaster->bacaperiode($icoabank,$dfrom,$dto,$ibank,$config['per_page'],$this->uri->segment(11),$cari);
			/*$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Kas Kecil Bank '.$ibank.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  */
			/*----------  Ambil Closing Dari Table  ----------*/			
			$tableclosing = $this->db->query("SELECT d_closing_kbank, d_closing_kbankin, d_open_kbank, d_open_kbankin FROM tm_closing_kas_bank", FALSE);
			if ($tableclosing->num_rows()>0) {
				$row = $tableclosing->row();
				$data['d_closing_kbank']   = date('Ymd', strtotime($row->d_closing_kbank));
				$data['d_open_kbank']      = date('Ymd', strtotime($row->d_open_kbank));
				$data['d_closing_kbankin'] = date('Ymd', strtotime($row->d_closing_kbankin));
				$data['d_open_kbankin']    = date('Ymd', strtotime($row->d_open_kbankin));
			}else{
				$data['d_closing_kbank']   = date('Ym').'04';
				$data['d_open_kbank'] 	   = date('Ym').'01';
				$data['d_closing_kbankin'] = date('Ym').'04';
				$data['d_open_kbankin']	   = date('Ym').'01';
			}
			$this->load->view('list-akt-bk/vmainform', $data);
		}else{
			$this->load->view('index.php');
		}
	}
	function cari()
	{
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') &&  
			$this->session->userdata('isession')!=0) 
		{
			$cari		    = strtoupper($this->input->post('cari'));
			$dfrom		  = $this->input->post('dfrom');
			$dto		    = $this->input->post('dto');
			$iarea		  = $this->input->post('iarea');
			$icoabank		= $this->input->post('icoabank');
			$ibank  		= $this->input->post('ibank');
			$lepel		  = $this->session->userdata('i_area');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($icoabank=='') $icoabank	= $this->uri->segment(7);
			if($ibank=='') $ibank	= $this->uri->segment(8);
			$config['base_url'] = base_url().'index.php/list-akt-bk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$icoabank.'/'.$ibank.'/index/';
			$query = $this->db->query(" select	distinct on (i_kbank) vc, f_kbank_cancel, i_kbank, i_area, d_bank, i_coa, e_description, v_bank, v_sisa, i_periode, f_debet, f_posting, 
		                    i_cek, e_area_name, i_bank, i_coa_bank, e_nama_bank
	                    	from(
                  			select x.i_rvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, 
                  			a.v_sisa, a.i_periode, a.f_debet, a.f_posting, 
                  			a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_nama_bank
                  			from tr_area e, tm_kbank a
                  			left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' 
                  			and b.i_coa_bank='$icoabank')
                  			left join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                  			left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv 
                  			and x.i_coa_bank='$icoabank')
                  			left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                  			where (upper(a.i_kbank) like '%$cari%' or upper(c.i_rv) like '%$cari%') and a.i_area=e.i_area
                  			and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
                  			and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_debet = 'f'
                  			union
                    		select x.i_pvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, 
                    		a.v_sisa, a.i_periode, a.f_debet, a.f_posting, 
                    		a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_nama_bank
                    		from tr_area e, tm_kbank a
                    		left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' 
                    		and b.i_coa_bank='$icoabank')
                    		left join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                    		left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv 
                    		and x.i_coa_bank='$icoabank')
                    		left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                    		where (upper(a.i_kbank) like '%$cari%' or upper(c.i_pv) like '%$cari%') and a.i_area=e.i_area 
                    		and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
                    		and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_debet = 't'
                  			) as a
                        ORDER BY i_kbank",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listbk');
			$this->load->model('list-akt-bk/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['icoabank']		= $icoabank;
			$data['ibank']		= $ibank;
			$data['lepel']		= $lepel;
			$data['isi']		= $this->mmaster->bacaperiode($icoabank,$dfrom,$dto,$ibank,$config['per_page'],$this->uri->segment(9),$cari);
			/*----------  Ambil Closing Dari Table  ----------*/			
			$tableclosing = $this->db->query("SELECT d_closing_kbank, d_closing_kbankin, d_open_kbank, d_open_kbankin FROM tm_closing_kas_bank", FALSE);
			if ($tableclosing->num_rows()>0) {
				$row = $tableclosing->row();
				$data['d_closing_kbank']   = date('Ymd', strtotime($row->d_closing_kbank));
				$data['d_open_kbank']      = date('Ymd', strtotime($row->d_open_kbank));
				$data['d_closing_kbankin'] = date('Ymd', strtotime($row->d_closing_kbankin));
				$data['d_open_kbankin']    = date('Ymd', strtotime($row->d_open_kbankin));
			}else{
				$data['d_closing_kbank']   = date('Ym').'04';
				$data['d_open_kbank'] 	   = date('Ym').'01';
				$data['d_closing_kbankin'] = date('Ym').'04';
				$data['d_open_kbankin']	   = date('Ym').'01';
			}
			$this->load->view('list-akt-bk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bank()
	{
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') &&  
			$this->session->userdata('isession')!=0) 
		{
			$config['base_url'] = base_url().'index.php/list-akt-bk/cform/bank/index/';
			$query = $this->db->query("select * from tr_bank",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination_ori->initialize($config);
			$this->load->model('list-akt-bk/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5));
			$this->load->view('list-akt-bk/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caribank()
	{
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') &&  
			$this->session->userdata('isession')!=0) 
		{
			$config['base_url'] = base_url().'index.php/list-akt-bk/cform/bank/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("select * from tr_bank where (upper(e_nama_bank) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('list-akt-bk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->caribank($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('list-akt-bk/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
