<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		$is_logged_in =$this->session->userdata('is_logged_in');
	if(!isset($is_logged_in) || $is_logged_in != true){	
		redirect('loginform');
		}
			$data['page_title'] = $this->lang->line('neracasaldo');
			$data['periode']	= '';
			$data['isi']	= 'akt-neracasaldo/vmainform';
			$this->load->view('template', $data);
		
	}
	function view()
	{$is_logged_in =$this->session->userdata('is_logged_in');
	if(!isset($is_logged_in) || $is_logged_in != true){	
		redirect('loginform');
		}
			$periode	= $this->uri->segment(4);
			if(
				($periode!='')
			  )
			{
				$this->load->model('akt-neracasaldo/mmaster');
				$data['page_title'] = $this->lang->line('neracasaldo');
				$tmp 	= explode("-", $periode);
				$tahun	= $tmp[2];
				$bulan	= $tmp[1];
				$tanggal= $tmp[0];
				$periode=$tahun.$bulan;
				$kiwari		= $tahun."/".$bulan."/01";
				$namabulan	= $this->mmaster->NamaBulan($bulan);
				$data['periode']	= $periode;
				$dfrom				= $tahun."-".$bulan."-01";
				$data['dfrom']		= $dfrom;
				$dtos	= $tahun."/".$bulan."/".$tanggal;
				$dtos	=$this->mmaster->dateAdd("d",1,$dtos);
				$tmp 	= explode("-", $dtos);
				$th		= $tmp[0];
				$bl		= $tmp[1];
				$dt 	= $tmp[2];
				$dto				= $th."-".$bl."-".$dt;
				$data['dto']		= $th."-".$bl."-".$dt;
				$data['tanggal']	= $tanggal;
				$data['namabulan']	= $namabulan;
				$data['tahun']		= $tahun;
#				$data['isi']		= $this->mmaster->bacaperiode($periode);
#				$data['isi']		= $this->mmaster->bacanilai($dfrom,$dto);
				$data['query']		= $this->mmaster->bacanilai($periode);
				$data['periode']=$periode;
	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
			$data['isi']	= 'akt-neracasaldo/vmainform';
			$this->load->view('template', $data);
				
			}
	
	}
}
?>
