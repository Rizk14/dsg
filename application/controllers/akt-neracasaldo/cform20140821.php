<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu131')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('neracasaldo');
			$data['periode']	= '';
			$this->load->view('akt-neracasaldo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu131')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$periode	= $this->uri->segment(4);
			if(
				($periode!='')
			  )
			{
				$this->load->model('akt-neracasaldo/mmaster');
				$data['page_title'] = $this->lang->line('neracasaldo');
				$tmp 	= explode("-", $periode);
				$tahun	= $tmp[2];
				$bulan	= $tmp[1];
				$tanggal= $tmp[0];
				$periode=$tahun.$bulan;
				$kiwari		= $tahun."/".$bulan."/01";
				$namabulan	= $this->mmaster->NamaBulan($bulan);
				$data['periode']	= $periode;
				$dfrom				= $tahun."-".$bulan."-01";
				$data['dfrom']		= $dfrom;
				$dtos	= $tahun."/".$bulan."/".$tanggal;
				$dtos	=$this->mmaster->dateAdd("d",1,$dtos);
				$tmp 	= explode("-", $dtos);
				$th		= $tmp[0];
				$bl		= $tmp[1];
				$dt 	= $tmp[2];
				$dto				= $th."-".$bl."-".$dt;
				$data['dto']		= $th."-".$bl."-".$dt;
				$data['tanggal']	= $tanggal;
				$data['namabulan']	= $namabulan;
				$data['tahun']		= $tahun;
//				$data['isi']		= $this->mmaster->bacaperiode($periode);
				$data['isi']		= $this->mmaster->bacanilai($dfrom,$dto);

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Buka Neraca Saldo periode:'.$periode;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$this->load->view('akt-neracasaldo/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
