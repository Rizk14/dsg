<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmmasuk-hsl-cutting/mmaster');
  }

  function index(){	
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['cari'] = '';
	$data['bonm_detail'] = $this->mmaster->get_detail_bonm();
	$th_now	= date("Y");
	$data['msg'] = '';
	$data['isi'] = 'bonmmasuk-hsl-cutting/vmainform';
	$this->load->view('template',$data);
  }
  
  function edit(){
	$id_apply_stok 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_bonm($id_apply_stok);
	$data['msg'] = '';
	$data['id_apply_stok'] = $id_apply_stok;

	$data['isi'] = 'bonmmasuk/veditform';
	$this->load->view('template',$data);

  }
    
  function show_popup_sj(){
	$id_gudang 	= $this->uri->segment(4);
	$csupplier 	= $this->uri->segment(5);
	if ($csupplier == '')
		$csupplier 	= $this->input->post('csupplier', TRUE);  
	if ($id_gudang == '')
		$id_gudang 	= $this->input->post('id_gudang', TRUE);  
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jum_total = $this->mmaster->get_sjtanpalimit($csupplier, $id_gudang, $keywordcari);
							$config['base_url'] = base_url()."index.php/bonmmasuk/cform/show_popup_sj/";
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_sj($config['per_page'],$this->uri->segment(5), $csupplier, $id_gudang, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$data['csupplier'] = $csupplier;
	$data['id_gudang'] = $id_gudang;

	$this->load->view('bonmmasuk/vpopupsj',$data);

  }

  function submit() {		
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	  $tgl = date("Y-m-d");
			$no_bonm	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm	= $this->input->post('tgl_bonm', TRUE);
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			
			$jumheader	= $this->input->post('jumheader', TRUE);
			$jumdetail	= $this->input->post('jumdetail', TRUE);
			
			$list_detail 	= $this->input->post('list_detail', TRUE);
			 
			$id_detail = explode(",", $list_detail);
			
			$ambil=0;
			foreach($id_detail as $row1) {
				
				if ($row1 != '') {
					$query1	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting_detail 
										WHERE id='$row1' AND status_stok='f' ");
					if($query1->num_rows()>0) {
						
						$hasilrow = $query1->row();
						$id_apply_stok	= $hasilrow->id_apply_stok;
						$kode_brg_jadi	= $hasilrow->kode_brg_jadi;
						$kode_brg		= $hasilrow->kode_brg;
						$qty			= $hasilrow->qty;
						$id_realisasi_cutting_detail	= $hasilrow->id_realisasi_cutting_detail;
						
						$this->db->query(" UPDATE tm_apply_stok_hasil_cutting_detail SET status_stok='t', no_bonm = '$no_bonm',
										tgl_bonm = '$tgl_bonm', tgl_update = '$tgl'
										WHERE id='$row1' ");

						$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting_detail WHERE status_stok='f' 
												AND id_apply_stok='$id_apply_stok' ");
						if ($query2->num_rows()==0){
							$this->db->query(" UPDATE tm_apply_stok_hasil_cutting SET status_stok='t' WHERE id='$id_apply_stok' ");
						}
						
						$query3	= $this->db->query(" SELECT a.is_dacron, b.* FROM tt_realisasi_cutting a, 
											tt_realisasi_cutting_detail b WHERE a.id = b.id_realisasi_cutting AND
											b.id='$id_realisasi_cutting_detail' ");
						
						if($query3->num_rows()>0){
							$ridrealisasi	= $query3->row();
							$id_realisasi	= $ridrealisasi->id_realisasi_cutting;
							//$qty_realisasi	= $ridrealisasi->qty_realisasi;
							$jum_gelar		= $ridrealisasi->jum_gelar;
							$diprint		= $ridrealisasi->diprint;
							$is_dacron		= $ridrealisasi->is_dacron;
							
							// no longer used anymore
							/*$query4	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_hasil_cutting WHERE id='$id_apply_stok' ");
							$row2	= $query4->row();
							
							$no_bonm	= $row2->no_bonm; */
							
							$this->db->query(" UPDATE tt_realisasi_cutting_detail SET status_apply_stok='t' WHERE id='$id_realisasi_cutting_detail' ");
										
							$query3	= $this->db->query(" SELECT id FROM tt_realisasi_cutting WHERE status_apply_stok='f' 
													AND id='$id_realisasi' ");
							if ($query3->num_rows()==0){
								$this->db->query(" UPDATE tt_realisasi_cutting SET status_apply_stok='t' WHERE id='$id_realisasi' ");
							}
							
							$qstokhslc = $this->db->query(" SELECT * FROM tm_stok_hasil_cutting WHERE 
												kode_brg_jadi='$kode_brg_jadi' AND kode_brg='$kode_brg' AND diprint = '$diprint'
												AND is_dacron = '$is_dacron' ");
							
							if($qstokhslc->num_rows()>0){
								$rstokhslc = $qstokhslc->row();
								$id_stok_cutting= $rstokhslc->id;
								$total_gelar	= $rstokhslc->total_gelar;
								//$jmlstokhslc 	= ($rstokhslc->stok + $qty_realisasi);
								$jmlstokhslc 	= ($rstokhslc->stok + $qty);
								$jmlgelarhslc	= ($total_gelar + $jum_gelar);
								
								$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok='$jmlstokhslc', total_gelar='$jmlgelarhslc' 
								WHERE id='$id_stok_cutting' ");	
								
								/*$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg_jadi,kode_brg,no_bukti,masuk,
												saldo,tgl_input, diprint, is_dacron) VALUES('$kode_brg_jadi','$kode_brg','$no_bonm',
												'$qty_realisasi','$jmlstokhslc','$tgl_bonm', '$diprint', '$is_dacron') "); */
							}else{
								$this->db->query(" INSERT INTO tm_stok_hasil_cutting(kode_brg_jadi,kode_brg,stok,total_gelar,tgl_update_stok, diprint, is_dacron) 
												VALUES('$kode_brg_jadi','$kode_brg','$qty','$jum_gelar','$tgl', '$diprint', '$is_dacron') ");
								/*$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg_jadi,kode_brg,no_bukti,masuk,saldo,tgl_input, diprint, is_dacron) 
												VALUES('$kode_brg_jadi','$kode_brg','$no_bonm','$qty_realisasi','$jum_gelar','$tgl_bonm', '$diprint', '$is_dacron' ) "); */
							}
						}						
					}			
					
				}
			} 
			
			redirect('bonmmasuk-hsl-cutting/cform/view'); //

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $data['isi'] = 'bonmmasuk-hsl-cutting/vformview';
    $keywordcari = "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk-hsl-cutting/cform/view/index/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$config['cur_page'], $keywordcari);				
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari=='') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari=='')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk-hsl-cutting/cform/cari/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'], $config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'bonmmasuk-hsl-cutting/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function batal(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $no_bonm 	= $this->uri->segment(4);
    
    $this->mmaster->delete($no_bonm);
    
    /*if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "bonmmasuk/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "bonmmasuk/cform/cari/".$cgudang."/".$carinya."/".$cur_page;
	
	/redirect($url_redirectnya); */
	redirect('bonmmasuk/cform/view');
  }
  
  // ===================================================== 15-07-2013 =====================================================
  function addbonm(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	// 25-09-2015, dikomen aja
	/*$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmmasukcutting ORDER BY no_bonm DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==9) {
				$nobonm = substr($no_bonm, 0, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm;
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			}
	*/
	$data['isi'] = 'bonmmasuk-hsl-cutting/vform1bonm';
	$data['msg'] = '';
	//$data['no_bonm'] = $nomorbonm;
	$this->load->view('template',$data);
  }
  
  function show_popup_brg_jadi(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$posisi		= $this->input->post('posisi', TRUE);   
	$keywordcari	= $this->input->post('cari', TRUE);  
	
	if ($posisi=='' && $keywordcari=='') {
		$posisi 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}	

	if ($keywordcari=='')
		$keywordcari = "all";
			
	$qjum_total = $this->mmaster->getbarangjaditanpalimit($keywordcari);

	$config['base_url'] 	= base_url()."index.php/bonmmasuk-hsl-cutting/cform/show_popup_brg_jadi/".$posisi."/".$keywordcari."/";
	$config['total_rows'] 	= count($qjum_total); 
	$config['per_page'] 	= 10;
	$config['first_link'] 	= 'Awal';
	$config['last_link'] 	= 'Akhir';
	$config['next_link'] 	= 'Selanjutnya';
	$config['prev_link'] 	= 'Sebelumnya';
	$config['cur_page']		= $this->uri->segment(6,0);
	$this->pagination->initialize($config);
	
	$data['query']		= $this->mmaster->getbarangjadi($config['per_page'],$config['cur_page'], $keywordcari);	
	$data['jum_total']	= count($qjum_total);
	$data['posisi']		= $posisi;
	$data['jumdata'] = $posisi-1;

	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
				
	$data['startnya']	= $config['cur_page'];
	
	$this->load->view('bonmmasuk-hsl-cutting/vpopupbrgjadi',$data);  
  }
  
  function viewbonm(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$jenis 	= $this->input->post('jenis', TRUE);
	$sumber 	= $this->input->post('sumber', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($jenis == '')
		$jenis = $this->uri->segment(6);
	if ($sumber == '')
		$sumber = $this->uri->segment(7);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(8);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(9);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(10);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($jenis == '')
		$jenis = "0";
	if ($sumber == '')
		$sumber = "0";
	if ($filterbrg == '')
		$filterbrg = 'n';
	if ($caribrg == '')
		$caribrg = "all";
	
	$jum_total = $this->mmaster->getAllbonmtanpalimit($keywordcari, $date_from, $date_to, $jenis, $sumber, $caribrg, $filterbrg);
    
				$config['base_url'] = base_url().'index.php/bonmmasuk-hsl-cutting/cform/viewbonm/'.$date_from.'/'.$date_to.'/'.$jenis.'/'.$sumber.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
				$config['total_rows'] = count($jum_total); 
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(11);
				$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbonm($config['per_page'],$this->uri->segment(11), $keywordcari, $date_from, $date_to, $jenis, $sumber, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['isi'] = 'bonmmasuk-hsl-cutting/vformviewbonm';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$data['jenis'] = $jenis;
	$data['sumber'] = $sumber;
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	$this->load->view('template',$data);
  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$is_quilting 	= $this->uri->segment(5);
	$posisi 	= $this->uri->segment(6);
	
	if ($is_quilting == '' || $posisi == '') {
		$is_quilting 	= $this->input->post('is_quilting', TRUE);  
		$posisi 	= $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || ( $is_quilting == '' && $posisi == '')) {
			$is_quilting 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $is_quilting);
		
				$config['base_url'] = base_url()."index.php/bonmmasuk-hsl-cutting/cform/show_popup_brg/".$is_quilting."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $is_quilting);
	$data['jum_total'] = count($qjum_total);
	$data['is_quilting'] = $is_quilting;
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$this->load->view('bonmmasuk-hsl-cutting/vpopupbrg',$data);
  }
	
	// modif 25-09-2015
  function submitbonm(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
			//$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);  
			$jenis 	= $this->input->post('jenis', TRUE);  
			$sumber 	= $this->input->post('sumber', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data($no_bonm_manual, $thn1);
			if (count($cek_data) > 0) {
				$data['isi'] = 'bonmmasuk-hsl-cutting/vform1bonm';
				$data['msg'] = "Data no bon M ".$no_bonm_manual." utk tahun $thn1 sudah ada..!";
				
				/*$th_now	= date("Y");
				$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmmasukcutting ORDER BY no_bonm DESC LIMIT 1 ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) 
					$no_bonm	= $hasilrow->no_bonm;
				else
					$no_bonm = '';
				if(strlen($no_bonm)==9) {
					$nobonm = substr($no_bonm, 0, 9);
					$n_bonm	= (substr($nobonm,4,5))+1;
					$th_bonm	= substr($nobonm,0,4);
					if($th_now==$th_bonm) {
							$jml_n_bonm	= $n_bonm;
							switch(strlen($jml_n_bonm)) {
								case "1": $kodebonm	= "0000".$jml_n_bonm;
								break;
								case "2": $kodebonm	= "000".$jml_n_bonm;
								break;	
								case "3": $kodebonm	= "00".$jml_n_bonm;
								break;
								case "4": $kodebonm	= "0".$jml_n_bonm;
								break;
								case "5": $kodebonm	= $jml_n_bonm;
								break;	
							}
							$nomorbonm = $th_now.$kodebonm;
					}
					else {
						$nomorbonm = $th_now."00001";
					}
				}
				else {
					$nomorbonm	= $th_now."00001";
				}
				
				$data['no_bonm'] = $nomorbonm; */
				$this->load->view('template',$data);
			}
			else {
				// 24-11-2014
				$tgl = date("Y-m-d H:i:s");
				$uid_update_by = $this->session->userdata('uid');
				
				// insert di tm_bonmmasukcutting
				$data_header = array(
				  //'no_bonm'=>$no_bonm,
				  'no_manual'=>$no_bonm_manual,
				  'tgl_bonm'=>$tgl_bonm,
				  'jenis_masuk'=>$jenis,
				  'sumber'=>$sumber,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'keterangan'=>$ket,
				  'uid_update_by'=>$uid_update_by
				);
				$this->db->insert('tm_bonmmasukcutting',$data_header);
				
				// ambil data terakhir di tabel tm_bonmmasukcutting
				$query2	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query2->row();
				$id_bonm	= $hasilrow->id;
				
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->savebonm($id_bonm, $jenis, $ket, 
					$this->input->post('id_brg_wip_'.$i, TRUE), $this->input->post('nama_brg_wip_'.$i, TRUE), 
					//$this->input->post('kode_bhn_'.$i, TRUE),$this->input->post('qty_bhn_'.$i, TRUE),
					$this->input->post('id_warna_'.$i, TRUE), $this->input->post('qty_warna_'.$i, TRUE),
					$this->input->post('ket_detail_'.$i, TRUE) );						
				}
				redirect('bonmmasuk-hsl-cutting/cform/viewbonm');
			}
  }
  
  // modif 25-09-2015
  function deletebonm(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$tgl_awal 	= $this->uri->segment(6);
	$tgl_akhir 	= $this->uri->segment(7);
	$jenis 	= $this->uri->segment(8);
	$sumber 	= $this->uri->segment(9);
	$carinya 	= $this->uri->segment(10);
	$caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
    
    $this->mmaster->deletebonm($id);
    
    if ($carinya == '') $carinya = "all";
	//$url_redirectnya = "bonmmasuk-hsl-cutting/cform/viewbonm/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
	$url_redirectnya = "bonmmasuk-hsl-cutting/cform/viewbonm/".$tgl_awal."/".$tgl_akhir."/".$cjenis."/".$csumber."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
	redirect($url_redirectnya);
  }
  
  // 05-03-2014, modif 25-09-2015
  function editbonm(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_bonm 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$tgl_awal 	= $this->uri->segment(6);
	$tgl_akhir 	= $this->uri->segment(7);
	$jenis 	= $this->uri->segment(8);
	$sumber 	= $this->uri->segment(9);
	$carinya 	= $this->uri->segment(10);
	$caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
	$data['cur_page'] = $cur_page;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['jenis'] = $jenis;
	$data['sumber'] = $sumber;
	$data['carinya'] = $carinya;
	$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['query'] = $this->mmaster->get_bonmcutting($id_bonm); 
	$data['msg'] = '';
	$data['isi'] = 'bonmmasuk-hsl-cutting/veditformbonm';
	$this->load->view('template',$data);

  }
  
  // modif 25-09-2015
  function updatedatabonm() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_bonm 	= $this->input->post('id_bonm', TRUE);
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$jenis 	= $this->input->post('jenis', TRUE);  
			$sumber 	= $this->input->post('sumber', TRUE);  
			
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$cjenis = $this->input->post('cjenis', TRUE);
			$csumber = $this->input->post('csumber', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
						
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');

			//update headernya
			$this->db->query(" UPDATE tm_bonmmasukcutting SET tgl_bonm = '$tgl_bonm', jenis_masuk = '$jenis', 
							sumber='$sumber', tgl_update='$tgl',
							no_manual='$no_bonm_manual', uid_update_by = '$uid_update_by',
							keterangan = '$ket' where id= '$id_bonm' ");
							
				//reset stok, dan hapus dulu detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasukcutting_detail WHERE id_bonmmasukcutting = '$id_bonm' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$qty = $row2->qty;
						// ============ update stok =====================
				
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting 
												WHERE id_brg_wip = '$row2->id_brg_wip' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$id_stok	= $hasilrow->id;
						}
						$new_stok = $stok_lama-$qty; // berkurang stok karena reset dari bon M masuk
						
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where id_brg_wip= '$row2->id_brg_wip' ");
						// ==============================================
						
						// 2. reset stok per warna dari tabel tm_bonmmasukcutting_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_bonmmasukcutting_detail_warna 
												WHERE id_bonmmasukcutting_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND id_warna='$rowwarna->id_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama-$rowwarna->qty; // berkurang stok karena reset dari bon M masuk
										
								$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND id_warna = '$rowwarna->id_warna' ");
							}
						} // end if detail warna
						// hapus data di tm_bonmmasukcutting_detail_warna
						$this->db->query(" DELETE FROM tm_bonmmasukcutting_detail_warna WHERE id_bonmmasukcutting_detail='".$row2->id."' ");
						//---------------------------------------------------------------------------------------------------
												
					} // end foreach
				} // end reset stok
				
				$this->db->delete('tm_bonmmasukcutting_detail', array('id_bonmmasukcutting' => $id_bonm));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$nama_brg_wip = $this->input->post('nama_brg_wip_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
						
						$temp_qty = $this->input->post('temp_qty_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$qty_warna = $this->input->post('qty_warna_'.$i, TRUE);
						
						// 05-03-2014 --------------------------
						//-------------- hitung total qty dari detail tiap2 warna -------------------
						$qtytotal = 0;
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$qty_warna[$xx] = trim($qty_warna[$xx]);
							$qtytotal+= $qty_warna[$xx];
						} // end for
						// ---------------------------------------------------------------------
						
						// 1. stok total
						// ============================= update stok hasil cutting ======================================
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$id_brg_wip' ");
							if ($query3->num_rows() == 0){
								$id_stok = 0;
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok	= $hasilrow->id;
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qtytotal;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
								$data_stok = array(
									'id_brg_wip'=>$id_brg_wip,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting', $data_stok);
								
								// ambil id_stok utk dipake di stok warna
								$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
								if($sqlxx->num_rows() > 0) {
									$hasilxx	= $sqlxx->row();
									$id_stok	= $hasilxx->id;
								}else{
									$id_stok	= 1;
								}
							}
							else {
								$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg_wip= '$id_brg_wip' ");
							}
					
					// jika semua data tdk kosong, insert ke tm_bonmmasukcutting_detail
					$data_detail = array(
						'id_bonmmasukcutting'=>$id_bonm,
						'id_brg_wip'=>$id_brg_wip,
						'nama_brg_wip'=>$nama_brg_wip,
						'qty'=>$qtytotal,
						'keterangan'=>$ket_detail
					);
					$this->db->insert('tm_bonmmasukcutting_detail',$data_detail);
					// ================ end insert item detail ===========
					
					// ambil id detail tm_bonmmasukcutting_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;
					
					// ---------------- stok per warna ------------------------------
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$qty_warna[$xx] = trim($qty_warna[$xx]); 
								
						$seq_warna	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}

						$tm_bonmmasukcutting_detail_warna	= array(
							 'id'=>$idbaru,
							 'id_bonmmasukcutting_detail'=>$iddetail,
							 'id_warna'=>$id_warna[$xx],
							 'qty'=>$qty_warna[$xx]
						);
						$this->db->insert('tm_bonmmasukcutting_detail_warna',$tm_bonmmasukcutting_detail_warna);
						
						// ========================= stok per warna ===============================================
						//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE id_warna = '".$id_warna[$xx]."'
								AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() == 0){
							$stok_warna_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_warna_lama	= $hasilrow->stok;
						}
						$new_stok_warna = $stok_warna_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_cutting'=>$id_stok,
								'id_warna'=>$id_warna[$xx],
								'stok'=>$new_stok_warna,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
							where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
						}
						
					} // end for
					// ----------------------------------------------
				} // end perulangan

			if ($carinya == '') $carinya = "all";
			$url_redirectnya = "bonmmasuk-hsl-cutting/cform/viewbonm/".$tgl_awal."/".$tgl_akhir."/".$cjenis."/".$csumber."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
			
			redirect($url_redirectnya);
				
  }
  
  // ===================================================== END =====================================================
  
  // 10-02-2014
  /*function additemwarna(){

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna_brg_jadi'=> $rowxx->id,
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('bonmmasuk-hsl-cutting/vlistwarna', $data); 
		return true;
  } */
  
  /*function additembhnbaku(){

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 bhn baku berdasarkan kode brg jadi dari tm_material_brg_jadi
		$queryxx = $this->db->query(" SELECT a.*, b.nama as nama_warna FROM tm_material_brg_jadi a, tm_warna b 
								WHERE a.kode_warna=b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				// bhn baku/quilting
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$query31	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
							if ($query31->num_rows() > 0){
								$hasilrow31 = $query31->row();
								$nama_brg	= $hasilrow31->nama_brg;
								//$satuan	= $hasilrow31->nama_satuan;
							}
							else {
								$nama_brg = '';
								//$satuan = '';
							}
						}
				
				$detailbhnbaku[] = array(	'id_material_brg_jadi'=> $rowxx->id,
										'kode_brg'=> $rowxx->kode_brg,
										'nama_brg'=> $nama_brg,
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama_warna
									);
			}
		}
		else
			$detailbhnbaku = '';
		
		$data['detailbhnbaku'] = $detailbhnbaku;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('bonmmasuk-hsl-cutting/vlistbhnbaku', $data); 
		return true;
  } */
  
  // 22-11-2014, modif 25-09-2015
  function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil kode, nama
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = '';
			$nama_brg_wip = '';
		}
		
		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('bonmmasuk-hsl-cutting/vinfobrgwip', $data); 
		return true;
  }
  
  // modif 25-09-2015
  function additemwarna(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
						INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' ORDER BY b.nama");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('bonmmasuk-hsl-cutting/vlistwarna', $data); 
		return true;
  }
  
  function additembhnbaku(){

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		//$jenis 	= $this->input->post('jenis', TRUE);
				
		// query ambil data2 bhn baku berdasarkan kode brg jadi dari tm_material_brg_jadi
		$queryxx = $this->db->query(" SELECT a.*, b.nama as nama_warna FROM tm_material_brg_jadi a LEFT JOIN tm_warna b
								ON a.kode_warna=b.kode
								WHERE  a.kode_brg_jadi = '".$kode_brg_jadi."' AND a.kode_brg <> '00' ");
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				// bhn baku/quilting
				if ($rowxx->kode_brg != '00') {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$query31	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
							if ($query31->num_rows() > 0){
								$hasilrow31 = $query31->row();
								$nama_brg	= $hasilrow31->nama_brg;
								//$satuan	= $hasilrow31->nama_satuan;
							}
							else {
								$nama_brg = '';
								//$satuan = '';
							}
						}
				}
				else
					$nama_brg = $rowxx->nama_brg_manual;
				
				if ($rowxx->kode_warna == '00')
					$nama_warna = "Tidak Ada";
				else
					$nama_warna = $rowxx->nama_warna;
				$detailbhnbaku[] = array(	'id_material_brg_jadi'=> $rowxx->id,
										'kode_brg'=> $rowxx->kode_brg,
										'nama_brg'=> $nama_brg,
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $nama_warna
										//'qty_perpcs_brgjadi'=> $rowxx->qty_perpcs_brgjadi,
										//'rumus_forecast'=> $rowxx->rumus_forecast
									);
			}
		}
		else
			$detailbhnbaku = '';
		
		$data['detailbhnbaku'] = $detailbhnbaku;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		//if ($jenis == 1)
			$this->load->view('bonmmasuk-hsl-cutting/vlistbhnbaku', $data); 
		/*else if ($jenis == 2)
			$this->load->view('produksi/vlistforecast', $data); 
		else if ($jenis == 3)
			$this->load->view('produksi/vlistket', $data); 
		else
			$this->load->view('produksi/vlistforecast2', $data); */
		return true;
  }
  
}
