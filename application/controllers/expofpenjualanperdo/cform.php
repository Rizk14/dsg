<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expofpenjualanperdo/mclass');
	}
	
	function index() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_penjualanperdo']			= $this->lang->line('page_title_penjualanperdo');
			$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
			$data['list_penjperdo_kd_brg']				= $this->lang->line('list_penjperdo_kd_brg');
			$data['list_fpenjperdo_tgl_mulai_faktur']	= $this->lang->line('list_fpenjperdo_tgl_mulai_faktur');
			$data['list_fpenjperdo_no_faktur']			= $this->lang->line('list_fpenjperdo_no_faktur');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			/*** $this->load->model('expofpenjualanperdo/mclass'); ***/
			$data['isi']	= 'expofpenjualanperdo/vmainform';	
			$this->load->view('template',$data);	
		
		
	}
	
	function carilistpenjualanperdo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanperdo']			= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		
		$data['list_fpenjperdo_tgl_mulai_faktur']	= $this->lang->line('list_fpenjperdo_tgl_mulai_faktur');
		$data['list_fpenjperdo_no_faktur']		= $this->lang->line('list_fpenjperdo_no_faktur');
		$data['list_fpenjperdo_no_pajak']		= $this->lang->line('list_fpenjperdo_no_pajak');
		$data['list_fpenjperdo_nm_pelanggan']	= $this->lang->line('list_fpenjperdo_nm_pelanggan');
		$data['list_fpenjperdo_npwp']			= $this->lang->line('list_fpenjperdo_npwp');
		$data['list_fpenjperdo_tgl_faktur']		= $this->lang->line('list_fpenjperdo_tgl_faktur');
		$data['list_fpenjperdo_tgl_bts_kirim']	= $this->lang->line('list_fpenjperdo_tgl_bts_kirim');
		$data['list_fpenjperdo_discount']		= $this->lang->line('list_fpenjperdo_discount');
		$data['list_fpenjperdo_total_faktur']	= $this->lang->line('list_fpenjperdo_total_faktur');
		$data['list_fpenjperdo_grand_total']	= $this->lang->line('list_fpenjperdo_grand_total');
		
		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";		
		$data['limages']	= base_url();
		
		$no_faktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->uri->segment(4);
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->uri->segment(5);
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->uri->segment(6);

		$data['tgldomulai']	= empty($d_do_first)?"":$d_do_first;
		$data['tgldoakhir']	= empty($d_do_last)?"":$d_do_last;
		$data['nofaktur']	= empty($no_faktur)?"":$no_faktur;
		
		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:date("Y-m-d");
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:date("Y-m-d");
		
		$nofak	= empty($no_faktur)?'0':$no_faktur;
		$dfirst	= empty($d_do_first)?'0':$n_d_do_first;
		$dlast	= empty($d_do_last)?'0':$n_d_do_last;

		$data['tfirst']	= $dfirst;
		$data['tlast']	= $dlast;
		$data['nofak']	= $nofak;
				
		/*** $this->load->model('expofpenjualanperdo/mclass'); ***/

		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expofpenjualanperdo/cform/carilistpenjualanperdoperpages/'.$nofak.'/'.$dfirst.'/'.$dlast;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
				
		$data['query']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);
			$data['isi']	= 'expofpenjualanperdo/vlistform';	
			$this->load->view('template',$data);
	
	}
	
	function carilistpenjualanperdoperpages() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanperdo']			= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
	
		$data['list_fpenjperdo_tgl_mulai_faktur']	= $this->lang->line('list_fpenjperdo_tgl_mulai_faktur');
		$data['list_fpenjperdo_no_faktur']		= $this->lang->line('list_fpenjperdo_no_faktur');
		$data['list_fpenjperdo_no_pajak']		= $this->lang->line('list_fpenjperdo_no_pajak');
		$data['list_fpenjperdo_nm_pelanggan']	= $this->lang->line('list_fpenjperdo_nm_pelanggan');
		$data['list_fpenjperdo_npwp']			= $this->lang->line('list_fpenjperdo_npwp');
		$data['list_fpenjperdo_tgl_faktur']		= $this->lang->line('list_fpenjperdo_tgl_faktur');
		$data['list_fpenjperdo_tgl_bts_kirim']	= $this->lang->line('list_fpenjperdo_tgl_bts_kirim');
		$data['list_fpenjperdo_discount']		= $this->lang->line('list_fpenjperdo_discount');
		$data['list_fpenjperdo_total_faktur']	= $this->lang->line('list_fpenjperdo_total_faktur');
		$data['list_fpenjperdo_grand_total']	= $this->lang->line('list_fpenjperdo_grand_total');

		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";		
		$data['limages']	= base_url();
		
		$no_faktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->uri->segment(4); // Y-m-d
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->uri->segment(5);
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->uri->segment(6);

		$e_d_do_first	= explode("-",$d_do_first,strlen($d_do_first)); // Y-m-d
		$e_d_do_last	= explode("-",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:date("d/m/Y"); // d/m/Y
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:date("d/m/Y");
				
		$data['tgldomulai']	= empty($n_d_do_first)?"":$n_d_do_first;
		$data['tgldoakhir']	= empty($n_d_do_last)?"":$n_d_do_last;
		$data['nofaktur']	= empty($no_faktur) || $no_faktur=='0'?"":$no_faktur;

		$nofak	= empty($no_faktur) || $no_faktur=='0' ?'0':$no_faktur;
		$dfirst	= empty($d_do_first) || $d_do_first=='0' ?'0':$d_do_first;
		$dlast	= empty($d_do_last) || $d_do_last=='0' ?'0':$d_do_last;

		$data['tfirst']	= $dfirst;
		$data['tlast']	= $dlast;
		$data['nofak']	= $nofak;
				
		/*** $this->load->model('expofpenjualanperdo/mclass'); ***/

		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expofpenjualanperdo/cform/carilistpenjualanperdoperpages/'.$nofak.'/'.$dfirst.'/'.$dlast;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('expofpenjualanperdo/vlistform',$data);	
	}
	
	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expofpenjualanperdo/mclass'); ***/

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expofpenjualanperdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('expofpenjualanperdo/vlistformbrgjadi',$data);			
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expofpenjualanperdo/mclass'); ***/

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expofpenjualanperdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('expofpenjualanperdo/vlistformbrgjadi',$data);			
	}		

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');

		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		/*** $this->load->model('expofpenjualanperdo/mclass'); ***/

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row) {
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
	
	function gexportpenjualando() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
	$db2=$this->load->database('db_external', TRUE);
		$data['page_title_penjualanperdo']			= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
	
		$data['list_fpenjperdo_tgl_mulai_faktur']	= $this->lang->line('list_fpenjperdo_tgl_mulai_faktur');
		$data['list_fpenjperdo_no_faktur']		= $this->lang->line('list_fpenjperdo_no_faktur');
		$data['list_fpenjperdo_no_pajak']		= $this->lang->line('list_fpenjperdo_no_pajak');
		$data['list_fpenjperdo_nm_pelanggan']	= $this->lang->line('list_fpenjperdo_nm_pelanggan');
		$data['list_fpenjperdo_npwp']			= $this->lang->line('list_fpenjperdo_npwp');
		$data['list_fpenjperdo_tgl_faktur']		= $this->lang->line('list_fpenjperdo_tgl_faktur');
		$data['list_fpenjperdo_tgl_bts_kirim']	= $this->lang->line('list_fpenjperdo_tgl_bts_kirim');
		$data['list_fpenjperdo_discount']		= $this->lang->line('list_fpenjperdo_discount');
		$data['list_fpenjperdo_total_faktur']	= $this->lang->line('list_fpenjperdo_total_faktur');
		$data['list_fpenjperdo_grand_total']	= $this->lang->line('list_fpenjperdo_grand_total');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$nofaktur	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		
		$nofak	= empty($nofaktur) || $nofaktur=='0' ?'0':$nofaktur;
		$dfirst	= empty($tdofirst) || $tdofirst=='0' ?'0':$tdofirst;
		$dlast	= empty($tdolast) || $tdolast=='0' ?'0':$tdolast;
		
		$extdofirst	= $tdofirst!=0?explode("-",$tdofirst,strlen($tdofirst)):$tdofirst;
		$extdolast	=  $tdolast!=0?explode("-",$tdolast,strlen($tdolast)):$tdolast;
		
		$nwdofirst	= $extdofirst!=0?$extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0]:$extdofirst;
		$nwdolast	=$extdolast!=0? $extdolast[2]."/".$extdolast[1]."/".$extdolast[0]:$extdolast;
		
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= empty($nwdofirst)?"":$nwdofirst;
		$data['tgldoakhir']	= empty($nwdolast)?"":$nwdolast;
		$data['nofaktur']	= $nofaktur=='0'?"":$nofaktur;
				
		/*** $this->load->model('expofpenjualanperdo/mclass'); ***/
		
		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expofpenjualanperdo/cform/gexportpenjualandonext/'.$nofak.'/'.$dfirst.'/'.$dlast;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
				
		$data['query']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);		
	

		$ObjPHPExcel = new PHPExcel();
		
		$qexppenjualando	= $this->mclass->explistpenjualanperdo($nofak,$dfirst,$dlast);
		
		if($qexppenjualando->num_rows()>0) {
			
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Faktur Penjualan Berdasarkan DO")
				->setSubject("Laporan Faktur Penjualan")
				->setDescription("Laporan Faktur Penjualan")
				->setKeywords("Laporan Faktur Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();
			/*
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => false,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true

				)
			),
			'A1:T1'
			);
			*/
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4); //1
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8); //2
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8); //3
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8); //4
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8); //5
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6); //6
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15); //7
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20); //8
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15); //9
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12); //10
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12); //11
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12); //12
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(9); //13
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(9); //14
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8); //15
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8); //16
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8); //17
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10); //18
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10); //19
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10); //20
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'No');
			$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'Kode Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Kode Transaksi');
			$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'Kode Status');
			$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'Kode Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Flat VAT');
			$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'NPWP/ Nomor Paspor');
			$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'Nama Lawan Transaksi');
			$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'Nomor Faktur/ Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('J1', 'Jenis Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('K1', 'Nomor Faktur Pengganti/ Retur');
			$ObjPHPExcel->getActiveSheet()->setCellValue('L1', 'Jenis Dokumen Pengganti/ Retur');
			$ObjPHPExcel->getActiveSheet()->setCellValue('M1', 'Tanggal Faktur/ Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('N1', 'Tanggal SSP');
			$ObjPHPExcel->getActiveSheet()->setCellValue('O1', 'Masa Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('P1', 'Tahun Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('Q1', 'Pembetulan');
			$ObjPHPExcel->getActiveSheet()->setCellValue('R1', 'DPP');
			$ObjPHPExcel->getActiveSheet()->setCellValue('S1', 'PPN');
			$ObjPHPExcel->getActiveSheet()->setCellValue('T1', 'PPnBM');
			/*
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => false,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true

				)
			),
			'A2'
			);
			*/																													
			//if($qexppenjualando->num_rows()>0) {
				
				$j	= 2;
				$nomor	= 1;
				$jml=1;
				
				$bruto	= array();
				$dpp	= array();
				$ppn	= array();
				$total	= array();
				
				$nomorpajak	= array();
				$tglpajak	= array();
				$tahunpajak	= array();
				$masapajak	= array();
				$thpajak	= array();
				$tglfaktur	= array();
				
				foreach($qexppenjualando->result() as $row) {

					$qttfaktur	= $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_do_item_t WHERE i_faktur='$row->ifaktur' ");
					if($qttfaktur->num_rows()>0){
						$rttfaktur	= $qttfaktur->row();

						$bruto[$jml]	= $rttfaktur->totalfaktur;
						$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
						
						// Pembulantan ke atas
						$ppn[$jml]		= round(($dpp[$jml]*10)/100);
						$total[$jml]	= round($dpp[$jml]+$ppn[$jml]);														
					}
										
					$row->dfaktur	= explode("-",$row->dfaktur,strlen($row->dfaktur)); // Y-m-d
					$tfaktur	= $row->dfaktur[2];
					$bfaktur	= $row->dfaktur[1];
					$thfaktur	= $row->dfaktur[0];
					$tglfaktur[$jml]	= $tfaktur."/".$bfaktur."/".$thfaktur;
						
					$row->dduedate	= explode("-",$row->dduedate,strlen($row->dduedate)); // Y-m-d
					$tduedate	= $row->dduedate[2];
					$bduedate	= $row->dduedate[1];
					$thduedate	= $row->dduedate[0];
					$tglduedate	= $tduedate."/".$bduedate."/".$thduedate;

					$row->dpajak	= explode("-",$row->dpajak,strlen($row->dpajak)); // Y-m-d
					$tpajak	= $row->dpajak[2];
					$bpajak	= $row->dpajak[1];
					$thpajak[$jml]	= $row->dpajak[0];
					$tglpajak[$jml]	= $tpajak."/".$bpajak."/".$thpajak[$jml];
					$tahunpajak[$jml]	= substr($thpajak[$jml],2,2);
					$masapajak[$jml]	= str_repeat($row->dpajak[1],2);
					
					// Nomor Pajak 
					/***
					$nopajak		= $this->mclass->pajak($row->ifakturcode);
					if($nopajak->num_rows() > 0) {
						$row_pajak	= $nopajak->row();			
						$nomorpajak	= $row_pajak->ifakturpajak;			
						
						switch(strlen($nomorpajak)) {
							case "1": $nfaktur	= '0000000'.$nomorpajak; break;
							case "2": $nfaktur	= '000000'.$nomorpajak; break;
							case "3": $nfaktur	= '00000'.$nomorpajak; break;
							case "4": $nfaktur	= '0000'.$nomorpajak; break;
							case "5": $nfaktur	= '000'.$nomorpajak; break;
							case "6": $nfaktur	= '00'.$nomorpajak; break;
							case "7": $nfaktur	= '0'.$nomorpajak; break;
							default: $nfaktur	= $nomorpajak;
						}
						$nomorpajak[$jml]	= "010"."."."000".".".$tahunpajak.".".$nfaktur;			
					} else {
						$nomorpajak[$jml]	= "";
					}
					***/
					// End 0f Nomer Pajak
																	
					//$bruto[$jml]	= $row->totalfaktur;
					//$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
					
					// Pembulantan ke atas
					//$ppn[$jml]		= ceil(($dpp[$jml]*10)/100);
					//$total[$jml]	= ceil($dpp[$jml]+$ppn[$jml]);

					$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),
						'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
		
						)
					),
					'A'.$j.':'.'T'.$j
					);
			
					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, "A"); // $tglfaktur
					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, "2"); // $tglduedate
					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, "1"); // $row->ifakturcode
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, "1"); // $nomorpajak
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, "0"); // $nomorpajak
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->npwp); // $row->customername
					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->customername); // $bruto[$jml]
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->ifakturcode); // $row->nilaidiscon
					$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, "0"); // $dpp[$jml]
					$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, ""); // $ppn[$jml]
					$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$j, ""); // $total[$jml]
					$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$j, $tglfaktur[$jml], Cell_DataType::TYPE_STRING); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('N'.$j, ""); 
					//$ObjPHPExcel->getActiveSheet()->getStyle('O'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT );
					$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$j, $masapajak[$jml], Cell_DataType::TYPE_STRING); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('P'.$j, $thpajak[$jml]); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('Q'.$j, "0"); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('R'.$j, $dpp[$jml]); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('S'.$j, $ppn[$jml]); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('T'.$j, "0"); 
																																																					
					$jml++;													
					$j++;																																																							
					$nomor++;
				}
			//}
				
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."laporan_fak_pajak_penj_do_".$dfirst."-".$dlast.".xls";
			$ObjWriter->save("files/".$files);
																							
		} else {

			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Faktur Penjualan Berdasarkan DO")
				->setSubject("Laporan Faktur Penjualan")
				->setDescription("Laporan Faktur Penjualan")
				->setKeywords("Laporan Faktur Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => false,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true

				)
			),
			'A1:T1'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4); //1
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8); //2
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8); //3
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8); //4
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8); //5
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6); //6
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15); //7
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20); //8
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15); //9
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12); //10
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12); //11
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12); //12
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(9); //13
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(9); //14
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8); //15
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8); //16
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8); //17
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10); //18
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10); //19
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10); //20
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'No');
			$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'Kode Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Kode Transaksi');
			$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'Kode Status');
			$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'Kode Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Flat VAT');
			$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'NPWP/ Nomor Paspor');
			$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'Nama Lawan Transaksi');
			$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'Nomor Faktur/ Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('J1', 'Jenis Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('K1', 'Nomor Faktur Pengganti/ Retur');
			$ObjPHPExcel->getActiveSheet()->setCellValue('L1', 'Jenis Dokumen Pengganti/ Retur');
			$ObjPHPExcel->getActiveSheet()->setCellValue('M1', 'Tanggal Faktur/ Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('N1', 'Tanggal SSP');
			$ObjPHPExcel->getActiveSheet()->setCellValue('O1', 'Masa Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('P1', 'Tahun Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('Q1', 'Pembetulan');
			$ObjPHPExcel->getActiveSheet()->setCellValue('R1', 'DPP');
			$ObjPHPExcel->getActiveSheet()->setCellValue('S1', 'PPN');
			$ObjPHPExcel->getActiveSheet()->setCellValue('T1', 'PPnBM');
			
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."laporan_fak_pajak_penj_do_".$dfirst."-".$dlast.".xls";
			$ObjWriter->save("files/".$files);									
		}
		
		$efilename = @substr($files,1,strlen($files));
	
		$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
		$data['isi']	= 'expofpenjualanperdo/vexpform';	
			$this->load->view('template',$data);	
		
	}
	
	function gexportpenjualandonext() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanperdo']			= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
	
		$data['list_fpenjperdo_tgl_mulai_faktur']	= $this->lang->line('list_fpenjperdo_tgl_mulai_faktur');
		$data['list_fpenjperdo_no_faktur']		= $this->lang->line('list_fpenjperdo_no_faktur');
		$data['list_fpenjperdo_no_pajak']		= $this->lang->line('list_fpenjperdo_no_pajak');
		$data['list_fpenjperdo_nm_pelanggan']	= $this->lang->line('list_fpenjperdo_nm_pelanggan');
		$data['list_fpenjperdo_npwp']			= $this->lang->line('list_fpenjperdo_npwp');
		$data['list_fpenjperdo_tgl_faktur']		= $this->lang->line('list_fpenjperdo_tgl_faktur');
		$data['list_fpenjperdo_tgl_bts_kirim']	= $this->lang->line('list_fpenjperdo_tgl_bts_kirim');
		$data['list_fpenjperdo_discount']		= $this->lang->line('list_fpenjperdo_discount');
		$data['list_fpenjperdo_total_faktur']	= $this->lang->line('list_fpenjperdo_total_faktur');
		$data['list_fpenjperdo_grand_total']	= $this->lang->line('list_fpenjperdo_grand_total');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$nofaktur	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		
		$nofak	= empty($nofaktur) || $nofaktur=='0' ?'0':$nofaktur;
		$dfirst	= empty($tdofirst) || $tdofirst=='0' ?'0':$tdofirst;
		$dlast	= empty($tdolast) || $tdolast=='0' ?'0':$tdolast;
		
		$extdofirst	= $tdofirst!=0?explode("-",$tdofirst,strlen($tdofirst)):$tdofirst;
		$extdolast	=  $tdolast!=0?explode("-",$tdolast,strlen($tdolast)):$tdolast;
		
		$nwdofirst	= $extdofirst!=0?$extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0]:$extdofirst;
		$nwdolast	=$extdolast!=0? $extdolast[2]."/".$extdolast[1]."/".$extdolast[0]:$extdolast;
		
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= empty($nwdofirst)?"":$nwdofirst;
		$data['tgldoakhir']	= empty($nwdolast)?"":$nwdolast;
		$data['nofaktur']	= $nofaktur=='0'?"":$nofaktur;
				
		/*** $this->load->model('expofpenjualanperdo/mclass'); ***/
		
		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expofpenjualanperdo/cform/gexportpenjualandonext/'.$nofak.'/'.$dfirst.'/'.$dlast;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
				
		$data['isi']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('expofpenjualanperdo/vexpform',$data);
	}	
}
?>
