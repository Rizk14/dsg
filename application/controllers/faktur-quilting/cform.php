<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-quilting/mmaster');
  }

function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$no_sj_masuk = $this->input->post('sj_masuk', TRUE);  
	$id_sj_masuk = $this->input->post('id_sj', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	//$id_sj_detail = $this->input->post('id_brg', TRUE);  
	$unit_makloon = $this->input->post('unit_makloon', TRUE);  
	$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
	
	$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
		//if ($no_sj_keluar !='') {
			$data['sj_detail'] = $this->mmaster->get_detail_sj_masuk($no_sj_masuk, $id_sj_masuk, $unit_makloon);
			$data['msg'] = '';
			$data['no_sj_masuk'] = $no_sj_masuk;
		//}
		/*else {
			$data['msg'] = 'SJ Masuk harus dipilih';
			$data['no_sj_masuk'] = '';
			$data['list_quilting'] = $this->mmaster->get_supplier();
		} */
		$data['go_proses'] = '1';
		$data['jenis_pembelian'] = $jenis_pembelian;
		
		$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$unit_makloon' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) 
			$nama_unit	= $hasilrow->nama;
				
			$data['nama_unit'] = $nama_unit;
			$data['unit_makloon'] = $unit_makloon;
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['list_quilting'] = $this->mmaster->get_supplier();
	}

	$data['isi'] = 'faktur-quilting/vmainform';
	$this->load->view('template',$data);

  }
  
  function edit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_faktur 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$unit_makloon 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
	
	$data['query'] = $this->mmaster->get_sjfaktur($id_faktur);
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['cunit_makloon'] = $unit_makloon;
	$data['carinya'] = $carinya;

	$data['isi'] = 'faktur-quilting/veditform';
	$this->load->view('template',$data); // 

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$no_faktur 	= $this->input->post('no_faktur', TRUE);
			$no_sj_masuk 	= $this->input->post('no_sj_masuk', TRUE);
			$no_sj_masuk = trim($no_sj_masuk);
			$unit_makloon = $this->input->post('unit_makloon', TRUE);
			$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);
			$gtotal = $this->input->post('gtotal', TRUE);  
			
			$tgl_faktur = $this->input->post('tgl_faktur', TRUE);  
			$pisah1 = explode("-", $tgl_faktur);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_faktur = $thn1."-".$bln1."-".$tgl1;
			
			$no 	= $this->input->post('no', TRUE);
						
			// jika edit, var ini ada isinya
			$id_faktur 	= $this->input->post('id_faktur', TRUE);
			
			if ($id_faktur == '') {
				$cek_data = $this->mmaster->cek_data($no_faktur, $unit_makloon);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'faktur-quilting/vmainform';
					$data['msg'] = "Data no faktur ".$no_faktur." untuk unit makloon ".$unit_makloon." sudah ada..!";
					$data['list_quilting'] = $this->mmaster->get_supplier();
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						// 1. update data harga dan totalnya (OK)
						$this->mmaster->saveharga($unit_makloon, $this->input->post('id_sj_detail_'.$i, TRUE), 
								$this->input->post('kode_brg_makloon_'.$i, TRUE),
								$this->input->post('harga_'.$i, TRUE), $this->input->post('harga_lama_'.$i, TRUE),
								$this->input->post('total_'.$i, TRUE) );
					} 
					// 2. insert data faktur dan detail no sj-nya (OK)
					$tgl = date("Y-m-d");
					$data_header = array(
							  'no_faktur'=>$no_faktur,
							  'tgl_faktur'=>$tgl_faktur,
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl,
							  'kode_unit'=>$unit_makloon,
							  'jenis_makloon'=>'1',
							  'jenis_pembelian'=>$jenis_pembelian,
							  'jumlah'=>$gtotal
							);
					$this->db->insert('tm_faktur_makloon',$data_header);
					
					$list_sj = explode(",", $no_sj_masuk); 
					// ambil data terakhir di tabel tm_faktur_makloon
					$query3	= $this->db->query(" SELECT id FROM tm_faktur_makloon ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query3->row();
					$id_fq	= $hasilrow->id;
					
					// insert tabel detail sj-nya
					foreach($list_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') {
							$data_detail = array(
							  'id_faktur_makloon'=>$id_fq,
							  'no_sj_masuk'=>$row1
							);
							$this->db->insert('tm_faktur_makloon_sj',$data_detail);
														
							// 10-04-2012 tambahkan update field jenis_pembelian
							$this->db->query(" UPDATE tm_sj_hasil_makloon SET no_faktur = '$no_faktur', status_faktur = 't',
												jenis_pembelian = '$jenis_pembelian'
												WHERE no_sj = '$row1' AND kode_unit = '$unit_makloon' ");
						}
					}
					
					redirect('faktur-quilting/cform/view');
				}
			} // end if id_faktur == ''
			else { // update
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$cunit_makloon = $this->input->post('cunit_makloon', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
							
				$tgl = date("Y-m-d");
				
				// update di tabel tm_faktur_makloon
					$this->db->query(" UPDATE tm_faktur_makloon SET no_faktur = '$no_faktur', tgl_faktur = '$tgl_faktur', 
									jumlah= '$gtotal', tgl_update = '$tgl'
									where id= '$id_faktur' ");
				
				$jumlah_input=$no-1;
				for ($i=1;$i<=$jumlah_input;$i++)
				{ // detailnya
					
					// ============ update harga =====================
					$id_sj_detail = $this->input->post('id_sj_detail_'.$i, TRUE);
					$harga = $this->input->post('harga_'.$i, TRUE);
					$harga_lama = $this->input->post('harga_lama_'.$i, TRUE);
					$total = $this->input->post('total_'.$i, TRUE);
					$kode_brg_makloon = $this->input->post('kode_brg_makloon_'.$i, TRUE);
					
					if ($harga != $harga_lama) {
						$this->db->query(" UPDATE tm_sj_hasil_makloon_detail SET harga = '$harga', biaya = '$total' 
						WHERE id = '$id_sj_detail' ");	
						
						$this->db->query(" UPDATE tm_harga_quilting SET harga = '$harga', tgl_update = '$tgl'
									WHERE kode_brg_quilting = '$kode_brg_makloon' AND kode_unit = '$unit_makloon' ");
						
						$datanya = array(
							  'kode_brg_quilting'=>$kode_brg_makloon,
							  'kode_unit'=>$unit_makloon,
							  'tgl_input'=>$tgl,
							  'harga'=>$harga
							);
						$this->db->insert('tt_harga_quilting',$datanya);	
						
					} // end if
				} // end for
				// 10-04-2012, update jenis_pembelian
				$list_sj = explode(",", $no_sj_masuk); 
				foreach($list_sj as $row1) {
					$row1 = trim($row1);
					if ($row1 != '') {
						$this->db->query(" UPDATE tm_sj_hasil_makloon SET jenis_pembelian = '$jenis_pembelian'
												WHERE no_sj = '$row1' AND kode_unit = '$unit_makloon' ");
					}
				}
					
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "faktur-quilting/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "faktur-quilting/cform/cari/".$cunit_makloon."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('sj-masuk-makloon/cform/view');
			}

  }
  
  function view(){ 
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'faktur-quilting/vformview';
    $keywordcari = "all";
    $unit_makloon = '0';
	//$kode_bagian = '';
	
   $jum_total = $this->mmaster->getAlltanpalimit($unit_makloon, $keywordcari); 
							$config['base_url'] = base_url().'index.php/faktur-quilting/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $unit_makloon, $keywordcari);		
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['list_unit'] = $this->mmaster->get_supplier();
	$data['unit_makloon'] = $unit_makloon;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$unit_makloon = $this->input->post('unit_makloon', TRUE);  
	
	if ($keywordcari == '' && $unit_makloon == '') {
		$unit_makloon 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($unit_makloon == '')
		$unit_makloon = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($unit_makloon, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-quilting/cform/cari/'.$unit_makloon.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $unit_makloon, $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'faktur-quilting/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_unit'] = $this->mmaster->get_supplier();
	$data['unit_makloon'] = $unit_makloon;
	$this->load->view('template',$data);
  }

  function delete(){
    $id_faktur 	= $this->uri->segment(4);
    
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $cunit_makloon 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($id_faktur);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "faktur-quilting/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "faktur-quilting/cform/cari/".$cunit_makloon."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('sj-masuk-makloon/cform/view');
  }
  
  function generate_nomor() {
		//$jenis_brg 	= $this->uri->segment(4);
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================

	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);

	if ($posisi == '' || $kel_brg == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg 	= $this->input->post('kel_brg', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		$id_jenis_bhn = $this->input->post('id_jenis_bhn', TRUE);  
		
		if ($keywordcari == '' && ($id_jenis_bhn == '' || $posisi == '' || $kel_brg == '') ) {
			$kel_brg 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$id_jenis_bhn 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $id_jenis_bhn);
		
		$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg/".$kel_brg."/".$posisi."/".$id_jenis_bhn."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $id_jenis_bhn);

	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['kel_brg'] = $kel_brg;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;
	$data['jenis_bhn'] = $this->mmaster->get_jenis_bhn($kel_brg);
	$data['cjenis_bhn'] = $id_jenis_bhn;

	$this->load->view('sj-keluar-makloon/vpopupbrg',$data);
  }
  
  // popup sj masuk
  function show_popup_sj_masuk(){
	// =======================
	// disini coding utk pengecekan user login
	//========================

	$unit_makloon	= $this->uri->segment(4);

	if ($unit_makloon == '') {
		$unit_makloon 	= $this->input->post('unit_makloon', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $unit_makloon == '' ) {
			$unit_makloon 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";

		$qjum_total = $this->mmaster->get_sj_masuktanpalimit($keywordcari, $unit_makloon);
		
		/*$config['base_url'] = base_url()."index.php/faktur-quilting/cform/show_popup_sj_masuk/".$unit_makloon."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config); */
	//$data['query'] = $this->mmaster->get_sj_masuk($config['per_page'],$config['cur_page'], $keywordcari, $unit_makloon);
	$data['query'] = $this->mmaster->get_sj_masuk($keywordcari, $unit_makloon);

	$data['jum_total'] = count($qjum_total);
	$data['unit_makloon'] = $unit_makloon;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$unit_makloon' ");
	$hasilrow = $query3->row();
	$nama_unit	= $hasilrow->nama;
	$data['nama_unit'] = $nama_unit;

	$this->load->view('faktur-quilting/vpopupsjmasuk',$data);
	
  }
  
}
