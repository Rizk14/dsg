<?php
class ctest extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('trial-keluarother-unitpacking/mmaster');
  }
	
	// 14-03-2013, SJ masuk WIP
  function viewsjotherunitpacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'trial-keluarother-unitpacking/vformviewsjotherunitpacking';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	$id_unit_asal = '0';
	$id_unit_tujuan = '0';
	$caribrg = "all";
	$filterbrg = "n";
	
    $jum_total = $this->mmaster->getAllsjotherunitpackingtanpalimit($keywordcari, $date_from, $date_to, $id_unit_asal, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/trial-keluarother-unitpacking/ctest/viewsjotherunitpacking/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllsjotherunitpacking($config['per_page'],$this->uri->segment(4), $keywordcari, $date_from, $date_to, $id_unit_asal,  $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['id_unit_asal'] = $id_unit_asal;
	$this->load->view('template',$data);
  }
  
  function addsjotherunitpacking(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_sj FROM tm_sjkeluarother_unit_packing ORDER BY no_sj DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_sj	= $hasilrow->no_sj;
			else
				$no_sj = '';
			if(strlen($no_sj)==9) {
				$nosj = substr($no_sj, 0, 9);
				$n_sj	= (substr($nosj,4,5))+1;
				$th_sj	= substr($nosj,0,4);
				if($th_now==$th_sj) {
						$jml_n_sj	= $n_sj;
						switch(strlen($jml_n_sj)) {
							case "1": $kodesj	= "0000".$jml_n_sj;
							break;
							case "2": $kodesj	= "000".$jml_n_sj;
							break;	
							case "3": $kodesj	= "00".$jml_n_sj;
							break;
							case "4": $kodesj	= "0".$jml_n_sj;
							break;
							case "5": $kodesj	= $jml_n_sj;
							break;	
						}
						$nomorsj = $th_now.$kodesj;
				}
				else {
					$nomorsj = $th_now."00001";
				}
			}
			else {
				$nomorsj	= $th_now."00001";
			}

	$data['isi'] = 'trial-keluarother-unitpacking/vmainformsjotherunitpacking';
	$data['msg'] = '';
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['no_sj'] = $nomorsj;
	$this->load->view('template',$data);
  }
  function submitsjotherunitpacking(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
			$no_bp 	= $this->input->post('no_bp', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$id_unit_asal 	= $this->input->post('id_unit_asal', TRUE);  
		 
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data_sjotherunitpacking($no_sj, $thn1, $id_unit_asal);
			if (count($cek_data) > 0) {
				$data['isi'] = 'trial-keluarother-unitpacking/vmainformsjotherunitpacking';
				$data['msg'] = "Data no SJ ".$no_sj." sudah ada..!";
				$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
				$this->load->view('template',$data);
			}
			else {
				// new 05-03-2014
				$tgl = date("Y-m-d H:i:s");
				$uid_update_by = $this->session->userdata('uid');
				// insert di tm_sjkeluarwip
				$data_header = array(
				  'no_sj'=>$no_sj,
				   'no_bp'=>$no_bp,
				  'tgl_sj'=>$tgl_sj,
				  'id_unit_asal'=>$id_unit_asal,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'keterangan'=>$ket,
				  'uid_update_by' => $uid_update_by
				);
				$this->db->insert('tm_sjkeluarother_unit_packing',$data_header);
				
				// ambil data terakhir di tabel tm_sjotherrunit
				$query2	= $this->db->query(" SELECT id FROM tm_sjkeluarother_unit_packing ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query2->row();
				$id_sj	= $hasilrow->id; 
				
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->savesjotherunitpacking($id_sj, $id_unit_asal,
							$this->input->post('id_brg_wip_'.$i, TRUE),$this->input->post('nama_brg_wip_'.$i, TRUE), 
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('id_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) );						
				}
				redirect('trial-keluarother-unitpacking/ctest/viewsjotherunitpacking');
			}
  }
  
   function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil id, nama_brg
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = 0;
			$nama_brg_wip = '';
		}
		
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['id_brg_wip'] = $id_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('trial-keluarother-unitpacking/vinfobrgwip', $data); 
		return true;
  }
  function carisjotherunitpacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$id_unit_asal 	= $this->input->post('id_unit_asal', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
	
	// 24-02-2015
	$submit2 	= $this->input->post('submit2', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($id_unit_asal == '')
		$id_unit_asal = $this->uri->segment(6);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(7);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(8);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(9);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($caribrg == '')
		$caribrg = "all";
	if ($filterbrg == '')
		$filterbrg = "n";
	
	//if ($submit2 == "") {
		$jum_total = $this->mmaster->getAllsjotherunitpackingtanpalimit($keywordcari, $date_from, $date_to, $id_unit_asal, $caribrg, $filterbrg);
								$config['base_url'] = base_url().'index.php/wip/cform/carisjotherunitpacking/'.$date_from.'/'.$date_to.'/'.$id_unit_asal.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
								$config['total_rows'] = count($jum_total); 
								$config['per_page'] = '10';
								$config['first_link'] = 'Awal';
								$config['last_link'] = 'Akhir';
								$config['next_link'] = 'Selanjutnya';
								$config['prev_link'] = 'Sebelumnya';
								$config['cur_page'] = $this->uri->segment(10);
								$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllsjotherunitpacking($config['per_page'],$this->uri->segment(10), $keywordcari, $date_from, $date_to, $id_unit_asal, $caribrg, $filterbrg);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = 1;
		
		$data['isi'] = 'trial-keluarother-unitpacking/vformviewsjotherunitpacking';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		if ($date_from=="00-00-0000")
			$data['date_from'] = '';
		else
			$data['date_from'] = $date_from;
		
		if ($date_to=="00-00-0000")
			$data['date_to'] = '';
		else
			$data['date_to'] = $date_to;
		
		if ($caribrg == "all")
			$data['caribrg'] = '';
		else
			$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;
		
		$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
		$data['id_unit_asal'] = $id_unit_asal;
		$this->load->view('template',$data);
  }
  
  function ceknosjotherunitpacking(){
		$no_sj 	= $this->input->post('no_sj', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sj', TRUE);
		$id_unit_asal 	= $this->input->post('id_unit_asal', TRUE);
	
		
		$pisah1 = explode("-", $tgl_sj);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$cek_data = $this->mmaster->cek_data_sjotherunitpacking($no_sj, $thn1, $id_unit_asal);
		if (count($cek_data) > 0)
			$ada = "ada";	
		else
			$ada = "tidak";
		
		echo $ada; 
		return true;
  }
   
  function additemwarna(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ke tabel tm_barang_wip utk ambil id, nama_brg
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = 0;
			$nama_brg_wip = '';
		}
		
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
									WHERE a.id_brg_wip = '".$id_brg_wip."' ORDER BY b.nama");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['id_brg_wip'] = $id_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('trial-keluarother-unitpacking/vlistwarna', $data); 
		return true;
  }
  function deletesjotherunitpacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$id_unit_asal 	= $this->uri->segment(9);
	$id_unit_tujuan 	= $this->uri->segment(10);
	$carinya 	= $this->uri->segment(11);
    
    $this->mmaster->deletesjotherunitpacking($id);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "trial-keluarother-unitpacking/ctest/viewsjotherunitpacking/".$cur_page;
	else
		$url_redirectnya = "trial-keluarother-unitpacking/ctest/viewsjotherunitpacking/".$tgl_awal."/".$tgl_akhir."/".$id_unit_asal."/".$id_unit_tujuan."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  function editsjotherunitpacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_sj 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$id_unit_asal 	= $this->uri->segment(9);
	$carinya 	= $this->uri->segment(10);
	$caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);

	
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['id_unit_asal'] = $id_unit_asal;
	$data['carinya'] = $carinya;
	$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['query'] = $this->mmaster->get_sjkeluar_other_unit_packing($id_sj); 
	$data['msg'] = '';
	$data['isi'] = 'trial-keluarother-unitpacking/veditformsjkeluarother';
	$this->load->view('template',$data);

  }
  
  function updatedatasjkeluar() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_sj 					= $this->input->post('id_sj', TRUE);
			$no_sj 					= $this->input->post('no_sj', TRUE);
			$tgl_sj 				= $this->input->post('tgl_sj', TRUE);  
			$ket 					= $this->input->post('ket', TRUE);  
			$id_unit_asal 			= $this->input->post('id_unit_asal', TRUE);
			$id_unit_packing_lama 	= $this->input->post('id_unit_packing_lama', TRUE);  
			
			
			$pisah1 = explode("-", $tgl_sj);
			$tgl1	= $pisah1[0];
			$bln1	= $pisah1[1];
			$thn1	= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$id_unit_asal = $this->input->post('id_unit_asal', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
			
						
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			

			//update headernya
			$this->db->query(" UPDATE tm_sjkeluarother_unit_packing SET tgl_sj = '$tgl_sj',  tgl_update='$tgl',
							no_sj='".$this->db->escape_str($no_sj)."',  
							id_unit_asal = '$id_unit_asal', 
							keterangan = '$ket' , uid_update_by='$uid_update_by' where id= '$id_sj' ");
							
				//reset stok, dan hapus dulu detailnya
				//============= 08-03-2016 ====================
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarother_unit_packing_detail WHERE id_sjkeluarother_unit_packing = '$id_sj' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					//$tgl = date("Y-m-d");
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok  =====================
						//$nama_tabel_stok = "tm_stok_unit_packing";
				
						//cek stok terakhir tm_stok_unit_packing, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_packing
										WHERE id_brg_wip = '$row2->id_brg_wip' AND id_unit='$id_unit_packing_lama' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
								
						$this->db->query(" UPDATE tm_stok_unit_packing SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg_wip= '$row2->id_brg_wip' AND id_unit = '$id_unit_packing_lama' ");
						
						
						// 2. reset stok per warna dari tabel tm_sjkeluarother_unit_packing_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_sjkeluarother_unit_packing_detail_warna 
												WHERE id_sjkeluarother_unit_packing_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_unit_packing_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_packing_warna
											 WHERE id_stok_unit_packing = '$id_stok' 
											 AND id_warna='$rowwarna->id_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari SJ keluar
										
								$this->db->query(" UPDATE tm_stok_unit_packing_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_unit_packing= '$id_stok'
											AND id_warna = '$rowwarna->id_warna' ");
							}
						} // end if detail warna
						
						$this->db->query(" DELETE FROM tm_sjkeluarother_unit_packing_detail_warna WHERE id_sjkeluarother_unit_packing_detail = '$row2->id' ");
						
						
						// ==============================================
					} // end foreach detail
				} // end reset stok
				//=============================================
				$this->db->delete('tm_sjkeluarother_unit_packing_detail', array('id_sjkeluarother_unit_packing' => $id_sj));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$nama_brg_wip = $this->input->post('nama_brg_wip_'.$i, TRUE);
						//$qty = $this->input->post('qty_'.$i, TRUE);
						//$ket_warna = $this->input->post('ket_warna_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
						
						$temp_qty = $this->input->post('temp_qty_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$qty_warna = $this->input->post('qty_warna_'.$i, TRUE);
						
						// 08-03-2016, insert ke tabel detail ----------------------------------
						//-------------- hitung total qty dari detail tiap2 warna -------------------
							$qtytotal = 0;
							for ($xx=0; $xx<count($id_warna); $xx++) {
								$id_warna[$xx] = trim($id_warna[$xx]);
								$qty_warna[$xx] = trim($qty_warna[$xx]);
																
								$qtytotal+= $qty_warna[$xx];
							} // end for
						// ---------------------------------------------------------------------
						
						
						// ======== update stoknya! =============
						
						// 08-03-2016, pake id_unit_asal yg baru
						// 1. stok total
						// ============================= update stok PABRIK ======================================
						//cek stok terakhir tm_stok_unit_packing, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_packing WHERE id_brg_wip = '$id_brg_wip'
												AND id_unit='$id_unit_asal' ");
							if ($query3->num_rows() == 0){
								$id_stok = 0;
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok	= $hasilrow->id;
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qtytotal;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_packing, insert
								$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_packing ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
								
								$data_stok = array(
								'id'=>$id_stok_unit,
									'id_brg_wip'=>$id_brg_wip,
									'stok'=>$new_stok,
									'id_unit'=>$id_unit_asal,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_unit_packing', $data_stok);
								
								// ambil id_stok utk dipake di stok warna
								$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_unit_packing ORDER BY id DESC LIMIT 1 ");
								if($sqlxx->num_rows() > 0) {
									$hasilxx	= $sqlxx->row();
									$id_stok	= $hasilxx->id;
								}else{
									$id_stok	= 1;
								}
							}
							else {
								$this->db->query(" UPDATE tm_stok_unit_packing SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg_wip= '$id_brg_wip' AND id_unit = '$id_unit_asal' ");
							}
					
					// =====================================================================================
					
					// jika semua data tdk kosong, insert ke tm_sjkeluarother_unit_packing_detail
					$data_detail = array(
						'id_sjkeluarother_unit_packing'=>$id_sj,
						'id_brg_wip'=>$id_brg_wip,
						'nama_brg_wip'=>$nama_brg_wip,
						'qty'=>$qtytotal,
						'keterangan'=>$ket_detail
						//'ket_qty_warna'=>$ket_warna,
					);
					$this->db->insert('tm_sjkeluarother_unit_packing_detail',$data_detail);
							// ================ end insert item detail ===========
					
					// ambil id detail sjmasukwip_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tm_sjkeluarother_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;
					
					// ----------------------------------------------
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$qty_warna[$xx] = trim($qty_warna[$xx]);
								
						$seq_warna	= $this->db->query(" SELECT id FROM tm_sjkeluarother_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
							
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}

						$tm_sjkeluarother_unit_packing_detail_warna	= array(
							 'id'=>$idbaru,
							 'id_sjkeluarother_unit_packing_detail'=>$iddetail,
							 'id_warna'=>$id_warna[$xx],
							 'qty'=>$qty_warna[$xx]
						);
						$this->db->insert('tm_sjkeluarother_unit_packing_detail_warna',$tm_sjkeluarother_unit_packing_detail_warna);
						
						// ========================= 08-03-2016, stok per warna ===============================================
						//cek stok terakhir tm_stok_unit_packing_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_packing_warna WHERE id_warna = '".$id_warna[$xx]."'
								AND id_stok_unit_packing='$id_stok' ");
						if ($query3->num_rows() == 0){
							$stok_warna_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_warna_lama	= $hasilrow->stok;
						}
						$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_packing_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_packing_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_unit_packing'=>$id_stok,
								'id_warna'=>$id_warna[$xx],
								'stok'=>$new_stok_warna,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_packing_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_packing_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
							where id_warna= '".$id_warna[$xx]."' AND id_stok_unit_packing='$id_stok' ");
						}
						
					
					// ------------------------------------------------------------------------------------------
						
					} // end for
					// ----------------------------------------------
						
				} // end perulangan

			if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "trial-keluarother-unitpacking/ctest/viewsjotherunitpacking/".$cur_page;
	else
		$url_redirectnya = "trial-keluarother-unitpacking/ctest/viewsjotherunitpacking/".$tgl_awal."/".$tgl_akhir."/".$id_unit_asal."/".$id_unit_tujuan."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
				
  }
  
  
}
