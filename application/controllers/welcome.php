<?php

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
	}
	
	function index()
	{
		if ($this->session->userdata('uid')!= 0) {
			$isi['isi'] = 'tengah';
			$this->load->view('template',$isi);
		}
		else
			redirect('loginform');
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
