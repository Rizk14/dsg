<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
		$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
		$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
		$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
		$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('penyetor/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'penyetor/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'penyetor/vmainform';
		$this->load->view('template',$data);
		
	}
function tambah() {
		$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
		$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
		$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
		$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
		$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('penyetor/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'penyetor/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'penyetor/vmainformadd';
		$this->load->view('template',$data);
		
	}
	function pagesnext() {
		$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
		$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
		$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
		$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
		$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('penyetor/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'penyetor/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();	
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'penyetor/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail() {	
		$data['isi']	= 'penyetor/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
		$epenyetor	= @$this->input->post('txtpenyetor')?@$this->input->post('txtpenyetor'):@$this->input->get_post('txtpenyetor');
		$this->load->model('penyetor/mclass');
		$qpenyetor	= $this->mclass->cekpenyetor($epenyetor);
		$npenyetor	= $qpenyetor->num_rows();
		if(($epenyetor!=0 || $epenyetor!="")) {
			if($npenyetor==0 || $npenyetor<=0){
				$this->mclass->msimpan($epenyetor);
			}else{
				$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
				$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
				$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
				$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
				$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
				$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
				$data['detail']		= "";
				$data['list']		= "";
				$data['not_defined']= "Maaf Data tdk dapat disimpan, terimakasih";
				$data['limages']	= base_url();
				
				$this->load->model('penyetor/mclass');
				$query	= $this->mclass->view();
				$jml	= $query->num_rows();
				
				$pagination['base_url'] = 'penyetor/cform/pagesnext/';
				$pagination['total_rows']	= $jml;
				$pagination['per_page']		= 20;
				$pagination['first_link'] 	= 'Awal';
				$pagination['last_link'] 	= 'Akhir';
				$pagination['next_link'] 	= 'Selanjutnya';
				$pagination['prev_link'] 	= 'Sebelumnya';
				$pagination['cur_page'] 	= $this->uri->segment(4,0);
				$this->pagination_ori->initialize($pagination);
				$data['create_link']	= $this->pagination_ori->create_links();
				$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
				
				$data['isi']	= 'penyetor/vmainform';
				$this->load->view('template',$data);
			
			}
		} else {
			$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
			$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
			$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
			$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
			$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
			$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Maaf Data tdk dapat disimpan, terimakasih";
			$data['limages']	= base_url();
				
			$this->load->model('penyetor/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
				
			$pagination['base_url'] = 'penyetor/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			
			$data['isi']	= 'penyetor/vmainform';
				$this->load->view('template',$data);
		}
	}
	
	function edit() {
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
		$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
		$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
		$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
		$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$limages			= base_url();
		$data['list']		= "";
		$this->load->model('penyetor/mclass');
		$qry_klsbrg		= $this->mclass->medit($id);
		if( $qry_klsbrg->num_rows() > 0 ) {
			$row_klsbrg	= $qry_klsbrg->row();
			$data['ipenyetor']		= $row_klsbrg->i_penyetor;
			$data['epenyetor']	= $row_klsbrg->e_penyetor;
		} else {
			$data['epenyetor']	= "";		
		}
		$data['isi']	= 'penyetor/veditform';
		$this->load->view('template',$data);
		
	}
	
	function actedit() {
		$ipenyetor= @$this->input->post('ipenyetor')?@$this->input->post('ipenyetor'):@$this->input->get_post('ipenyetor');
		$epenyetor= @$this->input->post('txtpenyetor')?@$this->input->post('txtpenyetor'):@$this->input->get_post('txtpenyetor');
		if((isset($epenyetor) || !empty($epenyetor)) &&
		    ($ipenyetor!=0 || $ipenyetor!="")) {
			$this->load->model('penyetor/mclass');
			$this->mclass->mupdate($ipenyetor,$epenyetor);
		} else {
				$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
				$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
				$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
				$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
				$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
				$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
				$data['detail']		= "";
				$data['list']		= "";
				$data['not_defined']= "Maaf Data tdk dapat disimpan, terimakasih";
				$data['limages']	= base_url();
				
				$this->load->model('penyetor/mclass');
				$query	= $this->mclass->view();
				$jml	= $query->num_rows();
				
				$pagination['base_url'] = 'penyetor/cform/pagesnext/';
				$pagination['total_rows']	= $jml;
				$pagination['per_page']		= 20;
				$pagination['first_link'] 	= 'Awal';
				$pagination['last_link'] 	= 'Akhir';
				$pagination['next_link'] 	= 'Selanjutnya';
				$pagination['prev_link'] 	= 'Sebelumnya';
				$pagination['cur_page'] 	= $this->uri->segment(4,0);
				$this->pagination_ori->initialize($pagination);
				$data['create_link']	= $this->pagination_ori->create_links();
				$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
				
				$data['isi']	= 'penyetor/vmainform';
				$this->load->view('template',$data);
		}
	}

	function actdelete() {
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('penyetor/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$txtcaripenyetor	= $this->input->post('txtcaripenyetor')?$this->input->post('txtcaripenyetor'):$this->input->get_post('txtakunpajak');

		$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
		$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
		$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
		$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
		$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('penyetor/mclass');
		$query	= $this->mclass->viewcari($txtcaripenyetor);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'penyetor/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txtcaripenyetor,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('penyetor/vcariform',$data);
	}

	function carinext() {
		$txtcaripenyetor	= $this->input->post('txtcaripenyetor')?$this->input->post('txtcaripenyetor'):$this->input->get_post('txtakunpajak');

		$data['page_title_penyetor']	= $this->lang->line('page_title_penyetor');
		$data['form_nama_penyetor']	= $this->lang->line('form_nama_penyetor');
		$data['form_panel_daftar_penyetor']	= $this->lang->line('form_panel_daftar_penyetor');
		$data['form_form_penyetor']	= $this->lang->line('form_form_penyetor');
		$data['form_cari_penyetor']	= $this->lang->line('form_cari_penyetor');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('penyetor/mclass');
		$query	= $this->mclass->viewcari($txtcaripenyetor);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'penyetor/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txtcaripenyetor,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('penyetor/vcariform',$data);
	}	
}
?>
