<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-stok-quilting/mmaster');
  }

/*  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['isi'] = 'info-opname-hutang/vmainform';
	$this->load->view('template',$data);

  } */
  
  function view(){
    $data['isi'] = 'info-stok-quilting/vformview';
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

    $jum_total = $this->mmaster->get_stoktanpalimit($keywordcari);

			$config['base_url'] = base_url().'index.php/info-stok-quilting/cform/view/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mmaster->get_stok($config['per_page'],$this->uri->segment(6), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }

}
