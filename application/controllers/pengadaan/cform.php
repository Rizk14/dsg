<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('pengadaan/mmaster');
	}

	// 27-01-2014
	function addsjmasuk()
	{
		// =======================
		// disini coding utk pengecekan user login	
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$th_now	= date("Y");

		$data['isi'] = 'pengadaan/vformaddsjmasuk';
		$data['msg'] = '';
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$data['thn_forecast'] = $th_now;
		$this->load->view('template', $data);
	}

	function show_popup_brg_jadi()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$posisi		= $this->input->post('posisi', TRUE);
		$keywordcari	= $this->input->post('cari', TRUE);

		if ($posisi == '' && $keywordcari == '') {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}

		if ($keywordcari == '')
			$keywordcari = "all";

		$qjum_total = $this->mmaster->getbarangjaditanpalimit($keywordcari);

		$config['base_url'] 	= base_url() . "index.php/pengadaan/cform/show_popup_brg_jadi/" . $posisi . "/" . $keywordcari . "/";
		$config['total_rows'] 	= count($qjum_total);
		$config['per_page'] 	= 10;
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page']		= $this->uri->segment(6, 0);
		$this->pagination->initialize($config);

		$data['query']		= $this->mmaster->getbarangjadi($config['per_page'], $config['cur_page'], $keywordcari);
		$data['jum_total']	= count($qjum_total);
		$data['posisi']		= $posisi;
		$data['jumdata'] = $posisi - 1;

		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$data['startnya']	= $config['cur_page'];

		$this->load->view('pengadaan/vpopupbrgjadi', $data);
	}

	function submitsjmasuk()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$bln_forecast = $this->input->post('bln_forecast', TRUE);
		$thn_forecast = $this->input->post('thn_forecast', TRUE);
		$no_sj 	= $this->input->post('no_sj', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sj', TRUE);
		$jenis 	= $this->input->post('jenis', TRUE);
		$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);
		$ket 	= $this->input->post('ket', TRUE);
		$pisah1 = explode("-", $tgl_sj);
		$tgl1 = $pisah1[0];
		$bln1 = $pisah1[1];
		$thn1 = $pisah1[2];
		$tgl_sj = $thn1 . "-" . $bln1 . "-" . $tgl1;

		$no 	= $this->input->post('no', TRUE);
		$jumlah_input = $no - 1;

		$cek_data = $this->mmaster->cek_data_sjmasuk($no_sj, $thn1, $id_unit_jahit);
		if (count($cek_data) > 0) {
			$data['isi'] = 'pengadaan/vformaddsjmasuk';
			$data['msg'] = "Data no SJ " . $no_sj . " utk tahun $thn1 sudah ada..!";
			$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
			$this->load->view('template', $data);
		} else {
			// insert di tm_sjmasukbhnbakupic
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			$data_header = array(
				'no_sj' => $no_sj,
				'tgl_sj' => $tgl_sj,
				'bln_forecast' => $bln_forecast,
				'thn_forecast' => $thn_forecast,
				'jenis_masuk' => $jenis,
				'id_unit' => $id_unit_jahit,
				'tgl_input' => $tgl,
				'tgl_update' => $tgl,
				'keterangan' => $ket,
				'uid_update_by' => $uid_update_by
			);
			$this->db->insert('tm_sjmasukbhnbakupic', $data_header);

			// ambil data terakhir di tabel tm_sjmasukbhnbakupic
			$query2	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id;
			//-----------

			for ($i = 1; $i <= $jumlah_input; $i++) {
				$this->mmaster->savesjmasuk(
					$id_sj,
					$jenis,
					$id_unit_jahit,
					$this->input->post('id_brg_wip_' . $i, TRUE),
					$this->input->post('brg_wip_' . $i, TRUE),
					$this->input->post('temp_qty_' . $i, TRUE),
					$this->input->post('id_warna_' . $i, TRUE),
					$this->input->post('qty_warna_' . $i, TRUE),
					$this->input->post('ket_detail_' . $i, TRUE)
				);
			}
			redirect('pengadaan/cform/viewsjmasuk');
		}
	}

	function viewsjmasuk()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}

		$keywordcari 	= $this->input->post('cari', TRUE);
		$date_from 	= $this->input->post('date_from', TRUE);
		$date_to 	= $this->input->post('date_to', TRUE);
		$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);
		$filterbrg 	= $this->input->post('filter_brg', TRUE);
		$caribrg 	= $this->input->post('cari_brg', TRUE);

		// 24-02-2015
		$submit2 	= $this->input->post('submit2', TRUE);

		if ($date_from == '')
			$date_from = $this->uri->segment(4);
		if ($date_to == '')
			$date_to = $this->uri->segment(5);
		if ($id_unit_jahit == '0')
			$id_unit_jahit = $this->uri->segment(6);
		if ($keywordcari == '')
			$keywordcari = $this->uri->segment(7);
		if ($caribrg == '')
			$caribrg = $this->uri->segment(8);
		if ($filterbrg == '')
			$filterbrg = $this->uri->segment(9);

		if ($keywordcari == '')
			$keywordcari 	= "all";
		if ($date_from == '')
			$date_from = "00-00-0000";
		if ($date_to == '')
			$date_to = "00-00-0000";
		if ($id_unit_jahit == '')
			$id_unit_jahit = '0';
		if ($caribrg == '')
			$caribrg = "all";
		if ($filterbrg == '')
			$filterbrg = "n";

		if ($submit2 == "") {
			$jum_total = $this->mmaster->getAllsjmasuktanpalimit($keywordcari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg);

			$config['base_url'] = base_url() . 'index.php/pengadaan/cform/viewsjmasuk/' . $date_from . '/' . $date_to . '/' . $id_unit_jahit . '/' . $keywordcari . '/' . $caribrg . '/' . $filterbrg;
			$config['total_rows'] = count($jum_total);
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(10);
			$this->pagination->initialize($config);
			$data['query'] = $this->mmaster->getAllsjmasuk($config['per_page'], $this->uri->segment(10), $keywordcari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg);
			$data['jum_total'] = count($jum_total);
			if ($config['cur_page'] == '')
				$cur_page = 0;
			else
				$cur_page = $config['cur_page'];
			$data['cur_page'] = $cur_page;

			$data['isi'] = 'pengadaan/vformviewsjmasuk';
			if ($keywordcari == "all")
				$data['cari'] = '';
			else
				$data['cari'] = $keywordcari;
			if ($date_from == "00-00-0000")
				$data['date_from'] = '';
			else
				$data['date_from'] = $date_from;

			if ($date_to == "00-00-0000")
				$data['date_to'] = '';
			else
				$data['date_to'] = $date_to;

			if ($caribrg == "all")
				$data['caribrg'] = '';
			else
				$data['caribrg'] = $caribrg;
			$data['filterbrg'] = $filterbrg;
			$data['id_unit_jahit'] = $id_unit_jahit;
			$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
			$this->load->view('template', $data);
		} else {
			// export excel
			$query = $this->mmaster->getAllsjmasuk_export($keywordcari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg);

			// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='9' align='center'>DATA SJ MASUK BAHAN BAKU DARI JAHITAN</th>
				 </tr>
				</table><br>";

			$html_data .= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th width='3%'>No</th>
					 <th width='10%'>No SJ</th>
					 <th width='8%'>Tgl SJ</th>
					 <th width='10%'>Jenis</th>
					 <th width='10%'>Unit Jahit</th>
					 <th width='15%'>List Barang Jadi</th>
					 <th width='5%'>Qty</th>
					 <th width='15%'>Qty Ket Warna</th>
					 <th>Last Update</th>
				 </tr>
				</thead>
				<tbody>";

			if (is_array($query)) {
				$no = 1;
				for ($j = 0; $j < count($query); $j++) {

					$pisah1 = explode("-", $query[$j]['tgl_update']);
					$tgl1 = $pisah1[2];
					$bln1 = $pisah1[1];
					$thn1 = $pisah1[0];
					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";

					$exptgl1 = explode(" ", $tgl1);
					$tgl1nya = $exptgl1[0];
					$jam1nya = $exptgl1[1];
					$tgl_update = $tgl1nya . " " . $nama_bln . " " . $thn1 . " " . $jam1nya;

					$pisah1 = explode("-", $query[$j]['tgl_sj']);
					$tgl1 = $pisah1[2];
					$bln1 = $pisah1[1];
					$thn1 = $pisah1[0];
					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					$tgl_sj = $tgl1 . " " . $nama_bln . " " . $thn1;

					$html_data .= "<tr>";
					$html_data .=    "<td>" . $no . "</td>";
					$html_data .=    "<td>" . $query[$j]['no_sj'] . "</td>";
					$html_data .=    "<td>" . $tgl_sj . "</td>";
					$html_data .=    "<td>" . $query[$j]['nama_jenis'] . "</td>";

					$html_data .=    "<td>" . $query[$j]['kode_unit'] . " - " . $query[$j]['nama_unit'] . "</td>";

					$html_data .= "<td style='white-space:nowrap;'>";
					if (is_array($query[$j]['detail_sj'])) {
						$var_detail = array();
						$var_detail = $query[$j]['detail_sj'];
						$hitung = count($var_detail);
						for ($k = 0; $k < count($var_detail); $k++) {
							$html_data .= $var_detail[$k]['kode_brg_jadi'] . " - " . $var_detail[$k]['nama_brg_jadi'];
							if ($k < $hitung - 1)
								$html_data .= "<br> ";
						}
					}
					$html_data .= "</td>";

					$html_data .= "<td style='white-space:nowrap;' align='right'>";
					if (is_array($query[$j]['detail_sj'])) {
						$var_detail = array();
						$var_detail = $query[$j]['detail_sj'];
						$hitung = count($var_detail);
						for ($k = 0; $k < count($var_detail); $k++) {
							$html_data .= $var_detail[$k]['qty'];
							if ($k < $hitung - 1)
								$html_data .= "<br> ";
						}
					}
					$html_data .= "</td>";

					$html_data .= "<td style='white-space:nowrap;'>";
					if (is_array($query[$j]['detail_sj'])) {
						$var_detail = array();
						$var_detail = $query[$j]['detail_sj'];
						$hitung = count($var_detail);
						for ($k = 0; $k < count($var_detail); $k++) {
							$html_data .= $var_detail[$k]['ket_qty_warna'];
							if ($k < $hitung - 1)
								$html_data .= "<br> ";
						}
					}
					$html_data .= "</td>";

					$html_data .=    "<td align='center'>" . $tgl_update . "</td>";

					$html_data .=  "</tr>";
					$no++;
				} // end for1
			} // end if1

			//--------------------------------------------------------------------------------------------	

			$nama_file = "data_sj_masuk_bhnbaku_jahitan";
			$export_excel1 = '1';
			//if ($export_excel1 != '')
			$nama_file .= ".xls";
			//else
			//	$nama_file.= ".ods";
			$data = $html_data;

			include("../blnproduksi/system/application/libraries/generateExcelFile.php");
			return true;
		}
	}

	function deletesjmasuk()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}

		$id 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$tgl_awal 	= $this->uri->segment(6);
		$tgl_akhir 	= $this->uri->segment(7);
		$id_unit_jahit 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);
		$caribrg 	= $this->uri->segment(10);
		$filterbrg 	= $this->uri->segment(11);

		$this->mmaster->deletesjmasuk($id);

		if ($carinya == '') $carinya = "all";

		$url_redirectnya = "pengadaan/cform/viewsjmasuk/" . $tgl_awal . "/" . $tgl_akhir . "/" . $id_unit_jahit . "/" . $carinya . "/" . $caribrg . "/" . $filterbrg . "/" . $cur_page;
		redirect($url_redirectnya);
	}

	function editsjmasuk()
	{
		// =======================
		// disini coding utk pengecekan user login
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_sj 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$tgl_awal 	= $this->uri->segment(6);
		$tgl_akhir 	= $this->uri->segment(7);
		$id_unit_jahit 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);
		$caribrg 	= $this->uri->segment(10);
		$filterbrg 	= $this->uri->segment(11);

		$data['cur_page'] = $cur_page;
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['carinya'] = $carinya;
		$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;

		$data['query'] = $this->mmaster->get_sjmasuk($id_sj);
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vformeditsjmasuk';
		$this->load->view('template', $data);
	}

	// 04-02-2014
	function additemwarna()
	{

		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);

		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0) {
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		} else
			$id_brg_wip = 0;

		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
									WHERE a.id_brg_wip = '" . $id_brg_wip . "' ORDER BY b.nama ");
		if ($queryxx->num_rows() > 0) {
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(
					'id_warna' => $rowxx->id_warna,
					'nama_warna' => $rowxx->nama
				);
			}
		} else
			$detailwarna = '';

		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('pengadaan/vlistwarna', $data);
		return true;
	}

	function updatedatasjmasuk()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id_sj 	= $this->input->post('id_sj', TRUE);
		$no_sj 	= $this->input->post('no_sj', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sj', TRUE);
		$ket 	= $this->input->post('ket', TRUE);
		$jenis 	= $this->input->post('jenis', TRUE);
		$id_unit 	= $this->input->post('id_unit', TRUE);
		$id_unit_lama 	= $this->input->post('id_unit_lama', TRUE);

		$pisah1 = explode("-", $tgl_sj);
		$tgl1 = $pisah1[0];
		$bln1 = $pisah1[1];
		$thn1 = $pisah1[2];
		$tgl_sj = $thn1 . "-" . $bln1 . "-" . $tgl1;
		$no 	= $this->input->post('no', TRUE);

		$cur_page = $this->input->post('cur_page', TRUE);
		$tgl_awal = $this->input->post('tgl_awal', TRUE);
		$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
		$id_unit_jahit = $this->input->post('id_unit_jahit', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		$caribrg = $this->input->post('caribrg', TRUE);
		$filterbrg = $this->input->post('filterbrg', TRUE);

		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');

		//update headernya
		$this->db->query(" UPDATE tm_sjmasukbhnbakupic SET tgl_sj = '$tgl_sj', jenis_masuk = '$jenis', 
							id_unit = '$id_unit', tgl_update='$tgl', no_sj='$no_sj', 
							keterangan = '$ket', uid_update_by='$uid_update_by' where id= '$id_sj' ");

		//reset stok, dan hapus dulu detailnya
		$query2	= $this->db->query(" SELECT * FROM tm_sjmasukbhnbakupic_detail WHERE id_sjmasukbhnbakupic = '$id_sj' ");
		if ($query2->num_rows() > 0) {
			$hasil2 = $query2->result();

			foreach ($hasil2 as $row2) {
				$qty = $row2->qty;

				// 1. stok total
				// ============ update stok PABRIK =====================
				//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$row2->id_brg_wip' ");
				if ($query3->num_rows() == 0) {
					$stok_lama = 0;
					$id_stok = 0;
				} else {
					$hasilrow = $query3->row();
					$id_stok	= $hasilrow->id;
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama - $row2->qty; // berkurang stok karena reset

				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg_wip= '$row2->id_brg_wip' ");

				//================ update stok UNIT JAHIT ===============================
				//if ($id_unit_lama != '0') {
				//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
				$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit
											WHERE id_brg_wip = '$row2->id_brg_wip' AND id_unit='$id_unit_lama' ");
				if ($query3->num_rows() == 0) {
					$id_stok_unit = 0;
					$stok_unit_lama = 0;
					$stok_unit_bagus_lama = 0;
				} else {
					$hasilrow = $query3->row();
					$id_stok_unit	= $hasilrow->id;
					$stok_unit_lama	= $hasilrow->stok;
					$stok_unit_bagus_lama = $hasilrow->stok_bagus;
				}
				$new_stok_unit = $stok_unit_lama + $row2->qty; // bertambah stok unit karena reset sj masuk
				$new_stok_unit_bagus = $stok_unit_bagus_lama + $row2->qty;

				$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
										WHERE id_brg_wip= '$row2->id_brg_wip' AND id_unit = '$id_unit_lama' ");
				//} // end if

				// 2. reset stok per warna dari tabel tm_sjmasukbhnbakupic_detail_warna
				$querywarna	= $this->db->query(" SELECT * FROM tm_sjmasukbhnbakupic_detail_warna 
												WHERE id_sjmasukbhnbakupic_detail = '$row2->id' ");
				if ($querywarna->num_rows() > 0) {
					$hasilwarna = $querywarna->result();

					foreach ($hasilwarna as $rowwarna) {
						//============== update stok pabrik ===============================================
						//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND id_warna='$rowwarna->id_warna' ");
						if ($query3->num_rows() == 0) {
							$stok_warna_lama = 0;
						} else {
							$hasilrow = $query3->row();
							$stok_warna_lama	= $hasilrow->stok;
						}
						$new_stok_warna = $stok_warna_lama - $rowwarna->qty; // berkurang stok karena reset dari sj masuk

						$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND id_warna = '$rowwarna->id_warna' ");

						// ============= update stok unit jahit
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND id_warna='$rowwarna->id_warna' ");
						if ($query3->num_rows() == 0) {
							$stok_unit_warna_lama = 0;
							$stok_unit_warna_bagus_lama = 0;
						} else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama + $rowwarna->qty; // bertambah stok di unit karena reset dari sj masuk
						$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama + $rowwarna->qty;

						$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												stok_bagus = '$new_stok_unit_warna_bagus', 
												tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND id_warna = '$rowwarna->id_warna' ");
					}
				} // end if detail warna

				$this->db->query(" DELETE FROM tm_sjmasukbhnbakupic_detail_warna WHERE id_sjmasukbhnbakupic_detail = '$row2->id' ");
			} // end foreach detail
		} // end reset stok
		//=============================================
		$this->db->delete('tm_sjmasukbhnbakupic_detail', array('id_sjmasukbhnbakupic' => $id_sj));

		$jumlah_input = $no - 1;
		for ($i = 1; $i <= $jumlah_input; $i++) {
			$id_brg_wip = $this->input->post('id_brg_wip_' . $i, TRUE);
			$nama_brg_wip = $this->input->post('brg_wip_' . $i, TRUE);
			//$qty = $this->input->post('qty_'.$i, TRUE);
			$ket_detail = $this->input->post('ket_detail_' . $i, TRUE);

			$temp_qty = $this->input->post('temp_qty_' . $i, TRUE);
			$id_warna = $this->input->post('id_warna_' . $i, TRUE);
			$qty_warna = $this->input->post('qty_warna_' . $i, TRUE);

			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotal = 0;
			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);
				$qtytotal += $qty_warna[$xx];
			} // end for
			// ---------------------------------------------------------------------

			// 1. stok total
			// ============================= update stok PABRIK ======================================
			//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
			$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$id_brg_wip' ");
			if ($query3->num_rows() == 0) {
				$id_stok = 0;
				$stok_lama = 0;
			} else {
				$hasilrow = $query3->row();
				$id_stok	= $hasilrow->id;
				$stok_lama	= $hasilrow->stok;
			}
			$new_stok = $stok_lama + $qtytotal;

			if ($query3->num_rows() == 0) { // jika blm ada data di tm_stok_hasil_cutting, insert
				$data_stok = array(
					'id_brg_wip' => $id_brg_wip,
					'stok' => $new_stok,
					'tgl_update_stok' => $tgl
				);
				$this->db->insert('tm_stok_hasil_cutting', $data_stok);

				// ambil id_stok utk dipake di stok warna
				$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
				if ($sqlxx->num_rows() > 0) {
					$hasilxx	= $sqlxx->row();
					$id_stok	= $hasilxx->id;
				} else {
					$id_stok	= 1;
				}
			} else {
				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg_wip= '$id_brg_wip' ");
			}

			// ============= update stok UNIT JAHIT ======================
			//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
			$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit 
											WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$id_unit' ");
			if ($query3->num_rows() == 0) {
				$id_stok_unit = 0;
				$stok_unit_lama = 0;
				$stok_unit_bagus_lama = 0;
			} else {
				$hasilrow = $query3->row();
				$id_stok_unit	= $hasilrow->id;
				$stok_unit_lama	= $hasilrow->stok;
				$stok_unit_bagus_lama	= $hasilrow->stok_bagus;
			}
			$new_stok_unit = $stok_unit_lama - $qtytotal;
			$new_stok_unit_bagus = $stok_unit_bagus_lama - $qtytotal;

			if ($query3->num_rows() == 0) { // jika blm ada data di tm_stok_unit_jahit, insert
				$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
				if ($seqxx->num_rows() > 0) {
					$seqrowxx	= $seqxx->row();
					$id_stok_unit	= $seqrowxx->id + 1;
				} else {
					$id_stok_unit	= 1;
				}

				$data_stok = array(
					'id' => $id_stok_unit,
					'id_brg_wip' => $id_brg_wip,
					'stok' => $new_stok_unit,
					'stok_bagus' => $new_stok_unit_bagus,
					'id_unit' => $id_unit,
					'tgl_update_stok' => $tgl
				);
				$this->db->insert('tm_stok_unit_jahit', $data_stok);
			} else {
				$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
								stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
								where id_brg_wip= '$id_brg_wip' AND id_unit = '$id_unit' ");
			}
			// =====================================================================================

			// jika semua data tdk kosong, insert ke tm_sjmasukbhnbakupengadaan_detail
			$data_detail = array(
				'id_sjmasukbhnbakupic' => $id_sj,
				'id_brg_wip' => $id_brg_wip,
				'nama_brg_wip' => $nama_brg_wip,
				'qty' => $qtytotal,
				'keterangan' => $ket_detail
			);
			$this->db->insert('tm_sjmasukbhnbakupic_detail', $data_detail);
			// ================ end insert item detail ===========

			// ambil id detail tm_sjmasukbhnbakupic_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail ORDER BY id DESC LIMIT 1 ");
			if ($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			} else
				$iddetail = 0;

			// ----------------------------------------------
			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);

				$seq_warna	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail_warna ORDER BY id DESC LIMIT 1 ");

				if ($seq_warna->num_rows() > 0) {
					$seqrow	= $seq_warna->row();
					$idbaru	= $seqrow->id + 1;
				} else {
					$idbaru	= 1;
				}

				$tm_sjmasukbhnbakupic_detail_warna	= array(
					'id' => $idbaru,
					'id_sjmasukbhnbakupic_detail' => $iddetail,
					'id_warna' => $id_warna[$xx],
					'qty' => $qty_warna[$xx]
				);
				$this->db->insert('tm_sjmasukbhnbakupic_detail_warna', $tm_sjmasukbhnbakupic_detail_warna);

				// ========================= 04-02-2014, stok per warna ===============================================
				//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE id_warna = '" . $id_warna[$xx] . "'
								AND id_stok_hasil_cutting='$id_stok' ");
				if ($query3->num_rows() == 0) {
					$stok_warna_lama = 0;
				} else {
					$hasilrow = $query3->row();
					$stok_warna_lama	= $hasilrow->stok;
				}
				$new_stok_warna = $stok_warna_lama + $qty_warna[$xx];

				if ($query3->num_rows() == 0) { // jika blm ada data di tm_stok_hasil_cutting_warna, insert
					$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
					if ($seq_stokwarna->num_rows() > 0) {
						$seq_stokwarnarow	= $seq_stokwarna->row();
						$id_stok_warna	= $seq_stokwarnarow->id + 1;
					} else {
						$id_stok_warna	= 1;
					}

					$data_stok = array(
						'id' => $id_stok_warna,
						'id_stok_hasil_cutting' => $id_stok,
						'id_warna' => $id_warna[$xx],
						'stok' => $new_stok_warna,
						'tgl_update_stok' => $tgl
					);
					$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
				} else {
					$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
							where id_warna= '" . $id_warna[$xx] . "' AND id_stok_hasil_cutting='$id_stok' ");
				}

				// ----------------------- stok unit jahit -------------------------------------------
				//update stok unit jahit
				//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
				$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna 
								WHERE id_warna = '" . $id_warna[$xx] . "'
								AND id_stok_unit_jahit='$id_stok_unit' ");
				if ($query3->num_rows() == 0) {
					$stok_unit_warna_lama = 0;
					$stok_unit_warna_bagus_lama = 0;
				} else {
					$hasilrow = $query3->row();
					$stok_unit_warna_lama	= $hasilrow->stok;
					$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
				}
				$new_stok_unit_warna = $stok_unit_warna_lama - $qty_warna[$xx]; // berkurang stok karena keluar unit
				$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama - $qty_warna[$xx];

				if ($query3->num_rows() == 0) { // jika blm ada data di tm_stok_unit_jahit_warna, insert
					$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
					if ($seq_stokunitwarna->num_rows() > 0) {
						$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
						$id_stok_unit_warna	= $seq_stokunitwarnarow->id + 1;
					} else {
						$id_stok_unit_warna	= 1;
					}

					$data_stok = array(
						'id' => $id_stok_unit_warna,
						'id_stok_unit_jahit' => $id_stok_unit,
						'id_warna' => $id_warna[$xx],
						'stok' => $new_stok_unit_warna,
						'stok_bagus' => $new_stok_unit_warna_bagus,
						'tgl_update_stok' => $tgl
					);
					$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
				} else {
					$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
							where id_warna= '" . $id_warna[$xx] . "' AND id_stok_unit_jahit='$id_stok_unit' ");
				}
				// ------------------------------------------------------------------------------------------

			} // end for

		} // end perulangan
		//die();
		if ($carinya == '') $carinya = "all";
		$url_redirectnya = "pengadaan/cform/viewsjmasuk/" . $tgl_awal . "/" . $tgl_akhir . "/" . $id_unit_jahit . "/" . $carinya . "/" . $caribrg . "/" . $filterbrg . "/" . $cur_page;

		redirect($url_redirectnya);
	}

	// 06-03-2014 -----------------------------
	function editwarnabrgjadi()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		// 1. pilih warna2 yg baru
		$data['msg'] = '';
		$data['list_warna'] = $this->mmaster->get_warna();
		$data['isi'] = 'pengadaan/vform1editwarna';
		$this->load->view('template', $data);
	}
	//-----------------------------------------

	function editwarnabrgjadi2()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}
		// 2. save warna2 baru,
		//lalu isikan stok2 yg baru berdasarkan warna. 
		//munculkan stok dari warna2 lama, trus ada inputan utk masukkan stok hasil jahit baru (di perusahaan) pake warna2 baru
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$jumwarna	= $this->input->post('jumwarna');
		$kodewarna = "";

		for ($i = 1; $i <= $jumwarna; $i++) {
			$kodewarna	.= $this->input->post('kode_warna_' . $i) . ";";
		}

		$this->mmaster->savetempwarnabrgjadi($kode_brg_jadi, $kodewarna);

		$query4	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif='$kode_brg_jadi' ");
		if ($query4->num_rows() > 0) {
			$hasilrow2	= $query4->row();
			$nama_brg_jadi	= $hasilrow2->e_product_motifname;
		} else {
			$nama_brg_jadi	= '';
		}
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['list_stok_warna'] = $this->mmaster->get_stok_hasil_jahit_warna($kode_brg_jadi);
		$data['list_warna'] = $this->mmaster->get_tempwarnabrgjadi_bygudang($kode_brg_jadi);
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vform2editwarna';
		$this->load->view('template', $data);

		// next: rubah warna di tiap2 relasi ke bhn baku berdasarkan brg jadi (di tabel material_brg_jadi). ini di paling belakang aja setelah stok hasil jahit dan unit jahit
		//$data['list_stok_hasil_cutting_bhnbaku'] = $this->mmaster->get_stok_hasil_cutting_bhnbaku($kode_brg_jadi);
		//$data['list_bhnbaku'] = $this->mmaster->get_bhnbakubrgjadi($kode_brg_jadi);
	}

	// 07-03-2014
	function editwarnabrgjadi3()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}
		// 3. save stok hasil jahit warna2 baru ke tm_temp_stok_hasil_jahit_warna
		//munculkan stok unit jahit dari warna2 lama berdasarkan brg jadi tsb, 
		//trus ada inputan utk masukkan stok unit jahit baru pake warna2 baru

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$jumgudang	= $this->input->post('jumgudang');
		$adastok	= $this->input->post('adastok');

		$this->db->query(" DELETE FROM tm_temp_stok_hasil_jahit_warna WHERE kode_brg_jadi = '$kode_brg_jadi' ");
		if ($adastok == "ada") {
			for ($i = 1; $i <= $jumgudang; $i++) {
				$id_gudang = $this->input->post('id_gudang_' . $i, TRUE);
				$jumwarna = $this->input->post('jumwarna_' . $i, TRUE);
				$kode_warna = $this->input->post('kode_warna_' . $i, TRUE);
				$qty_warna = $this->input->post('qty_warna_' . $i, TRUE);

				for ($xx = 0; $xx < count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
					//$qtytotal+= $qty_warna[$xx];

					//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
					$sqlcek	= $this->db->query(" SELECT id FROM tm_temp_stok_hasil_jahit_warna 
										 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_warna = '" . $kode_warna[$xx] . "'
										 AND id_gudang='$id_gudang' ");
					if ($sqlcek->num_rows() == 0) {
						$query2	= $this->db->query(" SELECT id FROM tm_temp_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if ($query2->num_rows() > 0) {
							$hasilrow = $query2->row();
							$id_stok	= $hasilrow->id;
							$new_id_stok = $id_stok + 1;
						} else
							$new_id_stok = 1;

						$datanya	= array(
							'id' => $new_id_stok,
							'stok' => $qty_warna[$xx],
							'kode_brg_jadi' => $kode_brg_jadi,
							'kode_warna' => $kode_warna[$xx],
							'id_gudang' => $id_gudang
						);
						$this->db->insert('tm_temp_stok_hasil_jahit_warna', $datanya);
					} // end if

				} // end for

			} // end for
		} // end if

		$query4	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif='$kode_brg_jadi' ");
		if ($query4->num_rows() > 0) {
			$hasilrow2	= $query4->row();
			$nama_brg_jadi	= $hasilrow2->e_product_motifname;
		} else {
			$nama_brg_jadi	= '';
		}
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['list_stok_warna'] = $this->mmaster->get_stok_unit_jahit_warna($kode_brg_jadi);
		$data['list_warna'] = $this->mmaster->get_tempwarnabrgjadi_byunit($kode_brg_jadi);
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vform3editwarna';
		$this->load->view('template', $data);
	}

	function editwarnabrgjadi4()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}
		// 4. save stok unit jahit warna2 baru ke tm_temp_stok_unit_jahit_warna
		//munculkan data2 bhn baku yg ada acuan warna dari material_brg_jadi berdasarkan brg jadi tsb, 
		//trus ada inputan utk milih warna per bhn baku pake warna2 baru.

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$jumunit	= $this->input->post('jumunit');
		$adastok	= $this->input->post('adastok');

		$this->db->query(" DELETE FROM tm_temp_stok_unit_jahit_warna WHERE kode_brg_jadi = '$kode_brg_jadi' ");
		if ($adastok == "ada") {
			for ($i = 1; $i <= $jumunit; $i++) {
				$kode_unit = $this->input->post('kode_unit_' . $i, TRUE);
				//$jumunit = $this->input->post('jumunit_'.$i, TRUE);
				$kode_warna = $this->input->post('kode_warna_' . $i, TRUE);
				$qty_warna_bagus = $this->input->post('qty_warna_bagus_' . $i, TRUE);
				$qty_warna_perbaikan = $this->input->post('qty_warna_perbaikan_' . $i, TRUE);

				for ($xx = 0; $xx < count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna_bagus[$xx] = trim($qty_warna_bagus[$xx]);
					$qty_warna_perbaikan[$xx] = trim($qty_warna_perbaikan[$xx]);
					//$qtytotal+= $qty_warna[$xx];
					$totalxx = $qty_warna_bagus[$xx] + $qty_warna_perbaikan[$xx];

					//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
					$sqlcek	= $this->db->query(" SELECT id FROM tm_temp_stok_unit_jahit_warna 
										 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_warna = '" . $kode_warna[$xx] . "'
										 AND kode_unit='$kode_unit' ");
					if ($sqlcek->num_rows() == 0) {
						$query2	= $this->db->query(" SELECT id FROM tm_temp_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if ($query2->num_rows() > 0) {
							$hasilrow = $query2->row();
							$id_stok	= $hasilrow->id;
							$new_id_stok = $id_stok + 1;
						} else
							$new_id_stok = 1;

						$datanya	= array(
							'id' => $new_id_stok,
							'stok' => $totalxx,
							'stok_bagus' => $qty_warna_bagus[$xx],
							'stok_perbaikan' => $qty_warna_perbaikan[$xx],
							'kode_brg_jadi' => $kode_brg_jadi,
							'kode_warna' => $kode_warna[$xx],
							'kode_unit' => $kode_unit
						);
						$this->db->insert('tm_temp_stok_unit_jahit_warna', $datanya);
					} // end if

				} // end for
			} // end for
		} // end if

		$query4	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif='$kode_brg_jadi' ");
		if ($query4->num_rows() > 0) {
			$hasilrow2	= $query4->row();
			$nama_brg_jadi	= $hasilrow2->e_product_motifname;
		} else {
			$nama_brg_jadi	= '';
		}
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['list_material_bhnbaku'] = $this->mmaster->get_material_bhnbaku($kode_brg_jadi);
		$data['list_warna'] = $this->mmaster->get_tempwarnabrgjadi($kode_brg_jadi);
		//print_r($data['list_warna']); die();
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vform4editwarna';
		$this->load->view('template', $data);

		// next: rubah warna di tiap2 relasi ke bhn baku berdasarkan brg jadi (di tabel material_brg_jadi). ini di paling belakang aja setelah stok hasil jahit dan unit jahit
		//$data['list_stok_hasil_cutting_bhnbaku'] = $this->mmaster->get_stok_hasil_cutting_bhnbaku($kode_brg_jadi);
		//$data['list_bhnbaku'] = $this->mmaster->get_bhnbakubrgjadi($kode_brg_jadi);
	}

	// 08-03-2014
	function editwarnabrgjadi5()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}
		// 5. save warna2 baru yg ada relasi ke tabel material brg jadi ke tm_temp_material_brg_jadi
		//munculkan data2 stok hasil cutting bhn baku dan hasil cutting warna
		//trus ada inputan utk input stok hasil cutting baru pake warna2 baru.

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$jumbhnbaku	= $this->input->post('jumbhnbaku');
		$adawarna	= $this->input->post('adawarna');

		$this->db->query(" DELETE FROM tm_temp_material_brg_jadi WHERE kode_brg_jadi = '$kode_brg_jadi' ");
		if ($adawarna == "ada") {
			for ($i = 1; $i <= $jumbhnbaku; $i++) {
				$kode_brg = $this->input->post('kode_brg_' . $i, TRUE);
				$kode_warna = $this->input->post('kode_warna_' . $i, TRUE);

				$sqlcek	= $this->db->query(" SELECT id FROM tm_temp_material_brg_jadi 
										 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_warna = '" . $kode_warna . "'
										 AND kode_brg='$kode_brg' ");
				if ($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_temp_material_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if ($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_material	= $hasilrow->id;
						$new_id_material = $id_material + 1;
					} else
						$new_id_material = 1;

					$datanya	= array(
						'id' => $new_id_material,
						'kode_brg_jadi' => $kode_brg_jadi,
						'kode_warna' => $kode_warna,
						'kode_brg' => $kode_brg
					);
					$this->db->insert('tm_temp_material_brg_jadi', $datanya);
				} // end if

			} // end for
		} // end if adawarna

		$query4	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif='$kode_brg_jadi' ");
		if ($query4->num_rows() > 0) {
			$hasilrow2	= $query4->row();
			$nama_brg_jadi	= $hasilrow2->e_product_motifname;
		} else {
			$nama_brg_jadi	= '';
		}
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['list_stok_warna'] = $this->mmaster->get_stok_hasil_cutting_warna($kode_brg_jadi);
		$data['list_stok_bhnbaku'] = $this->mmaster->get_stok_hasil_cutting_bhnbaku($kode_brg_jadi);
		$data['list_material_bhnbaku'] = $this->mmaster->get_tempmaterialbrgjadi($kode_brg_jadi);
		$data['list_warna'] = $this->mmaster->get_tempwarnabrgjadi_hasilcutting($kode_brg_jadi);
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vform5editwarna';
		$this->load->view('template', $data);
	}

	// 10-03-2014
	function editwarnabrgjadi6()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}
		// 6. save semua data2 baru yg ada di tabel temp ke tabel yg sesungguhnya
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$jumstokwarna	= $this->input->post('jumstokwarna');
		$jumstokbhnbaku	= $this->input->post('jumstokbhnbaku');

		$adastokwarna	= $this->input->post('adastokwarna');
		$adastokbhnbaku	= $this->input->post('adastokbhnbaku');

		$this->db->query(" DELETE FROM tm_temp_stok_hasil_cutting_warna WHERE kode_brg_jadi = '$kode_brg_jadi' ");
		$this->db->query(" DELETE FROM tm_temp_stok_hasil_cutting_bhnbaku WHERE kode_brg_jadi = '$kode_brg_jadi' ");

		// stok hasil cutting warna
		if ($adastokwarna == "ada") {
			for ($i = 1; $i <= $jumstokwarna; $i++) {
				$qty_warna = $this->input->post('qty_warna_' . $i, TRUE);
				$kode_warna = $this->input->post('kode_warna_' . $i, TRUE);

				$sqlcek	= $this->db->query(" SELECT id FROM tm_temp_stok_hasil_cutting_warna 
										 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_warna = '" . $kode_warna . "' ");
				if ($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_temp_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
					if ($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_stok	= $hasilrow->id;
						$new_id_stok = $id_stok + 1;
					} else
						$new_id_stok = 1;

					$datanya	= array(
						'id' => $new_id_stok,
						'kode_brg_jadi' => $kode_brg_jadi,
						'kode_warna' => $kode_warna,
						'stok' => $qty_warna
					);
					$this->db->insert('tm_temp_stok_hasil_cutting_warna', $datanya);
				} // end if

			} // end for
		} // end if adawarna

		// stok hasil cutting bhnbaku
		if ($adastokbhnbaku == "ada") {
			for ($i = 1; $i <= $jumstokbhnbaku; $i++) {
				$qty_brg = $this->input->post('qty_brg_' . $i, TRUE);
				$kode_brg = $this->input->post('kode_brg_' . $i, TRUE);
				$kode_warna = $this->input->post('kode_warna_' . $i, TRUE);

				$sqlcek	= $this->db->query(" SELECT id FROM tm_temp_stok_hasil_cutting_bhnbaku 
										 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg='$kode_brg' 
										AND kode_warna = '" . $kode_warna . "' ");
				if ($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_temp_stok_hasil_cutting_bhnbaku ORDER BY id DESC LIMIT 1 ");
					if ($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_stok	= $hasilrow->id;
						$new_id_stok = $id_stok + 1;
					} else
						$new_id_stok = 1;

					$datanya	= array(
						'id' => $new_id_stok,
						'kode_brg_jadi' => $kode_brg_jadi,
						'kode_warna' => $kode_warna,
						'kode_brg' => $kode_brg,
						'stok' => $qty_brg
					);
					$this->db->insert('tm_temp_stok_hasil_cutting_bhnbaku', $datanya);
				} // end if

			} // end for
		} // end if adawarna

		$this->mmaster->saveallpengubahanwarna($kode_brg_jadi);

		$query4	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif='$kode_brg_jadi' ");
		if ($query4->num_rows() > 0) {
			$hasilrow2	= $query4->row();
			$nama_brg_jadi	= $hasilrow2->e_product_motifname;
		} else {
			$nama_brg_jadi	= '';
		}
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		/*	$data['list_stok_warna'] = $this->mmaster->get_stok_hasil_cutting_warna($kode_brg_jadi);
		$data['list_stok_bhnbaku'] = $this->mmaster->get_stok_hasil_cutting_bhnbaku($kode_brg_jadi);
		$data['list_material_bhnbaku'] = $this->mmaster->get_tempmaterialbrgjadi($kode_brg_jadi);
		$data['list_warna'] = $this->mmaster->get_tempwarnabrgjadi_hasilcutting($kode_brg_jadi); */
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vformselesaieditwarna';
		$this->load->view('template', $data);
	}

	function show_popup_brgjadi_editwarna()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$keywordcari	= $this->input->post('cari', TRUE);
		if ($keywordcari == '') {
			$keywordcari 	= $this->uri->segment(4);
		}
		if ($keywordcari == '')
			$keywordcari = "all";

		$qjum_total = $this->mmaster->getbarangjaditanpalimit($keywordcari);

		$config['base_url'] 	= base_url() . "index.php/pengadaan/cform/show_popup_brgjadi_editwarna/" . $keywordcari . "/";
		$config['total_rows'] 	= count($qjum_total);
		$config['per_page'] 	= 10;
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page']		= $this->uri->segment(5, 0);
		$this->pagination->initialize($config);

		$data['query']		= $this->mmaster->getbarangjadi($config['per_page'], $config['cur_page'], $keywordcari);
		$data['jum_total']	= count($qjum_total);

		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$data['startnya']	= $config['cur_page'];
		$this->load->view('pengadaan/vpopupbrgjadi_editwarna', $data);
	}

	function showwarnaasal()
	{

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);

		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '" . $kode_brg_jadi . "' ");
		if ($queryxx->num_rows() > 0) {
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(
					'kode_warna' => $rowxx->kode_warna,
					'nama_warna' => $rowxx->nama
				);
			}
		} else
			$detailwarna = '';

		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$this->load->view('pengadaan/vshowwarnaasal', $data);
		return true;
	}

	// 04-03-2014 -----------
	// skrip utk konversi id_warna_brg_jadi ke kode_warna di tabel tm_bonmkeluarcutting_detail_warna, tm_sjmasukbhnbakupengadaan_detail_warna, 
	//tm_stok_hasil_cutting_warna, tm_stok_unit_jahit_warna. Relasi ke tabel tm_warna_brg_jadi dan tm_warna 
	function editidwarnabrgjadi2kodewarna()
	{
		// 1. query ke tm_bonmkeluarcutting_detail_warna
		$sqlxx = $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail_warna ORDER BY id ASC ");
		if ($sqlxx->num_rows() > 0) {
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$id_warna_brg_jadi = $rowxx->id_warna_brg_jadi;
				//----------------------------------------------------------
				$sqlxx2	= $this->db->query(" SELECT b.kode FROM tm_warna_brg_jadi a, tm_warna b WHERE a.kode_warna = b.kode 
									AND a.id = '$id_warna_brg_jadi' ");
				if ($sqlxx2->num_rows() > 0) {
					$hasilxx2 = $sqlxx2->row();
					$kode_warna	= $hasilxx2->kode;

					$this->db->query(" UPDATE tm_bonmkeluarcutting_detail_warna SET kode_warna = '$kode_warna', id_warna_brg_jadi = '0' 
								 WHERE id='$id' ");
				}
				//----------------------------------------------------------
			} // end foreach
			echo "edit SJ Keluar Hasil Cutting detail warna sukses <br>";
			//echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
		}

		// 2. query ke tm_sjmasukbhnbakupengadaan_detail_warna
		$sqlxx = $this->db->query(" SELECT * FROM tm_sjmasukbhnbakupengadaan_detail_warna ORDER BY id ASC ");
		if ($sqlxx->num_rows() > 0) {
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$id_warna_brg_jadi = $rowxx->id_warna_brg_jadi;
				//----------------------------------------------------------
				$sqlxx2	= $this->db->query(" SELECT b.kode FROM tm_warna_brg_jadi a, tm_warna b WHERE a.kode_warna = b.kode 
									AND a.id = '$id_warna_brg_jadi' ");
				if ($sqlxx2->num_rows() > 0) {
					$hasilxx2 = $sqlxx2->row();
					$kode_warna	= $hasilxx2->kode;

					$this->db->query(" UPDATE tm_sjmasukbhnbakupengadaan_detail_warna SET kode_warna = '$kode_warna', id_warna_brg_jadi = '0' 
								 WHERE id='$id' ");
				}
				//----------------------------------------------------------
			} // end foreach
			echo "edit sj masuk retur bhn baku detail warna sukses <br>";
			//echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
		}

		// 3. query ke tm_stok_hasil_cutting_warna
		$sqlxx = $this->db->query(" SELECT * FROM tm_stok_hasil_cutting_warna ORDER BY id ASC ");
		if ($sqlxx->num_rows() > 0) {
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$id_warna_brg_jadi = $rowxx->id_warna_brg_jadi;
				//----------------------------------------------------------
				$sqlxx2	= $this->db->query(" SELECT b.kode FROM tm_warna_brg_jadi a, tm_warna b WHERE a.kode_warna = b.kode 
									AND a.id = '$id_warna_brg_jadi' ");
				if ($sqlxx2->num_rows() > 0) {
					$hasilxx2 = $sqlxx2->row();
					$kode_warna	= $hasilxx2->kode;

					$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET kode_warna = '$kode_warna', id_warna_brg_jadi = '0' 
								 WHERE id='$id' ");
				}
				//----------------------------------------------------------
			} // end foreach
			echo "edit stok hasil cutting warna sukses <br>";
			//echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
		}

		// 4. query ke tm_stok_unit_jahit_warna
		$sqlxx = $this->db->query(" SELECT * FROM tm_stok_unit_jahit_warna ORDER BY id ASC ");
		if ($sqlxx->num_rows() > 0) {
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$id_warna_brg_jadi = $rowxx->id_warna_brg_jadi;
				//----------------------------------------------------------
				$sqlxx2	= $this->db->query(" SELECT b.kode FROM tm_warna_brg_jadi a, tm_warna b WHERE a.kode_warna = b.kode 
									AND a.id = '$id_warna_brg_jadi' ");
				if ($sqlxx2->num_rows() > 0) {
					$hasilxx2 = $sqlxx2->row();
					$kode_warna	= $hasilxx2->kode;

					$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET kode_warna = '$kode_warna', id_warna_brg_jadi = '0' 
								 WHERE id='$id' ");
				}
				//----------------------------------------------------------
			} // end foreach
			echo "edit stok unit jahit warna sukses <br>";
			echo "<a href='" . base_url() . "'>Kembali ke halaman depan</a>";
		}
	}
	//-----------------------

	// 25-03-2014
	function caribrgwip()
	{
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);

		// query ke tabel brg jadi utk ambil kode, nama, dan stok
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip WHERE kode_brg = '" . $kode_brg_wip . "' ");

		if ($queryxx->num_rows() > 0) {
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		} else {
			$id_brg_wip = '';
			$nama_brg_wip = '';
		}

		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('pengadaan/vinfobrgwip', $data);
		return true;
	}

	// 08-04-2014
	function ceknosj()
	{
		$no_sj 	= $this->input->post('no_sj', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sj', TRUE);
		$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);

		$pisah1 = explode("-", $tgl_sj);
		$tgl1 = $pisah1[0];
		$bln1 = $pisah1[1];
		$thn1 = $pisah1[2];

		$cek_data = $this->mmaster->cek_data_sjmasuk($no_sj, $thn1, $id_unit_jahit);
		if (count($cek_data) > 0)
			$ada = "ada";
		else
			$ada = "tidak";

		echo $ada;
		return true;
	}

	// 10-10-2015
	function viewbonmkeluarcutting()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}

		$keywordcari 	= $this->input->post('cari', TRUE);
		$date_from 	= $this->input->post('date_from', TRUE);
		$date_to 	= $this->input->post('date_to', TRUE);
		$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);
		$filterbrg 	= $this->input->post('filter_brg', TRUE);
		$caribrg 	= $this->input->post('cari_brg', TRUE);

		// 24-02-2015
		$submit2 	= $this->input->post('submit2', TRUE);

		if ($date_from == '')
			$date_from = $this->uri->segment(4);
		if ($date_to == '')
			$date_to = $this->uri->segment(5);
		if ($id_unit_jahit == '0')
			$id_unit_jahit = $this->uri->segment(6);
		if ($keywordcari == '')
			$keywordcari = $this->uri->segment(7);
		if ($caribrg == '')
			$caribrg = $this->uri->segment(8);
		if ($filterbrg == '')
			$filterbrg = $this->uri->segment(9);

		if ($keywordcari == '')
			$keywordcari 	= "all";
		if ($date_from == '')
			$date_from = "00-00-0000";
		if ($date_to == '')
			$date_to = "00-00-0000";
		if ($id_unit_jahit == '')
			$id_unit_jahit = '0';
		if ($caribrg == '')
			$caribrg = "all";
		if ($filterbrg == '')
			$filterbrg = "n";

		//if ($submit2 == "") {
		$jum_total = $this->mmaster->getAllbonmkeluarcuttingtanpalimit($keywordcari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg);

		$config['base_url'] = base_url() . 'index.php/pengadaan/cform/viewbonmkeluarcutting/' . $date_from . '/' . $date_to . '/' . $id_unit_jahit . '/' . $keywordcari . '/' . $caribrg . '/' . $filterbrg;
		$config['total_rows'] = count($jum_total);
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(10);
		$this->pagination->initialize($config);
		$data['query'] = $this->mmaster->getAllbonmkeluarcutting($config['per_page'], $this->uri->segment(10), $keywordcari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;

		$data['isi'] = 'pengadaan/vformviewbonmkeluarcutting';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		if ($date_from == "00-00-0000")
			$data['date_from'] = '';
		else
			$data['date_from'] = $date_from;

		if ($date_to == "00-00-0000")
			$data['date_to'] = '';
		else
			$data['date_to'] = $date_to;

		if ($caribrg == "all")
			$data['caribrg'] = '';
		else
			$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$this->load->view('template', $data);
		//	}
		/*else {
		// export excel
		$query = $this->mmaster->getAllbonmkeluarcutting_export($keywordcari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='10' align='center'>DATA SJ KELUAR BAHAN BAKU (HASIL CUTTING) KE JAHITAN</th>
				 </tr>
				</table><br>";
				
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th width='3%'>No</th>
					 <th width='10%'>No Bon M / SJ</th>
					 <th width='10%'>No Manual</th>
					 <th width='8%'>Tgl Bon M</th>
					 <th width='10%'>Jenis</th>
					 <th width='10%'>Unit Jahit</th>
					 <th width='15%'>List Barang Jadi</th>
					 <th width='5%'>Qty</th>
					 <th width='15%'>Qty Ket Warna</th>
					 <th>Last Update</th>
				 </tr>
				</thead>
				<tbody>";
				
		if (is_array($query)) {
			$no=1;
			for($j=0;$j<count($query);$j++){

				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";

				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;

				$pisah1 = explode("-", $query[$j]['tgl_bonm']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_bonm = $tgl1." ".$nama_bln." ".$thn1;
				 
				 $html_data.= "<tr>";
				 $html_data.=    "<td>".$no."</td>";
				 $html_data.=    "<td>".$query[$j]['no_bonm']."</td>";
				 $html_data.=    "<td>".$query[$j]['no_manual']."</td>";
				 $html_data.=    "<td>".$tgl_bonm."</td>";
				 $html_data.=    "<td>".$query[$j]['nama_jenis']."</td>";
				 
				$html_data.=    "<td>".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nama_brg_jadi'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 				 
				 $html_data.= "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['ket_qty_warna'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				  
				 $html_data.=    "<td align='center'>".$tgl_update."</td>";
				 				 
				 $html_data.=  "</tr>";
				 $no++;
			} // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "data_sj_keluar_bhnbaku_hasil_cutting_jahitan";
		$export_excel1 = '1';
		//if ($export_excel1 != '')
			$nama_file.= ".xls";
		//else
		//	$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
	} */
	}

	function submitbonmkeluarcutting()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		//$no_bonm 	= $this->input->post('no_bonm', TRUE);
		$tgl_bonm 				= $this->input->post('tgl_bonm', TRUE);
		$no_bonm_manual 		= $this->input->post('no_bonm_manual', TRUE);
		$jenis 					= $this->input->post('jenis', TRUE);
		$id_unit_jahit 			= $this->input->post('id_unit_jahit', TRUE);
		$ket 					= $this->input->post('ket', TRUE);
		$pisah1 				= explode("-", $tgl_bonm);
		$bln_forecast 			= $this->input->post('bln_forecast', TRUE);
		$thn_forecast 			= $this->input->post('thn_forecast', TRUE);
		$tgl1 = $pisah1[0];
		$bln1 = $pisah1[1];
		$thn1 = $pisah1[2];
		$tgl_bonm = $thn1 . "-" . $bln1 . "-" . $tgl1;

		/* TAMBAHAN 05 AGS 2023 */
		$esender 			= $this->input->post('esender', TRUE);
		$esendercompany 	= $this->input->post('esendercompany', TRUE);
		$erecipient 		= $this->input->post('erecipient', TRUE);
		$erecipientcompany 	= $this->input->post('erecipientcompany', TRUE);
		/* ******************** */

		$no 	= $this->input->post('no', TRUE);
		$jumlah_input = $no - 1;

		$cek_data = $this->mmaster->cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $id_unit_jahit, $jenis);
		if (count($cek_data) > 0) {
			$data['isi'] = 'pengadaan/vform1bonmkeluarcutting';
			$data['msg'] = "Data no bon M " . $no_bonm_manual . " utk tahun $thn1 sudah ada..!";

			/*$th_now	= date("Y");
				$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluarcutting ORDER BY no_bonm DESC LIMIT 1 ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) 
					$no_bonm	= $hasilrow->no_bonm;
				else
					$no_bonm = '';
				if(strlen($no_bonm)==9) {
					$nobonm = substr($no_bonm, 0, 9);
					$n_bonm	= (substr($nobonm,4,5))+1;
					$th_bonm	= substr($nobonm,0,4);
					if($th_now==$th_bonm) {
							$jml_n_bonm	= $n_bonm;
							switch(strlen($jml_n_bonm)) {
								case "1": $kodebonm	= "0000".$jml_n_bonm;
								break;
								case "2": $kodebonm	= "000".$jml_n_bonm;
								break;	
								case "3": $kodebonm	= "00".$jml_n_bonm;
								break;
								case "4": $kodebonm	= "0".$jml_n_bonm;
								break;
								case "5": $kodebonm	= $jml_n_bonm;
								break;	
							}
							$nomorbonm = $th_now.$kodebonm;
					}
					else {
						$nomorbonm = $th_now."00001";
					}
				}
				else {
					$nomorbonm	= $th_now."00001";
				}
				
				$data['no_bonm'] = $nomorbonm; */
			$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
			$this->load->view('template', $data);
		} else {
			$tgl = date("Y-m-d H:i:s");

			$uid_update_by = $this->session->userdata('uid');

			$uid_approve = $this->db->query(" SELECT uid_sjp_approve FROM tm_user WHERE uid = '$uid_update_by' ")->row()->uid_sjp_approve;
			// insert di tm_bonmkeluarcutting
			$data_header = array(
				//'no_bonm'=>$no_bonm,
				'no_manual' => $no_bonm_manual,
				'tgl_bonm' => $tgl_bonm,
				'jenis_keluar' => $jenis,
				'id_unit' => $id_unit_jahit,
				'tgl_input' => $tgl,
				'tgl_update' => $tgl,
				'keterangan' => $ket,
				'uid_update_by' => $uid_update_by,
				'bln_forecast' => $bln_forecast,
				/* TAMBAHAN 05 AGS 2023 */
				'e_sender' => $esender,
				'e_sender_company' => $esendercompany,
				'e_recipient' => $erecipient,
				'e_recipient_company' => $erecipientcompany,
				/* ******************* */
				'thn_forecast' => $thn_forecast,
				'uid' => $uid_update_by,
				'uid_approve' => $uid_approve

			);
			$this->db->insert('tm_bonmkeluarcutting', $data_header);

			// ambil data terakhir di tabel tm_bonmkeluarcutting
			$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_bonm	= $hasilrow->id;

			for ($i = 1; $i <= $jumlah_input; $i++) {
				/*$this->mmaster->savebonmkeluarcutting($no_bonm,$tgl_bonm, $no_bonm_manual, $jenis, $kode_unit_jahit, $ket, 
							$this->input->post('kode_brg_jadi_'.$i, TRUE), 
								//$this->input->post('qty_'.$i, TRUE), 
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) ); */

				$this->mmaster->savebonmkeluarcutting(
					$id_bonm,
					$jenis,
					$id_unit_jahit,
					$this->input->post('id_brg_wip_' . $i, TRUE),
					$this->input->post('brg_wip_' . $i, TRUE),
					$this->input->post('temp_qty_' . $i, TRUE),
					$this->input->post('id_warna_' . $i, TRUE),
					$this->input->post('qty_warna_' . $i, TRUE),
					$this->input->post('ket_detail_' . $i, TRUE)
				);
			}
			redirect('pengadaan/cform/viewbonmkeluarcutting');
		}
	}

	function deletebonmkeluarcutting()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}

		$id 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$tgl_awal 	= $this->uri->segment(6);
		$tgl_akhir 	= $this->uri->segment(7);
		$id_unit_jahit 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);
		$caribrg 	= $this->uri->segment(10);
		$filterbrg 	= $this->uri->segment(11);

		$this->mmaster->deletebonmkeluarcutting($id);

		if ($carinya == '') $carinya = "all";

		$url_redirectnya = "pengadaan/cform/viewbonmkeluarcutting/" . $tgl_awal . "/" . $tgl_akhir . "/" . $id_unit_jahit . "/" . $carinya . "/" . $caribrg . "/" . $filterbrg . "/" . $cur_page;
		redirect($url_redirectnya);
	}

	function viewdetail()
	{
		// =======================
		// disini coding utk pengecekan user login
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_bonm 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$tgl_awal 	= $this->uri->segment(6);
		$tgl_akhir 	= $this->uri->segment(7);
		$id_unit_jahit 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);
		$caribrg 	= $this->uri->segment(10);
		$filterbrg 	= $this->uri->segment(11);

		$data['cur_page'] = $cur_page;
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['carinya'] = $carinya;
		$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;

		$data['query'] = $this->mmaster->get_bonmkeluarcutting($id_bonm);
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vformviewdetail';
		$this->load->view('template', $data);
	}

	function editbonmkeluarcutting()
	{
		// =======================
		// disini coding utk pengecekan user login
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_bonm 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$tgl_awal 	= $this->uri->segment(6);
		$tgl_akhir 	= $this->uri->segment(7);
		$id_unit_jahit 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);
		$caribrg 	= $this->uri->segment(10);
		$filterbrg 	= $this->uri->segment(11);

		$data['cur_page'] = $cur_page;
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['carinya'] = $carinya;
		$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;

		$data['query'] = $this->mmaster->get_bonmkeluarcutting($id_bonm);
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/veditformbonmkeluarcutting';
		$this->load->view('template', $data);
	}

	function updatedatabonmkeluarcutting()
	{
		$is_logged_in 		= $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id_bonm		 	= $this->input->post('id_bonm', TRUE);
		$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
		$tgl_bonm 			= $this->input->post('tgl_bonm', TRUE);
		$ket 				= $this->input->post('ket', TRUE);
		$jenis 				= $this->input->post('jenis', TRUE);
		$id_unit 			= $this->input->post('id_unit', TRUE);
		$id_unit_lama 		= $this->input->post('id_unit_lama', TRUE);
		$bln_forecast 		= $this->input->post('bln_forecast', TRUE);
		$thn_forecast 		= $this->input->post('thn_forecast', TRUE);

		$pisah1 			= explode("-", $tgl_bonm);
		$tgl1 				= $pisah1[0];
		$bln1 				= $pisah1[1];
		$thn1 				= $pisah1[2];
		$tgl_bonm 			= $thn1 . "-" . $bln1 . "-" . $tgl1;
		$no 				= $this->input->post('no', TRUE);

		$cur_page 			= $this->input->post('cur_page', TRUE);
		$tgl_awal 			= $this->input->post('tgl_awal', TRUE);
		$tgl_akhir 			= $this->input->post('tgl_akhir', TRUE);
		$id_unit_jahit 		= $this->input->post('id_unit_jahit', TRUE);
		$carinya 			= $this->input->post('carinya', TRUE);
		$caribrg 			= $this->input->post('caribrg', TRUE);
		$filterbrg	 		= $this->input->post('filterbrg', TRUE);

		/* TAMBAHAN 05 AGS 2023  */
		$esender 			= $this->input->post('esender', TRUE);
		$esendercompany 	= $this->input->post('esendercompany', TRUE);
		$erecipient 		= $this->input->post('erecipient', TRUE);
		$erecipientcompany 	= $this->input->post('erecipientcompany', TRUE);
		/* ******************** */

		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');

		//update headernya
		$this->db->query(" 	UPDATE tm_bonmkeluarcutting SET tgl_bonm = '$tgl_bonm', jenis_keluar = '$jenis', bln_forecast = '$bln_forecast', thn_forecast = '$thn_forecast', 
							id_unit = '$id_unit', tgl_update='$tgl', no_manual='$no_bonm_manual', uid_update_by='$uid_update_by',
							keterangan = '$ket', d_notapprove = null, e_remark_notapprove = null,e_sender = '$esender', e_sender_company = '$esendercompany', e_recipient = '$erecipient',
							e_recipient_company = '$erecipientcompany' where id= '$id_bonm' ");

		//reset stok, dan hapus dulu detailnya
		$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail WHERE id_bonmkeluarcutting = '$id_bonm' ");
		if ($query2->num_rows() > 0) {
			$hasil2 = $query2->result();

			foreach ($hasil2 as $row2) {
				$qty = $row2->qty;

				// 1. stok total
				// ============ update stok PABRIK =====================
				//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$row2->id_brg_wip' ");
				if ($query3->num_rows() == 0) {
					$stok_lama = 0;
					$id_stok = 0;
				} else {
					$hasilrow = $query3->row();
					$id_stok	= $hasilrow->id;
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama + $row2->qty; // bertambah stok karena reset

				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg_wip= '$row2->id_brg_wip' ");

				//================ update stok UNIT JAHIT ===============================
				if ($kode_unit_lama != '0') {
					//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit
											WHERE id_brg_wip = '$row2->id_brg_wip' AND id_unit='$id_unit_lama' ");
					if ($query3->num_rows() == 0) {
						$id_stok_unit = 0;
						$stok_unit_lama = 0;
						$stok_unit_bagus_lama = 0;
					} else {
						$hasilrow = $query3->row();
						$id_stok_unit	= $hasilrow->id;
						$stok_unit_lama	= $hasilrow->stok;
						$stok_unit_bagus_lama = $hasilrow->stok_bagus;
					}
					$new_stok_unit = $stok_unit_lama - $row2->qty; // berkurang stok unit karena reset bon M keluar
					$new_stok_unit_bagus = $stok_unit_bagus_lama - $row2->qty;

					$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
										WHERE id_brg_wip= '$row2->id_brg_wip' AND id_unit = '$id_unit_lama' ");
				} // end if

				// 2. reset stok per warna dari tabel tm_bonmkeluarcutting_detail_warna
				$querywarna	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail_warna 
												WHERE id_bonmkeluarcutting_detail = '$row2->id' ");
				if ($querywarna->num_rows() > 0) {
					$hasilwarna = $querywarna->result();

					foreach ($hasilwarna as $rowwarna) {
						//============== update stok pabrik ===============================================
						//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND id_warna='$rowwarna->id_warna' ");
						if ($query3->num_rows() == 0) {
							$stok_warna_lama = 0;
						} else {
							$hasilrow = $query3->row();
							$stok_warna_lama	= $hasilrow->stok;
						}
						$new_stok_warna = $stok_warna_lama + $rowwarna->qty; // bertambah stok karena reset dari bon M keluar

						$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND id_warna = '$rowwarna->id_warna' ");

						// ============= update stok unit jahit
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND id_warna='$rowwarna->id_warna' ");
						if ($query3->num_rows() == 0) {
							$stok_unit_warna_lama = 0;
							$stok_unit_warna_bagus_lama = 0;
						} else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama - $rowwarna->qty; // berkurang stok di unit karena reset dari bon M keluar
						$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama - $rowwarna->qty;

						$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna',
												stok_bagus = '$new_stok_unit_warna_bagus',
												tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND id_warna = '$rowwarna->id_warna' ");
					}
				} // end if detail warna

				$this->db->query(" DELETE FROM tm_bonmkeluarcutting_detail_warna WHERE id_bonmkeluarcutting_detail = '$row2->id' ");

				// ============ update stok =====================
				// modif 27-01-2014, sementara ga dipake =
				//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
				/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting 
														WHERE kode_brg = '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'kode_brg_jadi'=>$row2->kode_brg_jadi,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_cutting', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' AND kode_brg_jadi= '$row2->kode_brg_jadi' ");
								} */
				// ==============================================
			} // end foreach detail
		} // end reset stok
		//=============================================
		$this->db->delete('tm_bonmkeluarcutting_detail', array('id_bonmkeluarcutting' => $id_bonm));

		$jumlah_input = $no - 1;
		for ($i = 1; $i <= $jumlah_input; $i++) {
			$id_brg_wip = $this->input->post('id_brg_wip_' . $i, TRUE);
			$nama_brg_wip = $this->input->post('brg_wip_' . $i, TRUE);
			//$qty = $this->input->post('qty_'.$i, TRUE);
			$ket_detail = $this->input->post('ket_detail_' . $i, TRUE);

			$temp_qty = $this->input->post('temp_qty_' . $i, TRUE);
			$id_warna = $this->input->post('id_warna_' . $i, TRUE);
			$qty_warna = $this->input->post('qty_warna_' . $i, TRUE);

			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotal = 0;
			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);
				$qtytotal += $qty_warna[$xx];
			} // end for
			// ---------------------------------------------------------------------

			// 1. stok total
			// ============================= update stok PABRIK ======================================
			//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
			$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$id_brg_wip' ");
			if ($query3->num_rows() == 0) {
				$id_stok = 0;
				$stok_lama = 0;
			} else {
				$hasilrow = $query3->row();
				$id_stok	= $hasilrow->id;
				$stok_lama	= $hasilrow->stok;
			}
			$new_stok = $stok_lama - $qtytotal;

			if ($query3->num_rows() == 0) { // jika blm ada data di tm_stok_hasil_cutting, insert
				$data_stok = array(
					'id_brg_wip' => $id_brg_wip,
					'stok' => $new_stok,
					'tgl_update_stok' => $tgl
				);
				$this->db->insert('tm_stok_hasil_cutting', $data_stok);

				// ambil id_stok utk dipake di stok warna
				$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
				if ($sqlxx->num_rows() > 0) {
					$hasilxx	= $sqlxx->row();
					$id_stok	= $hasilxx->id;
				} else {
					$id_stok	= 1;
				}
			} else {
				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg_wip= '$id_brg_wip' ");
			}

			// ============= update stok UNIT JAHIT ======================
			//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
			$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit WHERE id_brg_wip = '$id_brg_wip'
											AND id_unit='$id_unit' ");
			if ($query3->num_rows() == 0) {
				$id_stok_unit = 0;
				$stok_unit_lama = 0;
				$stok_unit_bagus_lama = 0;
			} else {
				$hasilrow = $query3->row();
				$id_stok_unit	= $hasilrow->id;
				$stok_unit_lama	= $hasilrow->stok;
				$stok_unit_bagus_lama = $hasilrow->stok_bagus;
			}
			$new_stok_unit = $stok_unit_lama + $qtytotal;
			$new_stok_unit_bagus = $stok_unit_bagus_lama + $qtytotal;

			if ($query3->num_rows() == 0) { // jika blm ada data di tm_stok_unit_jahit, insert
				$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
				if ($seqxx->num_rows() > 0) {
					$seqrowxx	= $seqxx->row();
					$id_stok_unit	= $seqrowxx->id + 1;
				} else {
					$id_stok_unit	= 1;
				}

				$data_stok = array(
					'id' => $id_stok_unit,
					'id_brg_wip' => $id_brg_wip,
					'stok' => $new_stok_unit,
					'stok_bagus' => $new_stok_unit_bagus,
					'id_unit' => $id_unit,
					'tgl_update_stok' => $tgl
				);
				$this->db->insert('tm_stok_unit_jahit', $data_stok);
			} else {
				$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
								stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
								where id_brg_wip= '$id_brg_wip' AND id_unit = '$id_unit' ");
			}
			// =====================================================================================

			// jika semua data tdk kosong, insert ke tm_bonmkeluarcutting_detail
			$data_detail = array(
				'id_bonmkeluarcutting' => $id_bonm,
				'id_brg_wip' => $id_brg_wip,
				'nama_brg_wip' => $nama_brg_wip,
				'qty' => $qtytotal,
				'keterangan' => $ket_detail
			);
			$this->db->insert('tm_bonmkeluarcutting_detail', $data_detail);
			// ================ end insert item detail ===========

			// ambil id detail tm_bonmkeluarcutting_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail ORDER BY id DESC LIMIT 1 ");
			if ($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			} else
				$iddetail = 0;

			// ----------------------------------------------
			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);

				$seq_warna	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail_warna ORDER BY id DESC LIMIT 1 ");

				if ($seq_warna->num_rows() > 0) {
					$seqrow	= $seq_warna->row();
					$idbaru	= $seqrow->id + 1;
				} else {
					$idbaru	= 1;
				}

				$tm_bonmkeluarcutting_detail_warna	= array(
					'id' => $idbaru,
					'id_bonmkeluarcutting_detail' => $iddetail,
					'id_warna' => $id_warna[$xx],
					'qty' => $qty_warna[$xx]
				);
				$this->db->insert('tm_bonmkeluarcutting_detail_warna', $tm_bonmkeluarcutting_detail_warna);

				// ========================= 04-02-2014, stok per warna ===============================================
				//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE id_warna = '" . $id_warna[$xx] . "'
								AND id_stok_hasil_cutting='$id_stok' ");
				if ($query3->num_rows() == 0) {
					$stok_warna_lama = 0;
				} else {
					$hasilrow = $query3->row();
					$stok_warna_lama	= $hasilrow->stok;
				}
				$new_stok_warna = $stok_warna_lama - $qty_warna[$xx];

				if ($query3->num_rows() == 0) { // jika blm ada data di tm_stok_hasil_cutting_warna, insert
					$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
					if ($seq_stokwarna->num_rows() > 0) {
						$seq_stokwarnarow	= $seq_stokwarna->row();
						$id_stok_warna	= $seq_stokwarnarow->id + 1;
					} else {
						$id_stok_warna	= 1;
					}

					$data_stok = array(
						'id' => $id_stok_warna,
						'id_stok_hasil_cutting' => $id_stok,
						'id_warna' => $id_warna[$xx],
						'stok' => $new_stok_warna,
						'tgl_update_stok' => $tgl
					);
					$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
				} else {
					$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
							where id_warna= '" . $id_warna[$xx] . "' AND id_stok_hasil_cutting='$id_stok' ");
				}

				// ----------------------- stok unit jahit -------------------------------------------
				//update stok unit jahit
				//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
				$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna WHERE id_warna = '" . $id_warna[$xx] . "'
								AND id_stok_unit_jahit='$id_stok_unit' ");
				if ($query3->num_rows() == 0) {
					$stok_unit_warna_lama = 0;
					$stok_unit_warna_bagus_lama = 0;
				} else {
					$hasilrow = $query3->row();
					$stok_unit_warna_lama	= $hasilrow->stok;
					$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
				}
				$new_stok_unit_warna = $stok_unit_warna_lama + $qty_warna[$xx]; // bertambah stok karena masuk ke unit
				$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama + $qty_warna[$xx];

				if ($query3->num_rows() == 0) { // jika blm ada data di tm_stok_unit_jahit_warna, insert
					$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
					if ($seq_stokunitwarna->num_rows() > 0) {
						$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
						$id_stok_unit_warna	= $seq_stokunitwarnarow->id + 1;
					} else {
						$id_stok_unit_warna	= 1;
					}

					$data_stok = array(
						'id' => $id_stok_unit_warna,
						'id_stok_unit_jahit' => $id_stok_unit,
						'id_warna' => $id_warna[$xx],
						'stok' => $new_stok_unit_warna,
						'stok_bagus' => $new_stok_unit_warna_bagus,
						'tgl_update_stok' => $tgl
					);
					$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
				} else {
					$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
							where id_warna= '" . $id_warna[$xx] . "' AND id_stok_unit_jahit='$id_stok_unit' ");
				}
				// ------------------------------------------------------------------------------------------

			} // end for
		} // end perulangan
		//die();
		if ($carinya == '') $carinya = "all";
		$url_redirectnya = "pengadaan/cform/viewbonmkeluarcutting/" . $tgl_awal . "/" . $tgl_akhir . "/" . $id_unit_jahit . "/" . $carinya . "/" . $caribrg . "/" . $filterbrg . "/" . $cur_page;

		redirect($url_redirectnya);
	}

	// 12-10-2015
	function addbonmkeluarcutting()
	{
		// =======================
		// disini coding utk pengecekan user login	
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		// 12-10-2015 dikomen
		$th_now	= date("Y");
		/*
	$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmkeluarcutting ORDER BY no_bonm DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==9) {
				$nobonm = substr($no_bonm, 0, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm;
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			} */

		$data['isi'] = 'pengadaan/vform1bonmkeluarcutting';
		$data['msg'] = '';
		//$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['thn_forecast'] = $th_now;
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		//$data['no_bonm'] = $nomorbonm;
		$this->load->view('template', $data);
	}

	// 15-10-2015
	function ceknobonm()
	{
		$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
		$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);
		$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);
		$jenis 	= $this->input->post('jenis', TRUE);

		$pisah1 = explode("-", $tgl_bonm);
		$tgl1 = $pisah1[0];
		$bln1 = $pisah1[1];
		$thn1 = $pisah1[2];

		$cek_data = $this->mmaster->cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $id_unit_jahit, $jenis);
		if (count($cek_data) > 0)
			$ada = "ada";
		else
			$ada = "tidak";

		echo $ada;
		return true;
	}

	function printstb()
	{
		$id = $this->input->get('id');

		if ($id == null) {
			redirect('pengadaan/cform/index');
		}

		// jika sudah pernah cetak 
		$status_print = $this->mmaster->get_status_print($id);
		if ($status_print == 't') {
			$js = "<script>window.close()</script>";

			echo $js;
			return;
		}

		$data = [
			'isi' => $this->mmaster->get_data_edit_header($id)->row(),
			'detail' => $this->mmaster->get_data_edit_detail($id)->result()
		];

		$this->load->view('pengadaan/vformprint', $data);
	}

	function updatecetak()
	{
		$id = $this->input->post('id');
		$this->mmaster->increment_cetak($id);
	}

	function approve()
	{
		// =======================
		// disini coding utk pengecekan user login
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_bonm 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$tgl_awal 	= $this->uri->segment(6);
		$tgl_akhir 	= $this->uri->segment(7);
		$id_unit_jahit 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);
		$caribrg 	= $this->uri->segment(10);
		$filterbrg 	= $this->uri->segment(11);

		$data['cur_page'] = $cur_page;
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['carinya'] = $carinya;
		$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;

		$data['query'] = $this->mmaster->get_bonmkeluarcutting($id_bonm);
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vformapprove';
		$this->load->view('template', $data);
	}

	function approve_submit()
	{
		// die('approve');
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id = $this->input->post('id_bonm');

		if ($id == null) {
			redirect('pengadaan/cform/viewbonmkeluarcutting');
		}

		$approve = $this->input->post('approve');
		if (@$approve != null && $approve == 'approve') {
			$this->mmaster->approve($id);
			$data['msg'] = 'Approve successfull';
		}

		$reject = $this->input->post('reject');
		$e_remark_notapprove = $this->input->post('e_remark_notapprove');
		if ($reject != null and $e_remark_notapprove != '') {
			$this->mmaster->reject($id, $e_remark_notapprove);
			$data['msg'] = 'Document rejected';
		}

		redirect('pengadaan/cform/viewbonmkeluarcutting');
	}

	function accept()
	{
		// =======================
		// disini coding utk pengecekan user login
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_bonm 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$tgl_awal 	= $this->uri->segment(6);
		$tgl_akhir 	= $this->uri->segment(7);
		$id_unit_jahit 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);
		$caribrg 	= $this->uri->segment(10);
		$filterbrg 	= $this->uri->segment(11);

		$data['cur_page'] = $cur_page;
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['carinya'] = $carinya;
		$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;

		$data['query'] = $this->mmaster->get_bonmkeluarcutting($id_bonm);
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();

		$data['msg'] = '';
		$data['isi'] = 'pengadaan/vformaccept';
		$this->load->view('template', $data);
	}

	function accept_submit()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id = $this->input->post('id_bonm');
		if ($id == null) {
			die('Error ID');
		}

		$id_bonm 	= $this->input->post('id_bonm', TRUE);

		$d_stb_receive = $this->input->post('d_stb_receive');

		$d_stb_receive = date('Y-m-d', strtotime($d_stb_receive));

		$e_receive = $this->input->post('e_receive');

		/**@params Array $items */
		$items = $this->input->post('items');

		/** update header */
		$this->mmaster->update_accept($id, $d_stb_receive, $e_receive);

		$no 			= $this->input->post('no', TRUE);
		$jumlah_input 	= $no - 1;

		$id_detail = array();

		for ($i = 1; $i <= $jumlah_input; $i++) {

			$id 		= $this->input->post('id' . $i, TRUE); /* id_detail_bonkeluarcutting */
			$id_brg_wip = $this->input->post('id_brg_wip_' . $i, TRUE);
			$id_warna 	= $this->input->post('id_warnarec_' . $i, TRUE);
			$iddetail 	= $this->input->post('id_detailrec' . $i, TRUE);
			$qty_warna 	= $this->input->post('qty_warnarec_' . $i, TRUE);

			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotal = 0;
			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);
				$qtytotal += $qty_warna[$xx];
			} // end for

			// update receive ke tm_bonmkeluarcutting_detail

			$this->db->query(" 	UPDATE tm_bonmkeluarcutting_detail SET n_quantity_receive = $qtytotal
								WHERE id_bonmkeluarcutting = '$id_bonm' AND id_brg_wip = '$id_brg_wip' ");

			// ================ end update item detail ===========

			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$iddetail[$xx] 	= trim($iddetail[$xx]);
				$id_warna[$xx] 	= trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);

				// update receive ke tm_bonmkeluarcutting_detail warna
				$this->db->query(" 	UPDATE tm_bonmkeluarcutting_detail_warna SET n_quantity_receive = $qty_warna[$xx]
									WHERE id_bonmkeluarcutting_detail = '$iddetail[$xx]' AND id_warna = '$id_warna[$xx]' ");
				// ================ end update item detail warna ===========
			}
		}

		redirect('pengadaan/cform/viewbonmkeluarcutting');
	}
}
