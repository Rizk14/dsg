<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-bb/mmaster');
  }

 /* function updatestokawal() {
	  $query3	= $this->db->query(" select kode_brg FROM tm_barang where status_aktif = 't'
					AND kode_brg NOT IN (select kode_brg FROM tm_stok) ");
											
		if($query3->num_rows()>0) {
			$hasil = $query3->result();
			 //$jum_dobel = 0; $jum_beda = 0;
			//$temp_kode = "";
			foreach ($hasil as $row1) { */
				/*if ($temp_kode != $row1->kode_brg) {
					echo "kode_brg = ".$row1->kode_brg."<br>";
					$jum_beda++;
				}
				else
					$jum_dobel++; */
				
				// utk data dummy aja
			/*	$stoknya = 1000;
				$tgl_update_stoknya = "2011-12-01";
				$harganya = 1;
				
				//insert ke tm_stok
				$data_header = array(
					'kode_brg'=>$row1->kode_brg,
					'stok'=>$stoknya,
					'tgl_update_stok'=>$tgl_update_stoknya);
				$this->db->insert('tm_stok',$data_header);
				
				//insert ke tt_stok
				$data_header = array(
					'kode_brg'=>$row1->kode_brg,
					'no_bukti'=>"SA",
					'masuk'=>$stoknya,
					'saldo'=>$stoknya,
					'tgl_input'=>$tgl_update_stoknya,
					'harga'=>$harganya
					);
				$this->db->insert('tt_stok',$data_header);
				
				//insert ke tm_stok_harga
				$data_header = array(
					'kode_brg'=>$row1->kode_brg,
					'stok'=>$stoknya,
					'harga'=>$harganya,
					'tgl_update_stok'=>$tgl_update_stoknya
					);
				$this->db->insert('tm_stok_harga',$data_header);
				
				$temp_kode = $row1->kode_brg;
				
			}
		}
		//echo $jum_dobel." ".$jum_beda. " done";
		echo "DONE";
  } */
  
  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$cjenis_bahan = $this->uri->segment(7);
		$cgudang = $this->uri->segment(8);
		$cstatus = $this->uri->segment(9);
		$carinya 	= $this->uri->segment(10);
		
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->kode_brg;
			$enama_brg = $row->nama_brg;
			$enama_brg_sup = $row->nama_brg_supplier;
			$esatuan = $row->satuan;
			$eid_jenis_bhn = $row->id_jenis_bahan;
			$edeskripsi = $row->deskripsi;
			
			if (strlen($ekode) == 14 ) {
				$ekode1 = substr($ekode, 0, 10);
				$ekode2 = substr($ekode, 10, 4); //echo $ekode2; die();
			}
			else if (strlen($ekode) == 15 ) {
				$ekode1 = substr($ekode, 0, 11);
				$ekode2 = substr($ekode, 11, 4);
			}
			else {
				$ekode1 = substr($ekode, 0, 12);
				$ekode2 = substr($ekode, 12, 4);
			}
			
			$query3	= $this->db->query(" SELECT a.kode as kode_kel, b.id as id_jns_brg, b.kode as kj_brg, b.nama as nj_brg, c.kode, c.nama  
			FROM tm_kelompok_barang a, tm_jenis_barang b, tm_jenis_bahan c 
			WHERE a.kode = b.kode_kel_brg AND b.id = c.id_jenis_barang AND c.id = '$eid_jenis_bhn' ");
			$hasilrow = $query3->row();
			$ekode_kel	= $hasilrow->kode_kel;
			$eid_jenis_brg	= $hasilrow->id_jns_brg;
			$ekode_jenis	= $hasilrow->kj_brg;
			$enama_jenis	= $hasilrow->nj_brg;
			$ekode_jenis_bhn = $hasilrow->kode;
			$enama_jenis_bhn = $hasilrow->nama;
			$eid_gudang	= $row->id_gudang;
			$eitem = $row->kode_item;
			$emotif = $row->kode_motif;
			$ewarna = $row->kode_warna;
			$estatus_aktif = $row->status_aktif;
			/*$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row->id_gudang' ");
			$hasilrow = $query3->row();
			$ekode_gudang	= $hasilrow->kode_gudang;
			$eid_gudang	= $hasilrow->id;
			$enama_gudang	= $hasilrow->nama;
			$enama_lokasi	= $hasilrow->nama_lokasi; */
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['cjenis_bahan'] = $cjenis_bahan;
		$data['cgudang'] = $cgudang;
		$data['cstatus'] = $cstatus;
		$data['carinya'] = $carinya;
	}
	else {
			$ekode = '';
			$ekode1 = '';
			$ekode2 = '';
			$enama_brg = '';
			$enama_brg_sup = '';
			$esatuan = '';
			$eid_jenis_bhn = '';
			$edeskripsi = '';
			$ekode_kel = '';
			$eid_jenis_brg	= '';
			$ekode_jenis = '';
			$enama_jenis = '';
			$ekode_jenis_bhn = '';
			$enama_jenis_bhn = '';
			$eid_gudang = '';
			/*$ekode_gudang = '';
			$eid_gudang = '';
			$enama_gudang	= '';
			$enama_lokasi	= ''; */
			$eitem = '';
			$emotif = '';
			$ewarna = '';
			$estatus_aktif = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['ekode1'] = $ekode1;
	$data['ekode2'] = $ekode2;
	$data['enama_brg'] = $enama_brg;
	$data['enama_brg_sup'] = $enama_brg_sup;
	$data['esatuan'] = $esatuan;
	$data['eid_jenis_bhn'] = $eid_jenis_bhn;
	$data['edeskripsi'] = $edeskripsi;
	$data['ekode_kel'] = $ekode_kel;
	$data['eid_jenis_brg'] = $eid_jenis_brg;
	$data['ekode_jenis'] = $ekode_jenis;
	$data['enama_jenis'] = $enama_jenis;
	$data['ekode_jenis_bhn'] = $ekode_jenis_bhn;
	$data['enama_jenis_bhn'] = $enama_jenis_bhn;
	$data['eid_gudang'] = $eid_gudang;
/*	$data['ekode_gudang'] =	$ekode_gudang;
	$data['eid_gudang'] =		$eid_gudang;
	$data['enama_gudang'] =		$enama_gudang;
	$data['enama_lokasi'] =		$enama_lokasi; */

	$data['eitem'] = $eitem;
	$data['emotif'] = $emotif;
	$data['ewarna'] = $ewarna;
	$data['estatus_aktif'] = $estatus_aktif;
	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['satuan'] = $this->mmaster->get_satuan();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['item'] = $this->mmaster->get_item();
	$data['motif'] = $this->mmaster->get_motif();
	$data['warna'] = $this->mmaster->get_warna();
    $data['isi'] = 'mst-bb/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	//$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');

		} */
		
			$kode 	= $this->input->post('kode', TRUE);
			$kode2 	= $this->input->post('kode2', TRUE);
			$kodegabung = $kode.$kode2;
			
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$nama_brg_sup 	= $this->input->post('nama_brg_sup', TRUE);
			$kel_brg 	= $this->input->post('kel_brg', TRUE);
			$kode_jenis 	= $this->input->post('kode_jenis', TRUE);
			$kode_jenis_bhn = $this->input->post('kode_jenis_bhn', TRUE);
			$id_jenis 	= $this->input->post('id_jenis', TRUE);
			$id_jenis_bhn 	= $this->input->post('id_jenis_bhn', TRUE);
			$satuan 	= $this->input->post('satuan', TRUE);
			
			$hide_satuan 	= $this->input->post('hide_satuan', TRUE);
			//$ada 	= $this->input->post('ada', TRUE);
			//if ($ada == 1)
			//	$satuan = $hide_satuan;
			//echo $satuan; die();
			
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$gudang 	= $this->input->post('gudang', TRUE);
			
			$item 	= $this->input->post('item', TRUE);
			$motif 	= $this->input->post('motif', TRUE);
			$warna 	= $this->input->post('warna', TRUE);
			$status_aktif 	= $this->input->post('status_aktif', TRUE);
			
			if ($goedit == '') { // tambah data
				//$cek_data = $this->mmaster->cek_data($kode, $id_jenis_bhn);
				$cek_data = $this->mmaster->cek_data($kodegabung);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-bb/vmainform';
					$data['msg'] = "Data kode ".$kodegabung." sudah ada..!";
					
					$ekode = '';
					$ekode1 = '';
					$ekode2 = '';
					$enama_brg = '';
					$enama_brg_sup = '';
					$esatuan = '';
					$eid_jenis_brg = '';
					$eid_jenis_bhn = '';
					$edeskripsi = '';
					$ekode_kel = '';
					$ekode_jenis = '';
					$enama_jenis = '';
					$ekode_jenis_bhn = '';
					$enama_jenis_bhn = '';
					$eid_gudang = '';
					
					$eitem = '';
					$emotif = '';
					$ewarna = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['ekode1'] = $ekode1;
					$data['ekode2'] = $ekode2;
					$data['enama_brg'] = $enama_brg;
					$data['enama_brg_sup'] = $enama_brg_sup;
					$data['esatuan'] = $esatuan;
					$data['eid_jenis_brg'] = $eid_jenis_brg;
					$data['eid_jenis_bhn'] = $eid_jenis_bhn;
					$data['edeskripsi'] = $edeskripsi;
					$data['ekode_kel'] = $ekode_kel;
					$data['ekode_jenis'] = $ekode_jenis;
					$data['enama_jenis'] = $enama_jenis;
					$data['ekode_jenis_bhn'] = $ekode_jenis_bhn;
					$data['enama_jenis_bhn'] = $enama_jenis_bhn;
					$data['eid_gudang'] = $eid_gudang;
					
					$data['eitem'] = $eitem;
					$data['emotif'] = $emotif;
					$data['ewarna'] = $ewarna;
					
					$data['edit'] = $edit;
					$data['kel_brg'] = $this->mmaster->get_kel_brg();
					$data['satuan'] = $this->mmaster->get_satuan();
					$data['list_gudang'] = $this->mmaster->get_gudang();
					$data['item'] = $this->mmaster->get_item();
					$data['motif'] = $this->mmaster->get_motif();
					$data['warna'] = $this->mmaster->get_warna();
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($kodegabung,$nama, $nama_brg_sup, $id_jenis_bhn, $item, $motif, $warna, $satuan, $deskripsi, $gudang, $status_aktif, $kodeedit, $goedit);
					redirect('mst-bb/cform');
				}
			} // end if goedit == ''
			else {
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$cjenis_bahan = $this->input->post('cjenis_bahan', TRUE);
				$cstatus = $this->input->post('cstatus', TRUE);
				$cgudang = $this->input->post('cgudang', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
				
				if ($kodegabung != $kodeedit) {
					//$cek_data = $this->mmaster->cek_data($kode, $id_jenis_bhn);
					$cek_data = $this->mmaster->cek_data($kodegabung);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'mst-bb/vmainform';
						$data['msg'] = "Data kode ".$kodegabung." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kodegabung;
						$data['ekode1'] = $kode;
						$data['ekode2'] = $kode2;
						$data['enama_brg'] = $nama;
						$data['enama_brg_sup'] = $nama_brg_sup;
						$data['esatuan'] = $satuan;
						$data['eid_jenis_brg'] = $id_jenis;
						$data['eid_jenis_bhn'] = $id_jenis_bhn;
						$data['edeskripsi'] = $deskripsi;
						$data['ekode_kel'] = $kel_brg;
						$data['ekode_jenis'] = $kode_jenis;
						$data['enama_jenis'] = '';
						$data['ekode_jenis_bhn'] = $kode_jenis_bhn;
						$data['enama_jenis_bhn'] = '';
						$data['estatus_aktif'] = $status_aktif;
						$data['edit'] = $edit;
						
						$data['eid_gudang'] = $gudang;
						$data['eitem'] = $item;
						$data['emotif'] = $motif;
						$data['ewarna'] = $warna;
						
						$data['kel_brg'] = $this->mmaster->get_kel_brg();
						$data['satuan'] = $this->mmaster->get_satuan();
						$data['list_gudang'] = $this->mmaster->get_gudang();
						$data['item'] = $this->mmaster->get_item();
						$data['motif'] = $this->mmaster->get_motif();
						$data['warna'] = $this->mmaster->get_warna();
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kodegabung,$nama, $nama_brg_sup, $id_jenis_bhn, $item, $motif, $warna, $satuan, $deskripsi, $gudang, $status_aktif, $kodeedit, $goedit);
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "mst-bb/cform/view/index/".$cur_page;
						else
							$url_redirectnya = "mst-bb/cform/cari/index/".$cjenis_bahan."/".$cgudang."/".$cstatus."/".$carinya."/".$cur_page;
					
						redirect($url_redirectnya);
						//redirect('mst-bb/cform/view');
					}
				}
				else {
					$this->mmaster->save($kodegabung,$nama, $nama_brg_sup, $id_jenis_bhn, $item, $motif, $warna, $satuan, $deskripsi, $gudang, $status_aktif, $kodeedit, $goedit);
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "mst-bb/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "mst-bb/cform/cari/index/".$cjenis_bahan."/".$cgudang."/".$cstatus."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('mst-bb/cform/view');
				}
			}
			
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'mst-bb/vformview';
    $keywordcari = "all";
    $jenis_bahan = '0';
    $cgudang = '0';
    $cstatus = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_bahan, $cgudang, $cstatus);	
							$config['base_url'] = base_url().'index.php/mst-bb/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $jenis_bahan, $cgudang, $cstatus);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['jns_bhn'] = $this->mmaster->get_jns_bhn();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['ejenis_bahan'] = $jenis_bahan;
	$data['cgudang'] = $cgudang;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jenis_bahan 	= $this->input->post('jenis_bahan', TRUE);  
	$cgudang = $this->input->post('gudang', TRUE);  
	$cstatus = $this->input->post('statusnya', TRUE);  
	
	if ($keywordcari == '' && ($jenis_bahan == '' || $cgudang == '' || $cstatus == '')) {
		$jenis_bahan 	= $this->uri->segment(5);
		$cgudang 	= $this->uri->segment(6);
		$cstatus 	= $this->uri->segment(7);
		$keywordcari 	= $this->uri->segment(8);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($jenis_bahan == '')
		$jenis_bahan = '0';
	
	if ($cgudang == '')
		$cgudang = '0';
	
	if ($cstatus == '')
		$cstatus = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_bahan, $cgudang, $cstatus);
							$config['base_url'] = base_url().'index.php/mst-bb/cform/cari/index/'.$jenis_bahan.'/'.$cgudang.'/'.$cstatus.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(9);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(9), $keywordcari, $jenis_bahan, $cgudang, $cstatus);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'mst-bb/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['ejenis_bahan'] = $jenis_bahan;
	$data['cgudang'] = $cgudang;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];
	$data['jns_bhn'] = $this->mmaster->get_jns_bhn();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);
  }
  
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg 	= $this->uri->segment(4);
	if ($kel_brg == '')
		$kel_brg = $this->input->post('kel_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $kel_brg == '') {
		$kel_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barangtanpalimit($kel_brg, $keywordcari);
			$config['base_url'] = base_url()."index.php/mst-bb/cform/show_popup_jenis/".$kel_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang($config['per_page'],$this->uri->segment(6), $kel_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['kel_brg'] = $kel_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('mst-bb/vpopupjenis',$data);

  }
  
  function show_popup_jenis_bhn(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jns_brg 	= $this->uri->segment(4);
	if ($jns_brg == '')
		$jns_brg = $this->input->post('jns_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $jns_brg == '') {
		$jns_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_bahantanpalimit($jns_brg, $keywordcari);
							$config['base_url'] = base_url()."index.php/mst-bb/cform/show_popup_jenis_bhn/".$jns_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_bahan($config['per_page'],$this->uri->segment(6), $jns_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['jns_brg'] = $jns_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		$hasilrow = $query3->row();
		$nama_jns_brg	= $hasilrow->nama;
	$data['nama_jns_brg'] = $nama_jns_brg;

	$this->load->view('mst-bb/vpopupjenisbhn',$data);

  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$cjenis_bahan = $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "mst-bb/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-bb/cform/cari/index/".$cjenis_bahan."/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
    //redirect('mst-bb/cform/view');
  }
}
