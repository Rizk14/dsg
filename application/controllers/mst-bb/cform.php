<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-bb/mmaster');
  }
  
  // 11-06-2015, skrg pake field id utk primary key. nanti di tabel2 lain ganti pake id_brg, bukan kode_brg
  function updateidbarang() {
	  $sql = " SELECT * FROM tm_barang ORDER BY tgl_input ";
	  $query	= $this->db->query($sql);
	  
	  if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil id tertinggi
			$sql2 = " SELECT id FROM tm_barang ORDER BY id DESC LIMIT 1 ";
			$query2	= $this->db->query($sql2);
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->row();
				$idlama	= $hasil2->id;
				$idbaru = $idlama+1;
				
				$this->db->query(" UPDATE tm_barang SET id='$idbaru' WHERE kode_brg='$row1->kode_brg' ");
			}
		}
	}
	echo "sukses";
  }
  
  // 30-06-2015, ganti jenis barang dari 34 (PLR) ke 32 (KAT)
  function updatejenisbrgkode() {
	  $sql = " SELECT * FROM tm_barang WHERE id_jenis_barang = '34' ORDER BY kode_brg ";
	  $query	= $this->db->query($sql);
	  
	  if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// query ke tabel tm_barang utk ambil no urut terakhir
			$queryxx = $this->db->query(" SELECT kode_brg FROM tm_barang
										WHERE kode_brg LIKE 'KAT%' ORDER BY kode_brg DESC LIMIT 1 ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$kode_brg_last = $hasilxx->kode_brg;
				$digitbaru = (substr($kode_brg_last, 3, 4))+1; // 4 digit no urut
				
				switch(strlen($digitbaru)) {
					case "1": $newdigit	= "000".$digitbaru;
					break;
					case "2": $newdigit	= "00".$digitbaru;
					break;	
					case "3": $newdigit	= "0".$digitbaru;
					break;
					case "4": $newdigit	= $digitbaru;
					break;	
				}
				$kode_brg_baru = "KAT".$newdigit;
			}
			else
				$kode_brg_baru = 'KAT0001';
			
			$this->db->query(" UPDATE tm_barang SET kode_brg = '".$kode_brg_baru."', id_jenis_barang = '32' WHERE id = '".$row1->id."' ");
		}
	}
	echo "sukses update jenis barang dan kode barang jadi KAT";
  }
  
  function updatekode() {
	  $sql = " SELECT * FROM tm_barang WHERE id_jenis_barang = '32' AND kode_brg between 'KAT0045' AND 'KAT0057' ORDER BY nama_brg ";
	  $query	= $this->db->query($sql);
	  
	  if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$kode_brg = $row1->kode_brg;
			$digitbaru = (substr($kode_brg, 3, 4))-1;
			
			$kode_brg_baru = "KAT".$digitbaru;
			$this->db->query(" UPDATE tm_barang SET kode_brg = '".$kode_brg_baru."' WHERE id = '".$row1->id."' ");
		}
	}
	echo "sukses update kode";
  }
  
  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_barang 	= $this->uri->segment(4);
	
	if ($id_barang != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$cjenis_barang = $this->uri->segment(7);
		$cgudang = $this->uri->segment(8);
		$cstatus = $this->uri->segment(9);
		$carinya 	= $this->uri->segment(10);
		
		$hasil = $this->mmaster->get($id_barang);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid_barang = $row->id;
			$ekode = $row->kode_brg;
			$enama_brg = $row->nama_brg;
			$enama_brg_sup = $row->nama_brg_supplier;
			$esatuan = $row->satuan;
			$eid_jenis_brg = $row->id_jenis_barang;
			$edeskripsi = $row->deskripsi;
			// 29-10-2014
			$esatuan_konv = $row->id_satuan_konversi;
			$erumus_konv = $row->rumus_konversi;
			$eangka_faktor_konversi = $row->angka_faktor_konversi;
			
		/*	if (strlen($ekode) == 12 ) {
				$ekode1 = substr($ekode, 0, 8);
				$ekode2 = substr($ekode, 8, 4); //echo $ekode2; die();
			}
			else if (strlen($ekode) == 13 ) {
				$ekode1 = substr($ekode, 0, 9);
				$ekode2 = substr($ekode, 9, 4);
			}
			else {
				$ekode1 = substr($ekode, 0, 10);
				$ekode2 = substr($ekode, 10, 4);
			} */
			
			$query3	= $this->db->query(" SELECT a.kode as kode_kel, b.id as id_jns_brg, b.kode as kj_brg, b.nama as nj_brg  
			FROM tm_kelompok_barang a INNER JOIN tm_jenis_barang b ON a.kode = b.kode_kel_brg
			WHERE b.id = '$eid_jenis_brg' ");
			$hasilrow = $query3->row();
			$ekode_kel	= $hasilrow->kode_kel;
			$eid_jenis_brg	= $hasilrow->id_jns_brg;
			$ekode_jenis	= $hasilrow->kj_brg;
			$enama_jenis	= $hasilrow->nj_brg;
			$eid_gudang	= $row->id_gudang;
			$estatus_aktif = $row->status_aktif;
			$eis_mutasi_stok_harga = $row->is_mutasi_stok_harga;
			
			/*$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row->id_gudang' ");
			$hasilrow = $query3->row();
			$ekode_gudang	= $hasilrow->kode_gudang;
			$eid_gudang	= $hasilrow->id;
			$enama_gudang	= $hasilrow->nama;
			$enama_lokasi	= $hasilrow->nama_lokasi; */
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['cjenis_barang'] = $cjenis_barang;
		$data['cgudang'] = $cgudang;
		$data['cstatus'] = $cstatus;
		$data['carinya'] = $carinya;
	}
	else {
			$eid_barang = '';
			$ekode = '';
			$enama_brg = '';
			$enama_brg_sup = '';
			$esatuan = '';
			$edeskripsi = '';
			$ekode_kel = '';
			$eid_jenis_brg	= '';
			$ekode_jenis = '';
			$enama_jenis = '';
			$eid_gudang = '';
			/*$ekode_gudang = '';
			$eid_gudang = '';
			$enama_gudang	= '';
			$enama_lokasi	= ''; */
			$estatus_aktif = '';
			// 29-10-2014
			$esatuan_konv = '0';
			$erumus_konv = '0';
			$eangka_faktor_konversi = '0';
			
			// 31-08-2015
			$eis_mutasi_stok_harga = '';
			$edit = '';
	}
	$data['eid_barang'] = $eid_barang;
	$data['ekode'] = $ekode;
	$data['enama_brg'] = $enama_brg;
	$data['enama_brg_sup'] = $enama_brg_sup;
	$data['esatuan'] = $esatuan;
	$data['edeskripsi'] = $edeskripsi;
	$data['ekode_kel'] = $ekode_kel;
	$data['eid_jenis_brg'] = $eid_jenis_brg;
	$data['ekode_jenis'] = $ekode_jenis;
	$data['enama_jenis'] = $enama_jenis;
	$data['eid_gudang'] = $eid_gudang;
/*	$data['ekode_gudang'] =	$ekode_gudang;
	$data['eid_gudang'] =		$eid_gudang;
	$data['enama_gudang'] =		$enama_gudang;
	$data['enama_lokasi'] =		$enama_lokasi; */

	$data['estatus_aktif'] = $estatus_aktif;
	
	// 29-10-2014
	$data['esatuan_konv'] = $esatuan_konv;
	$data['erumus_konv'] = $erumus_konv;
	$data['eangka_faktor_konversi'] = $eangka_faktor_konversi;
	// 31-08-2015
	$data['eis_mutasi_stok_harga'] = $eis_mutasi_stok_harga;
	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['satuan'] = $this->mmaster->get_satuan();
	$data['list_gudang'] = $this->mmaster->get_gudang();
    $data['isi'] = 'mst-bb/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');

		} */
		
			$kode 	= $this->input->post('kode', TRUE);
			$kode_lama 	= $this->input->post('kode_lama', TRUE);
			$id_barang 	= $this->input->post('id_barang', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$nama_brg_sup 	= $this->input->post('nama_brg_sup', TRUE);
			$kel_brg 	= $this->input->post('kel_brg', TRUE);
			$kode_jenis 	= $this->input->post('kode_jenis', TRUE);
			$id_jenis 	= $this->input->post('id_jenis', TRUE);
			$satuan 	= $this->input->post('satuan', TRUE);
			
			$hide_satuan 	= $this->input->post('hide_satuan', TRUE);			
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$gudang 	= $this->input->post('gudang', TRUE);
			$status_aktif 	= $this->input->post('status_aktif', TRUE);
			
			// 29-10-2014
			$satuan_konv 	= $this->input->post('satuan_konv', TRUE);
			$rumus_konv 	= $this->input->post('rumus_konv', TRUE);
			$angka_faktor_konversi 	= $this->input->post('angka_faktor_konversi', TRUE);
			
			//31-08-2015
			$is_mutasi_stok_harga 	= $this->input->post('is_mutasi_stok_harga', TRUE);
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data($kode, trim($nama));
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-bb/vmainform';
					$data['msg'] = "Data dgn nama barang ".$nama." sudah ada..!";
					
					$ekode = '';
					$enama_brg = '';
					$enama_brg_sup = '';
					$esatuan = '';
					$eid_jenis_brg = '';
					$edeskripsi = '';
					$ekode_kel = '';
					$ekode_jenis = '';
					$enama_jenis = '';
					$eid_gudang = '';					
					// 29-10-2014
					$esatuan_konv = '0';
					$erumus_konv = '0';
					$eangka_faktor_konversi = '0';
					// 31-08-2015
					$eis_mutasi_stok_harga = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama_brg'] = $enama_brg;
					$data['enama_brg_sup'] = $enama_brg_sup;
					$data['esatuan'] = $esatuan;
					$data['eid_jenis_brg'] = $eid_jenis_brg;
					$data['edeskripsi'] = $edeskripsi;
					$data['ekode_kel'] = $ekode_kel;
					$data['ekode_jenis'] = $ekode_jenis;
					$data['enama_jenis'] = $enama_jenis;
					$data['eid_gudang'] = $eid_gudang;					
					// 29-10-2014
					$data['esatuan_konv'] = $esatuan_konv;
					$data['erumus_konv'] = $erumus_konv;
					$data['eangka_faktor_konversi'] = $eangka_faktor_konversi;
					// 31-08-2015
					$data['eis_mutasi_stok_harga'] = $eis_mutasi_stok_harga;
					
					$data['edit'] = $edit;
					$data['kel_brg'] = $this->mmaster->get_kel_brg();
					$data['satuan'] = $this->mmaster->get_satuan();
					$data['list_gudang'] = $this->mmaster->get_gudang();
					$this->load->view('template',$data);
				}
				else {//$item,
					$this->mmaster->save($kode,$nama, $nama_brg_sup, $id_jenis, $satuan, 
										$deskripsi, $gudang, $status_aktif, $id_barang, $goedit, 
										$satuan_konv, $rumus_konv, $angka_faktor_konversi, $is_mutasi_stok_harga);
					redirect('mst-bb/cform');
				}
			} // end if goedit == ''
			else {
				$cur_page = $this->input->post('cur_page', TRUE);
				$is_cari = $this->input->post('is_cari', TRUE);
				$cjenis_barang = $this->input->post('cjenis_barang', TRUE);
				$cstatus = $this->input->post('cstatus', TRUE);
				$cgudang = $this->input->post('cgudang', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
				
				if ($kode != $kode_lama) {
					//$cek_data = $this->mmaster->cek_data($kode, $id_jenis_bhn);
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'mst-bb/vmainform';
						$data['msg'] = "Data kode ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama_brg'] = $nama;
						$data['enama_brg_sup'] = $nama_brg_sup;
						$data['esatuan'] = $satuan;
						$data['eid_jenis_brg'] = $id_jenis;
						$data['edeskripsi'] = $deskripsi;
						$data['ekode_kel'] = $kel_brg;
						$data['ekode_jenis'] = $kode_jenis;
						$data['enama_jenis'] = '';
						$data['estatus_aktif'] = $status_aktif;
						$data['edit'] = $edit;
						
						$data['eid_gudang'] = $gudang;
						// 29-10-2014
						$data['esatuan_konv'] = $satuan_konv;
						$data['erumus_konv'] = $rumus_konv;
						$data['eangka_faktor_konversi'] = $angka_faktor_konversi;
						// 31-08-2015
						$data['eis_mutasi_stok_harga'] = $is_mutasi_stok_harga;
						
						$data['kel_brg'] = $this->mmaster->get_kel_brg();
						$data['satuan'] = $this->mmaster->get_satuan();
						$data['list_gudang'] = $this->mmaster->get_gudang();
						$this->load->view('template',$data);
					}
					else {//$item,
						$this->mmaster->save($kode,$nama, $nama_brg_sup, $id_jenis, $satuan, 
										$deskripsi, $gudang, $status_aktif, $id_barang, $goedit, 
										$satuan_konv, $rumus_konv, $angka_faktor_konversi, $is_mutasi_stok_harga);
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "mst-bb/cform/view/index/".$cur_page;
						else
							$url_redirectnya = "mst-bb/cform/cari/index/".$cjenis_barang."/".$cgudang."/".$cstatus."/".$carinya."/".$cur_page;
					
						redirect($url_redirectnya);
						//redirect('mst-bb/cform/view');
					}
				}
				else {//$item,
					$this->mmaster->save($kode,$nama, $nama_brg_sup, $id_jenis, $satuan, 
										$deskripsi, $gudang, $status_aktif, $id_barang, $goedit, 
										$satuan_konv, $rumus_konv, $angka_faktor_konversi, $is_mutasi_stok_harga);
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "mst-bb/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "mst-bb/cform/cari/index/".$cjenis_barang."/".$cgudang."/".$cstatus."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('mst-bb/cform/view');
				}
			}
			
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'mst-bb/vformview';
    $keywordcari = "all";
    $kelompok_barang = '0';
    $jenis_barang = '0';
    $cgudang = '0';
    $cstatus = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_barang, $cgudang, $cstatus,$kelompok_barang);	
							$config['base_url'] = base_url().'index.php/mst-bb/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $jenis_barang, $cgudang, $cstatus,$kelompok_barang);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	
	$data['jns_brg'] = $this->mmaster->get_jns_brg();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['ekelompok_barang'] = $kelompok_barang;
	$data['ejenis_barang'] = $jenis_barang;
	$data['cgudang'] = $cgudang;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		
if(isset($_POST["submit2"])) {
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jenis_barang 	= $this->input->post('jenis_barang', TRUE);  
	$kelompok_barang 	= $this->input->post('kelompok_barang', TRUE);  
	$cgudang = $this->input->post('gudang', TRUE);  
	$cstatus = $this->input->post('statusnya', TRUE); 
	$export_excel1 = $this->input->post('export_excel', TRUE);  
	$export_ods1 = $this->input->post('export_ods', TRUE);  
	
	if ($keywordcari == '' && ($jenis_barang == '' || $cgudang == '' || $cstatus == '')) {
		$jenis_barang 	= $this->uri->segment(5);
		$cgudang 	= $this->uri->segment(6);
		$cstatus 	= $this->uri->segment(7);
		$keywordcari 	= $this->uri->segment(8);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($jenis_barang == '')
		$jenis_barang = '0';
	
	if ($cgudang == '')
		$cgudang = '0';
	
	if ($cstatus == '')
		$cstatus = '0';
		$query = $this->mmaster->getAlltanpalimit( $keywordcari, $jenis_barang, $cgudang, $cstatus,$kelompok_barang);
		$nama_file = "master_bahan_baku_pembantu_makloon";
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%' class='isi'>
	<thead>
	 <tr class='judulnya'>
		<th>No</th>
		 <th>Jenis Barang</th>
		 <th>Kode</th>
		 <th>Nama Barang</th>
		 <th>Nama Barang ke Supplier</th>
		 <th>Satuan</th>
		 <th>Satuan Konv</th>
		 <th>Angka Faktor<br>Konv</th>
		 <th>Rumus Konv</th>
		 <th>Deskripsi</th>
		 <th>Lokasi Gudang</th>
		 <th>Status</th>
		 
	 </tr>
	</thead>
	<tbody>";
	$i=1;
	foreach ($query as $row){
		
		$html_data .= " <tr class=\"record\">";
		$html_data .= "<td>$i</td>";
		
		$html_data .=     "<td>[$row->nama_kel_brg] $row->kj_brg - $row->nj_brg</td>";
		$html_data .=     "<td>$row->kode_brg</td>";
		$html_data .=     "<td>$row->nama_brg</td>";
		$html_data .=     "<td>$row->nama_brg_supplier</td>";
		$html_data .=     "<td>$row->nama_satuan</td>";
		
		if ($row->id_satuan_konversi != '0') {
					 $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id='".$row->id_satuan_konversi."' ");
					 if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konversi = $hasilrow->nama;
					 }
				 }
				 else
					$nama_satuan_konversi = '';
		$html_data .=    "<td>".$nama_satuan_konversi."</td>";
		$html_data .=    "<td align='right'>$row->angka_faktor_konversi</td>";
		
					$html_data .= "<td>";
				 if ($row->rumus_konversi == '0')
					$html_data .= "Tidak Ada";
				 else if ($row->rumus_konversi == '1')
					$html_data .= "Dikali (Qty Konv = Qty Sat Awal x Angka Faktor)";
				 else if ($row->rumus_konversi == '2')
					$html_data .= "Dibagi (Qty Konv = Qty Sat Awal / Angka Faktor)";
				 else if ($row->rumus_konversi == '3')
					$html_data .= "Ditambah (Qty Konv = Qty Sat Awal + Angka Faktor)";
				 else
					echo "Dikurang (Qty Konv = Qty Sat Awal - Angka Faktor)";
					$html_data .= "</td>";
			 $html_data .=    "<td>$row->deskripsi</td>";
			 if ($row->id_gudang != '') {
					 $query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$row->id_gudang' ");
					$hasilrow = $query3->row();
					$ekode_gudang	= $hasilrow->kode_gudang;
					$eid_gudang	= $hasilrow->id;
					$enama_gudang	= $hasilrow->nama;
					$enama_lokasi	= $hasilrow->nama_lokasi;
					
				$html_data .=     "<td>[$enama_lokasi] $ekode_gudang - $enama_gudang</td>";
				 }
				 else {
					$html_data .=  "<td>&nbsp;</td>";	 
				 }
				 if ($row->status_aktif == 't')
					$html_data .=     "<td>Aktif</td>";
				 else
					$html_data .=     "<td>Non-Aktif</td>";
					$html_data .=  "</tr>";
		$i++;
	}
	$html_data .= "
	</tbody>
		";
		
		
		if ($export_excel1 == '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
		redirect('mst-bb/cform/cari');
}
	  else {
 
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$kelompok_barang 	= $this->input->post('kelompok_barang', TRUE);  
	$jenis_barang 	= $this->input->post('jenis_barang', TRUE);  
	$cgudang = $this->input->post('gudang', TRUE);  
	$cstatus = $this->input->post('statusnya', TRUE);  
	
	if ($keywordcari == '' && ($jenis_barang == '' || $cgudang == '' || $cstatus == '' )) {
		$jenis_barang 	= $this->uri->segment(5);
		$cgudang 	= $this->uri->segment(6);
		$cstatus 	= $this->uri->segment(7);
		$keywordcari 	= $this->uri->segment(8);
		
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($jenis_barang == '')
		$jenis_barang = '0';
	
	if ($cgudang == '')
		$cgudang = '0';
	
	if ($cstatus == '')
		$cstatus = '0';
	
	if ($kelompok_barang == '')
		$kelompok_barang = '0';	
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_barang, $cgudang, $cstatus,	$kelompok_barang);
							$config['base_url'] = base_url().'index.php/mst-bb/cform/cari/index/'.$jenis_barang.'/'.$cgudang.'/'.$cstatus.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(9);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(9), $keywordcari, $jenis_barang, $cgudang, $cstatus,	$kelompok_barang);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'mst-bb/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['ekelompok_barang'] = $kelompok_barang;
	$data['ejenis_barang'] = $jenis_barang;
	$data['cgudang'] = $cgudang;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['jns_brg'] = $this->mmaster->get_jns_brg();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);
	
}

  }
  
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg 	= $this->uri->segment(4);
	if ($kel_brg == '')
		$kel_brg = $this->input->post('kel_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $kel_brg == '') {
		$kel_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barangtanpalimit($kel_brg, $keywordcari);
			$config['base_url'] = base_url()."index.php/mst-bb/cform/show_popup_jenis/".$kel_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang($config['per_page'],$this->uri->segment(6), $kel_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['kel_brg'] = $kel_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('mst-bb/vpopupjenis',$data);

  }
  
  function show_popup_jenis_bhn(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jns_brg 	= $this->uri->segment(4);
	if ($jns_brg == '')
		$jns_brg = $this->input->post('jns_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $jns_brg == '') {
		$jns_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_bahantanpalimit($jns_brg, $keywordcari);
							$config['base_url'] = base_url()."index.php/mst-bb/cform/show_popup_jenis_bhn/".$jns_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_bahan($config['per_page'],$this->uri->segment(6), $jns_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['jns_brg'] = $jns_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		$hasilrow = $query3->row();
		$nama_jns_brg	= $hasilrow->nama;
	$data['nama_jns_brg'] = $nama_jns_brg;

	$this->load->view('mst-bb/vpopupjenisbhn',$data);

  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id_barang 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$cjenis_barang = $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($id_barang);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "mst-bb/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-bb/cform/cari/index/".$cjenis_barang."/".$carinya."/".$cur_page;
			
	redirect($url_redirectnya);
    //redirect('mst-bb/cform/view');
  }
  
  // 11-06-2015
  function getnourutbrg(){
		$kode_jenis 	= $this->input->post('kode_jenis', TRUE);
		
		// query ke tabel tm_barang utk ambil no urut terakhir
		$queryxx = $this->db->query(" SELECT kode_brg FROM tm_barang
									WHERE kode_brg LIKE '".$kode_jenis."%' ORDER BY kode_brg DESC LIMIT 1 ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$kode_brg_last = $hasilxx->kode_brg;
			$digitbaru = (substr($kode_brg_last, 3, 4))+1; // 4 digit no urut
			
			switch(strlen($digitbaru)) {
				case "1": $newdigit	= "000".$digitbaru;
				break;
				case "2": $newdigit	= "00".$digitbaru;
				break;	
				case "3": $newdigit	= "0".$digitbaru;
				break;
				case "4": $newdigit	= $digitbaru;
				break;	
			}
			$kode_brg_baru = $kode_jenis.$newdigit;
		}
		else
			$kode_brg_baru = $kode_jenis.'0001';
		
		//$data['kode_brg_baru'] = $kode_brg_baru;
		//$this->load->view('mst-bb/vkodebrgbaru', $data); 
		echo $kode_brg_baru;
		return false;
  }
}
