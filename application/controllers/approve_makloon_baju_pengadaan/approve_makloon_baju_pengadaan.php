<?php
class approve_makloon_baju_pengadaan extends CI_Controller
{
    public $data = array(
        'halaman' => 'approve_makloon_baju_pengadaan',
        'isi' => 'approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan_list',
        'title' => 'Data Approve Makloon Baju',
    );

     public function __construct()
    {
        parent::__construct();
        $this->load->model('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan_model', 'approve_makloon_baju_pengadaan');
    }
    
     public function view($offset = 0)
    {
        $approve_makloon_baju_pengadaan = $this->approve_makloon_baju_pengadaan->get_all_paged_custom($offset);
        if ($approve_makloon_baju_pengadaan) {
            $this->data['approve_makloon_baju_pengadaan'] = $approve_makloon_baju_pengadaan;
            $this->data['paging'] = $this->approve_makloon_baju_pengadaan->paging('biasa', site_url('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/halaman/'), 4);
        } else {
            $this->data['approve_makloon_baju_pengadaan'] = 'Tidak ada data Approve Makloon Baju.';
        }
        $this->load->view('template', $this->data);
    }

    public function cari($offset = 0)
    {
        $approve_makloon_baju_pengadaan = $this->approve_makloon_baju_pengadaan->cari($offset);
        if ($approve_makloon_baju_pengadaan) {
            $this->data['approve_makloon_baju_pengadaan'] = $approve_makloon_baju_pengadaan;
            $this->data['paging'] = $this->approve_makloon_baju_pengadaan->paging('pencarian', site_url('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/cari/'), 4);
        } else {
            $this->data['approve_makloon_baju_pengadaan'] = 'Data tidak ditemukan.'. anchor('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan', ' Tampilkan semua approve_makloon_baju_pengadaan.', 'class="alert-link"');
        }
        $this->load->view('template', $this->data);
    }

   
    public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data Approve Makloon Baju';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data Approve Makloon Baju';
        $this->load->view('template', $this->data);
    }

   

    // Ubah status verifikasi
    public function ubah_status_verifikasi($id)
    {
        // Pastikan data approve_makloon_baju_pengadaan ada.
        $approve_makloon_baju_pengadaan = $this->approve_makloon_baju_pengadaan->get($id);
        if (! $approve_makloon_baju_pengadaan) {
            $this->session->set_flashdata('pesan_error', 'Data Approve Makloon Baju tidak ada. Kembali ke halaman ' . anchor('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/error');
        }

        // Ubah status verifikasi
        if ($this->approve_makloon_baju_pengadaan->ubah_status_approve($id, $approve_makloon_baju_pengadaan->status_approve)) {
            $this->session->set_flashdata('pesan', 'Status verifikasi berhasil diubah. Kembali ke halaman ' . anchor('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/sukses');
        } else {
            $this->session->set_flashdata('pesan', 'Status verifikasi gagal diubah. Kembali ke halaman ' . anchor('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/error');
        }
    }

    
    public function link_detail()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $approve_makloon_baju_pengadaan = $this->approve_makloon_baju_pengadaan->getAllDetail($id);
        if (! $approve_makloon_baju_pengadaan) {
            $this->session->set_flashdata('pesan_error', 'Data Detail Makloon Baju Gudang Jadi Wip tidak ada. Kembali ke halaman ' . anchor('approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_pengadaan/master-barang/error');
        }
		$this->data['list_unit_jahit'] = $this->approve_makloon_baju_pengadaan->get_unit_jahit(); 
		$this->data['list_unit_packing'] = $this->approve_makloon_baju_pengadaan->get_unit_packing(); 
		$this->data['list_gudang'] = $this->approve_makloon_baju_pengadaan->get_gudang(); 
        $this->data['values'] = $approve_makloon_baju_pengadaan;
		$this->data['isi'] = 'approve_makloon_baju_pengadaan/approve_makloon_baju_pengadaan_form_edit';
        $this->load->view('template', $this->data);
	 }

    
}
