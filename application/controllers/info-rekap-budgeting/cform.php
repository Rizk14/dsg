<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-rekap-budgeting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_jenis_barang'] = $this->mmaster->get_jenis_barang(); 
	$data['list_kelompok_barang'] = $this->mmaster->get_kelompok_barang();
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['isi'] = 'info-rekap-budgeting/vmainform';
	$this->load->view('template',$data);
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$supplier = $this->input->post('supplier', TRUE);
	
	$jns_data = $this->input->post('jns_data', TRUE); 
	if($jns_data==1){
		$nama_jns_data="CASH";
		}
	elseif($jns_data==2){
		$nama_jns_data="CREDIT";
		}
		else{
		$nama_jns_data="ALL";	
			}
		$data['query'] = $this->mmaster->get_budgeting($date_from, $date_to, $supplier,$jns_data);
		$data['isi'] = 'info-rekap-budgeting/vformview';
	
	
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['supplier'] = $supplier;
	
	
	if ($supplier != '0') {
		$query3	= $this->db->query(" SELECT kode_supplier, nama
									FROM tm_supplier WHERE id = '$supplier' ");
		$hasilrow = $query3->row();
		$kode_supplier	= $hasilrow->kode_supplier;
		$nama_supplier	= $hasilrow->nama;
	
	}
	else {
		$kode_supplier	= '';
		$nama_supplier	= '';
	
	}
	
	$data['kode_supplier'] = $kode_supplier;
	$data['nama_supplier'] = $nama_supplier;

	$data['jns_data'] = $jns_data;
	$data['nama_jns_data'] = $nama_jns_data;
	$this->load->view('template',$data);
  }
   
  function exportexcel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$date_from = $this->input->post('date_from', TRUE);
			$date_to = $this->input->post('date_to', TRUE);  
			$id_supplier = $this->input->post('id_supplier', TRUE);
			$jns_data = $this->input->post('jns_data', TRUE);  
			$export_excel1 = $this->input->post('export_excel', TRUE);  
			$export_ods = $this->input->post('jns_brg', TRUE);
				
		$query = $this->mmaster->get_budgeting($date_from, $date_to, $id_supplier,$jns_data);
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='5' align='center'>Rekap Budgeting</th>
		 </tr>
		 <tr>	
			<th colspan='5' align='center'>Dari Tanggal $date_from s/d $date_to  </th>	
		 </tr>
		<tr >
		   <th>No</th>
		 <th>Kode Supplier</th>
		  <th>Nama Supplier</th>
		 <th>Harga Permintaan</th>
		 <th>Harga Realisasi</th>
	 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++) {
				 
			 	  $html_data .=  "<tr class=\"record\">";
					 $no=1;$tottotal_permintaan=0;$tottotal_pemenuhan=0;
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++) {
				$html_data.= "<tr>
					<td>".$no."</td>
					<td>".$query[$j]['kode_supplier']."</td>
					<td>".$query[$j]['nama_supplier']."</td>
					<td>".number_format($query[$j]['total_permintaan'],0,',','.')."</td>
					<td>".number_format($query[$j]['total_pemenuhan'],0,',','.')."</td>
					</tr>";
				$tottotal_permintaan+=$query[$j]['total_permintaan'];
				$tottotal_pemenuhan+=$query[$j]['total_pemenuhan'];
				$no++;		 
			 	 }
			 	 $html_data.= "<tr>
					<td colspan='3' align='center'>Total</td>
					
					<td>".number_format($tottotal_permintaan,0,',','.')."</td>
					<td>".number_format($tottotal_pemenuhan,0,',','.')."</td>
					</tr>";
			 	}

				$html_data .= "</tr>";
					}
		   } 
			

		 $html_data.= "</tbody>
		</table>";

		$nama_file = "rekap-budjeting";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }

  }


