<?php

class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('gudangwip/mmaster');
  }

  function sounitjahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$data['isi'] = 'gudangwip/vformsounitjahit';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$data['list_unit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);

  }
  
  function viewsounitjahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  

	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
			
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	
	$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
	$hasilrow = $query3->row();
	$nama_unit	= $hasilrow->nama;
	
	$sql = "SELECT id FROM tt_stok_opname_unit_jahit WHERE status_approve = 'f' AND kode_unit = '$unit_jahit' ";
	if ($bulan == 1) // 24-03-2014
		//$sql.= " AND ((bulan <= '$bulan_sebelumnya' AND bulan<>'$bulan' AND tahun ='$tahun') OR (bulan<='12' AND tahun<'$tahun')) ";
		$sql.= " AND bulan<='12' AND tahun<'$tahun' ";
	else
		$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";
	//echo $sql; die();
	$query3	= $this->db->query($sql);
	
	if ($query3->num_rows() > 0){
		$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
		$data['list_unit'] = $this->mmaster->get_unit_jahit();
		$data['isi'] = 'gudangwip/vformsounitjahit';
		$this->load->view('template',$data);
	}
	else {
		// cek apakah sudah ada data SO di bulan setelahnya
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit WHERE bulan > '$bulan' AND tahun >= '$tahun' 
							AND kode_unit = '$unit_jahit' ");

		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformsounitjahit';
			$this->load->view('template',$data);
		}
		else {
			
			
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['unit_jahit'] = $unit_jahit;
			$data['nama_unit'] = $nama_unit;

			$cek_data = $this->mmaster->cek_so_unit_jahit($bulan, $tahun, $unit_jahit); 
			
			if ($cek_data['idnya'] == '' ) { 
				// jika data so blm ada, maka ambil stok terkini dari tabel tm_stok_unit_jahit_warna
				$data['query'] = $this->mmaster->get_stok_unit_jahit($bulan, $tahun, $unit_jahit);
				//print_r($data['query']); die();
				$data['is_new'] = '1';
				if (is_array($data['query']) )
					$data['jum_total'] = count($data['query']);
				else
					$data['jum_total'] = 0;
				
				$data['tgl_so'] = '';
				$data['isi'] = 'gudangwip/vviewsounitjahit';
				$this->load->view('template',$data);
			}
			else { // jika sudah diapprove maka munculkan msg
				if ($cek_data['status_approve'] == 't') {
					$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah di-approve..!";
					$data['list_unit'] = $this->mmaster->get_unit_jahit();
					$data['isi'] = 'gudangwip/vformsounitjahit';
					$this->load->view('template',$data);
				}
				else {
					// get data dari tabel tt_stok_opname_unit_jahit yg statusnya 'f'
					$data['query'] = $this->mmaster->get_all_stok_opname_unit_jahit($bulan, $tahun, $unit_jahit);
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					
					$tgl_so = $cek_data['tgl_so'];
					$pisah1 = explode("-", $tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
					
					$data['tgl_so'] = $tgl_so;
					
					$data['isi'] = 'gudangwip/vviewsounitjahit';
					$this->load->view('template',$data);
					
				}
			} // end else
		} // end else bln setelahnya
	} // end else bln sebelumnya
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++

  }
  
  function submitsounitjahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_jahit = $this->input->post('unit_jahit', TRUE);  
	  // 12-03-2014 variabel $no ga perlu dipake lg, karena udh ada $jum_data
	 // $no = $this->input->post('no', TRUE);  
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  // 04-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
	  $hasilrow = $query3->row();
	  $nama_unit	= $hasilrow->nama;
	  
	  if ($is_new == '1') {
	      // insert ke tabel tt_stok_opname_
	        $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$id_stok	= $seqrow->id+1;
			}else{
				$id_stok	= 1;
			}
	      $data_header = array(
			  'id'=>$id_stok,
			  'kode_unit'=>$unit_jahit,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_so'=>$tgl_so
			);
		  $this->db->insert('tt_stok_opname_unit_jahit',$data_header);
		  
		  // ambil data terakhir di tabel tt_stok_opname_
		 /*$query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id; */
	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mmaster->savesounitjahit($is_new, $id_stok, $this->input->post('kode_brg_jadi_'.$i, TRUE), 
			 $this->input->post('kode_warna_'.$i, TRUE),
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_bgs_'.$i, TRUE),
			 $this->input->post('stok_fisik_perbaikan_'.$i, TRUE));
		  }
		  
		$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil disimpan. Silahkan lakukan approval untuk mengupdate stok";
		$data['list_unit'] = $this->mmaster->get_unit_jahit();
		$data['isi'] = 'gudangwip/vformsounitjahit';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
			  // ambil data id dari tabel tt_stok_opname_unit_jahit
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit WHERE kode_unit = '$unit_jahit'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 // 06-02-2014, query ke tabel tt_stok_opname_unit_jahit_detail utk hapus tiap2 item warnanya
			 $sqlxx	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail
								WHERE id_stok_opname_unit_jahit = '$id_stok' ");
			if ($sqlxx->num_rows() > 0){
				$hasilxx = $sqlxx->result();
				foreach ($hasilxx as $rowxx) {
					$this->db->query(" DELETE FROM tt_stok_opname_unit_jahit_detail_warna WHERE id_stok_opname_unit_jahit_detail='$rowxx->id' ");
				} // end foreach detail
			} // end if
			 //==============================================================================================
			 
			$this->db->query(" delete from tt_stok_opname_unit_jahit_detail where id_stok_opname_unit_jahit = '$id_stok' ");
			$this->db->query(" delete from tt_stok_opname_unit_jahit where id = '$id_stok' ");
			
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil dihapus";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformsounitjahit';
			$this->load->view('template',$data);
		  }
		  else { // function update
			  // update ke tabel tt_stok_opname_
			   // ambil data terakhir di tabel tt_stok_opname_
				 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit WHERE kode_unit = '$unit_jahit' 
								AND bulan = '$bulan' AND tahun = '$tahun' ");
				 $hasilrow = $query2->row();
				 $id_stok	= $hasilrow->id; 
				 
				 $this->db->query(" UPDATE tt_stok_opname_unit_jahit SET tgl_so='$tgl_so', tgl_update = '$tgl' 
							where id = '$id_stok' ");
				 
			  for ($i=1;$i<=$jum_data;$i++)
			  {
				 $this->mmaster->savesounitjahit($is_new, $id_stok, 
				 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
				 $this->input->post('kode_warna_'.$i, TRUE),
				 $this->input->post('stok_'.$i, TRUE),
				 $this->input->post('stok_fisik_bgs_'.$i, TRUE),
				 $this->input->post('stok_fisik_perbaikan_'.$i, TRUE));
			  }
			  
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil diupdate. Silahkan lakukan approval untuk mengupdate stok";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformsounitjahit';
			$this->load->view('template',$data);
		}
      }
  } // sampe sini
  
  function approvalsounitjahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'gudangwip/vformappsounitjahit';
	$data['msg'] = '';
	$data['list_unit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);
  }
  
  function viewappsounitjahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$unit_jahit = $this->input->post('unit_jahit', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
		$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
		
		$data['unit_jahit'] = $unit_jahit;
		$data['nama_unit'] = $nama_unit;
			
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

	$cek_data = $this->mmaster->cek_so_unit_jahit($bulan, $tahun, $unit_jahit); 
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." belum diinput..!";
		$data['list_unit'] = $this->mmaster->get_unit_jahit();
		$data['isi'] = 'gudangwip/vformappsounitjahit';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah di-approve..!";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformappsounitjahit';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_ yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname_unit_jahit($bulan, $tahun, $unit_jahit);
			$data['jum_total'] = count($data['query']);
			
			$tgl_so = $cek_data['tgl_so'];
			$pisah1 = explode("-", $tgl_so);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_so = $tgl1."-".$bln1."-".$thn1;
			
			$data['tgl_so'] = $tgl_so;
			$data['isi'] = 'gudangwip/vviewappsounitjahit';
			$this->load->view('template',$data);
		}
	} // end else
  }
  
  function submitappsounitjahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_jahit = $this->input->post('unit_jahit', TRUE);
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  
	  // 04-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan < 12) {
		$bulanplus1 = $bulan+1;
		$tahunplus1 = $tahun;
		if ($bulanplus1 < 10)
			$bulanplus1= '0'.$bulanplus1;
	  }
	  else {
		$bulanplus1 = '01';
		$tahunplus1 = $tahun+1;
	  }
	  
	  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
	  
	  $tgl = date("Y-m-d H:i:s"); 
		  	      
	      for ($i=1;$i<=$no;$i++)
		  {
			 $kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
			 $kode_warna = $this->input->post('kode_warna_'.$i, TRUE);
			 //$stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE);
			 $stok_fisik_bgs = $this->input->post('stok_fisik_bgs_'.$i, TRUE);
			 $stok_fisik_perbaikan = $this->input->post('stok_fisik_perbaikan_'.$i, TRUE);
			 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
			 $idnya = $this->input->post('id_'.$i, TRUE);
			 
			 // ============== new skrip 06-02-2014 ================================================
			 //-------------- hitung total qty dari detail tiap2 warna -------------------
				//$qtytotal = 0;
				$qtytotalstokfisikbgs = 0;
				$qtytotalstokfisikperbaikan = 0;
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					//$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					$stok_fisik_bgs[$xx] = trim($stok_fisik_bgs[$xx]);
					$stok_fisik_perbaikan[$xx] = trim($stok_fisik_perbaikan[$xx]);
					//$qtytotal+= $stok_fisik[$xx];
					$qtytotalstokfisikbgs+= $stok_fisik_bgs[$xx];
					$qtytotalstokfisikperbaikan+= $stok_fisik_perbaikan[$xx];
				} // end for
			// ---------------------------------------------------------------------
				// 24-03-2014
				$totalxx = $qtytotalstokfisikbgs+$qtytotalstokfisikperbaikan; //echo $totalxx." ";
			
			// ======== update stoknya! =============
			// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/ 
		
			//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
					// AND a.tgl_bonm >= '$tglawalplus'
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
						// AND a.tgl_sj >= '$tglawalplus'
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b
									WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadi == '')
								$masuk_returbrgjadi = 0;
						}
						else
							$masuk_returbrgjadi = 0;
						
						//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
						// AND a.tgl_bonm >= '$tglawalplus'
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND a.jenis_keluar = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalian == '')
								$masuk_pengembalian = 0;
						}
						else
							$masuk_pengembalian = 0;
						
						$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						// AND a.tgl_sj >= '$tglawalplus'
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						// AND a.tgl_sj >= '$tglawalplus'
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikan == '')
								$keluar_perbaikan = 0;
						}
						else
							$keluar_perbaikan = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						// AND a.tgl_sj >= '$tglawalplus'
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbaku == '')
								$keluar_retur_bhnbaku = 0;
						}
						else
							$keluar_retur_bhnbaku = 0;
						
						$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
						$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
				// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
				$totalbagusxx = $qtytotalstokfisikbgs + $stok_akhir_bgs; //echo $stok_akhir_bgs." ";
				$totalperbaikanxx = $qtytotalstokfisikperbaikan + $stok_akhir_perbaikan; //echo $stok_akhir_perbaikan." ";
				$totalxx = $totalxx + $jum_stok_akhir; //echo $totalxx." ";
			
				//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok, stok_bagus, stok_perbaikan FROM tm_stok_unit_jahit 
							WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_unit='$unit_jahit' ");
					if ($query3->num_rows() == 0){
						$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
						if($seqxx->num_rows() > 0) {
							$seqrow	= $seqxx->row();
							$id_stok	= $seqrow->id+1;
						}else{
							$id_stok	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok,
							'kode_brg_jadi'=>$kode_brg_jadi,
							'kode_unit'=>$unit_jahit,
							'stok'=>$totalxx,
							/*'stok_bagus'=>$qtytotalstokfisikbgs,
							'stok_perbaikan'=>$qtytotalstokfisikperbaikan, */
							'stok_bagus'=>$totalbagusxx,
							'stok_perbaikan'=>$totalperbaikanxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_jahit', $data_stok);
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						
						/*echo "UPDATE tm_stok_unit_jahit SET stok = '$totalxx', stok_bagus='$totalbagusxx',
						stok_perbaikan='$totalperbaikanxx', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode_brg_jadi' AND kode_unit='$unit_jahit'"."<br>"; */
						
						$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$totalxx', stok_bagus='$totalbagusxx',
						stok_perbaikan='$totalperbaikanxx', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode_brg_jadi' AND kode_unit='$unit_jahit' ");
					}
					
				// stok_unit_jahit_warna
				// ----------------------------------------------
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					//$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					$stok_fisik_bgs[$xx] = trim($stok_fisik_bgs[$xx]);
					$stok_fisik_perbaikan[$xx] = trim($stok_fisik_perbaikan[$xx]);
					
					// 24-03-2014
					$totalxx = $stok_fisik_bgs[$xx]+$stok_fisik_perbaikan[$xx];
						
					// ========================= stok per warna ===============================================
					
					// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
		
			//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c
									WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
									AND a.tgl_bonm >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND a.jenis_keluar = '1'
									AND c.kode_warna = '".$kode_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c
									WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
									AND a.tgl_sj >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3'
									AND c.kode_warna = '".$kode_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadi == '')
								$masuk_returbrgjadi = 0;
						}
						else
							$masuk_returbrgjadi = 0;
						
						//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c 
									WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
									AND a.tgl_bonm >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND a.jenis_keluar = '2'
									AND c.kode_warna = '".$kode_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalian == '')
								$masuk_pengembalian = 0;
						}
						else
							$masuk_pengembalian = 0;
						
						$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.tgl_sj >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' 
									AND c.kode_warna = '".$kode_warna[$xx]."'");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.tgl_sj >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2'
									AND c.kode_warna = '".$kode_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikan == '')
								$keluar_perbaikan = 0;
						}
						else
							$keluar_perbaikan = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b, tm_sjmasukbhnbakupic_detail_warna c
									WHERE a.id = b.id_sjmasukbhnbakupic AND b.id = c.id_sjmasukbhnbakupic_detail
									AND a.tgl_sj >= '$tgl_so'
									AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit'
									AND c.kode_warna = '".$kode_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbaku == '')
								$keluar_retur_bhnbaku = 0;
						}
						else
							$keluar_retur_bhnbaku = 0;
						
						$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
						$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
				// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
				$totalbagusxx = $stok_fisik_bgs[$xx] + $stok_akhir_bgs;
				$totalperbaikanxx = $stok_fisik_perbaikan[$xx] + $stok_akhir_perbaikan;
				$totalxx = $totalxx + $jum_stok_akhir;
					
					//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_unit_jahit='$id_stok' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_unit_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_unit_jahit'=>$id_stok,
							'id_warna_brg_jadi'=>'0',
							'kode_warna'=>$kode_warna[$xx],
							//'stok'=>$stok_fisik[$xx],
							'stok'=>$totalxx,
							/*'stok_bagus'=>$stok_fisik_bgs[$xx],
							'stok_perbaikan'=>$stok_fisik_perbaikan[$xx], */
							'stok_bagus'=>$totalbagusxx,
							'stok_perbaikan'=>$totalperbaikanxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '".$totalxx."', 
						stok_bagus = '".$totalbagusxx."', stok_perbaikan = '".$totalperbaikanxx."', tgl_update_stok = '$tgl' 
						where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok' ");
					}
				} // end for
				// ----------------------------------------------
			 // ====================================================================================
			 
			 $this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET status_approve = 't'
								where id = '$idnya' ");

		  } // end for
		  $this->db->query(" UPDATE tt_stok_opname_unit_jahit SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  //die();
		    $data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$unit_jahit." - ".$nama_unit." sudah berhasil di-approve, dan otomatis mengupdate stok";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformappsounitjahit';
			$this->load->view('template',$data);
  }
  
  // 14-04-2014, INI DIPAKE DI TIRAI. kalo di duta, perlu dimodif di perhitungan selisih jumlah masuk keluar
  function autoupdatesounitjahit() {
	  $tgl = date("Y-m-d H:i:s"); 
	  
		$sqlxx = " SELECT id, bulan, tahun, kode_unit FROM tt_stok_opname_unit_jahit WHERE status_approve='t' ORDER BY kode_unit, tahun, bulan ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				//---------------------------------------
				if ($rowxx->bulan < 12) {
					$bulanplus1 = $rowxx->bulan+1;
					$tahunplus1 = $rowxx->tahun;
					if ($bulanplus1 < 10)
						$bulanplus1= '0'.$bulanplus1;
				  }
				  else {
					$bulanplus1 = '01';
					$tahunplus1 = $rowxx->tahun+1;
				  }
				  
				  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
				//---------------------------------------
				
				// detail item brg dan juga warnanya
				$sqlxx2 = " SELECT id, id_stok_opname_unit_jahit, kode_brg_jadi, jum_stok_opname, jum_bagus, jum_perbaikan 
							FROM tt_stok_opname_unit_jahit_detail WHERE id_stok_opname_unit_jahit='$rowxx->id' ";
				$queryxx2	= $this->db->query($sqlxx2);
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						
						//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit = '$rowxx->kode_unit' AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b
									WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadi == '')
								$masuk_returbrgjadi = 0;
						}
						else
							$masuk_returbrgjadi = 0;
						
						//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit = '$rowxx->kode_unit' AND a.jenis_keluar = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalian == '')
								$masuk_pengembalian = 0;
						}
						else
							$masuk_pengembalian = 0;
						
						$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikan == '')
								$keluar_perbaikan = 0;
						}
						else
							$keluar_perbaikan = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit = '$rowxx->kode_unit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbaku == '')
								$keluar_retur_bhnbaku = 0;
						}
						else
							$keluar_retur_bhnbaku = 0;
						
						$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
						$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
						//------------------------------- END query cek brg masuk/keluar ------------------------------------
						
						$qtytotalstokfisikbgs = 0;
						$qtytotalstokfisikperbaikan = 0;
						$query3	= $this->db->query(" SELECT sum(jum_bagus) as jum_bagus, sum(jum_perbaikan) as jum_perbaikan 
									FROM tt_stok_opname_unit_jahit_detail_warna WHERE id_stok_opname_unit_jahit_detail='$rowxx2->id' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qtytotalstokfisikbgs = $hasilrow->jum_bagus;
							$qtytotalstokfisikperbaikan = $hasilrow->jum_perbaikan;
						}
						$totalxx = $qtytotalstokfisikbgs+$qtytotalstokfisikperbaikan;
						
						// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
						$totalbagusxx = $qtytotalstokfisikbgs + $stok_akhir_bgs; //echo $stok_akhir_bgs." ";
						$totalperbaikanxx = $qtytotalstokfisikperbaikan + $stok_akhir_perbaikan; //echo $stok_akhir_perbaikan." ";
						$totalxx = $totalxx + $jum_stok_akhir; //echo $totalxx." ";
						
						// update stok
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_bagus, stok_perbaikan FROM tm_stok_unit_jahit 
								WHERE kode_brg_jadi = '$rowxx2->kode_brg_jadi' AND kode_unit='$rowxx->kode_unit' ");
						if ($query3->num_rows() == 0){
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrow	= $seqxx->row();
								$id_stok	= $seqrow->id+1;
							}else{
								$id_stok	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok,
								'kode_brg_jadi'=>$rowxx2->kode_brg_jadi,
								'kode_unit'=>$rowxx->kode_unit,
								'stok'=>$totalxx,
								'stok_bagus'=>$totalbagusxx,
								'stok_perbaikan'=>$totalperbaikanxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
														
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$totalxx', stok_bagus='$totalbagusxx',
							stok_perbaikan='$totalperbaikanxx', tgl_update_stok = '$tgl' 
							where kode_brg_jadi= '$rowxx2->kode_brg_jadi' AND kode_unit='$rowxx->kode_unit' ");
						}
						// end update stok
						
						$sqlxx3 = " SELECT * FROM tt_stok_opname_unit_jahit_detail_warna WHERE id_stok_opname_unit_jahit_detail='$rowxx2->id' ";
						$queryxx3	= $this->db->query($sqlxx3);
						if ($queryxx3->num_rows() > 0){
							$hasilxx3 = $queryxx3->result();
							foreach ($hasilxx3 as $rowxx3) {
								$totalxx = $rowxx3->jum_bagus + $rowxx3->jum_perbaikan;
								
								//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
													tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c
													WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
													AND a.tgl_bonm >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit = '$rowxx->kode_unit' AND a.jenis_keluar = '1'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$masuk_bgs = $hasilrow->jum_masuk;
											
											if ($masuk_bgs == '')
												$masuk_bgs = 0;
										}
										else
											$masuk_bgs = 0;
										
										//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a, 
													tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c
													WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
													AND a.tgl_sj >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_keluar = '3'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$masuk_returbrgjadi = $hasilrow->jum_masuk;
											
											if ($masuk_returbrgjadi == '')
												$masuk_returbrgjadi = 0;
										}
										else
											$masuk_returbrgjadi = 0;
										
										//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
													tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c 
													WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
													AND a.tgl_bonm >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit = '$rowxx->kode_unit' AND a.jenis_keluar = '2'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$masuk_pengembalian = $hasilrow->jum_masuk;
											
											if ($masuk_pengembalian == '')
												$masuk_pengembalian = 0;
										}
										else
											$masuk_pengembalian = 0;
										
										$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
										// ------------------------------------------ END MASUK --------------------------------
										
										// 4. hitung brg keluar bagus, dari tm_sjmasukwip
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a, 
													tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
													WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
													AND a.tgl_sj >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_masuk = '1' 
													AND c.kode_warna = '".$rowxx3->kode_warna."'");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$keluar_bgs = $hasilrow->jum_keluar;
											
											if ($keluar_bgs == '')
												$keluar_bgs = 0;
										}
										else
											$keluar_bgs = 0;
										
										// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a, 
													tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
													WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
													AND a.tgl_sj >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_masuk = '2'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$keluar_perbaikan = $hasilrow->jum_keluar;
											
											if ($keluar_perbaikan == '')
												$keluar_perbaikan = 0;
										}
										else
											$keluar_perbaikan = 0;
										
										// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
													tm_sjmasukbhnbakupic_detail b, tm_sjmasukbhnbakupic_detail_warna c
													WHERE a.id = b.id_sjmasukbhnbakupic AND b.id = c.id_sjmasukbhnbakupic_detail
													AND a.tgl_sj >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit = '$rowxx->kode_unit'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
											
											if ($keluar_retur_bhnbaku == '')
												$keluar_retur_bhnbaku = 0;
										}
										else
											$keluar_retur_bhnbaku = 0;
										
										$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
										
										// -------------------------------- END BARANG KELUAR -----------------------------------------------
										
										$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
										$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
										$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
							//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
								// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
								$totalbagusxx = $rowxx3->jum_bagus + $stok_akhir_bgs;
								$totalperbaikanxx = $rowxx3->jum_perbaikan + $stok_akhir_perbaikan;
								$totalxx = $totalxx + $jum_stok_akhir;
								
								//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$rowxx3->kode_warna."'
										AND id_stok_unit_jahit='$id_stok' ");
								if ($query3->num_rows() == 0){
									// jika blm ada data di tm_stok_unit_jahit_warna, insert
									$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
									if($seq_stokwarna->num_rows() > 0) {
										$seq_stokwarnarow	= $seq_stokwarna->row();
										$id_stok_warna	= $seq_stokwarnarow->id+1;
									}else{
										$id_stok_warna	= 1;
									}
									
									$data_stok = array(
										'id'=>$id_stok_warna,
										'id_stok_unit_jahit'=>$id_stok,
										'id_warna_brg_jadi'=>'0',
										'kode_warna'=>$rowxx3->kode_warna,
										'stok'=>$totalxx,
										'stok_bagus'=>$totalbagusxx,
										'stok_perbaikan'=>$totalperbaikanxx,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '".$totalxx."', 
									stok_bagus = '".$totalbagusxx."', stok_perbaikan = '".$totalperbaikanxx."', tgl_update_stok = '$tgl' 
									where kode_warna= '".$rowxx3->kode_warna."' AND id_stok_unit_jahit='$id_stok' ");
								}
					
							} //end for3
						} // end if3

						
					} // end for2
				} // end if2
				
			} // end for1
		} // end if1
		echo "update stok unit jahit sukses";
  }
  
  // 07-05-2014, cuma temp aja, ga usah dipake
  function autoupdatesounitjahit2() {
	  $tgl = date("Y-m-d H:i:s"); 
	  
		$sqlxx = " SELECT id, bulan, tahun, kode_unit FROM tt_stok_opname_unit_jahit WHERE 
					status_approve='t' ORDER BY kode_unit, tahun, bulan ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				//---------------------------------------
				/*if ($rowxx->bulan < 12) {
					$bulanplus1 = $rowxx->bulan+1;
					$tahunplus1 = $rowxx->tahun;
					if ($bulanplus1 < 10)
						$bulanplus1= '0'.$bulanplus1;
				  }
				  else {
					$bulanplus1 = '01';
					$tahunplus1 = $rowxx->tahun+1;
				  }
				  
				  $tglawalplus = $tahunplus1."-".$bulanplus1."-01"; */
				//---------------------------------------
				
				// detail item brg dan juga warnanya
				$sqlxx2 = " SELECT id, id_stok_opname_unit_jahit, kode_brg_jadi, jum_stok_opname, jum_bagus, jum_perbaikan 
							FROM tt_stok_opname_unit_jahit_detail WHERE id_stok_opname_unit_jahit='$rowxx->id' ";
				$queryxx2	= $this->db->query($sqlxx2);
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						$sqlxx3 = " SELECT sum(jum_bagus) as totdetail FROM tt_stok_opname_unit_jahit_detail_warna 
									WHERE id_stok_opname_unit_jahit_detail='$rowxx2->id' ";
						$queryxx3	= $this->db->query($sqlxx3);
						if ($queryxx3->num_rows() > 0){
							$hasilrowxx3 = $queryxx3->row();
							$totdetail = $hasilrowxx3->totdetail;
							//echo $totdetail." ".$rowxx2->jum_stok_opname."<br>";
							if ($totdetail != $rowxx2->jum_bagus)
								echo $rowxx->kode_unit." ".$rowxx2->kode_brg_jadi." ".$rowxx2->jum_bagus." ".$totdetail."<br>";
						} // end if3

						
					} // end for2
				} // end if2
				
			} // end for1
		} // end if1
  }
  
}
