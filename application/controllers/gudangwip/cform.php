<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('gudangwip/mmaster');
  }

  function sounitjahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$data['isi'] = 'gudangwip/vformsounitjahit';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$data['list_unit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);

  }
  
  function viewsounitjahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  

	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
			
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	
	$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
	$hasilrow = $query3->row();
	$kode_unit	= $hasilrow->kode_unit;
	$nama_unit	= $hasilrow->nama;
	
	$sql = "SELECT id FROM tt_stok_opname_unit_jahit WHERE status_approve = 'f' AND id_unit = '$unit_jahit' ";
	if ($bulan == 1) // 24-03-2014
		//$sql.= " AND ((bulan <= '$bulan_sebelumnya' AND bulan<>'$bulan' AND tahun ='$tahun') OR (bulan<='12' AND tahun<'$tahun')) ";
		$sql.= " AND bulan<='12' AND tahun<'$tahun' ";
	else
		$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";
	//echo $sql; die();
	$query3	= $this->db->query($sql);
	
	// 14-01-2016 SEMENTARA DIKOMEN UTK BERESIN DATA SO NOV 2015
	// 22-01-2016 DIBUKA LAGI
	if ($query3->num_rows() > 0){
		$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
		$data['list_unit'] = $this->mmaster->get_unit_jahit();
		$data['isi'] = 'gudangwip/vformsounitjahit';
		$this->load->view('template',$data);
	}
	else {
		// cek apakah sudah ada data SO di bulan setelahnya
		
		// 14-01-2016 SEMENTARA DIKOMEN UTK BERESIN DATA SO NOV 2015. udh dibuka lagi
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit WHERE bulan > '$bulan' AND tahun >= '$tahun' 
							AND id_unit = '$unit_jahit' ");

		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformsounitjahit';
			$this->load->view('template',$data);
		}
		else {
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['unit_jahit'] = $unit_jahit;
			$data['kode_unit'] = $kode_unit;
			$data['nama_unit'] = $nama_unit;

			$cek_data = $this->mmaster->cek_so_unit_jahit($bulan, $tahun, $unit_jahit); 
			
			if ($cek_data['idnya'] == '' ) { 
				// 30-10-2015, CEK APAKAH DATA DI TABEL SO MASIH KOSONG? JIKA MASIH KOSONG, MAKA MUNCULKAN FORM INPUT BARU
				
				// 09-01-2016, SEMENTARA PAKE INPUT SATU2 DULU SAMPE DATA DESEMBER 2015, JADI YG CEK SOKOSONG DIKOMEN DULU
				// 22-01-2016 DIBUKA LAGI
				$is_sokosong = $this->mmaster->cek_sokosong($unit_jahit); 
				if ($is_sokosong == 'f') { // JIKA SUDAH ADA SO
					// jika data so blm ada, maka ambil stok terkini dari tabel tm_stok_unit_jahit_warna
					$data['query'] = $this->mmaster->get_stok_unit_jahit($bulan, $tahun, $unit_jahit);
					$data['is_new'] = '1';
					if (is_array($data['query']) )
						$data['jum_total'] = count($data['query']);
					else
						$data['jum_total'] = 0;
					
					$data['tgl_so'] = '';
					$data['jenis_perhitungan_stok'] ='';
					$data['isi'] = 'gudangwip/vviewsounitjahit';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 30-10-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit
												WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					$data['is_new'] = '1';
					$data['tgl_so'] = '';
					$data['id_so'] = $id_so;
					$data['isi'] = 'gudangwip/vviewsounitjahitfirst';
					$this->load->view('template',$data);
				}
			}
			else { // jika sudah diapprove maka munculkan msg
				if ($cek_data['status_approve'] == 't') {
					$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." sudah di-approve..!";
					$data['list_unit'] = $this->mmaster->get_unit_jahit();
					$data['isi'] = 'gudangwip/vformsounitjahit';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 30-10-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					// get data dari tabel tt_stok_opname_unit_jahit yg statusnya 'f'
					$data['query'] = $this->mmaster->get_all_stok_opname_unit_jahit($bulan, $tahun, $unit_jahit);
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					
					$tgl_so = $cek_data['tgl_so'];
					$pisah1 = explode("-", $tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
					
					$data['tgl_so'] = $tgl_so;
					$data['id_so'] = $id_so;
					$data['jenis_perhitungan_stok'] = $cek_data['jenis_perhitungan_stok'];
					
					// 30-10-2015 sementara dikomen dulu, karena utk input SO pertama kali
					// 22-01-2016 DIBUKA LAGI
					
					// 30-01-2016 kasih pengecekan, apakah ini SO satu2nya yg pernah diinput. kalo ya, maka munculkan firstedit
					$is_satu2nya = $this->mmaster->cek_sopertamakali($unit_jahit);
					if ($is_satu2nya == 'f')
						$data['isi'] = 'gudangwip/vviewsounitjahit';
					else
						$data['isi'] = 'gudangwip/vviewsounitjahitfirstedit';
					$this->load->view('template',$data);
					
				}
			} // end else
		} // end else bln setelahnya
	} // end else bln sebelumnya
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++

  }
  
  function submitsounitjahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_jahit = $this->input->post('unit_jahit', TRUE);  
	  // 12-03-2014 variabel $no ga perlu dipake lg, karena udh ada $jum_data
	 // $no = $this->input->post('no', TRUE);  
	 
	  // 30-10-2015 utk pertama kali, pake yg ini
	  // 22-01-2016 DITUTUP
	  //$jum_data = $this->input->post('no', TRUE)-1; 
	  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  $is_pertamakali = $this->input->post('is_pertamakali', TRUE);
	  
	  // 30-01-2016
	  if ($is_pertamakali == '1')
		$jum_data = $this->input->post('no', TRUE)-1; 
	  else
		$jum_data = $this->input->post('jum_data', TRUE);
		
	  // 05-11-2015
	  $jenis_hitung 	= $this->input->post('jenis_hitung', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
	  $hasilrow = $query3->row();
	  $kode_unit	= $hasilrow->kode_unit;
	  $nama_unit	= $hasilrow->nama;
	  
	  if ($is_new == '1') {
		  $uid_update_by = $this->session->userdata('uid');
	      // insert ke tabel tt_stok_opname_unit_jahit
	        $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$id_stok	= $seqrow->id+1;
			}else{
				$id_stok	= 1;
			}
	      $data_header = array(
			  'id'=>$id_stok,
			  'id_unit'=>$unit_jahit,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_so'=>$tgl_so,
			  'jenis_perhitungan_stok'=>$jenis_hitung,
			  'uid_update_by'=>$uid_update_by
			);
		  $this->db->insert('tt_stok_opname_unit_jahit',$data_header);
		  
		  // ambil data terakhir di tabel tt_stok_opname_
		 /*$query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id; */
	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mmaster->savesounitjahit($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_warna_'.$i, TRUE),
			 
			 //01-12-2015
			 $this->input->post('id_warna2_'.$i, TRUE),
			 $this->input->post('saldo_akhir_warna_'.$i, TRUE),
			 
			 // 22-01-2016, item brg yg mau dihapus dari tm_stok_unit_jahit
			 $unit_jahit, $this->input->post('hapusitem_'.$i, TRUE)
			 );
		  }
		  
		$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." sudah berhasil disimpan. Silahkan lakukan approval untuk mengupdate stok";
		$data['list_unit'] = $this->mmaster->get_unit_jahit();
		$data['isi'] = 'gudangwip/vformsounitjahit';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
			  // ambil data id dari tabel tt_stok_opname_unit_jahit
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit WHERE id_unit = '$unit_jahit'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 			 
			$this->db->query(" delete from tt_stok_opname_unit_jahit_detail where id_stok_opname_unit_jahit = '$id_stok' ");
			$this->db->query(" delete from tt_stok_opname_unit_jahit where id = '$id_stok' ");
			
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." sudah berhasil dihapus";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformsounitjahit';
			$this->load->view('template',$data);
		  }
		  else { // function update
			  if ($is_pertamakali == '1') {
				  $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit WHERE
								bulan = '$bulan' AND tahun = '$tahun' AND id_unit='$unit_jahit' ");
					$hasilrow = $query2->row();
					$id_stok	= $hasilrow->id;
					
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tt_stok_opname_unit_jahit SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								jenis_perhitungan_stok='$jenis_hitung', uid_update_by='$uid_update_by' where id = '$id_stok' ");
					
					// 1. update data yg sudah ada
					$jum_data_ada = $this->input->post('jum_data', TRUE); 
					for ($i=1;$i<=$jum_data_ada;$i++)
					 {															
						$id_brg_wip1 = $this->input->post('id_brg_wip1_'.$i, TRUE);
						$id_warna1 = $this->input->post('id_warna1_'.$i, TRUE);
						$qty_warna1 = $this->input->post('qty_warna1_'.$i, TRUE);
						$stok1 = $this->input->post('stok1_'.$i, TRUE);
						// 01-12-2015
						$id_warna12 = $this->input->post('id_warna12_'.$i, TRUE);
						$qty_warna12 = $this->input->post('qty_warna12_'.$i, TRUE);
						
						$queryxx	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail WHERE
								id_stok_opname_unit_jahit = '$id_stok' AND id_brg_wip = '$id_brg_wip1' ");
						$hasilxx = $queryxx->row();
						$iddetail	= $hasilxx->id;
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna1); $xx++) {
							$id_warna1[$xx] = trim($id_warna1[$xx]);
							$stok1[$xx] = trim($stok1[$xx]);
							$qtytotalstokawal+= $stok1[$xx];				
							$qty_warna1[$xx] = trim($qty_warna1[$xx]);
							$qtytotalstokfisikwarna+= $qty_warna1[$xx];
							
							// 01-12-2015
							$id_warna12[$xx] = trim($id_warna12[$xx]);
							$qty_warna12[$xx] = trim($qty_warna12[$xx]);
							$qtytotalsaldoakhirwarna+= $qty_warna12[$xx];
							
							// 04-02-2016 update di tabel perwarna
							$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail_warna SET jum_stok_opname = '".$qty_warna1[$xx]."',
									saldo_akhir = '".$qty_warna12[$xx]."' WHERE id_stok_opname_unit_jahit_detail = '$iddetail'
									AND id_warna = '".$id_warna1[$xx]."' ");
							
						} // end for
												
						$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '".$qtytotalstokfisikwarna."', 
									saldo_akhir = '".$qtytotalsaldoakhirwarna."' where id_brg_wip = '$id_brg_wip1' AND id_stok_opname_unit_jahit = '$id_stok' ");
					 } // end for
					// --------------------------------------------------------
					
					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {						
						$tgl = date("Y-m-d H:i:s"); 
						 //-------------- hitung total qty dari detail tiap2 warna -------------------
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$stok_fisik_warna = $this->input->post('stok_fisik_warna_'.$i, TRUE);
						$stok = $this->input->post('stok_'.$i, TRUE);
						// 01-12-2015
						$id_warna2 = $this->input->post('id_warna2_'.$i, TRUE);
						$saldo_akhir_warna = $this->input->post('saldo_akhir_warna_'.$i, TRUE);
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok[$xx] = trim($stok[$xx]);
							$qtytotalstokawal+= $stok[$xx];				
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
							
							// 01-12-2015
							$id_warna2[$xx] = trim($id_warna2[$xx]);
							$saldo_akhir_warna[$xx] = trim($saldo_akhir_warna[$xx]);
							$qtytotalsaldoakhirwarna+= $saldo_akhir_warna[$xx];
						} // end for
						
						$seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail ORDER BY id DESC LIMIT 1 ");
						if($seq->num_rows() > 0) {
							$seqrow	= $seq->row();
							$id_stok_detail	= $seqrow->id+1;
						}else{
							$id_stok_detail	= 1;
						}
																		
						if ($id_brg_wip != '') {
							$data_detail = array(
									'id'=>$id_stok_detail,
									'id_stok_opname_unit_jahit'=>$id_stok,
									'id_brg_wip'=>$id_brg_wip, 
									'stok_awal'=>$qtytotalstokawal,
									'jum_stok_opname'=>$qtytotalstokfisikwarna,
									'saldo_akhir'=>$qtytotalsaldoakhirwarna
								);
							$this->db->insert('tt_stok_opname_unit_jahit_detail',$data_detail);
							
							// ------------------stok berdasarkan warna brg wip----------------------------
						   if (isset($id_warna) && is_array($id_warna)) {
							for ($xx=0; $xx<count($id_warna); $xx++) {
								$id_warna[$xx] = trim($id_warna[$xx]);
								$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
								//01-12-2015
								$id_warna2[$xx] = trim($id_warna2[$xx]);
								$saldo_akhir_warna[$xx] = trim($saldo_akhir_warna[$xx]);
								
								$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
								if($seq_warna->num_rows() > 0) {
									$seqrow	= $seq_warna->row();
									$idbaru	= $seqrow->id+1;
								}else{
									$idbaru	= 1;
								}
								
								$tt_stok_opname_unit_jahit_detail_warna	= array(
										 'id'=>$idbaru,
										 'id_stok_opname_unit_jahit_detail'=>$id_stok_detail,
										 'id_warna'=>$id_warna[$xx],
										 'jum_stok_opname'=>$stok_fisik_warna[$xx],
										 'saldo_akhir'=>$saldo_akhir_warna[$xx]
									);
								$this->db->insert('tt_stok_opname_unit_jahit_detail_warna',$tt_stok_opname_unit_jahit_detail_warna);
							} // end for
						  } // end if
					  }
					// ---------------------------------------------------------------------
				} // END FOR
			  }
			  else {
				  // update ke tabel tt_stok_opname_
				  $uid_update_by = $this->session->userdata('uid');
				   // ambil data terakhir di tabel tt_stok_opname_
					 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit WHERE id_unit = '$unit_jahit' 
									AND bulan = '$bulan' AND tahun = '$tahun' ");
					 $hasilrow = $query2->row();
					 $id_stok	= $hasilrow->id; 
					 
					 $this->db->query(" UPDATE tt_stok_opname_unit_jahit SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								jenis_perhitungan_stok='$jenis_hitung', uid_update_by='$uid_update_by' where id = '$id_stok' ");
					 
				  for ($i=1;$i<=$jum_data;$i++)
				  {
					 /*$this->mmaster->savesounitjahit($is_new, $id_stok, 
					 $this->input->post('id_brg_wip_'.$i, TRUE), 
					 $this->input->post('stok_'.$i, TRUE),
					 $this->input->post('stok_fisik_'.$i, TRUE),
					 $this->input->post('saldo_akhir_'.$i, TRUE),
					 // 22-01-2016, item brg yg mau dihapus dari tm_stok_unit_jahit
						$unit_jahit, $this->input->post('hapusitem_'.$i, TRUE)
					 ); */
					 
					 $this->mmaster->savesounitjahit($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
					 $this->input->post('id_warna_'.$i, TRUE),
					 $this->input->post('stok_'.$i, TRUE),
					 $this->input->post('stok_fisik_warna_'.$i, TRUE),
					 
					 //01-12-2015
					 $this->input->post('id_warna2_'.$i, TRUE),
					 $this->input->post('saldo_akhir_warna_'.$i, TRUE),
					 
					 // 22-01-2016, item brg yg mau dihapus dari tm_stok_unit_jahit
					 $unit_jahit, $this->input->post('hapusitem_'.$i, TRUE)
					 );
				  }
			  }
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." sudah berhasil diupdate. Silahkan lakukan approval untuk mengupdate stok";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformsounitjahit';
			$this->load->view('template',$data);
		}
      }
  }
  
  function approvalsounitjahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'gudangwip/vformappsounitjahit';
	$data['msg'] = '';
	$data['list_unit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);
  }
  
  function viewappsounitjahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$unit_jahit = $this->input->post('unit_jahit', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
		$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
		
		$data['unit_jahit'] = $unit_jahit;
		$data['kode_unit'] = $kode_unit;
		$data['nama_unit'] = $nama_unit;
			
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

	$cek_data = $this->mmaster->cek_so_unit_jahit($bulan, $tahun, $unit_jahit); 
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." belum diinput..!";
		$data['list_unit'] = $this->mmaster->get_unit_jahit();
		$data['isi'] = 'gudangwip/vformappsounitjahit';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." sudah di-approve..!";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformappsounitjahit';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_ yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname_unit_jahit($bulan, $tahun, $unit_jahit);
			$data['jum_total'] = count($data['query']);
			
			$tgl_so = $cek_data['tgl_so'];
			$pisah1 = explode("-", $tgl_so);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_so = $tgl1."-".$bln1."-".$thn1;
			
			$data['tgl_so'] = $tgl_so;
			$data['jenis_hitung'] = $cek_data['jenis_perhitungan_stok'];
			if ($cek_data['jenis_perhitungan_stok'] == '1')
				$data['nama_jenis_hitung'] = "1. Sudah menghitung barang masuk dan barang keluar";
			else if ($cek_data['jenis_perhitungan_stok'] == '2')
				$data['nama_jenis_hitung'] = "2. Sudah menghitung barang masuk, barang keluar belum";
			else if ($cek_data['jenis_perhitungan_stok'] == '3')
				$data['nama_jenis_hitung'] = "3. Belum menghitung barang masuk, barang keluar sudah";
			else if ($cek_data['jenis_perhitungan_stok'] == '4')
				$data['nama_jenis_hitung'] = "4. Belum menghitung barang masuk dan barang keluar";
			
			$data['isi'] = 'gudangwip/vviewappsounitjahit';
			$this->load->view('template',$data);
		}
	} // end else
  }
  
  function submitappsounitjahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_jahit = $this->input->post('unit_jahit', TRUE);
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  
	  // 04-11-2015
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  // 01-02-2016
	  $is_satu2nya 	= $this->input->post('is_satu2nya', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan < 12) {
		$bulanplus1 = $bulan+1;
		$tahunplus1 = $tahun;
		if ($bulanplus1 < 10)
			$bulanplus1= '0'.$bulanplus1;
	  }
	  else {
		$bulanplus1 = '01';
		$tahunplus1 = $tahun+1;
	  }
	  
	  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	  
	  $tgl = date("Y-m-d H:i:s"); 
		  	      
	      for ($i=1;$i<=$no;$i++)
		  {
			  // 01-02-2016
			  if ($is_satu2nya == 't') {
				 $id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
				 $stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE);
				 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
				 $idnya = $this->input->post('id_'.$i, TRUE);
				 
				 $totalxx = $stok_fisik;
			  }
			  else {
					 $id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
					 $id_warna = $this->input->post('id_warna_'.$i, TRUE);
					 $stok_fisik = $this->input->post('stok_fisik_warna_'.$i, TRUE);
					 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
					 $idnya = $this->input->post('id_'.$i, TRUE);
					 
					 // ============== new skrip 06-02-2014 ================================================
					 //-------------- hitung total qty dari detail tiap2 warna -------------------
						$qtytotal = 0;
						$qtytotal_saldo_akhir = 0;
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok_fisik[$xx] = trim($stok_fisik[$xx]);
							$qtytotal+= $stok_fisik[$xx];
						} // end for
					// ---------------------------------------------------------------------
					
					// 26-06-2014
					$totalxx = $qtytotal;
			  }
			
			// ======== update stoknya! =============
			// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/ 
			
			// 01-02-2016 GA DIPAKE LG
			
			//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
			/*$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_bonm > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_bonm >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' AND a.jenis_keluar = '1' ";
			$query3	= $this->db->query($sql3);
						// AND a.tgl_bonm >= '$tglawalplus'
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_bgs = $hasilrow->jum_masuk;
							
				if ($masuk_bgs == '')
					$masuk_bgs = 0;
			}
			else
				$masuk_bgs = 0;
						
			//2. hitung brg masuk retur brg wip dari QC, dari tm_sjkeluarwip
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.=" AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_returbrgwip = $hasilrow->jum_masuk;
							
				if ($masuk_returbrgwip == '')
					$masuk_returbrgwip = 0;
			}
			else
				$masuk_returbrgwip = 0;
						
			//3. hitung brg masuk pengembalian dari QC, dari tm_bonmkeluarcutting
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_bonm > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_bonm >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' AND a.jenis_keluar = '2' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_pengembalian = $hasilrow->jum_masuk;
							
				if ($masuk_pengembalian == '')
					$masuk_pengembalian = 0;
			}
			else
				$masuk_pengembalian = 0;
						
			$jum_masuk = $masuk_bgs+$masuk_returbrgwip+$masuk_pengembalian;
			// ------------------------------------------ END MASUK --------------------------------
						
			// 4. hitung brg keluar bagus, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs = $hasilrow->jum_keluar;
							
				if ($keluar_bgs == '')
					$keluar_bgs = 0;
			}
			else
				$keluar_bgs = 0;
						
			// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_perbaikan = $hasilrow->jum_keluar;
							
				if ($keluar_perbaikan == '')
					$keluar_perbaikan = 0;
			}
			else
				$keluar_perbaikan = 0;
						
			// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
				if ($keluar_retur_bhnbaku == '')
					$keluar_retur_bhnbaku = 0;
			}
			else
				$keluar_retur_bhnbaku = 0;
			
			// 30-10-2015
			// 7. hitung brg keluar bgs ke gudang jadi, dari tm_sjmasukgudangjadi
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs_gudangjadi = $hasilrow->jum_keluar;
							
				if ($keluar_bgs_gudangjadi == '')
					$keluar_bgs_gudangjadi = 0;
			}
			else
				$keluar_bgs_gudangjadi = 0;
						
			$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku+$keluar_bgs_gudangjadi; */
			// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						//$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						//$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
						//$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
					
			//$jum_stok_akhir = $jum_masuk-$jum_keluar;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
				// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
			/*	$totalbagusxx = $qtytotalstokfisikbgs + $stok_akhir_bgs; //echo $stok_akhir_bgs." ";
				$totalperbaikanxx = $qtytotalstokfisikperbaikan + $stok_akhir_perbaikan; //echo $stok_akhir_perbaikan." ";
				*/
				//----------------------------------------
				// 01-02-2016 dikomen. SKRIP DIATAS GA DIPAKE LG
				//$totalxx = $totalxx + $jum_stok_akhir;
				//---------------------------------------
				
				// 09-03-2015
			/*	$totalxx_saldo_akhir = $totalxx_saldo_akhir + $jum_stok_akhir;
				$totalxx_saldo_akhir_bagus = $totalxx_saldo_akhir_bagus + $stok_akhir_bgs;
				$totalxx_saldo_akhir_perbaikan = $totalxx_saldo_akhir_perbaikan + $stok_akhir_perbaikan; */
			
				//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_jahit 
							WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$unit_jahit' ");
					if ($query3->num_rows() == 0){
						$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
						if($seqxx->num_rows() > 0) {
							$seqrow	= $seqxx->row();
							$id_stok	= $seqrow->id+1;
						}else{
							$id_stok	= 1;
						}
						
						// 30-10-2015:
						// pada waktu input SO, field stok_bagus dan stok_perbaikan di-nol-kan lagi (utk duta). nanti diterapkan juga di perusahaan lain
						
						$data_stok = array(
							'id'=>$id_stok,
							'id_brg_wip'=>$id_brg_wip,
							'id_unit'=>$unit_jahit,
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl,
							'stok_bagus'=>0,
							'stok_perbaikan'=>0
							);
						$this->db->insert('tm_stok_unit_jahit', $data_stok);
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
												
						$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$totalxx', 
						stok_bagus='0',
						stok_perbaikan='0', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_unit='$unit_jahit' ");
					}
					
				// 01-02-2016, masukkan yg per warna
				if ($is_satu2nya == 'f') {
					// stok_unit_jahit_warna
					// ----------------------------------------------
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok_fisik[$xx] = trim($stok_fisik[$xx]);
						
						$totalxx = $stok_fisik[$xx];
						
						// ========================= stok per warna ===============================================
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit_warna WHERE id_warna = '".$id_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok' ");
						if ($query3->num_rows() == 0){
							// jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							//26-06-2014, pake $totalxx dari hasil perhitungan transaksi keluar/masuk
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_unit_jahit'=>$id_stok,
								'id_warna'=>$id_warna[$xx],
								//04-03-2015 dikomen. 21-10-2015 balikin ke totalxx
								'stok'=>$totalxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
							where id_warna= '".$id_warna[$xx]."' AND id_stok_unit_jahit='$id_stok' ");
						}
					} // end for
					// ----------------------------------------------
				}
			 // ====================================================================================
			 
			 $this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET status_approve = 't'
								where id = '$idnya' ");

		  } // end for
		  $this->db->query(" UPDATE tt_stok_opname_unit_jahit SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  //die();
		    $data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit jahit ".$kode_unit." - ".$nama_unit." sudah berhasil di-approve, dan otomatis mengupdate stok";
			$data['list_unit'] = $this->mmaster->get_unit_jahit();
			$data['isi'] = 'gudangwip/vformappsounitjahit';
			$this->load->view('template',$data);
  }
  
  // 14-04-2014
  function autoupdatesounitjahit() {
	  $tgl = date("Y-m-d H:i:s"); 
	  
		$sqlxx = " SELECT id, bulan, tahun, kode_unit FROM tt_stok_opname_unit_jahit WHERE status_approve='t' ORDER BY kode_unit, tahun, bulan ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				//---------------------------------------
				if ($rowxx->bulan < 12) {
					$bulanplus1 = $rowxx->bulan+1;
					$tahunplus1 = $rowxx->tahun;
					if ($bulanplus1 < 10)
						$bulanplus1= '0'.$bulanplus1;
				  }
				  else {
					$bulanplus1 = '01';
					$tahunplus1 = $rowxx->tahun+1;
				  }
				  
				  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
				//---------------------------------------
				
				// detail item brg dan juga warnanya
				$sqlxx2 = " SELECT id, id_stok_opname_unit_jahit, kode_brg_jadi, jum_stok_opname, jum_bagus, jum_perbaikan 
							FROM tt_stok_opname_unit_jahit_detail WHERE id_stok_opname_unit_jahit='$rowxx->id' ";
				$queryxx2	= $this->db->query($sqlxx2);
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						
						//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit = '$rowxx->kode_unit' AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b
									WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadi == '')
								$masuk_returbrgjadi = 0;
						}
						else
							$masuk_returbrgjadi = 0;
						
						//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit = '$rowxx->kode_unit' AND a.jenis_keluar = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalian == '')
								$masuk_pengembalian = 0;
						}
						else
							$masuk_pengembalian = 0;
						
						$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikan == '')
								$keluar_perbaikan = 0;
						}
						else
							$keluar_perbaikan = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.kode_unit = '$rowxx->kode_unit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbaku == '')
								$keluar_retur_bhnbaku = 0;
						}
						else
							$keluar_retur_bhnbaku = 0;
						
						$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
						$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
						//------------------------------- END query cek brg masuk/keluar ------------------------------------
						
						$qtytotalstokfisikbgs = 0;
						$qtytotalstokfisikperbaikan = 0;
						$query3	= $this->db->query(" SELECT sum(jum_bagus) as jum_bagus, sum(jum_perbaikan) as jum_perbaikan 
									FROM tt_stok_opname_unit_jahit_detail_warna WHERE id_stok_opname_unit_jahit_detail='$rowxx2->id' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qtytotalstokfisikbgs = $hasilrow->jum_bagus;
							$qtytotalstokfisikperbaikan = $hasilrow->jum_perbaikan;
						}
						$totalxx = $qtytotalstokfisikbgs+$qtytotalstokfisikperbaikan;
						
						// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
						$totalbagusxx = $qtytotalstokfisikbgs + $stok_akhir_bgs; //echo $stok_akhir_bgs." ";
						$totalperbaikanxx = $qtytotalstokfisikperbaikan + $stok_akhir_perbaikan; //echo $stok_akhir_perbaikan." ";
						$totalxx = $totalxx + $jum_stok_akhir; //echo $totalxx." ";
						
						// update stok
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_bagus, stok_perbaikan FROM tm_stok_unit_jahit 
								WHERE kode_brg_jadi = '$rowxx2->kode_brg_jadi' AND kode_unit='$rowxx->kode_unit' ");
						if ($query3->num_rows() == 0){
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrow	= $seqxx->row();
								$id_stok	= $seqrow->id+1;
							}else{
								$id_stok	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok,
								'kode_brg_jadi'=>$rowxx2->kode_brg_jadi,
								'kode_unit'=>$rowxx->kode_unit,
								'stok'=>$totalxx,
								'stok_bagus'=>$totalbagusxx,
								'stok_perbaikan'=>$totalperbaikanxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
														
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$totalxx', stok_bagus='$totalbagusxx',
							stok_perbaikan='$totalperbaikanxx', tgl_update_stok = '$tgl' 
							where kode_brg_jadi= '$rowxx2->kode_brg_jadi' AND kode_unit='$rowxx->kode_unit' ");
						}
						// end update stok
						
						$sqlxx3 = " SELECT * FROM tt_stok_opname_unit_jahit_detail_warna WHERE id_stok_opname_unit_jahit_detail='$rowxx2->id' ";
						$queryxx3	= $this->db->query($sqlxx3);
						if ($queryxx3->num_rows() > 0){
							$hasilxx3 = $queryxx3->result();
							foreach ($hasilxx3 as $rowxx3) {
								$totalxx = $rowxx3->jum_bagus + $rowxx3->jum_perbaikan;
								
								//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
													tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c
													WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
													AND a.tgl_bonm >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit = '$rowxx->kode_unit' AND a.jenis_keluar = '1'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$masuk_bgs = $hasilrow->jum_masuk;
											
											if ($masuk_bgs == '')
												$masuk_bgs = 0;
										}
										else
											$masuk_bgs = 0;
										
										//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a, 
													tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c
													WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
													AND a.tgl_sj >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_keluar = '3'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$masuk_returbrgjadi = $hasilrow->jum_masuk;
											
											if ($masuk_returbrgjadi == '')
												$masuk_returbrgjadi = 0;
										}
										else
											$masuk_returbrgjadi = 0;
										
										//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
													tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c 
													WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
													AND a.tgl_bonm >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit = '$rowxx->kode_unit' AND a.jenis_keluar = '2'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$masuk_pengembalian = $hasilrow->jum_masuk;
											
											if ($masuk_pengembalian == '')
												$masuk_pengembalian = 0;
										}
										else
											$masuk_pengembalian = 0;
										
										$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
										// ------------------------------------------ END MASUK --------------------------------
										
										// 4. hitung brg keluar bagus, dari tm_sjmasukwip
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a, 
													tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
													WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
													AND a.tgl_sj >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_masuk = '1' 
													AND c.kode_warna = '".$rowxx3->kode_warna."'");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$keluar_bgs = $hasilrow->jum_keluar;
											
											if ($keluar_bgs == '')
												$keluar_bgs = 0;
										}
										else
											$keluar_bgs = 0;
										
										// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a, 
													tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
													WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
													AND a.tgl_sj >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit_jahit = '$rowxx->kode_unit' AND a.jenis_masuk = '2'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$keluar_perbaikan = $hasilrow->jum_keluar;
											
											if ($keluar_perbaikan == '')
												$keluar_perbaikan = 0;
										}
										else
											$keluar_perbaikan = 0;
										
										// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
										$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
													tm_sjmasukbhnbakupic_detail b, tm_sjmasukbhnbakupic_detail_warna c
													WHERE a.id = b.id_sjmasukbhnbakupic AND b.id = c.id_sjmasukbhnbakupic_detail
													AND a.tgl_sj >= '$tglawalplus'
													AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
													AND a.kode_unit = '$rowxx->kode_unit'
													AND c.kode_warna = '".$rowxx3->kode_warna."' ");
										if ($query3->num_rows() > 0){
											$hasilrow = $query3->row();
											$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
											
											if ($keluar_retur_bhnbaku == '')
												$keluar_retur_bhnbaku = 0;
										}
										else
											$keluar_retur_bhnbaku = 0;
										
										$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
										
										// -------------------------------- END BARANG KELUAR -----------------------------------------------
										
										$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
										$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
										$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
							//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
								// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
								$totalbagusxx = $rowxx3->jum_bagus + $stok_akhir_bgs;
								$totalperbaikanxx = $rowxx3->jum_perbaikan + $stok_akhir_perbaikan;
								$totalxx = $totalxx + $jum_stok_akhir;
								
								//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$rowxx3->kode_warna."'
										AND id_stok_unit_jahit='$id_stok' ");
								if ($query3->num_rows() == 0){
									// jika blm ada data di tm_stok_unit_jahit_warna, insert
									$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
									if($seq_stokwarna->num_rows() > 0) {
										$seq_stokwarnarow	= $seq_stokwarna->row();
										$id_stok_warna	= $seq_stokwarnarow->id+1;
									}else{
										$id_stok_warna	= 1;
									}
									
									$data_stok = array(
										'id'=>$id_stok_warna,
										'id_stok_unit_jahit'=>$id_stok,
										'id_warna_brg_jadi'=>'0',
										'kode_warna'=>$rowxx3->kode_warna,
										'stok'=>$totalxx,
										'stok_bagus'=>$totalbagusxx,
										'stok_perbaikan'=>$totalperbaikanxx,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '".$totalxx."', 
									stok_bagus = '".$totalbagusxx."', stok_perbaikan = '".$totalperbaikanxx."', tgl_update_stok = '$tgl' 
									where kode_warna= '".$rowxx3->kode_warna."' AND id_stok_unit_jahit='$id_stok' ");
								}
					
							} //end for3
						} // end if3

						
					} // end for2
				} // end if2
				
			} // end for1
		} // end if1
		echo "update stok unit jahit sukses";
  }
  
  // 07-05-2014, cuma temp aja, ga usah dipake
  function autoupdatesounitjahit2() {
	  $tgl = date("Y-m-d H:i:s"); 
	  
		$sqlxx = " SELECT id, bulan, tahun, kode_unit FROM tt_stok_opname_unit_jahit WHERE 
					status_approve='t' ORDER BY kode_unit, tahun, bulan ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				//---------------------------------------
				/*if ($rowxx->bulan < 12) {
					$bulanplus1 = $rowxx->bulan+1;
					$tahunplus1 = $rowxx->tahun;
					if ($bulanplus1 < 10)
						$bulanplus1= '0'.$bulanplus1;
				  }
				  else {
					$bulanplus1 = '01';
					$tahunplus1 = $rowxx->tahun+1;
				  }
				  
				  $tglawalplus = $tahunplus1."-".$bulanplus1."-01"; */
				//---------------------------------------
				
				// detail item brg dan juga warnanya
				$sqlxx2 = " SELECT id, id_stok_opname_unit_jahit, kode_brg_jadi, jum_stok_opname, jum_bagus, jum_perbaikan 
							FROM tt_stok_opname_unit_jahit_detail WHERE id_stok_opname_unit_jahit='$rowxx->id' ";
				$queryxx2	= $this->db->query($sqlxx2);
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						$sqlxx3 = " SELECT sum(jum_bagus) as totdetail FROM tt_stok_opname_unit_jahit_detail_warna 
									WHERE id_stok_opname_unit_jahit_detail='$rowxx2->id' ";
						$queryxx3	= $this->db->query($sqlxx3);
						if ($queryxx3->num_rows() > 0){
							$hasilrowxx3 = $queryxx3->row();
							$totdetail = $hasilrowxx3->totdetail;
							//echo $totdetail." ".$rowxx2->jum_stok_opname."<br>";
							if ($totdetail != $rowxx2->jum_bagus)
								echo $rowxx->kode_unit." ".$rowxx2->kode_brg_jadi." ".$rowxx2->jum_bagus." ".$totdetail."<br>";
						} // end if3

						
					} // end for2
				} // end if2
				
			} // end for1
		} // end if1
  }
  
  // 22-01-2015, AUTOUPDATESOHASILJAHIT (cek 20-02-2015, ini ga akurat. GA USAH DIPAKE. PAKENYA YG DI app-stok-opname-hsl-jahit AJA)
  function autoupdatesohasiljahit() {
	  $tgl = date("Y-m-d H:i:s"); 
	  $gudang 	= $this->uri->segment(4);
	  
		$sqlxx = " SELECT id, bulan, tahun, id_gudang, tgl_so FROM tt_stok_opname_hasil_jahit WHERE status_approve='t' 
					AND id_gudang='$gudang' ORDER BY id_gudang, tahun, bulan "; //echo $sqlxx; die();
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				//---------------------------------------
				/*if ($rowxx->bulan < 12) {
					$bulanplus1 = $rowxx->bulan+1;
					$tahunplus1 = $rowxx->tahun;
					if ($bulanplus1 < 10)
						$bulanplus1= '0'.$bulanplus1;
				  }
				  else {
					$bulanplus1 = '01';
					$tahunplus1 = $rowxx->tahun+1;
				  }
				  
				  $tglawalplus = $tahunplus1."-".$bulanplus1."-01"; */
				//---------------------------------------
				
				// detail item brg dan juga warnanya
				$sqlxx2 = " SELECT id, id_stok_opname_hasil_jahit, kode_brg_jadi, jum_stok_opname
							FROM tt_stok_opname_hasil_jahit_detail WHERE id_stok_opname_hasil_jahit='$rowxx->id' ";
				$queryxx2	= $this->db->query($sqlxx2);
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						
						//1. hitung brg keluar dari gudang WIP ke packing/gd jadi, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b
									WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= '$rowxx->tgl_so'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.id_gudang = '$gudang' ");
						// AND a.tgl_sj >= '$tglawalplus'
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluarnya = $hasilrow->jum_keluar;
							
							if ($keluarnya == '')
								$keluarnya = 0;
						}
						else
							$keluarnya = 0;
						
						// 2. hitung brg masuk ke gudang WIP, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= '$rowxx->tgl_so'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.id_gudang = '$gudang' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuknya = $hasilrow->jum_masuk;
							
							if ($masuknya == '')
								$masuknya = 0;
						}
						else
							$masuknya = 0;
					
					$jum_stok_akhir = $masuknya-$keluarnya;
					//$totalxx = $totalxx + $jum_stok_akhir;			
					//------------------------ END HITUNG TRANSAKSI di sjkeluarwip dan sjmasukwip ------------------------------------------------
						
					//------------------------------- END query cek brg masuk/keluar ------------------------------------
						
						$query3	= $this->db->query(" SELECT sum(jum_stok_opname) as jum_so FROM tt_stok_opname_hasil_jahit_detail_warna 
													WHERE id_stok_opname_hasil_jahit_detail='$rowxx2->id' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$totalxx = $hasilrow->jum_so;
						}
						else
							$totalxx = 0;
						
						// tambahkan stok_akhir ini ke qty SO
						//$totalbagusxx = $qtytotalstokfisikbgs + $stok_akhir_bgs; //echo $stok_akhir_bgs." ";
						//$totalperbaikanxx = $qtytotalstokfisikperbaikan + $stok_akhir_perbaikan; //echo $stok_akhir_perbaikan." ";
						
						$totalxx = $totalxx + $jum_stok_akhir; //echo $totalxx." ";
						
						// update stok
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit 
								WHERE kode_brg_jadi = '$rowxx2->kode_brg_jadi' AND id_gudang='$gudang' ");
						if ($query3->num_rows() == 0){
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrow	= $seqxx->row();
								$id_stok	= $seqrow->id+1;
							}else{
								$id_stok	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok,
								'kode_brg_jadi'=>$rowxx2->kode_brg_jadi,
								'id_gudang'=>$gudang,
								'stok'=>$totalxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_jahit', $data_stok);
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
														
							$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$totalxx', tgl_update_stok = '$tgl' 
							where kode_brg_jadi= '$rowxx2->kode_brg_jadi' AND id_gudang='$gudang' ");
						}
						// end update stok
						
						$sqlxx3 = " SELECT * FROM tt_stok_opname_hasil_jahit_detail_warna WHERE id_stok_opname_hasil_jahit_detail='$rowxx2->id' ";
						$queryxx3	= $this->db->query($sqlxx3);
						if ($queryxx3->num_rows() > 0){
							$hasilxx3 = $queryxx3->result();
							foreach ($hasilxx3 as $rowxx3) {
								$totalxx = $rowxx3->jum_stok_opname;
								
								//1. hitung brg keluar dari gudang WIP ke packing/gd jadi, dari tm_sjkeluarwip
								$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a, 
												tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c
												WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
												AND a.tgl_sj >= '$rowxx->tgl_so'
												AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
												AND a.id_gudang = '$gudang'
												AND c.kode_warna = '".$rowxx3->kode_warna."' ");
								
									if ($query3->num_rows() > 0){
										$hasilrow = $query3->row();
										$keluarnya = $hasilrow->jum_keluar;
										
										if ($keluarnya == '')
											$keluarnya = 0;
									}
									else
										$keluarnya = 0;
								
								// 2. hitung brg masuk ke gudang WIP, dari tm_sjmasukwip
								$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a, 
												tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
												WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
												AND a.tgl_sj >= '$rowxx->tgl_so'
												AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
												AND a.id_gudang = '$gudang' 
												AND c.kode_warna = '".$rowxx3->kode_warna."'");
								
									if ($query3->num_rows() > 0){
										$hasilrow = $query3->row();
										$masuknya = $hasilrow->jum_masuk;
										
										if ($masuknya == '')
											$masuknya = 0;
									}
									else
										$masuknya = 0;
								
								$jum_stok_akhir = $masuknya-$keluarnya;
								// ------------ end hitung transaksi keluar/masuk ------------------------------
										
								//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
								
								// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
								$totalxx = $totalxx + $jum_stok_akhir;
								
								//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE kode_warna = '".$rowxx3->kode_warna."'
										AND id_stok_hasil_jahit='$id_stok' ");
								if ($query3->num_rows() == 0){
									// jika blm ada data di tm_stok_hasil_jahit_warna, insert
									$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
									if($seq_stokwarna->num_rows() > 0) {
										$seq_stokwarnarow	= $seq_stokwarna->row();
										$id_stok_warna	= $seq_stokwarnarow->id+1;
									}else{
										$id_stok_warna	= 1;
									}
									
									$data_stok = array(
										'id'=>$id_stok_warna,
										'id_stok_hasil_jahit'=>$id_stok,
										'id_warna_brg_jadi'=>'0',
										'kode_warna'=>$rowxx3->kode_warna,
										'stok'=>$totalxx,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '".$totalxx."', 
									tgl_update_stok = '$tgl' 
									where kode_warna= '".$rowxx3->kode_warna."' AND id_stok_hasil_jahit='$id_stok' ");
								}
					
							} //end for3
						} // end if3

						
					} // end for2
				} // end if2
				
			} // end for1
		} // end if1
		echo "update stok hasil jahit sukses";
  }
  
  // 30-10-2015
  function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$isedit 	= $this->input->post('isedit', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		if ($isedit == '0') {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
										WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		else {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
			}
			else {
				$id_brg_wip = '0';
			}
			
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
					WHERE id = '".$id_brg_wip."' AND id NOT IN 
					(SELECT b.id_brg_wip FROM tt_stok_opname_unit_jahit a 
					INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
					WHERE a.id = '$id_so' ) ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		
		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('gudangwip/vinfobrgwip', $data); 
		return true;
  }
  
  // 31-10-2015 SO UNIT PACKING
  function sounitpacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$data['isi'] = 'gudangwip/vformsounitpacking';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$data['list_unit'] = $this->mmaster->get_unit_packing();
	$this->load->view('template',$data);

  }
  
  function viewsounitpacking(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$unit_packing = $this->input->post('unit_packing', TRUE);  

	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
			
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	
	$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
	$hasilrow = $query3->row();
	$kode_unit	= $hasilrow->kode_unit;
	$nama_unit	= $hasilrow->nama;
	
	$sql = "SELECT id FROM tt_stok_opname_unit_packing WHERE status_approve = 'f' AND id_unit = '$unit_packing' ";
	if ($bulan == 1) // 24-03-2014
		//$sql.= " AND ((bulan <= '$bulan_sebelumnya' AND bulan<>'$bulan' AND tahun ='$tahun') OR (bulan<='12' AND tahun<'$tahun')) ";
		$sql.= " AND bulan<='12' AND tahun<'$tahun' ";
	else
		$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";
	//echo $sql; die();
	$query3	= $this->db->query($sql);
	
	// 18-01-2016 SEMENTARA DIKOMEN UTK BERESIN DATA SO NOV DAN DES 2015
	// 29-01-2016 dibuka lagi
	if ($query3->num_rows() > 0){
		$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
		$data['list_unit'] = $this->mmaster->get_unit_packing();
		$data['isi'] = 'gudangwip/vformsounitpacking';
		$this->load->view('template',$data);
	}
	else {
		// cek apakah sudah ada data SO di bulan setelahnya
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE bulan > '$bulan' AND tahun >= '$tahun' 
							AND id_unit = '$unit_packing' ");

		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
			$data['list_unit'] = $this->mmaster->get_unit_packing();
			$data['isi'] = 'gudangwip/vformsounitpacking';
			$this->load->view('template',$data);
		}
		else {
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['unit_packing'] = $unit_packing;
			$data['kode_unit'] = $kode_unit;
			$data['nama_unit'] = $nama_unit;

			$cek_data = $this->mmaster->cek_so_unit_packing($bulan, $tahun, $unit_packing); 
			
			if ($cek_data['idnya'] == '' ) { 
				// 30-10-2015, CEK APAKAH DATA DI TABEL SO MASIH KOSONG? JIKA MASIH KOSONG, MAKA MUNCULKAN FORM INPUT BARU
				$is_sokosong = $this->mmaster->cek_sokosong_unitpacking($unit_packing); 
				
				// 11-01-2016 SEMENTARA MASIH PAKE CARA PERTAMAKALI, KHUSUS DATA NOV DAN DES 2015
				// 29-01-2016 dibuka lagi
				if ($is_sokosong == 'f') { // JIKA SUDAH ADA SO
					// jika data so blm ada, maka ambil stok terkini dari tabel tm_stok_unit_packing_warna
					$data['query'] = $this->mmaster->get_stok_unit_packing($bulan, $tahun, $unit_packing);
					$data['is_new'] = '1';
					if (is_array($data['query']) )
						$data['jum_total'] = count($data['query']);
					else
						$data['jum_total'] = 0;
					
					$data['tgl_so'] = '';
					$data['jenis_perhitungan_stok'] = '';
					$data['isi'] = 'gudangwip/vviewsounitpacking';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 30-10-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing
												WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					$data['is_new'] = '1';
					$data['tgl_so'] = '';
					$data['id_so'] = $id_so;
					$data['isi'] = 'gudangwip/vviewsounitpackingfirst';
					$this->load->view('template',$data);
				}
			}
			else { // jika sudah diapprove maka munculkan msg
				if ($cek_data['status_approve'] == 't') {
					$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah di-approve..!";
					$data['list_unit'] = $this->mmaster->get_unit_packing();
					$data['isi'] = 'gudangwip/vformsounitpacking';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 30-10-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					// get data dari tabel tt_stok_opname_unit_jahit yg statusnya 'f'
					$data['query'] = $this->mmaster->get_all_stok_opname_unit_packing($bulan, $tahun, $unit_packing);
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					
					$tgl_so = $cek_data['tgl_so'];
					$pisah1 = explode("-", $tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
					
					$data['tgl_so'] = $tgl_so;
					$data['id_so'] = $id_so;
					$data['jenis_perhitungan_stok'] = $cek_data['jenis_perhitungan_stok'];
					
					// 30-10-2015 sementara dikomen dulu, karena utk input SO pertama kali
					// 29-01-2016 dibuka lagi pake cara input SO semula
					//$data['isi'] = 'gudangwip/vviewsounitpacking';
					//$data['isi'] = 'gudangwip/vviewsounitpackingfirstedit';
					
					// 30-01-2016 kasih pengecekan, apakah ini SO satu2nya yg pernah diinput. kalo ya, maka munculkan firstedit
					$is_satu2nya = $this->mmaster->cek_sopertamakali_unitpacking($unit_packing);
					if ($is_satu2nya == 'f')
						$data['isi'] = 'gudangwip/vviewsounitpacking';
					else
						$data['isi'] = 'gudangwip/vviewsounitpackingfirstedit';
					
					$this->load->view('template',$data);
					
				}
			} // end else
		} // end else bln setelahnya
	} // end else bln sebelumnya
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++
  }
  
  function submitsounitpacking() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_packing = $this->input->post('unit_packing', TRUE);  
	  // 12-03-2014 variabel $no ga perlu dipake lg, karena udh ada $jum_data
	 // $no = $this->input->post('no', TRUE);  
	 
	  $jum_data = $this->input->post('jum_data', TRUE);
	  // 30-10-2015 utk pertama kali, pake yg ini
	  // 29-01-2016 ditutup
	  //$jum_data = $this->input->post('no', TRUE)-1; 
	  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  $is_pertamakali = $this->input->post('is_pertamakali', TRUE);
	  // 04-11-2015
	  $jenis_hitung 	= $this->input->post('jenis_hitung', TRUE);
	  
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
	  $hasilrow = $query3->row();
	  $kode_unit	= $hasilrow->kode_unit;
	  $nama_unit	= $hasilrow->nama;
	  
	  if ($is_new == '1') {
		  $uid_update_by = $this->session->userdata('uid');
	      // insert ke tabel tt_stok_opname_
	        $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$id_stok	= $seqrow->id+1;
			}else{
				$id_stok	= 1;
			}
	      $data_header = array(
			  'id'=>$id_stok,
			  'id_unit'=>$unit_packing,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_so'=>$tgl_so,
			  'jenis_perhitungan_stok'=>$jenis_hitung,
			  'uid_update_by'=>$uid_update_by
			);
		  $this->db->insert('tt_stok_opname_unit_packing',$data_header);
		  	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 /*$this->mmaster->savesounitpacking($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE),
			 //01-12-2015
			 $this->input->post('saldo_akhir_'.$i, TRUE),
			  // 29-01-2016, item brg yg mau dihapus dari tm_stok_unit_packing
			 $unit_packing, $this->input->post('hapusitem_'.$i, TRUE)
			 ); */
			 
			 $this->mmaster->savesounitpacking($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_warna_'.$i, TRUE),
			 // 01-12-2015.
			 $this->input->post('id_warna2_'.$i, TRUE),
			 $this->input->post('saldo_akhir_warna_'.$i, TRUE),
			 // 29-01-2016, item brg yg mau dihapus dari tm_stok_unit_packing
			 $unit_packing, $this->input->post('hapusitem_'.$i, TRUE)
			 );
		  }
		  
		$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah berhasil disimpan. Silahkan lakukan approval untuk mengupdate stok";
		$data['list_unit'] = $this->mmaster->get_unit_packing();
		$data['isi'] = 'gudangwip/vformsounitpacking';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
			  // ambil data id dari tabel tt_stok_opname_unit_packing
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE id_unit = '$unit_packing'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 			 
			// query ke tabel tt_stok_opname_unit_packing_detail utk hapus tiap2 item warnanya
			 $sqlxx	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail
								WHERE id_stok_opname_unit_packing = '$id_stok' ");
			if ($sqlxx->num_rows() > 0){
				$hasilxx = $sqlxx->result();
				foreach ($hasilxx as $rowxx) {
					$this->db->query(" DELETE FROM tt_stok_opname_unit_packing_detail_warna WHERE id_stok_opname_unit_packing_detail='$rowxx->id' ");
				} // end foreach detail
			} // end if
			 //==============================================================================================
			
			$this->db->query(" delete from tt_stok_opname_unit_packing_detail where id_stok_opname_unit_packing = '$id_stok' ");
			$this->db->query(" delete from tt_stok_opname_unit_packing where id = '$id_stok' ");
			
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah berhasil dihapus";
			$data['list_unit'] = $this->mmaster->get_unit_packing();
			$data['isi'] = 'gudangwip/vformsounitpacking';
			$this->load->view('template',$data);
		  }
		  else { // function update
			if ($is_pertamakali == '1') {
				  $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE
								bulan = '$bulan' AND tahun = '$tahun' AND id_unit='$unit_packing' ");
					$hasilrow = $query2->row();
					$id_stok	= $hasilrow->id;
					
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tt_stok_opname_unit_packing SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								jenis_perhitungan_stok='$jenis_hitung', uid_update_by='$uid_update_by' where id = '$id_stok' ");
					
					// 1. update data yg sudah ada
					$jum_data_ada = $this->input->post('jum_data', TRUE); 
					for ($i=1;$i<=$jum_data_ada;$i++)
					 {									
						$id_brg_wip1 = $this->input->post('id_brg_wip1_'.$i, TRUE);
						$id_warna1 = $this->input->post('id_warna1_'.$i, TRUE);
						$qty_warna1 = $this->input->post('qty_warna1_'.$i, TRUE);
						$stok1 = $this->input->post('stok1_'.$i, TRUE);
						// 01-12-2015
						$id_warna12 = $this->input->post('id_warna12_'.$i, TRUE);
						$qty_warna12 = $this->input->post('qty_warna12_'.$i, TRUE);
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna1); $xx++) {
							$id_warna1[$xx] = trim($id_warna1[$xx]);
							$stok1[$xx] = trim($stok1[$xx]);
							$qtytotalstokawal+= $stok1[$xx];				
							$qty_warna1[$xx] = trim($qty_warna1[$xx]);
							$qtytotalstokfisikwarna+= $qty_warna1[$xx];
							
							// 01-12-2015
							$id_warna12[$xx] = trim($id_warna12[$xx]);
							$qty_warna12[$xx] = trim($qty_warna12[$xx]);
							//$qtytotalsaldoakhirwarna+= $qty_warna12[$xx];
						} // end for
												
						$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail SET jum_stok_opname = '".$qtytotalstokfisikwarna."', 
									saldo_akhir = '0' where id_brg_wip = '$id_brg_wip1' AND id_stok_opname_unit_packing = '$id_stok' ");
					 } // end for
					// --------------------------------------------------------
					
					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {						
						$tgl = date("Y-m-d H:i:s"); 
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$stok_fisik_warna = $this->input->post('stok_fisik_warna_'.$i, TRUE);
						$stok = $this->input->post('stok_'.$i, TRUE);
						// 01-12-2015
						$id_warna2 = $this->input->post('id_warna2_'.$i, TRUE);
						$saldo_akhir_warna = $this->input->post('saldo_akhir_warna_'.$i, TRUE);
						
						$seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
						if($seq->num_rows() > 0) {
							$seqrow	= $seq->row();
							$id_stok_detail	= $seqrow->id+1;
						}else{
							$id_stok_detail	= 1;
						}
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						//$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok[$xx] = trim($stok[$xx]);
							$qtytotalstokawal+= $stok[$xx];				
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
							
							// 01-12-2015
							$id_warna2[$xx] = trim($id_warna2[$xx]);
							$saldo_akhir_warna[$xx] = trim($saldo_akhir_warna[$xx]);
							//$qtytotalsaldoakhirwarna+= $saldo_akhir_warna[$xx];
						} // end for
																		
						$data_detail = array(
									'id'=>$id_stok_detail,
									'id_stok_opname_unit_packing'=>$id_stok,
									'id_brg_wip'=>$id_brg_wip, 
									'stok_awal'=>$qtytotalstokawal,
									'jum_stok_opname'=>$qtytotalstokfisikwarna,
									'saldo_akhir'=>0
								);
					   $this->db->insert('tt_stok_opname_unit_packing_detail',$data_detail);
					   
					   // 05-11-2015
					   $seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
					   if($seq_detail->num_rows() > 0) {
							$seqrow	= $seq_detail->row();
							$iddetail = $seqrow->id;
					   }
					   else
							$iddetail = 0;
							
					   // ------------------stok berdasarkan warna brg wip----------------------------
					   if (isset($id_warna) && is_array($id_warna)) {
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							//01-12-2015
							$id_warna2[$xx] = trim($id_warna2[$xx]);
							//$saldo_akhir_warna[$xx] = trim($saldo_akhir_warna[$xx]);
							
							$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_warna->num_rows() > 0) {
								$seqrow	= $seq_warna->row();
								$idbaru	= $seqrow->id+1;
							}else{
								$idbaru	= 1;
							}
							
							$tt_stok_opname_unit_packing_detail_warna	= array(
									 'id'=>$idbaru,
									 'id_stok_opname_unit_packing_detail'=>$iddetail,
									 'id_warna'=>$id_warna[$xx],
									 'jum_stok_opname'=>$stok_fisik_warna[$xx],
									 'saldo_akhir'=>0
								);
							$this->db->insert('tt_stok_opname_unit_packing_detail_warna',$tt_stok_opname_unit_packing_detail_warna);
						} // end for
					  }
					// ---------------------------------------------------------------------
				} // END FOR
			  }
			  else {
				  // update ke tabel tt_stok_opname_
				   // ambil data terakhir di tabel tt_stok_opname_
					 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE id_unit = '$unit_packing' 
									AND bulan = '$bulan' AND tahun = '$tahun' ");
					 $hasilrow = $query2->row();
					 $id_stok	= $hasilrow->id; 
					 
					 $uid_update_by = $this->session->userdata('uid');
					 $this->db->query(" UPDATE tt_stok_opname_unit_packing SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								jenis_perhitungan_stok='$jenis_hitung', uid_update_by='$uid_update_by' 
								where id = '$id_stok' ");
					 
				  for ($i=1;$i<=$jum_data;$i++)
				  {
					 $this->mmaster->savesounitpacking($is_new, $id_stok, $is_pertamakali,
					 $this->input->post('id_brg_wip_'.$i, TRUE), 
					 $this->input->post('id_warna_'.$i, TRUE),
					 $this->input->post('stok_'.$i, TRUE),
					 $this->input->post('stok_fisik_warna_'.$i, TRUE),
					 // 01-12-2015
					 $this->input->post('id_warna2_'.$i, TRUE),
					 $this->input->post('saldo_akhir_warna_'.$i, TRUE),
					 // 29-01-2016, item brg yg mau dihapus dari tm_stok_unit_packing
					$unit_packing, $this->input->post('hapusitem_'.$i, TRUE)
					 );
				  }
			  }
			
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah berhasil diupdate. Silahkan lakukan approval untuk mengupdate stok";
			$data['list_unit'] = $this->mmaster->get_unit_packing();
			$data['isi'] = 'gudangwip/vformsounitpacking';
			$this->load->view('template',$data);
		}
      }
  }
  
  function caribrgwippacking(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$isedit 	= $this->input->post('isedit', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		if ($isedit == '0') {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
										WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		else {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
			}
			else {
				$id_brg_wip = '0';
			}
			
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
					WHERE id = '".$id_brg_wip."' AND id NOT IN 
					(SELECT b.id_brg_wip FROM tt_stok_opname_unit_packing a 
					INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
					WHERE a.id = '$id_so' ) ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		
		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('gudangwip/vinfobrgwip', $data); 
		return true;
  }
  
  function approvalsounitpacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'gudangwip/vformappsounitpacking';
	$data['msg'] = '';
	$data['list_unit'] = $this->mmaster->get_unit_packing();
	$this->load->view('template',$data);
  }
  
  function viewappsounitpacking(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$unit_packing = $this->input->post('unit_packing', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
		$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_packing WHERE id = '$unit_packing' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
		
		$data['unit_packing'] = $unit_packing;
		$data['kode_unit'] = $kode_unit;
		$data['nama_unit'] = $nama_unit;
			
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

	$cek_data = $this->mmaster->cek_so_unit_packing($bulan, $tahun, $unit_packing); 
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." belum diinput..!";
		$data['list_unit'] = $this->mmaster->get_unit_packing();
		$data['isi'] = 'gudangwip/vformappsounitpacking';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah di-approve..!";
			$data['list_unit'] = $this->mmaster->get_unit_packing();
			$data['isi'] = 'gudangwip/vformappsounitpacking';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_ yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname_unit_packing($bulan, $tahun, $unit_packing);
			$data['jum_total'] = count($data['query']);
			
			$tgl_so = $cek_data['tgl_so'];
			$pisah1 = explode("-", $tgl_so);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_so = $tgl1."-".$bln1."-".$thn1;
			
			$data['tgl_so'] = $tgl_so;
			
			$data['jenis_hitung'] = $cek_data['jenis_perhitungan_stok'];
			if ($cek_data['jenis_perhitungan_stok'] == '1')
				$data['nama_jenis_hitung'] = "1. Sudah menghitung barang masuk dan barang keluar";
			else if ($cek_data['jenis_perhitungan_stok'] == '2')
				$data['nama_jenis_hitung'] = "2. Sudah menghitung barang masuk, barang keluar belum";
			else if ($cek_data['jenis_perhitungan_stok'] == '3')
				$data['nama_jenis_hitung'] = "3. Belum menghitung barang masuk, barang keluar sudah";
			else if ($cek_data['jenis_perhitungan_stok'] == '4')
				$data['nama_jenis_hitung'] = "4. Belum menghitung barang masuk dan barang keluar";
				
			// 01-02-2016
			$is_satu2nya = $this->mmaster->cek_sopertamakali_unitpacking($unit_packing);
			if ($is_satu2nya == 'f')
				$data['isi'] = 'gudangwip/vviewappsounitpacking';
			else
				$data['isi'] = 'gudangwip/vviewappsounitpackingglobal';
			
			//$data['isi'] = 'gudangwip/vviewappsounitpacking';
			$this->load->view('template',$data);
		}
	} // end else
  }
  
  function submitappsounitpacking() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		  redirect('loginform');
	  }
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $unit_packing = $this->input->post('unit_packing', TRUE);
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  
	  // 05-11-2015
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  // 01-02-2016
	  $is_satu2nya 	= $this->input->post('is_satu2nya', TRUE);
	  
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan < 12) {
		$bulanplus1 = $bulan+1;
		$tahunplus1 = $tahun;
		if ($bulanplus1 < 10)
			$bulanplus1= '0'.$bulanplus1;
	  }
	  else {
		$bulanplus1 = '01';
		$tahunplus1 = $tahun+1;
	  }
	  
	  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	  
	  $tgl = date("Y-m-d H:i:s"); 
		  	      
	      for ($i=1;$i<=$no;$i++)
		  {
			  // 01-02-2016
			  if ($is_satu2nya == 't') {
				 $id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
				 $stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE);
				 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
				 $idnya = $this->input->post('id_'.$i, TRUE);
							
				 $totalxx = $stok_fisik;
			 }
			 else {
				 $id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
				 $id_warna = $this->input->post('id_warna_'.$i, TRUE);
				 $stok_fisik = $this->input->post('stok_fisik_warna_'.$i, TRUE);
				 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
				 $idnya = $this->input->post('id_'.$i, TRUE);
				 
				 //-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				$qtytotal_saldo_akhir = 0;
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					$qtytotal+= $stok_fisik[$xx];
				} // end for
				// ---------------------------------------------------------------------
				
				$totalxx = $qtytotal;
			 }
			
			// ======== update stoknya! =============
			// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/ 
			
			// sampe sini 12:13 31-10-2015. perhitungan transaksi nanti dilanjut menggunakan relasi ke tabel2 terkait unit packing
			// lanjut 02-11-2015. lanjut 05-11-2015 pake per warna
			// 01-02-2016 GA DIPAKE LG
			
			//1. hitung brg masuk bagus (jenis=1) dari tm_sjkeluarwip
		/*	$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
						INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
						WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
			
			$sql3.=" AND b.id_brg_wip = '$id_brg_wip' AND a.id_unit_packing = '$unit_packing' AND a.jenis_keluar = '1' ";
				
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_bgs = $hasilrow->jum_masuk;
							
				if ($masuk_bgs == '')
					$masuk_bgs = 0;
			}
			else
				$masuk_bgs = 0;
			
			$jum_masuk = $masuk_bgs;
			// ------------------------------------------ END MASUK --------------------------------
						
			// 2. hitung brg keluar bagus, dari tm_sjmasukgudangjadi
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_packing = '$unit_packing' AND a.jenis_masuk = '2' ";
					
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs = $hasilrow->jum_keluar;
							
				if ($keluar_bgs == '')
					$keluar_bgs = 0;
			}
			else
				$keluar_bgs = 0;
						
			// 3. hitung brg keluar retur, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_packing = '$unit_packing' AND a.jenis_masuk = '3' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_retur = $hasilrow->jum_keluar;
							
				if ($keluar_retur == '')
					$keluar_retur = 0;
			}
			else
				$keluar_retur = 0;
															
			$jum_keluar = $keluar_bgs+$keluar_retur;
			// -------------------------------- END BARANG KELUAR -----------------------------------------------
											
			$jum_stok_akhir = $jum_masuk-$jum_keluar;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
			*/
				// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
				//$totalxx = $totalxx + $jum_stok_akhir;
							
				//cek stok terakhir tm_stok_unit_packing, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_packing 
							WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$unit_packing' ");
					if ($query3->num_rows() == 0){
						$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_packing ORDER BY id DESC LIMIT 1 ");
						if($seqxx->num_rows() > 0) {
							$seqrow	= $seqxx->row();
							$id_stok	= $seqrow->id+1;
						}else{
							$id_stok	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok,
							'id_brg_wip'=>$id_brg_wip,
							'id_unit'=>$unit_packing,
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_packing', $data_stok);
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
												
						$this->db->query(" UPDATE tm_stok_unit_packing SET stok = '$totalxx', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_unit='$unit_packing' ");
					}
			
			// 01-02-2016, masukkan yg per warna
			if ($is_satu2nya == 'f') {
				// stok_unit_packing_warna
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					
					$totalxx = $stok_fisik[$xx];
					
					//------------------------------------------------------------------------------
					// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
					// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
					// 01-02-2016 GA DIPAKE LG
					
					//1. hitung brg masuk bagus (jenis=1) dari tm_sjkeluarwip
				/*	$sql3 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a
								INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
								INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
								WHERE ";
					if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
						$sql3.=" a.tgl_sj > '$tgl_so' ";
					else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						$sql3.=" a.tgl_sj >= '$tgl_so' ";
					
					$sql3.=" AND b.id_brg_wip = '$id_brg_wip' AND a.id_unit_packing = '$unit_packing' 
							AND a.jenis_keluar = '1' AND c.id_warna = '".$id_warna[$xx]."' ";
						
					$query3	= $this->db->query($sql3);
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$masuk_bgs = $hasilrow->jum_masuk;
									
						if ($masuk_bgs == '')
							$masuk_bgs = 0;
					}
					else
						$masuk_bgs = 0;
					
					$jum_masuk = $masuk_bgs;
					
					// SAMPE SINI 05-11-2015 2:01
					// LANJUT 06-11-2015 8:35
					// ------------------------------------------ END MASUK --------------------------------
					
					// 2. hitung brg keluar bagus, dari tm_sjmasukgudangjadi
					$sql3 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
							INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
							INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
							WHERE ";
					if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
						$sql3.=" a.tgl_sj > '$tgl_so' ";
					else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
						$sql3.=" a.tgl_sj >= '$tgl_so' ";
						
					$sql3.= " AND b.id_brg_wip = '$id_brg_wip' AND a.id_unit_packing = '$unit_packing' 
							AND a.jenis_masuk = '2' AND c.id_warna = '".$id_warna[$xx]."' ";
							
					$query3	= $this->db->query($sql3);
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$keluar_bgs = $hasilrow->jum_keluar;
									
						if ($keluar_bgs == '')
							$keluar_bgs = 0;
					}
					else
						$keluar_bgs = 0;
								
					// 3. hitung brg keluar retur, dari tm_sjmasukwip
					$sql3 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
							WHERE ";
					if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
						$sql3.=" a.tgl_sj > '$tgl_so' ";
					else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
						$sql3.=" a.tgl_sj >= '$tgl_so' ";
						
					$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
							AND a.id_unit_packing = '$unit_packing' AND a.jenis_masuk = '3'
							AND c.id_warna = '".$id_warna[$xx]."' ";
					$query3	= $this->db->query($sql3);
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$keluar_retur = $hasilrow->jum_keluar;
									
						if ($keluar_retur == '')
							$keluar_retur = 0;
					}
					else
						$keluar_retur = 0;
																	
					$jum_keluar = $keluar_bgs+$keluar_retur;
					// -------------------------------- END BARANG KELUAR -----------------------------------------------
													
					$jum_stok_akhir = $jum_masuk-$jum_keluar;
					$totalxx = $totalxx + $jum_stok_akhir;
					//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
					*/
					
					// ========================= stok per warna ===============================================
					//cek stok terakhir tm_stok_unit_packing_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_packing_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_unit_packing='$id_stok' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_hasil_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_packing_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						//pake $totalxx dari hasil perhitungan transaksi keluar/masuk
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_unit_packing'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							//'stok'=>$stok_fisik[$xx],
							//04-03-2015 dikomen. 21-10-2015 balikin ke totalxx
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_packing_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_unit_packing_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_unit_packing='$id_stok' ");
					}
				} // end for
				// ----------------------------------------------
			}
			 
			 $this->db->query(" UPDATE tt_stok_opname_unit_packing_detail SET status_approve = 't'
								where id = '$idnya' ");

		  } // end for
		  $this->db->query(" UPDATE tt_stok_opname_unit_packing SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  
		    $data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di unit packing ".$kode_unit." - ".$nama_unit." sudah berhasil di-approve, dan otomatis mengupdate stok";
			$data['list_unit'] = $this->mmaster->get_unit_packing();
			$data['isi'] = 'gudangwip/vformappsounitpacking';
			$this->load->view('template',$data);
  }
  
  // 05-11-2015
  function additemwarna(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		if ($id_so != '') {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							AND a.id_brg_wip NOT IN 
						(SELECT b.id_brg_wip FROM tt_stok_opname_unit_packing a 
						INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
						WHERE a.id = '$id_so' )
							ORDER BY b.nama");
		}
		else {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							ORDER BY b.nama");
		}
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('gudangwip/vlistwarna', $data); 
		return true;
  }
  
  //01-12-2015
  function additemwarna2(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		if ($id_so != '') {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							AND a.id_brg_wip NOT IN 
						(SELECT b.id_brg_wip FROM tt_stok_opname_unit_packing a 
						INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
						WHERE a.id = '$id_so' )
							ORDER BY b.nama");
		}
		else {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							ORDER BY b.nama");
		}
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('gudangwip/vlistwarna2', $data); 
		return true;
  }
  
}
