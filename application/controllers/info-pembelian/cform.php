<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-pembelian/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['list_jenis_barang'] = $this->mmaster->get_jenis_barang(); 
	$data['list_kelompok_barang'] = $this->mmaster->get_kelompok_barang(); 
	$data['list_gudang'] = $this->mmaster->get_gudang(); 
	$data['isi'] = 'info-pembelian/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			
    $jenis_beli = $this->input->post('jenis_beli', TRUE);
    $kategori = $this->input->post('kategori', TRUE);
    $supplier = $this->input->post('supplier', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	$kel_brg = $this->input->post('kel_brg', TRUE);  
	$jns_brg = $this->input->post('jns_brg', TRUE);  
	
	if ($jenis_beli == '2') {
		//$jum_total = $this->mmaster->get_all_pembeliantanpalimit($jenis_beli, $date_from, $date_to, $supplier);
		$data['query'] = $this->mmaster->get_all_pembelian($jenis_beli, $kategori, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg);
		if (is_array($data['query']))
			$data['jum_total'] = count($data['query']);
		else
			$data['jum_total'] = 0;
		$data['isi'] = 'info-pembelian/vformview';
		
	}
	else {
		//$jum_total = $this->mmaster->get_all_pembeliancashtanpalimit($jenis_beli, $date_from, $date_to, $supplier);
		$data['query'] = $this->mmaster->get_all_pembeliancash($jenis_beli, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg);
		//$data['query2'] = $this->mmaster->get_all_pelunasantanpafaktur($date_from, $date_to);
		if (is_array($data['query']))
			$data['jum_total'] = count($data['query']);
		else
			$data['jum_total'] = 0;
		$data['isi'] = 'info-pembelian/vformviewcash';
	}
	
	// ambil data nama supplier
	if ($jns_brg != '0') {
		$query8	= $this->db->query(" SELECT kode, nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		if ($query8->num_rows() > 0){
			$hasilrow8 = $query8->row();
			$kode_jenis	= $hasilrow8->kode;
			$nama_jenis	= $hasilrow8->nama;
		}
		else {
			$nama_jenis = '';
			$kode_jenis = '';
		}
	}
	else {
		$kode_jenis = '';
		$nama_jenis = "Semua";
	}
	
	if ($kel_brg != '0') {
		$query6	= $this->db->query(" SELECT kode_perkiraan, nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		if ($query6->num_rows() > 0){
			$hasilrow6 = $query6->row();
			$kode_perkiraan	= $hasilrow6->kode_perkiraan;
			$nama_kelompok	= $hasilrow6->nama;
		}
		else {
			$nama_kelompok = '';
			$kode_perkiraan = '';
		}
	}
	else {
		$nama_kelompok = 'Semua';
			$kode_perkiraan = '';
	}
	
	if ($supplier != '0') {
		$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp FROM tm_supplier WHERE id = '$supplier' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
		}
		else {
			$nama_supplier = '';
			$kode_supplier = '';
		}
	}
	else {
		$kode_supplier = '';
		$nama_supplier = "Semua";
	}
	
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
		}
		else {
			$nama_gudang = '';
			$kode_gudang = '';
		}
	}
	else {
		$kode_gudang = '';
		$nama_gudang = "Semua";
	}
	
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$data['kategori'] = $kategori;
	$data['supplier'] = $supplier;
	$data['kode_supplier'] = $kode_supplier;
	$data['nama_supplier'] = $nama_supplier;
	$data['kode_jenis'] = $kode_jenis;
	$data['nama_jenis'] = $kel_brg;
	$data['jenis'] = $jns_brg;
	$data['kelompok'] = $kel_brg;
	$data['kode_perkiraan'] = $kode_perkiraan;
	$data['nama_kelompok'] = $nama_kelompok;
	$data['gudang'] = $gudang;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$this->load->view('template',$data);
  }
  
  // 12-04-2012
  function export_excel_khusus() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$kategori = $this->input->post('kategori', TRUE);
		$supplier = $this->input->post('id_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel_khusus', TRUE);  
		$export_ods1 = $this->input->post('export_ods_khusus', TRUE);  
		$gudang = $this->input->post('gudang', TRUE);  
		$kel_brg = $this->input->post('kelompok', TRUE);  
		$jns_brg = $this->input->post('jenis', TRUE);  
		
		$query = $this->mmaster->get_all_pembelian_khusus($jenis_beli, $kategori, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg);
		//print_r($query); die();
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
		
		if ($kategori == 1)
			$nama_kat = "Pembelian Bahan Baku/Pembantu";
		else if ($kategori == 2)
			$nama_kat = "Pembelian Makloon";
		else
			$nama_kat = "All";
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		// 12-04-2012, coba php excel (blm beres, ditunda dulu. sementara pake header, dibikin 2 aja utk excel dan ods)
		
		/* Excel */
	/*	$ObjPHPExcel = new PHPExcel();
		
		$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
		$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2012");
		$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2012");
		
		$ObjPHPExcel->getProperties()
			->setTitle("Laporan Pembelian")
			->setSubject("Laporan Pembelian")
			->setDescription("Laporan Pembelian per bulan")
			->setKeywords("Laporan")
			->setCategory("Laporan");

		$ObjPHPExcel->setActiveSheetIndex(0);

		$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		
		$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'Supplier');
		$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'No SJ');
		$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Tgl SJ');
		$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'List Barang');
		$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'No Perk');
		$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Harga Satuan');
		$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'Qty');
		$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'Satuan');
		$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'Jumlah');
		$ObjPHPExcel->getActiveSheet()->setCellValue('J1', 'Tot Hutang Dagang');
		$ObjPHPExcel->getActiveSheet()->setCellValue('K1', 'PPN');
		$ObjPHPExcel->getActiveSheet()->setCellValue('L1', 'Bahan Baku');
		$ObjPHPExcel->getActiveSheet()->setCellValue('M1', 'Bahan Pembantu');
		*/
		// ===================================================================================================
		if ($kategori == 0 || $kategori == 2)
			$jumcolspan=16;
		else
			$jumcolspan=16;
		
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>LAPORAN PEMBELIAN</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Jenis Pembelian: $nama_jenis</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Kategori Pembelian: $nama_kat</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='10%'>Kode Supplier</th>
			 <th width='30%'>Nama Supplier</th>
			 <th width='10%'>No SJ</th>
			 <th width='10%'>Tgl SJ</th>
			 <th width='20%'>Kode Barang</th>
			<th width='20%'>Nama Barang</th>

			 <th width='10%'>No Perk</th>
			 <th>Harga Satuan</th>
			 <th>Qty</th>
			 <th>Satuan</th>";
			
		if ($kategori == 0 || $kategori == 2) {
			$html_data.= "<th>Jenis Potong & Ukuran</th>";
		}
		
			 $html_data.="<th>Jumlah</th>
			 <th>Total<br>Hutang Dagang</th>
			 <th>DPP</th>";
		if ($kategori == 1) {
			$html_data.="<th>PPN</th>";
		}
		else
			$html_data.="<th>Total<br>Pajak/PPN</th>";
		
			
			 
		if ($kategori == 0 || $kategori == 2)
		{
			 $html_data.="<th>Makloon</th>";
		}else{
			$html_data.="<th>Bahan Baku</th>
				<th>Bahan Pembantu</th>";
			 }
		 $html_data.="</tr>
		</thead>
		<tbody>";
			if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_dpp = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				$tot_makloon = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_hutang += $query[$j]['jumlah'];
					$tot_dpp += $query[$j]['dpp'];
					$tot_ppn += $query[$j]['pajaknya'];
					
					if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
							 //if ($var_detail[$k]['kode_perk'] == "511.100")
							 if ($var_detail[$k]['kode_perk'] == "510-10100")
								$tot_baku += $var_detail[$k]['total'];
							 //else if ($var_detail[$k]['kode_perk'] == "512.100")
							 else if ($var_detail[$k]['kode_perk'] == "510-10200")
								$tot_pembantu += $var_detail[$k]['total'];
							 else
								$tot_makloon += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_dpp = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				$tot_makloon = 0;
			}
		 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.= "<tr class=\"record\">";
				  $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_supplier'];
						 // $html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				   $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_supplier'];
						 // $html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				    $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['no_sj'];
						 // $html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				     $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['tgl_sj'];
						 // $html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 //~ $html_data.=    "<td>".$query[$j]['kode_supplier']."</td>";
				 //~ $html_data.=    "<td>".$query[$j]['nama_supplier']."</td>";
				 //~ $html_data.=    "<td>".$query[$j]['no_sj']."</td>";
				 //~ $html_data.=    "<td style='white-space:nowrap;'>".$query[$j]['tgl_sj']."</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_brg'];
						 // $html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				  $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_brg'];
						 // $html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_perk'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data.= number_format($var_detail[$k]['harga'], 4,',','.');
						   $html_data.= $var_detail[$k]['harga'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['kode_perk'] != "523.100")
						  $html_data.= $var_detail[$k]['satuan'];
						else
						  $html_data.= "Yard";
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 if ($kategori == 0 || $kategori == 2) {
					 $html_data.= "<td style='white-space:nowrap;'>";
					 if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 $hitung = count($var_detail);
						for($k=0;$k<count($var_detail); $k++){
							$html_data.= $var_detail[$k]['nama_jenis_potong']." ".$var_detail[$k]['nama_ukuran_bisbisan'];
							if ($k<$hitung-1)
								$html_data.= "<br> ";
						}
					 }
					 $html_data.= "</td>";
				 }
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['total'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.=    "<td align='right'>".$query[$j]['jumlah']."</td>";
				 $html_data.=    "<td align='right'>".$query[$j]['dpp']."</td>";
				 $html_data.=    "<td align='right'>".$query[$j]['pajaknya']."</td>";
				 
				 
				 
				 if ($kategori == 0 || $kategori == 2) {
					 $html_data.= "<td align='right'>";
					 if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 $hitung = count($var_detail);
						for($k=0;$k<count($var_detail); $k++){
							 if ($var_detail[$k]['kode_perk'] == "523.100") 
								//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
								$html_data.= $var_detail[$k]['total'];
							 if ($k<$hitung-1)
								$html_data.= "<br> ";
						}
					 }
					 $html_data.= "</td>";
				}

				else{
					$html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 //if ($var_detail[$k]['kode_perk'] == "511.100") 
						 if ($var_detail[$k]['kode_perk'] == "510-10100")
							$html_data.= $var_detail[$k]['total'];
						 if ($k<$hitung-1)
						    $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 //if ($var_detail[$k]['kode_perk'] == "512.100") 
						 if ($var_detail[$k]['kode_perk'] == "510-10200")
							//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
							$html_data.= $var_detail[$k]['total'];
						 if ($k<$hitung-1)
						    $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
					}
				 
				 $html_data.=  "</tr>";

					
		 	}
		   }
		
		 $html_data.=  "<tr>";
			
				 if ($kategori == 0 || $kategori == 2) {
			$html_data.= "<td colspan='11' align='center'>TOTAL</td>";
		 }
		 else
			$html_data.= "<td colspan='10' align='center'>TOTAL</td>";
		 //~ $html_data.=  "
			//~ <td align='right'><b>". number_format($tot_jumlah_detail,4,',','.')  ."</b></td>
			//~ <td align='right'><b>". number_format($tot_hutang,4,',','.')  ."</b></td>
			//~ <td align='right'><b>". number_format($tot_dpp,4,',','.')  ."</b></td>
			//~ <td align='right'><b>".  number_format($tot_ppn,4,',','.')  ."</b></td>
			//~ <td align='right'><b>".  number_format($tot_baku,4,',','.')  ."</b></td>
			//~ <td align='right'><b>".  number_format($tot_pembantu,4,',','.')  ."</b></td>";
		
			 $html_data.=  "
			<td align='right'><b>". $tot_jumlah_detail  ."</b></td>
			<td align='right'><b>". $tot_hutang  ."</b></td>
			<td align='right'><b>". $tot_dpp ."</b></td>
			<td align='right'><b>". $tot_ppn  ."</b></td>";
			 if ($kategori == 0 || $kategori == 2) { 
				  $html_data.=  "
			<td align='right'><b>".  $tot_makloon  ."</b></td>";
			 } else{  $html_data.=  "
			<td align='right'><b>".  $tot_baku  ."</b></td>
			<td align='right'><b>".  $tot_pembantu ."</b></td>";
		}
			
			  $html_data.= "
		 </tr>
 	</tbody>
</table>
";
		//~ if (is_array($query)) {
			//~ $tot_jumlah_detail = 0;
			//~ $tot_hutang = 0;
			//~ $tot_dpp = 0;
			//~ $tot_ppn = 0;
			//~ $tot_baku = 0;
			//~ $tot_pembantu = 0;
			//~ $tot_makloon = 0;
			//~ $no_sj_temp = ""; $kode_sup_temp = "";
			//~ for($j=0;$j<count($query);$j++){
				//~ // hitung jumlah total masing2 field
				//~ if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					//~ $no_sj_temp = $query[$j]['no_sj'];
					//~ $kode_sup_temp = $query[$j]['kode_supplier'];
					//~ $tot_hutang += $query[$j]['jumlah'];
					//~ $tot_dpp += $query[$j]['dpp'];
					//~ $tot_ppn += $query[$j]['pajaknya'];
				//~ }
			//~ $var_detail=$query[$j]['detail_beli'];
				//~ for($k=0;$k<count($var_detail); $k++){
							 //~ $tot_jumlah_detail += $var_detail[$k]['total'];
							 //~ if ($var_detail[$k]['kode_perk'] == "511.100")
								//~ $tot_baku += $var_detail[$k]['total'];
							 //~ else if ($var_detail[$k]['kode_perk'] == "512.100")
								//~ $tot_pembantu += $var_detail[$k]['total'];
							 //~ else
								//~ $tot_makloon += $var_detail[$k]['total'];
						//~ }
			//~ } // end header
		//~ }
		//~ else {
			//~ $tot_jumlah_detail = 0;
			//~ $tot_hutang = 0;
			//~ $tot_dpp = 0;
			//~ $tot_ppn = 0;
			//~ $tot_baku = 0;
			//~ $tot_pembantu = 0;
			//~ $tot_makloon = 0;
		//~ }
		//~ 
		//~ if (is_array($query)) {
			//~ $no_sj_temp = ""; $kode_sup_temp = ""; $hitungitem = 1;
			 //~ for($j=0;$j<count($query);$j++){
				 //~ $html_data.= "<tr>";
				 //~ //<td width= '50px'>".$query[$j]['nama_supplier']."</td>";
				 //~ if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 //~ $hitungitem = 1;
					 //~ $html_data.= "<td>".$query[$j]['kode_supplier']."</td>";
					 //~ $html_data.= "<td>".$query[$j]['nama_supplier']."</td>";
					 //~ $html_data.="<td>"."'".$query[$j]['no_sj']."</td>
					  //~ <td>".$query[$j]['tgl_sj']."</td>";
				 //~ }
				 //~ else {
					 //~ $hitungitem++;
					 //~ $html_data.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					 //~ <td>&nbsp;</td>";
				 //~ }
				 //~ 
				 //~ $html_data.="<td>";
				//~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data.= $var_detail[$k]['kode_brg'];
					//~ //	  $html_data.= $var_detail[$k]['nama_brg'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>";
				 //~ $html_data.="<td>";
				//~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data.= $var_detail[$k]['nama_brg'];
					//~ //	  $html_data.= $var_detail[$k]['nama_brg'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>
				 //~ <td>";
				 //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						    //~ $html_data.= $var_detail[$k]['kode_perk'];
						    //~ 
						  //~ if ($k<$hitung-1)
						      //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>
				 //~ <td>";
				 //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data.= number_format($var_detail[$k]['harga'], 4,',','.');
						  //~ $html_data.= $var_detail[$k]['harga'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>
				 //~ 
				 //~ <td>";
				//~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						   //~ $html_data.= number_format($var_detail[$k]['qty'], 4,',','.');
						   //~ $html_data.= $var_detail[$k]['qty'];
						  //~ if ($k<$hitung-1)
						      //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>
				 //~ <td>";
				 //~ 
				  //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						//~ if ($var_detail[$k]['kode_perk'] != "523.100")
						  //~ $html_data.= $var_detail[$k]['satuan'];
						//~ else
						  //~ $html_data.= "Yard";
						  //~ if ($k<$hitung-1)
						     //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td> ";
				 //~ 
				 //~ 
				  //~ if ($kategori == 0 || $kategori == 2) {
					 //~ $html_data.= "<td>";
					 //~ if (is_array($query[$j]['detail_beli'])) {
						 //~ $var_detail = array();
						 //~ $var_detail = $query[$j]['detail_beli'];
						 //~ $hitung = count($var_detail);
						//~ for($k=0;$k<count($var_detail); $k++){
							 //~ $html_data.= $var_detail[$k]['nama_jenis_potong']." ".$var_detail[$k]['nama_ukuran_bisbisan'];
							//~ if ($k<$hitung-1)
								 //~ $html_data.= "<br> ";
						//~ }
					 //~ }
					  //~ $html_data.= "</td>";
				 //~ }
				 //~ 
				 //~ $html_data.= "<td>";
				  //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						    //~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
						    //~ $html_data.= $var_detail[$k]['total'];
						  //~ if ($k<$hitung-1)
						       //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>";
				 //~ 
				 //~ if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 //~ $no_sj_temp = $query[$j]['no_sj'];
					 //~ $kode_sup_temp = $query[$j]['kode_supplier'];
					//~ 
					//~ // 18-12-2015 oprek2
					//~ /*$html_data.="<td>".$query[$j]['jumlah']."</td>
					 //~ <td>".$query[$j]['dpp']."</td>
					 //~ <td>".$query[$j]['pajaknya']."</td>"; */
					 //~ 
					 //~ 
					 //~ if ($hitungitem == 1 && $query[$j]['jum_item'] == 1) {
						 //~ $html_data.="<td>".$query[$j]['jumlah']."</td>
						//~ <td>".$query[$j]['dpp']."</td>
						//~ <td>".$query[$j]['pajaknya']."</td>";
					 //~ }
					 //~ else {
						 //~ if (isset($query[$j+1]['no_sj']) && $hitungitem < $query[$j]['jum_item'])
							 //~ $html_data.="<td>&nbsp;</td>
							 //~ <td>&nbsp;</td>
							 //~ <td>&nbsp;</td>";
						 //~ else if (isset($query[$j+1]['no_sj']) && $hitungitem == $query[$j]['jum_item'])
							//~ $html_data.="<td>".$query[$j]['jumlah']."</td>
							//~ <td>".$query[$j]['dpp']."</td>
							//~ <td>".$query[$j]['pajaknya']."</td>";
					 //~ }
				 //~ }
				 //~ else {					 
					 //~ /*$html_data.="<td>&nbsp;</td>
					 //~ <td>&nbsp;</td>
					 //~ <td>&nbsp;</td>"; */
					 //~ if (isset($query[$j+1]['no_sj']) && $hitungitem < $query[$j]['jum_item'])
						 //~ $html_data.="<td>&nbsp;</td>
						 //~ <td>&nbsp;</td>
						 //~ <td>&nbsp;</td>";
					 //~ else if (isset($query[$j+1]['no_sj']) && $hitungitem == $query[$j]['jum_item'])
						//~ $html_data.="<td>".$query[$j]['jumlah']."</td>
						//~ <td>".$query[$j]['dpp']."</td>
						//~ <td>".$query[$j]['pajaknya']."</td>";
				 //~ }
				 				 //~ 
				  //~ $html_data.= "<td align='right'>";
				 //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						 //~ if ($var_detail[$k]['kode_perk'] == "511.100") 
							//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
							//~ $html_data.= $var_detail[$k]['total'];
						 //~ if ($k<$hitung-1)
						    //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>";
				 //~ 
				 //~ $html_data.= "<td align='right'>";
				 //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						 //~ if ($var_detail[$k]['kode_perk'] == "512.100") 
							//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
							//~ $html_data.= $var_detail[$k]['total'];
						 //~ if ($k<$hitung-1)
						    //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>";
				 //~ 
				 //~ if ($kategori == 0 || $kategori == 2) {
					 //~ $html_data.= "<td align='right'>";
					 //~ if (is_array($query[$j]['detail_beli'])) {
						 //~ $var_detail = array();
						 //~ $var_detail = $query[$j]['detail_beli'];
						 //~ $hitung = count($var_detail);
						//~ for($k=0;$k<count($var_detail); $k++){
							 //~ if ($var_detail[$k]['kode_perk'] == "523.100") 
								//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
								//~ $html_data.= $var_detail[$k]['total'];
							 //~ if ($k<$hitung-1)
								//~ $html_data.= "<br> ";
						//~ }
					 //~ }
					 //~ $html_data.= "</td>";
				//~ }
				 //~ 
				 //~ //echo $hitungitem."<br>";
				 //~ $html_data.=  "</tr>";
		 	//~ }
		 //~ }
		 //~ //echo $html_data; die();
		 //~ $html_data.= "<tr>";
		 //~ if ($kategori == 0 || $kategori == 2) {
			//~ $html_data.="<td colspan='11' align='center'>TOTAL</td>";
		 //~ }
		 //~ else
			//~ $html_data.="<td colspan='10' align='center'>TOTAL</td>";
		//~ 
			//~ $html_data.="<td align='right'>".$tot_jumlah_detail." </td>
			//~ <td align='right'>".$tot_hutang."</td>
			//~ <td align='right'>".$tot_dpp."</td>
			//~ <td align='right'>".$tot_ppn."</td>
			//~ <td align='right'>".$tot_baku."</td>
			//~ <td align='right'>".$tot_pembantu."</td>";
			//~ 
			//~ if ($kategori == 0 || $kategori == 2) {
				//~ $html_data.="<td align='right'>".$tot_makloon."</td>";
			//~ }
		 //~ $html_data.="</tr></tbody>
		 
		 
		//~ </table>";

		$nama_file = "laporan_pembelian_khusus";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$kategori = $this->input->post('kategori', TRUE);
		$supplier = $this->input->post('id_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		$gudang = $this->input->post('gudang', TRUE);  
		$kel_brg = $this->input->post('kelompok', TRUE);  
		$jns_brg = $this->input->post('jenis', TRUE);  
		
		$query = $this->mmaster->get_all_pembelian($jenis_beli, $kategori, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg);
		//print_r($query); die();
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
		
		if ($kategori == 1)
			$nama_kat = "Pembelian Bahan Baku/Pembantu";
		else if ($kategori == 2)
			$nama_kat = "Pembelian Makloon";
		else
			$nama_kat = "All";
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		// 12-04-2012, coba php excel (blm beres, ditunda dulu. sementara pake header, dibikin 2 aja utk excel dan ods)
		
		/* Excel */
	/*	$ObjPHPExcel = new PHPExcel();
		
		$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
		$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2012");
		$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2012");
		
		$ObjPHPExcel->getProperties()
			->setTitle("Laporan Pembelian")
			->setSubject("Laporan Pembelian")
			->setDescription("Laporan Pembelian per bulan")
			->setKeywords("Laporan")
			->setCategory("Laporan");

		$ObjPHPExcel->setActiveSheetIndex(0);

		$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		
		$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'Supplier');
		$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'No SJ');
		$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Tgl SJ');
		$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'List Barang');
		$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'No Perk');
		$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Harga Satuan');
		$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'Qty');
		$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'Satuan');
		$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'Jumlah');
		$ObjPHPExcel->getActiveSheet()->setCellValue('J1', 'Tot Hutang Dagang');
		$ObjPHPExcel->getActiveSheet()->setCellValue('K1', 'PPN');
		$ObjPHPExcel->getActiveSheet()->setCellValue('L1', 'Bahan Baku');
		$ObjPHPExcel->getActiveSheet()->setCellValue('M1', 'Bahan Pembantu');
		*/
		// ===================================================================================================
		if ($kategori == 0 || $kategori == 2)
			$jumcolspan=16;
		else
			$jumcolspan=15;
		
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>LAPORAN PEMBELIAN</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Jenis Pembelian: $nama_jenis</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Kategori Pembelian: $nama_kat</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='10%'>Kode Supplier</th>
			 <th width='30%'>Nama Supplier</th>
			 <th width='10%'>No SJ</th>
			 <th width='10%'>Tgl SJ</th>
			 <th width='10%'>No Faktur</th>
			 <th width='10%'>Tgl Faktur</th>
			 <th width='10%'>NO F Pajak</th>
			 <th width='20%'>Kode Barang</th>
			<th width='20%'>Nama Barang</th>

			 <th width='10%'>No Perk</th>
			 <th>Harga Satuan</th>
			 <th>Qty</th>
			 <th>Satuan</th>";
			
		if ($kategori == 0 || $kategori == 2) {
			$html_data.= "<th>Jenis Potong & Ukuran</th>";
		}
		
			 $html_data.="<th>Jumlah</th>
			 <th>Total<br>Hutang Dagang</th>
			 <th>DPP</th>";
		if ($kategori == 1) {
			$html_data.="<th>PPN</th>";
		}
		else
			$html_data.="<th>Total<br>Pajak/PPN</th>";
		
			 $html_data.="<th>Bahan Baku</th>
			 <th>Bahan Pembantu</th>";
			 
		if ($kategori == 0 || $kategori == 2)
		{
			 $html_data.="<th>Makloon</th>";
		}
		 $html_data.="</tr>
		</thead>
		<tbody>";
			if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_dpp = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				$tot_makloon = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_hutang += $query[$j]['jumlah'];
					$tot_dpp += $query[$j]['dpp'];
					$tot_ppn += $query[$j]['pajaknya'];
					
					if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
							 if ($var_detail[$k]['kode_perk'] == "510-10100") 
							 //if ($var_detail[$k]['kode_perk'] == "511.100")
								$tot_baku += $var_detail[$k]['total'];
							else if ($var_detail[$k]['kode_perk'] == "510-10200")
							 //else if ($var_detail[$k]['kode_perk'] == "512.100")
								$tot_pembantu += $var_detail[$k]['total'];
							 else
								$tot_makloon += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_dpp = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				$tot_makloon = 0;
			}
		 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.= "<tr class=\"record\">";
				 $html_data.=    "<td>".$query[$j]['kode_supplier']."</td>";
				 $html_data.=    "<td>".$query[$j]['nama_supplier']."</td>";
				 $html_data.=    "<td>".$query[$j]['no_sj']."</td>";
				 $html_data.=    "<td style='white-space:nowrap;'>".$query[$j]['tgl_sj']."</td>";
				 $html_data.=    "<td>".$query[$j]['no_faktur']."</td>";
				 $html_data.=    "<td style='white-space:nowrap;'>".$query[$j]['tgl_faktur']."</td>";
				 $html_data.=    "<td>".$query[$j]['no_faktur_pajak']."</td>";

				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_brg'];
						 // $html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				  $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_brg'];
						 // $html_data.= $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_perk'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data.= number_format($var_detail[$k]['harga'], 4,',','.');
						   $html_data.= $var_detail[$k]['harga'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['kode_perk'] != "523.100")
						  $html_data.= $var_detail[$k]['satuan'];
						else
						  $html_data.= "Yard";
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 if ($kategori == 0 || $kategori == 2) {
					 $html_data.= "<td style='white-space:nowrap;'>";
					 if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 $hitung = count($var_detail);
						for($k=0;$k<count($var_detail); $k++){
							$html_data.= $var_detail[$k]['nama_jenis_potong']." ".$var_detail[$k]['nama_ukuran_bisbisan'];
							if ($k<$hitung-1)
								$html_data.= "<br> ";
						}
					 }
					 $html_data.= "</td>";
				 }
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['total'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.=    "<td align='right'>".$query[$j]['jumlah']."</td>";
				 $html_data.=    "<td align='right'>".$query[$j]['dpp']."</td>";
				 $html_data.=    "<td align='right'>".$query[$j]['pajaknya']."</td>";
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 //if ($var_detail[$k]['kode_perk'] == "511.100") 
						 if ($var_detail[$k]['kode_perk'] == "510-10100")
							$html_data.= $var_detail[$k]['total'];
						 if ($k<$hitung-1)
						    $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 //if ($var_detail[$k]['kode_perk'] == "512.100") 
						 if ($var_detail[$k]['kode_perk'] == "510-10200")
							//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
							$html_data.= $var_detail[$k]['total'];
						 if ($k<$hitung-1)
						    $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 if ($kategori == 0 || $kategori == 2) {
					 $html_data.= "<td align='right'>";
					 if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 $hitung = count($var_detail);
						for($k=0;$k<count($var_detail); $k++){
							 if ($var_detail[$k]['kode_perk'] == "523.100") 
								//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
								$html_data.= $var_detail[$k]['total'];
							 if ($k<$hitung-1)
								$html_data.= "<br> ";
						}
					 }
					 $html_data.= "</td>";
				}
				 
				 $html_data.=  "</tr>";

					
		 	}
		   }
		
		 $html_data.=  "<tr>";
			
				 if ($kategori == 0 || $kategori == 2) {
			$html_data.= "<td colspan='13' align='center'>TOTAL</td>";
		 }
		 else
			$html_data.= "<td colspan='12' align='center'>TOTAL</td>";
		 //~ $html_data.=  "
			//~ <td align='right'><b>". number_format($tot_jumlah_detail,4,',','.')  ."</b></td>
			//~ <td align='right'><b>". number_format($tot_hutang,4,',','.')  ."</b></td>
			//~ <td align='right'><b>". number_format($tot_dpp,4,',','.')  ."</b></td>
			//~ <td align='right'><b>".  number_format($tot_ppn,4,',','.')  ."</b></td>
			//~ <td align='right'><b>".  number_format($tot_baku,4,',','.')  ."</b></td>
			//~ <td align='right'><b>".  number_format($tot_pembantu,4,',','.')  ."</b></td>";
		
			 $html_data.=  "
			<td align='right'><b>". $tot_jumlah_detail  ."</b></td>
			<td align='right'><b>". $tot_hutang  ."</b></td>
			<td align='right'><b>". $tot_dpp ."</b></td>
			<td align='right'><b>". $tot_ppn  ."</b></td>
			<td align='right'><b>".  $tot_baku  ."</b></td>
			<td align='right'><b>".  $tot_pembantu ."</b></td>";
			
			 if ($kategori == 0 || $kategori == 2) { 
				  $html_data.=  "
			<td align='right'><b>".  $tot_makloon  ."</b></td>";
			 } 
			  $html_data.= "
		 </tr>
 	</tbody>
</table>
";
		//~ if (is_array($query)) {
			//~ $tot_jumlah_detail = 0;
			//~ $tot_hutang = 0;
			//~ $tot_dpp = 0;
			//~ $tot_ppn = 0;
			//~ $tot_baku = 0;
			//~ $tot_pembantu = 0;
			//~ $tot_makloon = 0;
			//~ $no_sj_temp = ""; $kode_sup_temp = "";
			//~ for($j=0;$j<count($query);$j++){
				//~ // hitung jumlah total masing2 field
				//~ if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					//~ $no_sj_temp = $query[$j]['no_sj'];
					//~ $kode_sup_temp = $query[$j]['kode_supplier'];
					//~ $tot_hutang += $query[$j]['jumlah'];
					//~ $tot_dpp += $query[$j]['dpp'];
					//~ $tot_ppn += $query[$j]['pajaknya'];
				//~ }
			//~ $var_detail=$query[$j]['detail_beli'];
				//~ for($k=0;$k<count($var_detail); $k++){
							 //~ $tot_jumlah_detail += $var_detail[$k]['total'];
							 //~ if ($var_detail[$k]['kode_perk'] == "511.100")
								//~ $tot_baku += $var_detail[$k]['total'];
							 //~ else if ($var_detail[$k]['kode_perk'] == "512.100")
								//~ $tot_pembantu += $var_detail[$k]['total'];
							 //~ else
								//~ $tot_makloon += $var_detail[$k]['total'];
						//~ }
			//~ } // end header
		//~ }
		//~ else {
			//~ $tot_jumlah_detail = 0;
			//~ $tot_hutang = 0;
			//~ $tot_dpp = 0;
			//~ $tot_ppn = 0;
			//~ $tot_baku = 0;
			//~ $tot_pembantu = 0;
			//~ $tot_makloon = 0;
		//~ }
		//~ 
		//~ if (is_array($query)) {
			//~ $no_sj_temp = ""; $kode_sup_temp = ""; $hitungitem = 1;
			 //~ for($j=0;$j<count($query);$j++){
				 //~ $html_data.= "<tr>";
				 //~ //<td width= '50px'>".$query[$j]['nama_supplier']."</td>";
				 //~ if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 //~ $hitungitem = 1;
					 //~ $html_data.= "<td>".$query[$j]['kode_supplier']."</td>";
					 //~ $html_data.= "<td>".$query[$j]['nama_supplier']."</td>";
					 //~ $html_data.="<td>"."'".$query[$j]['no_sj']."</td>
					  //~ <td>".$query[$j]['tgl_sj']."</td>";
				 //~ }
				 //~ else {
					 //~ $hitungitem++;
					 //~ $html_data.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					 //~ <td>&nbsp;</td>";
				 //~ }
				 //~ 
				 //~ $html_data.="<td>";
				//~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data.= $var_detail[$k]['kode_brg'];
					//~ //	  $html_data.= $var_detail[$k]['nama_brg'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>";
				 //~ $html_data.="<td>";
				//~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data.= $var_detail[$k]['nama_brg'];
					//~ //	  $html_data.= $var_detail[$k]['nama_brg'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>
				 //~ <td>";
				 //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						    //~ $html_data.= $var_detail[$k]['kode_perk'];
						    //~ 
						  //~ if ($k<$hitung-1)
						      //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>
				 //~ <td>";
				 //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data.= number_format($var_detail[$k]['harga'], 4,',','.');
						  //~ $html_data.= $var_detail[$k]['harga'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>
				 //~ 
				 //~ <td>";
				//~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						   //~ $html_data.= number_format($var_detail[$k]['qty'], 4,',','.');
						   //~ $html_data.= $var_detail[$k]['qty'];
						  //~ if ($k<$hitung-1)
						      //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>
				 //~ <td>";
				 //~ 
				  //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						//~ if ($var_detail[$k]['kode_perk'] != "523.100")
						  //~ $html_data.= $var_detail[$k]['satuan'];
						//~ else
						  //~ $html_data.= "Yard";
						  //~ if ($k<$hitung-1)
						     //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td> ";
				 //~ 
				 //~ 
				  //~ if ($kategori == 0 || $kategori == 2) {
					 //~ $html_data.= "<td>";
					 //~ if (is_array($query[$j]['detail_beli'])) {
						 //~ $var_detail = array();
						 //~ $var_detail = $query[$j]['detail_beli'];
						 //~ $hitung = count($var_detail);
						//~ for($k=0;$k<count($var_detail); $k++){
							 //~ $html_data.= $var_detail[$k]['nama_jenis_potong']." ".$var_detail[$k]['nama_ukuran_bisbisan'];
							//~ if ($k<$hitung-1)
								 //~ $html_data.= "<br> ";
						//~ }
					 //~ }
					  //~ $html_data.= "</td>";
				 //~ }
				 //~ 
				 //~ $html_data.= "<td>";
				  //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						    //~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
						    //~ $html_data.= $var_detail[$k]['total'];
						  //~ if ($k<$hitung-1)
						       //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>";
				 //~ 
				 //~ if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 //~ $no_sj_temp = $query[$j]['no_sj'];
					 //~ $kode_sup_temp = $query[$j]['kode_supplier'];
					//~ 
					//~ // 18-12-2015 oprek2
					//~ /*$html_data.="<td>".$query[$j]['jumlah']."</td>
					 //~ <td>".$query[$j]['dpp']."</td>
					 //~ <td>".$query[$j]['pajaknya']."</td>"; */
					 //~ 
					 //~ 
					 //~ if ($hitungitem == 1 && $query[$j]['jum_item'] == 1) {
						 //~ $html_data.="<td>".$query[$j]['jumlah']."</td>
						//~ <td>".$query[$j]['dpp']."</td>
						//~ <td>".$query[$j]['pajaknya']."</td>";
					 //~ }
					 //~ else {
						 //~ if (isset($query[$j+1]['no_sj']) && $hitungitem < $query[$j]['jum_item'])
							 //~ $html_data.="<td>&nbsp;</td>
							 //~ <td>&nbsp;</td>
							 //~ <td>&nbsp;</td>";
						 //~ else if (isset($query[$j+1]['no_sj']) && $hitungitem == $query[$j]['jum_item'])
							//~ $html_data.="<td>".$query[$j]['jumlah']."</td>
							//~ <td>".$query[$j]['dpp']."</td>
							//~ <td>".$query[$j]['pajaknya']."</td>";
					 //~ }
				 //~ }
				 //~ else {					 
					 //~ /*$html_data.="<td>&nbsp;</td>
					 //~ <td>&nbsp;</td>
					 //~ <td>&nbsp;</td>"; */
					 //~ if (isset($query[$j+1]['no_sj']) && $hitungitem < $query[$j]['jum_item'])
						 //~ $html_data.="<td>&nbsp;</td>
						 //~ <td>&nbsp;</td>
						 //~ <td>&nbsp;</td>";
					 //~ else if (isset($query[$j+1]['no_sj']) && $hitungitem == $query[$j]['jum_item'])
						//~ $html_data.="<td>".$query[$j]['jumlah']."</td>
						//~ <td>".$query[$j]['dpp']."</td>
						//~ <td>".$query[$j]['pajaknya']."</td>";
				 //~ }
				 				 //~ 
				  //~ $html_data.= "<td align='right'>";
				 //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						 //~ if ($var_detail[$k]['kode_perk'] == "511.100") 
							//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
							//~ $html_data.= $var_detail[$k]['total'];
						 //~ if ($k<$hitung-1)
						    //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>";
				 //~ 
				 //~ $html_data.= "<td align='right'>";
				 //~ if (is_array($query[$j]['detail_beli'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_beli'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						 //~ if ($var_detail[$k]['kode_perk'] == "512.100") 
							//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
							//~ $html_data.= $var_detail[$k]['total'];
						 //~ if ($k<$hitung-1)
						    //~ $html_data.= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data.= "</td>";
				 //~ 
				 //~ if ($kategori == 0 || $kategori == 2) {
					 //~ $html_data.= "<td align='right'>";
					 //~ if (is_array($query[$j]['detail_beli'])) {
						 //~ $var_detail = array();
						 //~ $var_detail = $query[$j]['detail_beli'];
						 //~ $hitung = count($var_detail);
						//~ for($k=0;$k<count($var_detail); $k++){
							 //~ if ($var_detail[$k]['kode_perk'] == "523.100") 
								//~ $html_data.= number_format($var_detail[$k]['total'],4,',','.');
								//~ $html_data.= $var_detail[$k]['total'];
							 //~ if ($k<$hitung-1)
								//~ $html_data.= "<br> ";
						//~ }
					 //~ }
					 //~ $html_data.= "</td>";
				//~ }
				 //~ 
				 //~ //echo $hitungitem."<br>";
				 //~ $html_data.=  "</tr>";
		 	//~ }
		 //~ }
		 //~ //echo $html_data; die();
		 //~ $html_data.= "<tr>";
		 //~ if ($kategori == 0 || $kategori == 2) {
			//~ $html_data.="<td colspan='11' align='center'>TOTAL</td>";
		 //~ }
		 //~ else
			//~ $html_data.="<td colspan='10' align='center'>TOTAL</td>";
		//~ 
			//~ $html_data.="<td align='right'>".$tot_jumlah_detail." </td>
			//~ <td align='right'>".$tot_hutang."</td>
			//~ <td align='right'>".$tot_dpp."</td>
			//~ <td align='right'>".$tot_ppn."</td>
			//~ <td align='right'>".$tot_baku."</td>
			//~ <td align='right'>".$tot_pembantu."</td>";
			//~ 
			//~ if ($kategori == 0 || $kategori == 2) {
				//~ $html_data.="<td align='right'>".$tot_makloon."</td>";
			//~ }
		 //~ $html_data.="</tr></tbody>
		 
		 
		//~ </table>";

		$nama_file = "laporan_pembelian";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
   function export_excel_old() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$kategori = $this->input->post('kategori', TRUE);
		$supplier = $this->input->post('id_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		$gudang = $this->input->post('gudang', TRUE);  
		$kel_brg = $this->input->post('kelompok', TRUE);  
		$jns_brg = $this->input->post('jenis', TRUE);  
		
		$query = $this->mmaster->get_all_pembelian_for_print($jenis_beli, $kategori, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg);
		//print_r($query); die();
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
		
		if ($kategori == 1)
			$nama_kat = "Pembelian Bahan Baku/Pembantu";
		else if ($kategori == 2)
			$nama_kat = "Pembelian Makloon";
		else
			$nama_kat = "All";
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		// 12-04-2012, coba php excel (blm beres, ditunda dulu. sementara pake header, dibikin 2 aja utk excel dan ods)
		
		/* Excel */
	/*	$ObjPHPExcel = new PHPExcel();
		
		$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
		$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2012");
		$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2012");
		
		$ObjPHPExcel->getProperties()
			->setTitle("Laporan Pembelian")
			->setSubject("Laporan Pembelian")
			->setDescription("Laporan Pembelian per bulan")
			->setKeywords("Laporan")
			->setCategory("Laporan");

		$ObjPHPExcel->setActiveSheetIndex(0);

		$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		
		$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'Supplier');
		$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'No SJ');
		$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Tgl SJ');
		$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'List Barang');
		$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'No Perk');
		$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Harga Satuan');
		$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'Qty');
		$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'Satuan');
		$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'Jumlah');
		$ObjPHPExcel->getActiveSheet()->setCellValue('J1', 'Tot Hutang Dagang');
		$ObjPHPExcel->getActiveSheet()->setCellValue('K1', 'PPN');
		$ObjPHPExcel->getActiveSheet()->setCellValue('L1', 'Bahan Baku');
		$ObjPHPExcel->getActiveSheet()->setCellValue('M1', 'Bahan Pembantu');
		*/
		// ===================================================================================================
		if ($kategori == 0 || $kategori == 2)
			$jumcolspan=15;
		else
			$jumcolspan=14;
		
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>LAPORAN PEMBELIAN</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Jenis Pembelian: $nama_jenis</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Kategori Pembelian: $nama_kat</th>
		 </tr>
		 <tr>
			<th colspan='".$jumcolspan."' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='10%'>Kode Supplier</th>
			 <th width='30%'>Nama Supplier</th>
			 <th width='10%'>No SJ</th>
			 <th width='10%'>Tgl SJ</th>
			 <th width='40%'>List Barang</th>
			 <th width='10%'>No Perk</th>
			 <th>Harga Satuan</th>
			 <th>Qty</th>
			 <th>Satuan</th>";
			
		if ($kategori == 0 || $kategori == 2) {
			$html_data.= "<th>Jenis Potong & Ukuran</th>";
		}
		
			 $html_data.="<th>Jumlah</th>
			 <th>Total<br>Hutang Dagang</th>
			 <th>DPP</th>";
		if ($kategori == 1) {
			$html_data.="<th>PPN</th>";
		}
		else
			$html_data.="<th>Total<br>Pajak/PPN</th>";
		
			 $html_data.="<th>Bahan Baku</th>
			 <th>Bahan Pembantu</th>";
			 
		if ($kategori == 0 || $kategori == 2)
			 $html_data.="<th>Makloon</th>";
		
		 $html_data.="</tr>
		</thead>
		<tbody>";
		if (is_array($query)) {
			$tot_jumlah_detail = 0;
			$tot_hutang = 0;
			$tot_dpp = 0;
			$tot_ppn = 0;
			$tot_baku = 0;
			$tot_pembantu = 0;
			$tot_makloon = 0;
			$no_sj_temp = ""; $kode_sup_temp = "";
			for($j=0;$j<count($query);$j++){
				// hitung jumlah total masing2 field
				if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					$no_sj_temp = $query[$j]['no_sj'];
					$kode_sup_temp = $query[$j]['kode_supplier'];
					$tot_hutang += $query[$j]['jumlah'];
					$tot_dpp += $query[$j]['dpp'];
					$tot_ppn += $query[$j]['pajaknya'];
				}

				 $tot_jumlah_detail += $query[$j]['total'];
				 if ($query[$j]['kode_perk'] == "511.100")
					$tot_baku += $query[$j]['total'];
				 else if ($query[$j]['kode_perk'] == "512.100")
					$tot_pembantu += $query[$j]['total'];
				 else
					$tot_makloon += $query[$j]['total'];
			} // end header
		}
		else {
			$tot_jumlah_detail = 0;
			$tot_hutang = 0;
			$tot_dpp = 0;
			$tot_ppn = 0;
			$tot_baku = 0;
			$tot_pembantu = 0;
			$tot_makloon = 0;
		}
		
		if (is_array($query)) {
			$no_sj_temp = ""; $kode_sup_temp = ""; $hitungitem = 1;
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr>";
				 //<td width= '50px'>".$query[$j]['nama_supplier']."</td>";
				 if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 $hitungitem = 1;
					 $html_data.= "<td>".$query[$j]['kode_supplier']."</td>";
					 $html_data.= "<td>".$query[$j]['nama_supplier']."</td>";
					 $html_data.="<td>"."'".$query[$j]['no_sj']."</td>
					  <td>".$query[$j]['tgl_sj']."</td>";
				 }
				 else {
					 $hitungitem++;
					 $html_data.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					 <td>&nbsp;</td>";
				 }
				 
				 $html_data.="<td>";
				 $html_data.= $query[$j]['nama_brg'];
				 $html_data.= "</td>
				 <td>";
				 $html_data.= "'".$query[$j]['kode_perk'];
				 $html_data.= "</td>
				 <td>";
				 $html_data.= $query[$j]['harga'];
				 $html_data.= "</td>
				 
				 <td>";
				 $html_data.= $query[$j]['qty'];
				 $html_data.= "</td>
				 <td>";
				 
				 if ($query[$j]['kode_perk'] != '523.100')
					$html_data.= $query[$j]['satuan'];
				 else
					$html_data.= "Yard";
				 $html_data.= "</td>";
				 
				 
				 if ($kategori == 0 || $kategori == 2) {
					 $html_data.= "<td>";
					 $html_data.= $query[$j]['nama_jenis_potong']." ".$query[$j]['nama_ukuran_bisbisan'];
					 $html_data.= "</td>";
				 }
				 
				 $html_data.= "<td>";
				 $html_data.= $query[$j]['total'];				 
				 $html_data.= "</td>";
				 
				 if ($no_sj_temp != $query[$j]['no_sj'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 $no_sj_temp = $query[$j]['no_sj'];
					 $kode_sup_temp = $query[$j]['kode_supplier'];
					
					// 18-12-2015 oprek2
					/*$html_data.="<td>".$query[$j]['jumlah']."</td>
					 <td>".$query[$j]['dpp']."</td>
					 <td>".$query[$j]['pajaknya']."</td>"; */
					 
					 
					 if ($hitungitem == 1 && $query[$j]['jum_item'] == 1) {
						 $html_data.="<td>".$query[$j]['jumlah']."</td>
						<td>".$query[$j]['dpp']."</td>
						<td>".$query[$j]['pajaknya']."</td>";
					 }
					 else {
						 if (isset($query[$j+1]['no_sj']) && $hitungitem < $query[$j]['jum_item'])
							 $html_data.="<td>&nbsp;</td>
							 <td>&nbsp;</td>
							 <td>&nbsp;</td>";
						 else if (isset($query[$j+1]['no_sj']) && $hitungitem == $query[$j]['jum_item'])
							$html_data.="<td>".$query[$j]['jumlah']."</td>
							<td>".$query[$j]['dpp']."</td>
							<td>".$query[$j]['pajaknya']."</td>";
					 }
				 }
				 else {					 
					 /*$html_data.="<td>&nbsp;</td>
					 <td>&nbsp;</td>
					 <td>&nbsp;</td>"; */
					 if (isset($query[$j+1]['no_sj']) && $hitungitem < $query[$j]['jum_item'])
						 $html_data.="<td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>";
					 else if (isset($query[$j+1]['no_sj']) && $hitungitem == $query[$j]['jum_item'])
						$html_data.="<td>".$query[$j]['jumlah']."</td>
						<td>".$query[$j]['dpp']."</td>
						<td>".$query[$j]['pajaknya']."</td>";
				 }
				 				 
				 $html_data.= "<td align='right'>";
				 if ($query[$j]['kode_perk'] == "511.100") 
					$html_data.= $query[$j]['total'];
				else
					$html_data.= "&nbsp;";
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right'>";
				 if ($query[$j]['kode_perk'] == "512.100") 
					$html_data.= $query[$j]['total'];
				else
					$html_data.= "&nbsp;";
				$html_data.= "</td>";
				
				if ($kategori == 0 || $kategori == 2) {
					$html_data.= "<td align='right'>";
					if ($query[$j]['kode_perk'] == "523.100") 
						$html_data.= $query[$j]['total'];
					else
						$html_data.= "&nbsp;";
					
					$html_data.= "</td>";				 
				}
				 
				 //echo $hitungitem."<br>";
				 $html_data.=  "</tr>";
		 	}
		 }
		 //echo $html_data; die();
		 $html_data.= "<tr>";
		 if ($kategori == 0 || $kategori == 2) {
			$html_data.="<td colspan='10' align='right'>TOTAL</td>";
		 }
		 else
			$html_data.="<td colspan='9' align='right'>TOTAL</td>";
		
			$html_data.="<td align='right'>".$tot_jumlah_detail." </td>
			<td align='right'>".$tot_hutang."</td>
			<td align='right'>".$tot_dpp."</td>
			<td align='right'>".$tot_ppn."</td>
			<td align='right'>".$tot_baku."</td>
			<td align='right'>".$tot_pembantu."</td>";
			
			if ($kategori == 0 || $kategori == 2) {
				$html_data.="<td align='right'>".$tot_makloon."</td>";
			}
		 $html_data.="</tr></tbody>
		</table>";

		$nama_file = "laporan_pembelian";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  // =========== 30-05-2015 =================================
  function export_excel_cash_old() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$supplier = $this->input->post('kode_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		$gudang = $this->input->post('gudang', TRUE);  
				$kel_brg = $this->input->post('kelompok', TRUE);  
		$jns_brg = $this->input->post('jenis', TRUE);  
		
		$query = $this->mmaster->get_all_pembeliancash_for_print($jenis_beli, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg);
		//print_r($query); die();
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
		
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='12' align='center'>LAPORAN PEMBELIAN</th>
		 </tr>
		 <tr>
			<th colspan='12' align='center'>Jenis Pembelian: $nama_jenis</th>
		 </tr>
		 <tr>
			<th colspan='12' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%'>No</th>
			 <th width='30%'>Supplier</th>
			 <th width='20%'>No Faktur</th>
			 <th>Tgl Faktur</th>
			 <th width='20%'>No/Tgl SJ</th>
			 <th>Nama Barang</th>
			 <th>Satuan</th>
			 <th>Harga</th>
			 <th>Qty</th>
			 <th>Subtotal</th>
			 <th>Total</th>
			 <th>No/Tgl Voucher Pelunasan</th>
		 </tr>
		</thead>
		<tbody>";
		if (is_array($query)) {
			$tot_jumlah_detail = 0;
			$tot_jumlah = 0; 
			$no_faktur_temp = ""; $kode_sup_temp="";
			for($j=0;$j<count($query);$j++){
				// hitung jumlah total masing2 field
				//echo $query[$j]['no_faktur']." ".$query[$j]['kode_supplier']."<br>";
				if ($no_faktur_temp != $query[$j]['no_faktur'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					$no_faktur_temp = $query[$j]['no_faktur'];
					$kode_sup_temp = $query[$j]['kode_supplier'];
					$tot_jumlah += $query[$j]['jumlah'];
					//echo $query[$j]['jumlah']."<br><br>";
				}

				 $tot_jumlah_detail += $query[$j]['total'];
			} // end header
		}
		else {
			$tot_jumlah_detail = 0;
			$tot_jumlah = 0;
		}
		//die();
		
		if (is_array($query)) {
			$no_faktur_temp = ""; $kode_sup_temp=""; $no=1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.= "<tr class=\"record\">";
				 //<td width= '50px'>".$query[$j]['nama_supplier']."</td>";
				 if ($no_faktur_temp != $query[$j]['no_faktur'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 $html_data.= "<td>".$no."</td>";
					 $html_data.= "<td>".$query[$j]['nama_supplier']."</td>";
					 $html_data.="<td>"."'".$query[$j]['no_faktur']."</td>
					  <td>".$query[$j]['tgl_faktur']."</td>";
					  $no++;
				 }
				 else {
					 $html_data.="<td>&nbsp;</td><td>&nbsp;</td>
					 <td>&nbsp;</td><td>&nbsp;</td>"; 
				 }
				 $html_data.="<td>".$query[$j]['no_sj']." / ".$query[$j]['tgl_sj']."</td>";
				 
				 $html_data.="<td>";
				 $html_data.= $query[$j]['nama_brg'];
				 $html_data.= "</td>
				 <td>";
				 $html_data.= $query[$j]['nama_satuan'];
				 $html_data.= "</td>
				 <td>";
				 $html_data.= $query[$j]['harga'];				 
				 $html_data.= "</td>
				 <td>";
				 $html_data.= $query[$j]['qty'];
				 $html_data.= "</td>";
				 $html_data.="<td>";
				 $html_data.= $query[$j]['total'];
				 $html_data.= "</td>";
				 
				 if ($no_faktur_temp != $query[$j]['no_faktur'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 $no_faktur_temp = $query[$j]['no_faktur'];
					 $kode_sup_temp = $query[$j]['kode_supplier'];
					 $html_data.= "<td>";
					 $html_data.= $query[$j]['jumlah'];				 
					 $html_data.= "</td>";
					 $html_data.= "<td>";
					 $html_data.= $query[$j]['no_voucher']." / ".$query[$j]['tgl_voucher'];
					 $html_data.= "</td>";
				}
				else
					$html_data.= "<td>&nbsp;</td><td>&nbsp;</td>";
				 				 				 				 
				 $html_data.=  "</tr>";
		 	}
		 }
		 $html_data.= "<tr>
			<td colspan='9' align='right'>TOTAL</td>
			<td>".$tot_jumlah_detail." </td>
			<td>".$tot_jumlah." </td>
		 </tr></tbody>
		</table>";

		$nama_file = "laporan_pembelian";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  //=========================================================
  
  function export_excel_cash() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$supplier = $this->input->post('kode_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		$gudang = $this->input->post('gudang', TRUE);  
				$kel_brg = $this->input->post('kelompok', TRUE);  
		$jns_brg = $this->input->post('jenis', TRUE);  
		
		$query = $this->mmaster->get_all_pembeliancash($jenis_beli, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg);
		//print_r($query); die();
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
		
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='13' align='center'>LAPORAN PEMBELIAN</th>
		 </tr>
		 <tr>
			<th colspan='13' align='center'>Jenis Pembelian: $nama_jenis</th>
		 </tr>
		 <tr>
			<th colspan='13' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%'>No</th>
			 <th width='30%'>Supplier</th>
			 <th width='20%'>No Faktur</th>
			 <th>Tgl Faktur</th>
			 <th width='10%'>No F Pajak</th>
			 <th width='20%'>No/Tgl SJ</th>
			 <th>Kode Barang</th>
			 <th>Nama Barang</th>
			 <th>Satuan</th>
			 <th>Harga</th>
			 <th>Qty</th>
			 <th>Subtotal</th>
			 <th>Total</th>
			 <th>No/Tgl Voucher Pelunasan</th>
		 </tr>
		</thead>
		<tbody>";
		if (is_array($query)) {
			$tot_jumlah_detail = 0;
			$tot_jumlah = 0; 
			$no_faktur_temp = ""; $kode_sup_temp="";
			for($j=0;$j<count($query);$j++){
				// hitung jumlah total masing2 field
				//echo $query[$j]['no_faktur']." ".$query[$j]['kode_supplier']."<br>";
				if ($no_faktur_temp != $query[$j]['no_faktur'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					$no_faktur_temp = $query[$j]['no_faktur'];
					$kode_sup_temp = $query[$j]['kode_supplier'];
					$tot_jumlah += $query[$j]['jumlah'];
					//echo $query[$j]['jumlah']."<br><br>";
				}
		if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
				 for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
						}
					}
			} // end header
		
		}
		else {
			$tot_jumlah_detail = 0;
			$tot_jumlah = 0;
		}
		//die();
		
		if (is_array($query)) {
			$no_faktur_temp = ""; $kode_sup_temp=""; $no=1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.= "<tr class=\"record\">";
				 //<td width= '50px'>".$query[$j]['nama_supplier']."</td>";
				 if ($no_faktur_temp != $query[$j]['no_faktur'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 $html_data.= "<td>".$no."</td>";
					 $html_data.= "<td>".$query[$j]['nama_supplier']."</td>";
					 $html_data.="<td>"."'".$query[$j]['no_faktur']."</td>
					  <td>".$query[$j]['tgl_faktur']."</td>";
					$html_data.= "<td>".$query[$j]['no_faktur_pajak']."</td>";
					  $no++;
				 }
				 else {
					 $html_data.="<td>&nbsp;</td><td>&nbsp;</td>
					 <td>&nbsp;</td><td>&nbsp;</td>"; 
				 }
				  $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						   $html_data.= $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_satuan'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 				 
				 $html_data.= "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= number_format($var_detail[$k]['harga'], 2,',','.');
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= number_format($var_detail[$k]['total'],2,',','.');
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 if ($no_faktur_temp != $query[$j]['no_faktur'] || $kode_sup_temp != $query[$j]['kode_supplier']) {
					 $no_faktur_temp = $query[$j]['no_faktur'];
					 $kode_sup_temp = $query[$j]['kode_supplier'];
					 $html_data.= "<td>";
					 $html_data.= $query[$j]['jumlah'];				 
					 $html_data.= "</td>";
					 $html_data.= "<td>";
					 $html_data.= $query[$j]['no_voucher']." / ".$query[$j]['tgl_voucher'];
					 $html_data.= "</td>";
				}
				else
					$html_data.= "<td>&nbsp;</td><td>&nbsp;</td>";
				 				 				 				 
				 $html_data.=  "</tr>";
		 	}
		 }
		 $html_data.= "<tr>
			<td colspan='9' align='right'>TOTAL</td>
			<td>".$tot_jumlah_detail." </td>
			<td>".$tot_jumlah." </td>
		 </tr></tbody>
		</table>";

		$nama_file = "laporan_pembelian";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  //=========================================================
  
  
  //03-04-2012, ini utk cek sj pembelian yg tidak sinkron antara total di detail dgn di header
  function cek_sj_nonsinkron(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$data['query'] = $this->mmaster->get_sj_nonsinkron();
		$data['jum_total'] = count($data['query']);
		$data['isi'] = 'info-pembelian/vformviewsjnonsinkron';
		$this->load->view('template',$data);
  }
  
  // 29-05-2015
  function lapfakturwip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_unit_jahit'] = $this->mmaster->getlistunitjahit(); 
	$data['isi'] = 'info-pembelian/vmainformlapfakturwip';
	$this->load->view('template',$data);
  }
  
  function lapfakturwipview(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
    $data['isi'] = 'info-pembelian/vformviewlapfakturwip';
	$kode_unit = $this->input->post('kode_unit', TRUE);  
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE); 
	$jenis_masuk = $this->input->post('jenis_masuk', TRUE);

	$querynya = $this->mmaster->get_all_fakturwip($date_from, $date_to, $kode_unit, $jenis_masuk);
	$data['query'] = $querynya;
	if (is_array($querynya))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
		
	// ambil data nama unit jahit
	$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$kode_unit' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
	}
	else
		$nama_unit = '';
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
	$data['jenis_masuk'] = $jenis_masuk;
	$this->load->view('template',$data);
  }
  
  function export_excel_lapfakturwip() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$kode_unit = $this->input->post('kode_unit', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);
		$jenis_masuk = $this->input->post('jenis_masuk', TRUE);  		
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_fakturwip_for_print($date_from, $date_to, $kode_unit,$jenis_masuk);
		
	$html_data = "
	<table border='1 cellpadding= '1' cellspacing = '1' width='100%'>
	<thead>
	<tr>
			<th colspan='12' align='center'>Laporan Faktur Pembelian Makloon Hasil Jahit (WIP)</th>
		 </tr>
		 
		 <tr>
			<th colspan='12' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
	 <tr class='judulnya'>
		 <th rowspan='2'>No</th>
		 <th rowspan='2'>Unit Jahit</th>
		 <th rowspan='2'>No Faktur</th>
		 <th rowspan='2'>Tgl Faktur</th>
		 <th rowspan='2'>Tgl SJ</th>
		 <th colspan='6'>List Brg Jadi</th>		 
		 <th rowspan='2'>Grand Total (Setelah dikurangi pajak)</th>
	 </tr>
	 <tr class='judulnya'>
		<th>Kode & Nama Brg Jadi</th>
		<th>Satuan</th>
		<th>Quantity</th>
		<th>Harga (Rp.)</th>
		<th>Diskon</th>
		<th>Subtotal</th>
		
	 </tr>
	</thead>
	<tbody>";
			if (is_array($query)) {
				$tot_grandtotal = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					//$tot_grandtotal += $query[$j]['grandtotal'];
				} // end header
			}
			else {
				$tot_grandtotal = 0;
			}
			if (is_array($query)) {
				$no = 1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.="<tr class=\"record\">";
				 $html_data.= "<td align='center'>".$no."</td>";
				 $html_data.="<td>".$query[$j]['nama_unit']."</td>";
				 // echo    "<td>".$query[$j]['no_faktur']."</td>";
				$html_data.="<td></td>";
				// echo    "<td>".$query[$j]['tgl_faktur']."</td>";
				$html_data.="<td></td>";
				$html_data.="<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
				 $html_data.="<br> ";
					}
				 }
				 $html_data.= "</td>";
				 $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='center'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				$html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
				$html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						//  echo number_format($var_detail[$k]['harga'], 2,',','.');
				$html_data.=  "" ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 // echo number_format($var_detail[$k]['diskon'], 2,',','.');
				 $html_data.= ""   ;
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";
				 
				  $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						//  echo number_format($var_detail[$k]['subtotal'],2,',','.');
				 $html_data.= ""   ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";

				

			//	 echo    "<td align='right'>".number_format($query[$j]['grandtotal'],2,',','.')."</td>";
				  $html_data.=    "<td align='right'>";
				  $html_data.= "</td>";
				/* echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "511.100") 
							echo number_format($var_detail[$k]['total'],2,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "512.100") 
							echo number_format($var_detail[$k]['total'],2,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>"; */
				 
				  $html_data.=  "</tr>";

				$no++;
		 	} // end for
		   }

		 $html_data.=" <tr>
			<td colspan='11' align='right'><b>TOTAL</b></td>
			<td align='right'><b><?php echo number_format($tot_grandtotal,2,',','.') ?></b></td>
		 </tr>
 	</tbody>
</table><br>";
		// ====================================================================

		$nama_file = "laporan_faktur_pembelian_wip";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  // 13-06-2015
  function lappelunasan(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['isi'] = 'info-pembelian/vmainformlappelunasan';
	$this->load->view('template',$data);
  }
  
  function viewlappelunasan(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			
    $jenis_beli = $this->input->post('jenis_beli', TRUE);
    $supplier = $this->input->post('supplier', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  

	$data['query'] = $this->mmaster->get_all_pelunasan($jenis_beli, $date_from, $date_to, $supplier);
	//print_r($data['query']); die();
	$data['jum_total'] = count($data['query']);
	$data['isi'] = 'info-pembelian/vformviewlappelunasan';
		
	// ambil data nama supplier
	if ($supplier != '0') {
		$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$supplier' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$nama_supplier	= $hasilrow->nama;
		}
		else
			$nama_supplier = '';
	}
	else
		$nama_supplier = "Semua";
		
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$data['supplier'] = $supplier;
	$data['nama_supplier'] = $nama_supplier;
	$this->load->view('template',$data);
  }
  
  // 15-06-2015
  function export_excel_lappelunasan() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$supplier = $this->input->post('kode_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_pelunasan($jenis_beli, $date_from, $date_to, $supplier);
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='11' align='center'>LAPORAN PEMBAYARAN PEMBELIAN</th>
		 </tr>
		 <tr>
			<th colspan='11' align='center'>Jenis Pembelian: $nama_jenis</th>
		 </tr>
		 <tr>
			<th colspan='11' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%'>No</th>
			 <th width='30%'>Supplier</th>
			 <th width='20%'>No Voucher</th>
			 <th>Tgl Voucher</th>
			 <th width='20%'>No/Tgl Faktur</th>
			 <th>Nama Barang</th>
			 <th>Satuan</th>
			 <th>Harga</th>
			 <th>Qty</th>
			 <th>Subtotal</th>
			 <th>Total</th>
		 </tr>
		</thead>
		<tbody>";
		
		if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_jumlah = 0; 
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_jumlah += $query[$j]['jumlah_bayar'];
					
					if (is_array($query[$j]['detail_item'])) {
						 $var_detail = $query[$j]['detail_item'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_jumlah = 0;
			}
		 
			if (is_array($query)) {
				$no=1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.= "<tr class=\"record\">";
				 $html_data.=    "<td align='center'>".$no."</td>";
				 $html_data.=    "<td>".$query[$j]['nama_supplier']."</td>";
				 $html_data.=    "<td>".$query[$j]['no_voucher']."</td>";
				 $html_data.=    "<td>".$query[$j]['tgl_voucher']."</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_item'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_item'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['no_faktur']." / ".$var_detail[$k]['tgl_faktur'];
						  if ($k<$hitung-1)
						     $html_data.= "&nbsp;<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_item'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_item'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     $html_data.= "&nbsp;<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_item'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_item'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_satuan'];
						  if ($k<$hitung-1)
						     $html_data.= "&nbsp;<br> ";
					}
				 }
				 $html_data.= "</td>";
				 				 
				 $html_data.= "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_item'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_item'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['harga'];
						  if ($k<$hitung-1)
						     $html_data.= "&nbsp;<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_item'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_item'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data.= "&nbsp;<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 
				 $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_item'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_item'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['total'];
						  if ($k<$hitung-1)
						     $html_data.= "&nbsp;<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				 $html_data.=    "<td align='right'>".$query[$j]['jumlah_bayar']."</td>";
				 $html_data.=  "</tr>";

				$no++;
		 	}
		   }
		
		$html_data.="<tr>
			<td colspan='9' align='right'><b>TOTAL</b></td>
			<td align='right'><b>".$tot_jumlah_detail."</b></td>
			<td align='right'><b>".$tot_jumlah."</b></td>
		 </tr></tbody>
		</table>";

		$nama_file = "laporan_pembayaran_pembelian";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  //=========================================================

}
