<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('forecast-gdjd/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$data['isi'] = 'forecast-gdjd/vmainform';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_branch'] = $this->mmaster->get_branch();
	$this->load->view('template',$data);

  }
  
  function view(){
    // =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$gudang = $this->input->post('gudang', TRUE);  
	$branch = $this->input->post('branch', TRUE);  

	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
			
	if ($bulan > 1) {
		
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
		if($bulan <10){
			$bulan_sebelumnya = "0".$bulan_sebelumnya;
			}
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE 
								a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	
	  //~ $db2=$this->load->database('db_external',TRUE);
//~ 
	//~ $query4	= $db2->query(" SELECT * from tr_branch  WHERE 
								//~ i_branch = '$branch' order by e_branch_name ");
	//~ $hasilrow4 = $query4->row();
	//~ $nama_kota	= $hasilrow4->e_branch_city;
	//~ $nama_branch	= $hasilrow4->e_branch_name;
	//~ 
	//~ 
			//~ $data['nama_kota'] = $nama_kota;
			//~ $data['nama_branch'] = $nama_branch;
			$data['nama_bulan'] = $nama_bln;
			$data['branch'] = $branch;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['gudang'] = $gudang;
			$data['kode_gudang'] = $kode_gudang;
			$data['nama_gudang'] = $nama_gudang;
			$data['nama_lokasi'] = $nama_lokasi;

			$cek_data = $this->mmaster->cek_forecast($bulan, $tahun, $gudang); 
			
			if ($cek_data['idnya'] == '' ) {
				
			$cek_data_bulan_sebelumnya = $this->mmaster->cek_forecast_bulan_sebelumnya($bulan_sebelumnya, $tahun_sebelumnya, $gudang); 
				
					if ($cek_data_bulan_sebelumnya['idnya'] == '' ) {
						
					$query3	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_forecast	= $hasilrow->id;
					}
					else
						$id_forecast = 0;
					// -------------------------------------
					
					$data['is_new'] = '1';
					$data['tgl_forecast'] = '';
					$data['id_forecast'] = $id_forecast;
					$data['isi'] = 'forecast-gdjd/vformviewfirst';
					$this->load->view('template',$data);
						
					}//cek bln seblmnya
					else{
					// ---------- 20-10-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_forecast	= $hasilrow->id;
					}
					else
						$id_forecast = 0;
					// -------------------------------------	
						
					$data['query'] = $this->mmaster->get_all_forecast($bulan_sebelumnya, $tahun_sebelumnya, $gudang);
					$query_get=$data['query'];
		for($i=0;$i<count($query_get);$i++){
			$id_tt_forecast_gudang_jd=$query_get[$i]['id_tt_forecast_gudang_jd'];
			}
			$data['id_tt_forecast_gudang_jd'] = $id_tt_forecast_gudang_jd;
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					
					$tgl_forecast = '';
					
					$data['tgl_forecast'] = $tgl_forecast;
					$data['id_forecast'] = $id_forecast;
					//$data['id_tt_forecast_gudang_jd'] =()
					$data['isi'] = 'forecast-gdjd/vformview';
					$this->load->view('template',$data);
				}
			}
			else {
				
					// ---------- 20-10-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_forecast	= $hasilrow->id;
					}
					else
						$id_forecast = 0;
					// -------------------------------------
							
					$data['query'] = $this->mmaster->get_all_forecast($bulan, $tahun, $gudang);
			
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					
					$tgl_forecast = $cek_data['tgl_forecast'];
					$pisah1 = explode("-", $tgl_forecast);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_forecast = $tgl1."-".$bln1."-".$thn1;
					
					$data['tgl_forecast'] = $tgl_forecast;
					$data['id_forecast'] = $id_forecast;
				
					$data['isi'] = 'forecast-gdjd/vformviewfirstedit';
					$this->load->view('template',$data);
				
			} 
		
	
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++

  }
  
  function submit() {
	  // =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $gudang = $this->input->post('gudang', TRUE);  
	$id_tt_forecast_gudang_jd= $this->input->post('id_tt_forecast_gudang_jd', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  $branch = $this->input->post('branch', TRUE);  
	  $is_pertamakali = $this->input->post('is_pertamakali', TRUE);
	 
	  
	  // 30-01-2016
	  if ($is_pertamakali == '1')
		$jum_data = $this->input->post('no', TRUE)-1; 
	  else
		$jum_data = $this->input->post('jum_data', TRUE);
	  
	  // 22-12-2014
	  $tgl_forecast 	= $this->input->post('tgl_forecast', TRUE);
	  $pisah1 = explode("-", $tgl_forecast);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_forecast = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$gudang' ");
	  $hasilrow = $query3->row();
	  $kode_gudang	= $hasilrow->kode_gudang;
	  $nama_gudang	= $hasilrow->nama;
	  $nama_lokasi	= $hasilrow->nama_lokasi;
	  
	  if ($is_new == '1') {
		  $uid_update_by = $this->session->userdata('uid');
	      // insert ke tabel tt_forecast_
	      $data_header = array(
			'id_gudang'=>$gudang,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'i_branch'=>$branch,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_forecast'=>$tgl_forecast, 
			  'uid_update_by'=>$uid_update_by
			);
		  $this->db->insert('tt_forecast_gudang_jd',$data_header); 
		  
		  // ambil data terakhir di tabel tt_forecast_
		 $query2	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id;
	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mmaster->save($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_warna_'.$i, TRUE),
			
			 $gudang, $this->input->post('hapusitem_'.$i, TRUE)
			 );
		  }
		
		$data['msg'] = "Input forecast WIP untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah berhasil disimpan.";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['list_branch'] = $this->mmaster->get_branch();
		$data['isi'] = 'forecast-gdjd/vmainform';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
			  // ambil data id dari tabel tt_forecast_gudang_jd
			 $query2	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd WHERE id_gudang = '$gudang'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 // 06-02-2014, query ke tabel tt_forecast_gudang_jd_detail utk hapus tiap2 item warnanya
			 $sqlxx	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail
								WHERE id_forecast_gudang_jd = '$id_stok' ");
			if ($sqlxx->num_rows() > 0){
				$hasilxx = $sqlxx->result();
				foreach ($hasilxx as $rowxx) {
					$this->db->query(" DELETE FROM tt_forecast_gudang_jd_detail_warna WHERE id_forecast_gudang_jd_detail='$rowxx->id' ");
				} // end foreach detail
			} // end if
			 //==============================================================================================
			 
			$this->db->query(" delete from tt_forecast_gudang_jd_detail where id_forecast_gudang_jd = '$id_stok' ");
			$this->db->query(" delete from tt_forecast_gudang_jd where id = '$id_stok' ");
			//redirect('forecast-gdjd/cform');
			
			$data['msg'] = "Input forecast WIP untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah berhasil dihapus";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'forecast-gdjd/vmainform';
			$this->load->view('template',$data);
		  }
		  else { // function update
			  if ($is_pertamakali == '1') {
				  $query2	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd WHERE
								bulan = '$bulan' AND tahun = '$tahun' AND id_gudang='$gudang' ");
					$hasilrow = $query2->row();
					$id_stok	= $hasilrow->id;
					
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tt_forecast_gudang_jd SET tgl_forecast = '$tgl_forecast', tgl_update = '$tgl', 
								 uid_update_by='$uid_update_by' where id = '$id_stok' ");
					
					// 1. update data yg sudah ada
					$jum_data_ada = $this->input->post('jum_data', TRUE); 
					for ($i=1;$i<=$jum_data_ada;$i++)
					 {									
						$id_brg_wip1 = $this->input->post('id_brg_wip1_'.$i, TRUE);
						$id_warna1 = $this->input->post('id_warna1_'.$i, TRUE);
						$qty_warna1 = $this->input->post('qty_warna1_'.$i, TRUE);
						$stok1 = $this->input->post('stok1_'.$i, TRUE);
						// 01-12-2015
						$id_warna12 = $this->input->post('id_warna12_'.$i, TRUE);
						$qty_warna12 = $this->input->post('qty_warna12_'.$i, TRUE);
						
						$queryxx	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail WHERE
								id_forecast_gudang_jd = '$id_stok' AND id_brg_wip = '$id_brg_wip1' ");
						$hasilxx = $queryxx->row();
						$iddetail	= $hasilxx->id;
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna1); $xx++) {
							$id_warna1[$xx] = trim($id_warna1[$xx]);
							$stok1[$xx] = trim($stok1[$xx]);
							$qtytotalstokawal+= $stok1[$xx];				
							$qty_warna1[$xx] = trim($qty_warna1[$xx]);
							$qtytotalstokfisikwarna+= $qty_warna1[$xx];
							
						
							
							// 04-02-2016 update di tabel perwarna
							$this->db->query(" UPDATE tt_forecast_gudang_jd_detail_warna SET jum_forecast = '".$qty_warna1[$xx]."'
									 WHERE id_forecast_gudang_jd_detail = '$iddetail'
									AND id_warna = '".$id_warna1[$xx]."' ");
							
						} // end for
						
						$this->db->query(" UPDATE tt_forecast_gudang_jd_detail SET jum_forecast = '".$qtytotalstokfisikwarna."'
									 where id_brg_wip = '$id_brg_wip1' AND id_forecast_gudang_jd = '$id_stok' ");
						
					 } // end for
					// --------------------------------------------------------
						 
						 				  $query2	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd WHERE
								bulan = '$bulan' AND tahun = '$tahun' AND id_gudang='$gudang' ");
					$hasilrow = $query2->row();
					$id_stok	= $hasilrow->id;
						 
					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {						
						$tgl = date("Y-m-d H:i:s"); 
					  //-------------- hitung total qty dari detail tiap2 warna -------------------
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$stok_fisik_warna = $this->input->post('stok_fisik_warna_'.$i, TRUE);
						$stok = $this->input->post('stok_'.$i, TRUE);
						// 01-12-2015
					
												
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;

						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok[$xx] = trim($stok[$xx]);
							$qtytotalstokawal+= $stok[$xx];				
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
							
							// 01-12-2015
						
						} // end for
						
						// 04-02-2016
						if ($id_brg_wip != '') {
							$data_detail = array(
										'id_forecast_gudang_jd'=>$id_stok,
										'id_brg_wip'=>$id_brg_wip, 
										
										'jum_forecast'=>$qtytotalstokfisikwarna,
										
									);
						   $this->db->insert('tt_forecast_gudang_jd_detail',$data_detail);
						   
						   $seq_detail	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail ORDER BY id DESC LIMIT 1 ");
						   if($seq_detail->num_rows() > 0) {
								$seqrow	= $seq_detail->row();
								$iddetail = $seqrow->id;
						   }
						   else
								$iddetail = 0;
								
						   // ------------------stok berdasarkan warna brg wip----------------------------
						   if (isset($id_warna) && is_array($id_warna)) {
							for ($xx=0; $xx<count($id_warna); $xx++) {
								$id_warna[$xx] = trim($id_warna[$xx]);
								$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
								
								
								$seq_warna	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail_warna ORDER BY id DESC LIMIT 1 ");
								if($seq_warna->num_rows() > 0) {
									$seqrow	= $seq_warna->row();
									$idbaru	= $seqrow->id+1;
								}else{
									$idbaru	= 1;
								}
								
								$tt_forecast_gudang_jd_detail_warna	= array(
										 'id'=>$idbaru,
										 'id_forecast_gudang_jd_detail'=>$iddetail,
										 'id_warna'=>$id_warna[$xx],
										 'jum_forecast'=>$stok_fisik_warna[$xx]
										 
									);
								$this->db->insert('tt_forecast_gudang_jd_detail_warna',$tt_forecast_gudang_jd_detail_warna);
							} // end for
						  } // end if
						}
						// ----- end lanjutan 07-10-2015 ----------
					// ---------------------------------------------------------------------
				} // END FOR
			  }
			  else { 
				 
				  		  $uid_update_by = $this->session->userdata('uid');
	      // insert ke tabel tt_forecast_
	      $data_header = array(
			'id_gudang'=>$gudang,
			  'bulan'=>$bulan,
			  'i_branch'=>$branch,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_forecast'=>$tgl_forecast,
			  'uid_update_by'=>$uid_update_by
			);
		  $this->db->insert('tt_forecast_gudang_jd',$data_header); 

 $query2	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd WHERE
								bulan = '$bulan' AND tahun = '$tahun' AND id_gudang='$gudang' ");
					$hasilrow = $query2->row();
					$id_stok	= $hasilrow->id;

					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {						
						$tgl = date("Y-m-d H:i:s"); 
					  //-------------- hitung total qty dari detail tiap2 warna -------------------
						$id_brg_wip = $this->input->post('id_brg_wip1_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna1_'.$i, TRUE);
						$stok_fisik_warna = $this->input->post('qty_warna1_'.$i, TRUE);
						$stok = $this->input->post('stok_'.$i, TRUE);
						// 01-12-2015
					
												
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;

						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							//~ $stok[$xx] = trim($stok[$xx]);
							//~ $qtytotalstokawal+= $stok[$xx];				
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
							
							// 01-12-2015
						
						} // end for
						
						// 04-02-2016
						if ($id_brg_wip != '') {
							$data_detail = array(
										'id_forecast_gudang_jd'=>$id_stok,
										'id_brg_wip'=>$id_brg_wip, 
										//~ 'stok_awal'=>$qtytotalstokawal,
										'jum_forecast'=>$qtytotalstokfisikwarna,
										
									);
						   $this->db->insert('tt_forecast_gudang_jd_detail',$data_detail);
						   
						   $seq_detail	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail ORDER BY id DESC LIMIT 1 ");
						   if($seq_detail->num_rows() > 0) {
								$seqrow	= $seq_detail->row();
								$iddetail = $seqrow->id;
						   }
						   else
								$iddetail = 0;
								
						   // ------------------stok berdasarkan warna brg wip----------------------------
						   if (isset($id_warna) && is_array($id_warna)) {
							for ($xx=0; $xx<count($id_warna); $xx++) {
								$id_warna[$xx] = trim($id_warna[$xx]);
								$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
								
								
								$seq_warna	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail_warna ORDER BY id DESC LIMIT 1 ");
								if($seq_warna->num_rows() > 0) {
									$seqrow	= $seq_warna->row();
									$idbaru	= $seqrow->id+1;
								}else{
									$idbaru	= 1;
								}
								
								$tt_forecast_gudang_jd_detail_warna	= array(
										 'id'=>$idbaru,
										 'id_forecast_gudang_jd_detail'=>$iddetail,
										 'id_warna'=>$id_warna[$xx],
										 'jum_forecast'=>$stok_fisik_warna[$xx]
										 
									);
								$this->db->insert('tt_forecast_gudang_jd_detail_warna',$tt_forecast_gudang_jd_detail_warna);
							} // end for
						  } // end if
						}
						// ----- end lanjutan 07-10-2015 ----------
					// ---------------------------------------------------------------------
				} // END FOR

					// --------------------------------------------------------
					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {						
						$tgl = date("Y-m-d H:i:s"); 
					  //-------------- hitung total qty dari detail tiap2 warna -------------------
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$stok_fisik_warna = $this->input->post('stok_fisik_warna_'.$i, TRUE);
						$stok = $this->input->post('stok_'.$i, TRUE);
						// 01-12-2015
					
												
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;

						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok[$xx] = trim($stok[$xx]);
							$qtytotalstokawal+= $stok[$xx];				
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
							
							// 01-12-2015
						
						} // end for
						
						// 04-02-2016
						if ($id_brg_wip != '') {
							$data_detail = array(
										'id_forecast_gudang_jd'=>$id_stok,
										'id_brg_wip'=>$id_brg_wip, 
										//~ 'stok_awal'=>$qtytotalstokawal,
										'jum_forecast'=>$qtytotalstokfisikwarna,
										
									);
						   $this->db->insert('tt_forecast_gudang_jd_detail',$data_detail);
						   
						   $seq_detail	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail ORDER BY id DESC LIMIT 1 ");
						   if($seq_detail->num_rows() > 0) {
								$seqrow	= $seq_detail->row();
								$iddetail = $seqrow->id;
						   }
						   else
								$iddetail = 0;
								
						   // ------------------stok berdasarkan warna brg wip----------------------------
						   if (isset($id_warna) && is_array($id_warna)) {
							for ($xx=0; $xx<count($id_warna); $xx++) {
								$id_warna[$xx] = trim($id_warna[$xx]);
								$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
								
								
								$seq_warna	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail_warna ORDER BY id DESC LIMIT 1 ");
								if($seq_warna->num_rows() > 0) {
									$seqrow	= $seq_warna->row();
									$idbaru	= $seqrow->id+1;
								}else{
									$idbaru	= 1;
								}
								
								$tt_forecast_gudang_jd_detail_warna	= array(
										 'id'=>$idbaru,
										 'id_forecast_gudang_jd_detail'=>$iddetail,
										 'id_warna'=>$id_warna[$xx],
										 'jum_forecast'=>$stok_fisik_warna[$xx]
										 
									);
								$this->db->insert('tt_forecast_gudang_jd_detail_warna',$tt_forecast_gudang_jd_detail_warna);
							} // end for
						  } // end if
						}
						// ----- end lanjutan 07-10-2015 ----------
					// ---------------------------------------------------------------------
				} // END FOR
				
				
				/*
				$jum_data_ada = $this->input->post('jum_data', TRUE); 
					for ($i=1;$i<=$jum_data_ada;$i++)
					 {									
						$id_brg_wip1 = $this->input->post('id_brg_wip1_'.$i, TRUE);
						$id_warna1 = $this->input->post('id_warna1_'.$i, TRUE);
						$qty_warna1 = $this->input->post('qty_warna1_'.$i, TRUE);
						$stok1 = $this->input->post('stok1_'.$i, TRUE);
						// 01-12-2015
						$id_warna12 = $this->input->post('id_warna12_'.$i, TRUE);
						$qty_warna12 = $this->input->post('qty_warna12_'.$i, TRUE);
						
						$queryxx	= $this->db->query(" SELECT * FROM tt_forecast_gudang_jd_detail WHERE
								id_forecast_gudang_jd = '$id_tt_forecast_gudang_jd' AND id_brg_wip = '$id_brg_wip1' ");
						$hasilxx = $queryxx->row();
						$id_tt_forecast_gudang_jd	= $hasilxx->id;
						$jum_forecast	= $hasilxx->jum_forecast;
						
						if ($id_tt_forecast_gudang_jd != '') {
							$data_detail = array(
										'id_forecast_gudang_jd'=>$iddetail,
										'id_brg_wip'=>$id_brg_wip1, 
										'stok_awal'=>$qtytotalstokawal,
										'jum_forecast'=>$jum_forecast,
										
									);
						   $this->db->insert('tt_forecast_gudang_jd_detail',$data_detail);
						   
						   $seq_detail	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail ORDER BY id DESC LIMIT 1 ");
						   if($seq_detail->num_rows() > 0) {
								$seqrow	= $seq_detail->row();
								$iddetail = $seqrow->id;
						   }
						   else
								$iddetail = 0;
								
						   // ------------------stok berdasarkan warna brg wip----------------------------
						   if (isset($id_warna1) && is_array($id_warna1)) {
							for ($xx=0; $xx<count($id_warna1); $xx++) {
								$id_warna1[$xx] = trim($id_warna1[$xx]);
							//	print_r($id_warna1);
								$qty_warna1[$xx] = trim($qty_warna1[$xx]);
								
								
								$seq_warna	= $this->db->query(" SELECT id FROM tt_forecast_gudang_jd_detail_warna ORDER BY id DESC LIMIT 1 ");
								if($seq_warna->num_rows() > 0) {
									$seqrow	= $seq_warna->row();
									$idbaru	= $seqrow->id+1;
								}else{
									$idbaru	= 1;
								}
								
								$tt_forecast_gudang_jd_detail_warna	= array(
										 'id'=>$idbaru,
										 'id_forecast_gudang_jd_detail'=>$iddetail,
										 'id_warna'=>$id_warna1[$xx],
										 'jum_forecast'=>$qty_warna1[$xx]
										 
									);
								$this->db->insert('tt_forecast_gudang_jd_detail_warna',$tt_forecast_gudang_jd_detail_warna);
							}
						  } 
						}
						
					 }
				
					*/
					
			  }
			//  redirect('forecast-gdjd/cform');
			$data['msg'] = "Input forecast WIP untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah berhasil diupdate.";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['list_branch'] = $this->mmaster->get_branch();
			$data['isi'] = 'forecast-gdjd/vmainform';
			$this->load->view('template',$data);
		}
      }
  }
  
  // 20-10-2015
  function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$isedit 	= $this->input->post('isedit', TRUE);
		$id_forecast 	= $this->input->post('id_forecast', TRUE);
		
		if ($isedit == '0') {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
										WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		else {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
			}
			else {
				$id_brg_wip = '0';
			}
			
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
					WHERE id = '".$id_brg_wip."' AND id NOT IN 
					(SELECT b.id_brg_wip FROM tt_forecast_gudang_jd a 
					INNER JOIN tt_forecast_gudang_jd_detail b ON a.id = b.id_forecast_gudang_jd
					WHERE a.id = '$id_forecast' ) ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		
		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('forecast-gdjd/vinfobrgwip', $data); 
		return true;
  }
  
  function additemwarna(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$id_forecast 	= $this->input->post('id_forecast', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		if ($id_forecast != '') {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							AND a.id_brg_wip NOT IN 
						(SELECT b.id_brg_wip FROM tt_forecast_gudang_jd a 
						INNER JOIN tt_forecast_gudang_jd_detail b ON a.id = b.id_forecast_gudang_jd
						WHERE a.id = '$id_forecast' )
							ORDER BY b.nama");
		}
		else {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							ORDER BY b.nama");
		}
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('forecast-gdjd/vlistwarna', $data); 
		return true;
  }
  
  function show_popup_brg(){
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);
			
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	$posisi 	= $this->uri->segment(4);
	
	 if ($posisi == '')
	 $posisi 	= $this->input->post('posisi', TRUE);
	 
	$qjum_total = $this->mmaster->get_listbrgtanpalimit($keywordcari);

			$config['base_url'] = base_url()."index.php/forecast-gdjd/cform/show_popup_brg/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_listbrg($config['per_page'],$config['cur_page'], $keywordcari);
	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['posisi'] = $posisi;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('forecast-gdjd/vpopupbrg',$data);
  }
		
  
  

  
  
  
}
