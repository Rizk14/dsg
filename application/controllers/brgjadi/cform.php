<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();	
		
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
				$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
			$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
			$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
			$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
			$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
			$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
			$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
			$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
			$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
			$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
			$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
			$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
			$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
			$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
			$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
			$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');
				
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['tgl']		= date("dd/mm/YYYY");
			$data['limages']	= base_url();
			$data['lurl']		= base_url();
			$data['optstatusbarang']	= "";

			$this->load->model('brgjadi/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
		
			$pagination['base_url'] 	= base_url().'/index.php/brgjadi/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();

			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");	
			$data['isi']='brgjadi/vmainform';
			$this->load->view('template',$data);
		
	}
	
	function index_cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
				$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
			$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
			$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
			$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
			$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
			$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
			$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
			$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
			$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
			$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
			$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
			$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
			$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
			$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
			$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
			$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');
				
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['tgl']		= date("dd/mm/YYYY");
			$data['limages']	= base_url();
			$data['lurl']		= base_url();
			$data['optstatusbarang']	= "";

			$this->load->model('brgjadi/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
		
			$pagination['base_url'] 	= base_url().'/index.php/brgjadi/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();

			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");	
			$data['isi']='brgjadi/vmainformcari';
			$this->load->view('template',$data);
		
	}
	function tambah() {
		
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
			$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
			$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
			$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
			$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
			$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
			$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
			$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
			$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
			$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
			$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
			$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
			$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
			$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
			$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
			$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
			$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');
				
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['tgl']		= date("dd/mm/YYYY");
			$data['limages']	= base_url();
			$data['lurl']		= base_url();
			$data['optstatusbarang']	= "";

			$this->load->model('brgjadi/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
		
			$pagination['base_url'] 	= base_url().'index.php/brgjadi/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();

			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");	
			$data['isi']='brgjadi/vmainformadd';
			$this->load->view('template',$data);
		}
	
	
	function pagesnext() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$cari	= $this->uri->segment('4');
		$uri1	= $cari;
		if($cari=='kosong')
			$cari	= '';
			
		$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
		$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
		$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
		$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
		$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
		$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
		$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
		$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
		$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
		$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
		$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
		$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
		$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
		$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
		$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
		$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi');
				
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['tgl']		= date("dd/mm/YYYY");
		$data['limages']	= base_url();
		$data['lurl']		= base_url();
		$data['optstatusbarang']	= "";

		$this->load->model('brgjadi/mclass');
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'/index.php/brgjadi/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");	
		
		$data['isi']='brgjadi/vmainform';
			$this->load->view('template',$data);
	}
	
	function detail() {	
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$this->load->view('brgjadi/vmainform',$data);
	}

	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$vi_product_base1	= $this->input->post('i_product_base1');
		$ve_product_basename1	= $this->input->post('e_product_basename1');
		$vi_category1		= $this->input->post('i_category1');
		$vi_brand1		= $this->input->post('i_brand1');
		$vi_layout1		= $this->input->post('i_layout1');
		$vv_price1		= $this->input->post('v_price');
		
		$is_grosir		= $this->input->post('is_grosir');
		$harga_grosir		= $this->input->post('harga_grosir');
		
		$ve_quality1	= $this->input->post('e_quality1');
		$vf_stop_produksi1	= $this->input->post('f_stop_produksi1')=='1'?'true':'false';
		$ve_surat_penawaran1	= $this->input->post('e_surat_penawaran1');
		$d_penawaran	= $this->input->post('d_penawaran');
		$vn_status		= $this->input->post('n_status');
		$vd_launching1	= $this->input->post('d_launching1');
		$n_quantity	= $this->input->post('n_quantity');
		$ibrand		= $this->input->post('i_brand1_typ_hidden');
		$icategory	= $this->input->post('i_category1_typ_hidden');
		$ilayout	= $this->input->post('i_layout1_typ_hidden');
		
		$e_d_penawaran = !empty($d_penawaran)?explode("/",$d_penawaran,strlen($d_penawaran)):date("d/m/Y"); // dd/mm/YYYY
		$n_d_penawaran = $e_d_penawaran[2]."-".$e_d_penawaran[1]."-".$e_d_penawaran[0];
		$e_vd_launching1 = !empty($vd_launching1)?explode("/",$vd_launching1,strlen($vd_launching1)):date("d/m/Y"); // dd/mm/YYYY
		$n_vd_launching1 = $e_vd_launching1[2]."-".$e_vd_launching1[1]."-".$e_vd_launching1[0];
		
		$stupper_iproduct 		= strtoupper($vi_product_base1);
		$stupper_eproductbase	= strtoupper($ve_product_basename1);
		
		$this->load->model('brgjadi/mclass');
		
		if(!empty($vi_product_base1) &&
		   !empty($ve_product_basename1) &&
		   !empty($vn_status)
		) {	
			
			$cek_imotif	= $this->mclass->cek_imotif($stupper_iproduct);
			$num_imotif	= $cek_imotif->num_rows();
			
			if($num_imotif==0){
				$this->mclass->msimpan($stupper_iproduct,$stupper_eproductbase,$icategory,$ibrand,$ilayout,$vv_price1,
				$ve_quality1,$vf_stop_produksi1,$ve_surat_penawaran1,$n_d_penawaran,$vn_status,$n_vd_launching1,$n_quantity,
				$is_grosir, $harga_grosir);
			}else{
				$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
				$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
				$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
				$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
				$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
				$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
				$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
				$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
				$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
				$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
				$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
				$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
				$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
				$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
				$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
				$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
				$data['button_cari']	= $this->lang->line('button_cari');
				$data['text_cari']		= $this->lang->line('text_cari');
				$data['link_aksi']		= $this->lang->line('link_aksi');
						
				$data['isi']		= "";
				$data['list']		= "";
				$data['not_defined']	= "Kode Barang Sudah Ada!";
				$data['tgl']		= date("dd/mm/YYYY");
				$data['limages']	= base_url();
				$data['lurl']		= base_url();
				$data['optstatusbarang']	= "";
		
				$query	= $this->mclass->view();
				$jml	= $query->num_rows();
				$result	= $query->result();
				
				$pagination['base_url'] 	= base_url().'index.php/brgjadi/cform/pagesnext/';
				$pagination['total_rows']	= $jml;
				$pagination['per_page']		= 50;
				$pagination['first_link'] 	= 'Awal';
				$pagination['last_link'] 	= 'Akhir';
				$pagination['next_link'] 	= 'Selanjutnya';
				$pagination['prev_link'] 	= 'Sebelumnya';
				$pagination['cur_page'] 	= $this->uri->segment(4,0);
				$this->pagination->initialize($pagination);
				$data['create_link']	= $this->pagination->create_links();
		
				$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
				$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");	
				
				$this->load->view('brgjadi/vmainform',$data);			
			}
		} else {
			$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
			$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
			$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
			$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
			$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
			$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
			$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
			$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
			$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
			$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
			$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
			$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
			$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
			$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
			$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
			$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');			
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['tgl']		= date("dd/mm/YYYY");
			$data['limages']	= base_url();
			$data['lurl']		= base_url();
			$data['optstatusbarang']	= "";
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= base_url().'index.php/brgjadi/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
	
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");	
			
			$this->load->view('brgjadi/vmainform',$data);
		}
	}

	function listpemasokbarang() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['page_title']	= "PEMASOK";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('brgjadi/mclass');
		$data['isi']	= $this->mclass->listpquery($tabel="tr_supplier",$order="ORDER BY i_supplier ",$filter="");	
		$this->load->view('brgjadi/vlistformpemasok',$data);
	}

	function listmerekbarang() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['page_title']	= "MEREK BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('brgjadi/mclass');
		$data['isi']	= $this->mclass->pquery($tabel="tr_brand",$order="ORDER BY i_brand",$filter="");	
		$this->load->view('brgjadi/vlistformbrand',$data);
	}

	function listkategoribarang() {
		$data['page_title']	= "KATEGORI BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('brgjadi/mclass');
		$data['isi']	= $this->mclass->listpquery($tabel="tr_categories",$order="ORDER BY i_category",$filter="");	
		$this->load->view('brgjadi/vlistformkategori',$data);
	}

	function listtampilanbarang() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['page_title']	= "LAYOUT BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('brgjadi/mclass');
		$data['isi']	= $this->mclass->listpquery($tabel="tr_layout",$order="ORDER BY i_layout",$filter="");	
		$this->load->view('brgjadi/vlistformlayout',$data);
	}
	
	function edit() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$id	= $this->uri->segment(4);
		
		$data['id']	= $id;	
		$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
		$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
		$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
		$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
		$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
		$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
		$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
		$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
		$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
		$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
		$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
		$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
		$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
		$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
		$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
		$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
		
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi');
		
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['tgl']		= date("dd/mm/YYYY");
		$data['limages']	= base_url();
		$data['lurl']		= base_url();
		$data['optstatusbarang']	= "";
		
		$this->load->model('brgjadi/mclass');
		
		$qry_brg_jadi	= $this->mclass->medit($id);
		
		if($qry_brg_jadi->num_rows()>0) {
			
			$arr	= $qry_brg_jadi->row_array();
			
			$data['i_product_base']		= $arr['i_product_base'];
			$data['e_product_basename']	= $arr['e_product_basename'];
			
			$iproductbase = $arr['i_product_base'];
			$icategory	= $arr['i_category'];
			$ibrand		= $arr['i_brand'];
			$ilayout	= $arr['i_layout'];
			
			$data['i_category']	= $icategory;
			$data['i_brand']	= $ibrand;
			$data['i_layout']	= $ilayout;
			
			$data['v_unitprice']= $arr['v_unitprice'];
			$data['harga_grosir']= $arr['harga_grosir'];
			
			$data['f_stop_produksi']	= $arr['f_stop_produksi']=='t'?'checked':'';
			$data['e_quality']	= $arr['e_quality'];
			$data['e_surat_penawaran']	= $arr['e_surat_penawaran'];	
			$tglpenawaran	= explode("-",$arr['d_surat_penawaran'],strlen($arr['d_surat_penawaran']));

			$tglregistrasi0	= explode("-",$arr['d_entry'],strlen($arr['d_entry']));
			$dsuratpenawaran	= strlen($tglpenawaran[0])!=0?$tglpenawaran[2]."/".$tglpenawaran[1]."/".$tglpenawaran[0]:""; // Y-m-d
			$dentry			= strlen($tglregistrasi0[0])!=0?$tglregistrasi0[2]."/".$tglregistrasi0[1]."/".$tglregistrasi0[0]:""; // Y-m-d
			$data['d_surat_penawaran']	= $dsuratpenawaran;
			$data['d_entry']	= $dentry;
			$data['n_status']	= $arr['n_status'];

			$qry_layout		= $this->mclass->ilayout($ilayout);
			
			if($qry_layout->num_rows()>0) {
				$row_layout	= $qry_layout->row();
				$data['layout'] = $row_layout->e_layout_name;
			}else{
				$data['layout'] = "";			
			}

			$qry_category		= $this->mclass->icategory($icategory);
			
			if($qry_category->num_rows()>0) {
				$row_category	= $qry_category->row();
				$data['category'] = $row_category->e_category_name;
			}else{
				$data['category'] = "";			
			}

			$qry_brand		= $this->mclass->ibrand($ibrand);
			
			if($qry_brand->num_rows()>0) {
				$row_brand	= $qry_brand->row();
				$data['brand'] = $row_brand->e_brand_name;
			}else{
				$data['brand'] = "";
			}

			$qry_qty		= $this->mclass->qty($iproductbase);
			if($qry_qty->num_rows() > 0 ) {
				$row_qty	= $qry_qty->row();
				$data['n_quantity'] = $row_qty->n_quantity;
			}else{
				$data['n_quantity'] = "";
			}									
		}
		
		$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");
		
		$data['isi']='brgjadi/veditform';
		$this->load->view('template',$data);				
	}
	
	function actedit() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$iproductbase	= $this->input->post('i_product_base1');
		$eproductbasename	= $this->input->post('e_product_basename1');
		$icategory	= $this->input->post('i_category1');
		$ibrand		= $this->input->post('i_brand1');
		$ilayout	= $this->input->post('i_layout1');
		$vprice		= $this->input->post('v_price');
		
		$is_grosir		= $this->input->post('is_grosir');
		$harga_grosir		= $this->input->post('harga_grosir');
		
		$equality	= $this->input->post('e_quality1');
		$f_stop_produksi	= $this->input->post('f_stop_produksi1')=='1'?'true':'false';
		$esuratpenawaran	= $this->input->post('e_surat_penawaran1');
		$dpenawaran	= $this->input->post('d_penawaran');
		$nstatus	= $this->input->post('n_status');
		$dlaunching	= $this->input->post('d_launching1');
		$nquantity	= $this->input->post('n_quantity');
		$ibrand_typ_hidden		= $this->input->post('i_brand1_typ_hidden');
		$icategory_typ_hidden	= $this->input->post('i_category1_typ_hidden');
		$ilayout_typ_hidden	= $this->input->post('i_layout1_typ_hidden');
		
		$i_brand	= !empty($ibrand_typ_hidden)?$ibrand_typ_hidden:$ibrand;
		$i_category	= !empty($icategory_typ_hidden)?$icategory_typ_hidden:$icategory;
		$i_layout	= $ilayout_typ_hidden;

		$exp_dpenawaran	= explode("/",$dpenawaran,strlen($dpenawaran)); // dd/mm/YYYY
		$new_dpenawaran	= !empty($exp_dpenawaran[2])?$exp_dpenawaran[2]."-".$exp_dpenawaran[1]."-".$exp_dpenawaran[0]:date("Y-m-d");
		
		$this->load->model('brgjadi/mclass');
		
		if(!empty($iproductbase) &&
		   !empty($eproductbasename)
		) {
			$this->mclass->mupdate($iproductbase,$eproductbasename,$i_category,$i_brand,$i_layout,$vprice, $is_grosir, $harga_grosir, 
			$equality,$f_stop_produksi,$esuratpenawaran,$new_dpenawaran,$nstatus,$dlaunching,$nquantity);			
		}else{

			$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
			$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
			$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
			$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
			$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
			$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
			$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
			$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
			$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
			$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
			$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
			$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
			$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
			$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
			$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
			$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');
					
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['tgl']		= date("dd/mm/YYYY");
			$data['limages']	= base_url();
			$data['lurl']		= base_url();
			$data['optstatusbarang']	= "";
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] 	= base_url().'brgjadi/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
	
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");

			$this->load->view('brgjadi/vmainform',$data);
		}	
	}
	
	function actdelete() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$id	= $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		
		$this->load->model('brgjadi/mclass');
		
		if(!empty($id)) {	
			
			$query_motifname	= $this->mclass->getmotifname($id);
			
			if($query_motifname->num_rows() > 0) {
				$row_motifname	= $query_motifname->row();
				$iproductmotif	= $row_motifname->i_product_motif;
			}
		}
		
		$this->db->delete('tr_product_base',array('i_product_base'=>$id));
		
		if($this->db->delete('tr_product_motif',array('i_product_motif'=>$iproductmotif))) {
			$this->db->delete('tr_product_motif',array('i_product'=>$id));
		}else{
			$this->db->delete('tr_product_motif',array('i_product'=>$id));
		}
		
		$this->db->delete('tr_product_price',array('i_product'=>$iproductmotif));
		
	}
	
	function cari() {
		
		$cari	= $this->input->post('cari');
		$uri1	= $cari;
		
		if($cari=='')
			$uri1	= 'kosong';
			
		$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
		$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
		$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
		$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
		$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
		$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
		$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
		$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
		$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
		$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
		$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
		$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
		$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
		$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
		$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
		$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['text_cari']		= $this->lang->line('text_cari');			
		$data['link_aksi']		= $this->lang->line('link_aksi');
			
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['tgl']		= date("dd/mm/YYYY");
		$data['limages']	= base_url();
		$data['lurl']		= base_url();
		$data['optstatusbarang']	= "";
			
		$this->load->model('brgjadi/mclass');
		
		$query	= $this->mclass->viewcari($cari);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'brgjadi/cform/pagesnext/'.$uri1.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();

		$data['query']	= $this->mclass->mcari($cari,$pagination['per_page'],$pagination['cur_page']);
		$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");	
		
		$data['isi']='brgjadi/vmainform';
		$this->load->view('template',$data);
	}
	
	function carikodebrg() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$kodebrg	= $this->input->post('kodebrg');
		$tokodebrg	= strtoupper($kodebrg);
		
		$this->load->model('brgjadi/mclass');
		
		$qkode	= $this->mclass->carikodebrg($tokodebrg);
		
		if($qkode->num_rows()>0) {
			echo "Maaf, Kode Brg sdh ada!";
		}		
	}
	
	function detailbrg() {
		
		$id = $this->uri->segment(4,0);

		$data['id']	= $id;	
		$data['form_title_brgjadi']		= $this->lang->line('form_title_brgjadi');
		$data['form_kd_brg_brgjadi']	= $this->lang->line('form_kd_brg_brgjadi');
		$data['form_nm_brg_brgjadi']	= $this->lang->line('form_nm_brg_brgjadi');
		$data['form_pemasok_brgjadi']	= $this->lang->line('form_pemasok_brgjadi');
		$data['form_merek_brgjadi']		= $this->lang->line('form_merek_brgjadi');
		$data['form_quality_brgjadi']	= $this->lang->line('form_quality_brgjadi');
		$data['form_spenawaran_brgjadi']= $this->lang->line('form_spenawaran_brgjadi');
		$data['form_sbrg_brgjadi']		= $this->lang->line('form_sbrg_brgjadi');
		$data['form_pilih_status_brgjadi']	= $this->lang->line('form_pilih_status_brgjadi');
		$data['form_tglregist_brgjadi']	= $this->lang->line('form_tglregist_brgjadi');
		$data['form_kat_brgjadi']	= $this->lang->line('form_kat_brgjadi');
		$data['form_layout_brgjadi']	= $this->lang->line('form_layout_brgjadi');
		$data['form_sproduksi_brgjadi']	= $this->lang->line('form_sproduksi_brgjadi');
		$data['form_tpenawaran_brgjadi']	= $this->lang->line('form_tpenawaran_brgjadi');
		$data['form_qty_brgjadi']	= $this->lang->line('form_qty_brgjadi');
		$data['form_hjp_brgjadi']	= $this->lang->line('form_hjp_brgjadi');
		
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi');
		
		
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['tgl']		= date("dd/mm/YYYY");
		$data['limages']	= base_url();
		$data['lurl']		= base_url();
		$data['optstatusbarang']	= "";
		
		$this->load->model('brgjadi/mclass');
		
		$qry_brg_jadi	= $this->mclass->medit($id);
		
		if($qry_brg_jadi->num_rows()>0) {
			
			$arr	= $qry_brg_jadi->row_array();
			
			$data['i_product_base']		= $arr['i_product_base'];
			$data['e_product_basename']	= $arr['e_product_basename'];
			
			$iproductbase = $arr['i_product_base'];
			$icategory	= $arr['i_category'];
			$ibrand		= $arr['i_brand'];
			$ilayout	= $arr['i_layout'];
			
			$data['i_category']	= $icategory;
			$data['i_brand']	= $ibrand;
			$data['i_layout']	= $ilayout;
			
			$data['v_unitprice']= $arr['v_unitprice'];
			$data['harga_grosir']= $arr['harga_grosir'];
			$data['f_stop_produksi']	= $arr['f_stop_produksi']=='t'?'checked':'';
			$data['e_quality']	= $arr['e_quality'];
			$data['e_surat_penawaran']	= $arr['e_surat_penawaran'];	
			$tglpenawaran	= explode("-",$arr['d_surat_penawaran'],strlen($arr['d_surat_penawaran']));

			$tglregistrasi0	= explode("-",$arr['d_entry'],strlen($arr['d_entry']));
			$dsuratpenawaran	= strlen($tglpenawaran[0])!=0?$tglpenawaran[2]."/".$tglpenawaran[1]."/".$tglpenawaran[0]:""; // Y-m-d
			$dentry			= strlen($tglregistrasi0[0])!=0?$tglregistrasi0[2]."/".$tglregistrasi0[1]."/".$tglregistrasi0[0]:""; // Y-m-d
			$data['d_surat_penawaran']	= $dsuratpenawaran;
			$data['d_entry']	= $dentry;
			$data['n_status']	= $arr['n_status'];

			$qry_layout		= $this->mclass->ilayout($ilayout);
			
			if($qry_layout->num_rows()>0) {
				$row_layout	= $qry_layout->row();
				$data['layout'] = $row_layout->e_layout_name;
			}else{
				$data['layout'] = "";			
			}

			$qry_category		= $this->mclass->icategory($icategory);
			
			if($qry_category->num_rows()>0) {
				$row_category	= $qry_category->row();
				$data['category'] = $row_category->e_category_name;
			}else{
				$data['category'] = "";			
			}

			$qry_brand		= $this->mclass->ibrand($ibrand);
			
			if($qry_brand->num_rows()>0) {
				$row_brand	= $qry_brand->row();
				$data['brand'] = $row_brand->e_brand_name;
			}else{
				$data['brand'] = "";
			}

			$qry_qty		= $this->mclass->qty($iproductbase);
			if($qry_qty->num_rows() > 0 ) {
				$row_qty	= $qry_qty->row();
				$data['n_quantity'] = $row_qty->n_quantity;
			}else{
				$data['n_quantity'] = "";
			}									
		}
		
		$data['statusbarang']	= $this->mclass->pquery($tabel="tr_status_product",$order="ORDER BY i_status_product",$filter="");
		$data['isi']='brgjadi/vformdetail';
		$this->load->view('template',$data);		
	}
	
}
?>
