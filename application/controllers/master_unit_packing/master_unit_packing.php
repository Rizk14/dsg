<?php
class master_unit_packing extends CI_Controller
{
    public $data = array(
        'halaman' => 'master_unit_packing',        
        'title' => 'Master Unit packing',
        'isi' => 'master_unit_packing/master_unit_packing_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_unit_packing/master_unit_packing_model', 'master_unit_packing');
    }

   
    public function index()
    {
        $this->data['values'] = (object) $this->master_unit_packing->default_values;
		$this->load->view('template', $this->data);
			
    }
     public function sukses_input()
    {	
		
        $this->data['isi'] = 'master_unit_packing/master_unit_packing-sukses';
        $this->load->view('template', $this->data);
    }

    // Jika pendaftaran error, tampilkan informasi mengenai error.
    public function error_input()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Penginputan Master Unit packing Error';
        $this->load->view('template', $this->data);
    }
   public function submit()
    {
		//$nama_unit_packing=$this->input->post('nama_unit_packing',TRUE);
		//$kode_unit_packing=$this->input->post('kode_unit_packing',TRUE);
		$no=$this->input->post('no',TRUE);
        
       
		 $jumlah_input=$no-1;
		// print_r($jumlah_input);
		 for($i=1; $i<=$jumlah_input; $i++){
		$this->master_unit_packing->input(	
		$this->input->post('nama_unit_packing_'.$i,TRUE),
		$this->input->post('lokasi_'.$i,TRUE),
		$this->input->post('nama_perusahaan_'.$i,TRUE),
		$this->input->post('nama_penanggung_jawab_'.$i,TRUE),
		$this->input->post('nama_administrator_'.$i,TRUE)
		);
		
		}
		if(true)
		redirect('master_unit_packing/master_unit_packing/sukses_input');
		else 
		redirect('master_unit_packing/master_unit_packing/error_input');
    }
    
    
   public function view($offset= null)
    {	
		
       $master_unit_packing = $this->master_unit_packing->get_all_paged($offset);
        if ($master_unit_packing) {
            $this->data['master_unit_packing'] = $master_unit_packing;
            $this->data['paging'] = $this->master_unit_packing->paging('biasa', site_url('master_unit_packing/master_unit_packing/halaman/'), 4);
        } else {
            $this->data['master_unit_packing'] = 'Tidak ada data Master Unit packing, Silahkan Melakukan '.anchor('/master_unit_packing/master_unit_packing/', 'Proses penginputan.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('master_unit_packing/master_unit_packing/cari');
        $this->data['isi'] = 'master_unit_packing/master_unit_packing_list';
        $this->load->view('template', $this->data);
    }
    public function cari($offset = 0)
    {
        $master_unit_packing = $this->master_unit_packing->cari($offset);
        if ($master_unit_packing) {
            $this->data['master_unit_packing'] = $master_unit_packing;
            $this->data['paging'] = $this->master_unit_packing->paging('pencarian', site_url('/master_unit_packing/master_unit_packing/cari/'), 4);
        } else {
            $this->data['master_unit_packing'] = 'Data tidak ditemukan.'. anchor('/master_unit_packing/master_unit_packing/view', ' Tampilkan semua Master Unit packing.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('/master_unit_packing/master_unit_packing/cari');
        $this->data['isi'] = 'master_unit_packing/master_unit_packing_list';
        $this->load->view('template', $this->data);
    }
    
   
    
     public function hapus($id)
    {
        
        if ($this->session->userdata('user_bagian') != '2') {
            $this->session->set_flashdata('pesan_error', 'Anda tidak berhak menghapus data Master Unit packing. Kembali ke halaman ' . anchor('master_unit_packing/master_unit_packing', 'master_unit_packing.', 'class="alert-link"'));
            redirect('master_unit_packing/master_unit_packing/error');
        }

      
        if (! $this->master_unit_packing->get($id)) {
            $this->session->set_flashdata('pesan_error', 'Data Master Unit packing tidak ada. Kembali ke halaman ' . anchor('master_unit_packing/master_unit_packing', 'master_unit_packing.', 'class="alert-link"'));
            redirect('master_unit_packing/master_unit_packing/error');
        }

        // Hapus
        if ($this->master_unit_packing->delete($id)) {
            $this->session->set_flashdata('pesan', 'Data berhasil dihapus. Kembali ke halaman '. anchor('master_unit_packing/master_unit_packing/view', 'View Master Unit packing.', 'class="alert-link"'));
            redirect('master_unit_packing/master_unit_packing/sukses');
        } else {
            $this->session->set_flashdata('pesan_error', 'Data gagal dihapus. Kembali ke halaman '. anchor('master_unit_packing/master_unit_packing/view', 'View Master Unit packing.', 'class="alert-link"'));
            redirect('master_unit_packing/master_unit_packing/error');
        }
    }
     public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }
    
    public function edit()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $master_unit_packing = $this->master_unit_packing->get($id);
        if (! $master_unit_packing) {
            $this->session->set_flashdata('pesan_error', 'Data Master Unit packing tidak ada. Kembali ke halaman ' . anchor('master_unit_packing/master-barang/view', 'Master Unit packing.', 'class="alert-link"'));
            redirect('master_unit_packing/master-barang/error');
        }

        // Data untuk form.
        if (!$_POST) {
            $data = (object) $master_unit_packing;     
        } else {
            $data = (object) $this->input->post(null, true);
        }
        $this->data['values'] = $data;
        
		$this->data['isi'] = 'master_unit_packing/master_unit_packing_form_edit';
        $this->load->view('template', $this->data);
	 }
	    public function updatedata(){
			$id =  $this->input->post('id',TRUE);
	        $nama_unit_packing =  $this->input->post('nama_unit_packing',TRUE);
			$lokasi =  $this->input->post('lokasi',TRUE);
			$nama_perusahaan =  $this->input->post('nama_perusahaan',TRUE);
			$nama_penanggung_jawab =  $this->input->post('nama_penanggung_jawab',TRUE);
			$nama_administrator =  $this->input->post('nama_administrator',TRUE);

	    
	    if (! $this->master_unit_packing->validate('form_rules')) {
            $this->data['isi'] = 'master_unit_packing/master_unit_packing_form_edit';
            $this->data['form_action'] = site_url('master_unit_packing/master_unit_packing/edit');
            return;     
		}
    	 
		if ($this->master_unit_packing->edit($id,$nama_unit_packing,$lokasi,$nama_perusahaan,$nama_penanggung_jawab,$nama_administrator)) {
            $this->session->set_flashdata('pesan', 'Data berhasil diupdate. Kembali ke halaman ' . anchor('master_unit_packing/master_unit_packing/view', 'Master Unit packing.', 'class="alert-link"'));
            redirect('master_unit_packing/master_unit_packing/sukses');
        } else {
            $this->session->set_flashdata('pesan_error', 'Data tidak berhasil diupdate. Kembali ke halaman ' . anchor('master_unit_packing/master_unit_packing/view', 'Master Unit packing.', 'class="alert-link"'));
            redirect('/master_unit_packing/master_unit_packing/error');
	}
}
 public function cetak(){
 
		$nama = $this->session->userdata('nama');
		$today = date("YmdHis");  
		$master_unit_packing=$this->master_unit_packing->get_all();
    
        $data['master_unit_packing'] = $master_unit_packing;
		$html = $this->load->view('master_unit_packing/master_unit_packing_pdf', $data, true);
        require(APPPATH."/third_party/html2pdf_4_03/html2pdf.class.php");
        try {
            $html2pdf = new HTML2PDF('', 'A4', 'en', true, 'UTF-8', array('20', '15', '20', '5'));
            $html2pdf->WriteHTML($html);
            $html2pdf->Output('master_unit_packing_'.$nama.'_'.$today.'.pdf');
        } catch (HTML2PDF_exception $e) {
           
            $this->session->set_flashdata('pesan_error', 'Maaf, kami mengalami kendala teknis. Kembali ke halaman ' . anchor('master_unit_packing/master_unit_packing', 'master_unit_packing.', 'class="alert-link"'));
            redirect('master_unit_packing/master_unit_packing/error');
        }

}
 public function export(){
	 $nama = $this->session->userdata('nama');
	 $today = date("YmdHis");  
	 $master_unit_packing=$this->master_unit_packing->get_all();
	 $jumlah = $this->input->post('jumlah');
 error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');


// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator('$nama')
							 ->setLastModifiedBy('$nama')
							 ->setTitle("PHPExcel Master Barang WIP Document")
							 ->setSubject("PHPExcel Master Barang WIP Document")
							 ->setDescription("Test Master Barang WIP for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Master Barang WIP result file");

try {
	 $a   = 2;
	 $col = 0;
     $bar = 2;
     $col2= 7;
	
// Add some data
echo date('H:i:s') , " Add some data" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('A')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('B')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('C')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('D')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('E')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('F')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('G')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('H')->setRowHeight(1);
				 
					
// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0);
 for($i=0;$i<=$jumlah;$i++){
	 
$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'DATA MASTER UNIT PACKING');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				$a++;
				$bar++;
				
$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'CV. DUTA SETIA GARMEN ');
 $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $a++;
				  
$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'NO');
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('B'.$a, 'NAMA UNIT packing');
				  $objPHPExcel->getActiveSheet()->getStyle('B'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  
				   $objPHPExcel->getActiveSheet()->setCellValue('C'.$a, 'LOKASI');
				  $objPHPExcel->getActiveSheet()->getStyle('C'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				 $objPHPExcel->getActiveSheet()->setCellValue('D'.$a, 'NAMA PERUSAHAAN');
				  $objPHPExcel->getActiveSheet()->getStyle('D'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('E'.$a, 'NAMA PENANGGUNG JAWAB');
				  $objPHPExcel->getActiveSheet()->getStyle('E'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				   $objPHPExcel->getActiveSheet()->setCellValue('F'.$a, 'NAMA ADMINISTRATOR');
				  $objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  
				   $objPHPExcel->getActiveSheet()->setCellValue('G'.$a, 'TANGGAL INPUT');
				  $objPHPExcel->getActiveSheet()->getStyle('G'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('H'.$a, 'TANGGAL UPDATE');
				  $objPHPExcel->getActiveSheet()->getStyle('H'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
	
	
}				  
	$no=0;	
	$a++;		  
foreach ($master_unit_packing as $row){
	 $no++;
	 
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$a, $no, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' =>PHPExcel_Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$a, $row->nama_unit_packing, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('B'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' =>PHPExcel_Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    
				    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$a, $row->lokasi, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('C'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(  
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				     $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$a, $row->nama_perusahaan, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('D'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' =>PHPExcel_Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$a, $row->nama_penanggung_jawab, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('E'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' =>PHPExcel_Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    
				    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$a, $row->nama_administrator, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array( 
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$a, $row->created_at, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('G'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' =>PHPExcel_Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    
				    $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$a, $row->updated_at, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('H'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array( 
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    $a++;
}


    
// Save Excel 2007 file
echo date('H:i:s') , " Write to Excel2007 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('excel/master_unit_packing_'.$nama.'_'.$today.'.xlsx');
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , 'excel/master_unit_packing_'.$nama.'_'.$today.'.xlsx' , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Save Excel 95 file
echo date('H:i:s') , " Write to Excel5 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('excel/master_unit_packing_'.$nama.'_'.$today.'.xls');
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

	
		
echo date('H:i:s') , " File written to " , 'excel/master_unit_packing_'.$nama.'_'.$today.'.xlsx' , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing files" , EOL;
echo 'Files have been created in ' , base_url('excel/') , EOL;

$this->session->set_flashdata('pesan', 'Data berhasil diexport. Kembali ke halaman '. anchor('master_unit_packing/master_unit_packing/view', 'View Master Unit Packing.', 'class="alert-link"'). 
'  Atau melihat data yang diexport. '. anchor(base_url('excel'), 'View Export Excel.', 'class="alert-link"')
);
          
 redirect('master_unit_packing/master_unit_packing/sukses');

} 
		catch (Exception $e) {
			$this->session->set_flashdata('pesan_error', 'Maaf, kami mengalami kendala teknis. Kembali ke halaman ' . anchor('master_unit_packing/master_unit_packing', 'master_unit_packing.', 'class="alert-link"'));
            redirect('master_unit_packing/master_unit_packing/error');
   }

     }
     
}

