<?php
class makloon_baju_gudang_jadi_wip extends CI_Controller
{
    public $data = array(
        'halaman' => 'makloon_baju_gudang_jadi_wip',        
        'title' => 'Makloon Baju Gudang Jadi Wip',
        'isi' => 'makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip_model', 'makloon_baju_gudang_jadi_wip');
    }

   
    public function index()
    {
		$no=$this->input->post('no');
		
        $this->data['values'] = (object) $this->makloon_baju_gudang_jadi_wip->default_values;
        $this->data['unit_jahit'] =  $this->makloon_baju_gudang_jadi_wip->get_unit_jahit();
        $this->data['unit_packing'] =  $this->makloon_baju_gudang_jadi_wip->get_unit_packing();
        $this->data['gudang'] =  $this->makloon_baju_gudang_jadi_wip->get_gudang();
        $this->data['jenis_barang_bb'] =  $this->makloon_baju_gudang_jadi_wip->get_all_barang_bb();
        $this->data['no'] = $no;
		$this->load->view('template', $this->data);
			
    }
     public function sukses_input()
    {	
		
        $this->data['isi'] = 'makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip-sukses';
        $this->load->view('template', $this->data);
    }

    // Jika pendaftaran error, tampilkan informasi mengenai error.
    public function error_input()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Penginputan Makloon Baju Gudang Jadi Wip Error';
        $this->load->view('template', $this->data);
    }
   public function submit()
    {

	
		$no=$this->input->post('no',TRUE);
	
	   $id = $this->makloon_baju_gudang_jadi_wip->input_header(
       $this->input->post('no_sj',TRUE),
       $this->input->post('jenis_masuk',TRUE),
       $this->input->post('unit_jahit',TRUE),
       $this->input->post('unit_packing',TRUE),
       $this->input->post('gudang',TRUE),
       $this->input->post('keterangan_header',TRUE),
        $this->input->post('tanggal_sj',TRUE)
       );
   
      
       foreach ($id as $id_makloon_baju_gudang_jadi_wip){
		 $jumlah_input=$no-1;
		 for($i=1; $i<=$jumlah_input; $i++){
		 $this->makloon_baju_gudang_jadi_wip->input_detail(
		 $id_makloon_baju_gudang_jadi_wip->id,	
		 $this->input->post('id_barang_wip_'.$i,TRUE),
		 $this->input->post('id_barang_bb_'.$i,TRUE),
		 $this->input->post('qty_'.$i,TRUE),
		 $this->input->post('keterangan_detail_'.$i,TRUE),
		 $this->input->post('gudang',TRUE),
		 $this->input->post('unit_jahit',TRUE),
       $this->input->post('unit_packing',TRUE)
		);
	}
		}
		if(true)
		redirect('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/view');
		else 
		redirect('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/error_input');		
    }

    
   public function view($offset= null)
    {	
		
		$page = $this->uri->segment(4);
		$per_page=10;
		
		if (empty($page)) {
		$offset = 0;
		} else {
		$offset = ($page * $per_page - $per_page);
		}
		
       $makloon_baju_gudang_jadi_wip = $this->makloon_baju_gudang_jadi_wip->get_all_inner_paged($offset);
        if ($makloon_baju_gudang_jadi_wip) {
            $this->data['makloon_baju_gudang_jadi_wip'] = $makloon_baju_gudang_jadi_wip;
            $this->data['paging'] = $this->makloon_baju_gudang_jadi_wip->paging_barang('biasa', site_url('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/halaman/'), 4);
        } else {
            $this->data['makloon_baju_gudang_jadi_wip'] = 'Tidak ada data Makloon Baju Gudang Jadi Wip, Silahkan Melakukan '.anchor('/makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/', 'Proses penginputan.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/cari');
        $this->data['isi'] = 'makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip_list';
        $this->load->view('template', $this->data);
    }
    public function cari($offset = 0)
    {
        $makloon_baju_gudang_jadi_wip = $this->makloon_baju_gudang_jadi_wip->cari($offset);
        if ($makloon_baju_gudang_jadi_wip) {
            $this->data['makloon_baju_gudang_jadi_wip'] = $makloon_baju_gudang_jadi_wip;
            $this->data['paging'] = $this->makloon_baju_gudang_jadi_wip->paging('pencarian', site_url('/makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/cari/'), 4);
        } else {
            $this->data['makloon_baju_gudang_jadi_wip'] = 'Data tidak ditemukan.'. anchor('/makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/view', ' Tampilkan semua Makloon Baju Gudang Jadi Wip.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('/makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/cari');
        $this->data['isi'] = 'makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip_list';
        $this->load->view('template', $this->data);
    }
    
   
    
     public function hapus($id,$id_gudang,$id_unit_jahit,$id_unit_packing)
    {
		
      
        if ($this->session->userdata('user_bagian') != '2') {
            $this->session->set_flashdata('pesan_error', 'Anda tidak berhak menghapus data Makloon Baju Gudang Jadi Wip. Kembali ke halaman ' . anchor('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip', 'makloon_baju_gudang_jadi_wip.', 'class="alert-link"'));
            redirect('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/error');
        }

      
        if (! $this->makloon_baju_gudang_jadi_wip->get($id)) {
            $this->session->set_flashdata('pesan_error', 'Data Makloon Baju Gudang Jadi Wip tidak ada. Kembali ke halaman ' . anchor('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip', 'makloon_baju_gudang_jadi_wip.', 'class="alert-link"'));
            redirect('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/error');
        }

        // Hapus
        if ($this->makloon_baju_gudang_jadi_wip->delete_aktif($id,$id_gudang,$id_unit_jahit,$id_unit_packing)) {
            $this->session->set_flashdata('pesan', 'Data berhasil dihapus. Kembali ke halaman '. anchor('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/view', 'View Makloon Baju Gudang Jadi Wip.', 'class="alert-link"'));
            redirect('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/sukses');
        } else {
			
            $this->session->set_flashdata('pesan_error', 'Data gagal dihapus. Kembali ke halaman '. anchor('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/view', 'View Makloon Baju Gudang Jadi Wip.', 'class="alert-link"'));
            redirect('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/error');
        }
    
    }
     public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }
    
    public function edit()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $makloon_baju_gudang_jadi_wip = $this->makloon_baju_gudang_jadi_wip->getAllDetail($id);
        if (! $makloon_baju_gudang_jadi_wip) {
            $this->session->set_flashdata('pesan_error', 'Data Makloon Baju Gudang Jadi Wip tidak ada. Kembali ke halaman ' . anchor('makloon_baju_gudang_jadi_wip/master-barang/view', 'Makloon Baju Gudang Jadi Wip.', 'class="alert-link"'));
            redirect('makloon_baju_gudang_jadi_wip/master-barang/error');
        }
		$this->data['list_unit_jahit'] = $this->makloon_baju_gudang_jadi_wip->get_unit_jahit(); 
		$this->data['list_unit_packing'] = $this->makloon_baju_gudang_jadi_wip->get_unit_packing(); 
		$this->data['list_gudang'] = $this->makloon_baju_gudang_jadi_wip->get_gudang(); 
        $this->data['values'] = $makloon_baju_gudang_jadi_wip;
		$this->data['isi'] = 'makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip_form_edit';
        $this->load->view('template', $this->data);
	 }
	    public function updatedata(){
			$id =  $this->input->post('id',TRUE);
	        $no_sj =  $this->input->post('no_sj',TRUE);
			$tanggal_sj =  $this->input->post('tanggal_sj',TRUE);
			$jenis_masuk =  $this->input->post('jenis_masuk',TRUE);
			$id_unit_jahit =  $this->input->post('id_unit_jahit',TRUE);
			$id_unit_packing =  $this->input->post('id_unit_packing',TRUE);
			$id_gudang =  $this->input->post('id_gudang',TRUE);
			$keterangan_header =  $this->input->post('keterangan_header',TRUE);
			
			$jum_data_detail =  $this->input->post('jum_data_detail',TRUE);
			$jum_data_motif =  $this->input->post('jum_data_motif',TRUE);			
	  
		
		$update_header = $this->makloon_baju_gudang_jadi_wip->update_header($id,$no_sj,$tanggal_sj,$jenis_masuk,
		$id_unit_jahit,$id_unit_packing,$id_gudang,$keterangan_header);
		
		foreach ($update_header as $row){
    	 for($i=1;$i<=$jum_data_detail-1;$i++){	
    	 $this->makloon_baju_gudang_jadi_wip->update_detail(
    	$id_gudang, $row->id,$id_unit_packing,
    	 $this->input->post('id_barang_wip_'.$i,TRUE),
    	 $this->input->post('qty_'.$i,TRUE),
    	 $this->input->post('qty_lama_'.$i,TRUE),
    	 $this->input->post('keterangan_detail_'.$i,TRUE),
    	 $this->input->post('id_detail_'.$i,TRUE)
    	);
			$this->makloon_baju_gudang_jadi_wip->update_detail_motif($this->input->post('id_detail_motif_'.$i,TRUE),
			 $this->input->post('qty_'.$i,TRUE), $this->input->post('qty_lama_'.$i,TRUE),$id_unit_jahit,$id_unit_packing, 
			 $this->input->post('id_barang_bb_'.$i,TRUE),$this->input->post('id_detail_'.$i,TRUE),$jum_data_detail);
	 }
	 
		}
		if(true){
		$this->session->set_flashdata('pesan', 'Data berhasil diupdate. Kembali ke halaman ' . anchor('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/view', 'Makloon Baju Gudang Jadi WIP.', 'class="alert-link"'));
		redirect('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/view');
		}
		else {
		$this->session->set_flashdata('pesan_error', 'Data tidak berhasil diupdate. Kembali ke halaman ' . anchor('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/view', 'Makloon Baju Gudang Jadi WIP.', 'class="alert-link"'));
		redirect('makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/error_input');
	}
}

     
   
     
}

