<?php
class stokopname_unit extends CI_Controller
{
    public $data = array(
        'halaman' => 'stokopname_unit',        
        'title' => 'Stock opname',
        'isi' => 'stokopname_unit/stokopname_unit_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('stokopname_unit/stokopname_unit_model', 'stokopname_unit');
    }

   
    public function index()
    {
		$no=$this->input->post('no');
		
        $this->data['values'] = (object) $this->stokopname_unit->default_values;
        $this->data['unit_jahit'] =  $this->stokopname_unit->get_unit_jahit();
        $this->data['unit_packing'] =  $this->stokopname_unit->get_unit_packing();
        $this->data['gudang'] =  $this->stokopname_unit->get_gudang();
        $this->data['no'] = $no;
		$this->load->view('template', $this->data);
			
    }
     public function view()
    {
		$periode_so= $this->input->post('periode_so',TRUE);
		$experiode=explode('-',$periode_so);
		$tahun=$experiode[0];
		$bulan=$experiode[1];
		$unit_jahit= $this->input->post('unit_jahit',TRUE);
		$unit_packing= $this->input->post('unit_packing',TRUE);
		$gudang= $this->input->post('gudang',TRUE);
		$exgudang =explode(':',$gudang);
		$id_gudang=$exgudang[0];
		$id_jenis_so=$exgudang[1];
		
		$cek_so=$this->stokopname_unit->cek_data($unit_jahit,$unit_packing,$id_gudang,$periode_so,$tahun,$bulan,$id_jenis_so);
		
		if($cek_so==TRUE){
			$this->data['query']=$this->stokopname_unit->editAll($unit_jahit,$unit_packing,$id_gudang,$periode_so,$tahun,$bulan,$id_jenis_so);	
			$query=$this->data['query'];
			
			if($this->data['query'] == null){ 
				$this->data['query'] = 'Belum ada Proses Transaksi1 , atau silahkan mencoba melakukan pencarian lagi '.anchor('/stokopname_unit/stokopname_unit/', 'S.O.', 'class="alert-link"');
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_list';
			}	
			 elseif($query[0]['id_unit_jahit'] != 0 && $query[0]['id_unit_packing'] == 0 && $query[0]['id_gudang'] == 0){ 
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_edit';
			}
			elseif($query[0]['id_unit_jahit'] == 0 && $query[0]['id_unit_packing'] != 0 && $query[0]['id_gudang'] == 0){ 
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_edit_packing';
			}
			elseif($query[0]['id_unit_jahit'] == 0 && $query[0]['id_unit_packing'] == 0 && $query[0]['id_gudang'] != 0 && $id_jenis_so==1){ 
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_edit_gudang';
			}
			elseif($query[0]['id_unit_jahit'] == 0 && $query[0]['id_unit_packing'] == 0 && $query[0]['id_gudang'] != 0 && $id_jenis_so==2){ 
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_edit_gudang_bb';
			}
			
		}elseif($cek_so==FALSE){
			 $this->data['query']=$this->stokopname_unit->getAll($unit_jahit,$unit_packing,$id_gudang,$periode_so,$id_jenis_so);
			 $query=$this->data['query'];
			if($this->data['query'] == null){ 
				 $this->data['query'] = 'Belum ada Proses Transaksi , atau silahkan mencoba melakukan pencarian lagi '.anchor('/stokopname_unit/stokopname_unit/', 'S.O.', 'class="alert-link"');
			$this->data['isi'] = 'stokopname_unit/stokopname_unit_list';
			}	
			 elseif($query[0]['id_unit_jahit'] != 0 && $query[0]['id_unit_packing'] == 0 && $query[0]['id_gudang'] == 0){ 
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_list';
			}
			elseif($query[0]['id_unit_jahit'] == 0 && $query[0]['id_unit_packing'] != 0 && $query[0]['id_gudang'] == 0){ 
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_list_packing';
			}
			elseif($query[0]['id_unit_jahit'] == 0 && $query[0]['id_unit_packing'] == 0 && $query[0]['id_gudang'] != 0){ 
				if($query[0]['id_jenis_so'] ==1){
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_list_gudang';
				}
				elseif($query[0]['id_jenis_so'] ==2){
				$this->data['isi'] = 'stokopname_unit/stokopname_unit_list_gudang_bb';
				}
			}
		}
		$this->data['bulan']=$bulan;
		$this->data['tahun']=$tahun;
		$this->data['values'] = (object) $this->stokopname_unit->default_values;	
		
		$this->load->view('template', $this->data);		
    }
    
     public function submit()
    {
		$id_barang_bb	= $this->input->post('id_barang_bb',TRUE);
		$kode_barang_bb = $this->input->post('kode_barang_bb',TRUE);
		$id_barang_bb_1	= $this->input->post('id_barang_bb_1',TRUE);
		$id_barang_wip	= $this->input->post('id_barang_wip',TRUE);
		$id_barang_wip_1	= $this->input->post('id_barang_wip_1',TRUE);
		$kode_barang_bb_1 = $this->input->post('kode_barang_bb_1',TRUE);
		$qty			= $this->input->post('qty',TRUE);
		$qty_1			= $this->input->post('qty_1',TRUE);
		$jenis_hitung	= $this->input->post('jenis_hitung',TRUE);
		$tanggal_so	= $this->input->post('tanggal_so',TRUE);
		$bulan	= $this->input->post('bulan',TRUE);
		$tahun	= $this->input->post('tahun',TRUE);
		$id_gudang	= $this->input->post('id_gudang',TRUE);
		$id_jenis_so = $this->input->post('id_jenis_so',TRUE);
		$id_unit_jahit	= $this->input->post('id_unit_jahit',TRUE);
		$id_unit_packing	= $this->input->post('id_unit_packing',TRUE);
		$this->stokopname_unit->submitAll($id_barang_bb,$qty,$jenis_hitung,$tanggal_so,$bulan,$tahun,$id_unit_jahit,
		$id_barang_bb_1,$qty_1,$id_unit_packing,$id_barang_wip,$id_barang_wip_1,$id_gudang,$kode_barang_bb,$kode_barang_bb_1,$id_jenis_so);	
		
		if(true)
		//redirect('stokopname_unit/makloon_jadi_gudang_jadi_wip/sukses_input');
		redirect('stokopname_unit/stokopname_unit/list_all');
		else 
		redirect('stokopname_unit/stokopname_unit/error_input');
    }
    
    public function list_all($offset= null)
    {	

       $stokopname_unit = $this->stokopname_unit->get_all_paged_custom($offset);
        if ($stokopname_unit) {
            $this->data['stokopname_unit'] = $stokopname_unit;
          //  $this->data['paging'] = $this->stokopname_unit->paging_custom('biasa', site_url('stokopname_unit/stokopname_unit/halaman/'), 4);
        } else {
            $this->data['stokopname_unit'] = 'Tidak ada data Stok Opname, Silahkan Melakukan '.anchor('/stokopname_unit/stokopname_unit/', 'Proses penginputan.', 'class="alert-link"');
        }
       
        $this->data['form_action'] = site_url('stokopname_unit/stokopname_unit/cari');
        $this->data['isi'] = 'stokopname_unit/stokopname_unit_list_all';
        $this->load->view('template', $this->data);
   
    }
    
     
    public function cari($offset= null)
    {	

       $stokopname_unit = $this->stokopname_unit->get_all_paged_cari($offset);
        if ($stokopname_unit) {
            $this->data['stokopname_unit'] = $stokopname_unit;
          //  $this->data['paging'] = $this->stokopname_unit->paging_custom('biasa', site_url('stokopname_unit/stokopname_unit/halaman/'), 4);
        } else {
            $this->data['stokopname_unit'] = 'Tidak ada data Stok Opname, Silahkan Melakukan '.anchor('/stokopname_unit/stokopname_unit/', 'Proses penginputan.', 'class="alert-link"');
        }
       
        $this->data['form_action'] = site_url('stokopname_unit/stokopname_unit/cari');
        $this->data['isi'] = 'stokopname_unit/stokopname_unit_list_all';
        $this->load->view('template', $this->data);
   
    }
    public function hapus($id,$id_gudang_wip,$id_gudang_bb,$id_unit_jahit,$id_unit_packing)
    {
		
		$this->stokopname_unit->hapus($id,$id_gudang_wip,$id_gudang_bb,$id_unit_jahit,$id_unit_packing);	
		
		if(true)
		redirect('stokopname_unit/stokopname_unit/list_all');
		
    }
      public function update_data()
    {
		$id_detail_so	= $this->input->post('id_detail_so',TRUE);
		$id_barang_bb	= $this->input->post('id_barang_bb',TRUE);
		$kode_barang_bb	= $this->input->post('kode_barang_bb',TRUE);
		$id_barang_bb_1	= $this->input->post('id_barang_bb_1',TRUE);
		$kode_barang_bb_1	= $this->input->post('kode_barang_bb_1',TRUE);
		$id_barang_wip	= $this->input->post('id_barang_wip',TRUE);
		$kode_barang_wip	= $this->input->post('kode_barang_wip',TRUE);
		$id_barang_wip_1	= $this->input->post('id_barang_wip_1',TRUE);
		$kode_barang_wip_1	= $this->input->post('kode_barang_wip_1',TRUE);
		$qty			= $this->input->post('qty',TRUE);
		$qty_1			= $this->input->post('qty_1',TRUE);
		$id_so	= $this->input->post('id_so',TRUE);
		$jenis_hitung	= $this->input->post('jenis_hitung',TRUE);
		$tanggal_so	= $this->input->post('tanggal_so',TRUE);
		
		$id_gudang	= $this->input->post('id_gudang',TRUE);
		$id_unit_jahit	= $this->input->post('id_unit_jahit',TRUE);
		$id_unit_packing	= $this->input->post('id_unit_packing',TRUE);
		$this->stokopname_unit->updateAll($id_so,$id_barang_bb,$kode_barang_bb,$qty,
		$jenis_hitung,$tanggal_so,$id_unit_jahit,
		$id_barang_bb_1,$qty_1,$id_unit_packing,$id_barang_wip,$id_barang_wip_1,
		$id_gudang,$id_detail_so,$kode_barang_wip,$kode_barang_wip_1,$kode_barang_bb_1);	
		
		if(true)
		//redirect('stokopname_unit/makloon_jadi_gudang_jadi_wip/sukses_input');
		redirect('stokopname_unit/stokopname_unit/list_all');
		else 
		redirect('stokopname_unit/stokopname_unit/error_input');
    }
    
    public function edit($id,$id_gudang_wip,$id_gudang_bb,$id_unit_jahit,$id_unit_packing)
    {
		
		$this->data['query']=$this->stokopname_unit->edit_so($id,$id_gudang_bb,$id_gudang_wip,$id_unit_jahit,$id_unit_packing);	
		if($id_unit_jahit != 0){
		$this->data['isi'] = 'stokopname_unit/stokopname_unit_edit';
		}
		elseif($id_unit_packing != 0){
		$this->data['isi'] = 'stokopname_unit/stokopname_unit_edit_packing';
		}
		elseif($id_gudang_bb != 0){
		$this->data['isi'] = 'stokopname_unit/stokopname_unit_edit_gudang_bb';
		}
		elseif($id_gudang_wip != 0){
		$this->data['isi'] = 'stokopname_unit/stokopname_unit_edit_gudang_wip';
		}
		$this->data['values'] = (object) $this->stokopname_unit->default_values;	
		$this->load->view('template', $this->data);		
    }
}

