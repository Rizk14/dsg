<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-jual/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$data['isi'] = 'faktur-jual/vmainform';
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['msg'] = '';
	$this->load->view('template',$data);

  }
  
  function edit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id_faktur 	= $this->uri->segment(4);	
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$csupplier 	= $this->uri->segment(7);
	$tgl_awal 	= $this->uri->segment(8);
	$tgl_akhir 	= $this->uri->segment(9);
	$carinya 	= $this->uri->segment(10);
	
	$data['query'] = $this->mmaster->get_faktur($id_faktur);
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['msg'] = '';
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['csupplier'] = $csupplier;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['isi'] = 'faktur-jual/veditform';
	$data['id_faktur'] = $id_faktur;
	$this->load->view('template',$data);

  }
  
  // updatedata
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_faktur 	= $this->input->post('id_faktur', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj_lama = $this->input->post('no_sj_lama', TRUE);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$supplier = $this->input->post('supplier', TRUE);  
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			
			$no_faktur_lama = $this->input->post('no_faktur_lama', TRUE);
			$jum = $this->input->post('jum', TRUE);
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;
									
			//$tgl = date("Y-m-d");			
			$tgl = date("Y-m-d H:i:s");
			
			if ($no_fp != $no_faktur_lama) {
				$cek_data = $this->mmaster->cek_data($no_fp, $supplier);
				if (count($cek_data) == 0) {
					$this->db->query(" UPDATE tm_pembelian_nofaktur SET kode_supplier = '$supplier', 
									jenis_pembelian = '$jenis_pembelian', no_faktur = '$no_fp', 
									tgl_faktur = '$tgl_fp', tgl_update= '$tgl', jumlah = '$jum' where id= '$id_faktur' "); 
									
					// reset dulu status_faktur menjadi FALSE --------------------------------
					$list_sj_lama = explode(",", $no_sj_lama);
					foreach($list_sj_lama as $row2) {
						$row2 = trim($row2);
						if ($row2 != '') {
							$this->db->query(" UPDATE tm_pembelian SET status_faktur = 'f', no_faktur = '' 
							WHERE kode_supplier = '$supplier' AND no_sj = '$row2' "); 
						}
					}
					//-------------------------------------------------------------------------
					
					$this->db->query(" DELETE FROM tm_pembelian_nofaktur_sj where id_pembelian_nofaktur= '$id_faktur' ");
					
					// insert tabel detail sj-nya
					$list_sj = explode(",", $no_sj); 
					foreach($list_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') { //echo $row1."<br>";
							$data_detail = array(
							  'id_pembelian_nofaktur'=>$id_faktur,
							  'no_sj'=>$row1
							);
							$this->db->insert('tm_pembelian_nofaktur_sj',$data_detail);
							
							// update status_faktur dan jenis_pembelian di tabel tm_pembelian
							$this->db->query(" UPDATE tm_pembelian SET no_faktur = '$no_fp', status_faktur = 't',
									jenis_pembelian = '$jenis_pembelian'
								WHERE no_sj = '$row1' AND kode_supplier = '$supplier' ");
						}
					}
				
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "faktur-jual/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "faktur-jual/cform/cari/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('faktur-jual/cform/view');
				}
				else {
					$data['query'] = $this->mmaster->get_faktur($id_faktur);
					$data['list_supplier'] = $this->mmaster->get_supplier(); 
					$data['isi'] = 'faktur-jual/veditform';
					$data['id_faktur'] = $id_faktur;
					$data['msg'] = "Update gagal. Data no faktur ".$no_fp." untuk supplier ".$supplier." sudah ada..!";
					$this->load->view('template',$data);
				}
			}
			else { // jika sama
				$this->db->query(" UPDATE tm_pembelian_nofaktur SET kode_supplier = '$supplier', 
									jenis_pembelian = '$jenis_pembelian', no_faktur = '$no_fp', 
									tgl_faktur = '$tgl_fp', tgl_update= '$tgl', jumlah = '$jum' where id= '$id_faktur' "); 
				
					// reset dulu status_faktur menjadi FALSE --------------------------------
					$list_sj_lama = explode(",", $no_sj_lama);
					foreach($list_sj_lama as $row2) {
						$row2 = trim($row2);
						if ($row2 != '') {
							$this->db->query(" UPDATE tm_pembelian SET status_faktur = 'f', no_faktur = '' 
							WHERE kode_supplier = '$supplier' AND no_sj = '$row2' "); 
						}
					}
					//-------------------------------------------------------------------------
				
					$this->db->query(" DELETE FROM tm_pembelian_nofaktur_sj where id_pembelian_nofaktur= '$id_faktur' ");
					
					// insert tabel detail sj-nya
					$list_sj = explode(",", $no_sj); //print_r($list_sj); die();
					foreach($list_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') { //echo $row1."<br>";
							$data_detail = array(
							  'id_pembelian_nofaktur'=>$id_faktur,
							  'no_sj'=>$row1
							);
							$this->db->insert('tm_pembelian_nofaktur_sj',$data_detail);
							
							// update status_faktur di tabel tm_pembelian
							$this->db->query(" UPDATE tm_pembelian SET no_faktur = '$no_fp', status_faktur = 't' 
								WHERE no_sj = '$row1' AND kode_supplier = '$supplier' ");
						}
					} //die();
					
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "faktur-jual/cform/view/index/".$cur_page;
					else
						$url_redirectnya = "faktur-jual/cform/cari/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
					//redirect('faktur-jual/cform/view');
			}	
  }
  
  function show_popup_pembelian(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jnsaction = $this->uri->segment(4); 
	$csupplier = $this->uri->segment(5); 
	$no_fakturnya = $this->uri->segment(6); 
	
	if ($jnsaction == '')
		$jnsaction = $this->input->post('jnsaction', TRUE); 
	if ($csupplier == '')
		$csupplier = $this->input->post('csupplier', TRUE);  
	if ($no_fakturnya == '')
		$no_fakturnya = $this->input->post('no_fakturnya', TRUE);  
		
	if ($keywordcari == '' && ($csupplier == '' || $jnsaction == '' || $no_fakturnya == '') ) {
		$jnsaction 	= $this->uri->segment(4);
		$csupplier 	= $this->uri->segment(5);
		$no_fakturnya 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
	$jum_total = $this->mmaster->get_pembeliantanpalimit($jnsaction, $csupplier, $no_fakturnya, $keywordcari);
		/*		$config['base_url'] = base_url()."index.php/faktur-jual/cform/show_popup_pembelian/".$jnsaction."/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		*/
	//$data['query'] = $this->mmaster->get_pembelian($config['per_page'],$this->uri->segment(7), $jnsaction, $csupplier, $keywordcari);						
	$data['query'] = $this->mmaster->get_pembelian($jnsaction, $csupplier, $no_fakturnya, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$data['jnsaction'] = $jnsaction;
	$data['no_fakturnya'] = $no_fakturnya;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_supplier
				WHERE kode_supplier = '$csupplier' ");
				$hasilrow3 = $query3->row();
				$data['nama_supplier']	= $hasilrow3->nama;

	$this->load->view('faktur-jual/vpopuppemb',$data);

  }

  function submit(){
	//$this->load->library('form_validation');
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

			$supplier 	= $this->input->post('supplier', TRUE);
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			
			$jum 	= $this->input->post('jum', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj = trim($no_sj);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;						
			//$jum 	= $this->input->post('jum', TRUE);

			$cek_data = $this->mmaster->cek_data($no_fp, $supplier);
			if (count($cek_data) > 0) { 
				$data['isi'] = 'faktur-jual/vmainform';
				$data['list_supplier'] = $this->mmaster->get_supplier(); 
				$data['msg'] = "Data no faktur ".$no_fp." dari supplier ".$supplier." sudah ada..!";
				$this->load->view('template',$data);
			}
			else {
				$this->mmaster->save($no_fp, $tgl_fp, $supplier, $jenis_pembelian, $jum, $no_sj);
				redirect('faktur-jual/cform/view');
			}
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'faktur-jual/vformview';
    $keywordcari = "all";
    $csupplier = '0';
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/faktur-jual/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari, $date_from, $date_to);
				
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
		
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE);
	$date_to 	= $this->input->post('date_to', TRUE);
	
	if ($csupplier == '')
		$csupplier 	= $this->uri->segment(4);
	if ($date_from == '')
		$date_from = $this->uri->segment(5);
	if ($date_to == '')
		$date_to = $this->uri->segment(6);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(7);
	
	/*if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	} */
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($csupplier == '')
		$csupplier = '0';
			
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/faktur-jual/cform/cari/'.$csupplier.'/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(8), $csupplier, $keywordcari, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'faktur-jual/vformview';
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $csupplier 	= $this->uri->segment(7);
    $tgl_awal 	= $this->uri->segment(8);
    $tgl_akhir 	= $this->uri->segment(9);
    $carinya 	= $this->uri->segment(10);
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "faktur-jual/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "faktur-jual/cform/cari/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('faktur-jual/cform/view');
  }
}
