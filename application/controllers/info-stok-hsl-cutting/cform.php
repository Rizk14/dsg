<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-stok-hsl-cutting/mmaster');
  }
    
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-stok-hsl-cutting/vformview';
    $cstatus = $this->input->post('statusnya', TRUE);
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' && $cstatus == '' ) {
		$cstatus 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($cstatus == '')
		$cstatus = 't';
	
    $jum_total = $this->mmaster->get_stoktanpalimit($cstatus, $keywordcari);

			$config['base_url'] = base_url().'index.php/info-stok-hsl-cutting/cform/view/index/'.$cstatus.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mmaster->get_stok($config['per_page'],$this->uri->segment(7), $cstatus, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }

}
