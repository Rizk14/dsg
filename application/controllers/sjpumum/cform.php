<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('sjpumum/mmaster');
  }

  function add(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'sjpumum/vmainform';
	$data['msg'] = '';

    // area
    $data['list_area'] = $this->mmaster->get_list_area();

	$this->load->view('template',$data);

  }

  function edit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

    $id = $this->input->get('id');

    if ($id == null) {
        redirect('sjpumum/cform/index');
    }    

	$data['isi'] = 'sjpumum/veditform';
	$data['msg'] = '';

    // area
    $data['list_area'] = $this->mmaster->get_list_area();
    $data['header'] = $this->mmaster->get_data_edit_header($id)->row();
    $data['detail'] = $this->mmaster->get_data_edit_detail($id)->result();

	$this->load->view('template',$data);

  }  

  function update(){
    $is_logged_in = $this->session->userdata('is_logged_in');
    if (!isset($is_logged_in) || $is_logged_in!= true) {
        //$this->load->view('loginform', $data);
        redirect('loginform');
    }

    $id = $this->input->post('id');
    if ($id == null) {
        die('Error ID');
    }

    $i_sjp = $this->input->post('i_sjp');
    $i_area = $this->input->post('i_area');
    $d_sjp = date('Y-m-d', strtotime($this->input->post('d_sjp')));
    $e_sender = $this->input->post('e_sender');        
    $e_sender_company = $this->input->post('e_sender_company');
    $e_recipient = $this->input->post('e_recepient');
    $e_recipient_company = $this->input->post('e_recepient_company');
    $e_remark_header = $this->input->post('e_remark_header');

    $uid = $this->session->userdata('uid');
    $uid_approve = $this->mmaster->get_uid_sjp_approve($uid);

    /** update header */
    $this->mmaster->update_header($id, $i_sjp, $i_area, $d_sjp, $e_sender, $e_sender_company, $e_recipient, $e_recipient_company, $e_remark_header, $uid, $uid_approve);

    $this->mmaster->delete_detail($id);

    $count_detail = $this->input->post('no');
    $no_urut_detail = 1;
    for ($i=1; $i<=$count_detail; $i++) {
        if ($this->input->post("kode_$i") == null) {
            continue;
        }
        
        $i_product = $this->input->post("kode_$i");
        $e_product_name = $this->input->post("nama_$i");
        $n_quantity = $this->input->post("qty_$i");
        $n_satuan = $this->input->post("satuan_$i");
        $remark = $this->input->post("remark_$i");
        
        $this->mmaster->insert_detail($id, $i_product, $e_product_name, $n_quantity, $n_satuan, $remark, $no_urut_detail);

        $no_urut_detail++;
    }

    redirect('sjpumum/cform/index');
}

  function submit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
            //$this->load->view('loginform', $data);
            redirect('loginform');
		}

        $i_sjp = $this->input->post('i_sjp');
        $i_area = $this->input->post('i_area');
        $d_sjp = date('Y-m-d', strtotime($this->input->post('d_sjp')));
        $e_sender = $this->input->post('e_sender');        
        $e_sender_company = $this->input->post('e_sender_company');
        $e_recipient = $this->input->post('e_recepient');
        $e_recipient_company = $this->input->post('e_recepient_company');
        $e_remark_header = $this->input->post('e_remark_header');

        $uid = $this->session->userdata('uid');
        $uid_approve = $this->mmaster->get_uid_sjp_approve($uid);

        /** insert header */
        // $return_id = $this->mmaster->insert_header($i_sjp, $i_area, $d_sjp, $e_sender, $e_sender_company, $e_recipient, $e_recipient_company);
        $this->mmaster->insert_header($i_sjp, $i_area, $d_sjp, $e_sender, $e_sender_company, $e_recipient, $e_recipient_company, $e_remark_header, $uid, $uid_approve);
        $id_sjp = $this->db->insert_id();

        $count_detail = $this->input->post('no');
        $no_urut_detail = 1;
        for ($i=1; $i<=$count_detail; $i++) {
            if ($this->input->post("kode_$i") == null) {
                continue;
            }
            
            $i_product = $this->input->post("kode_$i");
            $e_product_name = $this->input->post("nama_$i");
            $n_quantity = $this->input->post("qty_$i");
            $n_satuan = $this->input->post("satuan_$i");
            $remark = $this->input->post("remark_$i");
            
            $this->mmaster->insert_detail($id_sjp, $i_product, $e_product_name, $n_quantity, $n_satuan, $remark, $no_urut_detail);

            $no_urut_detail++;
        }

        redirect('sjpumum/cform/index');
  }
  
  function index(){
    $is_logged_in = $this->session->userdata('is_logged_in');
    if (!isset($is_logged_in) || $is_logged_in!= true) {
        //$this->load->view('loginform', $data);
        redirect('loginform');
    }    
    	
    $data['isi'] = 'sjpumum/vformindex';

    $date_from = date('Y-m-01');
    $date_to = date('Y-m-d');
    $cari = '';

    if ($this->input->post('date_from') != null and $this->input->post('date_from') != '') {
        $_date = $this->input->post('date_from');
        $date_from = date('Y-m-d', strtotime($_date));
    }

    if ($this->input->post('date_to') != null and $this->input->post('date_to') != '') {
        $_date = $this->input->post('date_to');
        $date_to = date('Y-m-d', strtotime($_date));
    }

    if ($this->input->post('cari') != null and $this->input->post('cari') != '') {
        $cari = $this->input->post('cari');
    }

    $limit = 10;
    $offset = $this->uri->segment(5);

    $query = $this->mmaster->get_data_index($limit, $offset, $date_from, $date_to, $cari);

    $config = [];
    $config['base_url'] = base_url().'index.php/sjpumum/cform/index/';
    //$config['total_rows'] = $query->num_rows(); 
    $config['total_rows'] = $query->num_rows(); 
    $config['per_page'] = $limit;
    $config['first_link'] = 'Awal';
    $config['last_link'] = 'Akhir';
    $config['next_link'] = 'Selanjutnya';
    $config['prev_link'] = 'Sebelumnya';
    $config['cur_page'] = $this->uri->segment(5);
    $this->pagination->initialize($config);
	
    // $data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $date_from, $date_to, $cgudang, $ctujuan, $caribrg, $filterbrg);	
   
    $data['query'] = $query;
    $data['jum_total'] = $query->num_rows();

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['cari'] = $cari;
	
	$this->load->view('template',$data);
  }  
  
  function delete(){
    $is_logged_in = $this->session->userdata('is_logged_in');
    if (!isset($is_logged_in) || $is_logged_in!= true) {
        //$this->load->view('loginform', $data);
        redirect('loginform');
    }

    $id = $this->input->get('id');

    if ($id == null) {
        die('Error ID');
    }   
    
    $this->mmaster->delete($id);
    
    redirect('sjpumum/cform/index');
  }  

  function generate_nomor_dokumen()
  {
        $date = $this->input->post('date');
        $i_area = $this->input->post('i_area');

        /** reformat date */
        $date = date('Y-m-d', strtotime($date));
        
        $i_document = $this->mmaster->generate_nomor_dokumen($date, $i_area);

        echo $i_document;
  }

  function printsjp()
  {
    $id = $this->input->get('id');

    if ($id == null) {
        redirect('sjpumum/cform/index');
    }

    // jika sudah pernah cetak 
    $status_print = $this->mmaster->get_status_print($id);
    if ($status_print == 't') {
        $js = "<script>window.close()</script>";
        
        echo $js;
        return;
    }

    $data = [
        'isi' => $this->mmaster->get_data_edit_header($id)->row(),
        'detail' => $this->mmaster->get_data_edit_detail($id)->result()
    ];

    $this->load->view('sjpumum/vformprint', $data); 
  }

  function updatecetak() {
  	$id = $this->input->post('id');
  	$this->mmaster->increment_cetak($id);
  }

  function approve()
  {
    // die('approve');
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

    $id = $this->input->get('id');

    if ($id == null) {
        redirect('sjpumum/cform/index');
    }    

	$data['isi'] = 'sjpumum/vformapprove';
	$data['msg'] = '';

    // area
    $data['list_area'] = $this->mmaster->get_list_area();
    $data['header'] = $this->mmaster->get_data_edit_header($id)->row();
    $data['detail'] = $this->mmaster->get_data_edit_detail($id)->result();

	$this->load->view('template',$data);
  }

  function approve_submit()
  {
    // die('approve');
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

    $id = $this->input->post('id');

    if ($id == null) {
        redirect('sjpumum/cform/index');
    }   

    $approve = $this->input->post('approve');
    if (@$approve != null && $approve == 'approve') {
        $this->mmaster->approve($id);
        $data['msg'] = 'Approve successfull';
    }

    $reject = $this->input->post('reject');
    $e_remark_notapprove = $this->input->post('e_remark_notapprove');
    if ($reject != null and $e_remark_notapprove != '' ) {
        $this->mmaster->reject($id, $e_remark_notapprove);
        $data['msg'] = 'Document rejected';
    }	

    redirect('sjpumum/cform/index');
  }


  function accept()
  {
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

    $id = $this->input->get('id');

    if ($id == null) {
        redirect('sjpumum/cform/index');
    }    

	$data['isi'] = 'sjpumum/vformaccept';
	$data['msg'] = '';

    // area
    $data['list_area'] = $this->mmaster->get_list_area();
    $data['header'] = $this->mmaster->get_data_edit_header($id)->row();
    $data['detail'] = $this->mmaster->get_data_edit_detail($id)->result();

	$this->load->view('template',$data);
  }

  function accept_submit()
  {
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

    $id = $this->input->post('id');
    if ($id == null) {
        die('Error ID');
    }    

    $d_sjp_receive = $this->input->post('d_sjp_receive');

    $d_sjp_receive = date('Y-m-d', strtotime($d_sjp_receive));

    $e_receive = $this->input->post('e_receive'); 
    
    /**@params Array $items */
    $items = $this->input->post('items');

    /** update header */
    $this->mmaster->update_accept($id, $d_sjp_receive, $e_receive);

    foreach ($items as $item) {
        $id_detail = $item['id_detail'];
        $n_quantity_receive = $item['n_quantity_receive'];
        $this->mmaster->update_accept_item($id_detail, $n_quantity_receive);
    }

    redirect('sjpumum/cform/index');
  }

  function view()
  {
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

    $id = $this->input->get('id');

    if ($id == null) {
        redirect('sjpumum/cform/index');
    }    

	$data['isi'] = 'sjpumum/vformview';

    // area
    $data['list_area'] = $this->mmaster->get_list_area();
    $data['header'] = $this->mmaster->get_data_edit_header($id)->row();
    $data['detail'] = $this->mmaster->get_data_edit_detail($id)->result();

	$this->load->view('template',$data);
  }

}
