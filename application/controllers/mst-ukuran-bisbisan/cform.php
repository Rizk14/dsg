<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-ukuran-bisbisan/mmaster');
  }

  function index(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->id;
			$enama = $row->nama;
			$evar1 = $row->var1;
			$evar2 = $row->var2;
			$evar3 = $row->var3;
		}
	}
	else {
			$ekode = '';
			$enama = '';
			$evar1 = '';
			$evar2 = '';
			$evar3 = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['evar1'] = $evar1;
	$data['evar2'] = $evar2;
	$data['evar3'] = $evar3;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-ukuran-bisbisan/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		
			$id_ukuran 	= $this->input->post('id_ukuran', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			$var1 	= $this->input->post('var1', TRUE);
			$var2 	= $this->input->post('var2', TRUE);
			$var3 	= $this->input->post('var3', TRUE);
			
			if ($goedit == 1) {
				$this->mmaster->save($id_ukuran, $nama, $var1, $var2, $var3, $goedit);
			}
			else {
				$cek_data = $this->mmaster->cek_data($nama);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id_ukuran, $nama, $var1, $var2, $var3, $goedit);
				}
			}
			
			redirect('mst-ukuran-bisbisan/cform');
		
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-ukuran-bisbisan/cform');
  }
  
  
}
