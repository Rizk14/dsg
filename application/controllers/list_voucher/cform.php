<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('list_voucher/mmaster');
  }

function index(){
	 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']='list_voucher/vmainform';
		$data['area']=$this->mmaster->get_area();
		$data['bank']=$this->mmaster->get_bank();
		$this->load->view('template',$data);
	}
		function voucher()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}		
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$area	=$this->input->post('area', TRUE);
			$date_from=$this->input->post('date_from', TRUE);
			$date_to=$this->input->post('date_to', TRUE);
			$jenis=$this->input->post('jenis', TRUE);
			$icoabank=$this->input->post('i_coa_bank', TRUE);
			if($jenis==1){
				$nama_jenis="Kas Kecil";
				}
			elseif($jenis==2){
				$nama_jenis="Kas Besar Keluar";
				}
			elseif($jenis==3){
				$nama_jenis="Kas Besar Masuk";
				}
			elseif($jenis==4){
				$nama_jenis="Kas Bank Keluar";
				}	
			elseif($jenis==5){
				$nama_jenis="Kas Bank Masuk";
				}				
					
		if($area==''){
			$area=$this->uri->segment(4);
		}
		if($date_from==''){
			$date_from=$this->uri->segment(5);
		}
		if($date_to==''){
			$date_to=$this->uri->segment(6);
		}
		if($jenis==1){	
			$config['base_url'] = base_url().'index.php/list_voucher/cform/pv/'.$area.'/'.$date_from.'/'.$date_to.'/'.$cari.'/';
			if($cari==''){
			  $query = $this->db->query("	select distinct i_pv, v_pv from tm_pv
							                      where d_pv >='$date_from' AND d_pv <='$date_to' AND i_area='$area' and i_pv_type='00' ",false);
			}else{
				$query = $this->db->query("	select distinct i_pv, v_pv from tm_pv
							                      where i_area='$area' and d_pv >='$date_from' AND d_pv <='$date_to' and i_pv_type='00'
                                    and (upper(i_pv) like '%$cari%') ",false);
			}
			
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['area']	=$area;
			$data['date_from']=$date_from;
			$data['date_to']=$date_to;
			$data['jenis']=$jenis;
			$data['nama_jenis']=$nama_jenis;
			$data['isi']='list_voucher/vlistpv';
			$data['query']=$this->mmaster->bacapvprint1($cari,$area,$date_from,$date_to,$config['per_page'],$config['cur_page']);
			$data['total']=$config['total_rows'];
			$this->load->view('template', $data);
		}
			elseif($jenis==2){
				$config['base_url'] = base_url().'index.php/list_voucher/cform/pv/'.$area.'/'.$date_from.'/'.$date_to.'/'.$cari.'/';
			if($cari==''){
			  $query = $this->db->query("	select distinct i_pv, v_pv from tm_pv
							                      where d_pv >='$date_from' AND d_pv <='$date_to' AND i_area='$area' and i_pv_type='01'",false);
			}else{
				$query = $this->db->query("	select distinct i_pv, v_pv from tm_pv
							                      where i_area='$area' and d_pv >='$date_from' AND d_pv <='$date_to' and i_pv_type='01'
                                    and (upper(i_pv) like '%$cari%') ",false);
			}
			
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['area']	=$area;
			$data['date_from']=$date_from;
			$data['date_to']=$date_to;
			$data['jenis']=$jenis;
			$data['nama_jenis']=$nama_jenis;
			$data['isi']='list_voucher/vlistpv';
			$data['query']=$this->mmaster->bacapvprint2($cari,$area,$date_from,$date_to,$config['per_page'],$config['cur_page']);
			$data['total']=$config['total_rows'];
			$this->load->view('template', $data);
			}	
	elseif($jenis==3){
				$config['base_url'] = base_url().'index.php/list_voucher/cform/rv/'.$area.'/'.$date_from.'/'.$date_to.'/'.$cari.'/';
			if($cari==''){
			   $query = $this->db->query("	select i_rv, v_rv from tm_rv
							                      where i_area='$area' AND d_rv >='$date_from' AND d_rv <='$date_to'  and i_rv_type='01'",false);
      }else{
			  $query = $this->db->query("	select i_rv, v_rv from tm_rv
							                      where i_area='$area' AND d_rv >='$date_from' AND d_rv <='$date_to'  and i_rv_type='01'
                                    and (upper(i_rv) like '%$cari%') ",false);
      }
			
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['area']	=$area;
			$data['date_from']=$date_from;
			$data['date_to']=$date_to;
			$data['jenis']=$jenis;
			$data['nama_jenis']=$nama_jenis;
			$data['isi']='list_voucher/vlistrv';
			$data['query']=$this->mmaster->bacarvprint1($cari,$area,$date_from,$date_to,$config['per_page'],$config['cur_page']);
			$data['total']=$config['total_rows'];
			$this->load->view('template', $data);
			}	
		elseif($jenis==4){
				$config['base_url'] = base_url().'index.php/list_voucher/cform/pv/'.$area.'/'.$date_from.'/'.$date_to.'/'.$cari.'/';
			if($cari==''){
			   $query = $this->db->query("	select b.i_pvb as i_pv, a.v_pv from tm_pv a, tm_pvb b
							                      where a.i_area='$area' and d_pv >='$date_from' AND d_pv <='$date_to' and a.i_pv_type='02'
							                      and a.i_area=b.i_area and a.i_pv=b.i_pv and a.i_pv_type=b.i_pv_type and b.i_coa_bank='$icoabank'",false);
      }else{
			  $query = $this->db->query("	select b.i_pvb as i_pv, a.v_pv from tm_pv a, tm_pvb b
							                      where a.i_area='$area' and d_pv >='$date_from' AND d_pv <='$date_to' and a.i_pv_type='02'
							                      and a.i_area=b.i_area and a.i_pv=b.i_pv and a.i_pv_type=b.i_pv_type and b.i_coa_bank='$icoabank'
                                    and (upper(b.i_pvb) like '%$cari%') ",false);
      }
			
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['area']	=$area;
			$data['date_from']=$date_from;
			$data['date_to']=$date_to;
			$data['icoabank']=$icoabank;
			$data['jenis']=$jenis;
			$data['nama_jenis']=$nama_jenis;
			$data['isi']='list_voucher/vlistpvb';
			$data['query']=$this->mmaster->bacapvbprint($icoabank,$cari,$area,$date_from,$date_to,$config['per_page'],$config['cur_page']);
			$data['total']=$config['total_rows'];
			$this->load->view('template', $data);
		}	
			elseif($jenis==5){
				$config['base_url'] = base_url().'index.php/list_voucher/cform/rv/'.$area.'/'.$date_from.'/'.$date_to.'/'.$cari.'/';
			if($cari==''){
			   $query = $this->db->query("	select b.i_rvb as i_rv, a.v_rv from tm_rv a, tm_rvb b
							                      where a.i_area='$area' and d_rv >='$date_from' AND d_rv <='$date_to' and a.i_rv_type='02'
							                      and a.i_area=b.i_area and a.i_rv=b.i_rv and a.i_rv_type=b.i_rv_type and b.i_coa_bank='$icoabank'",false);
      }else{
			  $query = $this->db->query("	select b.i_rvb as i_rv, a.v_rv from tm_rv a, tm_rvb b
							                      where a.i_area='$area'and d_rv >='$date_from' AND d_rv <='$date_to' and a.i_rv_type='02'
							                      and a.i_area=b.i_area and a.i_rv=b.i_rv and a.i_rv_type=b.i_rv_type and b.i_coa_bank='$icoabank'
                                    and (upper(b.i_rvb) like '%$cari%') ",false);
      }
			
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['area']	=$area;
			$data['date_from']=$date_from;
			$data['date_to']=$date_to;
			$data['icoabank']=$icoabank;
			$data['jenis']=$jenis;
			$data['nama_jenis']=$nama_jenis;
			$data['isi']='list_voucher/vlistrvb';
			$data['query']=$this->mmaster->bacarvbprint($icoabank,$cari,$area,$date_from,$date_to,$config['per_page'],$config['cur_page']);
			$data['total']=$config['total_rows'];
			$this->load->view('template', $data);
				
				}	
	}
	function cetak()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			
			$ipv	  = $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$data['iarea']= $iarea;
			$data['ipv']  = $ipv;
			$data['perusahaan']=$this->mmaster->baca_persh();
			$data['query']=$this->mmaster->bacapv1($ipv,$iarea);
			$sess=$this->session->userdata('session_id');
			$sql	= "select * from pabrik_sessions where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
			}else{
			  $ip_address='kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$sql	= "select nama from tm_area where id='$iarea'";
			$rs		=  $this->db->query($sql);
				if($rs->num_rows>0){
				foreach($rs->result() as $tes){
			     $data['eareaname']=$tes->nama;
			  }
			}
			$pesan='Cetak PV Area:'.$iarea.' No:'.$ipv;
			$this->load->view('list_voucher/vformrpt', $data);
		
	}
	function cetak2()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			
			$ipv	  = $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$data['iarea']= $iarea;
			$data['ipv']  = $ipv;
			$data['perusahaan']=$this->mmaster->baca_persh();
			$data['query']=$this->mmaster->bacapv2($ipv,$iarea);
			$sess=$this->session->userdata('session_id');
			$sql	= "select * from pabrik_sessions where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
			}else{
			  $ip_address='kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$sql	= "select nama from tm_area where id='$iarea'";
			$rs		=  $this->db->query($sql);
				if($rs->num_rows>0){
				foreach($rs->result() as $tes){
			     $data['eareaname']=$tes->nama;
			  }
			}
			$pesan='Cetak PV Area:'.$iarea.' No:'.$ipv;
			$this->load->view('list_voucher/vformrpt2', $data);
		
	} 
	function cetak3()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			
			$irv	  = $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$data['iarea']= $iarea;
			$data['irv']  = $irv;
			$data['perusahaan']=$this->mmaster->baca_persh();
			$data['query']=$this->mmaster->bacarv1($irv,$iarea);
			$sess=$this->session->userdata('session_id');
			$sql	= "select * from pabrik_sessions where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
			}else{
			  $ip_address='kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$sql	= "select nama from tm_area where id='$iarea'";
			$rs		=  $this->db->query($sql);
				if($rs->num_rows>0){
				foreach($rs->result() as $tes){
			     $data['eareaname']=$tes->nama;
			  }
			}
			$pesan='Cetak RV Area:'.$iarea.' No:'.$irv;
			$this->load->view('list_voucher/vformrpt3', $data);
		
	} 
	function cetak4()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			
			$ipv	  = $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$icoabank=$this->input->post('icoabank', TRUE);
			if($icoabank=='') $icoabank = $this->uri->segment(6);
			$data['iarea']= $iarea;
			$data['ipv']  = $ipv;
			$data['icoabank']  = $icoabank;
			$data['perusahaan']=$this->mmaster->baca_persh();
			$data['query']=$this->mmaster->bacapvb1($ipv,$iarea,$icoabank);
			$sess=$this->session->userdata('session_id');
			$sql	= "select * from pabrik_sessions where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
			}else{
			  $ip_address='kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$sql	= "select nama from tm_area where id='$iarea'";
			$rs		=  $this->db->query($sql);
				if($rs->num_rows>0){
				foreach($rs->result() as $tes){
			     $data['eareaname']=$tes->nama;
			  }
			}
			$pesan='Cetak PV Area:'.$iarea.' No:'.$ipv;
			$this->load->view('list_voucher/vformrpt4', $data);
		
	}
	function cetak5()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			
			$irv	  = $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$icoabank=$this->input->post('icoabank', TRUE);
			if($icoabank=='') $icoabank = $this->uri->segment(6);
			$data['iarea']= $iarea;
			$data['irv']  = $irv;
			$data['icoabank']  = $icoabank;
			$data['query']=$this->mmaster->bacarvb1($irv,$iarea,$icoabank);
			$data['perusahaan']=$this->mmaster->baca_persh();
			$sess=$this->session->userdata('session_id');
			$sql	= "select * from pabrik_sessions where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
			}else{
			  $ip_address='kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$sql	= "select nama from tm_area where id='$iarea'";
			$rs		=  $this->db->query($sql);
				if($rs->num_rows>0){
				foreach($rs->result() as $tes){
			     $data['eareaname']=$tes->nama;
			  }
			}
			$pesan='Cetak RV Area:'.$iarea.' No:'.$irv;
			$this->load->view('list_voucher/vformrpt5', $data);
		
	} 
}
