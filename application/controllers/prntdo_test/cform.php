<?php
include_once("printipp_classes/PrintIPP.php"); 


class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
			$data['page_title_do']	= $this->lang->line('page_title_do');
			$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
			$data['list_no_do']	= $this->lang->line('list_no_do');
			$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
			$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
			$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
			$data['list_qty_do']	= $this->lang->line('list_qty_do');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');		
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$this->load->model('prntdo_test/mclass');
			$data['isi']	='prntdo_test/vmainform';
		$this->load->view('template',$data);
		
		
	}
	
	function carilistdo() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}		
		$data['page_title_do']	= $this->lang->line('page_title_do');
		$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
		$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
		$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
		$data['list_qty_do']	= $this->lang->line('list_qty_do');
		$data['list_sisa_delivery_do']	= $this->lang->line('list_sisa_delivery_do');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['ldobrg']	= "";

		$kddo	= $this->input->post('nomor_do')? $this->input->post('nomor_do'): $this->input->get_post('nomor_do');
		
		$this->load->model('prntdo_test/mclass');
		
		$data['query']= $this->mclass->clistdobrg($kddo);
		$qtgl		= $this->mclass->ctgldobrg($kddo);
		$qcabang	= $this->mclass->getcabang($kddo);
		
		if($qcabang->num_rows() > 0 ) {
			$row_cabang	= $qcabang->row_array();
			$data['nmcabang']	= $row_cabang['cabangname'];
		} else {
			$data['nmcabang']	= "";
		}
		
		if($qtgl->num_rows() > 0 ) {

			$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli', 
						'08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
			
			$row_tgl	= $qtgl->row();
			$exp_tgl	= explode("-",$row_tgl->tgl,strlen($row_tgl->tgl)); // Y-m-d
			$f_tgl		= substr($exp_tgl[2],0,1)=='0'?substr($exp_tgl[2],1,1):$exp_tgl[2];
			$f_bln		= $bglobal[$exp_tgl[1]];
			$f_thn		= $exp_tgl[0];
			
			$data['nomor']	= $kddo;
			$data['sh_tgl']	= $f_tgl." ".$f_bln." ".$f_thn;
		} else {
			$data['nomor']	= "";
			$data['sh_tgl']	= "";		
		}
		$data['isi']	='prntdo_test/vlistform';
		$this->load->view('template',$data);
	
	}
	
	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		//$data['iterasi']	= $this->uri->segment(4);
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntdo_test/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntdo_test/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('prntdo_test/vlistformbrgjadi',$data);		
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		//$data['iterasi']	= $this->uri->segment(4);
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntdo_test/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntdo_test/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('prntdo_test/vlistformbrgjadi',$data);		
	}

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('prntdo_test/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->i_do_code')\">".$row->i_do_code."</a></td>
				  <td width=\"120px;\"><a href=\"javascript:settextfield('$row->i_do_code')\">".$row->d_do."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->i_do_code')\">".$row->i_op_code."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}		
	
	function cdo(){
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$nodo	= $this->uri->segment(4);
		/*		
		$host		= "192.168.0.194";
		$uri		= "/printers/EpsonLX300";
		$ldo		= "";
			
		$data['host']		= $host;
		$data['uri']		= $uri;
		$data['ldo']		= $ldo;

		$ipp = new PrintIPP();
		
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		*/
		/*
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		
		$ipp->setData("Delivery Order".str_repeat(" ",55)."CV.DUTA SETIA GARMEN"."\n");
		$ipp->printJob();
		*/
		$data['page_title_do']	= $this->lang->line('page_title_do');
		$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
		$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
		$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
		$data['list_qty_do']	= $this->lang->line('list_qty_do');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['ldobrg']	= "";

		$kddo	= $nodo;
		
		$this->load->model('prntdo_test/mclass');
		
		$data['query']= $this->mclass->clistdobrg($nodo);
		$qtgl		= $this->mclass->ctgldobrg($nodo);
		$qcabang	= $this->mclass->getcabang($nodo);
		
		if($qcabang->num_rows() > 0 ) {
			$row_cabang	= $qcabang->row_array();
			$data['nmcabang']	= $row_cabang['cabangname'];
		} else {
			$data['nmcabang']	= "";
		}
		
		if($qtgl->num_rows() > 0 ) {

			$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli', 
						'08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
		
			$row_tgl	= $qtgl->row();
			$exp_tgl	= explode("-",$row_tgl->tgl,strlen($row_tgl->tgl)); // Y-m-d
			$f_tgl		= substr($exp_tgl[2],0,1)=='0'?substr($exp_tgl[2],1,1):$exp_tgl[2];
			$f_bln		= $bglobal[$exp_tgl[1]];
			$f_thn		= $exp_tgl[0];
			
			$data['nomor']	= $kddo;
			$data['sh_tgl']	= $f_tgl." ".$f_bln." ".$f_thn;
		} else {
			$data['nomor']	= "";
			$data['sh_tgl']	= "";		
		}
			$data['isi']	='prntdo_test/vprintform';
		$this->load->view('template',$data);
		
	}
	
	function cpopup() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$kddo		= $this->uri->segment(4);
		$iduserid	= $this->session->userdata('user_idx');
	
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.173';
		$uri		= '/printers/EPSON-LQ-2180yani';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;	
		
		//$data['charset']	= 'us-ascii';
		//$data['setLanguage']= 'en_us';
		//$data['mime_media_type']	= 'application/octet-stream';
		
		$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli', 
			'08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
		$tgl	= date("d");
		$bln	= date("m");						
		$thn	= date("Y");
		$new_bln= $bglobal[$bln];
		$data['tglprint']	= $tgl."-".$new_bln."-".$thn;
		
		$this->load->model('prntdo_test/mclass');
		
		$this->mclass->fprinted($kddo);
		
		//$qry_source_remote	= $this->mclass->remote($remote);
		$qry_source_remote	= $this->mclass->remote($iduserid, $remote);
		$qtgl		= $this->mclass->ctgldobrg($kddo);
		$qcabang	= $this->mclass->getcabang($kddo);
		$qnmrop		= $this->mclass->cnmrop($kddo);
		$qinitial	= $this->mclass->getinitial();

		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;
		$data['isi']	= $this->mclass->clistdobrg($kddo);
				
		if($qcabang->num_rows() > 0 ) {
			$row_cabang	= $qcabang->row_array();
			$data['nmcabang']	= $row_cabang['cabangname'];
		} else {
			$data['nmcabang']	= '';
		}

		if($qinitial->num_rows()>0) {
			$row_initial	= $qinitial->row_array();
			$data['nminitial']	= $row_initial['e_initial_name'];
		} else {
			$data['nminitial']	= "";
		}
				
		//$nomorop = "";
		if($qnmrop->num_rows() > 0) {
			$row_nmrop	= $qnmrop->row();
			$nomorop = $row_nmrop->iopcode;
			//$result_nmrop	= $qnmrop->result();
			//foreach($result_nmrop as $row_nmrop) {
				//$nomorop .= "Nomor OP : <b>".$row_nmrop->iopcode."</b>"."<br>";
			//}
		} else {
			$nomorop = "";
		}
		$data['nomorop']	= $nomorop;
		
		if($qtgl->num_rows() > 0 ) {

			$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
							'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
			
			$row_tgl	= $qtgl->row();
			$exp_tgl	= explode("-",$row_tgl->tgl,strlen($row_tgl->tgl));
			$f_tgl		= substr($exp_tgl[2],0,1)=='0'?substr($exp_tgl[2],1,1):$exp_tgl[2];
			$f_bln		= $bglobal[$exp_tgl[1]];
			$f_thn		= $exp_tgl[0];
			
			$data['nomor']	= $kddo;
			$data['shtgl']	= (string) html_entity_decode($f_tgl.'#'.$f_bln.'#'.$f_thn);
			
			$data['dotgl']	= (string) html_entity_decode($f_tgl);
			$data['dobln']	= (string) html_entity_decode($f_bln);
			$data['dothn']	= (string) html_entity_decode($f_thn);
			
		} else {
			$data['nomor']	= '';
			$data['shtgl']	= '';		
		}
				
		$this->load->view('prntdo_test/vtestform',$data);
		// 16-02-2013
		//$this->load->view('prntdo_test/vtestform_new160213',$data);
		//$this->load->view('prntdo_test/vpopupform',$data);
	}
}
?>
