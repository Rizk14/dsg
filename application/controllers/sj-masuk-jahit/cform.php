<?php
class Cform extends CI_Controller{

  function __construct () {
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('sj-masuk-jahit/mmaster');
  }

function index() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$is_no_sjkeluar = $this->input->post('is_no_sjkeluar', TRUE);
	$id_sj_keluar	= $this->input->post('id_sj_keluar', TRUE);  
	$no_sj_keluar	= $this->input->post('no_sj_keluar', TRUE);  
	$itembrg	= $this->input->post('sj_keluar', TRUE);  
	$proses_submit	= $this->input->post('submit', TRUE); 
	$id_proses_jahit_detail = $this->input->post('id_proses_jahit_detail', TRUE);  
	$unit_jahit	= $this->input->post('unit_jahit', TRUE);  
	
	$idsjprosesjahitdetail = explode(";", $id_proses_jahit_detail);
	$th_now	= date("Y");

	if($proses_submit=="Proses") {
		
		if ($is_no_sjkeluar == '') {  // jika ambil dari SJ keluar
			if($id_sj_keluar !='' && $no_sj_keluar!='') {
				$hsil_nosj	= '';
				$exp_nosj	= explode(";",$no_sj_keluar);
				$j=0;
				$exp_nosj2	= $exp_nosj;
				
				$arr	= array();
				
				foreach($exp_nosj as $s) { // 1,2,3,4,5
					
					$temp		= $s; // 1
					$c = 0;
					foreach($exp_nosj2 as $nosj) { // 1,2,3,4,5
						if($temp!=$nosj){
							$arr[$c]	= $nosj;
							//$hsil_nosj .= $nosj.'-';	
							foreach($arr as $cek) {
								if($temp==$cek){
									$hsil_nosj .= $cek.'-';
									//break;
								}	
							}		
							$c+=1;
							//break;			
						}else{
							break;
							//$hsil_nosj .= $nosj.'-';
						}
						
					}
					$j++;
				}
				
				$data['sj_detail'] = $this->mmaster->get_detail_sj_keluar($id_sj_keluar, $idsjprosesjahitdetail, $unit_jahit);
				$data['msg'] = '';
				$data['id_sj_keluar'] = $id_sj_keluar;
				$data['no_sj_keluar'] = $no_sj_keluar;
			}
			else {
				$data['msg'] = 'SJ Keluar harus dipilih';
				$data['id_sj_keluar'] = '';
				$data['no_sj_keluar'] = '';
				$data['list_unit'] = $this->mmaster->get_jahit();
			}
			
			$data['go_proses'] = '1';
			$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit='$unit_jahit' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$nama_unit	= $hasilrow->nama;
					
			$data['nama_unit'] = $nama_unit;
			$data['unit_jahit'] = $unit_jahit;
			$data['isi'] = 'sj-masuk-jahit/vmainform';
			$this->load->view('template',$data);
		}
		else { // jika add item brg manual
			$data['go_proses'] = '1';
			$data['msg'] = '';
			$data['is_no_sjkeluar'] = $is_no_sjkeluar;
			$data['no_sj_keluar'] = '';
			$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit='$unit_jahit' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$nama_unit	= $hasilrow->nama;
					
			$data['nama_unit'] = $nama_unit;
			$data['unit_jahit'] = $unit_jahit;
			$data['isi'] = 'sj-masuk-jahit/vmainformnosjkeluar';
			$this->load->view('template',$data);
		}
	
	}
	else {
		$data['msg'] = '';
		//$data['id_sj_keluar'] = '';
		$data['go_proses'] = '';
		$data['list_unit'] = $this->mmaster->get_jahit();
		$data['isi'] = 'sj-masuk-jahit/vmainform';
		$this->load->view('template',$data);
	}
	//$data['nosjhslpacking']	= $this->mmaster->generate_nomor();	
  }

  function view() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'sj-masuk-jahit/vformview';
    $keywordcari = "all";
    $unit_jahit = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($unit_jahit, $keywordcari); 
							$config['base_url'] = base_url().'index.php/sj-masuk-jahit/cform/view/index/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $unit_jahit, $keywordcari);		
	$data['jum_total'] = count($jum_total);
	
	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['list_unit'] = $this->mmaster->get_jahit();
	$data['unit_jahit'] = $unit_jahit;
	
	$this->load->view('template',$data);
  }
  
  function cari(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	
	if ($keywordcari=='' && $unit_jahit=='') {
		$unit_jahit 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari=='')
		$keywordcari 	= "all";

	if ($unit_jahit=='')
		$unit_jahit = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($unit_jahit, $keywordcari);
							$config['base_url'] = base_url().'index.php/sj-masuk-jahit/cform/cari/'.$unit_jahit.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $unit_jahit, $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'sj-masuk-jahit/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_unit'] = $this->mmaster->get_jahit();
	$data['unit_jahit'] = $unit_jahit;
	$this->load->view('template',$data);
  }
    
  function edit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$id_sj_hsl_jahit 	= $this->uri->segment(4);
	//$id_sj_proses_jahit	= $this->uri->segment(5);
	
	//$data['query'] = $this->mmaster->get_sj($id_sj_hsl_jahit,$id_sj_proses_jahit);
	$data['query'] = $this->mmaster->get_sj($id_sj_hsl_jahit);
	$data['msg'] = '';

	$data['isi'] = 'sj-masuk-jahit/veditform';
	$this->load->view('template',$data);
  }


  function submit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}		
			$id_sj_keluar 	= $this->input->post('id_sj_keluar', TRUE);
			$idsjkeluar		= explode(";",$id_sj_keluar);
			
			$indek_awal	= 0;
			$isjkeluar	= array();
			
			for($f=1;$f<=count($idsjkeluar);$f++) {
				$isjkeluar[$f]	= $idsjkeluar[$indek_awal];
				$indek_awal++;
			}
			
			$is_no_sjkeluar 	= $this->input->post('is_no_sjkeluar', TRUE);
			$gtotal 	= $this->input->post('gtotal', TRUE);
			$unit_jahit 	= $this->input->post('unit_jahit', TRUE);
			$no 			= $this->input->post('no', TRUE);
			
			//$no_sj_keluar	= $this->input->post('no_sj_keluar', TRUE);
			$no_sj_masuk 	= $this->input->post('no_sj_masuk', TRUE);		
			$tgl_sj_masuk	= $this->input->post('tgl_sj_masuk', TRUE);
			$pisah1 = explode("-", $tgl_sj_masuk);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			$ket = $this->input->post('ket', TRUE);
			
			//$totalnya	= $this->input->post('totalnya', TRUE);
			
			$id_sj_masuk 	= $this->input->post('id_sj_masuk', TRUE);
			
			if ($id_sj_masuk=='') { // lakukan insert
				$cek_data = $this->mmaster->cek_data($no_sj_masuk);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'sj-masuk-jahit/vmainform';
					$data['list_unit'] = $this->mmaster->get_jahit();
					$data['msg'] = "Data no SJ ".$no_sj_masuk." sudah ada..!";
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{	
						if ($is_no_sjkeluar == '') {
							$kodenya = $this->input->post('kode_'.$i, TRUE);
							$namanya = $this->input->post('nama_'.$i, TRUE);
						}
						else {
							$kodenya = $this->input->post('kode_brg_jadi_'.$i, TRUE);
							$namanya = $this->input->post('nama_brg_jadi_'.$i, TRUE);
						}
						
						//$this->mmaster->save($isjkeluar[$i], $no_sj_masuk, $tgl_sj, $unit_jahit, $ket, $totalnya,$this->input->post('kode_'.$i, TRUE), $this->input->post('nama_'.$i, TRUE), $this->input->post('qty_'.$i, TRUE), $this->input->post('biaya_'.$i, TRUE), $this->input->post('id_proses_jahit_'.$i, TRUE), $this->input->post('id_proses_jahit_detail_'.$i, TRUE) );
						$this->mmaster->save($isjkeluar[$i], $no_sj_masuk, $tgl_sj, $unit_jahit, $ket, 
								$kodenya, $namanya, 
								$this->input->post('qty_'.$i, TRUE), $this->input->post('id_proses_jahit_'.$i, TRUE), 
								$this->input->post('id_proses_jahit_detail_'.$i, TRUE), $is_no_sjkeluar, $gtotal,
								$this->input->post('harga_'.$i, TRUE), $this->input->post('harga_lama_'.$i, TRUE),
								$this->input->post('ket_warna_'.$i, TRUE), 
								$this->input->post('total_'.$i, TRUE) );
					}
					redirect('sj-masuk-jahit/cform/view');
				}
			}
			else {
				$tgl = date("Y-m-d");
				
				$qsjhsljahit	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit WHERE id='$id_sj_masuk' ");

				if($qsjhsljahit->num_rows()>0){

					$rsjhsljahit	= $qsjhsljahit->row();
					$id_sj_proses_jahit	= $rsjhsljahit->id_sj_proses_jahit;
					
					$this->db->query(" UPDATE tm_sj_hasil_jahit SET tgl_sj='$tgl_sj', keterangan='$ket', tgl_update='$tgl', status_edit='t' WHERE id='$id_sj_masuk' ");
					
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$id_hasil_jahit_detail	= $this->input->post('id_hasil_jahit_detail_'.$i, TRUE);
						$id_proses_jahit_detail = $this->input->post('id_proses_jahit_detail_'.$i, TRUE);
						$kode 		= $this->input->post('kode_'.$i, TRUE);
						$qty		= $this->input->post('qty_'.$i, TRUE);
						$qty_lama	= $this->input->post('qty_lama_'.$i, TRUE);
						//$biaya = $this->input->post('biaya_'.$i, TRUE);
						
						if ($qty!='' || $qty!=0) {
											
							//$query3	= $this->db->query(" UPDATE tm_sj_hasil_jahit_detail SET qty_brg_jadi='$qty', biaya='$biaya' WHERE id='$id_hasil_jahit_detail' ");	
							$query3	= $this->db->query(" UPDATE tm_sj_hasil_jahit_detail SET qty_brg_jadi='$qty' WHERE id='$id_hasil_jahit_detail' ");	
							
							if($kode!=''){
								$qstokhsiljahit	= $this->db->query(" SELECT * FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$kode' ORDER BY id DESC LIMIT 1 ");
								if($qstokhsiljahit->num_rows()>0){
									$rstokhsiljht	= $qstokhsiljahit->row();
									$qtystokhsljahit= $rstokhsiljht->stok;
									$idstokhsljahit	= $rstokhsiljht->id;
	
									$stokhsiljahit_upt = ($qtystokhsljahit-$qty_lama)+$qty;
									$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$stokhsiljahit_upt', tgl_update_stok='$tgl' WHERE kode_brg_jadi='$kode' AND id='$idstokhsljahit' ");
								}else{
								}
								
								//$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi,no_bukti,masuk,keluar,saldo,biaya,tgl_input) VALUES('$kode','$no_sj_masuk','$qty','$qty_lama','$stokhsiljahit_upt','$biaya','$tgl') ");
								$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi,no_bukti,masuk,keluar,saldo,tgl_input) VALUES('$kode','$no_sj_masuk','$qty','$qty_lama','$stokhsiljahit_upt','$tgl') ");
							}
						}
					}
					
				}									
				redirect('sj-masuk-jahit/cform/view');
			}
  }

  function delete(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $id_sj 	= $this->uri->segment(4); // id sj masuk
    $id_sj_proses_jahit_detail 	= $this->uri->segment(5); // sj detail keluar
    $this->mmaster->delete($id_sj, $id_sj_proses_jahit_detail);
    redirect('sj-masuk-jahit/cform/view');
  }
  
  function generate_nomor() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
  
  function show_popup_brg(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);

	if ($posisi == '' || $kel_brg == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg 	= $this->input->post('kel_brg', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		$id_jenis_bhn = $this->input->post('id_jenis_bhn', TRUE);  
		
		if ($keywordcari == '' && ($id_jenis_bhn == '' || $posisi == '' || $kel_brg == '') ) {
			$kel_brg 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$id_jenis_bhn 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $id_jenis_bhn);
		
		$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg/".$kel_brg."/".$posisi."/".$id_jenis_bhn."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
							
		$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $id_jenis_bhn);

		$data['jum_total'] = count($qjum_total);
		$data['posisi'] = $posisi;
		$data['kel_brg'] = $kel_brg;
		
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
		
		$data['nama_kel'] = $nama_kel;
		$data['jenis_bhn'] = $this->mmaster->get_jenis_bhn($kel_brg);
		$data['cjenis_bhn'] = $id_jenis_bhn;

		$this->load->view('sj-keluar-makloon/vpopupbrg',$data);
  }
  

  function show_popup_sj_keluar() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$unit_jahit	= $this->uri->segment(4);

	if ($unit_jahit=='') {
		$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
	if ($keywordcari=='' && $unit_jahit=='' ) {
		$unit_jahit 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
		
	if ($keywordcari=='')
		$keywordcari = "all";

	$qjum_total = $this->mmaster->get_sj_keluartanpalimit($keywordcari, $unit_jahit);
		
	$config['base_url'] = base_url()."index.php/sj-masuk-jahit/cform/show_popup_sj_keluar/".$unit_jahit."/".$keywordcari."/";
	$config['total_rows'] = count($qjum_total);
	$config['per_page'] = 10;
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(6);
	$this->pagination->initialize($config);
		
	$data['query'] = $this->mmaster->get_sj_keluar($config['per_page'],$config['cur_page'], $keywordcari, $unit_jahit);

	$data['jum_total'] = count($qjum_total);
	$data['unit_jahit'] = $unit_jahit;
	
	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit='$unit_jahit' ");
	
	if($query3->num_rows()>0) {
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
		$data['nama_unit'] = $nama_unit;
	}else{
		$data['nama_unit'] = '';
	}
	
	$this->load->view('sj-masuk-jahit/vpopupsjkeluar',$data);
	
  }
  
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi 	= $this->uri->segment(4);
	$kode_unit 	= $this->uri->segment(5);
	
	if ($posisi == '' || $kode_unit == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kode_unit = $this->input->post('kode_unit', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
	if ($keywordcari == '' || ($posisi == '' && $kode_unit == '')) {
		$posisi 	= $this->uri->segment(4);
		$kode_unit 	= $this->uri->segment(5);
		$keywordcari = $this->uri->segment(6);
	}
		
	if ($keywordcari == '')
		$keywordcari 	= "all";
		  
	$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari, $kode_unit);

				$config['base_url'] = base_url()."index.php/sj-masuk-jahit/cform/show_popup_brg_jadi/".$posisi."/".$kode_unit."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari, $kode_unit);			
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['kode_unit'] = $kode_unit;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('sj-masuk-jahit/vpopupbrgjadi',$data);
  }
  
}
