<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expopenjualanperdo/mclass');
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}		
			$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
			$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
			$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
			$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
			$data['list_penjperdo_tgl_faktur_mulai']	= $this->lang->line('list_penjperdo_tgl_faktur_mulai');

			$this->load->model('expopenjualanperdo/mclass');

			$data['pelanggan']	= $this->mclass->lpelanggan();
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			/*** $this->load->model('expopenjualanperdo/mclass'); ***/
				$data['isi']	= 'expopenjualanperdo/vmainform';	
			$this->load->view('template',$data);	
		
		
	}
	
	function carilistpenjualanperdo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_faktur_mulai']	= $this->lang->line('list_penjperdo_tgl_faktur_mulai');
		$data['list_penjperdo_no_do']	= $this->lang->line('list_penjperdo_no_do');
		$data['list_penjperdo_nm_brg']	= $this->lang->line('list_penjperdo_nm_brg');
		$data['list_penjperdo_qty']	= $this->lang->line('list_penjperdo_qty');
		$data['list_penjperdo_hjp']	= $this->lang->line('list_penjperdo_hjp');
		$data['list_penjperdo_amount']	= $this->lang->line('list_penjperdo_amount');
		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";		
		$data['limages']	= base_url();
		
		
		$pelanggan	=	$this->input->post('pelanggan');
		$no_faktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->uri->segment(4);
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->uri->segment(5);
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->uri->segment(6);

		$data['tgldomulai']	= empty($d_do_first)?"":$d_do_last;
		$data['tgldoakhir']	= empty($d_do_last)?"":$d_do_last;
		$data['nofaktur']	= empty($no_faktur)?"":$no_faktur;
		
		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:date("Y-m-d");
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:date("Y-m-d");
		
		$nofak	= empty($no_faktur)?'0':$no_faktur;
		$dfirst	= empty($d_do_first)?'0':$n_d_do_first;
		$dlast	= empty($d_do_last)?'0':$n_d_do_last;
		
		$data['tfirst']	= $dfirst;
		$data['tlast']	= $dlast;
		$data['nofak']	= $nofak;
		$data['pelang']	= $pelanggan;
		
		/*** $this->load->model('expopenjualanperdo/mclass'); ***/

		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast, $pelanggan);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expopenjualanperdo/cform/carilistpenjualanperdoperpages/'.$nofak.'/'.$dfirst.'/'.$dlast;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();

		$qtotalpenjualan	= $this->mclass->clistpenjualanperdototal($nofak,$dfirst,$dlast, $pelanggan);
		
		if($qtotalpenjualan->num_rows()>0){
			$rtotalpenjualan	= $qtotalpenjualan->row();
			$data['jmltotalpenjualan']	= $rtotalpenjualan->qty;
			$data['nilaitotalpenjualan']	= $rtotalpenjualan->amount;
		}else{
			$data['jmltotalpenjualan']	= 0;
			$data['nilaitotalpenjualan']	= 0;
		}

		$data['query']	= $this->mclass->clistpenjualanperdoperpages($nofak, $dfirst, $dlast, $pagination['per_page'], $pagination['cur_page'], $pelanggan);

		$data['isi']	= 'expopenjualanperdo/vlistform';	
		$this->load->view('template',$data);	
			
	}
	
	function carilistpenjualanperdoperpages() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_faktur_mulai']	= $this->lang->line('list_penjperdo_tgl_faktur_mulai');
		$data['list_penjperdo_no_do']	= $this->lang->line('list_penjperdo_no_do');
		$data['list_penjperdo_nm_brg']	= $this->lang->line('list_penjperdo_nm_brg');
		$data['list_penjperdo_qty']	= $this->lang->line('list_penjperdo_qty');
		$data['list_penjperdo_hjp']	= $this->lang->line('list_penjperdo_hjp');
		$data['list_penjperdo_amount']	= $this->lang->line('list_penjperdo_amount');
		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";		
		$data['limages']	= base_url();
		
		$no_faktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->uri->segment(4); // Y-m-d
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->uri->segment(5);
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->uri->segment(6);

		$e_d_do_first	= explode("-",$d_do_first,strlen($d_do_first)); // Y-m-d
		$e_d_do_last	= explode("-",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:date("d/m/Y"); // d/m/Y
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:date("d/m/Y");
				
		$data['tgldomulai']	= empty($n_d_do_first)?"":$n_d_do_first;
		$data['tgldoakhir']	= empty($n_d_do_last)?"":$n_d_do_last;
		$data['nofaktur']	= (empty($no_faktur) || $no_faktur=='0')?"":$no_faktur;

		$nofak	= (empty($no_faktur) || $no_faktur=='0')?'0':$no_faktur;
		$dfirst	= (empty($d_do_first) || $d_do_first=='0')?'0':$d_do_first;
		$dlast	= (empty($d_do_last) || $d_do_last=='0')?'0':$d_do_last;

		$data['tfirst']	= $dfirst;
		$data['tlast']	= $dlast;
		$data['nofak']	= $nofak;
				
		/*** $this->load->model('expopenjualanperdo/mclass'); ***/

		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expopenjualanperdo/cform/carilistpenjualanperdoperpages/'.$nofak.'/'.$dfirst.'/'.$dlast;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();

		$qtotalpenjualan	= $this->mclass->clistpenjualanperdototal($nofak,$dfirst,$dlast);
		
		if($qtotalpenjualan->num_rows()>0){
			$rtotalpenjualan	= $qtotalpenjualan->row();
			$data['jmltotalpenjualan']	= $rtotalpenjualan->qty;
			$data['nilaitotalpenjualan']	= $rtotalpenjualan->amount;
		}else{
			$data['jmltotalpenjualan']	= 0;
			$data['nilaitotalpenjualan']	= 0;
		}
				
		$data['isi']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('expopenjualanperdo/vlistform',$data);	
	}
	
	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expopenjualanperdo/mclass'); ***/

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expopenjualanperdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('expopenjualanperdo/vlistformbrgjadi',$data);			
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expopenjualanperdo/mclass'); ***/

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expopenjualanperdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('expopenjualanperdo/vlistformbrgjadi',$data);			
	}		

	function flistbarangjadi() {
		
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');

		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		/*** $this->load->model('expopenjualanperdo/mclass'); ***/

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
		echo $item;
	}

	function gexportpenjualando() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
	$db2=$this->load->database('db_external', TRUE);
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_faktur_mulai']	= $this->lang->line('list_penjperdo_tgl_faktur_mulai');
		$data['list_penjperdo_no_do']	= $this->lang->line('list_penjperdo_no_do');
		$data['list_penjperdo_nm_brg']	= $this->lang->line('list_penjperdo_nm_brg');
		$data['list_penjperdo_qty']	= $this->lang->line('list_penjperdo_qty');
		$data['list_penjperdo_hjp']	= $this->lang->line('list_penjperdo_hjp');
		$data['list_penjperdo_amount']	= $this->lang->line('list_penjperdo_amount');
		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$nofaktur	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		
		$nofak	= empty($nofaktur) || $nofaktur=='0' ?'0':$nofaktur;
		$dfirst	= empty($tdofirst) || $tdofirst=='0' ?'0':$tdofirst;
		$dlast	= empty($tdolast) || $tdolast=='0' ?'0':$tdolast;
		
		$extdofirst	= $tdofirst!=0?explode("-",$tdofirst,strlen($tdofirst)):$tdofirst;
		$extdolast	=  $tdolast!=0?explode("-",$tdolast,strlen($tdolast)):$tdolast;
		
		$nwdofirst	= $extdofirst!=0?$extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0]:$extdofirst;
		$nwdolast	=$extdolast!=0? $extdolast[2]."/".$extdolast[1]."/".$extdolast[0]:$extdolast;
		
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= empty($nwdofirst)?"":$nwdofirst;
		$data['tgldoakhir']	= empty($nwdolast)?"":$nwdolast;
		$data['nofaktur']	= $nofaktur=='0'?"":$nofaktur;
				
		/*** $this->load->model('expopenjualanperdo/mclass'); ***/
		
		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expopenjualanperdo/cform/gexportpenjualandonext/'.$nofak.'/'.$dfirst.'/'.$dlast.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
				
		$data['query']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);		
		
		$ObjPHPExcel = new PHPExcel();
		
		$qexppenjualando	= $this->mclass->explistpenjualanperdo($nofak,$dfirst,$dlast);
		
		if($qexppenjualando->num_rows()>0) {
		
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Penjualan Berdasarkan DO")
				->setSubject("Laporan Penjualan")
				->setDescription("Laporan Penjualan")
				->setKeywords("Laporan Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:L2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN PENJUALAN BERDASARKAN DO');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:L3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:L4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),													
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl. Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl. Jth Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'No.Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'No.Pajak');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Tgl.Faktur Pajak');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Nama');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Bruto');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Diskon');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'DPP');
			$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'PPN');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('L6', 'Total');
			$ObjPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
												
			if($qexppenjualando->num_rows()>0) {
				
				$j	= 7;
				$nomor	= 1;
				$jml=1;
				
				$bruto	= array();
				$dpp	= array();
				$ppn	= array();
				$total	= array();
				
				foreach($qexppenjualando->result() as $row) {

					$qttfaktur	= $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_do_item_t WHERE i_faktur='$row->ifaktur' ");
					if($qttfaktur->num_rows()>0){
						$rttfaktur	= $qttfaktur->row();
							
						$bruto[$jml]	= $rttfaktur->totalfaktur;
						$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
						$ppn[$jml]		= round(($dpp[$jml]*11)/100);
						$total[$jml]	= round($dpp[$jml]+$ppn[$jml]);							
					}
					
					$row->dfaktur	= explode("-",$row->dfaktur,strlen($row->dfaktur)); // Y-m-d
					$tfaktur	= $row->dfaktur[2];
					$bfaktur	= $row->dfaktur[1];
					$thfaktur	= $row->dfaktur[0];
					$tglfaktur	= $tfaktur."/".$bfaktur."/".$thfaktur;
						
					$row->dduedate	= explode("-",$row->dduedate,strlen($row->dduedate)); // Y-m-d
					$tduedate	= $row->dduedate[2];
					$bduedate	= $row->dduedate[1];
					$thduedate	= $row->dduedate[0];
					$tglduedate	= $tduedate."/".$bduedate."/".$thduedate;

					$row->dpajak	= explode("-",$row->dpajak,strlen($row->dpajak)); // Y-m-d
					$tpajak	= $row->dpajak[2];
					$bpajak	= $row->dpajak[1];
					$thpajak	= $row->dpajak[0];
					$tglpajak	= $tpajak."/".$bpajak."/".$thpajak;
					
					$nopajak		= $this->mclass->pajak($row->ifakturcode);
					if($nopajak->num_rows() > 0) {
						$row_pajak	= $nopajak->row();			
						$nomorpajak	= $row_pajak->ifakturpajak;			
						
						switch(strlen($nomorpajak)) {
							case "1": $nfaktur	= '0000000'.$nomorpajak; break;
							case "2": $nfaktur	= '000000'.$nomorpajak; break;
							case "3": $nfaktur	= '00000'.$nomorpajak; break;
							case "4": $nfaktur	= '0000'.$nomorpajak; break;
							case "5": $nfaktur	= '000'.$nomorpajak; break;
							case "6": $nfaktur	= '00'.$nomorpajak; break;
							case "7": $nfaktur	= '0'.$nomorpajak; break;
							default: $nfaktur	= $nomorpajak;
						}
						$nomorpajak	= "010"."."."000".".".date("y").".".$nfaktur;			
					} else {
						$nomorpajak	= "";
					}
																	
					//$bruto[$jml]	= $row->totalfaktur;
					//$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
					// Pembulantan ke atas
					//$ppn[$jml]		= ceil(($dpp[$jml]*10)/100);
					//$total[$jml]	= ceil($dpp[$jml]+$ppn[$jml]);

					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $tglfaktur);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $tglduedate);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->ifakturcode);
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $nomorpajak);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $tglpajak);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->customername);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $bruto[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->nilaidiscon);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $dpp[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, $ppn[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$j, $total[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
					$jml++;													
					$j++;																																																							
					$nomor++;
				}
			}
				
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."laporan_penj_do_".$dfirst."_".$dlast.".xls";
			$ObjWriter->save("files/".$files);
																							
		} else {

			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Penjualan Berdasarkan DO")
				->setSubject("Laporan Penjualan")
				->setDescription("Laporan Penjualan")
				->setKeywords("Laporan Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:L2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN PENJUALAN BERDASARKAN DO');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:L3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:L4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl. Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl. Jth Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'No.Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'No.Pajak');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Tgl.Faktur Pajak');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Nama');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Bruto');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Diskon');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'DPP');
			$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'PPN');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'Total');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."lap_penjualan_do_".$dfirst."_".$dlast.".xls";
			$ObjWriter->save("files/".$files);								
		}

		$efilename = @substr($files,1,strlen($files));
	
		$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
					$data['isi']	= 'expopenjualanperdo/vexpform';	
			$this->load->view('template',$data);	

	}
	function gexportpenjualandonofakpajak() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
	$db2=$this->load->database('db_external', TRUE);
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_faktur_mulai']	= $this->lang->line('list_penjperdo_tgl_faktur_mulai');
		$data['list_penjperdo_no_do']	= $this->lang->line('list_penjperdo_no_do');
		$data['list_penjperdo_nm_brg']	= $this->lang->line('list_penjperdo_nm_brg');
		$data['list_penjperdo_qty']	= $this->lang->line('list_penjperdo_qty');
		$data['list_penjperdo_hjp']	= $this->lang->line('list_penjperdo_hjp');
		$data['list_penjperdo_amount']	= $this->lang->line('list_penjperdo_amount');
		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$nofaktur	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		$pelanggan	= $this->uri->segment(7);
		
		$nofak	= empty($nofaktur) || $nofaktur=='0' ?'0':$nofaktur;
		$dfirst	= empty($tdofirst) || $tdofirst=='0' ?'0':$tdofirst;
		$dlast	= empty($tdolast) || $tdolast=='0' ?'0':$tdolast;
		
		$extdofirst	= $tdofirst!=0?explode("-",$tdofirst,strlen($tdofirst)):$tdofirst;
		$extdolast	=  $tdolast!=0?explode("-",$tdolast,strlen($tdolast)):$tdolast;
		
		$nwdofirst	= $extdofirst!=0?$extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0]:$extdofirst;
		$nwdolast	=$extdolast!=0? $extdolast[2]."/".$extdolast[1]."/".$extdolast[0]:$extdolast;
		
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= empty($nwdofirst)?"":$nwdofirst;
		$data['tgldoakhir']	= empty($nwdolast)?"":$nwdolast;
		$data['nofaktur']	= $nofaktur=='0'?"":$nofaktur;
				
		/*** $this->load->model('expopenjualanperdo/mclass'); ***/
		
		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expopenjualanperdo/cform/gexportpenjualandonext/'.$nofak.'/'.$dfirst.'/'.$dlast.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
				
		// $data['query']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page'],$pelanggan);		

		/*Update 30-07-2022*/
		$data['query']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);	
		
		$ObjPHPExcel = new PHPExcel();
		
		$qexppenjualando	= $this->mclass->explistpenjualanperdonofakpajak($nofak,$dfirst,$dlast,$pelanggan);
		
		if($qexppenjualando->num_rows()>0) {
		
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Penjualan Berdasarkan DO")
				->setSubject("Laporan Penjualan")
				->setDescription("Laporan Penjualan")
				->setKeywords("Laporan Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:L2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN PENJUALAN BERDASARKAN DO');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:L3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:L4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),													
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl. Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl. Jth Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'No.Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'No.Pajak');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Tgl.Faktur Pajak');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Nama');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Bruto');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Diskon');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'DPP');
			$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'PPN');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('L6', 'Total');
			$ObjPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
												
			if($qexppenjualando->num_rows()>0) {
				
				$j	= 7;
				$nomor	= 1;
				$jml=1;
				
				$bruto	= array();
				$dpp	= array();
				$ppn	= array();
				$total	= array();
				
				foreach($qexppenjualando->result() as $row) {

					$qttfaktur	= $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_do_item_t WHERE i_faktur='$row->ifaktur' ");
					if($qttfaktur->num_rows()>0){
						$rttfaktur	= $qttfaktur->row();
							
						$bruto[$jml]	= $rttfaktur->totalfaktur;
						$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
						$ppn[$jml]		= round(($dpp[$jml]*11)/100);
						$total[$jml]	= round($dpp[$jml]+$ppn[$jml]);							
					}
					
					$row->dfaktur	= explode("-",$row->dfaktur,strlen($row->dfaktur)); // Y-m-d
					$tfaktur	= $row->dfaktur[2];
					$bfaktur	= $row->dfaktur[1];
					$thfaktur	= $row->dfaktur[0];
					$tglfaktur	= $tfaktur."/".$bfaktur."/".$thfaktur;
						
					$row->dduedate	= explode("-",$row->dduedate,strlen($row->dduedate)); // Y-m-d
					$tduedate	= $row->dduedate[2];
					$bduedate	= $row->dduedate[1];
					$thduedate	= $row->dduedate[0];
					$tglduedate	= $tduedate."/".$bduedate."/".$thduedate;

					$row->dpajak	= explode("-",$row->dpajak,strlen($row->dpajak)); // Y-m-d
					$tpajak	= $row->dpajak[2];
					$bpajak	= $row->dpajak[1];
					$thpajak	= $row->dpajak[0];
					$tglpajak	= $tpajak."/".$bpajak."/".$thpajak;
					
					//~ $nopajak		= $this->mclass->pajak($row->ifakturcode);
					//~ if($nopajak->num_rows() > 0) {
						//~ $row_pajak	= $nopajak->row();			
						//~ $nomorpajak	= $row_pajak->ifakturpajak;			
						//~ 
						//~ switch(strlen($nomorpajak)) {
							//~ case "1": $nfaktur	= '0000000'.$nomorpajak; break;
							//~ case "2": $nfaktur	= '000000'.$nomorpajak; break;
							//~ case "3": $nfaktur	= '00000'.$nomorpajak; break;
							//~ case "4": $nfaktur	= '0000'.$nomorpajak; break;
							//~ case "5": $nfaktur	= '000'.$nomorpajak; break;
							//~ case "6": $nfaktur	= '00'.$nomorpajak; break;
							//~ case "7": $nfaktur	= '0'.$nomorpajak; break;
							//~ default: $nfaktur	= $nomorpajak;
						//~ }
						//~ $nomorpajak	= "010"."."."000".".".date("y").".".$nfaktur;			
					//~ } else {
						//~ $nomorpajak	= "";
					//~ }
																	
					//$bruto[$jml]	= $row->totalfaktur;
					//$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
					// Pembulantan ke atas
					//$ppn[$jml]		= ceil(($dpp[$jml]*10)/100);
					//$total[$jml]	= ceil($dpp[$jml]+$ppn[$jml]);

					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $tglfaktur);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $tglduedate);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->ifakturcode);
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->nomorpajak);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $tglpajak);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->customername);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $bruto[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->nilaidiscon);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $dpp[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, $ppn[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$j, $total[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
					$jml++;													
					$j++;																																																							
					$nomor++;
				}
			}
				
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."laporan_penj_do_as_no_faktur_pajak".$dfirst."_".$dlast.".xls";
			$ObjWriter->save("files/".$files);
																							
		} else {

			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Penjualan Berdasarkan DO")
				->setSubject("Laporan Penjualan")
				->setDescription("Laporan Penjualan")
				->setKeywords("Laporan Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:L2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN PENJUALAN BERDASARKAN DO');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:L3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:L4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl. Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl. Jth Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'No.Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'No.Pajak');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Tgl.Faktur Pajak');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Nama');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Bruto');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Diskon');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'DPP');
			$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'PPN');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'Total');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."laporan_penj_do_as_no_faktur_pajak".$dfirst."_".$dlast.".xls";
			$ObjWriter->save("files/".$files);								
		}

		$efilename = @substr($files,1,strlen($files));
	
		$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
					$data['isi']	= 'expopenjualanperdo/vexpform';	
			$this->load->view('template',$data);	

	}
	function gexportpenjualandonext() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
		$data['form_title_detail_penjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
		$data['list_penjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
		$data['list_penjperdo_no_faktur']	= $this->lang->line('list_penjperdo_no_faktur');
		$data['list_penjperdo_tgl_faktur_mulai']	= $this->lang->line('list_penjperdo_tgl_faktur_mulai');
		$data['list_penjperdo_no_do']	= $this->lang->line('list_penjperdo_no_do');
		$data['list_penjperdo_nm_brg']	= $this->lang->line('list_penjperdo_nm_brg');
		$data['list_penjperdo_qty']	= $this->lang->line('list_penjperdo_qty');
		$data['list_penjperdo_hjp']	= $this->lang->line('list_penjperdo_hjp');
		$data['list_penjperdo_amount']	= $this->lang->line('list_penjperdo_amount');
		$data['list_penjperdo_total_pengiriman']	= $this->lang->line('list_penjperdo_total_pengiriman');
		$data['list_penjperdo_total_penjualan']	= $this->lang->line('list_penjperdo_total_penjualan');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$nofaktur	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		
		$nofak	= empty($nofaktur) || $nofaktur=='0' ?'0':$nofaktur;
		$dfirst	= empty($tdofirst) || $tdofirst=='0' ?'0':$tdofirst;
		$dlast	= empty($tdolast) || $tdolast=='0' ?'0':$tdolast;
		
		$extdofirst	= $tdofirst!=0?explode("-",$tdofirst,strlen($tdofirst)):$tdofirst;
		$extdolast	=  $tdolast!=0?explode("-",$tdolast,strlen($tdolast)):$tdolast;
		
		$nwdofirst	= $extdofirst!=0?$extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0]:$extdofirst;
		$nwdolast	=$extdolast!=0? $extdolast[2]."/".$extdolast[1]."/".$extdolast[0]:$extdolast;
		
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= empty($nwdofirst)?"":$nwdofirst;
		$data['tgldoakhir']	= empty($nwdolast)?"":$nwdolast;
		$data['nofaktur']	= $nofaktur=='0'?"":$nofaktur;
				
		/*** $this->load->model('expopenjualanperdo/mclass'); ***/
		
		$query	= $this->mclass->clistpenjualanperdo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expopenjualanperdo/cform/gexportpenjualandonext/'.$nofak.'/'.$dfirst.'/'.$dlast.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		
		$data['create_link']	= $this->pagination->create_links();
				
		$data['isi']	= $this->mclass->clistpenjualanperdoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);			
		
		$this->load->view('expopenjualanperdo/vexpform',$data);
	}		
}
?>
