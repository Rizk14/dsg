<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('users-ganti-passwd/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['msg'] = "";
    $data['isi'] = 'users-ganti-passwd/vmainform';
	$this->load->view('template',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$username 	= $this->session->userdata('username');
			$uid 	= $this->session->userdata('uid');
			$passwd1 	= $this->input->post('passwd1', TRUE);
			$passwd2 	= $this->input->post('passwd2', TRUE);
			$passwd3 	= $this->input->post('passwd3', TRUE);
			
			$cek_data = $this->mmaster->cek_passwd_lama($username, $passwd1);
			if (count($cek_data) > 0) {
				$this->mmaster->update_passwd($uid, $passwd2);
				$data['isi'] = 'users-ganti-passwd/vmainform';
				$data['msg'] = "Password sukses diganti, silahkan logout dahulu, kemudian login lagi.";
				$this->load->view('template',$data);
			}
			else {
				$data['isi'] = 'users-ganti-passwd/vmainform';
				$data['msg'] = "Password lama salah..!";
				$this->load->view('template',$data);
			}
  }
  
}
