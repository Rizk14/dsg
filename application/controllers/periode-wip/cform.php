<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('periode-wip/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['msg'] = '';
	$data['isi'] = 'periode-wip/vmainform';
	$this->load->view('template',$data);
  }
  
function submit(){

$uid_input_by=$this->session->userdata('uid');
$tgl_periode= $this->input->post('tgl_periode');
$tgl_array=explode('-',$tgl_periode);
$tanggal= $tgl_array[0];
$bulan= $tgl_array[1];
$tahun= $tgl_array[2];
$tgl_periode=($tahun."-".$bulan."-".$tanggal);

$save=$this->mmaster->save($tgl_periode,$tanggal,$bulan,$tahun,$uid_input_by);
if($save){
	$data['msg'] = 'Penginputan Data Berhasil';
	$data['isi'] = 'periode-wip/vmainform';
	$this->load->view('template',$data);
	
	}

}
function view(){

$query =$this->mmaster->getview();

$data['query']=$query;
$data['isi'] = 'periode-wip/vformview';
$this->load->view('template',$data);

}
}
