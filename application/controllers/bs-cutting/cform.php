<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bs-cutting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'bs-cutting/vmainform';
	$data['msg'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$this->load->view('template',$data);

  }

  function submit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
		$tgl 	= $this->input->post('tgl', TRUE);  
		$ket 	= $this->input->post('ket', TRUE);  
		$pisah1 = explode("-", $tgl);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl = $thn1."-".$bln1."-".$tgl1;
						
		$no 	= $this->input->post('no', TRUE);
		$jumlah_input=$no-1;
					
		for ($i=1;$i<=$jumlah_input;$i++)
		{
			$this->mmaster->save($tgl, $ket, 
					$this->input->post('kode_'.$i, TRUE),
					$this->input->post('qty_'.$i, TRUE), 
					$this->input->post('kode_brg_jadi_'.$i, TRUE),
					$this->input->post('brg_jadi_manual_'.$i, TRUE),
					$this->input->post('diprint_'.$i, TRUE),
					$this->input->post('is_dacron_'.$i, TRUE) );
		}
		redirect('bs-cutting/cform/view');

  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'bs-cutting/vformview';
    $keywordcari = "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bs-cutting/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
    
    if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
    
							$config['base_url'] = base_url().'index.php/bs-cutting/cform/cari/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'bs-cutting/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function show_popup_brg_bisbisan(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi 	= $this->uri->segment(4);
	
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '') {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
					  
		$jum_total = $this->mmaster->get_bahanbisbisantanpalimit($keywordcari);
		$config['base_url'] = base_url()."index.php/pb-cutting/cform/show_popup_brg_bisbisan/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($jum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	
		$data['query'] = $this->mmaster->get_bahanbisbisan($config['per_page'],$config['cur_page'], $keywordcari);
		/*if ($data['query'] == '')
			$data['jum_total'] = 0;
		else
			$data['jum_total'] = count($data['query']); */
		
		$data['jum_total'] = count($jum_total);
		
		$config['total_rows'] = $data['jum_total'];
		
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['startnya'] = $config['cur_page'];
	$this->load->view('pb-cutting/vpopupbrgbisbisan',$data);
  }


  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
    
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "bs-cutting/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "bs-cutting/cform/cari/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
    
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$posisi 	= $this->uri->segment(4);
	
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $posisi == '') {
		$posisi 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
				
	if ($keywordcari == '')
		$keywordcari 	= "all";
		  
	$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari);

				$config['base_url'] = base_url()."index.php/bs-cutting/cform/show_popup_brg_jadi/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['posisi'] = $posisi;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('bs-cutting/vpopupbrgjadi',$data);
  }
  
  function show_popup_brg_cutting(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	//$kode_brg_jadi	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(4);
	
	if ($posisi == '')
		$posisi = $this->input->post('posisi', TRUE);  
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '') {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
					  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari);
		
				$config['base_url'] = base_url()."index.php/bs-cutting/cform/show_popup_brg_cutting/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('bs-cutting/vpopupbrg',$data);
  }
  
}
