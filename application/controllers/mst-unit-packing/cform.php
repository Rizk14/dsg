<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-unit-packing/mmaster');
  }
  
  // 16-06-2015, skrg pake field id utk primary key. nanti di tabel2 lain ganti pake id_unit_packing, bukan kode_unit_packing
  function updateidunit() {
	  $sql = " SELECT * FROM tm_unit_packing ORDER BY tgl_input ";
	  $query	= $this->db->query($sql);
	  
	  if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil id tertinggi
			$sql2 = " SELECT id FROM tm_unit_packing ORDER BY id DESC LIMIT 1 ";
			$query2	= $this->db->query($sql2);
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->row();
				$idlama	= $hasil2->id;
				$idbaru = $idlama+1;
				
				$this->db->query(" UPDATE tm_unit_packing SET id='$idbaru' WHERE kode_unit='$row1->kode_unit' ");
			}
		}
	}
	echo "sukses";
  }

  function index(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode_unit;
			$elokasi = $row->lokasi;
			$enama = $row->nama;
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$elokasi = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['elokasi'] = $elokasi;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-unit-packing/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode_unit', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode_unit', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			//$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('mst-unit-packing/cform');
		}
		else
		{ */
			$kode 	= $this->input->post('kode_unit', TRUE);
			$id_unit_packing 	= $this->input->post('id_unit_packing', TRUE); 
			$lokasi = $this->input->post('lokasi', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			
			$this->mmaster->save($kode, $id_unit_packing, $lokasi, $nama, $goedit);
			redirect('mst-unit-packing/cform');
		//}
  }

  function delete(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete($id);
    redirect('mst-unit-packing/cform');
  }
  
  
}
