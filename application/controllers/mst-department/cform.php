<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-department/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
			$igudang = $row->id_gudang;
			
		
		}	
	}
	else {
			$eid = '';
			$ekode = '';
			$enama = '';
			$edit = '';
			$igudang = '';
			$enama_gudang ='';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['igudang'] = $igudang;
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-department/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			//$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			//redirect('mst-unit-quilting/cform');
			$data['isi'] = 'mst-department/vmainform';
			$data['msg'] = 'Kode dan Nama harus diisi..!';
			$eid = '';
			$ekode = '';
			$enama = '';
			$edit = '';
			$data['eid'] = $eid;
			$data['ekode'] = $ekode;
			$data['enama'] = $enama;
			$data['edit'] = $edit;
			$data['query'] = $this->mmaster->getAll();
			$this->load->view('template',$data);
		}
		else
		{
			$kode 	= $this->input->post('kode', TRUE);
			$kode_lama 	= $this->input->post('kode_lama', TRUE);
			$id_bagian 	= $this->input->post('id_bagian', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$gudang 	= $this->input->post('gudang', TRUE);
			
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-department/vmainform';
					$data['msg'] = 'Data sudah ada..!';
					$eid = '';
					$ekode = '';
					$enama = '';
					$edit = '';
					$data['eid'] = $eid;
					$data['ekode'] = $ekode;
					$data['enama'] = $enama;
					$data['edit'] = $edit;
					
					$data['query'] = $this->mmaster->getAll();
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($kode, $id_bagian, $nama, $goedit,$gudang);
					redirect('mst-department/cform');
				}
			} // end if goedit == ''
			else {
				if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'mst-department/vmainform';
						$data['msg'] = "Data kode ".$kode." sudah ada..!";
						$eid = '';
						$ekode = '';
						$enama = '';
						$edit = '';
						$data['eid'] = $eid;
						$data['ekode'] = $ekode;
						$data['enama'] = $enama;
						$data['edit'] = $edit;
						
						$data['query'] = $this->mmaster->getAll();
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kode, $id_bagian,$nama, $goedit);
						redirect('mst-department/cform');
					}
				}
				else {
					$this->mmaster->save($kode, $id_bagian,$nama, $goedit);
					redirect('mst-department/cform');
				}
			}
			
		}
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete($id);
    redirect('mst-department/cform');
  }
  
  
}
