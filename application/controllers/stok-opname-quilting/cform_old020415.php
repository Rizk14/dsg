<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('stok-opname-quilting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['isi'] = 'stok-opname-quilting/vmainform';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	//$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);

	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
		if ($bulan > 1) {
			$bulan_sebelumnya = $bulan-1;
			$tahun_sebelumnya = $tahun;
		}
		else {
			$bulan_sebelumnya = 12;
			$tahun_sebelumnya = $tahun-1;
		}
		
		// misalnya mau input SO bln skrg. jika ada data SO di 1 bulan sebelumnya yg blm diapprove, maka tidak boleh input SO di bulan skrg.
		/*$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_quilting WHERE bulan = '$bulan_sebelumnya' 
							AND tahun = '$tahun_sebelumnya' AND status_approve = 'f' "); */
		// 14-02-2012							
		$sql = "SELECT id FROM tt_stok_opname_hasil_quilting WHERE status_approve = 'f' ";
		if ($bulan == 1)
			$sql.= " AND ((bulan <= '$bulan_sebelumnya' AND tahun ='$tahun') OR (bulan<='12' AND tahun<'$tahun')) ";
		else
			$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";

		// end 14-02-2012
		$query3	= $this->db->query($sql);
		
		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
			//$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'stok-opname-quilting/vmainform';
			$this->load->view('template',$data);
		}
		else {
			// cek apakah sudah ada data SO di bulan setelahnya
			$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_quilting WHERE bulan > '$bulan' AND tahun = '$tahun' ");
			if ($query3->num_rows() > 0){
				$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
				//$data['list_gudang'] = $this->mmaster->get_gudang();
				$data['isi'] = 'stok-opname-quilting/vmainform';
				$this->load->view('template',$data);
			}
			else {
				$data['nama_bulan'] = $nama_bln;
				$data['bulan'] = $bulan;
				$data['tahun'] = $tahun;

				$cek_data = $this->mmaster->cek_so($bulan, $tahun); 
				//print_r($cek_data); die();
				
				if ($cek_data['idnya'] == '' ) { 
					// jika data so blm ada, maka ambil stok terkini dari tabel tm_brg_hasil_makloon
					$data['query'] = $this->mmaster->get_all_stok();
				
					$data['is_new'] = '1';
					if (is_array($data['query']) )
						$data['jum_total'] = count($data['query']);
					else
						$data['jum_total'] = 0;
					
					$data['isi'] = 'stok-opname-quilting/vformview';
					$this->load->view('template',$data);
				}
				else { // jika sudah diapprove maka munculkan msg
					if ($cek_data['status_approve'] == 't') {
						$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
						//$data['list_gudang'] = $this->mmaster->get_gudang();
						$data['isi'] = 'stok-opname-quilting/vmainform';
						$this->load->view('template',$data);
					}
					else {
						// get data dari tabel tt_stok_opname_ yg statusnya 'f'
						$data['query'] = $this->mmaster->get_all_stok_opname($bulan, $tahun);
				
						$data['is_new'] = '0';
						$data['jum_total'] = count($data['query']);
						$data['isi'] = 'stok-opname-quilting/vformview';
						$this->load->view('template',$data);
						
					}
				} // end else
			}
		} //end if SO di 1 bulan sebelumnya
  }
  
  function submit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $no = $this->input->post('no', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d"); 
	  
	  $submit2 = $this->input->post('submit2', TRUE);
	  $submit3 = $this->input->post('submit3', TRUE);
	  
	  if ($is_new == '1') {
	      // insert ke tabel tt_stok_opname_hasil_quilting
	      $data_header = array(
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
		  $this->db->insert('tt_stok_opname_hasil_quilting',$data_header);
		  
		  // ambil data terakhir di tabel tt_stok_opname_hasil_quilting
		 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_quilting ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id; 
	      
	      for ($i=1;$i<=$no;$i++)
		  {
			 $this->mmaster->save($is_new, $id_stok, $this->input->post('kode_'.$i, TRUE), 
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE));
		  }
		  
		  redirect('stok-opname-quilting/cform');
	  }
	  else {
		  if ($submit2 != '') {
			  // ambil data terakhir di tabel tt_stok_opname_hasil_quilting
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_quilting WHERE
							bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 $this->db->query(" delete from tt_stok_opname_hasil_quilting_detail where id_stok_opname_hasil_quilting = '$id_stok' ");
			 $this->db->query(" delete from tt_stok_opname_hasil_quilting where id = '$id_stok' ");
			 redirect('stok-opname-quilting/cform');
		  }
		  // 14-02-2012
		  else if ($submit3 != '') {
			  // ambil data id dari tabel tt_stok_opname_hasil_quilting
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_quilting WHERE bulan = '$bulan' 
									AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			  for ($i=1;$i<=$no;$i++) {
				 if ($this->input->post('cek_'.$i, TRUE) == 'y') {
					 //echo $this->input->post('cek_'.$i, TRUE)." ".$this->input->post('kode_'.$i, TRUE)."<br>";
					 $this->db->query(" delete from tt_stok_opname_hasil_quilting_detail 
								WHERE id_stok_opname_hasil_quilting = '$id_stok' 
								AND kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
				 }
			  }
			  
			 redirect('stok-opname-quilting/cform');
		  } // end 14-02-2012
		  else {
			  // update ke tabel tt_stok_opname_hasil_quilting
			   // ambil data terakhir di tabel tt_stok_opname_hasil_quilting
				 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_quilting 
											WHERE bulan = '$bulan' AND tahun = '$tahun' ");
				 $hasilrow = $query2->row();
				 $id_stok	= $hasilrow->id; 
				 
				 $this->db->query(" UPDATE tt_stok_opname_hasil_quilting SET tgl_update = '$tgl' 
							where id = '$id_stok' ");
				 
			  for ($i=1;$i<=$no;$i++)
			  {
				 $this->mmaster->save($is_new, $id_stok, $this->input->post('kode_'.$i, TRUE), 
				 $this->input->post('stok_'.$i, TRUE),
				 $this->input->post('stok_fisik_'.$i, TRUE));
			  }
			  
			  redirect('stok-opname-quilting/cform');
		  }
      }
  }
}
