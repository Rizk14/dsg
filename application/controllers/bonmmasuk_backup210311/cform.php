<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmmasuk/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$gudang = $this->input->post('gudang', TRUE);  
	$kode_supplier = $this->input->post('kode_supplier', TRUE);  
	$no_sj = $this->input->post('no_sj', TRUE);  
	$id_sj = $this->input->post('id_sj', TRUE);  

	$proses_submit = $this->input->post('submit', TRUE); 
	
	$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
		if ($id_sj !='') {
			$data['brg_detail'] = $this->mmaster->get_detail_brg($id_sj, $gudang, $kode_supplier);
			$data['msg'] = '';
			$data['id_sj'] = $id_sj;
			$data['no_sj'] = $no_sj;
			$data['gudang'] = $gudang;
			//$data['supplier'] = $this->mmaster->get_supplier();
		}
		else {
			$data['msg'] = 'Nomor SJ Pembelian harus dipilih';
			$data['id_sj'] = '';
			$data['no_sj'] = '';
			$data['gudang'] = '';
			$data['supplier'] = $this->mmaster->get_supplier();
		}
		$data['go_proses'] = '1';
		
		// nama supplier
		$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$kode_supplier' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) 
			$nama_supplier	= $hasilrow->nama;
		
		// nama gudang
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
						WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) {
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
			$nama_lokasi	= $hasilrow->nama_lokasi;
		}
		
		// tgl sj
		$query2	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE 
									kode_supplier = '$kode_supplier' AND no_sj = '$no_sj' ");
		$hasilrow = $query2->row();
		$tgl_sj	= $hasilrow->tgl_sj;
		
		$pisah1 = explode("-", $tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
			// generate no Bon M
			$query3	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_pembelian ORDER BY no_bonm DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==12) {
				$nobonm = substr($no_bonm, 3, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm; //
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			}
			$nomorbonm = "BM-".$nomorbonm;
			$data['no_bonm'] = $nomorbonm;
			$data['nama_supplier'] = $nama_supplier;
			$data['kode_supplier'] = $kode_supplier;
			$data['kode_gudang'] = $kode_gudang;
			$data['nama_gudang'] = $nama_gudang;
			$data['nama_lokasi'] = $nama_lokasi;
			$data['tgl_sj'] = $tgl_sj;
	}
	else {
		$data['msg'] = '';
		$data['id_sj'] = '';
		$data['no_sj'] = '';
		$data['go_proses'] = '';
		$data['supplier'] = $this->mmaster->get_supplier();
		$data['list_gudang'] = $this->mmaster->get_gudang();
	}
	$data['isi'] = 'bonmmasuk/vmainform';
	$this->load->view('template',$data);

  }
  
  function edit(){
	$id_apply_stok 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_bonm($id_apply_stok);
	$data['msg'] = '';
	$data['id_apply_stok'] = $id_apply_stok;

	$data['isi'] = 'bonmmasuk/veditform';
	$this->load->view('template',$data);

  }
    
  function show_popup_sj(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	// sj pembelian
	
	$id_gudang 	= $this->uri->segment(4);
	$csupplier 	= $this->uri->segment(5);
	if ($csupplier == '')
		$csupplier 	= $this->input->post('csupplier', TRUE);  
	if ($id_gudang == '')
		$id_gudang 	= $this->input->post('id_gudang', TRUE);  
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jum_total = $this->mmaster->get_sjtanpalimit($csupplier, $id_gudang, $keywordcari);
							$config['base_url'] = base_url()."index.php/bonmmasuk/cform/show_popup_sj/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_sj($config['per_page'],$this->uri->segment(5), $csupplier, $id_gudang, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$data['csupplier'] = $csupplier;
	$data['id_gudang'] = $id_gudang;

	$this->load->view('bonmmasuk/vpopupsj',$data);

  }

  function submit(){

			$no_sj 	= $this->input->post('no_sj', TRUE);
			$id_sj 	= $this->input->post('id_sj', TRUE);
			$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$kode_supplier 	= $this->input->post('kode_supplier', TRUE);
			$gudang 	= $this->input->post('gudang', TRUE);
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			
			// jika edit, var ini ada isinya
			$id_bonm 	= $this->input->post('id_apply_stok', TRUE);
			
			if ($id_bonm == '') {
				$cek_data = $this->mmaster->cek_data($no_bonm, $gudang);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'bonmmasuk/vmainform';
					$data['msg'] = "Data no Bon M ".$no_bonm." sudah ada..!";
					$data['id_sj'] = '';
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						if ($this->input->post('cek_'.$i, TRUE) == 'y') {
							$this->mmaster->save($no_bonm, $tgl_bonm, $id_sj, $no_sj, $kode_supplier, $gudang,
									$this->input->post('id_detail_'.$i, TRUE),
									$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE) );
						}
					}
					redirect('bonmmasuk/cform/view');
				}
			} // end if id_bonm == ''
			else { // update
				$tgl = date("Y-m-d");
				$this->db->query(" UPDATE tm_apply_stok_pembelian SET tgl_bonm = '$tgl_bonm', tgl_update = '$tgl'
								where id= '$id_bonm' "); //
				$this->db->query(" DELETE FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$id_bonm' ");
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						// jika cek = 'y' dan cek != cek_lama, update status_stok, dan update stoknya
						if (($this->input->post('cek_'.$i, TRUE) == 'y') && ($this->input->post('cek_lama_'.$i, TRUE) != $this->input->post('cek_'.$i, TRUE)) ) {
							$data_detail = array(
								'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
								'qty'=>$this->input->post('qty_'.$i, TRUE),
								'id_apply_stok'=>$id_bonm
							);
							$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
							
						/*	$data_harga = array(
								'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
								'kode_supplier'=>$kode_supplier,
								'harga'=>$this->input->post('harga_'.$i, TRUE),
								'tgl_input'=>$tgl
							);
							$this->db->insert('tt_harga', $data_harga);
							
							// update harga di tabel harga_brg_supplier
							$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '$harga', tgl_update='$tgl'
												where kode_brg= '$kode' AND kode_supplier = '$kode_supplier' "); */
						 
						//insert data brg masuk ke tabel history stok, dan tambah stok di tabel tm_stok
							
							//cek stok terakhir tm_stok, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok 
							WHERE kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
									'stok'=>$new_stok,
								//	'id_gudang'=>$id_gudang, //
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."' ");
							}
							
							$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
							VALUES ('".$this->input->post('kode_'.$i, TRUE)."','$no_bonm', '".$this->input->post('qty_'.$i, TRUE)."', 
							'$new_stok', '$tgl', '".$this->input->post('harga_'.$i, TRUE)."' ) ");
							
							// update status_stok
							$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 't' 
							WHERE id= '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							
							// cek lagi status_stok di tabel detail, jika sudah 't' semua, maka di tabel tm_pembelian ubah juga jadi 't'
							$query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE status_stok = 'f' 
													AND id_pembelian = '$id_sj' ");
							if ($query3->num_rows() == 0){
								$this->db->query(" UPDATE tm_pembelian SET status_stok = 't' WHERE id= '$id_sj' ");
							}
						} // end if cek = 'y'
						
						// ======================
						// jika cek = 'y' dan cek != cek_lama, update status_stok, dan update stoknya
						if (($this->input->post('cek_'.$i, TRUE) == 't') && ($this->input->post('cek_lama_'.$i, TRUE) != $this->input->post('cek_'.$i, TRUE)) ) {
							// update status_stok = f
							$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 'f' 
							WHERE id= '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							
							
						// stoknya edit lagi
						// edit transaksi stoknya utk bon M
							//1. cek stok brg berdasarkan no_bukti di tt_stok
							$query3	= $this->db->query(" SELECT id, masuk FROM tt_stok WHERE no_bukti = '$no_bonm' 
											AND kode_brg='".$this->input->post('kode_'.$i, TRUE)."' ORDER BY id DESC LIMIT 1 ");
							$hasilrow = $query3->row();
							$masuk	= $hasilrow->masuk;
							
							//2. ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok 
											WHERE kode_brg='".$this->input->post('kode_'.$i, TRUE)."' ");
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$qty_baru = $this->input->post('qty_'.$i, TRUE);
							$stokreset = $stok_lama-$masuk;
							//$stokskrg = ($stok_lama-$masuk) + $qty_baru;
							
							//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
							$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga)
							VALUES ('".$this->input->post('kode_'.$i, TRUE)."', '$no_bonm', '$masuk', '$stokreset', '$tgl',
							'".$this->input->post('harga_'.$i, TRUE)."')  ");
							
								//5. update stok di tm_stok
							$this->db->query(" UPDATE tm_stok SET stok = '$stokreset', tgl_update_stok = '$tgl'
												where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."'  ");
						}
						
						//=======================

					}
					redirect('bonmmasuk/cform/view');
			}

  }
  
  function view(){
    $data['isi'] = 'bonmmasuk/vformview';
    $keywordcari = '';
    $csupplier = '0';
	//$kode_bagian = '';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);				
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk/cform/view/index/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'bonmmasuk/vformview';
	$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }


/*  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('retur-beli/cform/view');
  } */
  
}
