<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('pp-budjeting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_jenis_barang'] = $this->mmaster->get_jenis_barang(); 
	$data['list_kelompok_barang'] = $this->mmaster->get_kelompok_barang();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'pp-budjeting/vmainform';
	$this->load->view('template',$data);
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);
	$kel_brg = $this->input->post('kel_brg', TRUE);  
	$jns_brg = $this->input->post('jns_brg', TRUE); 
	// 12-05-2015
	$jenis = $this->input->post('jenis', TRUE);
	// 07-12-2015
	//~ $kalkulasi_ulang_so = $this->input->post('kalkulasi_ulang_so', TRUE);  
	//$html_data .= $kalkulasi_ulang_so; die();
	//~ $format_harga = $this->input->post('format_harga', TRUE);  

	if ($jenis == '1') {
		$data['query'] = $this->mmaster->get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		$data['isi'] = 'pp-budjeting/vformview';
		$data['nama_jenis'] = 'Qty saja (tanpa harga)';
	}
	elseif($jenis == '2') {
		$data['query'] = $this->mmaster->get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		$data['isi'] = 'pp-budjeting/vformviewhargasup';
		$data['nama_jenis'] = 'Lengkap ((dengan harga)Berdasarkan Harga OP)';
	}
	
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	// 12-05-2015
	$data['jenis'] = $jenis;
	//~ $data['format_harga'] = $format_harga;
	//~ $data['kalkulasi_ulang_so'] = $kalkulasi_ulang_so;
	
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
									FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
	}
	else {
		$kode_gudang	= '';
		$nama_gudang	= '';
		$nama_lokasi	= '';
	}
	
	if ($kel_brg != '0') {
		$query6	= $this->db->query(" SELECT kode_perkiraan, nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		if ($query6->num_rows() > 0){
			$hasilrow6 = $query6->row();
			$kode_perkiraan	= $hasilrow6->kode_perkiraan;
			$nama_kelompok	= $hasilrow6->nama;
		}
		else {
			$nama_kelompok = '';
			$kode_perkiraan = '';
		}
	}
	else {
		$nama_kelompok = 'Semua';
			$kode_perkiraan = '';
	}
	
	if ($jns_brg != '0') {
		$query8	= $this->db->query(" SELECT kode, nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		if ($query8->num_rows() > 0){
			$hasilrow8 = $query8->row();
			$kode_jenis_brg	= $hasilrow8->kode;
			$nama_jenis_brg	= $hasilrow8->nama;
		}
		else {
			$nama_jenis_brg = '';
			$kode_jenis_brg = '';
		}
	}
	else {
		$kode_jenis_brg = '';
		$nama_jenis_brg = "Semua";
	}
	
	$data['kode_jenis_brg'] = $kode_jenis_brg;
	$data['nama_jenis_brg'] = $nama_jenis_brg;
	$data['kode_perkiraan'] = $kode_perkiraan;
	$data['nama_kelompok'] = $nama_kelompok;
	
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['jns_brg'] = $jns_brg;
	$data['kelompok'] = $kel_brg;
	$this->load->view('template',$data);
  } 
  function exportexcel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$date_from = $this->input->post('date_from', TRUE);
			$date_to = $this->input->post('date_to', TRUE);  
			$gudang = $this->input->post('gudang', TRUE);
			$kel_brg = $this->input->post('kel_brg', TRUE);  
			$jns_brg = $this->input->post('jns_brg', TRUE);
			$export_excel1 = $this->input->post('export_excel', TRUE);  
			$export_ods = $this->input->post('jns_brg', TRUE);
				
		$query = $this->mmaster->get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='14' align='center'>DATA PP Budgeting</th>
		 </tr>
		 <tr>	
			<th colspan='14' align='center'>Dari Tanggal $date_from s/d $date_to  </th>	
		 </tr>
		<tr >
	<th>Nama Supplier</th>	
	 <th>Nomor PP</th>
		 <th>Tgl PP</th>
		
		 <th>List Barang</th>
		 <th>Qty PP</th>
		  <th>Nomor OP</th>
		 <th>Tgl OP</th>
		 <th>Qty Pemenuhan OP</th>
		 <th>Nomor SJ</th>
		  <th>Tgl SJ</th>
		 <th>Qty Pemenuhan SJ</th>
		  <th>Jeda PP ke OP (Hari) </th>
		 <th>Jeda PP ke SJ (Hari) </th>
		  <th>Jeda OP ke SJ (Hari) </th>
	 </tr>
		</thead>
		<tbody>";
		
			if(is_array($query)){

					$hitung=count($query)-1;
				 for($j=0;$j<=$hitung;$j++){
					 $html_data .= "<tr>";
					$html_data .= "<td nowrap>".$query[$j]['nama_supplier']."</td>"; 
				 $html_data .= "<td nowrap>".$query[$j]['no_pp']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['tgl_pp']."</td>";
				 
				 $html_data .= "<td nowrap>".$query[$j]['nama_brg']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['qty_pp']."</td>";
				
				  $html_data .= "<td nowrap>".$query[$j]['no_op']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['tgl_op']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['qty_op']."</td>";
				 //~ $html_data .= "<td nowrap>".$query[$j]['harga_op']."</td>";
				 $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
					for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['no_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				 $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['tgl_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				  $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['qty_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				$html_data .= "<td nowrap>".$query[$j]['jeda_pp_op']."</td>";
				  $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['jeda_pp_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				   $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['jeda_op_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				 $html_data .= "</tr>";
				}
			}	
				
			
			

		 $html_data.= "</tbody>
		</table>";

		$nama_file = "pp-budjeting";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  //~ function exportexcel() {
	    //~ $is_logged_in = $this->session->userdata('is_logged_in');
		//~ if (!isset($is_logged_in) || $is_logged_in!= true) {
			//~ //$this->load->view('loginform', $data);
			//~ redirect('loginform');
		//~ }
		//~ 
			//~ $date_from = $this->input->post('date_from', TRUE);
			//~ $date_to = $this->input->post('date_to', TRUE);  
			//~ $gudang = $this->input->post('gudang', TRUE);
			//~ $kel_brg = $this->input->post('kel_brg', TRUE);  
			//~ $jns_brg = $this->input->post('jns_brg', TRUE);
			//~ $export_excel1 = $this->input->post('export_excel', TRUE);  
			//~ $export_ods = $this->input->post('jns_brg', TRUE);
				//~ 
		//~ $query = $this->mmaster->get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		//~ 
		//~ $html_data = "
		//~ <table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		//~ <thead>
		 //~ <tr>
			//~ <th colspan='6' align='center'>DATA PP Budgeting</th>
		 //~ </tr>
		 //~ <tr>	
			//~ <th colspan='6' align='center'>Dari Tanggal $date_from s/d $date_to  </th>	
		 //~ </tr>
		//~ <tr >
		  //~ <th>List Barang</th>
		 //~ <th>Qty</th>
		 //~ <th>Qty Pemenuhan OP</th>
		 //~ <th>Qty Sisa</th>
		 //~ <th>Qty Pemenuhan SJ</th>
		 //~ <th>Qty Sisa</th>
	 //~ </tr>
		//~ </thead>
		//~ <tbody>";
		//~ 
			//~ if (is_array($query)) {
			 //~ for($j=0;$j<count($query);$j++) {
				 //~ 
			 	  //~ $html_data .=  "<tr class=\"record\">";
					//~ $html_data .= "<td nowrap>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data .= $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				  //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data .= $var_detail[$k]['qty'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				 //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data .= number_format($var_detail[$k]['jum_op'],2,'.','');
						  //~ if ($var_detail[$k]['count_op'] > 1)
							 //~ $html_data .= " (".$var_detail[$k]['count_op']." OP)";
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				 //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						//~ $html_data .= number_format($var_detail[$k]['sisanya'],2,'.','');
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				  //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
					//~ $var_detail_op = array();
					 //~ $var_detail_op = $var_detail[$k]['op_detail'];
					 //~ $hitung_op = count($var_detail_op);
					 //~ for($l=0;$l<count($var_detail_op); $l++){
						 //~ $html_data .= number_format($var_detail_op[$l]['pemenuhan'],2,'.','');
						  //~ if ($l<$hitung-1)
							 //~ $html_data .= "<br> ";
					 //~ }
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
					//~ $var_detail_op = array();
					 //~ $var_detail_op = $var_detail[$k]['op_detail'];
					 //~ $hitung_op = count($var_detail_op);
					 //~ for($l=0;$l<count($var_detail_op); $l++){
						 //~ $html_data .= number_format($var_detail_op[$l]['sisa'],2,'.','');
						  //~ if ($l<$hitung-1)
							 //~ $html_data .= "<br> ";
					 //~ }
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
//~ 
				//~ $html_data .= "</tr>";
					//~ }
		   //~ } 
			//~ 
//~ 
		 //~ $html_data.= "</tbody>
		//~ </table>";
//~ 
		//~ $nama_file = "pp-budjeting";
		//~ if ($export_excel1 != '')
			//~ $nama_file.= ".xls";
		//~ else
			//~ $nama_file.= ".ods";
		//~ $data = $html_data;
//~ 
		//~ $dir=getcwd();
		//~ include($dir."/application/libraries/generateExcelFile.php");
		//~ return true;
  //~ }
  //~ function exportexcelharga() {
	    //~ $is_logged_in = $this->session->userdata('is_logged_in');
		//~ if (!isset($is_logged_in) || $is_logged_in!= true) {
			//~ //$this->load->view('loginform', $data);
			//~ redirect('loginform');
		//~ }
		//~ 
			//~ $date_from = $this->input->post('date_from', TRUE);
			//~ $date_to = $this->input->post('date_to', TRUE);  
			//~ $gudang = $this->input->post('gudang', TRUE);
			//~ $kel_brg = $this->input->post('kel_brg', TRUE);  
			//~ $jns_brg = $this->input->post('jns_brg', TRUE);
			//~ $export_excel1 = $this->input->post('export_excel', TRUE);  
			//~ $export_ods = $this->input->post('jns_brg', TRUE);
				//~ 
		//~ $query = $this->mmaster->get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		//~ 
		//~ $html_data = "
		//~ <table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		//~ <thead>
		 //~ <tr>
			//~ <th colspan='11' align='center'>DATA PP Budjeting Harga</th>
		 //~ </tr>
		 //~ <tr>	
			//~ <th colspan='11' align='center'>Dari Tanggal $date_from s/d $date_to  </th>	
		 //~ </tr>
		//~ <tr >
		 //~ <th>List Barang</th>
		 //~ <th>Qty PP</th>
		 //~ <th>Harga PP (Berdasarkan harga OP)</th>
		 //~ <th>Qty Pemenuhan OP</th>
		 //~ <th>Harga Pemenuhan OP</th>
		 //~ <th>Qty Sisa PP </th>
		 //~ <th>Harga Sisa PP</th>
		 //~ <th>Qty Pemenuhan SJ</th>
		 //~ <th>Harga Pemenuhan SJ</th>
		 //~ <th>Qty Sisa OP</th>
		 //~ <th>Harga Sisa OP</th>
	 //~ </tr>
		//~ </thead>
		//~ <tbody>";
		//~ 
			//~ if (is_array($query)) {
			 //~ for($j=0;$j<count($query);$j++) {
				 //~ 
			 	  //~ $html_data .=  "<tr class=\"record\">";
					//~ $html_data .= "<td nowrap>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data .= $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				  //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data .= $var_detail[$k]['qty'];
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				  //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data .= number_format($var_detail[$k]['hargaqty'],2,',','.');
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				 //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						  //~ $html_data .= number_format($var_detail[$k]['jum_op'],2,'.','');
						  //~ if ($var_detail[$k]['count_op'] > 1)
							 //~ $html_data .= " (".$var_detail[$k]['count_op']." OP)";
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				  //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						//~ $html_data .= number_format($var_detail[$k]['hargajum_op'],2,',','.');
					//~ 
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				 //~ 
				 //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						//~ $html_data .= number_format($var_detail[$k]['sisanya'],2,'.','');
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				  //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
						//~ $html_data .= number_format($var_detail[$k]['hargasisanya'],2,',','.');
						//~ 
						  //~ if ($k<$hitung-1)
						     //~ $html_data .= "<br> ";
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				  //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
					//~ $var_detail_op = array();
					 //~ $var_detail_op = $var_detail[$k]['op_detail'];
					 //~ $hitung_op = count($var_detail_op);
					 //~ for($l=0;$l<count($var_detail_op); $l++){
						 //~ $html_data .= number_format($var_detail_op[$l]['pemenuhan'],2,'.','');
						  //~ if ($l<$hitung-1)
							 //~ $html_data .= "<br> ";
					 //~ }
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				   //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
					//~ $var_detail_op = array();
					 //~ $var_detail_op = $var_detail[$k]['op_detail'];
					 //~ $hitung_op = count($var_detail_op);
					 //~ for($l=0;$l<count($var_detail_op); $l++){
						 //~ $html_data .= number_format($var_detail_op[$l]['hargapemenuhan'],2,',','.');
						  //~ if ($l<$hitung-1)
							 //~ $html_data .= "<br> ";
					 //~ }
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				 //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
					//~ $var_detail_op = array();
					 //~ $var_detail_op = $var_detail[$k]['op_detail'];
					 //~ $hitung_op = count($var_detail_op);
					 //~ for($l=0;$l<count($var_detail_op); $l++){
						 //~ $html_data .= number_format($var_detail_op[$l]['sisa'],2,',','');
						  //~ if ($l<$hitung-1)
							 //~ $html_data .= "<br> ";
					 //~ }
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
				 //~ 
				//~ 
				 //~ $html_data .= "<td nowrap align='right'>";
				 //~ if (is_array($query[$j]['detail_pp'])) {
					 //~ $var_detail = array();
					 //~ $var_detail = $query[$j]['detail_pp'];
					 //~ $hitung = count($var_detail);
					//~ for($k=0;$k<count($var_detail); $k++){
					//~ $var_detail_op = array();
					 //~ $var_detail_op = $var_detail[$k]['op_detail'];
					 //~ $hitung_op = count($var_detail_op);
					 //~ for($l=0;$l<count($var_detail_op); $l++){
						 //~ $html_data .= number_format($var_detail_op[$l]['hargasisa'],2,',','.');
						  //~ if ($l<$hitung-1)
							 //~ $html_data .= "<br> ";
					 //~ }
					//~ }
				 //~ }
				 //~ $html_data .= "</td>";
//~ 
				//~ $html_data .= "</tr>";
					//~ }
		   //~ } 
			//~ 
//~ 
		 //~ $html_data.= "</tbody>
		//~ </table>";
//~ 
		//~ $nama_file = "pp-budjeting-harga";
		//~ if ($export_excel1 != '')
			//~ $nama_file.= ".xls";
		//~ else
			//~ $nama_file.= ".ods";
		//~ $data = $html_data;
//~ 
		//~ $dir=getcwd();
		//~ include($dir."/application/libraries/generateExcelFile.php");
		//~ return true;
  //~ }
    function exportexcelharga() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$date_from = $this->input->post('date_from', TRUE);
			$date_to = $this->input->post('date_to', TRUE);  
			$gudang = $this->input->post('gudang', TRUE);
			$kel_brg = $this->input->post('kel_brg', TRUE);  
			$jns_brg = $this->input->post('jns_brg', TRUE);
			$export_excel1 = $this->input->post('export_excel', TRUE);  
			$export_ods = $this->input->post('jns_brg', TRUE);
				
		$query = $this->mmaster->get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='16' align='center'>DATA PP Budgeting</th>
		 </tr>
		 <tr>	
			<th colspan='16' align='center'>Dari Tanggal $date_from s/d $date_to  </th>	
		 </tr>
		<tr >
	<th>Nama Supplier</th>	
	 <th>Nomor PP</th>
		 <th>Tgl PP</th>
		
		 <th>List Barang</th>
		 <th>Qty PP</th>
		  <th>Nomor OP</th>
		 <th>Tgl OP</th>
		 <th>Qty Pemenuhan OP</th>
		 <th>Harga Pemenuhan OP</th>
		 <th>Nomor SJ</th>
		  <th>Tgl SJ</th>
		 <th>Qty Pemenuhan SJ</th>
		 <th>Harga Pemenuhan SJ</th>
		  <th>Jeda PP ke OP (Hari) </th>
		 <th>Jeda PP ke SJ (Hari) </th>
		  <th>Jeda OP ke SJ (Hari) </th>
	 </tr>
		</thead>
		<tbody>";
		
			if(is_array($query)){

					$hitung=count($query)-1;
				 for($j=0;$j<=$hitung;$j++){
					 $html_data .= "<tr>";
			 $html_data .= "<td nowrap>".$query[$j]['nama_supplier']."</td>";		 
				 $html_data .= "<td nowrap>".$query[$j]['no_pp']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['tgl_pp']."</td>";
				
				 $html_data .= "<td nowrap>".$query[$j]['nama_brg']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['qty_pp']."</td>";
				
				  $html_data .= "<td nowrap>".$query[$j]['no_op']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['tgl_op']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['qty_op']."</td>";
				 $html_data .= "<td nowrap>".$query[$j]['harga_op']."</td>";
				 $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
					for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['no_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				 $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['tgl_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				  $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['qty_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				  $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data .= $var_detail[$k]['harga_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				$html_data .= "<td nowrap>".$query[$j]['jeda_pp_op']."</td>";
				  $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['jeda_pp_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				   $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  $html_data .= $var_detail[$k]['jeda_op_sj'];
						  if ($k<$hitung_det-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				 $html_data .= "</tr>";
				}
			}	
				
			
			

		 $html_data.= "</tbody>
		</table>";

		$nama_file = "pp-budjeting";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
}
