<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('pembelian_wip/mmaster');
  }

  function saldoawalhutang(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['isi'] = 'pembelian_wip/vformsaldoawalhutang';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$this->load->view('template',$data);

  }
  
  function viewsaldoawalhutang(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	// Di fitur input SO bahan baku/pembantu, misalnya mau input SO bln skrg. jika ada data SO di 1 bulan sebelumnya 
	// yg blm diapprove, maka tidak boleh input SO di bulan skrg.
		
	// =================== 19-02-2014, ga perlu pengecekan SO di bulan sebelum & sesudah ============================
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			
			$cek_data = $this->mmaster->cek_saldoawal($bulan, $tahun); 
			
			if ($cek_data['idnya'] == '' ) { 
				// jika data so blm ada, maka hitung hutang dagangnya utk bln tsb. Koreksi 19-02-2014: sementara ga perlu hitung transaksi2 sebelumnya. kosong aja
				$data['query'] = $this->mmaster->get_all_transaksi_pembelian_kredit($bulan, $tahun);
			
				$data['is_new'] = '1';
				if (is_array($data['query']) )
					$data['jum_total'] = count($data['query']);
				else
					$data['jum_total'] = 0;
				
				$data['isi'] = 'pembelian_wip/vviewsaldoawalhutang';
				$this->load->view('template',$data);
			}
			else {
					// get data dari tabel tt_stok_opname_hutang_dagang
					$data['query'] = $this->mmaster->get_all_saldoawal($bulan, $tahun);
			
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					$data['isi'] = 'pembelian_wip/vviewsaldoawalhutang';
					$this->load->view('template',$data);
			} // end else
	
	// ===========================================================================================

  }
  
  function submitsaldoawalhutang() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $no = $this->input->post('no', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  $submit2 = $this->input->post('submit2', TRUE);
	  $submit3 = $this->input->post('submit3', TRUE);
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  if ($is_new == '1') {
	    // insert ke tabel tt_stok_opname_hutang_dagang
	    $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_hutang_dagang ORDER BY id DESC LIMIT 1 ");
		if($seq->num_rows() > 0) {
			$seqrow	= $seq->row();
			$idbaru	= $seqrow->id+1;
		}else{
			$idbaru	= 1;
		}
	      
	      $data_header = array(
			  'id'=>$idbaru,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
		  $this->db->insert('tt_stok_opname_hutang_dagang',$data_header);
		  	      
	      for ($i=1;$i<=$no;$i++)
		  {
			 $this->mmaster->savesaldoawalhutang($is_new, $idbaru, $this->input->post('id_supplier_'.$i, TRUE), 
			 $this->input->post('stok_fisik_'.$i, TRUE));
		  }
		  
		  //redirect('stok-opname/cform');
		  $data['msg'] = "Data saldo awal untuk bulan ".$nama_bln." ".$tahun." berhasil disimpan";
		  $data['isi'] = 'pembelian_wip/vformsaldoawalhutang';
		  $this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') {
			  // ambil data id dari tabel tt_stok_opname_hutang_dagang
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hutang_dagang WHERE 
							bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 $this->db->query(" delete from tt_stok_opname_hutang_dagang_detail where id_stok_opname_hutang_dagang = '$id_stok' ");
			 $this->db->query(" delete from tt_stok_opname_hutang_dagang where id = '$id_stok' ");
			 $data['msg'] = "Data saldo awal untuk bulan ".$nama_bln." ".$tahun." berhasil dihapus";
			 $data['isi'] = 'pembelian_wip/vformsaldoawalhutang';
			 $this->load->view('template',$data);
			 //redirect('stok-opname/cform');
		  }
		  else if ($submit3 != '') {
			  // ambil data id dari tabel tt_stok_opname_hutang_dagang
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hutang_dagang WHERE 
							bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 $kodenya = "";
			  for ($i=1;$i<=$no;$i++) {
				 if ($this->input->post('cek_'.$i, TRUE) == 'y') {
					 //echo "masuk delete per item</br>";
					 //echo $this->input->post('cek_'.$i, TRUE);
					 $this->db->query(" delete from tt_stok_opname_hutang_dagang_detail 
								WHERE id_stok_opname_hutang_dagang = '$id_stok' 
								AND id_supplier = '".$this->input->post('id_supplier_'.$i, TRUE)."' ");
					$kodenya.=$this->input->post('kode_supplier_'.$i, TRUE)." ";
				 }
			  }
			 //redirect('stok-opname/cform');
			 $data['msg'] = "Data saldo awal untuk bulan ".$nama_bln." ".$tahun." untuk supplier ".$kodenya." berhasil dihapus";
			 $data['isi'] = 'pembelian_wip/vformsaldoawalhutang';
			 $this->load->view('template',$data);
		  }
		  else {
			  // update ke tabel tt_stok_opname_hutang_dagang
			   // ambil data terakhir di tabel tt_stok_opname_hutang_dagang
				 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hutang_dagang WHERE 
								bulan = '$bulan' AND tahun = '$tahun' ");
				 $hasilrow = $query2->row();
				 $id_stok	= $hasilrow->id; 
				 
				 $this->db->query(" UPDATE tt_stok_opname_hutang_dagang SET tgl_update = '$tgl' 
							where id = '$id_stok' ");
				$kodenya = "";
			  for ($i=1;$i<=$no;$i++)
			  {
				 $this->mmaster->savesaldoawalhutang($is_new, $id_stok, $this->input->post('id_supplier_'.$i, TRUE), 
				 $this->input->post('stok_fisik_'.$i, TRUE));
			  }
			  
			 // redirect('stok-opname/cform');
			 $data['msg'] = "Data saldo awal untuk bulan ".$nama_bln." ".$tahun." berhasil diupdate";
			 $data['isi'] = 'pembelian_wip/vformsaldoawalhutang';
			 $this->load->view('template',$data);
		  } // end if $submit2
      }
  }
  
  // 05-08-2015
  function opnamehutang(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'pembelian_wip/vmainformopnamehutang';
	$data['msg'] = '';
	$data['supplier'] = $this->mmaster->get_supplier();
	$this->load->view('template',$data);
  }
  
  function submitopnamehutang(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
			$id_supplier 	= $this->input->post('id_supplier', TRUE);
			$isretur 	= $this->input->post('isretur', TRUE);
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			/*$cek_data = $this->mmaster->cek_data_manual($no_bonm, $tgl_bonm, $id_gudang);
			if (count($cek_data) > 0) {
				$data['isi'] = 'bonmmasuk/vmainformmanual';
				$data['msg'] = "Data no bon M ".$no_bonm." sudah ada..!";
				
				$data['list_gudang'] = $this->mmaster->get_gudang();
				$this->load->view('template',$data);
			}
			else { */
				$tgl = date("Y-m-d H:i:s");
				
				// ambil pkp dan tipe pajak dari tabel supplier
				$query2	= $this->db->query(" SELECT pkp, tipe_pajak FROM tm_supplier WHERE 
								id = '$id_supplier' ");
				 $hasilrow = $query2->row();
				 $pkp	= $hasilrow->pkp; 
				 $tipe_pajak	= $hasilrow->tipe_pajak; 
				
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->saveopnamehutang($id_supplier, $isretur, $pkp, $tipe_pajak,
							$this->input->post('tgl_bukti_'.$i, TRUE),$this->input->post('no_bukti_'.$i, TRUE), 
								$this->input->post('jumlah_'.$i, TRUE) );						
				}
				//redirect('bonmmasuk/cform/viewmanual');
				$data['isi'] = 'pembelian_wip/vsimpanopnamesukses';
				$data['msg'] = "Data berhasil disimpan";
				$this->load->view('template',$data);
			//}
  }

}
?>
