<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			
			$data['page_title_voucher']			= $this->lang->line('page_title_voucher');
			$data['form_title_detail_voucher']	= $this->lang->line('form_title_detail_voucher');
			$data['list_voucher_no_kontrabon']	= $this->lang->line('list_voucher_no_kontrabon');
			$data['list_voucher_tgl_kontrabon']	= $this->lang->line('list_voucher_tgl_kontrabon');
			$data['list_voucher_total_kontrabon']	= $this->lang->line('list_voucher_total_kontrabon');
			$data['list_voucher_nilai_voucher_detail']	= $this->lang->line('list_voucher_nilai_voucher_detail');
			$data['list_voucher_tgl_faktur']	= $this->lang->line('list_voucher_tgl_faktur');
			$data['list_voucher_kd_sumber']		= $this->lang->line('list_voucher_kd_sumber');
			$data['list_voucher_no']			= $this->lang->line('list_voucher_no');
			$data['list_voucher_tgl']			= $this->lang->line('list_voucher_tgl');
			$data['list_voucher_kd_perusahaan']	= $this->lang->line('list_voucher_kd_perusahaan');
			$data['list_voucher_deskripsi']		= $this->lang->line('list_voucher_deskripsi');
			$data['list_voucher_total']			= $this->lang->line('list_voucher_total');
			$data['list_voucher_received']		= $this->lang->line('list_voucher_received');
			$data['list_voucher_approved']		= $this->lang->line('list_voucher_approved');
			$data['list_voucher_approved_dept']	= $this->lang->line('list_voucher_approved_dept');
			$data['list_voucher_approved_serv']	= $this->lang->line('list_voucher_approved_serv');
			$data['list_voucher_manual_input']	= $this->lang->line('list_voucher_manual_input');
			$data['list_voucher_piutang']	= $this->lang->line('list_voucher_piutang');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['disabled']	= 'f';
			$data['limages']	= base_url();
			
			$this->load->model('approvevoucher/mclass');

			$data['isi']	= 'approvevoucher/vmainform';	
			$this->load->view('template',$data);	
			
	}
	
	function carilistvoucher() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_voucher']			= $this->lang->line('page_title_voucher');
		$data['form_title_detail_voucher']	= $this->lang->line('form_title_detail_voucher');
		$data['list_voucher_no_kontrabon']	= $this->lang->line('list_voucher_no_kontrabon');
		$data['list_voucher_tgl_kontrabon']	= $this->lang->line('list_voucher_tgl_kontrabon');
		$data['list_voucher_total_kontrabon']	= $this->lang->line('list_voucher_total_kontrabon');
		$data['list_voucher_nilai_voucher_detail']	= $this->lang->line('list_voucher_nilai_voucher_detail');
		$data['list_voucher_tgl_faktur']	= $this->lang->line('list_voucher_tgl_faktur');
		$data['list_voucher_kd_sumber']		= $this->lang->line('list_voucher_kd_sumber');
		$data['list_voucher_no']			= $this->lang->line('list_voucher_no');
		$data['list_voucher_tgl']			= $this->lang->line('list_voucher_tgl');
		$data['list_voucher_kd_perusahaan']	= $this->lang->line('list_voucher_kd_perusahaan');
		$data['list_voucher_deskripsi']		= $this->lang->line('list_voucher_deskripsi');
		$data['list_voucher_total']			= $this->lang->line('list_voucher_total');
		$data['list_voucher_received']		= $this->lang->line('list_voucher_received');
		$data['list_voucher_approved']		= $this->lang->line('list_voucher_approved');
		$data['list_voucher_approved_dept']	= $this->lang->line('list_voucher_approved_dept');
		$data['list_voucher_approved_serv']	= $this->lang->line('list_voucher_approved_serv');
		$data['list_voucher_manual_input']	= $this->lang->line('list_voucher_manual_input');
		$data['list_voucher_piutang']		= $this->lang->line('list_voucher_piutang');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lvoucher']	= "";		
		$data['limages']	= base_url();
		
		$no_voucher	= $this->input->post('no_voucher');
		$kode_sumber = $this->input->post('kode_sumber');
		$i_voucher = $this->input->post('i_voucher');
		
		$dvoucherfirst = $this->input->post('d_voucher_first');
		$dvoucherlast = $this->input->post('d_voucher_last');
		
		$data['tglvouchermulai']	= (!empty($dvoucherfirst) || $dvoucherfirst!='0')?$dvoucherfirst:'';
		$data['tglvoucherakhir']	= (!empty($dvoucherlast) || $dvoucherlast!='0')?$dvoucherlast:'';
		$data['no_voucher']	= (!empty($no_voucher))?$no_voucher:'';
		$data['kode_sumber']	= (!empty($kode_sumber))?$kode_sumber:'';
		$data['i_voucher']	= (!empty($i_voucher))?$i_voucher:'';
		
		$e_d_voucher_first= explode("/",$dvoucherfirst,strlen($dvoucherfirst));
		$e_d_voucher_last	= explode("/",$dvoucherlast,strlen($dvoucherlast));
		
		$ndvoucherfirst	= !empty($e_d_voucher_first[2])?$e_d_voucher_first[2].'-'.$e_d_voucher_first[1].'-'.$e_d_voucher_first[0]:'0';
		$ndvoucherlast	= !empty($e_d_voucher_last[2])?$e_d_voucher_last[2].'-'.$e_d_voucher_last[1].'-'.$e_d_voucher_last[0]:'0';
		
		$turi1	= ($i_voucher!='' && $i_voucher!='0')?$i_voucher:'0';
		$turi2	= ($no_voucher!='' && $no_voucher!='0')?$no_voucher:'0';
		$turi3	= ($kode_sumber!='' && $kode_sumber!='0')?$kode_sumber:'0';
		$turi4	= $ndvoucherfirst;
		$turi5	= $ndvoucherlast;
		
		$this->load->model('approvevoucher/mclass');
		
		$pagination['base_url'] 	= '/approvevoucher/cform/carilistvouchernext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		$qlistvoucher	= $this->mclass->clistvoucher($i_voucher,$no_voucher,$ndvoucherfirst,$ndvoucherlast);
		
		$pagination['total_rows']	= $qlistvoucher->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->clistvoucherperpages($pagination['per_page'],$pagination['cur_page'],$i_voucher,$no_voucher,$ndvoucherfirst,$ndvoucherlast);
		$data['isi']	= 'approvevoucher/vlistform';	
			$this->load->view('template',$data);	
		
		
	}

	function carilistvouchernext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_voucher']			= $this->lang->line('page_title_voucher');
		$data['form_title_detail_voucher']	= $this->lang->line('form_title_detail_voucher');
		$data['list_voucher_no_kontrabon']	= $this->lang->line('list_voucher_no_kontrabon');
		$data['list_voucher_tgl_kontrabon']	= $this->lang->line('list_voucher_tgl_kontrabon');
		$data['list_voucher_total_kontrabon']	= $this->lang->line('list_voucher_total_kontrabon');
		$data['list_voucher_nilai_voucher_detail']	= $this->lang->line('list_voucher_nilai_voucher_detail');
		$data['list_voucher_tgl_faktur']	= $this->lang->line('list_voucher_tgl_faktur');
		$data['list_voucher_kd_sumber']		= $this->lang->line('list_voucher_kd_sumber');
		$data['list_voucher_no']			= $this->lang->line('list_voucher_no');
		$data['list_voucher_tgl']			= $this->lang->line('list_voucher_tgl');
		$data['list_voucher_kd_perusahaan']	= $this->lang->line('list_voucher_kd_perusahaan');
		$data['list_voucher_deskripsi']		= $this->lang->line('list_voucher_deskripsi');
		$data['list_voucher_total']			= $this->lang->line('list_voucher_total');
		$data['list_voucher_received']		= $this->lang->line('list_voucher_received');
		$data['list_voucher_approved']		= $this->lang->line('list_voucher_approved');
		$data['list_voucher_approved_dept']	= $this->lang->line('list_voucher_approved_dept');
		$data['list_voucher_approved_serv']	= $this->lang->line('list_voucher_approved_serv');
		$data['list_voucher_manual_input']	= $this->lang->line('list_voucher_manual_input');
		$data['list_voucher_piutang']	= $this->lang->line('list_voucher_piutang');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lvoucher']	= "";
		$data['limages']	= base_url();
		
		$i_voucher		= $this->uri->segment(4);
		$no_voucher		= $this->uri->segment(5);
		$kode_sumber	= $this->uri->segment(6);
		$ndvoucherfirst	= $this->uri->segment(7);
		$ndvoucherlast	= $this->uri->segment(8);
				
		$e_d_voucher_first	= ($ndvoucherfirst!='0')?explode("-",$ndvoucherfirst,strlen($ndvoucherfirst)):'';
		$e_d_voucher_last	= ($ndvoucherlast!='0')?explode("-",$ndvoucherlast,strlen($ndvoucherlast)):'';

		$ndvoucherfirst	= !empty($e_d_voucher_first[2])?$e_d_voucher_first[2].'/'.$e_d_voucher_first[1].'/'.$e_d_voucher_first[0]:'0';
		$ndvoucherlast	= !empty($e_d_voucher_last[2])?$e_d_voucher_last[2].'/'.$e_d_voucher_last[1].'/'.$e_d_voucher_last[0]:'0';
						
		$data['tglvouchermulai']	= (!empty($nddofirst) && $nddofirst!='0')?$nddofirst:'';
		$data['tglvoucherakhir']	= (!empty($nddolast) && $nddolast!='0')?$nddolast:'';
		$data['novoucher']	= (!empty($novoucher) || $novoucher!='0')?$novoucher:'';
		
		$turi1	= ($i_voucher!='' || $i_voucher!='0')?$i_voucher:'0';
		$turi2	= ($no_voucher!='' || $no_voucher!='0')?$no_voucher:'0';
		$turi3	= ($kode_sumber!='' || $kode_sumber!='0')?$kode_sumber:'0';
		$turi4	= $ndvoucherfirst;
		$turi5	= $ndvoucherlast;
 		
		$this->load->model('approvevoucher/mclass');
		
		$pagination['base_url'] 	= '/approvevoucher/cform/carilistvouchernext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/';
		
		$qlistvoucher	= $this->mclass->clistvoucher($i_voucher,$no_voucher,$ndvoucherfirst,$ndvoucherlast);
		
		$pagination['total_rows']	= $qlistvoucher->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->clistvoucherperpages($pagination['per_page'],$pagination['cur_page'],$i_voucher,$no_voucher,$ndvoucherfirst,$ndvoucherlast);
		
		$this->load->view('approvevoucher/vlistform',$data);	
	}
	
	function listvoucher() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "VOUCHER";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('approvevoucher/mclass');

		$query	= $this->mclass->lvoucher();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/approvevoucher/cform/listvouchernext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lvoucherperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('approvevoucher/vlistformvoucher',$data);			
	}

	function listvouchernext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "VOUCHER";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('approvevoucher/mclass');

		$query	= $this->mclass->lvoucher();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/approvevoucher/cform/listvouchernext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lvoucherperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('approvevoucher/vlistformvoucher',$data);			
	}		

	function flistvoucher() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key');

		$data['page_title']	= "VOUCHER";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('approvevoucher/mclass');

		$query	= $this->mclass->flvoucher($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				$ikontrabon	= trim($row->idtcode);
					
				$list .= "
				 <tr>
				  <td>".$cc."</td>

				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	 
	function edit() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_voucher']			= $this->lang->line('page_title_voucher');
			$data['form_title_detail_voucher']	= $this->lang->line('form_title_detail_voucher');
			$data['list_voucher_no_kontrabon']	= $this->lang->line('list_voucher_no_kontrabon');
			$data['list_voucher_tgl_kontrabon']	= $this->lang->line('list_voucher_tgl_kontrabon');
			$data['list_voucher_total_kontrabon']	= $this->lang->line('list_voucher_total_kontrabon');
			$data['list_voucher_nilai_voucher_detail']	= $this->lang->line('list_voucher_nilai_voucher_detail');
			$data['list_voucher_tgl_faktur']	= $this->lang->line('list_voucher_tgl_faktur');
			$data['list_voucher_kd_sumber']		= $this->lang->line('list_voucher_kd_sumber');
			$data['list_voucher_no']			= $this->lang->line('list_voucher_no');
			$data['list_voucher_tgl']			= $this->lang->line('list_voucher_tgl');
			$data['list_voucher_kd_perusahaan']	= $this->lang->line('list_voucher_kd_perusahaan');
			$data['list_voucher_deskripsi']		= $this->lang->line('list_voucher_deskripsi');
			$data['list_voucher_total']			= $this->lang->line('list_voucher_total');
			$data['list_voucher_received']		= $this->lang->line('list_voucher_received');
			$data['list_voucher_approved']		= $this->lang->line('list_voucher_approved');
			$data['list_voucher_approved_dept']	= $this->lang->line('list_voucher_approved_dept');
			$data['list_voucher_approved_serv']	= $this->lang->line('list_voucher_approved_serv');
			$data['list_voucher_manual_input']	= $this->lang->line('list_voucher_manual_input');
			$data['list_voucher_piutang']	= $this->lang->line('list_voucher_piutang');
			
			$data['button_approve']	= $this->lang->line('button_approve');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['disabled']	= 'f';
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			
			$i_voucher	= $this->uri->segment(4);
			$i_voucher_no = $this->uri->segment(5);
			
			$data['i_voucher']	= $i_voucher;
			$data['i_voucher_no']	= $i_voucher_no;
			
			$this->load->model('approvevoucher/mclass');
			
			$qVOUCHER	= $this->mclass->getvoucherheader($i_voucher,$i_voucher_no);
			
			if($qVOUCHER->num_rows()>0) {
				
				$rVOUCHER	= $qVOUCHER->row();
				
				$data['i_voucher_code'] = $rVOUCHER->i_voucher_code;
				$exp_d_voucher 			= explode('-',$rVOUCHER->d_voucher,strlen($rVOUCHER->d_voucher)); // YYYY-mm-dd
				$data['d_voucher'] 		= $exp_d_voucher[2].'/'.$exp_d_voucher[1].'/'.$exp_d_voucher[0];
				$data['i_company_code'] = $rVOUCHER->i_company_code;
				$data['e_recieved'] 	= $rVOUCHER->e_recieved;
				$data['e_approved'] 	= $rVOUCHER->e_approved;
				$data['v_total_voucher']= $rVOUCHER->v_total_voucher;
				$data['f_nilai_manual'] = $rVOUCHER->f_nilai_manual;
				$data['f_approve_dept'] = $rVOUCHER->f_approve_dept;
				$data['e_description'] 	= $rVOUCHER->e_description;
				$data['kodesumber'] 	= $rVOUCHER->kodesumber;
				$data['f_approved'] 	= $rVOUCHER->f_approved;
				
				$wh		= 0;			
				$iter	= 0;
				$totalpiutang = 0;
				$totalbayar = 0;
				
				$list	= '';
				
				$qvoucheritem = $this->mclass->lvoucheritem($i_voucher);
				$nvoucheritem = $qvoucheritem->num_rows();
				
				$result = $qvoucheritem->result();
				
				foreach($result as $row){
					
					$disabled = $row->f_nilai_manual=='t'?'':'disabled';
					
					$exp_d_dt = explode("-",$row->d_dt,strlen($row->d_dt)); // YYYY-mm-dd
					
					$list	.= "
						<tr>
							<td><div style=\"font:24px;text-align:right;width:13px;margin-right:0px;\">".($iter*1+1)."</div></td>
							
							<td>
							<DIV ID=\"ajax_i_kontrabon_code_tblItem_".$iter."\" style=\"width:85px;\">
							<input type=\"text\" ID=\"i_kontrabon_code_tblItem_".$iter."\" name=\"i_kontrabon_code_tblItem_".$iter."\" style=\"width:83px;\" value=\"".trim($row->i_dt_code)."\" readonly >
							<input type=\"hidden\" ID=\"i_kontrabon_tblItem_".$iter."\" name=\"i_kontrabon_tblItem_".$iter."\" value=\"".$row->i_dt."\">
							</DIV>
							</td>
							
							<td>
							<DIV ID=\"ajax_tgl_kontrabon_tblItem_".$iter."\" style=\"width:85px;\" >
							<input type=\"text\" ID=\"tgl_kontrabon_tblItem_".$iter."\" name=\"tgl_kontrabon_tblItem_".$iter."\" style=\"width:83px;text-align:left;\" value=\"".$exp_d_dt[2]."/".$exp_d_dt[1]."/".$exp_d_dt[0]."\" readonly >
							<input type=\"hidden\" name=\"tgl_kontrabonhidden_tblItem_".$iter."\" id=\"tgl_kontrabonhidden_tblItem_".$iter."\" value=\"".$row->d_dt."\">
							</DIV>
							</td>
									
							<td><DIV ID=\"ajax_total_kontrabon_tblItem_".$iter."\" style=\"width:130px;\" >
							<input type=\"text\" ID=\"total_kontrabon_tblItem_".$iter."\" name=\"total_kontrabon_tblItem_".$iter."\" style=\"width:128px;text-align:right;\" value=\"".number_format($row->v_total_grand,'2','.',',')."\" readonly >
							<input type=\"hidden\" ID=\"total_kontrabonhidden_tblItem_".$iter."\" name=\"total_kontrabonhidden_tblItem_".$iter."\" value=\"".$row->v_total_grand."\" ></DIV></td>

							<td><DIV ID=\"ajax_piutang_tblItem_".$iter."\" style=\"width:130px;\" >
							<input type=\"text\" ID=\"piutang_tblItem_".$iter."\" name=\"piutang_tblItem_".$iter."\" style=\"width:128px;text-align:right;\" value=\"".number_format($row->v_grand_sisa,'2','.',',')."\" readonly >
							<input type=\"hidden\" ID=\"piutanghidden_tblItem_".$iter."\" name=\"piutanghidden_tblItem_".$iter."\" value=\"".$row->v_grand_sisa."\"></DIV></td>
							
							<td><DIV ID=\"ajax_nilai_voucher_tblItem_".$iter."\" style=\"width:145px;text-align:right;\" >
							<input type=\"text\" ID=\"nilai_voucher_tblItem_".$iter."\" name=\"nilai_voucher_tblItem_".$iter."\" value=\"".number_format($row->v_voucher,'2','.',',')."\" style=\"width:143px;text-align:right;\" ".$disabled." onkeyup=\"hitung(".$nvoucheritem.",".$iter.");\">
							<input type=\"hidden\" ID=\"nilai_voucherhidden_tblItem_".$iter."\" name=\"nilai_voucherhidden_tblItem_".$iter."\" value=\"".$row->v_voucher."\" >
							</DIV>
							<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\">
							<input type=\"hidden\" id=\"f_nota_sederhana".$iter."\" name=\"f_nota_sederhana".$iter."\" value=\"".$row->f_nota_sederhana."\" >
							</td>
						</tr>";
					
					$totalpiutang = $totalpiutang+$row->v_grand_sisa;
					$totalbayar = $totalbayar+$row->v_voucher;
					$iter+=1;	
					$wh+=1;
					
				}
				
				$list	.= "<input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"".($iter-1)."\">";
				$list	.= "<input type=\"hidden\" id=\"totalpiutang\" name=\"totalpiutang\" value=\"".$totalpiutang."\">";
				$list	.= "<input type=\"hidden\" id=\"totalbayar\" name=\"totalbayar\" value=\"".$totalbayar."\">";
							
			} else {
				$list = '';
			}
			
			$data['isi']	= $list;
			
			$this->load->view('approvevoucher/veditform',$data);
			
	}	
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_voucher		= $this->input->post('i_voucher');
		$ckapprovehidden	= $this->input->post('ckapprovehidden');
		
		$kode_sumber	= $this->input->post('kode_sumber');
		$no_voucher		= $this->input->post('no_voucher');
		
		$this->load->model('approvevoucher/mclass');
		
		if(!empty($i_voucher)
		) {
			$this->mclass->mupdate($i_voucher,$ckapprovehidden,$kode_sumber,$no_voucher);
		} else {
			$data['page_title_voucher']			= $this->lang->line('page_title_voucher');
			$data['form_title_detail_voucher']	= $this->lang->line('form_title_detail_voucher');
			$data['list_voucher_no_kontrabon']	= $this->lang->line('list_voucher_no_kontrabon');
			$data['list_voucher_tgl_kontrabon']	= $this->lang->line('list_voucher_tgl_kontrabon');
			$data['list_voucher_total_kontrabon']	= $this->lang->line('list_voucher_total_kontrabon');
			$data['list_voucher_nilai_voucher_detail']	= $this->lang->line('list_voucher_nilai_voucher_detail');
			$data['list_voucher_tgl_faktur']	= $this->lang->line('list_voucher_tgl_faktur');
			$data['list_voucher_kd_sumber']		= $this->lang->line('list_voucher_kd_sumber');
			$data['list_voucher_no']			= $this->lang->line('list_voucher_no');
			$data['list_voucher_tgl']			= $this->lang->line('list_voucher_tgl');
			$data['list_voucher_kd_perusahaan']	= $this->lang->line('list_voucher_kd_perusahaan');
			$data['list_voucher_deskripsi']		= $this->lang->line('list_voucher_deskripsi');
			$data['list_voucher_total']			= $this->lang->line('list_voucher_total');
			$data['list_voucher_received']		= $this->lang->line('list_voucher_received');
			$data['list_voucher_approved']		= $this->lang->line('list_voucher_approved');
			$data['list_voucher_approved_dept']	= $this->lang->line('list_voucher_approved_dept');
			$data['list_voucher_approved_serv']	= $this->lang->line('list_voucher_approved_serv');
			$data['list_voucher_manual_input']	= $this->lang->line('list_voucher_manual_input');
			$data['list_voucher_piutang']		= $this->lang->line('list_voucher_piutang');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['disabled']	= 'f';
			$data['limages']	= base_url();

			$this->load->model('approvevoucher/mclass');

			print "<script>alert(\"Maaf, voucher gagal diapprove. Terimakasih.\");show(\"approvevoucher/cform\",\"#content\");</script>";				
			
			$this->load->view('approvevoucher/vmainform',$data);			
		}	
	}
}
?>
