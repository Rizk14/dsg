<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('users-admin-user/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->uid;
			$eusername = $row->username;
			$epasswd = $row->passwd;
			$enama = $row->nama;
			$egid = $row->gid;
			
			$query3	= $this->db->query(" SELECT nama_group FROM tm_groups 
			WHERE gid = '$egid' ");
			$hasilrow = $query3->row();
			$enama_group	= $hasilrow->nama_group;
		}
	}
	else {
			$eid = '';
			$eusername = '';
			$epasswd = '';
			$enama = '';
			$egid = '';
			$enama_group = '';
			$edit = '';
	}
	
	$data['eid'] = $eid;
	$data['eusername'] = $eusername;
	$data['epasswd'] = $epasswd;
	$data['enama'] = $enama;
	$data['egid'] = $egid;
	$data['enama_group'] = $enama_group;
	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	$data['list_group'] = $this->mmaster->get_group_user();
    $data['isi'] = 'users-admin-user/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');

		} */
			
			$uid_user 	= $this->input->post('uid_user', TRUE);
			$username 	= $this->input->post('username', TRUE);
			$passwd 	= $this->input->post('passwd', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			$group_user 	= $this->input->post('group_user', TRUE);
						
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data($username);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'users-admin-user/vmainform';
					$data['msg'] = "Data user ".$username." sudah ada..!";
										
					$eid = '';
					$eusername = '';
					$epasswd = '';
					$enama = '';
					$egid = '';
					$edit = '';
					
					$data['eid'] = $eid;
					$data['eusername'] = $eusername;
					$data['epasswd'] = $epasswd;
					$data['enama'] = $enama;
					$data['egid'] = $egid;
					$data['edit'] = $edit;
					
					$data['list_group'] = $this->mmaster->get_group_user();
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($username,$passwd, $nama, $group_user, $uid_user, $goedit);
					redirect('users-admin-user/cform/view');
				}
			} // end if goedit == ''
			else {
				$this->mmaster->save($username,$passwd, $nama, $group_user, $uid_user, $goedit);
				redirect('users-admin-user/cform/view');
			}
			
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'users-admin-user/vformview';
    $keywordcari = "all";
    $groupnya = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $groupnya);	
							$config['base_url'] = base_url().'index.php/users-admin-user/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $groupnya);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_group'] = $this->mmaster->get_group_user();
	$data['egroupnya'] = $groupnya;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$groupnya 	= $this->input->post('group_user', TRUE);  
	
	if ($keywordcari == '' && $groupnya == '') {
		$groupnya 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($groupnya == '')
		$groupnya = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $groupnya);
							$config['base_url'] = base_url().'index.php/users-admin-user/cform/cari/index/'.$groupnya.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $groupnya);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'users-admin-user/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_group'] = $this->mmaster->get_group_user();
	$data['egroupnya'] = $groupnya;
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('users-admin-user/cform/view');
  }
  
  
}
