<?php
class informasi_mutasi_makloon_baju extends CI_Controller
{
    public $data = array(
        'halaman' => 'informasi_mutasi_makloon_baju',        
        'title' => 'Informasi Mutasi Makloon Baju',
        'isi' => 'informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju_form'
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju_model', 'informasi_mutasi_makloon_baju');
    }

   
    public function index()
    {
		$no=$this->input->post('no');
		
        $this->data['values'] = (object) $this->informasi_mutasi_makloon_baju->default_values;
        $this->data['unit_jahit'] =  $this->informasi_mutasi_makloon_baju->get_unit_jahit();
        $this->data['unit_packing'] =  $this->informasi_mutasi_makloon_baju->get_unit_packing();
        $this->data['gudang'] =  $this->informasi_mutasi_makloon_baju->get_gudang();
		$this->load->view('template', $this->data);
			
    }
    
 public function view($offset=null)
    {
		$no=$this->input->post('no');
		$tanggal_sj_dari = $this->input->post('tanggal_sj_dari',TRUE);
		$tanggal_sj_ke = $this->input->post('tanggal_sj_ke',TRUE);
		$unit_jahit = $this->input->post('unit_jahit',TRUE);
		$unit_packing = $this->input->post('unit_packing',TRUE);
		$gudang = $this->input->post('gudang',TRUE);
		
		$informasi_mutasi_makloon_baju = $this->informasi_mutasi_makloon_baju->get_all_inner_paged($tanggal_sj_dari,$tanggal_sj_ke,$unit_jahit,$unit_packing,$gudang);
	
		if ($informasi_mutasi_makloon_baju[0] != null) {
			 $this->data['informasi_mutasi_makloon_baju'] = $informasi_mutasi_makloon_baju; 
		if($unit_jahit != 0){
            $this->data['isi'] = 'informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju_list'; 
			}
		elseif($unit_packing != 0){
            $this->data['isi'] = 'informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju_list_packing'; 
			}	
		elseif($gudang != 0){
            $this->data['isi'] = 'informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju_list_gudang'; 
			}	
        } else {
            $this->data['informasi_mutasi_makloon_baju'] = 'Tidak ada data Makloon Baju Gudang Jadi Wip, Silahkan Melakukan '.anchor('/informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju/view', 'Pencarian kembali.', 'class="alert-link"');
			if($unit_jahit != 0){
            $this->data['isi'] = 'informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju_list'; 
			}
		elseif($unit_packing != 0){
            $this->data['isi'] = 'informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju_list_packing'; 
			}	
		elseif($gudang != 0){
            $this->data['isi'] = 'informasi_mutasi_makloon_baju/informasi_mutasi_makloon_baju_list_gudang'; 
			}
        }
		$this->data['tanggal_sj_dari'] = $tanggal_sj_dari;
		$this->data['tanggal_sj_ke'] = $tanggal_sj_ke;
		$this->data['unit_jahit'] = $unit_jahit;
		$this->data['unit_packing'] = $unit_packing;
		$this->data['gudang'] = $gudang;
		
		$this->load->view('template', $this->data);		
    }
     
}

