<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('sj-keluar-packing/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
	  }
	$data['isi'] = 'sj-keluar-packing/vmainform';
	$data['msg'] = '';
	$data['list_unit_packing']	= $this->mmaster->lunitpacking();
	$this->load->view('template',$data);
  }

  function view(){ 
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $data['isi'] = 'sj-keluar-packing/vformview';
    $keywordcari = "all";
    $unit_packing = '0';
	
   $jum_total = $this->mmaster->getAlltanpalimit($unit_packing, $keywordcari); 
							$config['base_url'] = base_url().'index.php/sj-keluar-packing/cform/view/index/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $unit_packing, $keywordcari);		
	$data['jum_total'] = count($jum_total);
	
	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		$data['unit_packing'] 	= $unit_packing;
	
	$data['list_unit_packing']	= $this->mmaster->lunitpacking();
	$this->load->view('template',$data);
  }
  
  function cari(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$keywordcari	= $this->input->post('cari', TRUE);  
	$unit_packing	= $this->input->post('unit_packing', TRUE);  
	
	if ($keywordcari=='' && $unit_packing=='') {
		$unit_packing 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari=='')
		$keywordcari	= "all";

	if ($unit_packing=='')
		$unit_packing = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($unit_packing, $keywordcari);
							$config['base_url'] = base_url().'index.php/sj-keluar-packing/cform/cari/'.$unit_packing.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $unit_packing, $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'sj-keluar-packing/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_unit_packing']	= $this->mmaster->lunitpacking();
	$data['unit_packing'] = $unit_packing;
	$this->load->view('template',$data);
  }

  function delete(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $id_sj 	= $this->uri->segment(4);
    $this->mmaster->delete($id_sj);
    redirect('sj-keluar-packing/cform/view');
  }

  function generate_nomor() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);
  }
      
  function edit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$id_sj 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_sj($id_sj);
	$data['list_unit_packing']	= $this->mmaster->lunitpacking();
	$data['msg'] = '';

	$data['isi'] = 'sj-keluar-packing/veditform';
	$this->load->view('template',$data);
  }

  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj = $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$unit_packing = $this->input->post('unit_packing', TRUE);  			
			$ket = $this->input->post('ket', TRUE);   
						
			$id_sj_packing 	= $this->input->post('id_sj_packing', TRUE);
			
			if ($id_sj_packing=='') { // insert
				$cek_data = $this->mmaster->cek_data($no_sj);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'sj-keluar-packing/vmainform';
					$data['msg'] = "Data no SJ ".$no_sj." sudah ada..!";
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$this->mmaster->save($no_sj,$tgl_sj, $unit_packing, $ket, $this->input->post('kode_'.$i, TRUE), $this->input->post('nama_'.$i, TRUE), $this->input->post('qty_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE), $this->input->post('qty_nyumput_'.$i, TRUE), $this->input->post('gudang_'.$i, TRUE), $this->input->post('idhsljahit_'.$i, TRUE));
					}
					redirect('sj-keluar-packing/cform/view');
				}
			} 
			else {
				$tgl = date("Y-m-d");
				
					$jumlah_input=$no-1;
					
					$this->db->query(" UPDATE tm_sj_proses_packing SET tgl_sj='$tgl_sj', keterangan='$ket', tgl_update = '$tgl'
									where id='$id_sj_packing' ");
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$kodenya	= $this->input->post('kode_'.$i, TRUE);
						$qty 		= $this->input->post('qty_'.$i, TRUE);
						$qty_lama	= $this->input->post('qty_lama_'.$i, TRUE);
						$keterangan	= $this->input->post('keterangan_'.$i, TRUE);
						$gudang		= $this->input->post('gudang_'.$i, TRUE);
						$idhsljahit	= $this->input->post('idhsljahit_'.$i, TRUE);
						
						if (($qty != $qty_lama) && ($qty!='' || $qty!=0)) {
							
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$kodenya' AND id='$idhsljahit' ");
							if ($query3->num_rows()==0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = ($stok_lama+$qty_lama)-$qty;
							$fpacking	= ($new_stok==0 || $new_stok<0)?'t':'f';
							
							$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$new_stok', tgl_update_stok='$tgl', f_packing='$fpacking' WHERE kode_brg_jadi='$kodenya' AND id='$idhsljahit' ");
																
							/*$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='".$kodenya."' AND id='$idhsljahit' ");
							$hasilrow = $query3->row();
							$stok = $hasilrow->stok; // ini stok terkini di tm_stok
							*/

							$this->db->query(" UPDATE tm_sj_proses_packing_detail SET qty='$qty' WHERE id='".$this->input->post('id_'.$i, TRUE)."' ");
						}
						else {
							$this->db->query(" UPDATE tm_sj_proses_packing_detail SET qty='".$this->input->post('qty_'.$i, TRUE)."' WHERE id='".$this->input->post('id_'.$i, TRUE)."' ");
						}
					}
					redirect('sj-keluar-packing/cform/view');
			}

  }
  
  function show_popup_brg(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$posisi 	= $this->uri->segment(4);

	if ($posisi=='') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari=='' && $posisi=='' ) {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari);
		$num	= $qjum_total->num_rows();
		
		$config['base_url'] = base_url()."index.php/sj-keluar-packing/cform/show_popup_brg/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = $num;
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6,0);
							$this->pagination->initialize($config);
							
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari);

	$data['jum_total'] = $num;
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['startnya'] = $config['cur_page'];

	$this->load->view('sj-keluar-packing/vpopupbrg',$data);
  }

}
