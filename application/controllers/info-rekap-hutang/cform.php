<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-rekap-hutang/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		
	$data['isi'] = 'info-rekap-hutang/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $data['isi'] = 'info-rekap-hutang/vformview';
    $jenis_beli = $this->input->post('jenis_beli', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	// 23-07-2015
	$jenisdata = $this->input->post('jenisdata', TRUE);
	
	if ($date_from == '' && $date_to == '') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
	}

    //$jum_total = $this->mmaster->get_all_rekap_pembeliantanpalimit($jenis_beli, $date_from, $date_to, $jenisdata);
						/*	$config['base_url'] = base_url().'index.php/info-pembelian/cform/view/index/'.$date_from.'/'.$date_to.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						*/
	//$data['query'] = $this->mmaster->get_all_pembelian($config['per_page'],$this->uri->segment(7), $jenis_beli, $date_from, $date_to);						
	$data['query'] = $this->mmaster->get_all_rekap_pembelian($jenis_beli, $date_from, $date_to, $jenisdata);
	$data['jum_total'] = count($data['query']);
	//$data['cari'] = $keywordcari;
	//$data['list_supplier'] = $this->mmaster->get_supplier();
	//$data['csupplier'] = $csupplier;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$data['jenisdata'] = $jenisdata;
	if ($jenisdata == '1')
		$data['namajenisdata'] = "Dari SJ";
	else
		$data['namajenisdata'] = "Dari Faktur";
	$this->load->view('template',$data);
  }
  
  // 18-04-2012
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$jenisdata = $this->input->post('jenisdata', TRUE);
		$namajenisdata = $this->input->post('namajenisdata', TRUE);
		$jenis_beli = $this->input->post('jenis_beli', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_rekap_pembelian($jenis_beli, $date_from, $date_to, $jenisdata);
		
		// ======== start html_data =================
		$html_data = "
		<table border='2' cellpadding= '1' cellspacing = '1' width='100%'>
			<thead>
			<tr>
			<th colspan='11' align='center'><font size='3' face='arial'>REKAPITULASI HUTANG DAGANG</font></th>
		 </tr>
		 <tr>
			<th colspan='11' align='center'><font size='3' face='arial'>Periode: $date_from s.d. $date_to</font></th>
		 </tr>
		 <tr>
			<th colspan='11' align='center'><font size='3' face='arial'>Jenis Pengambilan Data: $namajenisdata</font></th>
		 </tr>
	 <tr>
		<th width='5%'><font size='2' face='arial'>No </font></th>
		 <th width='10%'><font size='2' face='arial'>Kode Supplier</font></th>
		 <th width='35%'><font size='2' face='arial'>Nama Supplier</font></th>
		 <th width='15%'><font size='2' face='arial'>Saldo Awal</font></th>
		 <th width='15%'><font size='2' face='arial'>Pembelian<br>Bahan Baku/Pembantu</font></th>
		 <th width='15%'><font size='2' face='arial'>Retur</font></th>
		 <th width='15%'><font size='2' face='arial'>Pembelian<br>Makloon</font></th>
		 <th width='15%'><font size='2' face='arial'>Pelunasan</font></th>
		 <th width='5%'><font size='2' face='arial'>C/n</font></th>
		 <th width='15%'><font size='2' face='arial'>Pembulatan</font></th>
		 <th width='15%'><font size='2' face='arial'>Saldo Akhir</font></th>
	 </tr>
	</thead>
	<tbody> ";
		$nomor = 1;
			if (is_array($query)) {
				$tot_hutang = 0;
				$tot_debet = 0;
				$tot_retur = 0;
				$tot_pembelian = 0; $tot_pembelian_makloon = 0;
				$tot_bulat1 = 0;
				$tot_bulat2 = 0;
				$tot_saldo_akhir = 0;
				
			 for($j=0;$j<count($query);$j++){
				 $tot_hutang+= $query[$j]['tot_hutang'];
				 $tot_debet+= $query[$j]['tot_debet'];
				 $tot_retur+= $query[$j]['tot_retur'];
				 
				 if ($query[$j]['kategori_sup'] == 1)
					$tot_pembelian+= $query[$j]['tot_pembelian'];
				 else
					$tot_pembelian_makloon+= $query[$j]['tot_pembelian'];
				 
				 $tot_saldo_akhir+= $query[$j]['saldo_akhir'];
				 
				 $html_data.= "<tr class=\"record\">
				 <td><font face='arial'>".$nomor."</font></td>
				 <td><font face='arial'>".$query[$j]['kode_supplier']."</font></td>
				 <td><font face='arial'>".$query[$j]['nama_supplier']."</font></td>
				 <td ><font face='arial'>".$query[$j]['tot_hutang']."</font></td>";
				 
				 if ($query[$j]['kategori_sup'] == 1)
					$html_data.="<td ><font face='arial'>".$query[$j]['tot_pembelian']."</font></td>";
				 else
					$html_data.="<td ><font face='arial'>0</font></td>";
					
				 $html_data.="<td ><font face='arial'>".$query[$j]['tot_retur']."</font></td>";
				 
				 /*if ($query[$j]['tot_bulat'] < 0) {
					$html_data.=  "<td ><font face='arial'>".abs($query[$j]['tot_bulat'])."</font></td>";
					$tot_bulat1+= abs($query[$j]['tot_bulat']);
				 }
				 else
					$html_data.=    "<td>&nbsp;</td>"; */
				
				 if ($query[$j]['kategori_sup'] == 2)
					$html_data.="<td ><font face='arial'>".$query[$j]['tot_pembelian']."</font></td>";
				 else
					$html_data.="<td ><font face='arial'>0</font></td>";
					
				 $html_data.=    "<td ><font face='arial'>".$query[$j]['tot_debet']."</font></td>
				 <td>&nbsp;</td>";
				 
				 //if ($query[$j]['tot_bulat'] > 0) {
					$html_data.=    "<td ><font face='arial'>".$query[$j]['tot_bulat']."</font></td>";
					$tot_bulat2+= $query[$j]['tot_bulat'];
				 //}
				 //else
				//	$html_data.=    "<td>&nbsp;</td>";
				 
				 $html_data.=    "<td ><font face='arial'>".$query[$j]['saldo_akhir']."</font></td>";
				 				 				 							 
				 $html_data.=  "</tr>";
				$nomor++;
		 	}
		 	$html_data.= "<tr class=\"record\">";
			
			$html_data.=    "<td colspan='3' align='right'><font face='arial'><b>GRAND TOTAL</b></font></td>
				 <td ><font face='arial'><b>".$tot_hutang."</b></font></td>
				 <td ><font face='arial'><b>".$tot_pembelian."</b></font></td>
				 <td ><font face='arial'><b>".$tot_retur."</b></font></td>
				 <td ><font face='arial'><b>".$tot_pembelian_makloon."</b></font></td>
				 <td ><font face='arial'><b>".$tot_debet."</b></font></td>
				 <td>&nbsp;</td>
				 
				 <td ><font face='arial'><b>".$tot_bulat2."</b></font></td>
				 
				 <td ><font face='arial'><b>".$tot_saldo_akhir."</b></font></td>
		 	
		 	</tr>";
		}
		$html_data.= "</tbody>
				</table><br>";
		//===========================================
		$nama_file = "laporan_rekap_hutang_dagang";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }

}
