<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expoopvsdogudang/mclass');
	}
	
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_opvsdo']	= $this->lang->line('page_title_opvsdo');
			$data['form_title_detail_opvsdo']	= $this->lang->line('form_title_detail_opvsdo');
			$data['list_opvsdo_tgl_mulai_op']	= $this->lang->line('list_opvsdo_tgl_mulai_op');
			$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
			$data['list_opvsdo_kd_brg']	= $this->lang->line('list_opvsdo_kd_brg');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$data['customer']	= $this->mclass->lcustomer();
				$data['isi']	= 'expoopvsdogudang/vmainform';	
			$this->load->view('template',$data);	
			
		
	}
	
	function carilistopvsdo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_opvsdo']	= $this->lang->line('page_title_opvsdo');
		$data['form_title_detail_opvsdo']	= $this->lang->line('form_title_detail_opvsdo');
		$data['list_opvsdo_tgl_mulai_op']	= $this->lang->line('list_opvsdo_tgl_mulai_op');
		$data['list_opvsdo_t_permintaan']	= $this->lang->line('list_opvsdo_t_permintaan');
		$data['list_opvsdo_t_pengiriman']	= $this->lang->line('list_opvsdo_t_pengiriman');
		$data['list_opvsdo_n_permintaan']	= $this->lang->line('list_opvsdo_n_permintaan');
		$data['list_opvsdo_n_penjualan']	= $this->lang->line('list_opvsdo_n_penjualan');
		$data['list_opvsdo_selisih']	= $this->lang->line('list_opvsdo_selisih');
		$data['list_opvsdo_t_n_selisih']	= $this->lang->line('list_opvsdo_t_n_selisih');
		$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
		$data['list_opvsdo_kd_brg']	= $this->lang->line('list_opvsdo_kd_brg');
		$data['list_opvsdo_nm_brg']	= $this->lang->line('list_opvsdo_nm_brg');
		$data['list_opvsdo_unit_price']	= $this->lang->line('list_opvsdo_unit_price');
		$data['list_opvsdo_op']	= $this->lang->line('list_opvsdo_op');
		$data['list_opvsdo_n_op']	= $this->lang->line('list_opvsdo_n_op');
		$data['list_opvsdo_do']	= $this->lang->line('list_opvsdo_do');
		$data['list_opvsdo_n_do']	= $this->lang->line('list_opvsdo_n_do');
		$data['list_opvsdo_selisih_opdo']	= $this->lang->line('list_opvsdo_selisih_opdo');
		$data['list_opvsdo_n_selisih']	= $this->lang->line('list_opvsdo_n_selisih');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
		
		$i_product	= $this->input->post('i_product');
		$iproduct	= !empty($i_product)?$i_product:"kosong";
		$d_op_first	= $this->input->post('d_op_first');
		$d_op_last	= $this->input->post('d_op_last');
		$f_stop_produksi	= $this->input->post('f_stop_produksi');
		$stop	= $this->input->post('stop');
		$stopproduct	= $stop==1?'TRUE':'FALSE';
		$fdropforcast	= $this->input->post('fdropforcast');
		$is_grosir	= $this->input->post('is_grosir');
		
		$icustomer	= $this->input->post('customer');
		
		$data['tglopmulai']	= $d_op_first;
		$data['tglopakhir']	= $d_op_last;
		$data['kproduksi']	= $i_product;
		$data['sproduksi']	= $f_stop_produksi==1?' checked ':'';
		$data['stop']		= $stop==1?' checked ':'';
		$data['stop2']		= $stop==1?'1':'0';
		
		$e_d_do_first	= explode("/",$d_op_first,strlen($d_op_first));
		$e_d_do_last	= explode("/",$d_op_last,strlen($d_op_last));
		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:'0';
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:'0';
		
		$data['iproduct']	= !empty($i_product)?$i_product:"kosong";
		$data['tdofirst']	= $n_d_do_first;
		$data['tdolast']	= $n_d_do_last;
		$data['icustomer']	= $icustomer;
		$data['fdropforcast']	= $fdropforcast;
		$data['is_grosir']	= $is_grosir;
		
		$data['var_iproduct']	= $iproduct;
		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;
		$data['var_stopproduct']= $stop==1?'TRUE':'FALSE';
		
		$data['customer']	= $this->mclass->lcustomer();
		$data['query']	= $this->mclass->clistopvsdo_new($icustomer,$i_product,$n_d_do_first,$n_d_do_last,$stopproduct,$fdropforcast, $is_grosir);
		$data['isixx']	= $this->mclass->clistopvsdo_dokosong($icustomer,$i_product,$n_d_do_first,$n_d_do_last,$stopproduct,$fdropforcast, $is_grosir);
		
		
		$data['isi']	= 'expoopvsdogudang/vlistform';	
			$this->load->view('template',$data);	
	}
	
	
	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/expoopvsdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
	
		$this->load->view('expoopvsdo/vlistformbrgjadi',$data);	
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expoopvsdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('expoopvsdo/vlistformbrgjadi',$data);	
	}	
	
	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		if(!empty($key)) {
			$query	= $this->mclass->flbarangjadi($key);
			$jml	= $query->num_rows();
		}else{
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->iproduct."</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->imotif."</a></td>
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->motifname."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}
	
	function gexportopvsdo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$db2=$this->load->database('db_external', TRUE);
		$data['page_title_opvsdo']	= $this->lang->line('page_title_opvsdo');
		$data['form_title_detail_opvsdo']	= $this->lang->line('form_title_detail_opvsdo');
		$data['list_opvsdo_tgl_mulai_op']	= $this->lang->line('list_opvsdo_tgl_mulai_op');
		$data['list_opvsdo_t_permintaan']	= $this->lang->line('list_opvsdo_t_permintaan');
		$data['list_opvsdo_t_pengiriman']	= $this->lang->line('list_opvsdo_t_pengiriman');
		$data['list_opvsdo_n_permintaan']	= $this->lang->line('list_opvsdo_n_permintaan');
		$data['list_opvsdo_n_penjualan']	= $this->lang->line('list_opvsdo_n_penjualan');
		$data['list_opvsdo_selisih']	= $this->lang->line('list_opvsdo_selisih');
		$data['list_opvsdo_t_n_selisih']	= $this->lang->line('list_opvsdo_t_n_selisih');
		$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
		$data['list_opvsdo_kd_brg']	= $this->lang->line('list_opvsdo_kd_brg');
		$data['list_opvsdo_nm_brg']	= $this->lang->line('list_opvsdo_nm_brg');
		$data['list_opvsdo_unit_price']	= $this->lang->line('list_opvsdo_unit_price');
		$data['list_opvsdo_op']	= $this->lang->line('list_opvsdo_op');
		$data['list_opvsdo_n_op']	= $this->lang->line('list_opvsdo_n_op');
		$data['list_opvsdo_do']	= $this->lang->line('list_opvsdo_do');
		$data['list_opvsdo_n_do']	= $this->lang->line('list_opvsdo_n_do');
		$data['list_opvsdo_selisih_opdo']	= $this->lang->line('list_opvsdo_selisih_opdo');
		$data['list_opvsdo_n_selisih']	= $this->lang->line('list_opvsdo_n_selisih');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$iproduct	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		$sproduct	= $this->uri->segment(7);
		$stop		= $this->uri->segment(8);
		$icustomer	= $this->uri->segment(9);
		if ($icustomer == 0) $icustomer ='';
		$fdropforcast	= $this->uri->segment(10);
		$is_grosir	= $this->uri->segment(11);
		
		$stopproduct	= $stop=='1'?'TRUE':'FALSE';
		
		$extdofirst	= explode("-",$tdofirst,strlen($tdofirst));
		$extdolast	=  explode("-",$tdolast,strlen($tdolast));
		
		$nwdofirst	= (!empty($tdofirst) || $tdofirst!=0)?$extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0]:'0';
		$nwdolast	= (!empty($tdolast) || $tdolast!=0)?$extdolast[2]."/".$extdolast[1]."/".$extdolast[0]:'0';
		
		$periode	= $nwdofirst." s.d ".$nwdolast;
		
		$data['tglopmulai']	= $nwdofirst==0?'':$nwdofirst;
		$data['tglopakhir']	= $nwdolast==0?'':$nwdolast;
		$data['kproduksi']	= $iproduct!='kosong'?$iproduct:'';
		$data['sproduksi']	= $sproduct=='TRUE'?' checked ':'';
		$data['stop']		= $stop==1?' checked ':'';
		$data['icustomer']	= $icustomer;
		$data['fdropforcast']	= $fdropforcast;
		$data['is_grosir']	= $is_grosir;
		
		$data['var_iproduct']	= $iproduct;
		$data['var_ddofirst']	= $tdofirst;
		$data['var_ddolast']	= $tdolast;
		$data['var_stopproduct']= $stopproduct;
		
		$data['customer']	= $this->mclass->lcustomer();
		
		if ($icustomer == 0)
			$icustomer = '';
		if ($iproduct == "kosong")
			$i_product = '';
		
		$qcompany = $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ORDER BY i_initial DESC LIMIT 1 ");
		if($qcompany->num_rows()>0) {
			$rcompany = $qcompany->row();
			$e_initial_name	= $rcompany->e_initial_name;		
		}else{
			$e_initial_name	= "";
		}
		if ($icustomer != '') {
			$qcustomer	= $db2->query(" SELECT e_customer_name FROM tr_customer WHERE i_customer='$icustomer' ORDER BY i_customer DESC LIMIT 1 ");
			if($qcustomer->num_rows()>0) {
				$rcustomer = $qcustomer->row();
				$ecustomername = "Pelanggan : ".strtoupper($rcustomer->e_customer_name);
			}else{
				$ecustomername = "Semua Pelanggan";
			}
		}
		else{
			$ecustomername = "Semua Pelanggan";
		}
			
		$data['query']	= $this->mclass->clistopvsdo_new($icustomer,$i_product,$tdofirst,$tdolast,$stopproduct,$fdropforcast, $is_grosir);
		$data['isixx']	= $this->mclass->clistopvsdo_dokosong($icustomer,$i_product,$tdofirst,$tdolast,$stopproduct,$fdropforcast,$is_grosir);
		
		$ObjPHPExcel = new PHPExcel();
		
		$qexpopvsdo	= $this->mclass->explistopvsdo_new($icustomer,$i_product,$tdofirst,$tdolast,$stopproduct,$fdropforcast, $is_grosir);
		$qexpopvsdoxx	= $this->mclass->explistopvsdo_dokosong($icustomer,$i_product,$tdofirst,$tdolast,$stopproduct,$fdropforcast, $is_grosir);
		$isixx = $qexpopvsdoxx;
	// 29-12-2012, ga perlu pake if disini, langsung ajah
	//	if($qexpopvsdo->num_rows()>0) {
		
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan OP VS DO")
				->setSubject("Laporan OP VS DO")
				->setDescription("Laporan OP VS DO")
				->setKeywords("Laporan Bulanan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:J2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN OP VS DO');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:J3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:J4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', $e_initial_name);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A5:J5');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A5'
			);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A5', $ecustomername);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A7', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B7', 'Kode Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C7', 'Nama Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						),
						'alignment' => array(
						'wrap'      => true
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D7', 'HJP');
			$ObjPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E7', 'OP');
			$ObjPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F7', 'Nilai OP');
			$ObjPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G7', 'DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H7', 'Nilai DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I7', 'Selisih OP DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('J7', 'Nilai Selisih');
			$ObjPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				
			if($this->session->userdata('ses_level')==3 || $this->session->userdata('ses_level')==1) {
				$ObjPHPExcel->getActiveSheet()->setCellValue('K7', 'Qty Total Di Faktur');
				$ObjPHPExcel->getActiveSheet()->getStyle('K7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			}
			
			// if($qexpopvsdo->num_rows()>0) {
				
				$j	= 8;
				$nomor	= 1;

				$totalpermintaan	= 0;
				$nilaipermintaan	= 0;
				$totalpengiriman	= 0;
				$nilaipenjualan	= 0;
				$selisihtotal	= 0;
				$selisihnilai	= 0;
				$totalqtyfaktur	= 0;
			
			// new 29-12-2012, pengecekannya disini
			if($qexpopvsdo->num_rows()>0) {				
				foreach($qexpopvsdo->result() as $row) {
					// ======= my script ========================+++++++
					// 12-07-2012
						// ambil jml OP
						$qjmlopall	= $this->mclass->jumopall($i_product,$tdofirst,$tdolast,$stopproduct,
											$row->imotif,$icustomer,$fdropforcast);
						if($qjmlopall->num_rows()>0) {
							$rjmlopall	= $qjmlopall->row();
							$jmlorderall	= $rjmlopall->jmlorder;
						}
						else
							$jmlorderall = 0;
							
						//$qjmlopgrosir	= $this->mclass->jmlorder_new($i_product,$tdofirst,$tdolast,$stopproduct,
						//					$row->imotif,$icustomer,$fdropforcast, 1);
						/*if($qjmlopgrosir->num_rows()>0) {
							$rjmlopgrosir	= $qjmlopgrosir->row();
							$jmlordergrosir	= $rjmlopgrosir->jmlorder;
						}
						else
							$jmlordergrosir = 0; */
					/*	if (is_array($qjmlopgrosir))
							$jmlordergrosir = $qjmlopgrosir['jmlorder'];
						else */
							$jmlordergrosir = 0;
							
						if ($is_grosir == '2')
							$selisihnya = $jmlorderall-$jmlordergrosir;
						else
							$selisihnya = $jmlordergrosir;
						
						$qjmlorderpemenuhan	= $this->mclass->jmlorder_new($i_product,$tdofirst,$tdolast,$stopproduct,
											$row->imotif,$icustomer,$fdropforcast,$is_grosir);
						/*if($qjmlorderpemenuhan->num_rows()>0) {
							
							$rjmlorderpemenuhan	= $qjmlorderpemenuhan->row();

							$hargaperunit = $row->unitprice;
							
							//$jmlorder	= $rjmlorderpemenuhan->jmlorder;
							if ($is_grosir == '1')
								$jmlorder	= $rjmlorderpemenuhan->jmlorder;
							else
								$jmlorder = $selisihnya;
							$pemenuhan = $rjmlorderpemenuhan->pemenuhan;
							//$nilaiorder	= $rjmlorderpemenuhan->jmlorder*$hargaperunit;
							$nilaiorder	= $jmlorder*$hargaperunit;
							$nilaipemenuhan	= $rjmlorderpemenuhan->pemenuhan*$hargaperunit;
							//$selisihopdo	= $rjmlorderpemenuhan->jmlorder - $rjmlorderpemenuhan->pemenuhan;
							$selisihopdo	= $jmlorder - $rjmlorderpemenuhan->pemenuhan;
							$nilaiselisih	= $selisihopdo*$hargaperunit;
						} */
						if (is_array($qjmlorderpemenuhan)) {
						//	$hargaperunit = $row->unitprice;
							
							//$jmlorder	= $rjmlorderpemenuhan->jmlorder;
							if ($is_grosir == '1')
								$jmlorder	= $qjmlorderpemenuhan['jmlorder'];
							else
								$jmlorder = $selisihnya;
							$pemenuhan = $qjmlorderpemenuhan['pemenuhan'];
							//$nilaiorder	= $rjmlorderpemenuhan->jmlorder*$hargaperunit;
						//	$nilaiorder	= $jmlorder*$hargaperunit;
						//	$nilaipemenuhan	= $qjmlorderpemenuhan['pemenuhan']*$hargaperunit;
							//$selisihopdo	= $rjmlorderpemenuhan->jmlorder - $rjmlorderpemenuhan->pemenuhan;
							$selisihopdo	= $jmlorder - $qjmlorderpemenuhan['pemenuhan'];
						//	$nilaiselisih	= $selisihopdo*$hargaperunit;
						}
						else{
							$jmlorder = $selisihnya;
							$pemenuhan = 0;
						//	$nilaiorder	= $jmlorder*$row->unitprice;
						//	$nilaipemenuhan	= 0;
							$selisihopdo	= $jmlorder;
						//	$nilaiselisih	= $jmlorder*$row->unitprice;
						//	$hargaperunit = $row->unitprice;
						}						
						// ==== end 12-07-2012 =====
						
					/*	$sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, 
								tm_faktur_do_item_t b, tr_product_base c, tr_product_motif d where 
								a.i_faktur = b.i_faktur AND a.f_faktur_cancel = 'f'
								AND c.i_product_base = d.i_product
								AND d.i_product_motif = b.i_product";

						$sqlxx.= " AND c.f_stop_produksi = '$row->stopproduct'
								AND a.d_faktur >='$tdofirst' AND a.d_faktur <='$tdolast' AND b.i_product ='$row->imotif'";
					*/
					// 13-02-2013 dikomen
					/*$sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, tm_faktur_do_item_t b, tm_do c,
								tr_product_base d, tr_product_motif e  
								where a.i_faktur = b.i_faktur AND c.i_do = b.i_do
								AND d.i_product_base = e.i_product AND e.i_product_motif = b.i_product
								AND ((a.d_faktur >= '$tdofirst' AND a.d_faktur <= '$tdolast') OR (c.d_do >='$tdofirst' 
								AND c.d_do<='$tdolast')) AND b.i_product = '".$row->imotif."' AND a.f_faktur_cancel = 'f' 
								AND c.f_do_cancel = 'f' AND d.f_stop_produksi = '".$stopproduct."' ";
						if ($icustomer != '')
							$sqlxx.=" AND c.i_customer = '$icustomer' ";
						$query3	= $db2->query($sqlxx);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_faktur	= $hasilrow->jumnya;
							if ($qty_faktur == '')
								$qty_faktur = 0;
						} */
						
					// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
					/*$qjmlorderpemenuhan	= $this->mclass->jmlorder($i_product,$tdofirst,$tdolast,$stopproduct,$row->imotif,$icustomer,$fdropforcast);
					
					if($qjmlorderpemenuhan->num_rows()>0) {
						
						$rjmlorderpemenuhan	= $qjmlorderpemenuhan->row();

						$qhargaperpelanggan = $this->mclass->hargaperpelanggan($row->imotif,$rjmlorderpemenuhan->i_customer);
						$qhargadefault 	    = $this->mclass->hargadefault($row->imotif);
							
						if($qhargaperpelanggan->num_rows()>0) {
							$rhargaperpelanggan = $qhargaperpelanggan->row();
								
							$hargaperunit = $rhargaperpelanggan->v_price;
							
						}elseif($qhargadefault->num_rows()>0) {
							$rhargadefault = $qhargadefault->row();
								
							$hargaperunit = $rhargadefault->v_price;
								
						}else{
							$hargaperunit = $row->unitprice;
						}
							
						$jmlorder	= $rjmlorderpemenuhan->jmlorder;
						$pemenuhan = $rjmlorderpemenuhan->pemenuhan;
						$nilaiorder	= $rjmlorderpemenuhan->jmlorder*$hargaperunit;
						$nilaipemenuhan	= $rjmlorderpemenuhan->pemenuhan*$hargaperunit;
						$selisihopdo	= $rjmlorderpemenuhan->jmlorder - $rjmlorderpemenuhan->pemenuhan;
						$nilaiselisih	= $selisihopdo*$hargaperunit;
					}else{
						$jmlorder	= 0;
						$pemenuhan = 0;
						$nilaiorder	= 0;
						$nilaipemenuhan	= 0;
						$selisihopdo	= 0;
						$nilaiselisih	= 0;
						$hargaperunit = $row->unitprice;
					} */
											
					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->imotif);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->productmotif);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					/* $ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->unitprice); */
				/*	$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $hargaperunit);
					
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */
					
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->op);
					*/
					/* $ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $rjmlorderpemenuhan->jmlorder); */
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $jmlorder);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->nilaiop);
					*/
				/*	$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $nilaiorder);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->delivery);
					*/
					/* $ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $rjmlorderpemenuhan->pemenuhan); */
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $pemenuhan);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->nilaido);
					*/
				/*	$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $nilaipemenuhan);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->selopdo);
					*/
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $selisihopdo);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $row->nilaiselopdo);
					*/
				/*	$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $nilaiselisih);
					$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */
					
					if($this->session->userdata('ses_level')==3 || $this->session->userdata('ses_level')==1) {
						$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, '');
						$ObjPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
						);
					}
					
					$j++;																																																							
					$nomor++;
					/*
					$totalpermintaan+=$rjmlorderpemenuhan->jmlorder;
					$nilaipermintaan+=$nilaiorder;
					$totalpengiriman+=$rjmlorderpemenuhan->pemenuhan;
					$nilaipenjualan+=$nilaipemenuhan;					
					*/ 
					$totalpermintaan+=$jmlorder;
				//	$nilaipermintaan+=$nilaiorder;
					$totalpengiriman+=$pemenuhan;
				//	$nilaipenjualan+=$nilaipemenuhan;
				//	$totalqtyfaktur+= $qty_faktur;	
				} // end foreach qexpopvsdo
			} // end if
				
				// 13-08-2012, utk brg yg pemenuhan OP-nya 0 ----------------
				//print_r($isixx); die();
				if (is_array($isixx)) {
					for($zz=0;$zz<count($isixx);$zz++){
						
						$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $isixx[$zz]['imotif']);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $isixx[$zz]['productmotif']);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					/* $ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->unitprice); */
				/*	$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $isixx[$zz]['hargaperunit']);
					
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */
					
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->op);
					*/
					/* $ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $rjmlorderpemenuhan->jmlorder); */
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $isixx[$zz]['jmlorder']);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->nilaiop);
					*/
				/*	$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $isixx[$zz]['nilaiorder']);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->delivery);
					*/
					/* $ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $rjmlorderpemenuhan->pemenuhan); */
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $isixx[$zz]['pemenuhan']);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->nilaido);
					*/
				/*	$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $isixx[$zz]['nilaipemenuhan']);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->selopdo);
					*/
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $isixx[$zz]['selisihopdo']);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					/* 18052011
					$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $row->nilaiselopdo);
					*/
				/*	$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $isixx[$zz]['nilaiselisih']);
					$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					); */
					if($this->session->userdata('ses_level')==3 || $this->session->userdata('ses_level')==1) {
						$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, $isixx[$zz]['qty_faktur']);
						$ObjPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								),
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
					}
					
					$j++;																																																							
					$nomor++;
					/*
					$totalpermintaan+=$rjmlorderpemenuhan->jmlorder;
					$nilaipermintaan+=$nilaiorder;
					$totalpengiriman+=$rjmlorderpemenuhan->pemenuhan;
					$nilaipenjualan+=$nilaipemenuhan;					
					*/ 
					$totalpermintaan+=$isixx[$zz]['jmlorder'];
				//	$nilaipermintaan+=$isixx[$zz]['nilaiorder'];
					$totalpengiriman+=$isixx[$zz]['pemenuhan'];
				//	$nilaipenjualan+=$isixx[$zz]['nilaipemenuhan'];			
				//	$totalqtyfaktur+= $isixx[$zz]['qty_faktur'];	
					}
				}
				//-----------------------------------------------------------
				
				$selisihtotal	= ($totalpermintaan - $totalpengiriman);
				//$selisihnilai	= ($nilaipermintaan - $nilaipenjualan);	
				
				$jj=$j+1;
				
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$jj, 'TOTAL :');
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
					
				$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$jj, $totalpermintaan);
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
					
			/*	$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$jj, $nilaipermintaan);
				$ObjPHPExcel->getActiveSheet()->getStyle('F'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					); */
					
				$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$jj, $totalpengiriman);
				$ObjPHPExcel->getActiveSheet()->getStyle('G'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
					
			/*	$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$jj, $nilaipenjualan);
				$ObjPHPExcel->getActiveSheet()->getStyle('H'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					); */
					
				$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$jj, $selisihtotal);
				$ObjPHPExcel->getActiveSheet()->getStyle('I'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
					
			/*	$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$jj, $selisihnilai);
				$ObjPHPExcel->getActiveSheet()->getStyle('J'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					); */		
				if($this->session->userdata('ses_level')==3 || $this->session->userdata('ses_level')==1) {
					$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$jj, $totalqtyfaktur);
					$ObjPHPExcel->getActiveSheet()->getStyle('K'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);	
				}			
			// }
			
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');

			$stp	= $stop=='1'?'brgstp':'nonstp';
			//$files	= "2laporan_opvsdo"."_".$stp."_".$tdofirst."-".$tdolast.".xls";
			$files	= $this->session->userdata('gid')."laporan_opvsdogudang"."_".$stp."_".$tdofirst."-".$tdolast.".xls";	
			$ObjWriter->save("files/".$files);
																							
	//	} 
	/*	else {

			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2012");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2012");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan OP VS DO")
				->setSubject("Laporan OP VS DO")
				->setDescription("Laporan OP VS DO")
				->setKeywords("Laporan Bulanan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:J2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN OP VS DO');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:J3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:J4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', $e_initial_name);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A5', $ecustomername);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Kode Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Nama Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'HJP');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'OP');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Nilai OP');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Nilai DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Selisih OP DO');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'Nilai Selisih');
			$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			if($this->session->userdata('ses_level')==3 || $this->session->userdata('ses_level')==1) {
				$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'Qty Total Di Faktur');
				$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			}

			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');

			$stp	= $stop=='1'?'brgstp':'nonstp';
			$files	= $this->session->userdata('user_idx')."laporan_opvsdogrosirnew"."_".$stp."_".$tdofirst."-".$tdolast.".xls";			
			$ObjWriter->save("files/".$files);							
		} */

		$efilename = substr($files,1,strlen($files));
	
		$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
				$data['isi']	= 'expoopvsdogudang/vexpform';	
			$this->load->view('template',$data);	
		
	}

	function cpopup() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$iproduct	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		$sproduct	= $this->uri->segment(7);
		$stop		= $this->uri->segment(8);
		
		$extdofirst	= explode("-",$tdofirst,strlen($tdofirst));
		$extdolast	=  explode("-",$tdolast,strlen($tdolast));
		
		$nwdofirst	= $extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0];
		$nwdolast	= $extdolast[2]."/".$extdolast[1]."/".$extdolast[0];
		
		$periode	= $nwdofirst." s.d ".$nwdolast;
		
		$data['tglopmulai']	= $nwdofirst;
		$data['tglopakhir']	= $nwdolast;
		$data['kproduksi']	= $iproduct!='kosong'?$iproduct:"";
		$data['sproduksi']	= $sproduct=='TRUE'?" checked ":"";
		$data['stop']		= $stop==1?" checked ":"";
		
		$data['isi']	= $this->mclass->clistopvsdo($iproduct,$tdofirst,$tdolast,$sproduct);
		
		$this->load->view('expoopvsdo/header_excelfiles',$data);	
	}
	
}
?>
