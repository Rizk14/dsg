<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('stok-opname/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['isi'] = 'stok-opname/vmainform';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$gudang = $this->input->post('gudang', TRUE);  
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	// Di fitur input SO bahan baku/pembantu, misalnya mau input SO bln skrg. jika ada data SO di 1 bulan sebelumnya 
	// yg blm diapprove, maka tidak boleh input SO di bulan skrg.
	/*$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan = '$bulan_sebelumnya' 
						AND tahun = '$tahun_sebelumnya' AND status_approve = 'f' AND id_gudang = '$gudang' "); */
	// 14-02-2012					
	$sql = "SELECT id FROM tt_stok_opname_bahan_baku WHERE status_approve = 'f' AND id_gudang = '$gudang' ";
	if ($bulan == 1) {
		// modif 30-10-2014
		//$sql.= " AND ((bulan <= '$bulan_sebelumnya' AND tahun ='$tahun') OR (bulan<='12' AND tahun<'$tahun')) ";
		$sql.= " AND bulan<='12' AND tahun<'$tahun' ";
	}
	else
		$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";
	//$sql.= " AND status_approve = 'f' AND id_gudang = '$gudang'"; 
	
	//end 14-02-2012
	$query3	= $this->db->query($sql);
	
	if ($query3->num_rows() > 0){
		$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'stok-opname/vmainform';
		$this->load->view('template',$data);
	}
	else {	
		// 19 nov 2011
		// cek apakah sudah ada data SO di bulan setelahnya
		/*$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan > '$bulan' AND tahun = '$tahun' 
							AND status_approve = 't' AND id_gudang = '$gudang' "); */
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan > '$bulan' AND tahun >= '$tahun' 
							AND id_gudang = '$gudang' ");

		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'stok-opname/vmainform';
			$this->load->view('template',$data);
		}
		else {
			$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$gudang' ");
			$hasilrow = $query3->row();
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
			$nama_lokasi	= $hasilrow->nama_lokasi;
			
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['gudang'] = $gudang;
			$data['kode_gudang'] = $kode_gudang;
			$data['nama_gudang'] = $nama_gudang;
			$data['nama_lokasi'] = $nama_lokasi;

			$cek_data = $this->mmaster->cek_so_bahanbaku($bulan, $tahun, $gudang); 
			
			if ($cek_data['idnya'] == '' ) {
				// 01-07-2015, CEK APAKAH DATA DI TABEL SO MASIH KOSONG? JIKA MASIH KOSONG, MAKA MUNCULKAN FORM INPUT BARU
				$is_sokosong = $this->mmaster->cek_sokosong_bahanbaku($gudang); 
				if ($is_sokosong == 'f') { // JIKA SUDAH ADA SO
					// jika data so blm ada, maka ambil stok terkini dari tabel tm_barang
					$data['query'] = $this->mmaster->get_all_stok_bahanbaku($bulan, $tahun, $gudang);
				
					$data['is_new'] = '1';
					if (is_array($data['query']) )
						$data['jum_total'] = count($data['query']);
					else
						$data['jum_total'] = 0;
					
					$data['tgl_so'] = '';
					$data['isi'] = 'stok-opname/vformview';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 28-07-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					$data['is_new'] = '1';
					$data['tgl_so'] = '';
					$data['id_so'] = $id_so;
					$data['isi'] = 'stok-opname/vformviewfirst';
					$this->load->view('template',$data);
				}
			}
			else { // jika sudah diapprove maka munculkan msg
				if ($cek_data['status_approve'] == 't') {
					$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
					$data['list_gudang'] = $this->mmaster->get_gudang();
					$data['isi'] = 'stok-opname/vmainform';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 28-07-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					// get data dari tabel tt_stok_opname_bahan_baku yg statusnya 'f'
					$data['query'] = $this->mmaster->get_all_stok_opname_bahanbaku($bulan, $tahun, $gudang);
					
					// 06-07-2015 SEMENTARA DIKOMEN DULU, KHUSUS UTK INPUT SO BARU
					//$data['querybrgbaru'] = $this->mmaster->get_detail_stokbrgbaru($bulan, $tahun, $gudang); 
			
					$data['is_new'] = '0';
					if (is_array($data['query']))
						$data['jum_total'] = count($data['query']);
					
					// 06-07-2015 SEMENTARA DIKOMEN DULU, KHUSUS UTK INPUT SO BARU
					//if (is_array($data['querybrgbaru']))
					//	$data['jum_total'] += count($data['querybrgbaru']);
					
					$tgl_so = $cek_data['tgl_so'];
					$pisah1 = explode("-", $tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
					
					$data['tgl_so'] = $tgl_so;
					
					$data['id_so'] = $id_so;
					//$data['isi'] = 'stok-opname/vformview';
					$data['isi'] = 'stok-opname/vformviewfirstedit';
					$this->load->view('template',$data);
					
				}
			} // end else
		} // end else bln setelahnya
	} // end else bln sebelumnya
  }
  
  function submit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $gudang = $this->input->post('gudang', TRUE);  
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	  // 06-07-2015
	  $is_pertamakali = $this->input->post('is_pertamakali', TRUE);  
	  
	  // 01-07-2015
	  //if ($jum_data == '0')
	//	$jum_data = $this->input->post('no', TRUE)-1; 
	
	// 07-07-2015
		$jum_data = $this->input->post('no', TRUE)-1; 
	  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  //13-05-2015
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  $submit2 = $this->input->post('submit2', TRUE);
	  //$submit3 = $this->input->post('submit3', TRUE);
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
	  $hasilrow = $query3->row();
	  $kode_gudang	= $hasilrow->kode_gudang;
	  $nama_gudang	= $hasilrow->nama;
	  
	  if ($is_new == '1') {
	      // insert ke tabel tt_stok_opname_bahan_baku
	      $data_header = array(
			  'id_gudang'=>$gudang,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_so'=>$tgl_so
			);
		  $this->db->insert('tt_stok_opname_bahan_baku',$data_header);
		  
		  // ambil data terakhir di tabel tt_stok_opname_bahan_baku
		 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id; 
	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mmaster->save($is_new, $id_stok, $this->input->post('id_brg_'.$i, TRUE), 
			 $this->input->post('brgbaru_'.$i, TRUE),
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 //$this->input->post('harga_'.$i, TRUE),
			 // 14-03-2015, 13-05-2015 dikomen
			 //$this->input->post('auto_saldo_akhir_'.$i, TRUE),
			 //$this->input->post('auto_saldo_akhir_harga_'.$i, TRUE)
			 );
		  }
		  
		  //redirect('stok-opname/cform');
		  $data['msg'] = "Input stok opname bahan baku/pembantu untuk bulan ".$nama_bln." ".$tahun." di gudang ".$kode_gudang." - ".$nama_gudang." sudah berhasil disimpan. Silahkan lakukan approval untuk mengupdate stok";
		  $data['list_gudang'] = $this->mmaster->get_gudang();
		  $data['isi'] = 'stok-opname/vmainform';
	      $this->load->view('template',$data); // 3:24 31-10-2014
	  }
	  else {
		  if ($submit2 != '') {
			  // ambil data id dari tabel tt_stok_opname_bahan_baku
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '$gudang'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 // query ke tabel tt_stok_opname_bahan_baku_detail utk hapus tiap2 item warnanya
			 // 13-05-2015 dikomen ga dipake lagi
			 /*$sqlxx	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail
								WHERE id_stok_opname_bahan_baku = '$id_stok' ");
			 if ($sqlxx->num_rows() > 0){
				$hasilxx = $sqlxx->result();
				foreach ($hasilxx as $rowxx) {
					$this->db->query(" DELETE FROM tt_stok_opname_bahan_baku_detail_harga 
									WHERE id_stok_opname_bahan_baku_detail='$rowxx->id' ");
				} // end foreach detail
			 } // end if
			 */
			 
			 $this->db->query(" delete from tt_stok_opname_bahan_baku_detail where id_stok_opname_bahan_baku = '$id_stok' ");
			 $this->db->query(" delete from tt_stok_opname_bahan_baku where id = '$id_stok' ");
			 
			 $data['msg'] = "Input stok opname bahan baku/pembantu untuk bulan ".$nama_bln." ".$tahun." di gudang ".$kode_gudang." - ".$nama_gudang." sudah berhasil dihapus";
			 $data['list_gudang'] = $this->mmaster->get_gudang();
			 $data['isi'] = 'stok-opname/vmainform';
			 $this->load->view('template',$data); // done
		  }
		  else {
			  // 06-07-2015
			  if ($is_pertamakali == '1') {
				  //echo "didieu2"; die();
				  //echo $jum_data; die(); 125
					// update ke tabel tt_stok_opname_bahan_baku
				   // ambil data terakhir di tabel tt_stok_opname_bahan_baku
					 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '$gudang'
									AND bulan = '$bulan' AND tahun = '$tahun' ");
					 $hasilrow = $query2->row();
					 $id_stok	= $hasilrow->id; 
					 
					 //for ($i=1;$i<=$jum_data;$i++)
					 //{						 
						 //($_POST['something']	
						/* if (isset($_POST['id_brg_'.$i]))
							$id_brg = $_POST['id_brg_'.$i];
						if (isset($_POST['stok_'.$i]))
						 $stok_awal = $_POST['stok_'.$i];
						if (isset($_POST['stok_fisik_'.$i]))
						 $jum_stok_opname = $_POST['stok_fisik_'.$i];
						  echo $i." ".$id_brg."<br>"; */
						 //@$id_brg = $this->input->post('id_brg_'.$i, TRUE);
						 //@$stok_awal = $this->input->post('stok_'.$i, TRUE);
						 //@$jum_stok_opname = $this->input->post('stok_fisik_'.$i, TRUE);
						 
						 //echo $i." ".@$id_brg."<br>";
					 //}
					 //echo "125 = ".$_POST['id_brg_125'];
					 //die();
					 
					 $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET tgl_so='$tgl_so', tgl_update = '$tgl' 
								where id = '$id_stok' ");
					
					// -----------28-07-2015----------------------------------
					// 1. update data yg sudah ada
					$jum_data_ada = $this->input->post('jum_data', TRUE); 
					for ($i=1;$i<=$jum_data_ada;$i++)
					 {
						 $this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET 
								jum_stok_opname = '".$this->input->post('stok_fisik1_'.$i, TRUE)."'
								where id_brg = '".$this->input->post('id_brg1_'.$i, TRUE)."' 
								AND id_stok_opname_bahan_baku = '$id_stok' ");
					 }
					// --------------------------------------------------------
					 //$this->db->query(" DELETE FROM tt_stok_opname_bahan_baku_detail WHERE id_stok_opname_bahan_baku='$id_stok' ");
					 
					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {
						 $data_detail = array(
										'id_stok_opname_bahan_baku'=>$id_stok,
										'id_brg'=>$this->input->post('id_brg_'.$i, TRUE), 
										'stok_awal'=>$this->input->post('stok_'.$i, TRUE),
										'jum_stok_opname'=>$this->input->post('stok_fisik_'.$i, TRUE)
									);
						 $this->db->insert('tt_stok_opname_bahan_baku_detail',$data_detail);
		   						 
						 /*$id_brg = $this->input->post('id_brg_'.$i, TRUE);
						 $stok_awal = $this->input->post('stok_'.$i, TRUE);
						 $jum_stok_opname = $this->input->post('stok_fisik_'.$i, TRUE);
						 
						 $data_detail = array(
										'id_stok_opname_bahan_baku'=>$id_stok,
										'id_brg'=>$id_brg, 
										'stok_awal'=>$stok_awal,
										'jum_stok_opname'=>$jum_stok_opname
									);
						   $this->db->insert('tt_stok_opname_bahan_baku_detail',$data_detail);
						   echo $i." ".$id_brg."<br>"; */
					 }
					 //echo "kesini"; die();
			  }
			  else {
				  // update ke tabel tt_stok_opname_bahan_baku
				   // ambil data terakhir di tabel tt_stok_opname_bahan_baku
					 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '$gudang'
									AND bulan = '$bulan' AND tahun = '$tahun' ");
					 $hasilrow = $query2->row();
					 $id_stok	= $hasilrow->id; 
					 
					 $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET tgl_so='$tgl_so', tgl_update = '$tgl' 
								where id = '$id_stok' ");
					 
				  for ($i=1;$i<=$jum_data;$i++)
				  {
					 $this->mmaster->save($is_new, $id_stok, $this->input->post('id_brg_'.$i, TRUE), 
					 $this->input->post('brgbaru_'.$i, TRUE),
					 $this->input->post('stok_'.$i, TRUE),
					 $this->input->post('stok_fisik_'.$i, TRUE)
					 //$this->input->post('harga_'.$i, TRUE),
					 // 14-03-2015
					 //$this->input->post('auto_saldo_akhir_'.$i, TRUE),
					 //$this->input->post('auto_saldo_akhir_harga_'.$i, TRUE)
					 );
				  }
			  } // end if
			  
			  //redirect('stok-opname/cform');
			  $data['msg'] = "Input stok opname bahan baku/pembantu untuk bulan ".$nama_bln." ".$tahun." di gudang ".$kode_gudang." - ".$nama_gudang." sudah berhasil diupdate. Silahkan lakukan approval untuk mengupdate stok";
			  $data['list_gudang'] = $this->mmaster->get_gudang();
			  $data['isi'] = 'stok-opname/vmainform';
			  $this->load->view('template',$data);
		  } // end if $submit2
      }
  }
  
  // 01-07-2015. modif 28-07-2015
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_gudang	= $this->uri->segment(4);
	$id_so 	= $this->uri->segment(5);
	$posisi 	= $this->uri->segment(6);
	
	if ($id_gudang == '' && $posisi == '' && $id_so == '') {
		$id_gudang = $this->input->post('id_gudang', TRUE);  
		$id_so 	= $this->input->post('id_so', TRUE);  
		$posisi 	= $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || $id_gudang == '' || $posisi == '' || $id_so == '' ) {
			$id_gudang	= $this->uri->segment(4);
			$id_so 	= $this->uri->segment(5);
			$posisi 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$keywordcari = str_replace("%20"," ", $keywordcari);
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $id_gudang);
		
				$config['base_url'] = base_url()."index.php/stok-opname/cform/show_popup_brg/".$id_gudang."/".$id_so."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $id_gudang);
	$data['jum_total'] = count($qjum_total);
	$data['id_gudang'] = $id_gudang;
	$data['id_so'] = $id_so;
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$id_gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$nama_gudang	= '';
	}
	$data['nama_gudang'] = $nama_gudang;

	$this->load->view('stok-opname/vpopupbrg',$data);
  }
  
  function cekadabrg(){
		$id_so 	= $this->input->post('id_so', TRUE);
		$id_brg 	= $this->input->post('id_brg', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail
									WHERE id_stok_opname_bahan_baku = '$id_so' AND id_brg='$id_brg' ");
				
		if ($queryxx->num_rows() > 0)
			echo "ada";
		else
			echo "tidak";
		return false;
  }
  
  // 31-05-2014, skrip utk reset status_approve stok opname bhn baku maret 2014
  function resetstatusapproveso() {
	  $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '12'
								AND bulan = '03' AND tahun = '2014' ");
	  $hasilrow = $query2->row();
	  $id_stok	= $hasilrow->id; 
	  
	  $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET status_approve='f' WHERE id='$id_stok' ");
	  $this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET status_approve='f' WHERE id_stok_opname_bahan_baku='$id_stok' ");
	  echo "berhasil reset stok opname bln 03-2014";
  }

}
