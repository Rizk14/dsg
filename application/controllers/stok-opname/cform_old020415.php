<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('stok-opname/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['isi'] = 'stok-opname/vmainform';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$gudang = $this->input->post('gudang', TRUE);  
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	// Di fitur input SO bahan baku/pembantu, misalnya mau input SO bln skrg. jika ada data SO di 1 bulan sebelumnya 
	// yg blm diapprove, maka tidak boleh input SO di bulan skrg.
	/*$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan = '$bulan_sebelumnya' 
						AND tahun = '$tahun_sebelumnya' AND status_approve = 'f' AND id_gudang = '$gudang' "); */
	// 14-02-2012					
	$sql = "SELECT id FROM tt_stok_opname_bahan_baku WHERE status_approve = 'f' AND id_gudang = '$gudang' ";
	if ($bulan == 1)
		$sql.= " AND ((bulan <= '$bulan_sebelumnya' AND tahun ='$tahun') OR (bulan<='12' AND tahun<'$tahun')) ";
	else
		$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";
	//$sql.= " AND status_approve = 'f' AND id_gudang = '$gudang'"; 
	//echo $sql."<br>";
	//end 14-02-2012
	$query3	= $this->db->query($sql);
	
	if ($query3->num_rows() > 0){
		$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'stok-opname/vmainform';
		$this->load->view('template',$data);
	}
	else {	
		// 19 nov 2011
		// cek apakah sudah ada data SO di bulan setelahnya
		/*$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan > '$bulan' AND tahun = '$tahun' 
							AND status_approve = 't' AND id_gudang = '$gudang' "); */
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan > '$bulan' AND tahun >= '$tahun' 
							AND id_gudang = '$gudang' ");
		/*echo "SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan > '$bulan' AND tahun >= '$tahun' 
							AND id_gudang = '$gudang'"; */
		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'stok-opname/vmainform';
			$this->load->view('template',$data);
		}
		else {
			$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
			$hasilrow = $query3->row();
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
			$nama_lokasi	= $hasilrow->nama_lokasi;
			
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['gudang'] = $gudang;
			$data['kode_gudang'] = $kode_gudang;
			$data['nama_gudang'] = $nama_gudang;
			$data['nama_lokasi'] = $nama_lokasi;

			$cek_data = $this->mmaster->cek_so_bahanbaku($bulan, $tahun, $gudang); 
			//print_r($cek_data); die();
			
			if ($cek_data['idnya'] == '' ) { 
				// jika data so blm ada, maka ambil stok terkini dari tabel tm_barang
				$data['query'] = $this->mmaster->get_all_stok_bahanbaku($gudang);
			
				$data['is_new'] = '1';
				if (is_array($data['query']) )
					$data['jum_total'] = count($data['query']);
				else
					$data['jum_total'] = 0;
				
				$data['isi'] = 'stok-opname/vformview';
				$this->load->view('template',$data);
			}
			else { // jika sudah diapprove maka munculkan msg
				if ($cek_data['status_approve'] == 't') {
					$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
					$data['list_gudang'] = $this->mmaster->get_gudang();
					$data['isi'] = 'stok-opname/vmainform';
					$this->load->view('template',$data);
				}
				else {
					// get data dari tabel tt_stok_opname_bahan_baku yg statusnya 'f'
					$data['query'] = $this->mmaster->get_all_stok_opname_bahanbaku($bulan, $tahun, $gudang);
			
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					$data['isi'] = 'stok-opname/vformview';
					$this->load->view('template',$data);
					
				}
			} // end else
		} // end else bln setelahnya
	} // end else bln sebelumnya
  }
  
  function submit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $gudang = $this->input->post('gudang', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d"); 
	  
	  $submit2 = $this->input->post('submit2', TRUE);
	  $submit3 = $this->input->post('submit3', TRUE);
	  
	  if ($is_new == '1') {
	      // insert ke tabel tt_stok_opname_bahan_baku
	      $data_header = array(
			  'id_gudang'=>$gudang,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
		  $this->db->insert('tt_stok_opname_bahan_baku',$data_header);
		  
		  // ambil data terakhir di tabel tt_stok_opname_bahan_baku
		 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id; 
	      
	      for ($i=1;$i<=$no;$i++)
		  {
			 $this->mmaster->save($is_new, $id_stok, $this->input->post('kode_'.$i, TRUE), 
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE));
		  }
		  
		  redirect('stok-opname/cform');
	  }
	  else {
		  if ($submit2 != '') {
			  // ambil data id dari tabel tt_stok_opname_bahan_baku
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '$gudang'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 $this->db->query(" delete from tt_stok_opname_bahan_baku_detail where id_stok_opname_bahan_baku = '$id_stok' ");
			 $this->db->query(" delete from tt_stok_opname_bahan_baku where id = '$id_stok' ");
			 redirect('stok-opname/cform');
		  }
		  else if ($submit3 != '') {
			  // ambil data id dari tabel tt_stok_opname_bahan_baku
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '$gudang'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			  for ($i=1;$i<=$no;$i++) {
				 if ($this->input->post('cek_'.$i, TRUE) == 'y') {
					 //echo "masuk delete per item</br>";
					 //echo $this->input->post('cek_'.$i, TRUE);
					 $this->db->query(" delete from tt_stok_opname_bahan_baku_detail 
								WHERE id_stok_opname_bahan_baku = '$id_stok' 
								AND kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
				 }
			  }
			 redirect('stok-opname/cform');
		  }
		  else {
			  // update ke tabel tt_stok_opname_bahan_baku
			   // ambil data terakhir di tabel tt_stok_opname_bahan_baku
				 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '$gudang'
								AND bulan = '$bulan' AND tahun = '$tahun' ");
				 $hasilrow = $query2->row();
				 $id_stok	= $hasilrow->id; 
				 
				 $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET tgl_update = '$tgl' 
							where id = '$id_stok' ");
				 
			  for ($i=1;$i<=$no;$i++)
			  {
				 $this->mmaster->save($is_new, $id_stok, $this->input->post('kode_'.$i, TRUE), 
				 $this->input->post('stok_'.$i, TRUE),
				 $this->input->post('stok_fisik_'.$i, TRUE));
			  }
			  
			  redirect('stok-opname/cform');
		  } // end if $submit2
      }
  }
  
  // 31-05-2014, skrip utk reset status_approve stok opname bhn baku maret 2014
  function resetstatusapproveso() {
	  $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '12'
								AND bulan = '03' AND tahun = '2014' ");
	  $hasilrow = $query2->row();
	  $id_stok	= $hasilrow->id; 
	  
	  $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET status_approve='f' WHERE id='$id_stok' ");
	  $this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET status_approve='f' WHERE id_stok_opname_bahan_baku='$id_stok' ");
	  echo "berhasil reset stok opname bln 03-2014";
  }

}
