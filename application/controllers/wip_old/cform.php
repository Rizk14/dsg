<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('wip/mmaster');
  }
	
	// 14-03-2013, SJ masuk WIP
  function addsjmasuk(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$th_now	= date("Y");

	$data['isi'] = 'wip/vmainformsjmasuk';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$this->load->view('template',$data);

  }
  
  function editsjmasuk(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_sj 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_sjmasuk($id_sj); 
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['msg'] = '';
	$data['isi'] = 'wip/veditformsjmasuk';
	$this->load->view('template',$data);

  }
  
  function updatedatasjmasuk() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_sj 	= $this->input->post('id_sj', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$jenis 	= $this->input->post('jenis_masuk', TRUE);  
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  // id_gudang ga perlu diupdate
			$id_gudang_lama 	= $this->input->post('id_gudang_lama', TRUE);  
			
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");

			//update headernya
			$this->db->query(" UPDATE tm_sjmasukwip SET tgl_sj = '$tgl_sj', jenis_masuk = '$jenis', tgl_update='$tgl',
							no_sj='".$this->db->escape_str($no_sj)."', kode_unit_jahit = '$kode_unit_jahit', 
							kode_unit_packing = '$kode_unit_packing', 
							keterangan = '$ket' where id= '$id_sj' ");
							
				//reset stok, dan hapus dulu detailnya
				//============= 25-01-2013 ====================
				$query2	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail WHERE id_sjmasukwip = '$id_sj' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					$tgl = date("Y-m-d");
					foreach ($hasil2 as $row2) {
						// ============ update stok =====================
						
						$nama_tabel_stok = "tm_stok_hasil_jahit";
				
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." 
										WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$id_gudang_lama' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama-$row2->qty; // berkurang stok karena reset dari bon M masuk lain
								
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$row2->kode_brg_jadi' AND id_gudang = '$id_gudang_lama' ");
						// ==============================================
					} // end foreach
				} // end reset stok
				//=============================================
				$this->db->delete('tm_sjmasukwip_detail', array('id_sjmasukwip' => $id_sj));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$kode = $this->input->post('kode_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$ket_warna = $this->input->post('ket_warna_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
						
						// ======== update stoknya! =============
						$nama_tabel_stok = "tm_stok_hasil_jahit";
						
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg_jadi = '$kode'
												AND id_gudang='$id_gudang_lama' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
								$data_stok = array(
									'kode_brg_jadi'=>$kode,
									'stok'=>$new_stok,
									'id_gudang'=>$id_gudang_lama,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert($nama_tabel_stok, $data_stok);
							}
							else {
								$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$kode' AND id_gudang = '$id_gudang_lama' ");
							}
					
					// jika semua data tdk kosong, insert ke tm_sjmasukwip_detail
					$data_detail = array(
						'id_sjmasukwip'=>$id_sj,
						'kode_brg_jadi'=>$kode,
						'qty'=>$qty,
						'keterangan'=>$ket_detail,
						'ket_qty_warna'=>$ket_warna,
					);
					$this->db->insert('tm_sjmasukwip_detail',$data_detail);
							// ================ end insert item detail ===========
				} // end perulangan

			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "wip/cform/viewsjmasuk/".$cur_page;
			else
				$url_redirectnya = "wip/cform/carisjmasuk/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
				
  }

  function submitsjmasuk(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			$jenis 	= $this->input->post('jenis_masuk', TRUE);  
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data_sjmasukwip($no_sj, $thn1);
			if (count($cek_data) > 0) {
				$data['isi'] = 'wip/vmainformsjmasuk';
				$data['msg'] = "Data no SJ masuk ".$no_sj." utk tahun $thn1 sudah ada..!";
				$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
				$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
				$data['list_gudang'] = $this->mmaster->get_gudang();
				$this->load->view('template',$data);
			}
			else {
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->savesjmasuk($no_sj,$tgl_sj, $id_gudang, $jenis, $kode_unit_jahit, $kode_unit_packing, $ket, 
							$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								$this->input->post('qty_'.$i, TRUE), $this->input->post('ket_warna_'.$i, TRUE),  
								$this->input->post('ket_detail_'.$i, TRUE) );						
				}
				redirect('wip/cform/viewsjmasuk');
			}
  }
  
  function viewsjmasuk(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'wip/vformviewsjmasuk';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAllsjmasuktanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/wip/cform/viewsjmasuk/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllsjmasuk($config['per_page'],$this->uri->segment(4), $keywordcari, $date_from, $date_to);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }
  
  function carisjmasuk(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(6);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	
	$jum_total = $this->mmaster->getAllsjmasuktanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/wip/cform/carisjmasuk/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllsjmasuk($config['per_page'],$this->uri->segment(7), $keywordcari, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'wip/vformviewsjmasuk';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }

  function show_popup_brgjadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$gudang 	= $this->input->post('id_gudang', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '' && $gudang == '') {
			$gudang 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
	// +++++++++++++++

		$qjum_total = $this->mmaster->get_brgjaditanpalimit($gudang, $keywordcari);
		
				$config['base_url'] = base_url()."index.php/wip/cform/show_popup_brgjadi/".$gudang."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $gudang, $keywordcari);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$nama_gudang = '';
	}
	$data['gudang'] = $gudang;
	$data['nama_gudang'] = $nama_gudang;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('wip/vpopupbrgjadi',$data);
  }
  
  function deletesjmasuk(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
    
    $this->mmaster->deletesjmasuk($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "wip/cform/viewsjmasuk/".$cur_page;
	else
		$url_redirectnya = "wip/cform/carisjmasuk/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  
  // +++++++++++++++++ 14-03-2013, SJ keluar WIP +++++++++++++++++
  function addsjkeluar(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_sj FROM tm_sjkeluarwip ORDER BY no_sj DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_sj	= $hasilrow->no_sj;
			else
				$no_sj = '';
			if(strlen($no_sj)==9) {
				$nosj = substr($no_sj, 0, 9);
				$n_sj	= (substr($nosj,4,5))+1;
				$th_sj	= substr($nosj,0,4);
				if($th_now==$th_sj) {
						$jml_n_sj	= $n_sj;
						switch(strlen($jml_n_sj)) {
							case "1": $kodesj	= "0000".$jml_n_sj;
							break;
							case "2": $kodesj	= "000".$jml_n_sj;
							break;	
							case "3": $kodesj	= "00".$jml_n_sj;
							break;
							case "4": $kodesj	= "0".$jml_n_sj;
							break;
							case "5": $kodesj	= $jml_n_sj;
							break;	
						}
						$nomorsj = $th_now.$kodesj;
				}
				else {
					$nomorsj = $th_now."00001";
				}
			}
			else {
				$nomorsj	= $th_now."00001";
			}

	$data['isi'] = 'wip/vmainformsjkeluar';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['no_sj'] = $nomorsj;
	$this->load->view('template',$data);

  }
  
  function editsjkeluar(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_sj 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_sjkeluar($id_sj); 
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['msg'] = '';
	$data['isi'] = 'wip/veditformsjkeluar';
	$this->load->view('template',$data);

  }
  
  function updatedatasjkeluar() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_sj 	= $this->input->post('id_sj', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$jenis 	= $this->input->post('jenis_keluar', TRUE);  
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  // id_gudang ga perlu diupdate
			$id_gudang_lama 	= $this->input->post('id_gudang_lama', TRUE);  
			
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");

			//update headernya
			$this->db->query(" UPDATE tm_sjkeluarwip SET tgl_sj = '$tgl_sj', jenis_keluar = '$jenis', tgl_update='$tgl',
							no_sj='".$this->db->escape_str($no_sj)."', kode_unit_jahit = '$kode_unit_jahit', 
							kode_unit_packing = '$kode_unit_packing', 
							keterangan = '$ket' where id= '$id_sj' ");
							
				//reset stok, dan hapus dulu detailnya
				//============= 25-01-2013 ====================
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$id_sj' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					$tgl = date("Y-m-d");
					foreach ($hasil2 as $row2) {
						// ============ update stok =====================
						
						$nama_tabel_stok = "tm_stok_hasil_jahit";
				
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." 
										WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$id_gudang_lama' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
								
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$row2->kode_brg_jadi' AND id_gudang = '$id_gudang_lama' ");
						// ==============================================
					} // end foreach
				} // end reset stok
				//=============================================
				$this->db->delete('tm_sjkeluarwip_detail', array('id_sjkeluarwip' => $id_sj));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$kode = $this->input->post('kode_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$ket_warna = $this->input->post('ket_warna_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
						
						// ======== update stoknya! =============
						$nama_tabel_stok = "tm_stok_hasil_jahit";
						
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg_jadi = '$kode'
												AND id_gudang='$id_gudang_lama' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
								$data_stok = array(
									'kode_brg_jadi'=>$kode,
									'stok'=>$new_stok,
									'id_gudang'=>$id_gudang_lama,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert($nama_tabel_stok, $data_stok);
							}
							else {
								$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$kode' AND id_gudang = '$id_gudang_lama' ");
							}
					
					// jika semua data tdk kosong, insert ke tm_sjkeluarwip_detail
					$data_detail = array(
						'id_sjkeluarwip'=>$id_sj,
						'kode_brg_jadi'=>$kode,
						'qty'=>$qty,
						'keterangan'=>$ket_detail,
						'ket_qty_warna'=>$ket_warna,
					);
					$this->db->insert('tm_sjkeluarwip_detail',$data_detail);
							// ================ end insert item detail ===========
				} // end perulangan

			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "wip/cform/viewsjkeluar/".$cur_page;
			else
				$url_redirectnya = "wip/cform/carisjkeluar/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
				
  }

  function submitsjkeluar(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			$jenis 	= $this->input->post('jenis_keluar', TRUE);  
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data_sjkeluarwip($no_sj);
			if (count($cek_data) > 0) {
				$data['isi'] = 'wip/vmainformsjkeluar';
				$data['msg'] = "Data no SJ keluar ".$no_sj." sudah ada..!";
				$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
				$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
				$data['list_gudang'] = $this->mmaster->get_gudang();
				$this->load->view('template',$data);
			}
			else {
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->savesjkeluar($no_sj,$tgl_sj, $id_gudang, $jenis, $kode_unit_jahit, $kode_unit_packing, $ket, 
							$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								$this->input->post('qty_'.$i, TRUE), $this->input->post('ket_warna_'.$i, TRUE),  
								$this->input->post('ket_detail_'.$i, TRUE) );						
				}
				redirect('wip/cform/viewsjkeluar');
			}
  }
  
  function viewsjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'wip/vformviewsjkeluar';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAllsjkeluartanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/wip/cform/viewsjkeluar/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllsjkeluar($config['per_page'],$this->uri->segment(4), $keywordcari, $date_from, $date_to);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }
  
  function carisjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(6);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	
	$jum_total = $this->mmaster->getAllsjkeluartanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/wip/cform/carisjkeluar/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllsjkeluar($config['per_page'],$this->uri->segment(7), $keywordcari, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'wip/vformviewsjkeluar';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }
  
  function deletesjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
    
    $this->mmaster->deletesjkeluar($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "wip/cform/viewsjkeluar/".$cur_page;
	else
		$url_redirectnya = "wip/cform/carisjkeluar/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  
}
