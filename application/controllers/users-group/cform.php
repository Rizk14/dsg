<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('users-group/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->gid;
			$enama = $row->nama_group;
		}
	}
	else {
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'users-group/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		
			$id_group 	= $this->input->post('id_group', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == 1) {
				$this->mmaster->save($id_group, $nama, $goedit);
			}
			else {
				$cek_data = $this->mmaster->cek_data($nama);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id_group, $nama, $goedit);
				}
			}
			
			redirect('users-group/cform');
		
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('users-group/cform');
  }
}
