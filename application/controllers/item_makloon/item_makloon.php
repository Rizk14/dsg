<?php
class item_makloon extends CI_Controller
{
    public $data = array(
        'halaman' => 'item_makloon',        
        'title' => 'Item Makloon',
        'main_view' => 'item_makloon/item_makloon_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('item_makloon/item_makloon_model', 'item_makloon');
    }

   

     
     public function barang_wip($offset=null){

		$posisi =  $this->input->post('posisi');
		$page = $this->uri->segment(5);
		$per_page=10;
		
		if (empty($page)) {
		$offset = 0;
		} else {
		$offset = ($page * $per_page - $per_page);
		}
		$item_makloon_barang_wip = $this->item_makloon->get_all_barang_wip($offset);
		if($posisi == ''){
		$posisi =$this->uri->segment(4);
			}
			
		if ($item_makloon_barang_wip) {
            $this->data['item_makloon_barang_wip'] = $item_makloon_barang_wip;
            $this->data['paging'] = $this->item_makloon->paging_barang('biasa', site_url('item_makloon/item_makloon/halaman/'.$posisi), 5);
        } else {
            $this->data['item_makloon_barang_wip'] = 'Tidak ada data di master barang wip, Silahkan Melakukan '.anchor('/item_makloon/item_makloon/', 'Proses penginputan.', 'class="alert-link"');
        }
        
        $this->data['posisi'] = $posisi;
        $this->data['form_action'] = site_url('item_makloon/item_makloon/cari_barang/'.$posisi);		
        
        $this->load->view('item_makloon/item_makloon_list_barang',$this->data);
		

}

public function cari_barang($offset = 0)
    {
        $kata_kunci =  $this->input->post('kata_kunci',TRUE);
        $posisi =  $this->input->post('posisi',TRUE);
		if($posisi == ''){
			$posisi =$this->uri->segment(4);
		}
		$item_makloon_barang_wip = $this->item_makloon->cari_barang($offset,$kata_kunci);
        if ($item_makloon_barang_wip) {
            $this->data['item_makloon_barang_wip'] = $item_makloon_barang_wip;
            $this->data['paging'] = $this->item_makloon->paging_barang('pencarian', site_url('/item_makloon/item_makloon/cari_barang/'.$posisi), 5);
        } else {
            $this->data['item_makloon_barang_wip'] = 'Data tidak ditemukan.'. anchor('/item_makloon/item_makloon/barang_wip/'.$posisi, ' Tampilkan semua data di master barang wip.', 'class="alert-link"');
        }
		
        $this->data['posisi'] = $posisi;
        $this->data['form_action'] = site_url('/item_makloon/item_makloon/cari_barang/'.$posisi);
        $this->data['main_view'] = 'item_makloon/item_makloon_list_barang';
        $this->load->view($this->layout2, $this->data);
    }
    
     public function barang_bb($offset=null){

		$posisi =  $this->input->post('posisi');
		$page = $this->uri->segment(5);
		$per_page=10;
		
		if (empty($page)) {
		$offset = 0;
		} else {
		$offset = ($page * $per_page - $per_page);
		}
		$item_makloon_barang_bb = $this->item_makloon->get_all_barang_bb($offset);
		if($posisi == ''){
		$posisi =$this->uri->segment(4);
			}
			
		if ($item_makloon_barang_bb) {
            $this->data['item_makloon_barang_bb'] = $item_makloon_barang_bb;
            $this->data['paging'] = $this->item_makloon->paging_barang_bb('biasa', site_url('item_makloon/item_makloon/halaman/'.$posisi), 5);
        } else {
            $this->data['item_makloon_barang_bb'] = 'Tidak ada data di master barang wip, Silahkan Melakukan '.anchor('/item_makloon/item_makloon/', 'Proses penginputan.', 'class="alert-link"');
        }
        
        $this->data['posisi'] = $posisi;
        $this->data['form_action'] = site_url('item_makloon/item_makloon/cari_barang_bb/'.$posisi);		
        $this->data['main_view'] = 'item_makloon/item_makloon_list_barang_bb';
        $this->load->view($this->layout2, $this->data);
		

}


public function cari_barang_bb($offset = 0)
    {
        $kata_kunci =  $this->input->post('kata_kunci',TRUE);
        $posisi =  $this->input->post('posisi',TRUE);
		if($posisi == ''){
			$posisi =$this->uri->segment(4);
		}
		$item_makloon_barang_bb = $this->item_makloon->cari_barang_bb($offset,$kata_kunci);
        if ($item_makloon_barang_bb) {
            $this->data['item_makloon_barang_bb'] = $item_makloon_barang_bb;
            $this->data['paging'] = $this->item_makloon->paging_barang_bb('pencarian', site_url('/item_makloon/item_makloon/cari_barang_bb/'.$posisi), 5);
        } else {
            $this->data['item_makloon_barang_bb'] = 'Data tidak ditemukan.'. anchor('/item_makloon/item_makloon/barang_bb/'.$posisi, ' Tampilkan semua data di master barang BB.', 'class="alert-link"');
        }
		
        $this->data['posisi'] = $posisi;
        $this->data['form_action'] = site_url('/item_makloon/item_makloon/cari_barang_bb/'.$posisi);
        $this->data['main_view'] = 'item_makloon/item_makloon_list_barang_bb';
        $this->load->view($this->layout2, $this->data);
    }
     public function barang_bb_ck($offset=null){

		$posisi =  $this->input->post('posisi');
		$page = $this->uri->segment(5);
		$per_page=10;
		
		if (empty($page)) {
		$offset = 0;
		} else {
		$offset = ($page * $per_page - $per_page);
		}
		$item_makloon_barang_bb = $this->item_makloon->get_all_barang_bb($offset);
		if($posisi == ''){
		$posisi =$this->uri->segment(4);
			}
			
		if ($item_makloon_barang_bb) {
            $this->data['item_makloon_barang_bb'] = $item_makloon_barang_bb;
            $this->data['paging'] = $this->item_makloon->paging_barang_bb('biasa', site_url('item_makloon/item_makloon/halaman/'.$posisi), 5);
        } else {
            $this->data['item_makloon_barang_bb'] = 'Tidak ada data di master barang wip, Silahkan Melakukan '.anchor('/item_makloon/item_makloon/', 'Proses penginputan.', 'class="alert-link"');
        }
        
        $this->data['posisi'] = $posisi;
        $this->data['form_action'] = site_url('item_makloon/item_makloon/cari_barang_bb_ck/'.$posisi);		
       
        $this->load->view('item_makloon/item_makloon_list_barang_bb_ck', $this->data);
		

}


public function cari_barang_bb_ck($offset = 0)
    {
        $kata_kunci =  $this->input->post('kata_kunci',TRUE);
        $posisi =  $this->input->post('posisi',TRUE);
		if($posisi == ''){
			$posisi =$this->uri->segment(4);
		}
		$item_makloon_barang_bb = $this->item_makloon->cari_barang_bb_ck($offset,$kata_kunci);
        if ($item_makloon_barang_bb) {
            $this->data['item_makloon_barang_bb'] = $item_makloon_barang_bb;
            $this->data['paging'] = $this->item_makloon->paging_barang_bb('pencarian', site_url('/item_makloon/item_makloon/cari_barang_bb/'.$posisi), 5);
        } else {
            $this->data['item_makloon_barang_bb'] = 'Data tidak ditemukan.'. anchor('/item_makloon/item_makloon/barang_bb_ck/'.$posisi, ' Tampilkan semua data di master barang BB.', 'class="alert-link"');
        }
		
        $this->data['posisi'] = $posisi;
        $this->data['form_action'] = site_url('/item_makloon/item_makloon/cari_barang_bb_ck/'.$posisi);
          $this->load->view('item_makloon/item_makloon_list_barang_bb_ck', $this->data);
   
        $this->load->view($this->layout2, $this->data);
    } 
}

