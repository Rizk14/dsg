<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() { 
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
			$data['page_title_sj']	= $this->lang->line('page_title_sj');
			$data['list_tgl_mulai_sj']	= $this->lang->line('list_tgl_mulai_sj');
			$data['form_title_detail_sj']	= $this->lang->line('form_title_detail_sj');
			$data['list_no_sj']	= $this->lang->line('list_no_sj');
			$data['list_tgl_sj']	= $this->lang->line('list_tgl_sj');
			$data['list_kd_brg_sj']	= $this->lang->line('list_kd_brg_sj');
			$data['list_nm_brg_sj']	= $this->lang->line('list_nm_brg_sj');
			$data['list_unit_sj']	= $this->lang->line('list_unit_sj');
			$data['list_unit_belumfaktur_sj']	= $this->lang->line('list_unit_belumfaktur_sj');
			$data['list_hjp_sj']	= $this->lang->line('list_hjp_sj');
			$data['list_harga_sj']	= $this->lang->line('list_harga_sj');	
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');		
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['lsj']	= "";
			$this->load->model('listsj/mclass');
				
		$data['isi']	= 'listsj/vmainform';	
			$this->load->view('template',$data);	
		
		
	}
	
	function carilistsj() {		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$data['page_title_sj']	= $this->lang->line('page_title_sj');
		$data['list_tgl_mulai_sj']	= $this->lang->line('list_tgl_mulai_sj');
		$data['form_title_detail_sj']	= $this->lang->line('form_title_detail_sj');
		$data['list_no_sj']	= $this->lang->line('list_no_sj');
		$data['list_to_sj']	= $this->lang->line('list_to_sj');
		$data['list_tgl_sj']	= $this->lang->line('list_tgl_sj');
		$data['list_kd_brg_sj']	= $this->lang->line('list_kd_brg_sj');
		$data['list_nm_brg_sj']	= $this->lang->line('list_nm_brg_sj');
		$data['list_unit_sj']	= $this->lang->line('list_unit_sj');
		$data['list_unit_belumfaktur_sj']	= $this->lang->line('list_unit_belumfaktur_sj');
		$data['list_hjp_sj']	= $this->lang->line('list_hjp_sj');
		$data['list_harga_sj']	= $this->lang->line('list_harga_sj');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lsj']	= "";
		
		$isj	= $this->input->post('isj')? $this->input->post('isj'): $this->input->get_post('isj');
		$dsjfirst	= $this->input->post('d_sj_first')? $this->input->post('d_sj_first'): $this->input->get_post('d_sj_first');
		$dsjlast	= $this->input->post('d_sj_last')? $this->input->post('d_sj_last'): $this->input->get_post('d_sj_last');
			
		$data['tglsjmulai']	= $dsjfirst;
		$data['tglsjakhir']	= $dsjlast;
		$data['isj']	= $isj;
		
		$dsj_first	= explode("/",$dsjfirst,strlen($dsjfirst)); // dd/mm/YYYY
		$dsj_last	= explode("/",$dsjlast,strlen($dsjlast)); // dd/mm/YYYY
		
		$n_d_sj_first	= (!empty($dsj_first[2]) && strlen($dsj_first[2])==4)?$dsj_first[2].'-'.$dsj_first[1].'-'.$dsj_first[0]:"";
		$n_d_sj_last	= (!empty($dsj_last[2]) && strlen($dsj_last[2])==4)?$dsj_last[2].'-'.$dsj_last[1].'-'.$dsj_last[0]:"";
		
		$this->load->model('listsj/mclass');
		$data['query']	= $this->mclass->clistsj($isj,$n_d_sj_first,$n_d_sj_last);
			$data['isi']	= 'listsj/vlistform';	
			$this->load->view('template',$data);	
		
	}

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$data['page_title']	= "SURAT JALAN (SJ)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listsj/mclass');

		$query	= $this->mclass->lsj();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listsj/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lsjperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listsj/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$data['page_title']	= "SURAT JALAN (SJ)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listsj/mclass');

		$query	= $this->mclass->lsj();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listsj/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lsjperpages($pagination['per_page'],$pagination['cur_page']);

		$this->load->view('listsj/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "SURAT JALAN (SJ)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listsj/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->flsj($key);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
					'01'=>'Januari', 
					'02'=>'Februari', 
					'03'=>'Maret', 
					'04'=>'April', 
					'05'=>'Mei', 
					'06'=>'Juni', 
					'07'=>'Juli', 
					'08'=>'Agustus', 
					'09'=>'September', 
					'10'=>'Oktober', 
					'11'=>'Nopember', 
					'12'=>'Desember' );
							
			$cc	= 1; 			
			foreach($query->result() as $row) {
					$tgl			= (!empty($row->dsj) || strlen($row->dsj)!=0)?@explode("-",$row->dsj,strlen($row->dsj)):"";
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
								
				// $list .= "
				//  <tr>
				//   <td>".$cc."</td>
				// 	  <td><a href=\"javascript:settextfield('$row->isj')\">".$row->isj."</a></td>	 
				// 	  <td><a href=\"javascript:settextfield('$row->isj')\">".$tanggal[$cc]."</a></td>
				//  </tr>";
				
				$list .= "
				 <tr>
				  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->isjcode')\">".$row->isjcode."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->isjcode')\">".$tanggal[$cc]."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
	
	/* Edit Session */
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$isj	= $this->uri->segment(4,0);
		$dsj	= $this->uri->segment(5,0);

		$expdsj	= explode("-",$dsj,strlen($dsj));
		$bulan	= $expdsj[1];
		$tahun	= $expdsj[0];
				
		$data['page_title_sj']	= $this->lang->line('page_title_sj');
		$data['form_title_detail_sj']	= $this->lang->line('form_title_detail_sj');
		$data['form_nomor_sj']	= $this->lang->line('form_nomor_sj');
		$data['form_tanggal_sj']	= $this->lang->line('form_tanggal_sj');
		$data['form_kode_produk_sj']	= $this->lang->line('form_kode_produk_sj');
		$data['form_nm_produk_sj']	= $this->lang->line('form_nm_produk_bbk');
		$data['form_hjp_sj']	= $this->lang->line('form_hjp_sj');
		$data['form_unit_sj']	= $this->lang->line('form_unit_sj');
		$data['form_total_sj']	= $this->lang->line('form_total_sj');
		$data['form_total_nilai_brg_sj']	= $this->lang->line('form_total_nilai_brg_sj');
		$data['form_ket_sj']	= $this->lang->line('form_ket_sj');
		$data['form_cabang_sj']	= $this->lang->line('form_cabang_sj');
		$data['form_option_pel_sj']	= $this->lang->line('form_option_pel_sj');
		$data['lpelanggan']	= "";
		$data['button_update']	= $this->lang->line('button_update');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$this->load->model('listsj/mclass');
		
		$data['opt_pelanggan']	= $this->mclass->lpelanggan();
		$qsj	= $this->mclass->getsj($isj);

		if($qsj->num_rows()>0) {
			$row_sj= $qsj->row();
			
			$isj		= $row_sj->i_sj;
			$isjcode	= $row_sj->i_sj_code;
			$enote		= $row_sj->e_note;
			$icustomer	= $row_sj->i_customer;
			$ibranch	= $row_sj->i_branch;
			
			$data['isj']	= $isj;
			$data['isjcode']= $isjcode;
			$data['enote']	= $enote;
			$data['icustomer']	= $icustomer;
			$data['ibranch']	= $ibranch;
			$data['opt_cabang']	= $this->mclass->lcabang($icustomer);
			
			$tglsj	= explode("-",$row_sj->d_sj,strlen($row_sj->d_sj)); // YYYY-mm-dd
			$data['tsj']	= (!empty($tglsj[2]) || strlen($tglsj[2])!=0)?($tglsj[2]."/".$tglsj[1]."/".$tglsj[0]):"";
			$data['dsj']	= $row_sj->d_sj;
			$data['sjitem']	= $this->mclass->getsjitem($isj,$tahun,$bulan);
		}
						
		$this->load->view('listsj/veditform', $data);
				
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$iteration	= $this->input->post('iteration');
		$iterasi	= $iteration;
		$i_sjcode	= $this->input->post('i_sj');
		$isj	= $this->input->post('isj');
		$d_sj	= $this->input->post('d_sj');
		$ex_d_sj	= explode("/",$d_sj,strlen($d_sj));
		$dsj	= $ex_d_sj[2]."-".$ex_d_sj[1]."-".$ex_d_sj[0];
		
		$i_customer	= $this->input->post('icustomer');
		$i_branch	= $this->input->post('ibranch');		
		$v_sj_total	= $this->input->post('v_sj_total');
		$e_note		= $this->input->post('e_note');
		
		$i_product	= array();
		$e_product_name	= array();
		$v_product_price= array();
		$n_unit	= array();
		$v_unit_price	= array();
		$stp	= array();
		$iso	= array();
		
		$i_product_0	= $this->input->post('i_product'.'_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iteration;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_product_price[$cacah]	= $this->input->post('v_product_price'.'_'.'tblItem'.'_'.$cacah);
			$n_unit[$cacah]	= $this->input->post('n_unit'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
			$stp[$cacah]	= $this->input->post('f_stp_tblItem_'.$cacah);
			$iso[$cacah]	= $this->input->post('i_so_tblItem_'.$cacah);
		}

		if ( !empty($i_sjcode) &&
		     !empty($d_sj) && 
		     !empty($isj) ) {
			 	if(!empty($i_product_0)) {
					$this->load->model('listsj/mclass');
					$this->mclass->mupdate($i_sjcode,$isj,$dsj,$i_customer,$i_branch,$v_sj_total,$e_note,$i_product,$e_product_name,$v_product_price,$n_unit,$v_unit_price,$iterasi,$stp,$iso);
				} else {
					print "<script>alert(\"Maaf, SJ gagal diupdate!\");show(\"listsj/cform\",\"#content\");</script>";
				}
		} else {
			print "<script>alert(\"Maaf, SJ gagal diupdate!\");show(\"listsj/cform\",\"#content\");</script>";
		}			
	}
	
	function carisj() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$nsj	= $this->input->post('nsj')?$this->input->post('nsj'):$this->input->get_post('nsj');
		$this->load->model('listsj/mclass');
		$qsj	= $this->mclass->cari_sj($nsj);
		if($qsj->num_rows()>0) {
			echo "Maaf, No. SJ tdk dpt diubah!";
		}		
	}
	/* End 0f Edit Session */		
	
	/* Cancel SJ */
	function undo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$isj	= $this->uri->segment(4);
		$this->load->model('listsj/mclass');
		$this->mclass->mbatal($isj);
	}
	/* End 0f Cancel */
}
?>
