<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('stok-opname-hsl-cutting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['isi'] = 'stok-opname-hsl-cutting/vmainform';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$this->load->view('template',$data);

  }
  
  function submit() {
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  //$jum_data = $this->input->post('jum_data', TRUE);
	  // utk pertama kali, pake yg ini
	  $jum_data = $this->input->post('no', TRUE)-1; 
	  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  $is_pertamakali = $this->input->post('is_pertamakali', TRUE);  
	  
	  // 04-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  if ($is_new == '1') {
		  $uid_update_by = $this->session->userdata('uid');
	      // insert ke tabel tt_stok_opname_hasil_cutting
	      $data_header = array(
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_so'=>$tgl_so,
			  'uid_update_by'=>$uid_update_by
			);
		  $this->db->insert('tt_stok_opname_hasil_cutting',$data_header);
		  
		  // ambil data terakhir di tabel tt_stok_opname_
		 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id; 
	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mmaster->save($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),$this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_warna_'.$i, TRUE)
			 );
		  }
		  
		  $data['msg'] = "Input stok opname hasil cutting untuk bulan ".$nama_bln." ".$tahun." sudah berhasil disimpan. Silahkan lakukan approval untuk mengupdate stok";
		  $data['isi'] = 'stok-opname-hsl-cutting/vmainform';
		  $this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
			  // ambil data id dari tabel tt_stok_opname_hasil_cutting
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting WHERE bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 // query ke tabel tt_stok_opname_hasil_cutting_detail utk hapus tiap2 item warna dan bhn baku
			 $sqlxx	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting_detail
								WHERE id_stok_opname_hasil_cutting = '$id_stok' ");
			if ($sqlxx->num_rows() > 0){
				$hasilxx = $sqlxx->result();
				foreach ($hasilxx as $rowxx) {
					$this->db->query(" DELETE FROM tt_stok_opname_hasil_cutting_detail_warna WHERE id_stok_opname_hasil_cutting_detail='$rowxx->id' ");
					//$this->db->query(" DELETE FROM tt_stok_opname_hasil_cutting_detail_bhnbaku WHERE id_stok_opname_hasil_cutting_detail='$rowxx->id' ");
				} // end foreach detail
			} // end if
			 //==============================================================================================
			 
			$this->db->query(" delete from tt_stok_opname_hasil_cutting_detail where id_stok_opname_hasil_cutting = '$id_stok' ");
			$this->db->query(" delete from tt_stok_opname_hasil_cutting where id = '$id_stok' ");
			
			$data['msg'] = "Input stok opname hasil cutting untuk bulan ".$nama_bln." ".$tahun." sudah berhasil dihapus";
			$data['isi'] = 'stok-opname-hsl-cutting/vmainform';
			$this->load->view('template',$data);
		  }
		  else {
			  if ($is_pertamakali == '1') {
					$query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting WHERE
								bulan = '$bulan' AND tahun = '$tahun' ");
					$hasilrow = $query2->row();
					$id_stok	= $hasilrow->id;
					
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tt_stok_opname_hasil_cutting SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								uid_update_by='$uid_update_by' where id = '$id_stok' ");
					
					// 1. update data yg sudah ada
					$jum_data_ada = $this->input->post('jum_data', TRUE); 
					for ($i=1;$i<=$jum_data_ada;$i++)
					 {
						 /*$this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET 
									jum_stok_opname = '".$this->input->post('stok_fisik1_'.$i, TRUE)."'
									where id_brg = '".$this->input->post('id_brg1_'.$i, TRUE)."' 
									AND id_stok_opname_bahan_baku = '$id_stok' "); */
									
						$id_brg_wip1 = $this->input->post('id_brg_wip1_'.$i, TRUE);
						$id_warna1 = $this->input->post('id_warna1_'.$i, TRUE);
						$qty_warna1 = $this->input->post('qty_warna1_'.$i, TRUE);
						$stok1 = $this->input->post('stok1_'.$i, TRUE);
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						for ($xx=0; $xx<count($id_warna1); $xx++) {
							$id_warna1[$xx] = trim($id_warna1[$xx]);
							$stok1[$xx] = trim($stok1[$xx]);
							$qtytotalstokawal+= $stok1[$xx];				
							$qty_warna1[$xx] = trim($qty_warna1[$xx]);
							$qtytotalstokfisikwarna+= $qty_warna1[$xx];
						} // end for
						
						$this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail SET jum_stok_opname = '".$qtytotalstokfisikwarna."' 
									where id_brg_wip = '$id_brg_wip1' AND id_stok_opname_hasil_cutting = '$id_stok' ");
					 } // end for
					// --------------------------------------------------------
						 
					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {
						 /*$data_detail = array(
											'id_stok_opname_bahan_baku'=>$id_stok,
											'id_brg'=>$this->input->post('id_brg_'.$i, TRUE), 
											'stok_awal'=>$this->input->post('stok_'.$i, TRUE),
											'jum_stok_opname'=>$this->input->post('stok_fisik_'.$i, TRUE)
									);
						 $this->db->insert('tt_stok_opname_bahan_baku_detail',$data_detail); */
						
						$tgl = date("Y-m-d H:i:s"); 
					  //-------------- hitung total qty dari detail tiap2 warna -------------------
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$stok_fisik_warna = $this->input->post('stok_fisik_warna_'.$i, TRUE);
						$stok = $this->input->post('stok_'.$i, TRUE);
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok[$xx] = trim($stok[$xx]);
							$qtytotalstokawal+= $stok[$xx];				
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
						} // end for
						
						// 07-10-2015 lanjutannya
						$data_detail = array(
									'id_stok_opname_hasil_cutting'=>$id_stok,
									'id_brg_wip'=>$id_brg_wip, 
									'stok_awal'=>$qtytotalstokawal,
									'jum_stok_opname'=>$qtytotalstokfisikwarna
								);
					   $this->db->insert('tt_stok_opname_hasil_cutting_detail',$data_detail);
					   
					   $seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting_detail ORDER BY id DESC LIMIT 1 ");
					   if($seq_detail->num_rows() > 0) {
							$seqrow	= $seq_detail->row();
							$iddetail = $seqrow->id;
					   }
					   else
							$iddetail = 0;
							
					   // ------------------stok berdasarkan warna brg wip----------------------------
					   if (isset($id_warna) && is_array($id_warna)) {
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							
							$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting_detail_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_warna->num_rows() > 0) {
								$seqrow	= $seq_warna->row();
								$idbaru	= $seqrow->id+1;
							}else{
								$idbaru	= 1;
							}
							
							$tt_stok_opname_hasil_cutting_detail_warna	= array(
									 'id'=>$idbaru,
									 'id_stok_opname_hasil_cutting_detail'=>$iddetail,
									 'id_warna'=>$id_warna[$xx],
									 'jum_stok_opname'=>$stok_fisik_warna[$xx]
								);
							$this->db->insert('tt_stok_opname_hasil_cutting_detail_warna',$tt_stok_opname_hasil_cutting_detail_warna);
						} // end for
					  } // end if
						// ----- end lanjutan 07-10-2015 ----------
					// ---------------------------------------------------------------------
				} // END FOR
			  } // end if pertamakali
			  else {
					// update ke tabel tt_stok_opname_hasil_cutting
				   // ambil data terakhir di tabel tt_stok_opname_hasil_cutting
					 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting WHERE bulan = '$bulan' 
									AND tahun = '$tahun' ");
					 $hasilrow = $query2->row();
					 $id_stok	= $hasilrow->id; 
					 
					 $uid_update_by = $this->session->userdata('uid');
					 $this->db->query(" UPDATE tt_stok_opname_hasil_cutting SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								uid_update_by='$uid_update_by' where id = '$id_stok' ");
					 
				  for ($i=1;$i<=$jum_data;$i++)
				  {
					  $this->mmaster->save($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
					 $this->input->post('id_warna_'.$i, TRUE),$this->input->post('stok_'.$i, TRUE),
					 $this->input->post('stok_fisik_warna_'.$i, TRUE)
					 );
				  }
			  }
			  //redirect('stok-opname-hsl-cutting/cform');
			  $data['msg'] = "Input stok opname hasil cutting untuk bulan ".$nama_bln." ".$tahun." sudah berhasil diupdate. Silahkan lakukan approval untuk mengupdate stok";
			  $data['isi'] = 'stok-opname-hsl-cutting/vmainform';
			  $this->load->view('template',$data); //
		}
      }
  }
  
  // 25-11-2014
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	// Di fitur input SO bahan baku/pembantu, misalnya mau input SO bln skrg. jika ada data SO di 1 bulan sebelumnya 
	// yg blm diapprove, maka tidak boleh input SO di bulan skrg.
	/*$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan = '$bulan_sebelumnya' 
						AND tahun = '$tahun_sebelumnya' AND status_approve = 'f' AND id_gudang = '$gudang' "); */
	// 14-02-2012					
	$sql = "SELECT id FROM tt_stok_opname_hasil_cutting WHERE status_approve = 'f' ";
	if ($bulan == 1) {
		// modif 30-10-2014
		//$sql.= " AND ((bulan <= '$bulan_sebelumnya' AND tahun ='$tahun') OR (bulan<='12' AND tahun<'$tahun')) ";
		$sql.= " AND bulan<='12' AND tahun<'$tahun' ";
	}
	else
		$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";
	//$sql.= " AND status_approve = 'f' AND id_gudang = '$gudang'"; 
	
	//end 14-02-2012
	$query3	= $this->db->query($sql);
	
	if ($query3->num_rows() > 0){
		$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
		//$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'stok-opname-hsl-cutting/vmainform';
		$this->load->view('template',$data);
	}
	else {	
		// 19 nov 2011
		// cek apakah sudah ada data SO di bulan setelahnya
		/*$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE bulan > '$bulan' AND tahun = '$tahun' 
							AND status_approve = 't' AND id_gudang = '$gudang' "); */
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting WHERE bulan > '$bulan' AND tahun >= '$tahun' ");

		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
			//$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'stok-opname-hsl-cutting/vmainform';
			$this->load->view('template',$data);
		}
		else {			
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;

			$cek_data = $this->mmaster->cek_so_hasil_cutting($bulan, $tahun); 
			
			if ($cek_data['idnya'] == '' ) {
				// 25-09-2015. adopsi dari SO bahan baku
				// 01-07-2015, CEK APAKAH DATA DI TABEL SO MASIH KOSONG? JIKA MASIH KOSONG, MAKA MUNCULKAN FORM INPUT BARU
				$is_sokosong = $this->mmaster->cek_sokosong_hasil_cutting(); 
				if ($is_sokosong == 'f') { // JIKA SUDAH ADA SO
					
					// jika data so blm ada, maka ambil stok terkini dari tabel tm_barang
					$data['query'] = $this->mmaster->get_all_stok_hasil_cutting($bulan, $tahun);
					$data['is_new'] = '1';
					if (is_array($data['query']) )
						$data['jum_total'] = count($data['query']);
					else
						$data['jum_total'] = 0;
					
					$data['tgl_so'] = '';
					$data['isi'] = 'stok-opname-hsl-cutting/vformview';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 28-07-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					$data['is_new'] = '1';
					$data['tgl_so'] = '';
					$data['id_so'] = $id_so;
					$data['isi'] = 'stok-opname-hsl-cutting/vformviewfirst';
					$this->load->view('template',$data);
				}
				// ======================================================================================
			}
			else { // jika sudah diapprove maka munculkan msg
				if ($cek_data['status_approve'] == 't') {
					$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
					//$data['list_gudang'] = $this->mmaster->get_gudang();
					$data['isi'] = 'stok-opname-hsl-cutting/vmainform';
					$this->load->view('template',$data);
				}
				else {
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					
					// get data dari tabel tt_stok_opname_bahan_baku yg statusnya 'f'
					$data['query'] = $this->mmaster->get_all_stok_opname_hasil_cutting($bulan, $tahun);
			
					$data['is_new'] = '0';
					if (is_array($data['query']))
						$data['jum_total'] = count($data['query']);
					else
						$data['jum_total'] = 0;
					
					$tgl_so = $cek_data['tgl_so'];
					$pisah1 = explode("-", $tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
					
					$data['tgl_so'] = $tgl_so;
					$data['id_so'] = $id_so;
					
					// 25-09-2015 sementara dikomen dulu, karena utk input SO pertama kali
					//$data['isi'] = 'stok-opname-hsl-cutting/vformview';
					$data['isi'] = 'stok-opname-hsl-cutting/vformviewfirstedit';
					$this->load->view('template',$data);
				}
			} // end else
		} // end else bln setelahnya
	} // end else bln sebelumnya
  }
  
  // 25-09-2015
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_so 	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);
	
	if ($posisi == '' && $id_so == '') {
		$id_so 	= $this->input->post('id_so', TRUE);  
		$posisi 	= $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || $posisi == '' || $id_so == '' ) {
			$id_so 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$keywordcari = str_replace("%20"," ", $keywordcari);
		$qjum_total = $this->mmaster->get_barangtanpalimit($keywordcari); // getbahan
		
				$config['base_url'] = base_url()."index.php/stok-opname-hsl-cutting/cform/show_popup_brg/".$id_so."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_barang($config['per_page'],$config['cur_page'], $keywordcari);
	$data['jum_total'] = count($qjum_total);
	$data['id_so'] = $id_so;
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('stok-opname-hsl-cutting/vpopupbrg',$data);
  }
  
  // 26-09-2015
  function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$isedit 	= $this->input->post('isedit', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		if ($isedit == '0') {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
										WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		else {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
			}
			else {
				$id_brg_wip = '0';
			}
						
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
					WHERE id = '".$id_brg_wip."' AND id NOT IN 
					(SELECT b.id_brg_wip FROM tt_stok_opname_hasil_cutting a 
					INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
					WHERE a.id = '$id_so' ) ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		
		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('stok-opname-hsl-cutting/vinfobrgwip', $data); 
		return true;
  }
  
  // modif 25-09-2015
  function additemwarna(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		if ($id_so != '') {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							AND a.id_brg_wip NOT IN 
						(SELECT b.id_brg_wip FROM tt_stok_opname_hasil_cutting a 
						INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
						WHERE a.id = '$id_so' )
							ORDER BY b.nama");
		}
		else {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							ORDER BY b.nama");
		}
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('stok-opname-hsl-cutting/vlistwarna', $data); 
		return true;
  }
}
