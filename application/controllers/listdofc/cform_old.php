<?php
class Cform extends Controller {

	function __construct() { 
		parent::Controller();
	}
	
	function index() {
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0 ) 
		{	
			$data['page_title_do']	= $this->lang->line('page_title_do');
			$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
			$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
			$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
			$data['list_no_do']	= $this->lang->line('list_no_do');
			$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
			$data['list_nm_cab_do']	= $this->lang->line('list_nm_cab_do');
			$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
			$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
			$data['list_qty_do']	= $this->lang->line('list_qty_do');
			$data['list_sisa_delivery_do']	= $this->lang->line('list_sisa_delivery_do');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');		
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['disabled']	= 'f';
			$this->load->model('listdo/mclass');
			$this->load->view('listdo/vmainform',$data);
		}	
	}
	
	function carilistdo() {
		
		$data['page_title_do']	= $this->lang->line('page_title_do');
		$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
		$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
		$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
		$data['list_nm_cab_do']	= $this->lang->line('list_nm_cab_do');
		$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
		$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
		$data['list_qty_do']	= $this->lang->line('list_qty_do');
		$data['list_sisa_delivery_do']	= $this->lang->line('list_sisa_delivery_do');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['ldobrg']	= "";

		$d_do_first	= $this->input->post('d_do_first');
		$d_do_last	= $this->input->post('d_do_last');
		$kddo	= $this->input->post('nomor_do');
		$ftrans	= $this->input->post('ftrans');
		$ff		= $this->input->post('ff');
		$fc		= $this->input->post('fc');
		
		if($ftrans=='')
			$ftrans = 0;
			
		if($ff=='')
			$ff	= 0;

		if($fc=='')
			$fc	= 0;
		
		$ftransfer	= $ftrans=='1'?'t':'f';
		$ffaktur	= $ff=='1'?'t':'f';
		$fcetak		= $fc=='1'?'t':'f';
		
		if($d_do_first!='' || $d_do_last!=''){
		
			$data['tgldomulai']	= $d_do_first;
			$data['tgldoakhir']	= $d_do_last;
			$data['nomordo']	= $kddo;
	
			$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first)); // dd/mm/YYYY
			$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last)); // dd/mm/YYYY
	
			$n_d_do_first	= (!empty($e_d_do_first[2]) && strlen($e_d_do_first[2])==4)?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:'';
			$n_d_do_last	= (!empty($e_d_do_last[2]) && strlen($e_d_do_last[2])==4)?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:'';
			
		}else{
			$n_d_do_first	= '';
			$n_d_do_last	= '';
			$data['tgldomulai']	= '';
			$data['tgldoakhir']	= '';
			$data['nomordo']	= $kddo;
		}
		
		$turi1	= ($n_d_do_first!='' && $n_d_do_first!='empty')?$n_d_do_first:'empty';
		$turi2	= ($n_d_do_last!='' && $n_d_do_last!='empty')?$n_d_do_last:'empty';
		$turi3	= $kddo==''?'empty':$kddo;

		$flag	= 	$turi1=='empty'?'f':'t';
		$data['flag']	= $flag;
		
		$this->load->model('listdo/mclass');

		if($flag=='t'){
			$query	= $this->mclass->clistdobrg_t($n_d_do_first,$n_d_do_last,$kddo,$ftransfer,$ffaktur,$fcetak);
			$jml	= $query->num_rows();
			$pagination['base_url'] 	= '/listdo/cform/clistdobrgperpages/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$ftrans.'/'.$ff.'/'.$fc.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(10,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			$data['isi_t']	= $this->mclass->viewperpages_t($pagination['per_page'],$pagination['cur_page'],$n_d_do_first,$n_d_do_last,$kddo,$ftransfer,$ffaktur,$fcetak);
		}elseif($flag=='f'){			
			$data['isi_f']	= $this->mclass->clistdobrg($n_d_do_first,$n_d_do_last,$kddo,$ftransfer,$ffaktur,$fcetak);
		}
		
		$this->load->view('listdo/vlistform',$data);
	}

	function clistdobrgperpages() {		
		$data['page_title_do']	= $this->lang->line('page_title_do');
		$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
		$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
		$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
		$data['list_nm_cab_do']	= $this->lang->line('list_nm_cab_do');
		$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
		$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
		$data['list_qty_do']	= $this->lang->line('list_qty_do');
		$data['list_sisa_delivery_do']	= $this->lang->line('list_sisa_delivery_do');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['ldobrg']	= "";

		$tawal	= $this->uri->segment(4);
		$takhir	= $this->uri->segment(5);
		$nomer	= $this->uri->segment(6);
		$ftrans	= $this->uri->segment(7);
		$ff		= $this->uri->segment(8);
		$fc		= $this->uri->segment(9);
		
		if($ftrans=='')
			$ftrans = 0;

		if($ff=='')
			$ff	= 0;

		if($fc=='')
			$fc	= 0;
						
		$ftransfer	= $ftrans=='1'?'t':'f';
		$ffaktur	= $ff=='1'?'t':'f';
		$fcetak		= $fc=='1'?'t':'f';
				
		$e_d_op_first	= explode("-",$tawal,strlen($tawal)); // YYYY-mm-dd
		$e_d_op_last	= explode("-",$takhir,strlen($takhir)); // YYYY-mm-dd
		$data['tgldomulai']	= $e_d_op_first[2].'/'.$e_d_op_first[1].'/'.$e_d_op_first[0];
		$data['tgldoakhir']	= $e_d_op_last[2].'/'.$e_d_op_last[1].'/'.$e_d_op_last[0];
		$data['nomordo']	= '';

		$flag	= 	't';
		$data['flag']	= $flag;
						
		$this->load->model('listdo/mclass');

		if($flag=='t'){
			$query	= $this->mclass->clistdobrg_t($tawal,$takhir,"empty",$ftransfer,$ffaktur,$fcetak);
			$jml	= $query->num_rows();
			$pagination['base_url'] 	= '/listdo/cform/clistdobrgperpages/'.$tawal.'/'.$takhir.'/empty/'.$ftrans.'/'.$ff.'/'.$fc.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(10,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			$data['isi_t']	= $this->mclass->viewperpages_t($pagination['per_page'],$pagination['cur_page'],$tawal,$takhir,"empty",$ftransfer,$ffaktur,$fcetak);
		}
		
		$this->load->view('listdo/vlistform',$data);
	}
	
	function listbarangjadi() {
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listdo/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listdo/vlistformbrgjadi',$data);
	}	
	
	function listbarangjadinext() {
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listdo/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);

		$this->load->view('listdo/vlistformbrgjadi',$data);
	}
	
	function flistbarangjadi() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listdo/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->flbarangjadi($key);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			$tanggal	= array();			
			$cc	= 1; 
			
			foreach($query->result() as $row){
				$tgl	= (!empty($row->ddo) || strlen($row->ddo)!=0)?@explode("-",$row->ddo,strlen($row->ddo)):""; // YYYY-mm-dd
				$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
				$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
								
				$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->ido_code')\">".$row->ido_code."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->ido_code')\">".$tanggal[$cc]."</a></td>
					 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;		
	}
	

	function edit() {
	
		$idocode	= $this->uri->segment(4);

		$data['form_nomor_do']	= $this->lang->line('form_nomor_do');
		$data['form_tgl_do']	= $this->lang->line('form_tgl_do');
		$data['form_cabang_do']	= $this->lang->line('form_cabang_do');
		$data['form_option_pel_do']	= $this->lang->line('form_option_pel_do');
		$data['form_option_cab_do']	= $this->lang->line('form_option_cab_do');
		$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
		$data['form_nomor_op_do']	= $this->lang->line('form_nomor_op_do');
		$data['form_kode_produk_do']	= $this->lang->line('form_kode_produk_do');
		$data['form_nm_produk_do']	= $this->lang->line('form_nm_produk_do');
		$data['form_jml_product_do']	= $this->lang->line('form_jml_product_do');
		$data['form_harga_product_do']	= $this->lang->line('form_harga_product_do');
		$data['form_ket_do']		= $this->lang->line('form_ket_do');
		$data['button_update']	= $this->lang->line('button_update');	
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['page_title_do']	= $this->lang->line('page_title_do');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lpelanggan']	= "";
		$data['lcabang']	= "";
		$data['selected_cab']	= "";
		$data['disabled']	= 'f';
		
		$this->load->model('listdo/mclass');
		
		$qrydoheader	= $this->mclass->getdoheader($idocode);
		
		if($qrydoheader->num_rows()>0) {
			$row_doheader	= $qrydoheader->row();
			$ido			= $row_doheader->i_do;
			$data['no']		= $row_doheader->i_do_code;
			$data['ido']	= $row_doheader->i_do;
			$tgldo	= explode("-",$row_doheader->d_do,strlen($row_doheader->d_do)); // YYYY-mm-dd
			$data['tgDO']	= $tgldo[2]."/".$tgldo[1]."/".$tgldo[0];
			$icustomer		= $row_doheader->i_customer;
			$ibranch		= $row_doheader->i_branch;		

			$qcustomer			= $this->mclass->getpelanggan($icustomer);
			$qcabang			= $this->mclass->getcabang($ibranch);
			
			$data['doitem']		= $this->mclass->ldoitem($ido);
			$data['opt_cabang']	= $this->mclass->lcabang($icustomer);

			if($qcustomer->num_rows()>0) {
				$row_customer	= $qcustomer->row();
				$icust			= $row_customer->i_customer;
				$data['icust']	= $icust;
			} else {
				$data['icust']	= "";
			}
			
			if($qcabang->num_rows()>0) {
				$row_cabang			= $qcabang->row();
				$ibranch			= $row_cabang->i_branch;
				$data['ibranch']	= $ibranch;
			} else {
				$data['ibranch']	= "";
			}						
		} else {
			$data['icust']	= "";
			$data['ibranch']= "";
			$data['no']		= "";
			$data['ido']	= "";
			$data['tgDO']	= "";
			$data['doitem']	= "";
			$data['opt_cabang']	= "";
		}
		$data['opt_pelanggan']	= $this->mclass->lpelanggan();
		
		$this->load->view('listdo/veditform',$data);
	}

	function cari_cabang() {
		$i_customer	= $this->input->post('ibranch');
		$this->load->model('listdo/mclass');
		$query	= $this->mclass->getcabang($i_customer);
		$c	= "";
		if($query->num_rows()>0) {
			$cabang	= $query->result();

			$c .= "<select name=\"i_branch\" id=\"i_branch\" >";
			foreach ($cabang as $row) {
				$c .= "<option value=".$row->i_branch_code.">".$row->e_branch_name." ( ".$row->e_initial." ) "."</option>";
			}
			$c .= "</option>";
			echo $c;
		}
	}

	function cari_do() {
		
		$ndo		= $this->input->post('ndo');
		$codelama	= $this->input->post('code');
		
		$this->load->model('listdo/mclass');
		$qndo	= $this->mclass->cari_do($ndo,$codelama);
		if($qndo->num_rows()>0) {
			//echo "Maaf, No. DO tdk dpt diubah!";
			echo "Maaf, No. DO tsb sudah ada!";
		}
	}	
	
	function actedit() {
		
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration;

		$i_do				= $this->input->post('i_do'); // i_do_code baru
		$i_do_code_hidden	= $this->input->post('i_do_code_hidden');
		$i_do_hidden		= $this->input->post('i_do_hidden');
		$i_branch_hidden	= $this->input->post('i_branch_hidden');
		$i_customer_hidden	= $this->input->post('i_customer_hidden');
		$d_do				= $this->input->post('d_do');
		//no longer used anymore
		//$i_op_0	= $this->input->post('i_op_'.'tblItem'.'_'.'0');
		
		$stpnya	= $this->input->post('stpnya');
		$stpny	= explode(";",$stpnya);
		
		$ex_d_do = explode("/",$d_do,strlen($d_do)); // dd/mm/YYYY
		$nw_d_do = $ex_d_do[2]."-".$ex_d_do[1]."-".$ex_d_do[0];
		
		$i_product_item	= array();
		$n_deliver_item	= array();
		
		$i_opcode	= array();
		$i_product	= array();
		$e_product_name	= array();
		$n_deliver	= array();
		$v_do_gross	= array();
		$e_note	= array();
		$qty_product	= array();
		$qty_op	= array();
		$i_op_sebunyi = array();
		$f_stp	= array();
		
		$is_grosir	= array();
		$harga_grosir	= array();
		
		// 10-05-2013
		$adaboneka	= array();
		
		$fstp_arr	= array();		
		$fstpny	= 0;
		
		foreach($stpny as $tp) {
			$fstp_arr[$fstpny]	= $tp;
			$fstpny++;
		}
				// revisi 07-08-2012, ganti dari cacah=0 menjadi 1
		for($cacah=1; $cacah<=$iteration; $cacah++) { // iterasi=2, 0<2, 1<2
			
			$i_product_item[$cacah]	= $this->input->post('i_product_item_'.'tblItem'.'_'.$cacah);
			$n_deliver_item[$cacah]	= $this->input->post('n_deliver_item_'.'tblItem'.'_'.$cacah);
			$i_opcode[$cacah]		= $this->input->post('i_op_'.'tblItem'.'_'.$cacah);
			$i_product[$cacah]		= $this->input->post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah);
			$n_deliver[$cacah]		= $this->input->post('n_deliver_'.'tblItem'.'_'.$cacah);
			
			$is_grosir[$cacah]		= $this->input->post('is_grosir'.'_'.$cacah);
			$harga_grosir[$cacah]		= $this->input->post('grosir_'.'tblItem'.'_'.$cacah);
			
			$v_do_gross[$cacah]		= $this->input->post('v_do_gross_'.'tblItem'.'_'.$cacah);
			$e_note[$cacah]			= $this->input->post('e_note_'.'tblItem'.'_'.$cacah);
			$qty_product[$cacah]	= $this->input->post('qty_product_'.'tblItem'.'_'.$cacah);
			$qty_op[$cacah]			= $this->input->post('qty_op_'.'tblItem'.'_'.$cacah);
			$i_op_sebunyi[$cacah]	= $this->input->post('i_op_sebunyi_'.'tblItem'.'_'.$cacah);
			$f_stp[$cacah]			= $this->input->post('f_stp_'.'tblItem'.'_'.$cacah);
			
			$adaboneka[$cacah]		= $this->input->post('adaboneka_'.'tblItem'.'_'.$cacah);
		}

		if( !empty($i_do_hidden) && 
		    !empty($i_customer_hidden) && 
		    !empty($i_branch_hidden) ) {
			
			//if(!empty($i_op_0)) {
				
				$this->load->model('listdo/mclass');
				
				$this->mclass->mupdate($i_product_item,$n_deliver_item,$i_do,$i_do_code_hidden,$i_do_hidden,
				$nw_d_do,$i_customer_hidden,$i_branch_hidden,$i_opcode,$i_op_sebunyi,$i_product,$e_product_name,
				$n_deliver,$v_do_gross,$e_note,$iterasi,$qty_product,$qty_op,$f_stp,$fstp_arr, $is_grosir, $harga_grosir,
				$adaboneka);
			/*}else{
				print "<script>alert(\"Maaf, item DO harus terisi. Terimakasih.\");show(\"listdo/cform\",\"#content\");</script>";
			} */
		}else{
			$data['page_title_do']	= $this->lang->line('page_title_do');
			$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
			$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
			$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
			$data['list_no_do']	= $this->lang->line('list_no_do');
			$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
			$data['list_nm_cab_do']	= $this->lang->line('list_nm_cab_do');
			$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
			$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
			$data['list_qty_do']	= $this->lang->line('list_qty_do');
			$data['list_sisa_delivery_do']	= $this->lang->line('list_sisa_delivery_do');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');		
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			print "<script>alert('Maaf, DO gagal diupdate!');show(\"listdo/cform\",\"#content\");</script>";
		}
	}
	

	function undo() {
		$idocode	= $this->uri->segment(4);
		$this->load->model('listdo/mclass');
		$this->mclass->mbatal($idocode);
	}	

	
	function refresh(){
		$data['page_title_do']	= $this->lang->line('page_title_do');
		$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
		$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
		$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
		$data['list_nm_cab_do']	= $this->lang->line('list_nm_cab_do');
		$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
		$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
		$data['list_qty_do']	= $this->lang->line('list_qty_do');
		$data['list_sisa_delivery_do']	= $this->lang->line('list_sisa_delivery_do');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['disabled']	= 't';
		
		$idoitem	= array();
		$iproduct	= array();
		$ndeliver	= array();
		
		$iproductbase	= array();
		$price	= array();
		$vdogross_new	= array();
		
		$inde	= 0;
		
		$qdoitem	= $this->db->query(" SELECT i_do_item, i_product, n_deliver FROM tm_do_item WHERE n_residual > 0 AND f_price_refresh='f' ");
		if($qdoitem->num_rows()>0){
			foreach($qdoitem->result() as $row1){
				$idoitem[$inde]	= $row1->i_do_item;
				$iproduct[$inde]	= $row1->i_product; // i_product_motif
				$ndeliver[$inde]	= $row1->n_deliver;

				$qitembarang	= $this->db->query(" SELECT a.i_product_base, a.v_unitprice, b.i_product_motif, b.i_product FROM tr_product_base a INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product WHERE b.i_product_motif='$iproduct[$inde]' ORDER BY b.i_product_motif DESC ");
				if($qitembarang->num_rows()>0){
					$row2	= $qitembarang->row();
					$iproductbase[$inde]	= $row2->i_product_base; // i_product_base
					$price[$inde]	= $row2->v_unitprice;
					
					$vdogross_new[$inde]	= $ndeliver[$inde]*$price[$inde];
					$this->db->query(" UPDATE tm_do_item SET v_do_gross='$vdogross_new[$inde]' WHERE i_do_item='$idoitem[$inde]' AND i_product='$iproduct[$inde]' ");
				}
						
				$inde+=1;
			}
		}
		
		$this->load->view('listdo/vmainform',$data);		
	}
	
	function refreshitembrg(){
		
		$data['page_title_do']	= $this->lang->line('page_title_do');
		$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
		$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
		$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
		$data['list_nm_cab_do']	= $this->lang->line('list_nm_cab_do');
		$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
		$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
		$data['list_qty_do']	= $this->lang->line('list_qty_do');
		$data['list_sisa_delivery_do']	= $this->lang->line('list_sisa_delivery_do');
		
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['disabled']	= 't';
		
		$ido		= $this->uri->segment(4);
		$icustomer	= $this->uri->segment(5);
		
		$idoitem	= array();
		$iproduct	= array();
		$ndeliver	= array();
		
		$iproductbase	= array();
		$price	= array();
		$vdogross_new	= array();
		
		$inde	= 0;
		
		$qdoitem	= $this->db->query(" SELECT i_do_item, i_product, n_deliver FROM tm_do_item WHERE n_residual > 0 AND f_price_refresh='f' AND i_do='$ido' ");
		
		if($qdoitem->num_rows()>0) {
			
			foreach($qdoitem->result() as $row1) {
				
				$idoitem[$inde]	= $row1->i_do_item;
				
				$iproduct[$inde]	= $row1->i_product; // i_product_motif
				$ndeliver[$inde]	= $row1->n_deliver;

				$qitembarang	= $this->db->query(" SELECT * FROM tr_product_price WHERE i_product_motif='$iproduct[$inde]' AND i_customer='$icustomer' AND f_active='t' LIMIT 1 ");
				
				if($qitembarang->num_rows()>0) {
					$row2	= $qitembarang->row();
					
					$price[$inde]	= $row2->v_price;

					$vdogross_new[$inde]	= $ndeliver[$inde]*$price[$inde];
					
					$this->db->query(" UPDATE tm_do_item SET v_do_gross='$vdogross_new[$inde]' WHERE i_do_item='$idoitem[$inde]' AND i_product='$iproduct[$inde]' ");
				}else{
					$qitembarang	= $this->db->query(" SELECT * FROM tr_product_price WHERE i_product_motif='$iproduct[$inde]' AND i_customer='0' AND f_active='t' LIMIT 1 ");
					if($qitembarang->num_rows()>0) {
						$row2	= $qitembarang->row();
												
						$price[$inde]	= $row2->v_price;

						$vdogross_new[$inde]	= $ndeliver[$inde]*$price[$inde];
						
						$this->db->query(" UPDATE tm_do_item SET v_do_gross='$vdogross_new[$inde]' WHERE i_do_item='$idoitem[$inde]' AND i_product='$iproduct[$inde]' ");						
					}
				}
				
				$inde+=1;
			}
		}
		
		$this->load->view('listdo/vmainform',$data);		
	}
	
	function listop() {
		
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$icust		= $this->uri->segment(6);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['icust']		= $icust;
		
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listdo/mclass');

		$query	= $this->mclass->lop($ibranch,$icust);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listdo/cform/listop/'.$iterasi.'/'.$ibranch.'/'.$icust.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lopperpages($ibranch,$icust,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listdo/vlistformop',$data);
	}

	function listopnext() {
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$icust	= $this->uri->segment(6);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['icust']	= $icust;
		
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listop/mclass');

		$query	= $this->mclass->lop($ibranch,$icust);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listdo/cform/listopnext/'.$iterasi.'/'.$ibranch.'/'.$icust.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lopperpages($ibranch,$icust,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listdo/vlistformop',$data);
	}

	function flistop() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$icust	= $this->input->post('icust');
		
		$iterasi	= $this->uri->segment(4,0);
		//$ibranch	= $this->uri->segment(5,0);
		//$icust	= $this->uri->segment(6,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('listdo/mclass');

		$query	= $this->mclass->flop($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){

				$qhargaperpelanggan = $this->mclass->hargaperpelanggan($row->iproduct,$icust);
				$qhargadefault 	    = $this->mclass->hargadefault($row->iproduct);
						
				if($qhargaperpelanggan->num_rows()>0){
					$rhargaperpelanggan = $qhargaperpelanggan->row();
							
					$hargaperunit = $rhargaperpelanggan->v_price;
					$harga = $row->qtyakhir*$hargaperunit;
							
				}elseif($qhargadefault->num_rows()>0){
					$rhargadefault = $qhargadefault->row();
							
					$hargaperunit = $rhargadefault->v_price;
					$harga = $row->qtyakhir*$hargaperunit;
						
				}else{
					$hargaperunit = $row->unitprice;
					$harga = $row->qtyakhir*$hargaperunit;
				}
				
				/*** 02012012					
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$row->harga','$row->qtyproduk','$row->qtyakhir','$row->unitprice','$row->iop','$row->stp')\">".$row->iopcode."</a></td>	 
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$row->harga','$row->qtyproduk','$row->qtyakhir','$row->unitprice','$row->iop','$row->stp')\">".$row->iproduct."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$row->harga','$row->qtyproduk','$row->qtyakhir','$row->unitprice','$row->iop','$row->stp')\">".$row->qtyproduk."</a></td>
				  <td><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$row->harga','$row->qtyproduk','$row->qtyakhir','$row->unitprice','$row->iop','$row->stp')\">".$row->productname."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$row->harga','$row->qtyproduk','$row->qtyakhir','$row->unitprice','$row->iop','$row->stp')\">".$row->qtyakhir."</a></td>
				 </tr>";
				***/
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->iopcode."</a></td>	 
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->iproduct."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->qtyproduk."</a></td>
				  <td><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->productname."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->qtyakhir."</a></td>
				 </tr>";
				 				
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
		
		echo $item;
	} 

}
?>
