<?php
include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {
	
	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
			$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
			$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
			$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
			$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$data['isi']	='khususprntfpajak/vmainform';
		$this->load->view('template',$data);
		
	}
	
	function carilistpenjualanperdo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$db2=$this->load->database('db_external', TRUE);
		$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
		$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
		$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
		$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
		$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
		$data['list_pjkpenjperdo_no_do']	= $this->lang->line('list_pjkpenjperdo_no_do');
		$data['list_pjkpenjperdo_nm_brg']	= $this->lang->line('list_pjkpenjperdo_nm_brg');
		$data['list_pjkpenjperdo_qty']	= $this->lang->line('list_pjkpenjperdo_qty');
		$data['list_pjkpenjperdo_hjp']	= $this->lang->line('list_pjkpenjperdo_hjp');
		$data['list_pjkpenjperdo_amount']	= $this->lang->line('list_pjkpenjperdo_amount');
		$data['list_pjkpenjperdo_total_pengiriman']	= $this->lang->line('list_pjkpenjperdo_total_pengiriman');
		$data['list_pjkpenjperdo_total_penjualan']	= $this->lang->line('list_pjkpenjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		// 11-10-2012
		$fpengganti	= $this->input->post('fpengganti');
		// end 11-10-2012
		
		$nofaktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->input->get_post('no_faktur');
		$nofaktur = trim($nofaktur);
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->input->get_post('d_do_first');
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->input->get_post('d_do_last');

		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";
		
		$data['tgldomulai']	= $d_do_first; // DD/MM/YYYY
		$data['tgldoakhir']	= $d_do_last;
		
		$data['tgldomulai2']= $n_d_do_first; // YYYY-MM-DD
		$data['tgldoakhir2']= $n_d_do_last;
		
		$data['nofaktur']	= $nofaktur;
		if ($fpengganti == '')
			$fpengganti = 'n';
		$data['fpengganti']	= $fpengganti;
						
		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";
		
		$tahunpajak = date("y");
		// 09-04-2013 ------------------
		$query=$db2->query(" SELECT * FROM tr_format_pajak ");
			if ($query->num_rows() > 0) {
				$hasilrow = $query->row();
				$segmen1 = $hasilrow->segmen1;
				$noawal = $hasilrow->nourut_awal;
				$noakhir = $hasilrow->nourut_akhir;
			}
			
			// 1. cek no fakturnya di tabel tm_nomor_pajak_faktur, apakah udh ada atau blm. Jika blm ada, maka generate nomor urut baru, kemudian insert baru
			// 2. Ambil data terakhir dari tm_nomor_pajak_faktur, cek nomor faktur pajak terakhir

			$query=$db2->query(" SELECT * FROM tm_nomor_pajak_faktur WHERE i_faktur_code like '%".trim($nofaktur)."%' ");
			if ($query->num_rows() == 0) {
				$status = "Faktur pajak belum pernah dicetak";
				
				// ambil no urut faktur pajak terbaru berdasarkan id
				$query3	= $db2->query(" SELECT * FROM tm_nomor_pajak_faktur ORDER BY id DESC LIMIT 1 ");
				if ($query3->num_rows() > 0) {
					$hasilrow3 = $query3->row();
					$last_id	= $hasilrow3->id;
					$last_nomor_pajak	= $hasilrow3->nomor_pajak;
				}
				else {
					$last_id = 0;
					$last_nomor_pajak = '';
				}
				
				if ($last_nomor_pajak != '') {
					$nourutpajak = substr($last_nomor_pajak, 11, 8);
					// 09-07-2013, besok hari ke-1 puasa. 
					//cek noawal. jika noawal > last nomor pajak, maka ambil acuan tebarunya dari noawal. else, last nomor pajak
					// koreksi 30-06-2014, potong 3 digit awal
					$potong4noawal = substr($noawal, 0, 3);
					$potong4nourutpajak = substr($nourutpajak, 0, 3);
					
					//if ($noawal > $nourutpajak) {
					if ($potong4noawal != $potong4nourutpajak) {
						$nourutpajak = $noawal;
						$new_nourutpajak = $nourutpajak;
					}
					else
						$new_nourutpajak = $nourutpajak+1;
				}
				else {
					$nourutpajak = $noawal;
					$new_nourutpajak = $nourutpajak;
				}
				
				if ($nourutpajak == $noakhir) {
					print "<script>alert(\"Nomor urut faktur pajak sudah mencapai jatah maksimum, hubungi kantor pajak.\");</script>";
					//exit;
				}
				else {
					/*if ($last_nomor_pajak != '') {
						$new_nourutpajak = $nourutpajak+1;
					}
					else
						$new_nourutpajak = $noawal; */
						
					$new_id = $last_id+1;
					
					$exp_segmen1 = explode(".",$segmen1);
					$kodeawal1= $exp_segmen1[0];
					$kodeawal2= $exp_segmen1[1];
					
					if ($fpengganti == 'y') {
						$kodeawal = '011';
					}
					else {
						$kodeawal = '010';
					}
					
					$new_segmen1 = $kodeawal.".".$kodeawal2;
					
					$new_nomorpajak = $new_segmen1."-".$tahunpajak.".".$new_nourutpajak;

				/*	$str = array(
						'id'=>$new_id,
						'i_faktur_code'=>trim($nofaktur),
						'nomor_pajak'=>$new_nomorpajak
					);
					$db2->insert('tm_nomor_pajak_faktur',$str); */
				}
				
				$data['nomorpajak'] = $new_nomorpajak;
				$data['status'] = $status;
				$data['input_manual'] = 1;
			}
			else {
				$hasilrow = $query->row();
				$nomor_pajaknya	= $hasilrow->nomor_pajak;
				$status = "Faktur pajak sudah pernah dicetak";
				$data['status'] = $status;
				$data['input_manual'] = 0;
				$data['nomorpajak'] = $nomor_pajaknya;
			}
		//------------------------------
		
		$this->load->model('khususprntfpajak/mclass');
		$data['query']	= $this->mclass->clistfpenjbrgjadiperdo($nofaktur,$n_d_do_first,$n_d_do_last, $fpengganti);
		$data['isi']	='khususprntfpajak/vlistform';
		$this->load->view('template',$data);
		
		
	}
	
	/*function cpenjualanperdo() {
		$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
		$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
		$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
		$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
		$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
		$data['list_pjkpenjperdo_no_do']	= $this->lang->line('list_pjkpenjperdo_no_do');
		$data['list_pjkpenjperdo_nm_brg']	= $this->lang->line('list_pjkpenjperdo_nm_brg');
		$data['list_pjkpenjperdo_qty']	= $this->lang->line('list_pjkpenjperdo_qty');
		$data['list_pjkpenjperdo_hjp']	= $this->lang->line('list_pjkpenjperdo_hjp');
		$data['list_pjkpenjperdo_amount']	= $this->lang->line('list_pjkpenjperdo_amount');
		$data['list_pjkpenjperdo_total_pengiriman']	= $this->lang->line('list_pjkpenjperdo_total_pengiriman');
		$data['list_pjkpenjperdo_total_penjualan']	= $this->lang->line('list_pjkpenjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		$nofaktur	= $this->uri->segment(4); 
		$d_do_first	= $this->uri->segment(5); // YYYY-MM-DD
		$d_do_last	= $this->uri->segment(6);
		$fpengganti	= $this->uri->segment(7);
		
		$exp_ddofirst	= explode("-",$d_do_first,strlen($d_do_first)); // YYYY-MM-DD
		$exp_ddolast	= explode("-",$d_do_last,strlen($d_do_last));
		$new_ddofirst	= !empty($exp_ddofirst[2])?$exp_ddofirst[2].'/'.$exp_ddofirst[1].'/'.$exp_ddofirst[0]:""; // DD-MM-YYYY
		$new_ddolast	= !empty($exp_ddolast[2])?$exp_ddolast[2].'/'.$exp_ddolast[1].'/'.$exp_ddolast[0]:""; // DD-MM-YYYY
		
		$data['tgldomulai']	= $new_ddofirst; // DD/MM/YYYY
		$data['tgldoakhir']	= $new_ddolast;
		$data['tgldomulai2']	= $d_do_first; // YYYY-MM-DD
		$data['tgldoakhir2']	= $d_do_last;
		$data['nofaktur']	= $nofaktur;
		$data['fpengganti']	= $fpengganti;
		
		$this->load->model('prntfpajak_test/mclass');
	
		$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo($nofaktur,$d_do_first,$d_do_last, $fpengganti);
		$this->load->view('prntfpajak_test/vprintform',$data);
	} */
	
	function cpopup() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		//Nomor Seri Faktur Pajak : 010.000.10.00000001
		//011	= pada digit titik pertama jensi faktur, 011 jika faktur pengganti
		//10	= pada digit titik ke-tiga adalah tahun sekarang
		
		$this->load->model('prntfpajak_test/mclass');
		
		$iduserid	= $this->session->userdata('user_idx');
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.194';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;

		$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
		$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
		$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
		$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
		$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
		$data['list_pjkpenjperdo_no_do']	= $this->lang->line('list_pjkpenjperdo_no_do');
		$data['list_pjkpenjperdo_nm_brg']	= $this->lang->line('list_pjkpenjperdo_nm_brg');
		$data['list_pjkpenjperdo_qty']	= $this->lang->line('list_pjkpenjperdo_qty');
		$data['list_pjkpenjperdo_hjp']	= $this->lang->line('list_pjkpenjperdo_hjp');
		$data['list_pjkpenjperdo_amount']	= $this->lang->line('list_pjkpenjperdo_amount');
		$data['list_pjkpenjperdo_total_pengiriman']	= $this->lang->line('list_pjkpenjperdo_total_pengiriman');
		$data['list_pjkpenjperdo_total_penjualan']	= $this->lang->line('list_pjkpenjperdo_total_penjualan');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		$qpenyetorpajak		= $this->mclass->penyetorpajak();
		if($qpenyetorpajak->num_rows()>0) {
			$rpenyetorpajak	= $qpenyetorpajak->row();
			$data['TtdPajak01']	= $rpenyetorpajak->e_penyetor;
		}else{
			$data['TtdPajak01']	= $this->lang->line('TtdPajak01');
		}
		
		$data['TtdPajak02']	= $this->lang->line('TtdPajak02');
		
		$data['falamat']	= $this->lang->line('AddInisial');
				
		$nofaktur	= $this->uri->segment(4); 
		$d_do_first	= $this->uri->segment(5); // YYYY-MM-DD
		$d_do_last	= $this->uri->segment(6);
		$fpengganti	= $this->uri->segment(7);

		$bglobal	= array(
			'01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
			'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
		
		$tanggal= date("d");
		$bulan	= date("m");
		$tahun	= date("Y");
		
		$tgl	= substr($tanggal,0,1)=='0'?substr($tanggal,1,1):$tanggal;
		$nw_tgl	= $tgl;
		$bln	= $bglobal[$bulan];
		
		$data['ftgl']	= $nw_tgl." ".$bln." ".$tahun;
		
		$this->mclass->fprinted($nofaktur);
		
		/*
		$qry_source_remote	= $this->mclass->remote($remote);
		*/
		$qry_source_remote	= $this->mclass->remote($iduserid);

		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;
		
		$qry_infoheader		= $this->mclass->clistfpenjperdo($nofaktur,$d_do_first,$d_do_last);
			
		if($qry_infoheader->num_rows() > 0) {
			
			$row_infoheader	= $qry_infoheader->row();
			$nofaktur		= $row_infoheader->ifakturcode;
			$tglfaktur		= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$tglpajak		= $row_infoheader->dpajak;
			
			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$exp_tpajak		= explode("-",$tglpajak,strlen($tglpajak));	
			
			$tahunpajak	= !empty($exp_tpajak[0])?substr($exp_tpajak[0],2,2):date("y");
			
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];
			
			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']		= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;
		}
		
		$qry_jml	= $this->mclass->clistfpenjperdo_jml($nofaktur,$d_do_first,$d_do_last);
		$qry_totalnyaini	= $this->mclass->clistfpenjperdo_totalnyaini($nofaktur,$d_do_first,$d_do_last);
		
		if($qry_jml->num_rows() > 0) {
			$row_jml = $qry_jml->row_array();		
			$row_totalnyaini = $qry_totalnyaini->row_array();
			$total	 = $row_totalnyaini['totalnyaini'];
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			/*
			$total=0;
			$n_disc=0;
			$v_disc=0;
			foreach($qry_jml->result() as $row){
				$total	 += $row->total;
				$n_disc	 += $row->n_disc;
				$v_disc	 += $row->v_disc;
			}
			*/
			$total_pls_disc	= $total - $v_disc; // DPP
			$nilai_ppn		= (($total_pls_disc*10) / 100 ); // PPN
			$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			
			$data['jumlah']	= $total; 
			$data['dpp']	= $total_pls_disc; // DPP = Dasar Pengenaan Pajak
			$data['diskon']	= $v_disc;
			$data['nilai_ppn']		= $nilai_ppn;
			$data['nilai_faktur']	= round($nilai_faktur);
		} else {
			$data['jumlah']	= 0;
			$data['dpp']	= 0;
			$data['diskon']	= 0;
			$data['nilai_ppn']		= 0;
			$data['nilai_faktur']	= 0;		
		}
		
		$qrypelanggan	= $this->mclass->pelanggan($nofaktur);
		if($qrypelanggan->num_rows() >0) {
			$row_pelanggan		= $qrypelanggan->row_array();
			
			$data['nmkostumer']	= $row_pelanggan['customername'];
			$data['almtkostumer']	= $row_pelanggan['customeraddress'];
			$data['npwp']		= $row_pelanggan['npwp'];
		}
		
		$ititas			= $this->mclass->ititas();
		if($ititas->num_rows() > 0) {
			$row_ititas	= $ititas->row();
			$data['nminitial']	= $row_ititas->e_initial_name;
			$data['almtperusahaan']	= $row_ititas->e_initial_address;
			$data['npwpperusahaan']	= $row_ititas->e_initial_npwp;
		}
		$nopajak		= $this->mclass->pajak($nofaktur);
		if($nopajak->num_rows() > 0) {
			$row_pajak	= $nopajak->row();			
			$nomorpajak	= $row_pajak->ifakturpajak;			
			
			switch(strlen($nomorpajak)) {
				case "1":
					$nfaktur	= '0000000'.$nomorpajak;
				break;
				case "2":
					$nfaktur	= '000000'.$nomorpajak;
				break;
				case "3":
					$nfaktur	= '00000'.$nomorpajak;
				break;
				case "4":
					$nfaktur	= '0000'.$nomorpajak;
				break;
				case "5":
					$nfaktur	= '000'.$nomorpajak;
				break;
				case "6":
					$nfaktur	= '00'.$nomorpajak;
				break;
				case "7":
					$nfaktur	= '0'.$nomorpajak;
				break;
				default:
					$nfaktur	= $nomorpajak;
			}
		/*
		 * maka sekarang format Duta jadi
		010.900-13.31928693 s/d 010.900-13.31929876
		ini no yg di kasih dari kantor pajaknya
		 * 
		 * */
		 
		 // +++++++++++++++++++ ambil data format faktur pajak (khusus faktur >= april 2013) ++++++++++++++++++++++++++++++++++++++
		$query=$db2->query(" SELECT d_faktur FROM tm_faktur_do_t WHERE i_faktur_code='$nofaktur' AND f_faktur_cancel = 'f' ");
		if ($query->num_rows() > 0) {
			$hasilrow = $query->row();
			$d_faktur = $hasilrow->d_faktur;
		}
		else
			$d_faktur = '';
		
		if ($d_faktur != '' && $d_faktur >='2013-04-01') { // mulai dari faktur april 2013
			$query=$db2->query(" SELECT * FROM tr_format_pajak ");
			if ($query->num_rows() > 0) {
				$hasilrow = $query->row();
				$segmen1 = $hasilrow->segmen1;
				$noawal = $hasilrow->nourut_awal;
				$noakhir = $hasilrow->nourut_akhir;
			}
			
			// 1. cek no fakturnya di tabel tm_nomor_pajak_faktur, apakah udh ada atau blm. Jika blm ada, maka generate nomor urut baru, kemudian insert baru
			// 2. Ambil data terakhir dari tm_nomor_pajak_faktur, cek nomor faktur pajak terakhir
			$query=$db2->query(" SELECT * FROM tm_nomor_pajak_faktur WHERE i_faktur_code like '%".trim($nofaktur)."%' ");
			if ($query->num_rows() == 0) {
				// ambil no urut faktur pajak terbaru berdasarkan id
				$query3	= $db2->query(" SELECT * FROM tm_nomor_pajak_faktur ORDER BY id DESC LIMIT 1 ");
				if ($query3->num_rows() > 0) {
					$hasilrow3 = $query3->row();
					$last_id	= $hasilrow3->id;
					$last_nomor_pajak	= $hasilrow3->nomor_pajak;
				}
				else {
					$last_id = 0;
					$last_nomor_pajak = '';
				}
				
				if ($last_nomor_pajak != '') {
					$nourutpajak = substr($last_nomor_pajak, 11, 8);
					// 09-07-2013, besok hari ke-1 puasa. 
					//cek noawal. jika noawal > last nomor pajak, maka ambil acuan tebarunya dari noawal. else, last nomor pajak
					// koreksi 30-06-2014, potong 3 digit awal
					$potong4noawal = substr($noawal, 0, 3);
					$potong4nourutpajak = substr($nourutpajak, 0, 3);
					
					//if ($noawal > $nourutpajak) {
					if ($potong4noawal != $potong4nourutpajak) {
						$nourutpajak = $noawal;
						$new_nourutpajak = $nourutpajak;
					}
					else
						$new_nourutpajak = $nourutpajak+1;
				}
				else {
					$nourutpajak = $noawal;
					$new_nourutpajak = $nourutpajak;
				}
				
				if ($nourutpajak == $noakhir) {
					print "<script>alert(\"Nomor urut faktur pajak sudah mencapai jatah maksimum, hubungi kantor pajak.\");</script>";
					exit;
				}
				else {
					/*if ($last_nomor_pajak != '') {
						// 09-07-2013, besok hari ke-1 puasa. 
						if ($noawal > $nourutpajak)
							$new_nourutpajak = $noawal;
						else
							$new_nourutpajak = $nourutpajak+1;
					}
					else
						$new_nourutpajak = $noawal; */
						
					$new_id = $last_id+1;
					
					$exp_segmen1 = explode(".",$segmen1);
					$kodeawal1= $exp_segmen1[0];
					$kodeawal2= $exp_segmen1[1];
					
					if ($fpengganti == 'y') {
						$kodeawal = '011';
					}
					else {
						$kodeawal = '010';
					}
					
					$new_segmen1 = $kodeawal.".".$kodeawal2;
					
					$new_nomorpajak = $new_segmen1."-".$tahunpajak.".".$new_nourutpajak;
					//echo $new_nomorpajak; die();

					$str = array(
						'id'=>$new_id,
						'i_faktur_code'=>$nofaktur,
						'nomor_pajak'=>$new_nomorpajak
					);
					$db2->insert('tm_nomor_pajak_faktur',$str);
				}
				
				$data['nomorpajak'] = $new_nomorpajak;
			}
			else {
				$hasilrow = $query->row();
				$nomor_pajaknya	= $hasilrow->nomor_pajak;
				$data['nomorpajak'] = $nomor_pajaknya;
			}
		 } // end if
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		 else {
			if ($fpengganti == 'y') {
				$kodeawal = '011';
			}
			else {
				$kodeawal = '010';
			}
			$data['nomorpajak']	= $kodeawal."."."000"."-".$tahunpajak.".".$nfaktur;
		 }
		} // end if nopajak > 0
		else
			$data['nomorpajak'] = '';
			
		$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo2($nofaktur,$d_do_first,$d_do_last);
		
		$this->load->view('prntfpajak_test/vtestform',$data);
	}
	
	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('khususprntfpajak/mclass');
		
		$fpengganti	= $this->uri->segment(4); 

		$query	= $this->mclass->lbarangjadi($fpengganti);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/khususprntfpajak/cform/listbarangjadinext/'.$fpengganti;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'], $fpengganti);			

		$this->load->view('khususprntfpajak/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('khususprntfpajak/mclass');
		$fpengganti	= $this->uri->segment(4); 
		$query	= $this->mclass->lbarangjadi($fpengganti);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/khususprntfpajak/cform/listbarangjadinext/'.$fpengganti;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'], $fpengganti);				
		
		$this->load->view('khususprntfpajak/vlistformbrgjadi',$data);
	}
	
	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('khususprntfpajak/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
		
		echo $item;
	}
	
	// 11-12-2013
	function cetakfakturpajak() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$db2=$this->load->database('db_external', TRUE);
		//Nomor Seri Faktur Pajak : 010.000.10.00000001
		//011	= pada digit titik pertama jensi faktur, 011 jika faktur pengganti
		//10	= pada digit titik ke-tiga adalah tahun sekarang
		
		$this->load->model('khususprntfpajak/mclass');
		
		/*$iduserid	= $this->session->userdata('user_idx');
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.194';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate; */

		$data['page_title_pjkpenjualanperdo']	= $this->lang->line('page_title_pjkpenjualanperdo');
		$data['form_title_detail_pjkpenjualanperdo']	= $this->lang->line('form_title_detail_pjkpenjualanperdo');
		$data['list_pjkpenjperno_faktur']	= $this->lang->line("list_pjkpenjperno_faktur");
		$data['list_pjkpenjperdo_kd_brg']	= $this->lang->line('list_pjkpenjperdo_kd_brg');
		$data['list_pjkpenjperdo_tgl_mulai']	= $this->lang->line('list_pjkpenjperdo_tgl_mulai');
		$data['list_pjkpenjperdo_no_do']	= $this->lang->line('list_pjkpenjperdo_no_do');
		$data['list_pjkpenjperdo_nm_brg']	= $this->lang->line('list_pjkpenjperdo_nm_brg');
		$data['list_pjkpenjperdo_qty']	= $this->lang->line('list_pjkpenjperdo_qty');
		$data['list_pjkpenjperdo_hjp']	= $this->lang->line('list_pjkpenjperdo_hjp');
		$data['list_pjkpenjperdo_amount']	= $this->lang->line('list_pjkpenjperdo_amount');
		$data['list_pjkpenjperdo_total_pengiriman']	= $this->lang->line('list_pjkpenjperdo_total_pengiriman');
		$data['list_pjkpenjperdo_total_penjualan']	= $this->lang->line('list_pjkpenjperdo_total_penjualan');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		
		$qpenyetorpajak		= $this->mclass->penyetorpajak();
		if($qpenyetorpajak->num_rows()>0) {
			$rpenyetorpajak	= $qpenyetorpajak->row();
			$data['TtdPajak01']	= $rpenyetorpajak->e_penyetor;
		}else{
			$data['TtdPajak01']	= $this->lang->line('TtdPajak01');
		}
		
		$data['TtdPajak02']	= $this->lang->line('TtdPajak02');
		
		$data['falamat']	= $this->lang->line('AddInisial');
				
		$nofaktur	= trim($this->uri->segment(4)); 
		$d_do_first	= $this->uri->segment(5); // YYYY-MM-DD
		$d_do_last	= $this->uri->segment(6);
		$fpengganti	= $this->uri->segment(7);

		$bglobal	= array(
			'01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
			'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
		
		$tanggal= date("d");
		$bulan	= date("m");
		$tahun	= date("Y");
		
		$tgl	= substr($tanggal,0,1)=='0'?substr($tanggal,1,1):$tanggal;
		$nw_tgl	= $tgl;
		$bln	= $bglobal[$bulan];
		
		$data['ftgl']	= $nw_tgl." ".$bln." ".$tahun;
		
		$this->mclass->fprinted($nofaktur);
		
		/*
		$qry_source_remote	= $this->mclass->remote($remote);
		*/
	/*	$qry_source_remote	= $this->mclass->remote($iduserid);

		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile; */
		
		$qry_infoheader		= $this->mclass->clistfpenjperdo($nofaktur,$d_do_first,$d_do_last);
			
		if($qry_infoheader->num_rows() > 0) {
			
			$row_infoheader	= $qry_infoheader->row();
			$nofaktur		= $row_infoheader->ifakturcode;
			$tglfaktur		= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$tglpajak		= $row_infoheader->dpajak;
			
			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$exp_tpajak		= explode("-",$tglpajak,strlen($tglpajak));	
			
			$tahunpajak	= !empty($exp_tpajak[0])?substr($exp_tpajak[0],2,2):date("y");
			
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];
			
			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']		= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;
		}
		
		$qry_jml	= $this->mclass->clistfpenjperdo_jml($nofaktur,$d_do_first,$d_do_last);
		$qry_totalnyaini	= $this->mclass->clistfpenjperdo_totalnyaini($nofaktur,$d_do_first,$d_do_last);
		
		if($qry_jml->num_rows() > 0) {
			$row_jml = $qry_jml->row_array();		
			$row_totalnyaini = $qry_totalnyaini->row_array();
			$total	 = $row_totalnyaini['totalnyaini'];
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			/*
			$total=0;
			$n_disc=0;
			$v_disc=0;
			foreach($qry_jml->result() as $row){
				$total	 += $row->total;
				$n_disc	 += $row->n_disc;
				$v_disc	 += $row->v_disc;
			}
			*/
			$total_pls_disc	= $total - $v_disc; // DPP
			$nilai_ppn		= (($total_pls_disc*10) / 100 ); // PPN
			$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			
			$data['jumlah']	= $total; 
			$data['dpp']	= $total_pls_disc; // DPP = Dasar Pengenaan Pajak
			$data['diskon']	= $v_disc;
			$data['nilai_ppn']		= $nilai_ppn;
			$data['nilai_faktur']	= round($nilai_faktur);
		} else {
			$data['jumlah']	= 0;
			$data['dpp']	= 0;
			$data['diskon']	= 0;
			$data['nilai_ppn']		= 0;
			$data['nilai_faktur']	= 0;		
		}
		
		$qrypelanggan	= $this->mclass->pelanggan($nofaktur);
		if($qrypelanggan->num_rows() >0) {
			$row_pelanggan		= $qrypelanggan->row_array();
			
			$data['nmkostumer']	= $row_pelanggan['customername'];
			$data['almtkostumer']	= $row_pelanggan['customeraddress'];
			$data['npwp']		= $row_pelanggan['npwp'];
		}
		
		$ititas			= $this->mclass->ititas();
		if($ititas->num_rows() > 0) {
			$row_ititas	= $ititas->row();
			$data['nminitial']	= $row_ititas->e_initial_name;
			$data['almtperusahaan']	= $row_ititas->e_initial_address;
			$data['npwpperusahaan']	= $row_ititas->e_initial_npwp;
		}
		$nopajak		= $this->mclass->pajak($nofaktur);
		if($nopajak->num_rows() > 0) {
			$row_pajak	= $nopajak->row();			
			$nomorpajak	= $row_pajak->ifakturpajak;			
			
			switch(strlen($nomorpajak)) {
				case "1":
					$nfaktur	= '0000000'.$nomorpajak;
				break;
				case "2":
					$nfaktur	= '000000'.$nomorpajak;
				break;
				case "3":
					$nfaktur	= '00000'.$nomorpajak;
				break;
				case "4":
					$nfaktur	= '0000'.$nomorpajak;
				break;
				case "5":
					$nfaktur	= '000'.$nomorpajak;
				break;
				case "6":
					$nfaktur	= '00'.$nomorpajak;
				break;
				case "7":
					$nfaktur	= '0'.$nomorpajak;
				break;
				default:
					$nfaktur	= $nomorpajak;
			}
		/*
		 * maka sekarang format Duta jadi
		010.900-13.31928693 s/d 010.900-13.31929876
		ini no yg di kasih dari kantor pajaknya
		 * 
		 * */
		 
		 // +++++++++++++++++++ ambil data format faktur pajak (khusus faktur >= april 2013) ++++++++++++++++++++++++++++++++++++++
		$query=$db2->query(" SELECT d_faktur FROM tm_faktur_do_t WHERE i_faktur_code='$nofaktur' AND f_faktur_cancel = 'f' ");
		if ($query->num_rows() > 0) {
			$hasilrow = $query->row();
			$d_faktur = $hasilrow->d_faktur;
		}
		else
			$d_faktur = '';
		
		if ($d_faktur != '' && $d_faktur >='2013-04-01') { // mulai dari faktur april 2013
			$query=$db2->query(" SELECT * FROM tr_format_pajak ");
			if ($query->num_rows() > 0) {
				$hasilrow = $query->row();
				$segmen1 = $hasilrow->segmen1;
				$noawal = $hasilrow->nourut_awal;
				$noakhir = $hasilrow->nourut_akhir;
			}
			
			// 1. cek no fakturnya di tabel tm_nomor_pajak_faktur, apakah udh ada atau blm. Jika blm ada, maka generate nomor urut baru, kemudian insert baru
			// 2. Ambil data terakhir dari tm_nomor_pajak_faktur, cek nomor faktur pajak terakhir
			$query=$db2->query(" SELECT * FROM tm_nomor_pajak_faktur WHERE i_faktur_code like '%".trim($nofaktur)."%' ");
			if ($query->num_rows() == 0) {
				// ambil no urut faktur pajak terbaru berdasarkan id
				$query3	= $db2->query(" SELECT * FROM tm_nomor_pajak_faktur ORDER BY id DESC LIMIT 1 ");
				if ($query3->num_rows() > 0) {
					$hasilrow3 = $query3->row();
					$last_id	= $hasilrow3->id;
					$last_nomor_pajak	= $hasilrow3->nomor_pajak;
				}
				else {
					$last_id = 0;
					$last_nomor_pajak = '';
				}
				
				if ($last_nomor_pajak != '') {
					$nourutpajak = substr($last_nomor_pajak, 11, 8);
					// 09-07-2013, besok hari ke-1 puasa. 
					//cek noawal. jika noawal > last nomor pajak, maka ambil acuan tebarunya dari noawal. else, last nomor pajak
					// koreksi 30-06-2014, potong 3 digit awal
					$potong4noawal = substr($noawal, 0, 3);
					$potong4nourutpajak = substr($nourutpajak, 0, 3);
					
					//if ($noawal > $nourutpajak) {
					if ($potong4noawal != $potong4nourutpajak) {
						$nourutpajak = $noawal;
						$new_nourutpajak = $nourutpajak;
					}
					else
						$new_nourutpajak = $nourutpajak+1;
				}
				else {
					$nourutpajak = $noawal;
					$new_nourutpajak = $nourutpajak;
				}
				
				if ($nourutpajak == $noakhir) {
					print "<script>alert(\"Nomor urut faktur pajak sudah mencapai jatah maksimum, hubungi kantor pajak.\");</script>";
					exit;
				}
				else {
					/*if ($last_nomor_pajak != '') {
						// 09-07-2013, besok hari ke-1 puasa. 
						if ($noawal > $nourutpajak)
							$new_nourutpajak = $noawal;
						else
							$new_nourutpajak = $nourutpajak+1;
					}
					else
						$new_nourutpajak = $noawal; */
						
					$new_id = $last_id+1;
					
					$exp_segmen1 = explode(".",$segmen1);
					$kodeawal1= $exp_segmen1[0];
					$kodeawal2= $exp_segmen1[1];
					
					if ($fpengganti == 'y') {
						$kodeawal = '011';
					}
					else {
						$kodeawal = '010';
					}
					
					$new_segmen1 = $kodeawal.".".$kodeawal2;
					
					$new_nomorpajak = $new_segmen1."-".$tahunpajak.".".$new_nourutpajak;
					//echo $new_nomorpajak; die();

					$str = array(
						'id'=>$new_id,
						'i_faktur_code'=>$nofaktur,
						'nomor_pajak'=>$new_nomorpajak
					);
					$db2->insert('tm_nomor_pajak_faktur',$str);
				}
				
				$data['nomorpajak'] = $new_nomorpajak;
			}
			else {
				$hasilrow = $query->row();
				$nomor_pajaknya	= $hasilrow->nomor_pajak;
				$data['nomorpajak'] = $nomor_pajaknya;
			}
		 } // end if
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		 else {
			if ($fpengganti == 'y') {
				$kodeawal = '011';
			}
			else {
				$kodeawal = '010';
			}
			$data['nomorpajak']	= $kodeawal."."."000"."-".$tahunpajak.".".$nfaktur;
		 }
		} // end if nopajak > 0
		else
			$data['nomorpajak'] = '';
			
		$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo2($nofaktur,$d_do_first,$d_do_last);
		
		$this->load->view('khususprntfpajak/vprintfakturpajak',$data);
	}
}
?>
