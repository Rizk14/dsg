<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title'] = $this->lang->line('neraca');
			$data['periode']	= '';
			$data['isi']='akt-neraca/vmainform';		
			$this->load->view('template',$data);
			
		
	}
	function view()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$periode	= $this->uri->segment(4);
			if(
				($periode!='')
			  )
			{
				$this->load->model('akt-neraca/mmaster');
				$data['page_title'] = $this->lang->line('neraca');
#				$tmp 	= explode("-", $periode);
#				$tahun	= $tmp[2];
#				$bulan	= $tmp[1];
#				$tanggal= $tmp[0];
        $tahun=substr($periode,0,4);
        $bulan=substr($periode,4,2);
        $tahun2=substr($periode,0,4);
        $tanggal='01';        
				$periode=$tahun.$bulan;
				$kiwari		= $tahun."/".$bulan."/01";
				$namabulan	= $this->mmaster->NamaBulan($bulan);
				$data['periode']	= $periode;
				$dfrom				= $tahun."-".$bulan."-01";
				$dto				= $tahun."-".$bulan."-".$tanggal;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $tahun."-".$bulan."-".$tanggal;
				$data['tanggal']	= $tanggal;
				$data['namabulan']	= $namabulan;
				$data['tahun']		= $tahun;
				$data['kas']		= $this->mmaster->bacakas($periode);
				$data['piutang']	= $this->mmaster->bacapiutang($periode);
				$data['hutangdagang']= $this->mmaster->bacahutangdagang($periode);
				$data['modal']		= $this->mmaster->bacamodal($periode);
				//~ $data['giromundur']		= $this->mmaster->bacapiutanggiromundur($periode);
				$data['piutangkaryawan']		= $this->mmaster->bacapiutangkaryawan($periode);
				$data['piutanglainlain']		= $this->mmaster->bacapiutanglainlain($periode);
				$data['biayadibayardimuka']		= $this->mmaster->bacabiayadibayardimuka($periode);
				$data['persediaanbarangdagang']	= $this->mmaster->bacapersediaanbarangdagang($periode);
				$data['possementaradebet']		= $this->mmaster->bacapossementaradebet($periode);
				$data['aktivatetapkel1']		= $this->mmaster->bacaaktivatetapkel1($periode);
				$data['akumaktiva1']			= $this->mmaster->bacaakumaktiva1($periode);
				$data['aktivatetapkel2']		= $this->mmaster->bacaaktivatetapkel2($periode);
				$data['akumaktiva2']			= $this->mmaster->bacaakumaktiva2($periode);
				$data['akumamortisasi']			= $this->mmaster->bacaakumamortisasi($periode);
				$data['biayayangditangguhkan']	= $this->mmaster->bacabiayayangditangguhkan($periode);
				$data['akumamortisasiyangditangguhkan']	= $this->mmaster->bacaakumamortisasiyangditangguhkan($periode);
				$data['hutangbank']				= $this->mmaster->bacahutangbank($periode);
				$data['hutangpajakpph21']		= $this->mmaster->bacahutangpajakpph21($periode);
				$data['hutangpajakpph22']		= $this->mmaster->bacahutangpajakpph22($periode);
				$data['hutangpajakpph23']		= $this->mmaster->bacahutangpajakpph23($periode);
				$data['hutangpajakpph24']		= $this->mmaster->bacahutangpajakpph24($periode);
				$data['hutangpajakppn']			= $this->mmaster->bacahutangpajakppn($periode);
				$data['hutanglain']				= $this->mmaster->bacahutanglain($periode);
				$data['possementara']			= $this->mmaster->bacapossementara($periode);
				$data['hutangbankjangkapanjang']= $this->mmaster->bacahutangbankjangkapanjang($periode);
				$data['modalyangdisetor']		= $this->mmaster->bacamodalyangdisetor($periode);
				$data['labarugiyangditahantahunberjalan']		= $this->mmaster->bacalabarugiyangditahantahunberjalan($periode);
				$data['labarugiyangditahan']					= $this->mmaster->bacalabarugiyangditahan($periode);
				$data['labarugiyangditahanbulanberjalan']		= $this->mmaster->bacalabarugiyangditahanbulanberjalan($periode);
				$data['pajakpph21ydm']		= $this->mmaster->bacapajakpph21ydm($periode);
				$data['pajakpph22ydm']		= $this->mmaster->bacapajakpph22ydm($periode);
				$data['pajakpph23ydm']		= $this->mmaster->bacapajakpph23ydm($periode);
				$data['pajakpph25ydm']		= $this->mmaster->bacapajakpph25ydm($periode);
				$data['uangmukaleasing']	= $this->mmaster->bacauangmukaleasing($periode);
				$data['yhmdibayar']			= $this->mmaster->bacayhmdibayar($periode);

	      //~ $sess=$this->session->userdata('session_id');
	      //~ $id=$this->session->userdata('user_id');
	      //~ $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      //~ $rs		= pg_query($sql);
	      //~ if(pg_num_rows($rs)>0){
		      //~ while($row=pg_fetch_assoc($rs)){
			      //~ $ip_address	  = $row['ip_address'];
			      //~ break;
		      //~ }
	      //~ }else{
		      //~ $ip_address='kosong';
	      //~ }
	      //~ $query 	= pg_query("SELECT current_timestamp as c");
	      //~ while($row=pg_fetch_assoc($query)){
		      //~ $now	  = $row['c'];
	      //~ }
	      //~ $pesan='Buka Laporan Neraca periode:'.$periode;
	      //~ $this->load->model('logger');
	      //~ $this->logger->write($id, $ip_address, $now , $pesan );
			$data['isi']='akt-neraca/vmainform';		
			$this->load->view('template',$data);
			
		
	}
}
}
?>
