<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-perubahan-harga-wip-pck/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$data['list_unit_packing']=$this->mmaster->get_unit_packing();
	
	$data['isi'] = 'info-perubahan-harga-wip-pck/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $id_unit_packing = $this->input->post('id_unit_packing',TRUE);
    $data['isi'] = 'info-perubahan-harga-wip-pck/vformview';
	/*$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);   */
	$bulan_dari = $this->input->post('bulan_dari', TRUE);
	$bulan_ke = $this->input->post('bulan_ke', TRUE);
	$tahun_dari = $this->input->post('tahun_dari', TRUE);
	$tahun_ke = $this->input->post('tahun_ke', TRUE);
	
	$nama_unit_packing=$this->mmaster->get_nama_unit_packing($id_unit_packing);
	
	$cek_rekap = $this->mmaster->cek_rekap_harga($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_unit_packing);		
	if (count($cek_rekap) == 0) {
		
		$jum_total = $this->mmaster->get_rekap_harga($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_unit_packing);
		
		$data['query'] = $this->mmaster->get_rekap_harga($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_unit_packing);
		$is_save = 0;
	}
	else {
	
		$jum_total = $this->mmaster->get_rekap_hargatanpalimit($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_unit_packing);
		
		$data['query'] = $this->mmaster->get_rekap_harga($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_unit_packing);
		$is_save = 1;
			
	} // end else
	
	
	if ($bulan_dari == '01')
						$nama_bln_dari = "Januari";
					else if ($bulan_dari == '02')
						$nama_bln_dari = "Februari";
					else if ($bulan_dari == '03')
						$nama_bln_dari = "Maret";
					else if ($bulan_dari == '04')
						$nama_bln_dari = "April";
					else if ($bulan_dari == '05')
						$nama_bln_dari = "Mei";
					else if ($bulan_dari == '06')
						$nama_bln_dari = "Juni";
					else if ($bulan_dari == '07')
						$nama_bln_dari = "Juli";
					else if ($bulan_dari == '08')
						$nama_bln_dari = "Agustus";
					else if ($bulan_dari == '09')
						$nama_bln_dari = "September";
					else if ($bulan_dari == '10')
						$nama_bln_dari = "Oktober";
					else if ($bulan_dari == '11')
						$nama_bln_dari = "November";
					else if ($bulan_dari == '12')
						$nama_bln_dari = "Desember";
						
	if ($bulan_ke == '01')
						$nama_bln_ke = "Januari";
					else if ($bulan_ke == '02')
						$nama_bln_ke = "Februari";
					else if ($bulan_ke == '03')
						$nama_bln_ke = "Maret";
					else if ($bulan_ke == '04')
						$nama_bln_ke = "April";
					else if ($bulan_ke == '05')
						$nama_bln_ke = "Mei";
					else if ($bulan_ke == '06')
						$nama_bln_ke = "Juni";
					else if ($bulan_ke == '07')
						$nama_bln_ke = "Juli";
					else if ($bulan_ke == '08')
						$nama_bln_ke = "Agustus";
					else if ($bulan_ke == '09')
						$nama_bln_ke = "September";
					else if ($bulan_ke == '10')
						$nama_bln_ke = "Oktober";
					else if ($bulan_ke == '11')
						$nama_bln_ke = "November";
					else if ($bulan_ke == '12')
						$nama_bln_ke = "Desember";	
	
	if ($jum_total == '')
		$data['jum_total'] = 0;
	else
		$data['jum_total'] = count($jum_total);
		
	$data['nama_unit_packing']	=	$nama_unit_packing;
	$data['nama_bln_dari'] = $nama_bln_dari;
	$data['nama_bln_ke'] = $nama_bln_ke;
	$data['bulan_dari'] = $bulan_dari;
	$data['bulan_ke'] = $bulan_ke;
	$data['tahun_dari'] = $tahun_dari;
	$data['tahun_ke'] = $tahun_ke;
	$data['is_save'] = $is_save;
	//$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  
  
}
