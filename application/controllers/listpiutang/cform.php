<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_piutang']			= $this->lang->line('page_title_piutang');
			$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
			$data['list_piutang_no_kontrabon']	= $this->lang->line('list_piutang_no_kontrabon');
			$data['list_piutang_tgl_kontrabon']	= $this->lang->line('list_piutang_tgl_kontrabon');
			$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
			$data['list_piutang_total_kontrabon']	= $this->lang->line('list_piutang_total_kontrabon');
			$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
			$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
			$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
			$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
			$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
			$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
			$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
			$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
			$data['list_piutang_nilai_kontrabon'] = $this->lang->line('list_piutang_nilai_kontrabon');
			$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
			$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi']	= 'listpiutang/vmainform';	
			$this->load->view('template',$data);	
			
			
	}
	
	function carilistkontrabon() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_piutang']		= $this->lang->line('page_title_piutang');
		$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
		$data['list_piutang_no_kontrabon']	= $this->lang->line('list_piutang_no_kontrabon');
		$data['list_piutang_tgl_kontrabon']	= $this->lang->line('list_piutang_tgl_kontrabon');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_piutang_total_kontrabon']	= $this->lang->line('list_piutang_total_kontrabon');
		$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
		$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
		$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
		$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_nilai_kontrabon']	= $this->lang->line('list_piutang_nilai_kontrabon');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_kontrabon_nota_sederhana']	= $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpiutang']	= "";		
		$data['limages']	= base_url();
		
		$no_kontrabon	= $this->input->post('no_kontrabon');
		$i_kontrabon	= $this->input->post('i_kontrabon');
		$dkontrabonfirst= $this->input->post('d_kontrabon_first');
		$dkontrabonlast	= $this->input->post('d_kontrabon_last');
		
		$tf_nota_sederhana	= $this->input->post('tf_nota_sederhana');
		
		$data['tglkontrabonmulai']	= (!empty($dkontrabonfirst) && $dkontrabonfirst!='0')?$dkontrabonfirst:'';
		$data['tglkontrabonakhir']	= (!empty($dkontrabonlast) && $dkontrabonlast!='0')?$dkontrabonlast:'';
		$data['nokontrabon']		= (!empty($no_kontrabon) && $no_kontrabon!='0')?$no_kontrabon:'';
		$data['ikontrabon']		= (!empty($i_kontrabon) && $i_kontrabon!='0')?$i_kontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
				
		$e_d_kontrabon_first= explode("/",$dkontrabonfirst,strlen($dkontrabonfirst));
		$e_d_kontrabon_last	= explode("/",$dkontrabonlast,strlen($dkontrabonlast));
		
		$ndkontrabonfirst	= !empty($dkontrabonfirst)?$e_d_kontrabon_first[2].'-'.$e_d_kontrabon_first[1].'-'.$e_d_kontrabon_first[0]:'0';
		$ndkontrabonlast	= !empty($dkontrabonlast)?$e_d_kontrabon_last[2].'-'.$e_d_kontrabon_last[1].'-'.$e_d_kontrabon_last[0]:'0';
		
		$turi1	= ($no_kontrabon!='' && $no_kontrabon!='0')?$no_kontrabon:'0';
		$turi2	= ($i_kontrabon!='' && $i_kontrabon!='0')?$i_kontrabon:'0';
		$turi3	= $ndkontrabonfirst;
		$turi4	= $ndkontrabonlast;
		$turi5	= $tf_nota_sederhana;
		
		$this->load->model('listpiutang/mclass');
		
		$pagination['base_url'] 	= '/listpiutang/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if($dkontrabonfirst!='' && $dkontrabonlast!='') {
			$qlistkontrabonallpage	= $this->mclass->clistkontrabonallpage1($i_kontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['template'] = 1;
		}else{
			$qlistkontrabonallpage	= $this->mclass->clistkontrabonallpage2($i_kontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['template'] = 2;
		}
		
		$pagination['total_rows']	= $qlistkontrabonallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dkontrabonfirst!='' && $dkontrabonlast!='') {
			$data['query']	= $this->mclass->clistkontrabon1($pagination['per_page'],$pagination['cur_page'],$i_kontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}else{
			$data['query']	= $this->mclass->clistkontrabon2($pagination['per_page'],$pagination['cur_page'],$i_kontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}
		
		$jmltotalfaktur = 0;
		$qvtotalfaktur	= $this->mclass->vtotalfaktur($pagination['per_page'],$pagination['cur_page'],$i_kontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);

		if(sizeof($qvtotalfaktur)) {
			foreach($qvtotalfaktur as $vtotalfaktur) {
				$jmltotalfaktur	= $jmltotalfaktur+($vtotalfaktur->v_total_fppn);
			}
			$data['totalfaktur2'] = $jmltotalfaktur;
		}else{
			$data['totalfaktur2'] = array();
		}
		$data['isi']	= 'listpiutang/vlistform';	
			$this->load->view('template',$data);	
		
	}

	function carilistkontrabonnext() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_piutang']		= $this->lang->line('page_title_piutang');
		$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
		$data['list_piutang_no_kontrabon']	= $this->lang->line('list_piutang_no_kontrabon');
		$data['list_piutang_tgl_kontrabon']	= $this->lang->line('list_piutang_tgl_kontrabon');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_piutang_total_kontrabon']	= $this->lang->line('list_piutang_total_kontrabon');
		$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
		$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
		$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
		$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_nilai_kontrabon']	= $this->lang->line('list_piutang_nilai_kontrabon');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_kontrabon_nota_sederhana']	= $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpiutang']	= "";		
		$data['limages']	= base_url();

		$no_kontrabon	= $this->uri->segment(4);
		$i_kontrabon	= $this->uri->segment(5);
		$dkontrabonfirst= $this->uri->segment(6);
		$dkontrabonlast	= $this->uri->segment(7);
		$tf_nota_sederhana = $this->uri->segment(8); 
		
		$e_d_kontrabon_first = ($dkontrabonfirst!='0')?explode("-",$dkontrabonfirst,strlen($dkontrabonfirst)):'';
		$e_d_kontrabon_last	 = ($dkontrabonlast!='0')?explode("-",$dkontrabonlast,strlen($dkontrabonlast)):'';

		$ndkontrabonfirst	= $dkontrabonfirst!='0'?$e_d_kontrabon_first[2].'/'.$e_d_kontrabon_first[1].'/'.$e_d_kontrabon_first[0]:'0';
		$ndkontrabonlast	= $dkontrabonlast!='0'?$e_d_kontrabon_last[2].'/'.$e_d_kontrabon_last[1].'/'.$e_d_kontrabon_last[0]:'0';
						
		$data['tglkontrabonmulai']	= (!empty($ndkontrabonfirst) && $ndkontrabonfirst!='0')?$ndkontrabonfirst:'';
		$data['tglkontrabonakhir']	= (!empty($ndkontrabonlast) && $ndkontrabonlast!='0')?$ndkontrabonlast:'';
		$data['nokontrabon']	= (!empty($no_kontrabon) && $no_kontrabon!='0')?$no_kontrabon:'';
		$data['ikontrabon']		= (!empty($i_kontrabon) && $i_kontrabon!='0')?$i_kontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
				
		$turi1	= ($no_kontrabon!='' && $no_kontrabon!='0')?$no_kontrabon:'0';
		$turi2	= ($i_kontrabon!='' && $i_kontrabon!='0')?$i_kontrabon:'0';
		$turi3	= $dkontrabonfirst;
		$turi4	= $dkontrabonlast;
		$turi5	= $tf_nota_sederhana;
		
		$this->load->model('listpiutang/mclass');
		
		$pagination['base_url'] 	= '/listpiutang/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if($dkontrabonfirst!='0' && $dkontrabonlast!='0') {
			$qlistkontrabonallpage	= $this->mclass->clistkontrabonallpage1($i_kontrabon,$dkontrabonfirst,$dkontrabonlast,$tf_nota_sederhana);
		}else{
			$qlistkontrabonallpage	= $this->mclass->clistkontrabonallpage2($i_kontrabon,$dkontrabonfirst,$dkontrabonlast,$tf_nota_sederhana);
		}
		
		$pagination['total_rows']	= $qlistkontrabonallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dkontrabonfirst!='0' && $dkontrabonlast!='0') {
			$data['isi']	= $this->mclass->clistkontrabon1($pagination['per_page'],$pagination['cur_page'],$i_kontrabon,$dkontrabonfirst,$dkontrabonlast,$tf_nota_sederhana);
			$data['template'] = 1;
		}else{
			$data['isi']	= $this->mclass->clistkontrabon2($pagination['per_page'],$pagination['cur_page'],$i_kontrabon,$dkontrabonfirst,$dkontrabonlast,$tf_nota_sederhana);
			$data['template'] = 2;
		}
		
		$jmltotalfaktur = 0;
		$qvtotalfaktur	= $this->mclass->vtotalfaktur($pagination['per_page'],$pagination['cur_page'],$i_kontrabon,$dkontrabonfirst,$dkontrabonlast,$tf_nota_sederhana);
		foreach($qvtotalfaktur as $vtotalfaktur) {
			$jmltotalfaktur	= $jmltotalfaktur+($vtotalfaktur->v_total_fppn);
		}
		$data['totalfaktur2'] = $jmltotalfaktur;

		$this->load->view('listpiutang/vlistform',$data);
					
	}
	
	function listkontrabon() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana	= $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		$this->load->model('listpiutang/mclass');

		$query	= $this->mclass->lkontrabon($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpiutang/cform/listkontrabonnext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lkontrabonperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);		
				
		$this->load->view('listpiutang/vlistkontrabon',$data);			
	}

	function listkontrabonnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana	= $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		$this->load->model('listpiutang/mclass');

		$query	= $this->mclass->lkontrabon($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpiutang/cform/listkontrabonnext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lkontrabonperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);		
				
		$this->load->view('listpiutang/vlistkontrabon',$data);			
	}		

	function flistkontrabon() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$fnotasederhana = $this->input->post('fnotasederhana')?$this->input->post('fnotasederhana'):$this->input->get_post('fnotasederhana');
		
		$data['page_title']	= "KONTRA BON";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('listpiutang/mclass');

		$query	= $this->mclass->flkontrabon($key,$fnotasederhana);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt')\">".$row->i_dt_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt')\">".$row->d_dt."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}

}
?>
