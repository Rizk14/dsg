<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-opname-hutang/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['isi'] = 'info-opname-hutang/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $data['isi'] = 'info-opname-hutang/vformview';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	if ($date_from == '' && $date_to == '') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
	}	

    $jum_total = $this->mmaster->get_opname_hutangtanpalimit($date_from, $date_to);
						/*	$config['base_url'] = base_url().'index.php/info-opname-hutang/cform/view/index/'.$date_from.'/'.$date_to.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						*/
	//$data['query'] = $this->mmaster->get_opname_hutang($config['per_page'],$this->uri->segment(7), $date_from, $date_to);
	// sementara ga pake paging, tergantung org Duta
	$data['query'] = $this->mmaster->get_opname_hutang($date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	//$data['cari'] = $keywordcari;
	//$data['list_supplier'] = $this->mmaster->get_supplier();
	//$data['csupplier'] = $csupplier;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$this->load->view('template',$data);
  }
  
  // 13-04-2012
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_opname_hutang($date_from, $date_to);
		
		// ======== start html_data =================
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
			<thead>
			<tr>
			<th colspan='6' align='center'>LAPORAN OPNAME HUTANG DAGANG</th>
		 </tr>
		 <tr>
			<th colspan='6' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
			 <tr>
				<th width='5%'>No</th>
				<th width='20%'>Tgl SJ</th>
				 <th width='20%'>Nomor SJ</th>
				 <th>Supplier</th>
				 <th width='20%'>Jumlah (Rp.)</th>
				 <th>Last Update</th>
			 </tr>
			</thead>
			<tbody>
		";
		
		if (is_array($query)) {
			  $total_persupplier = 0; $grand_total = 0;
			  $urut_per_supplier = 0; $tot_retur = 0; 
			  
			 for($j=0;$j<count($query);$j++){
				 
				/*
				 * 1. query semua data retur berdasarkan tanggal awal dari data yg dihasilkan di query opname. 
				 * ( faktur_date_from >= tgl_awal). Tempatkan query di awal perulangan pada form view.
				   2. Hitung sum retur detailnya tiap2 data hasil query di tm_retur_beli. Hasil sum tiap2 data retur_beli dijumlahkan.
				   3. Dari hasil sum itu, bandingkan dgn tiap2 nominal faktur. Jika selisihnya dgn faktur masih ada sisa 100.000 di fakturnya, maka nilai sum retur berkurang. 
				   * Begitu seterusnya sampe nilai retur = 0

				 * */

				if (isset($query[$j+1]['kode_supplier']) && ($query[$j]['kode_supplier'] == $query[$j+1]['kode_supplier'])) {
					if ($urut_per_supplier == 0) {
						// 13-04-2012 ==============================================================
										$query2	= $this->db->query(" SELECT distinct a.id FROM tm_retur_beli a, tm_retur_beli_faktur b 
													WHERE a.id = b.id_retur_beli AND a.kode_supplier = '".$query[$j]['kode_supplier']."'
													AND a.faktur_date_from >= '".$query[$j]['tgl_sj']."' ");
						
										if ($query2->num_rows() > 0){
											$hasil2 = $query2->result();
											foreach ($hasil2 as $row2) {
												$query3	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
																tm_retur_beli_detail c 
																WHERE a.id = c.id_retur_beli
																AND c.id_retur_beli = '$row2->id'
																AND c.id_pembelian_detail <> '0' ");
													
												$hasilrow = $query3->row();
												$tot_retur+= $hasilrow->tot_retur;
											}
										
											$selisih = $query[$j]['total']-$tot_retur; 
											
											if ($selisih <=0) {
												$tot_retur = $tot_retur - $query[$j]['total']+100000;
												$query[$j]['total'] = 100000; // kasih batas minimal 100.000
											}
											else {
												$tot_retur = 0;
												$query[$j]['total'] = $selisih;
											}
										} // end if ada data returnya
						// ==============================================================
					} // end if $urut_per_supplier = 0
					else {
					  if ($tot_retur != 0) {
						$selisih = $query[$j]['total']-$tot_retur;

						if ($selisih <=0) {
							$tot_retur = $tot_retur - $query[$j]['total']+100000;
							$query[$j]['total'] = 100000; // kasih batas minimal 100.000
						}
						else {
							$tot_retur = 0;
							$query[$j]['total'] = $selisih;
						}
					  }
					  
					}
				} // end if
				$urut_per_supplier++; 
				$pisah1 = explode("-", $query[$j]['tgl_sj']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				 
				 $html_data.= "<tr class=\"record\">
				 <td>$urut_per_supplier</td>
				 <td>".$tgl_sj."</td>
				 <td>".$query[$j]['no_sj']."</td>
				 <td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>
				 <td align='right'>".number_format($query[$j]['total'],2,',','.')."</td>
				 <td align='center'>".$tgl_update."</td>
				 </tr>";
				 $total_persupplier = $total_persupplier + $query[$j]['total'];
				 
				 if (isset($query[$j+1]['kode_supplier']) && ($query[$j]['kode_supplier'] != $query[$j+1]['kode_supplier'])) {
					$html_data.= "<tr class=\"record\">
					 <td colspan='4' align='right'>".$query[$j]['nama_supplier']." TOTAL </td>
					 <td align='right'>".number_format($total_persupplier,2,',','.')."</td>
					 <td>&nbsp;</td>
					 </tr>";
					 $grand_total = $grand_total + $total_persupplier;
					 $total_persupplier = 0; $urut_per_supplier = 0; $tot_retur = 0;
				}
				if (!isset($query[$j+1]['kode_supplier'])) {
					$html_data.= "<tr class=\"record\">
					 <td colspan='4' align='right'>".$query[$j]['nama_supplier']." TOTAL </td>
					 <td align='right'>".number_format($total_persupplier,2,',','.')."</td>
					 <td>&nbsp;</td>
					 </tr>";
					 $grand_total = $grand_total + $total_persupplier;
					 $total_persupplier = 0; $urut_per_supplier = 0; $tot_retur = 0;
				}
					
		 	} // end for
			$html_data.= "<tr>
				<td colspan='4' align='center'><b>GRAND TOTAL</b></td>
				<td colspan='2' align='center'><b>".number_format($grand_total,2,',','.')."</b></td>
			</tr>";
		   }
		//===========================================
		
		$nama_file = "laporan_opname_hutang";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }

}
