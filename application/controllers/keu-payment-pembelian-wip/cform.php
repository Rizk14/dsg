<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('keu-payment-pembelian-wip/mmaster');
  }
  
  

  //02-04-2012, hidden skrip utk mengupdate field status_lunas di tabel tm_pembelian, berdasarkan faktur2 yg udh dilunasi
  function update_status_lunas_sj() {
	  $sql = " select a.*, b.kode_supplier from tm_payment_pembelian_nofaktur a, tm_payment_pembelian_detail b 
				where b.id = a.id_payment_pembelian_detail ";
	  $query	= $this->db->query($sql);
	  
	  if ($query->num_rows() > 0){
		  $hasil = $query->result();
			foreach ($hasil as $row1) {
				$q = 0; $baku = 0;
				if ($row1->is_makloon == 't' && $row1->is_jahit == 'f') {
					/*$this->db->query(" UPDATE tm_faktur_makloon SET status_lunas = 't' , sisa = '0'
								WHERE id = '$row1->id_pembelian_nofaktur' "); */
					$sql2 = " SELECT * FROM tm_faktur_makloon_sj WHERE id_faktur_makloon = '$row1->id_pembelian_nofaktur' ";
					$query2	= $this->db->query($sql2);
					if ($query2->num_rows() > 0){
						$hasil2 = $query2->result();
						foreach ($hasil2 as $row2) {
							$this->db->query(" UPDATE tm_sj_hasil_makloon SET status_lunas = 't' WHERE no_sj = '$row2->no_sj_masuk'
										AND kode_unit = '$row1->kode_supplier' ");
						}
					}
					$q++;
				}
				else if ($row1->is_makloon == 'f' && $row1->is_jahit == 'f') {
					/*$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_lunas = 't' , sisa = '0'
								WHERE id = '$row1->id_pembelian_nofaktur' "); */
					$sql2 = " SELECT * FROM tm_pembelian_nofaktur_sj WHERE id_pembelian_nofaktur = '$row1->id_pembelian_nofaktur' ";
					$query2	= $this->db->query($sql2);
					if ($query2->num_rows() > 0){
						$hasil2 = $query2->result();
						foreach ($hasil2 as $row2) {
							$this->db->query(" UPDATE tm_pembelian SET status_lunas = 't' WHERE no_sj = '$row2->no_sj'
										AND kode_supplier = '$row1->kode_supplier' ");
						}
					}
					$baku++;
				}
			}
	  }
	  echo "quilting = ".$q." , baku= ".$baku;
  }
  // ======================================================================
  
  
  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================

	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$id_faktur = $this->input->post('id_faktur', TRUE);  
	
	
	$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
	$is_makloon = $this->input->post('is_makloon', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	//$no_faktur = trim($no_faktur);
	
	// 13-07-2015
	$khusus = $this->input->post('khusus', TRUE);  
	$koma_id_faktur = substr($id_faktur,-1);
	if ($koma_id_faktur != ',')
	$id_faktur=	$id_faktur.',';
	$id_faktur = trim($id_faktur);
	
	$list_faktur = explode(",", $id_faktur);
	//$is_quilting = trim($is_quilting);
	//$list_is_quilting = explode(",", $is_quilting);
	//$is_jahit = trim($is_jahit);
	//$list_is_jahit = explode(",", $is_jahit);
	
	if ($proses_submit == "Proses") {
		// 13-07-2015
		if ($khusus == '') {
			if ($id_faktur !='') {
				//$data['kode_supplier']	= $kode_supplier;
				$data['supplier'] = $this->mmaster->get_supplier();
				//$data['faktur_detail'] = $this->mmaster->get_detail_faktur($list_faktur, $list_is_quilting, $list_is_jahit);
				$data['faktur_detail'] = $this->mmaster->get_detail_faktur($list_faktur, $is_makloon);
				$data['msg'] = '';
			}
			else {
				$data['msg'] = 'No Faktur harus dipilih';
				$data['no_faktur'] = '';
			}
			$data['go_proses'] = '1';
			$data['jenis_pembelian'] = $jenis_pembelian;
			$data['is_makloon'] = $is_makloon;
			
			$th_now	= date("Y");
			
			$data['isi'] = 'keu-payment-pembelian-wip/vmainform';
			$this->load->view('template',$data);
		}
		else {
			$data['supplier'] = $this->mmaster->get_supplier();
			$data['go_proses'] = '1';
			$data['jenis_pembelian'] = $jenis_pembelian;
			$data['is_makloon'] = $is_makloon;
			$data['msg'] = '';
			$data['isi'] = 'keu-payment-pembelian-wip/vmainformkhusus';
			$this->load->view('template',$data);
		}
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['supplier'] = $this->mmaster->get_supplier();
		$data['isi'] = 'keu-payment-pembelian-wip/vmainform';
		$this->load->view('template',$data);
	}
  }
  
 
  
  function show_popup_faktur(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	//$csupplier 	= $this->uri->segment(4);
	$jenis_pembelian 	= $this->uri->segment(4);
	$is_makloon 	= $this->uri->segment(5);
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
	if ($jenis_pembelian == '' && $is_makloon == '') {
	//if ($jenis_pembelian == '') {
		$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
		$is_makloon = $this->input->post('is_makloon', TRUE);  
	}
	
	if ($keywordcari == '' && ($jenis_pembelian == '' || $is_makloon == '')) {
	//if ($keywordcari == '' && $jenis_pembelian == '') {
		$jenis_pembelian 	= $this->uri->segment(4);
		$is_makloon 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
		
	if ($keywordcari == '')
		$keywordcari 	= "all";
		
	if ($jenis_pembelian == '')
		$jenis_pembelian = '0';
	if ($is_makloon == '')
		$is_makloon = 'f';
		
	/*$jum_total = $this->mmaster->get_fakturtanpalimit($csupplier, $keywordcari, $pkp);
				$config['base_url'] = base_url()."index.php/keu-payment-pembelian-wip/cform/show_popup_faktur/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		*/
	//$data['query'] = $this->mmaster->get_faktur($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari, $pkp);
	$data['query'] = $this->mmaster->get_faktur($keywordcari, $jenis_pembelian, $is_makloon);
	$data['jum_total'] = count($data['query']);
	//$data['list_supplier'] = $this->mmaster->get_supplier();
	
	//$data['csupplier'] = $csupplier;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['jenis_pembelian'] = $jenis_pembelian;
	$data['is_makloon'] = $is_makloon;
	
	$this->load->view('keu-payment-pembelian-wip/vpopupfaktur',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$tgl = date("Y-m-d H:i:s");
			$is_khusus 	= $this->input->post('is_khusus', TRUE);
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			$is_makloon 	= $this->input->post('is_makloon', TRUE);
			$no_voucher 	= $this->input->post('no_voucher', TRUE);
			$tgl_voucher = $this->input->post('tgl_voucher', TRUE);  
			$pisah1 = explode("-", $tgl_voucher);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_voucher = $thn1."-".$bln1."-".$tgl1;
			
			// MODIF 28-07-2015
			$ada_manual = $this->input->post('ada_manual', TRUE);
			$no2 = $this->input->post('no2', TRUE);
			//echo $ada_manual." ".$no2; die();
						
			$no 	= $this->input->post('no', TRUE);
			$jum_subtotal 	= $this->input->post('jum_subtotal', TRUE);
			//$kode_supplier = $this->input->post('kode_sup', TRUE);  
						
			if ($is_makloon == '')
				$is_makloon = 'f';
				
				$cek_data = $this->mmaster->cek_data($no_voucher, $thn1);
				if (count($cek_data) > 0) {
					$data['isi'] = 'keu-payment-pembelian-wip/vmainform';
					$data['msg'] = "Data no voucher ".$no_voucher." utk tahun $thn1 sudah ada..!";
					//$data['msg'] = '';
					$data['id_pp'] = '';
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					$total = 0;
					if ($is_khusus == '') {
						for ($i=1;$i<=$jum_subtotal;$i++) {
							$total = $total + $this->input->post('jum_bayar_'.$i, TRUE);
						}
						
						if ($ada_manual == 't') {
							$jumlah_manual=$no2-1;
							
							for ($xx=1;$xx<=$jumlah_manual;$xx++) {
								$total = $total + $this->input->post('jum_bayar2_'.$xx, TRUE);
							}
						}
					}
					else {
						for ($i=1;$i<=$jumlah_input;$i++) {
							$total = $total + $this->input->post('jum_bayar_'.$i, TRUE);
						}
					}
					
					// insert di tm_payment_pembelian
					$data_header = array(
					  'jenis_pembelian'=>$jenis_pembelian,
					  //'is_makloon'=>$is_makloon,
					  'no_voucher'=>$no_voucher,
					  'tgl'=>$tgl_voucher,
					  'tgl_input'=>$tgl,
					  'tgl_update'=>$tgl,
					  'total'=>$total,
					  'is_makloon'=>$is_makloon
					);
					$this->db->insert('tm_payment_pembelian',$data_header);
					
					// ambil data terakhir di tabel tm_payment_pembelian
					$query2	= $this->db->query(" SELECT id FROM tm_payment_pembelian ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_payment	= $hasilrow->id;
					
					if ($is_khusus == '') {
						for ($i=1;$i<=$jum_subtotal;$i++) {
							// function save udh ga dipake lagi
							/*$this->mmaster->save($no_voucher,$tgl_voucher, $total, $this->input->post('kode_supplier_'.$i, TRUE),
							$this->input->post('no_faktur_'.$i, TRUE), $this->input->post('jum_hutang_'.$i, TRUE), 
							$this->input->post('jum_gabung_'.$i, TRUE), $this->input->post('jum_bayar_'.$i, TRUE),
							$this->input->post('sisa_bayar_'.$i, TRUE), $this->input->post('jum_retur_'.$i, TRUE), 
							$this->input->post('jenis_pembulatan_'.$i, TRUE) ); */
							$pembulatan = $this->input->post('jum_bayar_'.$i, TRUE)-$this->input->post('subtotal_'.$i, TRUE);
							if ($pembulatan < 0)
								$jenis_pembulatan = 2;
							else if ($pembulatan > 0)
								$jenis_pembulatan = 1;
							else
								$jenis_pembulatan = 0;
								
							$data_subtotal = array(
								'id_payment'=>$id_payment,
								'jumlah_bayar'=>$this->input->post('jum_bayar_'.$i, TRUE),
								'pembulatan'=>$pembulatan,
								'jenis_pembulatan'=>$jenis_pembulatan,
								'id_supplier'=>$this->input->post('id_supplier2_'.$i, TRUE),
								'subtotal'=>$this->input->post('subtotal_'.$i, TRUE),
								'deskripsi'=>$this->input->post('deskripsi_'.$i, TRUE),
								'cndn'=>$this->input->post('jum_cndn_'.$i, TRUE),
								'keterangan_cndn'=>$this->input->post('ket_cndn_'.$i, TRUE)
							);
							$this->db->insert('tm_payment_pembelian_detail',$data_subtotal);
							
							// ambil data terakhir di tabel tm_payment_pembelian_detail
							$query2	= $this->db->query(" SELECT id FROM tm_payment_pembelian_detail ORDER BY id DESC LIMIT 1 ");
							$hasilrow = $query2->row();
							$id_payment_detail	= $hasilrow->id;
							
							$item_faktur = trim($this->input->post('item_faktur_'.$i, TRUE));
							$list_item_faktur = explode(";", $item_faktur);
							
							/*$item_is_quilting = trim($this->input->post('item_is_quilting_'.$i, TRUE));
							$list_item_is_quilting = explode(";", $item_is_quilting); // 
							//14-03-2012
							$item_is_jahit = trim($this->input->post('item_is_jahit_'.$i, TRUE));
							$list_item_is_jahit = explode(";", $item_is_jahit); */
							
							// 5 des 2011 &&&&&&&&&
							for($j=0; $j<count($list_item_faktur)-1; $j++){
								$data_faktur = array(
										'id_payment_pembelian_detail'=>$id_payment_detail,
										'id_pembelian_nofaktur'=>$list_item_faktur[$j]
										//'is_makloon'=>$list_item_is_quilting[$j],
										//'is_jahit'=>$list_item_is_jahit[$j]
									);
								$this->db->insert('tm_payment_pembelian_nofaktur',$data_faktur);
								
								// update status lunas di tabel faktur / tabel sj hasil jahit
								// 25-06-2015 IS_QUILTING DAN IS_JAHIT DIKOMEN
								//if ($list_item_is_quilting[$j] == 'f') {
								
								if ($is_makloon == 'f') {
									$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_lunas = 't', sisa = '0' 
													WHERE id = '".$list_item_faktur[$j]."' ");
									// 02-04-2012, update juga status_lunas di tabel tm_pembelian
									$sql2 = " SELECT b.*, a.id_supplier FROM tm_pembelian_nofaktur_sj b INNER JOIN tm_pembelian_nofaktur a ON a.id = b.id_pembelian_nofaktur
												WHERE b.id_pembelian_nofaktur = '".$list_item_faktur[$j]."' ";
									$query2	= $this->db->query($sql2);
									if ($query2->num_rows() > 0){
										$hasil2 = $query2->result();
										foreach ($hasil2 as $row2) {
											// 14-07-2015, ganti pake id_sj_pembelian
											$this->db->query(" UPDATE tm_pembelian SET status_lunas = 't' 
														WHERE id = '$row2->id_sj_pembelian' ");
										}
									}
								}
								else {
									$this->db->query(" UPDATE tm_pembelian_makloon_faktur SET status_lunas = 't'
													WHERE id = '".$list_item_faktur[$j]."' ");
									$sql2 = " SELECT b.*, a.id_supplier FROM tm_pembelian_makloon_faktur_detail b INNER JOIN tm_pembelian_makloon_faktur a ON a.id = b.id_pembelian_makloon_faktur
												WHERE b.id_pembelian_makloon_faktur = '".$list_item_faktur[$j]."' ";
									$query2	= $this->db->query($sql2);
									if ($query2->num_rows() > 0){
										$hasil2 = $query2->result();
										foreach ($hasil2 as $row2) {
											$this->db->query(" UPDATE tm_pembelian_makloon SET status_lunas = 't' 
														WHERE id = '$row2->id_pembelian_makloon' ");
										}
									}
								}
								//=================================================

							}
						}
						
						if ($ada_manual == 't') {
							$jumlah_manual=$no2-1;
							for ($i=1;$i<=$jumlah_manual;$i++) {
								$id_supplier = $this->input->post('id_supplier2x_'.$i, TRUE);
								$subtotal = $this->input->post('subtotal2_'.$i, TRUE);
								$jum_bayar = $this->input->post('jum_bayar2_'.$i, TRUE);
								$deskripsi = $this->input->post('deskripsi2_'.$i, TRUE);
								
								$pembulatan = $this->input->post('jum_bayar2_'.$i, TRUE)-$this->input->post('subtotal2_'.$i, TRUE);
								if ($pembulatan < 0)
									$jenis_pembulatan = 2;
								else if ($pembulatan > 0)
									$jenis_pembulatan = 1;
								else
									$jenis_pembulatan = 0;
								
								$data_detail = array(
									'id_payment'=>$id_payment,
									'jumlah_bayar'=>$jum_bayar,
									'pembulatan'=>$pembulatan,
									'jenis_pembulatan'=>$jenis_pembulatan,
									'id_supplier'=>$id_supplier,
									'subtotal'=>$subtotal,
									'deskripsi'=>$deskripsi
								);
								$this->db->insert('tm_payment_pembelian_detail',$data_detail);
							}
						}
					}
					else {
						for ($i=1;$i<=$jumlah_input;$i++) {
							$id_supplier = $this->input->post('id_supplier_'.$i, TRUE);
							$subtotal = $this->input->post('subtotal_'.$i, TRUE);
							$jum_bayar = $this->input->post('jum_bayar_'.$i, TRUE);
							//$pembulatan = $this->input->post('pembulatan_'.$i, TRUE);
							//$jenis_pembulatan = $this->input->post('jenis_pembulatan_'.$i, TRUE);
							$deskripsi = $this->input->post('deskripsi_'.$i, TRUE);
							
							$pembulatan = $this->input->post('jum_bayar_'.$i, TRUE)-$this->input->post('subtotal_'.$i, TRUE);
							if ($pembulatan < 0)
								$jenis_pembulatan = 2;
							else if ($pembulatan > 0)
								$jenis_pembulatan = 1;
							else
								$jenis_pembulatan = 0;
							
							$data_detail = array(
								'id_payment'=>$id_payment,
								'jumlah_bayar'=>$jum_bayar,
								'pembulatan'=>$pembulatan,
								'jenis_pembulatan'=>$jenis_pembulatan,
								'id_supplier'=>$id_supplier,
								'subtotal'=>$subtotal,
								'deskripsi'=>$deskripsi
							);
							$this->db->insert('tm_payment_pembelian_detail',$data_detail);
						}
					}
					redirect('keu-payment-pembelian-wip/cform/view');
				}
  }
  
 
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'keu-payment-pembelian-wip/vformview';
    $keywordcari = "all";
    $jenis_pembelian = '0';

    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_pembelian);
							$config['base_url'] = base_url().'index.php/keu-payment-pembelian-wip/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $jenis_pembelian);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	$data['jenis_pembelian'] = $jenis_pembelian;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);  
	
	if ($keywordcari == '' && $jenis_pembelian == '') {
		$keywordcari 	= $this->uri->segment(4);
		$jenis_pembelian 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
		
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_pembelian);
							$config['base_url'] = base_url().'index.php/keu-payment-pembelian-wip/cform/cari/'.$keywordcari.'/'.$jenis_pembelian;
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $keywordcari, $jenis_pembelian);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	$data['jenis_pembelian'] = $jenis_pembelian;
	
	$data['isi'] = 'keu-payment-pembelian-wip/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "keu-payment-pembelian-wip/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "keu-payment-pembelian-wip/cform/cari/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('keu-payment-pembelian-wip/cform/view');
  }
  
  function get_pkp_tipe_pajak() {
		$kode_sup 	= $this->uri->segment(4);
		$rows = array();
		if(isset($kode_sup)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			$rows = $this->mmaster->get_pkp_tipe_pajak_bykodesup($kode_sup);
			
			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);

  }
}
