<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('brgjadi_hpp/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->i_product;
			$enama_brg = $row->e_product_motifname;
			$ekel_brg_jadi = $row->id_kel_brg_jadi;
		}
	}
	else {
			$ekode = '';
			$enama_brg = '';
			$ekel_brg_jadi = '';
			$edit = '';
	}
	$data['list_kelbrgjadi_hpp'] = $this->mmaster->getAllkelbrgjadi_hpp();
	$data['ekode'] = $ekode;
	$data['enama_brg'] = $enama_brg;	
	$data['ekel_brg_jadi'] = $ekel_brg_jadi;	
	$data['edit'] = $edit;
	$data['msg'] = '';
    $data['isi'] = 'brgjadi_hpp/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		//$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');

		} */
		
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$kel_brg_jadi 	= $this->input->post('kel_brg_jadi', TRUE);
						
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'brgjadi_hpp/vmainform';
					$data['msg'] = "Data kode ".$kode." sudah ada..!";
					
					$ekode = '';
					$enama_brg = '';
					$ekel_brg_jadi = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama_brg'] = $enama_brg;
					$data['ekel_brg_jadi'] = $ekel_brg_jadi;
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($kode,$nama, $kel_brg_jadi, $kodeedit, $goedit);
					//redirect('brgjadi_hpp/cform');
					$data['isi'] = 'brgjadi_hpp/vmainform';
					$data['msg'] = "Data kode ".$kode." berhasil disimpan";
					
					$ekode = '';
					$enama_brg = '';
					$ekel_brg_jadi = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama_brg'] = $enama_brg;
					$data['ekel_brg_jadi'] = $ekel_brg_jadi;
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
			} // end if goedit == ''
			else {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'brgjadi_hpp/vmainform';
						$data['msg'] = "Data kode ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama_brg'] = $nama;
						$data['ekel_brg_jadi'] = $kel_brg_jadi;
						$data['edit'] = $edit;
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kode,$nama, $kel_brg_jadi, $kodeedit, $goedit);
						redirect('brgjadi_hpp/cform/view');
					}
				}
				else {
					$this->mmaster->save($kode,$nama, $kel_brg_jadi, $kodeedit, $goedit);
					redirect('brgjadi_hpp/cform/view');
				}
			}
			
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'brgjadi_hpp/vformview';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
			
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/cari/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'brgjadi_hpp/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('brgjadi_hpp/cform/view');
  }
  
  // ---------------------------- 28-01-2014 ----------------------------------
  function viewwarnabrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'brgjadi_hpp/vformviewwarna';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllwarnabrgjadi_hpptanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/viewwarnabrgjadi_hpp/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllwarnabrgjadi_hpp($config['per_page'],$this->uri->segment(5,0), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function cariwarnabrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
			
    $jum_total = $this->mmaster->getAllwarnabrgjadi_hpptanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/cariwarnabrgjadi_hpp/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllwarnabrgjadi_hpp($config['per_page'],$this->uri->segment(6,0), $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'brgjadi_hpp/vformviewwarna';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function addwarnabrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['msg'] = '';
	$data['list_warna'] = $this->mmaster->get_warna();
    $data['isi'] = 'brgjadi_hpp/vmainformwarna';
    
	$this->load->view('template',$data);
  }
  
  function show_popup_brgjadi_hpp(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	$keywordcari	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari=='') {
		$keywordcari 	= $this->uri->segment(4);
	}	

	if ($keywordcari=='')
		$keywordcari = "all";
			
	$qjum_total = $this->mmaster->getbarangjaditanpalimit($keywordcari);

	$config['base_url'] 	= base_url()."index.php/brgjadi_hpp/cform/show_popup_brgjadi_hpp/".$keywordcari."/";
	$config['total_rows'] 	= count($qjum_total); 
	$config['per_page'] 	= 10;
	$config['first_link'] 	= 'Awal';
	$config['last_link'] 	= 'Akhir';
	$config['next_link'] 	= 'Selanjutnya';
	$config['prev_link'] 	= 'Sebelumnya';
	$config['cur_page']		= $this->uri->segment(5,0);
	$this->pagination->initialize($config);
	
	$data['query']		= $this->mmaster->getbarangjadi($config['per_page'],$config['cur_page'], $keywordcari);	
	$data['jum_total']	= count($qjum_total);

	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
				
	$data['startnya']	= $config['cur_page'];
	$this->load->view('brgjadi_hpp/vpopupbrgjadi_hpp',$data);  
  }

  function submitwarnabrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$jumwarna	= $this->input->post('jumwarna');
		$kodewarna = "";
		
		for ($i=1; $i<=$jumwarna; $i++) {
			$kodewarna	.= $this->input->post('kode_warna_'.$i).";";
		}
		
		$this->mmaster->savewarnabrgjadi_hpp($kode_brg_jadi,$kodewarna);
		redirect('brgjadi_hpp/cform/viewwarnabrgjadi_hpp');		
  }
  
  function deletewarnabrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->deletewarnabrgjadi_hpp($kode);
    redirect('brgjadi_hpp/cform/viewwarnabrgjadi_hpp');
  }
  
  function editwarnabrgjadi_hpp() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$kode_brg_jadi	= $this->uri->segment(4);
		$data['kode_brg_jadi']	= $kode_brg_jadi;
		
		//--------------------------------------------------------------
		$sqlnya	= $this->db->query(" SELECT distinct a.kode_brg_jadi, b.e_product_motifname 
							FROM tm_warna_brg_jadi a, tr_product_motif b
							WHERE a.kode_brg_jadi = b.i_product_motif 
							AND UPPER(a.kode_brg_jadi) = '$kode_brg_jadi'
							ORDER BY a.kode_brg_jadi ");
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $this->db->query(" SELECT a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
										WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$rownya->kode_brg_jadi."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'kode_warna'=> $rowxx->kode_warna,
												'nama_warna'=> $rowxx->nama
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'kode_brg_jadi'=> $rownya->kode_brg_jadi,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		
		// ================ 12-02-2014, cek apakah data udh dipake di tiap2 tabel transaksi ==============================
		$ada = 0;
				 
		 // tabel bon M masuk hasil cutting
		 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail WHERE kode_brg_jadi = '".$kode_brg_jadi."' ");
		 if ($query3->num_rows() > 0){
			$ada = 1;
		 }
				 
		 // tabel Sj keluar ke jahitan
		 $query3	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail WHERE kode_brg_jadi = '".$kode_brg_jadi."' ");
		 if ($query3->num_rows() > 0){
			$ada = 1;
		 }
				 
		 // tabel Sj masuk WIP
		 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail WHERE kode_brg_jadi = '".$kode_brg_jadi."' ");
		 if ($query3->num_rows() > 0){
			$ada = 1;
		 }
				 
		 // tabel Sj keluar WIP
		 $query3	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail WHERE kode_brg_jadi = '".$kode_brg_jadi."' ");
		 if ($query3->num_rows() > 0){
			$ada = 1;
		 }
		// =====================================================================
		$data['ada'] = $ada;
		$data['msg'] = '';
		$data['list_warna'] = $this->mmaster->get_warna();
		$data['query'] = $outputdata;
		$data['isi'] = 'brgjadi_hpp/veditformwarna';
		
		$this->load->view('template',$data);
		//--------------------------------------------------------------
	}
	
	function updatewarnabrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$ada 	= $this->input->post('ada', TRUE);
		$jumwarna	= $this->input->post('jumwarna');
		$kodewarna = "";
		
		for ($i=1; $i<=$jumwarna; $i++) {
			$kodewarna	.= $this->input->post('kode_warna_'.$i).";";
		}
		
		/*$listwarna = explode(";", $kodewarna);
		$i=0;
		foreach ($listwarna as $row1) {
			if (trim($row1 == ''))
				echo "kosong"."<br>";
			else
				echo $row1."<br>";
			$i++;
		} echo $i; die(); */
		$this->mmaster->updatewarnabrgjadi_hpp($kode_brg_jadi, $ada, $kodewarna);
		redirect('brgjadi_hpp/cform/viewwarnabrgjadi_hpp');
  }
  
  // --------------------------------------------------------------------------
  // 04-02-2014
  function addmaterialbrgjadi_hpp(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	$data['isi'] = 'brgjadi_hpp/vmainformmaterial';
	$data['list_warna'] = $this->mmaster->get_warna();
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
// utk material brg jadi
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jumdata 	= $this->input->post('jumdata', TRUE);  
		
		if ($keywordcari == '' && $jumdata == '') {
			$jumdata 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_brgjadi_hpptanpalimit($keywordcari);

				$config['base_url'] = base_url()."index.php/brgjadi_hpp/cform/show_popup_brg_jadi/".$jumdata."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi_hpp($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$data['jumdata'] = $jumdata;
	$this->load->view('brgjadi_hpp/vpopupbrgjadi_hpp2',$data);
  }
  
  // 05-02-2014
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$is_quilting 	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);
	
	if ($is_quilting == '' || $posisi == '') {
		$is_quilting 	= $this->input->post('is_quilting', TRUE);  
		$posisi 	= $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || ($is_quilting == '' && $posisi == '') ) {
			$is_quilting 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $is_quilting);
		
				$config['base_url'] = base_url()."index.php/brgjadi_hpp/cform/show_popup_brg/".$is_quilting."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $is_quilting);
	$data['jum_total'] = count($qjum_total);
	$data['is_quilting'] = $is_quilting;
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$this->load->view('brgjadi_hpp/vpopupbrg',$data);
  }
  
  function submitmaterialbrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$jumdata	= $this->input->post('no')-1;
		$kode_bhn_baku = "";
		$kode_warna = "";
		
		for ($i=1; $i<=$jumdata; $i++) {
			$kode_bhn_baku	.= $this->input->post('kode_'.$i).";";
			$kode_warna	.= $this->input->post('kode_warna_'.$i).";";
		}
		
		$this->mmaster->savematerialbrgjadi_hpp($kode_brg_jadi,$kode_bhn_baku, $kode_warna);
		redirect('brgjadi_hpp/cform/viewmaterialbrgjadi_hpp');		
  }
  
  function viewmaterialbrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'brgjadi_hpp/vformviewmaterial';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllmaterialbrgjadi_hpptanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/viewmaterialbrgjadi_hpp/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllmaterialbrgjadi_hpp($config['per_page'],$this->uri->segment(5,0), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function carimaterialbrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
			
    $jum_total = $this->mmaster->getAllmaterialbrgjadi_hpptanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/carimaterialbrgjadi_hpp/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllmaterialbrgjadi_hpp($config['per_page'],$this->uri->segment(6,0), $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'brgjadi_hpp/vformviewmaterial';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function deletematerialbrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->deletematerialbrgjadi_hpp($kode);
    redirect('brgjadi_hpp/cform/viewmaterialbrgjadi_hpp');
  }
  
  function editmaterialbrgjadi_hpp() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$kode_brg_jadi	= $this->uri->segment(4);
		$data['kode_brg_jadi']	= $kode_brg_jadi;
		
		//--------------------------------------------------------------
		$sqlnya	= $this->db->query(" SELECT distinct a.kode_brg_jadi, b.e_product_motifname 
							FROM tm_material_brg_jadi a, tr_product_motif b
							WHERE a.kode_brg_jadi = b.i_product_motif 
							AND UPPER(a.kode_brg_jadi) = '$kode_brg_jadi'
							ORDER BY a.kode_brg_jadi ");
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailbrg = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 bhn baku berdasarkan kode brgnya
				$queryxx = $this->db->query(" SELECT kode_brg, kode_warna FROM tm_material_brg_jadi 
										WHERE kode_brg_jadi = '".$rownya->kode_brg_jadi."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
						if ($query3->num_rows() > 0){
							$quilting = 'f';
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$quilting = 't';
							$query31	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
							if ($query31->num_rows() > 0){
								$quilting = 't';
								$hasilrow31 = $query31->row();
								$nama_brg	= $hasilrow31->nama_brg;
								$satuan	= $hasilrow31->nama_satuan;
							}
							else {
								$nama_brg = '';
								$satuan = '';
								$quilting = 'f';
							}
						}
						
						$detailbrg[] = array(	'kode_brg'=> $rowxx->kode_brg,
												'nama_brg'=> $nama_brg,
												'quilting'=> $quilting,
												'satuan'=> $satuan,
												'kode_warna'=> $rowxx->kode_warna
												);
					}
				}
				else
					$detailbrg = '';
					
				$outputdata[] = array(	'kode_brg_jadi'=> $rownya->kode_brg_jadi,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailbrg'=> $detailbrg
								);
				
				$detailbrg = array();
			} // end for
		}
		else
			$outputdata = '';
		
		// list warna berdasarkan brg jadinya
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ");
		if ($queryxx->num_rows() > 0){
			$list_warna = $queryxx->result();
		}
		else
			$list_warna = '';
			
		// ================ 12-02-2014, cek apakah data udh dipake di tiap2 tabel transaksi ==============================
		$ada = 0;
				 
		 // tabel bon M masuk hasil cutting
		 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail WHERE kode_brg_jadi = '".$kode_brg_jadi."' ");
		 if ($query3->num_rows() > 0){
			$ada = 1;
		 }
		 $query3	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail WHERE kode_brg_jadi = '".$kode_brg_jadi."' ");
		 if ($query3->num_rows() > 0){
			$ada = 1;
		 }
		 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail WHERE kode_brg_jadi = '".$kode_brg_jadi."' ");
		 if ($query3->num_rows() > 0){
			$ada = 1;
		 }
				 
		// =====================================================================
		$data['ada'] = $ada;
		
		$data['msg'] = '';
		$data['query'] = $outputdata;
		//$data['list_warna'] = $this->mmaster->get_warna();
		$data['list_warna'] = $list_warna;
		$data['isi'] = 'brgjadi_hpp/veditformmaterial';
		$this->load->view('template',$data);
		//--------------------------------------------------------------
	}
	
	function updatedatamaterialbrgjadi_hpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$ada 	= $this->input->post('ada', TRUE);
		$jumdata	= $this->input->post('no')-1;
		$kode_bhn_baku = "";
		$kode_warna = "";
		
		for ($i=1; $i<=$jumdata; $i++) {
			$kode_bhn_baku	.= $this->input->post('kode_'.$i).";";
			$kode_warna	.= $this->input->post('kode_warna_'.$i).";";
		}
		
		$this->mmaster->updatedatamaterialbrgjadi_hpp($kode_brg_jadi,$ada,$kode_bhn_baku, $kode_warna);
		redirect('brgjadi_hpp/cform/viewmaterialbrgjadi_hpp');
  }
  
  // 11-02-2014
  function getlistwarna(){
		//$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$kode_brg_jadi 	= $this->uri->segment(4);
				
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			/*foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama
									);
			} */
		}
		//else
		//	$detailwarna = '';
		
		//$data['detailwarna'] = $detailwarna;
		//$data['kode_brg_jadi'] = $kode_brg_jadi;
		//$this->load->view('brgjadi_hpp/vlistwarna', $data); 
		echo json_encode($hasilxx);
  }
  
  // 23-05-2014
  /*function addhpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['msg'] = '';
	$data['go_proses'] = '';
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
    $data['isi'] = 'brgjadi_hpp/vmainformhpp';
    
	$this->load->view('template',$data);
  } */
  
  // 30-05-2014, rombak lagi utk fitur HPP
  function addhpp(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$submit = $this->input->post('submit', TRUE);
	
	//$data['query'] = $this->mmaster->get_hpp_brgjadi_hpp();
	$data['query'] = '';
	if (is_array($data['query']) )
		$data['jum_data'] = count($data['query'])+1;
	else
		$data['jum_data'] = 2;
	$data['isi'] = 'brgjadi_hpp/vmainformhpp';
	$this->load->view('template',$data);
	
	//if ($submit == "Proses") {
		/*$cek_data = $this->mreport->cek_forecast($bulan, $tahun); 
			
		if ($cek_data['idnya'] == '' ) { 
			// ambil nama2 brg dari tabel master brg jadi (tr_product_motif)
			//$data['query'] = $this->mreport->get_brgjadi_hpp();
			$data['query'] = '';
			
			$data['is_new'] = '1';
			if (is_array($data['query']) )
				$data['jum_total'] = count($data['query']);
			else
				$data['jum_total'] = 0;
				
			$data['isi'] = 'wip/vform2forecast';
			$this->load->view('template',$data);
		}
		else {
				// get data dari tabel tm_forecast_wip
				$data['query'] = $this->mreport->get_forecast($bulan, $tahun);
				$data['is_new'] = '0';
				$data['jum_total'] = count($data['query']);
				$data['isi'] = 'wip/vform2forecast';
				$this->load->view('template',$data);
		} // end else
		*/
	//}
	/*else {
		$data['isi'] = 'wip/vformforecast';
		$data['msg'] = '';
		//$data['bulan_skrg'] = date("m");
		$this->load->view('template',$data);
	} */
  }
  
  function caribrgjadi_hpp(){
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel brg jadi utk ambil kode, nama, dan stok
		$queryxx = $this->db->query(" SELECT id,nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_jadi."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$nama_brg_jadi = $hasilxx->nama_brg;
			$id_brg_jadi = $hasilxx->id;
		}
		else{
			$nama_brg_jadi = '';
			$id_brg_jadi='';
			
		}
		//echo $nama_brg_jadi; die();
		$data['id_brg_jadi'] = $id_brg_jadi;
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('brgjadi_hpp/vinfobrgjadi_hpp', $data); 
		return true;
  }
  
  
  /*function addhpp2() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_jahit 	= $this->input->post('unit_jahit', TRUE);
		$data['list_brg'] = $this->mmaster->get_stok_unit_jahit($unit_jahit);
		$data['msg'] = '';	
		$data['jum'] = count($data['list_brg']);
		//nm unit jahit
		$query	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");    
		$hasilrow = $query->row();
		$nama_unit	= $hasilrow->nama;
				
		$data['isi'] = 'brgjadi_hpp/vmainformhpp';
		$data['go_proses'] = '1';
		
		$data['kode_unit'] = $unit_jahit;
		$data['nama_unit'] = $nama_unit;		
		$this->load->view('template',$data);  
  } */
  
  function submithpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$jum_data = $this->input->post('jum_data', TRUE)-1;

		for ($i=1;$i<=$jum_data;$i++)
		{
			// cek apakah datanya udh ada
			$cek_data = $this->mmaster->cek_data_hpp($this->input->post('id_brg_jadi_'.$i, TRUE),$this->input->post('bulan_'.$i, TRUE),$this->input->post('tahun_'.$i, TRUE));
			if (count($cek_data) == 0) { 
				$this->mmaster->savehpp($this->input->post('id_brg_jadi_'.$i, TRUE),$this->input->post('kode_brg_jadi_'.$i, TRUE), 
							$this->input->post('harga_'.$i, TRUE),$this->input->post('bulan_'.$i, TRUE),$this->input->post('tahun_'.$i, TRUE) );
			}
			else {
				if ($this->input->post('harga_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0)
					$this->db->query(" UPDATE tm_hpp_wip SET harga = '".$this->input->post('harga_'.$i, TRUE)."'
							WHERE kode_brg_jadi = '".$this->input->post('kode_'.$i, TRUE)."' ");
			}
			
		}
		redirect('brgjadi_hpp/cform/viewhpp');
  }
  
  function viewhpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'brgjadi_hpp/vformviewhpp';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllhpptanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/viewhpp/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllhpp($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	//$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);
  }
  
  function carihpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  

	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

    $jum_total = $this->mmaster->getAllhpptanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/carihpp/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllhpp($config['per_page'],$this->uri->segment(6), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'brgjadi_hpp/vformviewhpp';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	//$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);
  }
  
  function deletehpp(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletehpp($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "brgjadi_hpp/cform/viewhpp/index/".$cur_page;
	else
		$url_redirectnya = "brgjadi_hpp/cform/carihpp/index/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
  }
  
  function edithpp() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $idharga 	= $this->uri->segment(4);  
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($idharga == '')
		$idharga 	= $this->input->post('idharga', TRUE);
    
    if ($go_edit == '') {
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
		
		$data['query'] = $this->mmaster->get_hpp($idharga);
		$data['isi'] = 'brgjadi_hpp/veditformhpp';
		$data['idharga'] = $idharga;
		$this->load->view('template',$data);
	}
	else { // update data harga
		$harga 	= $this->input->post('harga', TRUE);
		$harga_lama 	= $this->input->post('harga_lama', TRUE);
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		
		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		
		$tgl = date("Y-m-d H:i:s");
		
		if ($harga != $harga_lama) {
			$this->db->query(" UPDATE tm_hpp_wip SET harga = '$harga', tgl_update = '$tgl' WHERE id = '$idharga' ");    
		}
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "brgjadi_hpp/cform/viewhpp/index/".$cur_page;
		else
			$url_redirectnya = "brgjadi_hpp/cform/carihpp/index/".$carinya."/".$cur_page;
		
		redirect($url_redirectnya);
	}
  }
  
  // 12-09-2014
  function kelbrgjadi_hpp(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get_kel_brg_jadi($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
			$eket = $row->keterangan;
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$enama = '';
			$eket = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['eket'] = $eket;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAllkelbrgjadi_hpp();
    $data['isi'] = 'brgjadi_hpp/vmainformkelbrgjadi_hpp';
	$this->load->view('template',$data);
  }
  
  function submitkelbrgjadi_hpp(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('brgjadi_hpp/cform/kelbrgjadi_hpp');
		}
		else
		{
			$id 	= $this->input->post('id', TRUE);
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$ket 	= $this->input->post('ket', TRUE);
			
			if ($goedit == 1) {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data_kel_brg_jadi($kode);
					if (count($cek_data) == 0) { 
						$this->mmaster->save_kel_brg_jadi($kode, $kodeedit, $nama, $ket, $goedit);
					}
				}
				else {
					$this->mmaster->save_kel_brg_jadi($kode, $kodeedit, $nama, $ket, $goedit);
				}
			}
			else {
				$cek_data = $this->mmaster->cek_data_kel_brg_jadi($kode);
				if (count($cek_data) == 0) { 
					$this->mmaster->save_kel_brg_jadi($kode, $kodeedit, $nama, $ket, $goedit);
				}
			}
			
			redirect('brgjadi_hpp/cform/kelbrgjadi_hpp');
		}
  }

  function deletekelbrgjadi_hpp(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete_kel_brg_jadi($id);
    redirect('brgjadi_hpp/cform/kelbrgjadi_hpp');
  }
  
  // 26-09-2014 HPP pengadaan
  function addhpppengadaan(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$data['query'] = $this->mmaster->get_hpp_brgjadi_hpp_pengadaan();
	$data['query'] = '';
	if (is_array($data['query']) )
		$data['jum_data'] = count($data['query'])+1;
	else
		$data['jum_data'] = 2;
	$data['isi'] = 'brgjadi_hpp/vmainformhpppengadaan';
	$this->load->view('template',$data);
  }
  
  function submithpppengadaan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$jum_data = $this->input->post('jum_data', TRUE)-1;

		for ($i=1;$i<=$jum_data;$i++)
		{
			// cek apakah datanya udh ada
			$cek_data = $this->mmaster->cek_data_hpp_pengadaan($this->input->post('id_brg_jadi_'.$i, TRUE),$this->input->post('bulan_'.$i, TRUE),$this->input->post('tahun_'.$i, TRUE));
			if (count($cek_data) == 0) { 
				$this->mmaster->savehpppengadaan($this->input->post('kode_brg_jadi_'.$i, TRUE), $this->input->post('id_brg_jadi_'.$i, TRUE),
							$this->input->post('harga_'.$i, TRUE) ,$this->input->post('bulan_'.$i, TRUE),$this->input->post('tahun_'.$i, TRUE));
			}
			else {
				if ($this->input->post('harga_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0)
					$this->db->query(" UPDATE tm_hpp_pengadaan SET harga = '".$this->input->post('harga_'.$i, TRUE)."'
							WHERE kode_brg_jadi = '".$this->input->post('kode_'.$i, TRUE)."' ");
			}
			
		}
		redirect('brgjadi_hpp/cform/viewhpppengadaan');
  }
  
  function viewhpppengadaan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'brgjadi_hpp/vformviewhpppengadaan';
    $keywordcari = "all";

    $jum_total = $this->mmaster->getAllhpppengadaantanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/viewhpppengadaan/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllhpppengadaan($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('template',$data);
  }
  
  function carihpppengadaan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  

	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAllhpppengadaantanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/carihpppengadaan/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllhpppengadaan($config['per_page'],$this->uri->segment(6), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'brgjadi_hpp/vformviewhpppengadaan';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$this->load->view('template',$data);
  }
  
  function deletehpppengadaan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletehpppengadaan($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "brgjadi_hpp/cform/viewhpppengadaan/index/".$cur_page;
	else
		$url_redirectnya = "brgjadi_hpp/cform/carihpppengadaan/index/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
  }
  
  function edithpppengadaan() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $idharga 	= $this->uri->segment(4);  
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($idharga == '')
		$idharga 	= $this->input->post('idharga', TRUE);
    
    if ($go_edit == '') {
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
		
		$data['query'] = $this->mmaster->get_hpp_pengadaan($idharga);
		$data['isi'] = 'brgjadi_hpp/veditformhpppengadaan';
		$data['idharga'] = $idharga;
		$this->load->view('template',$data);
	}
	else { // update data harga
		$harga 	= $this->input->post('harga', TRUE);
		$harga_lama 	= $this->input->post('harga_lama', TRUE);
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		
		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		
		$tgl = date("Y-m-d H:i:s");
		
		if ($harga != $harga_lama) {
			$this->db->query(" UPDATE tm_hpp_pengadaan SET harga = '$harga', tgl_update = '$tgl' WHERE id = '$idharga' ");    
		}
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "brgjadi_hpp/cform/viewhpppengadaan/index/".$cur_page;
		else
			$url_redirectnya = "brgjadi_hpp/cform/carihpppengadaan/index/".$carinya."/".$cur_page;
		
		redirect($url_redirectnya);
	}
  }
  
  // HPP packing
  function addhpppacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$data['query'] = $this->mmaster->get_hpp_brgjadi_hpp_packing();
	$data['query'] = '';
	if (is_array($data['query']) )
		$data['jum_data'] = count($data['query'])+1;
	else
		$data['jum_data'] = 2;
	$data['isi'] = 'brgjadi_hpp/vmainformhpppacking';
	$this->load->view('template',$data);
  }
  
  function submithpppacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$jum_data = $this->input->post('jum_data', TRUE)-1;

		for ($i=1;$i<=$jum_data;$i++)
		{
			// cek apakah datanya udh ada
			$cek_data = $this->mmaster->cek_data_hpp_packing($this->input->post('id_brg_jadi_'.$i, TRUE),$this->input->post('bulan_'.$i, TRUE),$this->input->post('tahun_'.$i, TRUE));
			if (count($cek_data) == 0) { 
				$this->mmaster->savehpppacking($this->input->post('id_brg_jadi_'.$i, TRUE),$this->input->post('kode_brg_jadi_'.$i, TRUE), 
							$this->input->post('harga_'.$i, TRUE),$this->input->post('bulan_'.$i, TRUE),$this->input->post('tahun_'.$i, TRUE));
			}
			else {
				if ($this->input->post('harga_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0)
					$this->db->query(" UPDATE tm_hpp_packing SET harga = '".$this->input->post('harga_'.$i, TRUE)."'
							WHERE kode_brg_jadi = '".$this->input->post('kode_'.$i, TRUE)."' ");
			}
			
		}
		redirect('brgjadi_hpp/cform/viewhpppacking');
  }
  
  function viewhpppacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'brgjadi_hpp/vformviewhpppacking';
    $keywordcari = "all";

    $jum_total = $this->mmaster->getAllhpppackingtanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/viewhpppacking/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllhpppacking($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('template',$data);
  }
  
  function carihpppacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  

	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAllhpppackingtanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/carihpppacking/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllhpppacking($config['per_page'],$this->uri->segment(6), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'brgjadi_hpp/vformviewhpppacking';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$this->load->view('template',$data);
  }
  
  function deletehpppacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletehpppacking($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "brgjadi_hpp/cform/viewhpppacking/index/".$cur_page;
	else
		$url_redirectnya = "brgjadi_hpp/cform/carihpppacking/index/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
  }
  
  function edithpppacking() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $idharga 	= $this->uri->segment(4);  
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($idharga == '')
		$idharga 	= $this->input->post('idharga', TRUE);
    
    if ($go_edit == '') {
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
		
		$data['query'] = $this->mmaster->get_hpp_packing($idharga);
		$data['isi'] = 'brgjadi_hpp/veditformhpppacking';
		$data['idharga'] = $idharga;
		$this->load->view('template',$data);
	}
	else { // update data harga
		$harga 	= $this->input->post('harga', TRUE);
		$harga_lama 	= $this->input->post('harga_lama', TRUE);
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		
		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		
		$tgl = date("Y-m-d H:i:s");
		
		if ($harga != $harga_lama) {
			$this->db->query(" UPDATE tm_hpp_packing SET harga = '$harga', tgl_update = '$tgl' WHERE id = '$idharga' ");    
		}
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "brgjadi_hpp/cform/viewhpppacking/index/".$cur_page;
		else
			$url_redirectnya = "brgjadi_hpp/cform/carihpppacking/index/".$carinya."/".$cur_page;
		
		redirect($url_redirectnya);
	}
  }
  
  // hpp gudang jadi
  function addhppgudangjadi(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	//$data['query'] = $this->mmaster->get_hpp_brgjadi_hpp_gudangjadi();
	$data['query'] = '';
	if (is_array($data['query']) )
		$data['jum_data'] = count($data['query'])+1;
	else
		$data['jum_data'] = 2;
	$data['isi'] = 'brgjadi_hpp/vmainformhppgudangjadi';
	$this->load->view('template',$data);
  }
  
  function submithppgudangjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$jum_data = $this->input->post('jum_data', TRUE)-1;

		for ($i=1;$i<=$jum_data;$i++)
		{
			// cek apakah datanya udh ada
			$cek_data = $this->mmaster->cek_data_hpp_gudangjadi($this->input->post('id_brg_jadi_'.$i, TRUE));
			if (count($cek_data) == 0) { 
				$this->mmaster->savehppgudangjadi($this->input->post('id_brg_jadi_'.$i, TRUE),
				$this->input->post('kode_brg_jadi_'.$i, TRUE), 
							$this->input->post('harga_'.$i, TRUE) );
			}
			else {
				if ($this->input->post('harga_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0)
					$this->db->query(" UPDATE tm_hpp_gudang_jadi SET harga = '".$this->input->post('harga_'.$i, TRUE)."'
							WHERE kode_brg_jadi = '".$this->input->post('kode_'.$i, TRUE)."' ");
			}
			
		}
		redirect('brgjadi_hpp/cform/viewhppgudangjadi');
  }
  
  function viewhppgudangjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'brgjadi_hpp/vformviewhppgudangjadi';
    $keywordcari = "all";

    $jum_total = $this->mmaster->getAllhppgudangjaditanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/viewhppgudangjadi/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllhppgudangjadi($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('template',$data);
  }
  
  function carihppgudangjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  

	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAllhppgudangjaditanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgjadi_hpp/cform/carihppgudangjadi/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllhppgudangjadi($config['per_page'],$this->uri->segment(6), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'brgjadi_hpp/vformviewhppgudangjadi';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$this->load->view('template',$data);
  }
  
  function deletehppgudangjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    
    $this->mmaster->deletehppgudangjadi($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "brgjadi_hpp/cform/viewhppgudangjadi/index/".$cur_page;
	else
		$url_redirectnya = "brgjadi_hpp/cform/carihppgudangjadi/index/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
  }
  
  function edithppgudangjadi() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $idharga 	= $this->uri->segment(4);  
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $carinya 	= $this->uri->segment(7);
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($idharga == '')
		$idharga 	= $this->input->post('idharga', TRUE);
    
    if ($go_edit == '') {
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['carinya'] = $carinya;
		
		$data['query'] = $this->mmaster->get_hpp_gudangjadi($idharga);
		$data['isi'] = 'brgjadi_hpp/veditformhppgudangjadi';
		$data['idharga'] = $idharga;
		$this->load->view('template',$data);
	}
	else { // update data harga
		$harga 	= $this->input->post('harga', TRUE);
		$harga_lama 	= $this->input->post('harga_lama', TRUE);
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		
		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		
		$tgl = date("Y-m-d H:i:s");
		
		if ($harga != $harga_lama) {
			$this->db->query(" UPDATE tm_hpp_gudang_jadi SET harga = '$harga', tgl_update = '$tgl' WHERE id = '$idharga' ");    
		}
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "brgjadi_hpp/cform/viewhppgudangjadi/index/".$cur_page;
		else
			$url_redirectnya = "brgjadi_hpp/cform/carihppgudangjadi/index/".$carinya."/".$cur_page;
		
		redirect($url_redirectnya);
	}
  }
  // =============
  
  function hapuskolom_kodeunit_hpp() {
	  $this->db->query(" ALTER TABLE tm_hpp_wip DROP COLUMN kode_unit ");
	  echo "berhasil hapus field kode_unit";
  }
  
  function create_tabel_kel_brg_jadi() {
		/*$sql = " 
		CREATE TABLE tm_kel_brg_jadi
		(
		  id serial NOT NULL,
		  kode character varying(2) NOT NULL,
		  nama character varying(30),
		  keterangan character varying(50),
		  tgl_input timestamp without time zone,
		  tgl_update timestamp without time zone,
		  CONSTRAINT pk_kel_brg_jadi PRIMARY KEY (id)
		)
		WITH (
		  OIDS=FALSE
		);
		ALTER TABLE tm_kel_brg_jadi
		  OWNER TO postgres;
		 ";
		 $this->db->query($sql); */
		 
		  $this->db->query(" ALTER TABLE tr_product_motif ADD COLUMN id_kel_brg_jadi integer not null default 0 ");
		 echo "berhasil create tabel tm_kel_brg_jadi";
   }
  
}
