<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('pb-cutting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'pb-cutting/vmainform';
	$data['msg'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['ukuran_bisbisan'] = $this->mmaster->get_ukuran_bisbisan();
	$data['motif_brg_jadi'] = $this->mmaster->get_motif_brg_jadi();
	$this->load->view('template',$data);

  }
  
  function edit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_pb 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_pb_cutting($id_pb); 
	$data['msg'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['ukuran_bisbisan'] = $this->mmaster->get_ukuran_bisbisan();
	$data['motif_brg_jadi'] = $this->mmaster->get_motif_brg_jadi();
	$data['isi'] = 'pb-cutting/veditform';
	$this->load->view('template',$data);

  }
  
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_pb 	= $this->input->post('id_pb', TRUE);
			$no_request 	= $this->input->post('no_request', TRUE);
			$tgl_request 	= $this->input->post('tgl_request', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);  
			$motif_brg_jadi 	= $this->input->post('motif_brg_jadi', TRUE);  
			$edit_item 	= $this->input->post('edit_item', TRUE);  
			
			$pisah1 = explode("-", $tgl_request);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_request = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
			// 18-05-2012
			$is_kode_brg_jadi_manual = $this->input->post('is_kode_brg_jadi_manual', TRUE);  
			$brg_jadi_manual = $this->input->post('brg_jadi_manual', TRUE);  
			if ($is_kode_brg_jadi_manual == '')
				$is_kode_brg_jadi_manual = 'f';
			if ($is_kode_brg_jadi_manual == 't')
				$kode_brg_jadi = "000000";
			
			$tgl = date("Y-m-d");

			//update headernya
			$this->db->query(" UPDATE tm_pb_cutting SET tgl = '$tgl_request', kode_brg_jadi = '$kode_brg_jadi', 
							brg_jadi_manual='$brg_jadi_manual', is_brg_jadi_manual='$is_kode_brg_jadi_manual', 
							keterangan = '$ket', id_motif = '$motif_brg_jadi', tgl_update='$tgl' where id= '$id_pb' ");
			//10-04-2012
			if ($edit_item == 'y') {
				//hapus dulu detailnya
				$this->db->delete('tm_pb_cutting_detail', array('id_pb_cutting' => $id_pb));
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						/*$quilting = $this->input->post('quilting_'.$i, TRUE);
						
						if ($quilting == '')
							$quilting = 'f';

						//cek qty, jika lebih besar dari stok maka otomatis disamakan dgn sisa stoknya
						
						if ($quilting == 'f') {
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE 
											kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
						}
						else {
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE 
											kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
						}
						
						$hasilrow = $query3->row();
						$stok = $hasilrow->stok; // ini stok terkini di tm_stok
						*/
						
						$qty = $this->input->post('qty_'.$i, TRUE);		
						/*if ($qty > $stok)
							$qty = $stok;
						if ($stok == '')
							$qty = '0'; */
						$id_kebutuhanperpcs = $this->input->post('id_kebutuhanperpcs_'.$i, TRUE);
						$id_ukuran_bisbisan = $this->input->post('ukuran_bisbisan_'.$i, TRUE);
						$id_marker_gelaran = $this->input->post('id_marker_'.$i, TRUE);
						
						if ($id_kebutuhanperpcs == '')
							$id_kebutuhanperpcs = 0;
						
						if ($id_ukuran_bisbisan == '')
							$id_ukuran_bisbisan = 0;
						
						if ($id_marker_gelaran == '')
							$id_marker_gelaran = 0;
						
						$bordir = $this->input->post('bordir_'.$i, TRUE);
						$kcutting = $this->input->post('kcutting_'.$i, TRUE);
						$bscutting = $this->input->post('bscutting_'.$i, TRUE);
						$bagian_bs = $this->input->post('bagian_bs_'.$i, TRUE);
						
						// 06-07-2012
						$kebbismanual = $this->input->post('kebbismanual_'.$i, TRUE);
						$vkebbismanual = $this->input->post('vkebbismanual_'.$i, TRUE);
						if ($kebbismanual == '')
							$kebbismanual = 'f';
						if ($kebbismanual == 'f')
							$vkebbismanual = '0';
						
						if ($bordir == 't' && $this->input->post('jenis_proses_'.$i, TRUE) != '2')
							$bordir = 'f';
						
						if ($bordir == '')
							$bordir = 'f';
							
						if ($kcutting == '')
							$kcutting = 'f';
						
						if ($bscutting == '') {
							$bscutting = 'f';
							$bagian_bs = '';
						}
						
						$this->db->query(" INSERT INTO tm_pb_cutting_detail (id_pb_cutting, kode_brg, qty, gelaran, jum_set, 
											jum_gelar, panjang_kain, jenis_proses, id_marker_gelaran, kode_brg_quilting, 
											id_ukuran_bisbisan, id_kebutuhan_perpcs, kode_brg_bisbisan, bordir, 
											for_kekurangan_cutting, for_bs_cutting, bagian_bs, keb_bisbisan_manual, 
											is_keb_bisbisan_manual) 
							VALUES ('$id_pb', '".$this->input->post('kode_'.$i, TRUE)."', '$qty', 
							'".$this->input->post('gelaran_'.$i, TRUE)."', '".$this->input->post('set_'.$i, TRUE)."',
							'".$this->input->post('jum_gelar_'.$i, TRUE)."', '".$this->input->post('pjg_kain_'.$i, TRUE)."',
							'".$this->input->post('jenis_proses_'.$i, TRUE)."', '".$id_marker_gelaran."',
							'".$this->input->post('kode_brg_quilting_'.$i, TRUE)."', '".$id_ukuran_bisbisan."', 
							'".$id_kebutuhanperpcs."', '".$this->input->post('kode_brg_bisbisan_'.$i, TRUE)."',
							'".$bordir."', '".$kcutting."', '$bscutting', '$bagian_bs', '$vkebbismanual', '$kebbismanual' ) ");
					}
					//die();
			} // end if edit_item == 'y'
					
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "pb-cutting/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "pb-cutting/cform/cari/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
			//redirect('pb-cutting/cform/view');
				
  }

  function submit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
			$no_request 	= $this->input->post('no_request', TRUE);
			$tgl_request 	= $this->input->post('tgl_request', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_request);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_request = $thn1."-".$bln1."-".$tgl1;
			
			$kode_brg_jadi = $this->input->post('kode_brg_jadi', TRUE);  
			$is_kode_brg_jadi_manual = $this->input->post('is_kode_brg_jadi_manual', TRUE);  
			$brg_jadi_manual = $this->input->post('brg_jadi_manual', TRUE);  
			$motif_brg_jadi = $this->input->post('motif_brg_jadi', TRUE);  
						
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data($no_request, $thn1);
			if (count($cek_data) > 0) {
				$data['isi'] = 'pb-cutting/vmainform';
				$data['msg'] = "Data no request ".$no_request." utk tahun $thn1 sudah ada..!";
				$data['kel_brg'] = $this->mmaster->get_kel_brg();
				$data['ukuran_bisbisan'] = $this->mmaster->get_ukuran_bisbisan();
				$this->load->view('template',$data);
			}
			else {
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->save($thn1, $no_request,$tgl_request, $ket, $kode_brg_jadi, $motif_brg_jadi,
										$is_kode_brg_jadi_manual, $brg_jadi_manual,
							$this->input->post('jenis_proses_'.$i, TRUE), $this->input->post('kode_brg_quilting_'.$i, TRUE),
							$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								$this->input->post('qty_'.$i, TRUE), $this->input->post('ukuran_bisbisan_'.$i, TRUE),  
								$this->input->post('gelaran_'.$i, TRUE),
								$this->input->post('set_'.$i, TRUE), $this->input->post('jum_gelar_'.$i, TRUE),
								$this->input->post('pjg_kain_'.$i, TRUE), 
								$this->input->post('id_marker_'.$i, TRUE), $this->input->post('id_kebutuhanperpcs_'.$i, TRUE),
								$this->input->post('kode_brg_bisbisan_'.$i, TRUE), $this->input->post('bordir_'.$i, TRUE),
								$this->input->post('kcutting_'.$i, TRUE), $this->input->post('bscutting_'.$i, TRUE),
								$this->input->post('bagian_bs_'.$i, TRUE), 
								$this->input->post('kebbismanual_'.$i, TRUE),
								$this->input->post('vkebbismanual_'.$i, TRUE) );
						
						/*$this->mmaster->create_bonm($no_request, 
								$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE) ); */
				}
				redirect('pb-cutting/cform/view');
			}
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'pb-cutting/vformview';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/pb-cutting/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $date_from, $date_to);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
    
    /*if ($keywordcari == '' && ($date_from == '' || $date_to == '')) {
		$date_from 	= $this->uri->segment(4);
		$date_to 	= $this->uri->segment(5);
		$keywordcari = $this->uri->segment(6);
	} */
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(6);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	
	$jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to);
    
							$config['base_url'] = base_url().'index.php/pb-cutting/cform/cari/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'pb-cutting/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }

  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);
	$quilting 	= $this->uri->segment(6);
	$kode_brg_jadi 	= $this->uri->segment(7);
	$jenis_proses 	= $this->uri->segment(8);
	$kode_brg_quilting 	= $this->uri->segment(9);
	$uk_bisbisan 	= $this->uri->segment(10);
	$motif 	= $this->uri->segment(11);
	
	if ($posisi == '' || $kel_brg == '' || $quilting == '' || $kode_brg_jadi == '' || $jenis_proses == '' || $kode_brg_quilting == '' || $uk_bisbisan == '' || $motif == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg 	= $this->input->post('kel_brg', TRUE);  
		$quilting 	= $this->input->post('quilting', TRUE);  
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);  
		$jenis_proses 	= $this->input->post('jenis_proses', TRUE);
		$kode_brg_quilting 	= $this->input->post('kode_brg_quilting', TRUE);
		$uk_bisbisan 	= $this->input->post('uk_bisbisan', TRUE);
		$motif 	= $this->input->post('motif', TRUE);
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		$id_jenis_bhn = $this->input->post('id_jenis_bhn', TRUE);  
		
		if ($keywordcari == '' && ($id_jenis_bhn == '' || $posisi == '' || $kel_brg == '' || $quilting == '' || $kode_brg_jadi == '' || $jenis_proses == '' || $kode_brg_quilting == '' || $uk_bisbisan == '' || $motif == '') ) {
			$kel_brg 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$quilting 	= $this->uri->segment(6);
			$kode_brg_jadi 	= $this->uri->segment(7);
			$jenis_proses 	= $this->uri->segment(8);
			$kode_brg_quilting 	= $this->uri->segment(9);
			$uk_bisbisan 	= $this->uri->segment(10);
			$motif 	= $this->uri->segment(11);
			$id_jenis_bhn 	= $this->uri->segment(12);
			$keywordcari 	= $this->uri->segment(13);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		if ($kode_brg_quilting == '')
			$kode_brg_quilting 	= "0";	
		if ($id_jenis_bhn == '')
			$id_jenis_bhn 	= "0";	

		/*echo "index.php/pb-cutting/cform/show_popup_brg/".$kel_brg."/".
	$posisi."/".$quilting."/".$kode_brg_jadi."/".$jenis_proses."/".$kode_brg_quilting."/".$id_jenis_bhn."/".$keywordcari; */
	// awal buka: index.php/pb-cutting/cform/show_popup_brg/B/1/f/BCG110200/1/0/0/all 
		  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $id_jenis_bhn, $quilting, $kode_brg_jadi, $jenis_proses, $kode_brg_quilting, $uk_bisbisan, $motif);
		
				$config['base_url'] = base_url()."index.php/pb-cutting/cform/show_popup_brg/".$kel_brg."/".
							$posisi."/".$quilting."/".$kode_brg_jadi."/".$jenis_proses."/".$kode_brg_quilting."/".$uk_bisbisan."/".$motif."/".$id_jenis_bhn."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(14);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $id_jenis_bhn, $quilting, $kode_brg_jadi, $jenis_proses, $kode_brg_quilting, $uk_bisbisan, $motif);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['kel_brg'] = $kel_brg;
	$data['quilting'] = $quilting;
	$data['kode_brg_jadi'] = $kode_brg_jadi;
	$data['jenis_proses'] = $jenis_proses;
	$data['kode_brg_quilting'] = $kode_brg_quilting;
	$data['uk_bisbisan'] = $uk_bisbisan;
	$data['motif'] = $motif;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['startnya'] = $config['cur_page'];
	
	$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	}
	else {
		$nama_kel	= '';
	}
	
	$data['nama_kel'] = $nama_kel;
	$data['jenis_bhn'] = $this->mmaster->get_jenis_bhn($kel_brg);
	$data['cjenis_bhn'] = $id_jenis_bhn;
	

	$this->load->view('pb-cutting/vpopupbrg',$data);
  }
  
  function show_popup_brg_bisbisan(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi 	= $this->uri->segment(4);
	
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '') {
			$posisi 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
					  
		$jum_total = $this->mmaster->get_bahanbisbisantanpalimit($keywordcari);
		$config['base_url'] = base_url()."index.php/pb-cutting/cform/show_popup_brg_bisbisan/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($jum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	
		$data['query'] = $this->mmaster->get_bahanbisbisan($config['per_page'],$config['cur_page'], $keywordcari);
		/*if ($data['query'] == '')
			$data['jum_total'] = 0;
		else
			$data['jum_total'] = count($data['query']); */
		
		$data['jum_total'] = count($jum_total);
		
		$config['total_rows'] = $data['jum_total'];
		
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['startnya'] = $config['cur_page'];
	$this->load->view('pb-cutting/vpopupbrgbisbisan',$data);
  }


  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
    
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "pb-cutting/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "pb-cutting/cform/cari/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
    //redirect('pb-cutting/cform/view');
  }
  
  function generate_nomor() {
		//$jenis_brg 	= $this->uri->segment(4);
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }
  
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '') {
			$keywordcari 	= $this->uri->segment(4);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari);

				$config['base_url'] = base_url()."index.php/pb-cutting/cform/show_popup_brg_jadi/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('pb-cutting/vpopupbrgjadi',$data);
  }
  
  //09-04-2012
  
  function print_pb(){	  
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'pb-cutting/vprintform';
	$data['msg'] = '';
	$this->load->view('template',$data);

  }
  
  function do_print_pb() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  $tgl_request 	= $this->uri->segment(4);
	
	  $data['query'] = $this->mmaster->get_pb_cutting_for_print($tgl_request); 
	  $data['tgl_request'] = $tgl_request;
	  $this->load->view('pb-cutting/vprintpb',$data);
  }
}
