<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
		
		/* Disabled 
		$this->output->cache(2);
		*/ 
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		
			$data['form_nomor_op']	= $this->lang->line('form_nomor_op');
			$data['form_tgl_op']	= $this->lang->line('form_tgl_op');	
			$data['form_cabang_op']	= $this->lang->line('form_cabang_op');
			$data['form_option_pel_op']	= $this->lang->line('form_option_pel_op');
			$data['form_option_cab_op']	= $this->lang->line('form_option_cab_op');
			$data['form_tgl_bts_kirim_op']	= $this->lang->line('form_tgl_bts_kirim_op');
			$data['form_nomor_sop_op']	= $this->lang->line('form_nomor_sop_op');
			$data['form_title_detail_op']	= $this->lang->line('form_title_detail_op');
			$data['form_jml_op']	= $this->lang->line('form_jml_op');
			$data['form_kode_produk_op']	= $this->lang->line('form_kode_produk_op');
			$data['form_nm_produk_op']	= $this->lang->line('form_nm_produk_op');
			$data['form_ket_op']	= $this->lang->line('form_ket_op');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['page_title']	= $this->lang->line('page_title_op');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
			$data['lcabang']	= "";
			$data['selected_cab']	= "";
			
			$tahun	= date("Y");
			$this->load->model('opfcdgu/mclass');
			
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgOP']	= $tgl."/".$bln."/".$thn;
			
			$qryth	= $this->mclass->getthnop();
			$qryop	= $this->mclass->getnomorop();
	
			$qrytsop	= $this->mclass->getthnsop();
			$qrysop		= $this->mclass->getnomorsop();
					
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			$data['opt_cabang']	= $this->mclass->lcabang();
			
			if($qryth->num_rows() > 0) {
				$th		= $qryth->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}
			
			if($thn==$tahun) {
				if($qryop->num_rows() > 0)  {
					$row	= $qryop->row_array();
					$op		= $row['iopcode']+1;		
					switch(strlen($op)) {
						case "1": $nomorop	= "0000".$op;
						break;
						case "2": $nomorop	= "000".$op;
						break;	
						case "3": $nomorop	= "00".$op;
						break;
						case "4": $nomorop	= "0".$op;
						break;
						case "5": $nomorop	= $op;
						break;	
					}
				} else {
					$nomorop		= "00001";
				}
				$nomor	= $tahun.$nomorop;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['no']	= $nomor;
	
			if($qrytsop->num_rows() > 0) {
				$th		= $qrytsop->row_array();
				$thn2	= $th['thisop'];
			} else {
				$thn2	= $tahun;
			}
			
			if($thn2==$tahun) {
				if($qrysop->num_rows() > 0)  {
					$row2	= $qrysop->row_array();
					$sop		= $row2['isop']+1;		
					switch(strlen($sop)) {
						case "1": $nomorsop	= "0000".$sop;
						break;
						case "2": $nomorsop	= "000".$sop;
						break;	
						case "3": $nomorsop	= "00".$sop;
						break;
						case "4": $nomorsop	= "0".$sop;
						break;
						case "5": $nomorsop	= $sop;
						break;	
					}
				} else {
					$nomorsop		= "00001";
				}
				$nomor2	= $tahun.$nomorsop;
			} else {
				$nomor2	= $tahun."00001";
			}
			$data['sop']	= $nomor2;		
			$data['isi']='opfcdgu/vform';		
			$this->load->view('template',$data);
		
	}
	
	function cari_cabang() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$i_customer	= $this->input->post('ibranch')?$this->input->post('ibranch'):$this->input->get_post('ibranch');
		$this->load->model('opfcdgu/mclass');
		
		$qryth	= $this->mclass->getthnop_inc_cabang($i_customer);
		$qryop	= $this->mclass->getnomorop_inc_cabang($i_customer);
		
		$tahun	= date("Y");
		if($qryth->num_rows() > 0) {
			$th		= $qryth->row_array();
			$thn	= $th['thn'];
		} else {
			$thn	= $tahun;
		}
		
		if($thn==$tahun) {
			if($qryop->num_rows() > 0)  {
				$row	= $qryop->row_array();
				$op		= $row['iopcode']+1;		
				switch(strlen($op)) {
					case "1": $nomorop	= "0000".$op;
					break;
					case "2": $nomorop	= "000".$op;
					break;	
					case "3": $nomorop	= "00".$op;
					break;
					case "4": $nomorop	= "0".$op;
					break;
					case "5": $nomorop	= $op;
					break;	
				}
			} else {
				$nomorop		= "00001";
			}
			$nomor	= $tahun.$nomorop;
		} else {
			$nomor	= $tahun."00001";
		}
		
		$query	= $this->mclass->getcabang($i_customer);
		
		if($query->num_rows()>0) {
			
			$c	= "";
			$cabang	= $query->result();

			foreach($cabang as $row) {
				$c.="<option value=".$row->i_branch_code." >".$row->e_branch_name." ( ".$row->e_initial." ) "."</option>";
			}
			
			echo "<select name=\"i_branch\" id=\"i_branch\" onchange=\"incabcab(this.value);\" >
					<option value=\"\">[Pilih Cabang Pelanggan]</option>".$c."</select>";
			echo "<input type=\"hidden\" name=\"nomorop_hidden\" id=\"nomorop_hidden\" value=\"$nomor\" >";
		}
	}
	
	function cari_product(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$this->load->model('opfcdgu/mclass');
		$query	= $this->mclass->getproduct();

		$opt_product="<select name='test' size='10'>";
		$opt_product.="<option>[Pilih Kode Product]</option> ";
		foreach ($product as $row) {
			$opt_product.="<option value='";
			$opt_product.= $row->i_product_base."||".$row->e_product_basename."'>".$row->e_product_basename;   
			$opt_product.="</option>";
		} 
		$opt_product.="</select>";
	
		$option_choice = "^/&".$opt_product."^/&";	
		echo $option_choice;
	}	

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4,0);
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";		
		$data['lurl']		= base_url();
		$data['key']	= "";
				
		$this->load->model('opfcdgu/mclass');
		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();

		$config['base_url'] = base_url().'index.php/opfcdgu/cform/listbarangjadinext/'.$iterasi."/";
		$config['total_rows']	= $jml;
		$config['per_page'] 	= 20;
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $this->uri->segment(5,0);
		
		$this->pagination_ori->initialize($config);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($config['per_page'],$config['cur_page']);
		
		
		$query->free_result();
		$this->load->view('opfcdgu/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4,0);
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";		
		$data['lurl']		= base_url();
		$data['key']	= "";
				
		$this->load->model('opfcdgu/mclass');
		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();

		$config['base_url'] = base_url().'index.php/opfcdgu/cform/listbarangjadi/'.$iterasi."/";
		$config['total_rows'] 	= $jml;
		$config['per_page'] 	= 20;
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $this->uri->segment(5,0);
		
		$this->pagination_ori->initialize($config);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($config['per_page'],$config['cur_page']);
		$this->load->view('opfcdgu/vlistformbrgjadi',$data);
		
		$query->free_result();
	}

	function flistbarangjadi() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('opfcdgu/mclass');
		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname')\">".$row->imotif."</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname')\">".$row->motifname."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname')\">".$row->qty."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
	
	function simpan() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasii	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iteration	= $iterasii; // 0+1 = 2
		
		$v_count	= array();
		$i_product	= array();
		$e_product_name	= array();
		$e_note		= array();
		
		$iop		= $this->input->post('i_op');
		$i_customer	= $this->input->post('i_customer');
		$i_branch	= $this->input->post('i_branch');
		$d_op		= $this->input->post('d_op');
		$d_delivery_limit	= $this->input->post('d_delivery_limit');
		$i_sop		= $this->input->post('i_sop');
		$fopdropforcast	= $this->input->post('f_op_dropforcast')=='1'?'TRUE':'FALSE';
		$jenis_op	= $this->input->post('jenis_op');
		
		$v_count_0	= $this->input->post('v_count_'.'tblItem'.'_'.'0');
		
		$ex_d_op	= explode("/",$d_op,strlen($d_op)); // dd/mm/YYYY
		$ex_d_delivery_limit	= explode("/",$d_delivery_limit,strlen($d_delivery_limit)); // dd/mm/YYYY
		
		$nw_d_op	= $ex_d_op[2]."-".$ex_d_op[1]."-".$ex_d_op[0];
		$nw_d_delivery_limit	= $ex_d_delivery_limit[2]."-".$ex_d_delivery_limit[1]."-".$ex_d_delivery_limit[0];
		
		for($cacah=0; $cacah<=$iteration; $cacah++) { // iterasi=2, 0<2, 1<2
		
			$v_count[$cacah]	= $this->input->post('v_count_'.'tblItem'.'_'.$cacah);
			$i_product[$cacah]	= $this->input->post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah);
			$e_note[$cacah]		= $this->input->post('e_note_'.'tblItem'.'_'.$cacah);
			
			if($v_count[$cacah]=='')
				$v_count[$cacah] = 0;
			
		}
		
		$this->load->model('opfcdgu/mclass');
				
		if( !empty($iop) && 
		    !empty($i_sop) && 
		    !empty($i_customer) && 
		    !empty($i_branch)) {

			if(!empty($v_count_0)) {
				$this->mclass->msimpan($iop,$i_customer,$i_branch,$nw_d_op,$nw_d_delivery_limit,$i_sop,$fopdropforcast,
							$v_count,$i_product,$e_product_name,$e_note,$iteration, $jenis_op);
			}else{
				print "<script>alert(\"Maaf, item OP harus terisi. Terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
				
			}
		}else{
			$data['form_nomor_op']	= $this->lang->line('form_nomor_op');
			$data['form_tgl_op']	= $this->lang->line('form_tgl_op');	
			$data['form_cabang_op']	= $this->lang->line('form_cabang_op');
			$data['form_option_pel_op']	= $this->lang->line('form_option_pel_op');
			$data['form_option_cab_op']	= $this->lang->line('form_option_cab_op');
			$data['form_tgl_bts_kirim_op']	= $this->lang->line('form_tgl_bts_kirim_op');
			$data['form_nomor_sop_op']	= $this->lang->line('form_nomor_sop_op');
			$data['form_title_detail_op']	= $this->lang->line('form_title_detail_op');
			$data['form_jml_op']	= $this->lang->line('form_jml_op');
			$data['form_kode_produk_op']	= $this->lang->line('form_kode_produk_op');
			$data['form_nm_produk_op']	= $this->lang->line('form_nm_produk_op');
			$data['form_ket_op']	= $this->lang->line('form_ket_op');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['page_title']	= $this->lang->line('page_title_op');
			$data['detail']		= "";
			$data['list']		= "";

			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
			$data['lcabang']	= "";
			$data['selected_cab']	= "";
			
			$tahun	= date("Y");
			
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgOP']	= $tgl."/".$bln."/".$thn;
			
			$qryth	= $this->mclass->getthnop();
			$qryop	= $this->mclass->getnomorop();
	
			$qrytsop	= $this->mclass->getthnsop();
			$qrysop		= $this->mclass->getnomorsop();
		
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			$data['opt_cabang']	= $this->mclass->lcabang();
				
			if($qryth->num_rows() > 0) {
				$th		= $qryth->row_array();
				$thn	= $th['thn'];
			}else{
				$thn	= $tahun;
			}
			
			if($thn==$tahun) {
				if($qryop->num_rows() > 0)  {
					$row	= $qryop->row_array();
					$op		= $row['iopcode']+1;		
					switch(strlen($op)) {
						case "1": $nomorop	= "0000".$op;
						break;
						case "2": $nomorop	= "000".$op;
						break;	
						case "3": $nomorop	= "00".$op;
						break;
						case "4": $nomorop	= "0".$op;
						break;
						case "5": $nomorop	= $op;
						break;	
					}
				} else {
					$nomorop		= "00001";
				}
				$nomor	= $tahun.$nomorop;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['no']	= $nomor;

			if($qrytsop->num_rows() > 0) {
				$th		= $qrytsop->row_array();
				$thn2	= $th['thisop'];
			}else{
				$thn2	= $tahun;
			}
			
			if($thn2==$tahun) {
				if($qrysop->num_rows()>0)  {
					
					$row	= $qrysop->row_array();
					
					$sop	= $row['isop']+1;		
					
					switch(strlen($sop)) {
						case "1": $nomorsop	= "0000".$sop;
						break;
						case "2": $nomorsop	= "000".$sop;
						break;	
						case "3": $nomorsop	= "00".$sop;
						break;
						case "4": $nomorsop	= "0".$sop;
						break;
						case "5": $nomorsop	= $sop;
						break;	
					}
				}else{
					$nomorsop		= "00001";
				}
				
				$nomor2	= $tahun.$nomorsop;
			}else{
				$nomor2	= $tahun."00001";
			}
			
			$data['sop']	= $nomor2;		
			
			print "<script>alert(\"Maaf, Order Pembelian (OP) gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			/*** $this->load->view('opfcdgu/vmainform',$data); ***/
		}
	} 
	
	function cari_op() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$nop	= $this->input->post('nop');
		$icust	= $this->input->post('icust');
		
		$this->load->model('opfcdgu/mclass');
		
		$qnop	= $this->mclass->cari_op($nop,$icust);
		
		if($qnop->num_rows()>0) {
			/*** print "<script>alert(\"Maaf, Nomor OP sudah ada. Terimakasih.\");window.open(\"index\", \"_self\");</script>"; ***/
			echo "Maaf, No. OP sudah ada!";
		}
	}
	
	function cari_sop() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$nsop	= $this->input->post('nsop')?$this->input->post('nsop'):$this->input->get_post('nsop');
		
		$this->load->model('opfcdgu/mclass');
		
		$qnsop	= $this->mclass->cari_sop($nsop);
		
		if($qnsop->num_rows()>0) {
			/*** print "<script>alert(\"Maaf, Nomor OP sudah ada. Terimakasih.\");window.open(\"index\", \"_self\");</script>"; ***/
			echo "Maaf, No. SOP sudah ada!";
		}
	}	
	
	// 02-06-2012, ambil list OP dari DGU
	function opdgu() {
		
			$data['limages']		= base_url();
			
			$this->load->model('opfcdgu/mclass');
			$data['query']	= $this->mclass->get_opfc_dgu();
			$data['isi']='opfcdgu/vformopdgu';		
			$this->load->view('template',$data);
			
		
	}
	
	function simpanopdgu() {
		$iterasi			= $this->input->post('iteration',true); 
		$d_delivery_limit	= $this->input->post('d_delivery_limit');
		$exp_ddl = explode("/", $d_delivery_limit);
		$tgl1 = trim($exp_ddl[0]); 
		$bln1 = trim($exp_ddl[1]);
		$thn1 = trim($exp_ddl[2]);
		$d_delivery_limit = $thn1."-".$bln1."-".$tgl1;
			
		$f_op_dropforcast	= $this->input->post('f_op_dropforcast');
		if ($f_op_dropforcast == '')
			$f_op_dropforcast = 'f';
		else
			$f_op_dropforcast = 't';
		//~ 
		$jenis_op	= $this->input->post('jenis_op');
		if($jenis_op== 0){
			redirect('opfcdgu/cform/opdgu');
			}
		//~ echo $d_delivery_limit;
		$no_op	= array();
		$tgl_op	= array();
		$i_product_motif	= array();
		$i_product	= array();
		$e_product_name	= array();
		$qty	= array();
		$i_customer	= '1'; // khusus DGU
		//$i_branch	= '1101'; // DGU pst
		$i_branch_code = array();
		$fck	= array();

		for($cacah=1;$cacah<=$iterasi;$cacah++) {
		//~ for($cacah=0;$cacah<=$iterasi;$cacah++) {
				$no_op[$cacah] = $this->input->post('no_op_'.$cacah); //echo $no_op[$cacah]."<br>";
				$tgl_op[$cacah] = $this->input->post('tgl_op_'.$cacah);
				$i_branch_code[$cacah] = $this->input->post('i_branch_code_'.$cacah);
				
				$i_product[$cacah] = $this->input->post('i_product_'.$cacah);
				
				$i_product_motif[$cacah] = $this->input->post('i_product_motif_'.$cacah);
				$e_product_name[$cacah] = $this->input->post('e_product_name_'.$cacah);
				$qty[$cacah] = $this->input->post('qty_'.$cacah);
				$fck[$cacah] = $this->input->post('f_ck_tblItem'.$cacah);
		}

		$this->load->model('opfcdgu/mclass');
		/*$qryth	= $this->mclass->getthnop();
		$qryop	= $this->mclass->getnomorop();
		$qrytsop	= $this->mclass->getthnsop();
		$qrysop		= $this->mclass->getnomorsop(); */
		
		$this->mclass->msimpanopdgu($no_op,$tgl_op,$i_product, $i_product_motif,$e_product_name, $qty, $fck,$iterasi, 
						$i_customer, $i_branch_code, $d_delivery_limit, $f_op_dropforcast, $jenis_op);
	}
}
?>
