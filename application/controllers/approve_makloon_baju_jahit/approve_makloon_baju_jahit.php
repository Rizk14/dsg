<?php
class approve_makloon_baju_jahit extends CI_Controller
{
    public $data = array(
        'halaman' => 'approve_makloon_baju_jahit',
        'isi' => 'approve_makloon_baju_jahit/approve_makloon_baju_jahit_list',
        'title' => 'Data Approve Makloon Baju',
    );

     public function __construct()
    {
        parent::__construct();
        $this->load->model('approve_makloon_baju_jahit/approve_makloon_baju_jahit_model', 'approve_makloon_baju_jahit');
    }
    
     public function view($offset = 0)
    {
        $approve_makloon_baju_jahit = $this->approve_makloon_baju_jahit->get_all_paged_custom($offset);
        if ($approve_makloon_baju_jahit) {
            $this->data['approve_makloon_baju_jahit'] = $approve_makloon_baju_jahit;
            $this->data['paging'] = $this->approve_makloon_baju_jahit->paging('biasa', site_url('approve_makloon_baju_jahit/approve_makloon_baju_jahit/halaman/'), 4);
        } else {
            $this->data['approve_makloon_baju_jahit'] = 'Tidak ada data Approve Makloon Baju.';
        }
        $this->load->view('template', $this->data);
    }

    public function cari($offset = 0)
    {
        $approve_makloon_baju_jahit = $this->approve_makloon_baju_jahit->cari($offset);
        if ($approve_makloon_baju_jahit) {
            $this->data['approve_makloon_baju_jahit'] = $approve_makloon_baju_jahit;
            $this->data['paging'] = $this->approve_makloon_baju_jahit->paging('pencarian', site_url('approve_makloon_baju_jahit/approve_makloon_baju_jahit/cari/'), 4);
        } else {
            $this->data['approve_makloon_baju_jahit'] = 'Data tidak ditemukan.'. anchor('approve_makloon_baju_jahit/approve_makloon_baju_jahit', ' Tampilkan semua approve_makloon_baju_jahit.', 'class="alert-link"');
        }
        $this->load->view('template', $this->data);
    }

   
    public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data Approve Makloon Baju';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data Approve Makloon Baju';
        $this->load->view('template', $this->data);
    }

   

    // Ubah status verifikasi
    public function ubah_status_verifikasi($id)
    {
        // Pastikan data approve_makloon_baju_jahit ada.
        $approve_makloon_baju_jahit = $this->approve_makloon_baju_jahit->get($id);
        if (! $approve_makloon_baju_jahit) {
            $this->session->set_flashdata('pesan_error', 'Data Approve Makloon Baju tidak ada. Kembali ke halaman ' . anchor('approve_makloon_baju_jahit/approve_makloon_baju_jahit/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_jahit/approve_makloon_baju_jahit/error');
        }

        // Ubah status verifikasi
        if ($this->approve_makloon_baju_jahit->ubah_status_approve($id, $approve_makloon_baju_jahit->status_approve)) {
            $this->session->set_flashdata('pesan', 'Status verifikasi berhasil diubah. Kembali ke halaman ' . anchor('approve_makloon_baju_jahit/approve_makloon_baju_jahit/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_jahit/approve_makloon_baju_jahit/sukses');
        } else {
            $this->session->set_flashdata('pesan', 'Status verifikasi gagal diubah. Kembali ke halaman ' . anchor('approve_makloon_baju_jahit/approve_makloon_baju_jahit/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_jahit/approve_makloon_baju_jahit/error');
        }
    }

    
    public function link_detail()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $approve_makloon_baju_jahit = $this->approve_makloon_baju_jahit->getAllDetail($id);
        if (! $approve_makloon_baju_jahit) {
            $this->session->set_flashdata('pesan_error', 'Data Detail Makloon Baju Gudang Jadi Wip tidak ada. Kembali ke halaman ' . anchor('approve_makloon_baju_jahit/approve_makloon_baju_jahit/view', 'Approve Makloon Baju.', 'class="alert-link"'));
            redirect('approve_makloon_baju_jahit/master-barang/error');
        }
		$this->data['list_unit_jahit'] = $this->approve_makloon_baju_jahit->get_unit_jahit(); 
		$this->data['list_unit_packing'] = $this->approve_makloon_baju_jahit->get_unit_packing(); 
		$this->data['list_gudang'] = $this->approve_makloon_baju_jahit->get_gudang(); 
        $this->data['values'] = $approve_makloon_baju_jahit;
		$this->data['isi'] = 'approve_makloon_baju_jahit/approve_makloon_baju_jahit_form_edit';
        $this->load->view('template', $this->data);
	 }

    
}
