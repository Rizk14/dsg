<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('app-sj/mmaster');
  }
     function index(){

		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			
			redirect('loginform');
		}

		$tgl_sjmd='';
		$tgl_sjmk='';
		
		$data['msg'] = '';
		$data['id_sjmasukwip'] = '';
		$data['go_proses'] = '';
		$data['unit_jahit2'] = $this->mmaster->get_unit_jahit();
		$data['unit_packing2'] = $this->mmaster->get_unit_packing();
		$data['tgl_sjmd'] = $tgl_sjmd;	
		$data['tgl_sjmk'] = $tgl_sjmk;
		
		$data['isi'] = 'app-sj/vmainform';
		$this->load->view('template',$data);
	
	
  }
  
   function addsjkeluar(){

		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			
			redirect('loginform');
		}

		$tgl_sjmd='';
		$tgl_sjmk='';
		
		$data['msg'] = '';
		$data['id_sjmasukwip'] = '';
		$data['go_proses'] = '';
		$data['unit_jahit2'] = $this->mmaster->get_unit_jahit();
		$data['unit_packing2'] = $this->mmaster->get_unit_packing();
		$data['tgl_sjmd'] = $tgl_sjmd;	
		$data['tgl_sjmk'] = $tgl_sjmk;
		
		$data['isi'] = 'app-sj/vmainformsjkeluar';
		$this->load->view('template',$data);
	
	
  }
  
  function submit(){
	  
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			
			redirect('loginform');
		}
	  
	
	$kd_brg = $this->input->post('kd_brg', TRUE);  
	$id_sjmasukwip_detail = $this->input->post('id_brg', TRUE);  
	$list_brg_detail = explode(";", $id_sjmasukwip_detail);
	$id_sjmasukwip = $this->input->post('id_sjmasukwip', TRUE);
	$list_brg = explode(";", $id_sjmasukwip);
	$id_unit_jahit2 = $this->input->post('id_unit_jahit2', TRUE);    
	$tgl_sjmd = $this->input->post('tgl_sjmd', TRUE);    
	$tgl_sjmk = $this->input->post('tgl_sjmk', TRUE); 
	
	$jenismasuk = $this->input->post('jenismasuk',TRUE);  
					$jumlah_input=count($list_brg);
					for ($i=0;$i<=$jumlah_input;$i++)
					{
						$this->mmaster->save($list_brg[$i], $list_brg_detail[$i]);	
					}
					redirect('app-sj/cform/view');
				
     }
     
      function submitsjkeluar(){
	  
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			
			redirect('loginform');
		}
	  
	
	$kd_brg = $this->input->post('kd_brg', TRUE);  
	$id_sjkeluarwip_detail = $this->input->post('id_brg', TRUE);  
	$list_brg_detail = explode(";", $id_sjkeluarwip_detail);
	$id_sjkeluarwip = $this->input->post('id_sjkeluarwip', TRUE);
	$list_brg = explode(";", $id_sjkeluarwip);
	$id_unit_jahit2 = $this->input->post('id_unit_jahit2', TRUE);    
	$tgl_sjmd = $this->input->post('tgl_sjmd', TRUE);    
	$tgl_sjmk = $this->input->post('tgl_sjmk', TRUE); 
	
	$jeniskeluar = $this->input->post('jeniskeluar',TRUE);  
					$jumlah_input=count($list_brg);
					for ($i=0;$i<=$jumlah_input;$i++)
					{
						$this->mmaster->savesjkeluar($list_brg[$i], $list_brg_detail[$i]);	
					}
					redirect('app-sj/cform/viewsjkeluar');
				
     }
    function show_popup_sjmasukwip(){
	// =======================
	// disini coding utk pengecekan user login
//========================
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		
	$id_ujh 	= $this->uri->segment(4);
	$id_upk 	= $this->uri->segment(5);
	$js_msk		= $this->uri->segment(6);
	$tgl_sjmd	= $this->uri->segment(7);
	
	
	
	if ($tgl_sjmd==''){
		$tgl_sjmd="00-00-0000";
		}
	$pisah1 = explode("-", $tgl_sjmd);
						$tgl1= $pisah1[0];
						$bln1= $pisah1[1];
						$thn1= $pisah1[2];
	$tgldari=$thn1."-".$bln1."-".$tgl1;
	//print_r($tgldari);
	
	$tgl_sjmk	= $this->uri->segment(8);
	if (empty($tgl_sjmk)){
		$tgl_sjmk="00-00-0000";
		}
	$pisah2 = explode("-", $tgl_sjmk);
						$tgl2= $pisah2[0];
						$bln2= $pisah2[1];
						$thn2= $pisah2[2];
	$tglke=$thn2."-".$bln2."-".$tgl2;
	

	$keywordcari 	= $this->input->post('cari', TRUE);  
	//$cunit_jahit 	= $this->input->post('unit_jahit', TRUE);  
	
	if ($keywordcari == '' && ($id_ujh == ''||$id_upk == '' ) ) {
		$id_ujh 	= $this->uri->segment(4);
		$id_upk 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(9);
	}
	

	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($id_ujh == '')
		$id_ujh = $this->input->post('id_ujh', TRUE);  
		
	if ($id_upk == '')
		$id_upk = $this->input->post('id_upk', TRUE);  
	
	
	$jum_total =count( $this->mmaster->get_sjmasukwiptanpalimit($id_ujh, $keywordcari, $tgldari, $tglke,$js_msk,$id_upk)); 
					
	$data['query'] = $this->mmaster->get_sjmasukwip($id_ujh, $keywordcari, $tgldari, $tglke,$js_msk,$id_upk); 

	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['id_ujh'] = $id_ujh;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_ujh' ");
	
	$hasilrow = $query3->row();
	if ($query3->num_rows() != 0) {
		$kode_unit_jahit	= $hasilrow->kode_unit;
		$nama_unit_jahit	= $hasilrow->nama;
	}
	else {
		$kode_unit_jahit	= "-";
		$nama_unit_jahit	= "-";
	}
	
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['id_upk'] = $id_upk;
	
		$query4	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$id_upk' ");
	
	$hasilrow = $query4->row();
	if ($query4->num_rows() != 0) {
		$kode_unit_packing	= $hasilrow->kode_unit;
		$nama_unit_packing	= $hasilrow->nama;
	}
	else {
		$kode_unit_packing	= "-";
		$nama_unit_packing	= "-";
	}
	
	$data['kode_unit_packing'] = $kode_unit_packing;
	$data['nama_unit_packing'] = $nama_unit_packing;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['nama_unit_jahit'] = $nama_unit_jahit;
	$data['jum_total'] = $jum_total;
	
	$this->load->view('app-sj/vpopupsjmasukwip',$data);

  }
  
   function show_popup_sjkeluarwip(){
	// =======================
	// disini coding utk pengecekan user login
//========================
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		
	$id_ujh 	= $this->uri->segment(4);
	$id_upk 	= $this->uri->segment(5);
	$js_msk		= $this->uri->segment(6);
	$tgl_sjmd	= $this->uri->segment(7);
	
	
	
	if ($tgl_sjmd==''){
		$tgl_sjmd="00-00-0000";
		}
	$pisah1 = explode("-", $tgl_sjmd);
						$tgl1= $pisah1[0];
						$bln1= $pisah1[1];
						$thn1= $pisah1[2];
	$tgldari=$thn1."-".$bln1."-".$tgl1;
	//print_r($tgldari);
	
	$tgl_sjmk	= $this->uri->segment(8);
	if (empty($tgl_sjmk)){
		$tgl_sjmk="00-00-0000";
		}
	$pisah2 = explode("-", $tgl_sjmk);
						$tgl2= $pisah2[0];
						$bln2= $pisah2[1];
						$thn2= $pisah2[2];
	$tglke=$thn2."-".$bln2."-".$tgl2;
	

	$keywordcari 	= $this->input->post('cari', TRUE);  
	//$cunit_jahit 	= $this->input->post('unit_jahit', TRUE);  
	
	if ($keywordcari == '' && ($id_ujh == '' ) ) {
		$id_ujh 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(9);
	}
	

	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($id_ujh == '')
		$id_ujh = $this->input->post('id_ujh', TRUE);  
		
	if ($id_upk == '')
		$id_upk = $this->input->post('id_upk', TRUE);  
	
	
	$jum_total =count( $this->mmaster->get_sjkeluarwiptanpalimit($id_ujh, $keywordcari, $tgldari, $tglke,$js_msk,$id_upk)); 
					
	$data['query'] = $this->mmaster->get_sjkeluarwip($id_ujh, $keywordcari, $tgldari, $tglke,$js_msk,$id_upk); 

	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['id_ujh'] = $id_ujh;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_ujh' ");
	
	$hasilrow = $query3->row();
	if ($query3->num_rows() != 0) {
		$kode_unit_jahit	= $hasilrow->kode_unit;
		$nama_unit_jahit	= $hasilrow->nama;
	}
	else {
		$kode_unit_jahit	= "-";
		$nama_unit_jahit	= "-";
	}
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['id_upk'] = $id_upk;
	
	
		$query4	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$id_upk' ");
	
	$hasilrow = $query4->row();
	if ($query4->num_rows() != 0) {
		$kode_unit_packing	= $hasilrow->kode_unit;
		$nama_unit_packing	= $hasilrow->nama;
	}
	else {
		$kode_unit_packing	= "-";
		$nama_unit_packing	= "-";
	}
	
	$data['kode_unit_packing'] = $kode_unit_packing;
	$data['nama_unit_packing'] = $nama_unit_packing;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['nama_unit_jahit'] = $nama_unit_jahit;
	$data['jum_total'] = $jum_total;
	
	$this->load->view('app-sj/vpopupsjkeluarwip',$data);

  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	$id_unit_jahit = '0';
	$id_unit_packing = '0';
	$gudang = '0';
	$caribrg = "all";
	$filterbrg = "n";
	$jenis_masuk = "all";
	
    $jum_total = $this->mmaster->getAllsjmasuktanpalimit($keywordcari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_masuk);
							$config['base_url'] = base_url().'index.php/app-sj/cform/view/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllsjmasuk($config['per_page'],$this->uri->segment(4), $keywordcari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_masuk);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['gudang'] = $gudang;
	$data['id_unit_jahit'] = $id_unit_jahit;
	$data['id_unit_packing'] = $id_unit_packing;
	if ($jenis_masuk == "all")
		$data['jenis_masuk'] = '';
	else
		$data['jenis_masuk'] = $jenis_masuk;
		$data['isi'] = 'app-sj/vformviewsjmasuk';
	$this->load->view('template',$data);
  }
  
  function viewsjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	$id_unit_jahit = '0';
	$id_unit_packing = '0';
	$gudang = '0';
	$caribrg = "all";
	$filterbrg = "n";
	$jenis_keluar = "all";
	
    $jum_total = $this->mmaster->getAllsjkeluartanpalimit($keywordcari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_keluar);
							$config['base_url'] = base_url().'index.php/app-sj/cform/view/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllsjkeluar($config['per_page'],$this->uri->segment(4), $keywordcari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_keluar);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['gudang'] = $gudang;
	$data['id_unit_jahit'] = $id_unit_jahit;
	$data['id_unit_packing'] = $id_unit_packing;
	if ($jenis_keluar == "all")
		$data['jenis_keluar'] = '';
	else
		$data['jenis_keluar'] = $jenis_keluar;
		$data['isi'] = 'app-sj/vformviewsjkeluar';
	$this->load->view('template',$data);
  }
  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $cunit_jahit 	= $this->uri->segment(7);
    $tgl_awal 	= $this->uri->segment(8);
    $tgl_akhir 	= $this->uri->segment(9);
    $carinya 	= $this->uri->segment(10);
    $caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "app-sj/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "app-sj/cform/cari/".$cunit_jahit."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
	
	redirect($url_redirectnya);
					
   
  }
  
   function deletesjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $cunit_jahit 	= $this->uri->segment(7);
    $tgl_awal 	= $this->uri->segment(8);
    $tgl_akhir 	= $this->uri->segment(9);
    $carinya 	= $this->uri->segment(10);
    $caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
    $this->mmaster->deletesjkeluar($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "app-sj/cform/viewsjkeluar/index/".$cur_page;
	else
		$url_redirectnya = "app-sj/cform/carisjkeluar/".$cunit_jahit."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
	
	redirect($url_redirectnya);
					
   
  }
  
  function carisjmasuk(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);
	$id_unit_packing 	= $this->input->post('id_unit_packing', TRUE);
	$gudang 	= $this->input->post('gudang', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
	$jenis_masuk 	= $this->input->post('jenis_masuk', TRUE);
	
	// 24-02-2015
	$submit2 	= $this->input->post('submit2', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($id_unit_jahit == '')
		$id_unit_jahit = $this->uri->segment(6);
	if ($id_unit_packing == '')
		$id_unit_packing = $this->uri->segment(7);
	if ($gudang == '')
		$gudang = $this->uri->segment(8);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(9);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(10);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(11);
	if ($jenis_masuk == '')
		$jenis_masuk = $this->uri->segment(12);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($id_unit_jahit == '')
		$id_unit_jahit = '0';
	if ($id_unit_packing == '')
		$id_unit_packing = '0';
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($caribrg == '')
		$caribrg = "all";
	if ($filterbrg == '')
		$filterbrg = "n";
	if ($jenis_masuk == '')
		$jenis_masuk = "all";
	

		$jum_total = $this->mmaster->getAllsjmasuktanpalimit($keywordcari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_masuk);
								$config['base_url'] = base_url().'index.php/app-sj/cform/carisjmasuk/'.$date_from.'/'.$date_to.'/'.$id_unit_jahit.'/'.$id_unit_packing.'/'.$gudang.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg.'/'.$jenis_masuk;
								$config['total_rows'] = count($jum_total); 
								$config['per_page'] = '10';
								$config['first_link'] = 'Awal';
								$config['last_link'] = 'Akhir';
								$config['next_link'] = 'Selanjutnya';
								$config['prev_link'] = 'Sebelumnya';
								$config['cur_page'] = $this->uri->segment(13);
								$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllsjmasuk($config['per_page'],$this->uri->segment(13), $keywordcari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_masuk);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = 1;
		
		$data['isi'] = 'app-sj/vformviewsjmasuk';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		if ($date_from=="00-00-0000")
			$data['date_from'] = '';
		else
			$data['date_from'] = $date_from;
		
		if ($date_to=="00-00-0000")
			$data['date_to'] = '';
		else
			$data['date_to'] = $date_to;
		
		if ($caribrg == "all")
			$data['caribrg'] = '';
		else
			$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;
		
		$data['gudang'] = $gudang;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['id_unit_packing'] = $id_unit_packing;
		if ($jenis_masuk == "all")
			$data['jenis_masuk'] = '';
		else
			$data['jenis_masuk'] = $jenis_masuk;
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$this->load->view('template',$data);
	}
	function carisjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);
	$id_unit_packing 	= $this->input->post('id_unit_packing', TRUE);
	$gudang 	= $this->input->post('gudang', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
	$jenis_keluar 	= $this->input->post('jenis_keluar', TRUE);
	
	// 24-02-2015
	$submit2 	= $this->input->post('submit2', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($id_unit_jahit == '')
		$id_unit_jahit = $this->uri->segment(6);
	if ($id_unit_packing == '')
		$id_unit_packing = $this->uri->segment(7);
	if ($gudang == '')
		$gudang = $this->uri->segment(8);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(9);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(10);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(11);
	if ($jenis_keluar == '')
		$jenis_keluar = $this->uri->segment(12);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($id_unit_jahit == '')
		$id_unit_jahit = '0';
	if ($id_unit_packing == '')
		$id_unit_packing = '0';
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($caribrg == '')
		$caribrg = "all";
	if ($filterbrg == '')
		$filterbrg = "n";
	if ($jenis_keluar == '')
		$jenis_keluar = "all";
	

		$jum_total = $this->mmaster->getAllsjkeluartanpalimit($keywordcari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_keluar);
								$config['base_url'] = base_url().'index.php/app-sj/cform/carisjkeluar/'.$date_from.'/'.$date_to.'/'.$id_unit_jahit.'/'.$id_unit_packing.'/'.$gudang.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg.'/'.$jenis_keluar;
								$config['total_rows'] = count($jum_total); 
								$config['per_page'] = '10';
								$config['first_link'] = 'Awal';
								$config['last_link'] = 'Akhir';
								$config['next_link'] = 'Selanjutnya';
								$config['prev_link'] = 'Sebelumnya';
								$config['cur_page'] = $this->uri->segment(13);
								$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllsjkeluar($config['per_page'],$this->uri->segment(13), $keywordcari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_keluar);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = 1;
		
		$data['isi'] = 'app-sj/vformviewsjkeluar';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		if ($date_from=="00-00-0000")
			$data['date_from'] = '';
		else
			$data['date_from'] = $date_from;
		
		if ($date_to=="00-00-0000")
			$data['date_to'] = '';
		else
			$data['date_to'] = $date_to;
		
		if ($caribrg == "all")
			$data['caribrg'] = '';
		else
			$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;
		
		$data['gudang'] = $gudang;
		$data['id_unit_jahit'] = $id_unit_jahit;
		$data['id_unit_packing'] = $id_unit_packing;
		if ($jenis_keluar == "all")
			$data['jenis_keluar'] = '';
		else
			$data['jenis_keluar'] = $jenis_keluar;
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
		$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$this->load->view('template',$data);
	}
}
