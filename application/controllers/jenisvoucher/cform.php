<?php

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
		$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
		$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
		$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
		$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('jenisvoucher/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'jenisvoucher/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	= 'jenisvoucher/vmainform';
		$this->load->view('template',$data);
		
	}
	function tambah() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
		$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
		$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
		$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
		$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('jenisvoucher/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'jenisvoucher/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	= 'jenisvoucher/vmainformadd';
		$this->load->view('template',$data);
		
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
		$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
		$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
		$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
		$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('jenisvoucher/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'jenisvoucher/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'jenisvoucher/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']	= 'jenisvoucher/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ejenisvoucher	= $this->input->post('ejenisvoucher');
		
		$this->load->model('jenisvoucher/mclass');

		if(!empty($ejenisvoucher)) {
			
			$this->mclass->msimpan($ejenisvoucher);
			
		} else {
			$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
			$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
			$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
			$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
			$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('jenisvoucher/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'jenisvoucher/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();

			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
			
			$data['isi']	= 'jenisvoucher/vmainform';
		$this->load->view('template',$data);
	}
}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		
		$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
		$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
		$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
		$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
		$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
		$limages			= base_url();
		$data['list']		= "";
		
		$this->load->model('jenisvoucher/mclass');
		
		$qjnsvoucher		= $this->mclass->medit($id);
		
		if( $qjnsvoucher->num_rows() > 0 ) {
			$row_jnsvoucher		= $qjnsvoucher->row();
			$data['ijenis']		= $row_jnsvoucher->i_jenis;
			$data['ejenisvoucher']	= $row_jnsvoucher->e_jenis_voucher;
		} else {
			$data['ejenisvoucher']	= "";		
		}
		
		$data['isi']	= 'jenisvoucher/veditform';
		$this->load->view('template',$data);
	
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ijenis = $this->input->post('ijenis');
		$ejenisvoucher = $this->input->post('ejenisvoucher');
		
		if(!empty($ejenisvoucher)) {
			
			$this->load->model('jenisvoucher/mclass');
			
			$this->mclass->mupdate($ijenis,$ejenisvoucher);
			
		} else {
			
			$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
			$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
			$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
			$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
			$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('jenisvoucher/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'jenisvoucher/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();

			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
			
			$data['isi']	= 'jenisvoucher/vmainform';
		$this->load->view('template',$data);
		}
	}

	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('jenisvoucher/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txtjenisvoucher	= $this->input->post('txtjenisvoucher');

		$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
		$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
		$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
		$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
		$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('jenisvoucher/mclass');
		$query	= $this->mclass->viewcari($txtjenisvoucher);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'jenisvoucher/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txtjenisvoucher,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('jenisvoucher/vcariform',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txtjenisvoucher	= $this->input->post('txtjenisvoucher')?$this->input->post('txtjenisvoucher'):$this->input->get_post('txtjenisvoucher');

		$data['page_title_jnsvoucher']		= $this->lang->line('page_title_jnsvoucher');
		$data['form_panel_daftar_jnsvoucher']	= $this->lang->line('form_panel_daftar_jnsvoucher');
		$data['form_panel_form_jnsvoucher']	= $this->lang->line('form_panel_form_jnsvoucher');
		$data['form_panel_cari_jnsvoucher']	= $this->lang->line('form_panel_cari_jnsvoucher');
		$data['form_kode_jnsvoucher']		= $this->lang->line('form_kode_jnsvoucher');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('jenisvoucher/mclass');
		$query	= $this->mclass->viewcari($txtjenisvoucher);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'jenisvoucher/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txtjenisvoucher,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('jenisvoucher/vcariform',$data);
	}	
}
?>
