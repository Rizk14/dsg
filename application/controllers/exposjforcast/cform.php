<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('exposjforcast/mclass');
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['form_title_detail_expo_sjdforcast']	= $this->lang->line('form_title_detail_expo_sjdforcast');
			$data['page_title_expo_sjdforcast']		= $this->lang->line('page_title_expo_sjdforcast');
			$data['list_no_expo_sjdforcast']	= $this->lang->line('list_no_expo_sjdforcast');
			$data['list_no_forcast_expo_sjdforcast']	= $this->lang->line('list_no_forcast_expo_sjdforcast');
			$data['list_drop_forcast_expo_sjdforcast']	= $this->lang->line('list_drop_forcast_expo_sjdforcast');
			$data['list_tgl_forcast_expo_sjdforcast']	= $this->lang->line('list_tgl_forcast_expo_sjdforcast');
			$data['list_tgl_dforcast_expo_sjdforcast']	= $this->lang->line('list_tgl_dforcast_expo_sjdforcast');
			$data['list_kd_brg_expo_sjdforcast']	= $this->lang->line('list_kd_brg_expo_sjdforcast');
			$data['list_nm_brg_expo_sjdforcast']	= $this->lang->line('list_nm_brg_expo_sjdforcast');
			$data['list_jml_forcast_expo_sjdforcast']	= $this->lang->line('list_jml_forcast_expo_sjdforcast');
			$data['list_no_sj_expo_sjdforcast']	= $this->lang->line('list_no_sj_expo_sjdforcast');
			$data['list_tgl_sj_expo_sjdforcast']	= $this->lang->line('list_tgl_sj_expo_sjdforcast');
			$data['list_jml_sj_expo_sjdforcast']	= $this->lang->line('list_jml_sj_expo_sjdforcast');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			/*** $this->load->model('exposjforcast/mclass'); ***/
				$data['isi']	= 'exposjforcast/vmainform';	
			$this->load->view('template',$data);	
		
		
	}
	
	function carilistforcast() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['form_title_detail_expo_sjdforcast']	= $this->lang->line('form_title_detail_expo_sjdforcast');
		$data['page_title_expo_sjdforcast']		= $this->lang->line('page_title_expo_sjdforcast');
		$data['list_no_expo_sjdforcast']	= $this->lang->line('list_no_expo_sjdforcast');
		$data['list_no_forcast_expo_sjdforcast']	= $this->lang->line('list_no_forcast_expo_sjdforcast');
		$data['list_drop_forcast_expo_sjdforcast']	= $this->lang->line('list_drop_forcast_expo_sjdforcast');
		$data['list_tgl_forcast_expo_sjdforcast']	= $this->lang->line('list_tgl_forcast_expo_sjdforcast');
		$data['list_tgl_dforcast_expo_sjdforcast']	= $this->lang->line('list_tgl_dforcast_expo_sjdforcast');
		$data['list_kd_brg_expo_sjdforcast']	= $this->lang->line('list_kd_brg_expo_sjdforcast');
		$data['list_nm_brg_expo_sjdforcast']	= $this->lang->line('list_nm_brg_expo_sjdforcast');
		$data['list_jml_forcast_expo_sjdforcast']	= $this->lang->line('list_jml_forcast_expo_sjdforcast');
		$data['list_no_sj_expo_sjdforcast']	= $this->lang->line('list_no_sj_expo_sjdforcast');
		$data['list_tgl_sj_expo_sjdforcast']	= $this->lang->line('list_tgl_sj_expo_sjdforcast');
		$data['list_jml_sj_expo_sjdforcast']	= $this->lang->line('list_jml_sj_expo_sjdforcast');
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$d_forcast_first	= $this->input->post('d_sj_forcast_first');
		$d_forcast_last	= $this->input->post('d_sj_forcast_last');
		
		$data['tglforcastmulai']	= $d_forcast_first;
		$data['tglforcastakhir']	= $d_forcast_last;
		
		$e_d_forcast_first	= explode("/",$d_forcast_first,strlen($d_forcast_first));
		$e_d_forcast_last	= explode("/",$d_forcast_last,strlen($d_forcast_last));
		
		$n_d_forcast_first	= !empty($e_d_forcast_first[2])?$e_d_forcast_first[2].'-'.$e_d_forcast_first[1].'-'.$e_d_forcast_first[0]:'0';
		$n_d_forcast_last	= !empty($e_d_forcast_last[2])?$e_d_forcast_last[2].'-'.$e_d_forcast_last[1].'-'.$e_d_forcast_last[0]:'0';
		
		$data['tforcastfirst']	= $n_d_forcast_first;
		$data['tforcastlast']	= $n_d_forcast_last;
		
		/*** $this->load->model('exposjforcast/mclass'); ***/
		
		if($n_d_forcast_first!=0 && $n_d_forcast_last!=0){
			$data['query']	= $this->mclass->clistforcast2($n_d_forcast_first,$n_d_forcast_last);
			$data['template']= 1;
		}else{
			$data['query']	= '';
			$data['template']= 0;
		}
		$data['isi']	= 'exposjforcast/vlistform';	
			$this->load->view('template',$data);	
		
	}
	
	function gexporforcast() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['form_title_detail_expo_sjdforcast']	= $this->lang->line('form_title_detail_expo_sjdforcast');
		$data['page_title_expo_sjdforcast']		= $this->lang->line('page_title_expo_sjdforcast');
		$data['list_no_expo_sjdforcast']	= $this->lang->line('list_no_expo_sjdforcast');
		$data['list_no_forcast_expo_sjdforcast']	= $this->lang->line('list_no_forcast_expo_sjdforcast');
		$data['list_drop_forcast_expo_sjdforcast']	= $this->lang->line('list_drop_forcast_expo_sjdforcast');
		$data['list_tgl_forcast_expo_sjdforcast']	= $this->lang->line('list_tgl_forcast_expo_sjdforcast');
		$data['list_tgl_dforcast_expo_sjdforcast']	= $this->lang->line('list_tgl_dforcast_expo_sjdforcast');
		$data['list_kd_brg_expo_sjdforcast']	= $this->lang->line('list_kd_brg_expo_sjdforcast');
		$data['list_nm_brg_expo_sjdforcast']	= $this->lang->line('list_nm_brg_expo_sjdforcast');
		$data['list_jml_forcast_expo_sjdforcast']	= $this->lang->line('list_jml_forcast_expo_sjdforcast');
		$data['list_no_sj_expo_sjdforcast']	= $this->lang->line('list_no_sj_expo_sjdforcast');
		$data['list_tgl_sj_expo_sjdforcast']	= $this->lang->line('list_tgl_sj_expo_sjdforcast');
		$data['list_jml_sj_expo_sjdforcast']	= $this->lang->line('list_jml_sj_expo_sjdforcast');
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lfrcst']		= "";
			
		$tforcastfirst	= $this->uri->segment(4);
		$tforcastlast	= $this->uri->segment(5);
		
		$extffirst	= explode("-",$tforcastfirst,strlen($tforcastfirst));
		$extflast	=  explode("-",$tforcastlast,strlen($tforcastlast));
		
		$nwffirst	= (!empty($tforcastfirst) || $tforcastfirst!=0)?$extffirst[2]."/".$extffirst[1]."/".$extffirst[0]:'0';
		$nwflast	= (!empty($tforcastlast) || $tforcastlast!=0)?$extflast[2]."/".$extflast[1]."/".$extflast[0]:'0';
		
		$periode	= $nwffirst." s.d ".$nwflast;
		
		$data['tforcastfirst']	= $tforcastfirst;
		$data['tforcastlast']	= $tforcastlast;
		
		$data['tglforcastmulai']	= $nwffirst==0?'':$nwffirst;
		$data['tglforcastakhir']	= $nwflast==0?'':$nwflast;
		
		/*** $this->load->model('exposjforcast/mclass'); ***/
				
		$data['query']	= $this->mclass->clistforcast2($tforcastfirst,$tforcastlast);
		$data['template'] = 1;
		
		$ObjPHPExcel = new PHPExcel();
		
		$qexpoforcast	= $this->mclass->clistforcast_excell($tforcastfirst,$tforcastlast);
		
		$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
		$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
		$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
		$ObjPHPExcel->getProperties()
			->setTitle("Laporan SJ DROP Forcast")
			->setSubject("Laporan SJ DROP Forcast")
			->setDescription("Laporan SJ DROP Forcast")
			->setKeywords("Laporan Bulanan")
			->setCategory("Laporan");

		$ObjPHPExcel->setActiveSheetIndex(0);
		
		$ObjPHPExcel->createSheet();
			
		$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

		$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30); 
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
		
		$ObjPHPExcel->getActiveSheet()->mergeCells('A2:J2');
		$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN SJ DROP FORCAST');
		$ObjPHPExcel->getActiveSheet()->mergeCells('A3:J3');

		$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
						
		$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
		
		$ObjPHPExcel->getActiveSheet()->mergeCells('A4:J4');

		$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
		$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
		$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
		$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'No. SJ');
		$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl. SJ');
		$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'Kode Barang');
		$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Barang');
		$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'SJ');
		$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Forcast');
		$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Drop Forcast');
		$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Tgl. Forcast');
		$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'Tgl. Drop Forcast');
		$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
																
		if($qexpoforcast->num_rows()>0) {
				
			$j	= 7;
			$nomor	= 1;
			
			foreach($qexpoforcast->result() as $row) {

				$expdsj		= explode("-",$row->d_sj,strlen($row->d_sj)); // YYYY-mm-dd
				$dsj_new	= $expdsj[2].'/'.$expdsj[1].'/'.$expdsj[0];

				$expddropforcast	= explode("-",$row->ddropforcast,strlen($row->ddropforcast)); // YYYY-mm-dd
				$ddropforcast_new	= $expddropforcast[2].'/'.$expddropforcast[1].'/'.$expddropforcast[0];

				$expdforcast	= explode(" ",$row->dforcast,strlen($row->dforcast)); //
				$expdforcast2	= explode("-",$expdforcast[0],strlen($expdforcast[0]));
						
				$dforcast_new	= $expdforcast2[2].'/'.$expdforcast2[1].'/'.$expdforcast2[0];
												 											
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->i_sj_code);
				$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $dsj_new);
				$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->i_product);
				$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->e_product_name);
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->sj);
				$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->i_forcast_code);
				$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->i_drop_forcast);
				$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $dforcast_new);
				$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $ddropforcast_new);
				$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
																									
				$j++;																																					
				$nomor++;
			}
			$jj=$j+1;
 						
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			$files	= $this->session->userdata('gid')."laporan_sj_drop_forcast"."_".$tforcastfirst."-".$tforcastlast.".xls";
			$ObjWriter->save("files/".$files);

			$efilename = substr($files,1,strlen($files));
	
			$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));			
		}
		$data['isi']	= 'exposjforcast/vexpform';	
			$this->load->view('template',$data);	
		
	}
	
	/*
	function listforcast() {
		$data['page_title']	= "DATA FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('exposjforcast/mclass');

		$query	= $this->mclass->lforcast();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/exposjforcast/cform/listforcastnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lforcastperpages($pagination['per_page'],$pagination['cur_page']);
	
		$this->load->view('exposjforcast/vlistformforcast',$data);	
	}

	function listforcastnext() {
		$data['page_title']	= "DATA FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('exposjforcast/mclass');

		$query	= $this->mclass->lforcast();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/exposjforcast/cform/listforcastnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lforcastperpages($pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('exposjforcast/vlistformforcast',$data);	
	}	
	
	function flistforcast() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$data['page_title']	= "DATA FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('exposjforcast/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->flforcast($key);
			$jml	= $query->num_rows();
		} else {
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_forcast_code')\">".$row->i_forcast_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_forcast_code')\">".$row->d_forcast."</a></td>
				 </tr>";				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}

	function listdropforcast() {
		$data['page_title']	= "DATA DROP FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('exposjforcast/mclass');

		$query	= $this->mclass->ldropforcast();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/exposjforcast/cform/listdropforcastnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ldropforcastperpages($pagination['per_page'],$pagination['cur_page']);
	
		$this->load->view('exposjforcast/vlistformdropforcast',$data);	
	}

	function listdropforcastnext() {
		$data['page_title']	= "DATA DROP FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('exposjforcast/mclass');

		$query	= $this->mclass->ldropforcast();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/exposjforcast/cform/listdropforcastnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ldropforcastperpages($pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('exposjforcast/vlistformdropforcast',$data);	
	}
		
	function flistdropforcast() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$data['page_title']	= "DATA DROP FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('exposjforcast/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->fldropforcast($key);
			$jml	= $query->num_rows();
		} else {
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_forcast_code')\">".$row->i_forcast_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_forcast_code')\">".$row->d_forcast."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}
	*/	
}
?>
