<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bankmultiin');
			$data['iarea']='';
			$data['iperiode']='';
			$data['ikbank']='';
			$data['periode']='';
			$query=$this->db->query("	select i_periode from tm_periode ",false);
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $rw){
          $data['periode']=$rw->i_periode;
		    }
	    }
			$area1 = $this->session->userdata('i_area');
			$query = $this->db->query("select e_area_name from tr_area where i_area = '$area1'",false);
			foreach($query->result() as $tmp){
				$nama=$tmp->e_area_name;
			}
    	$data['lepel']= $area1;
    	$data['area1']= $area1;
    	$data['nama'] = $nama;      
     	$this->load->view('akt-bankin-multi/vmainform', $data);
    }else{
			$this->load->view('awal/index.php');
    }
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bankmultiin');
			$this->load->view('akt-bankin-multi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bankmultiin')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$ikk 		= $this->uri->segment(4);
				$iperiode	= $this->uri->segment(5);
				$iarea		= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto		= $this->uri->segment(8);
				$lepel		= $this->uri->segment(9);
				$this->load->model("akt-bankin-multi/mmaster");
				$data['isi']=$this->mmaster->baca($ikk,$iperiode,$iarea);
				$data['iarea']  = $iarea;
				$data['dfrom']  = $dfrom;
				$data['dto']    = $dto;
				$data['lepel']	= $lepel;
 		 		$this->load->view('akt-bankin-multi/vformupdate',$data);
			}else{
				$this->load->view('akt-bankin-multi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikk		= $this->input->post('ikk', TRUE);
			$iarea		= $this->input->post('iarea', TRUE);
			$iperiode	= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$tah		= substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul		= $this->input->post('iperiodebl', TRUE);
			$ikendaraan	= $this->input->post('ikendaraan', TRUE);
			$vkk		= $this->input->post('vkk', TRUE);
			$vkk		= str_replace(',','',$vkk);
			$dbank		= $this->input->post('dbank', TRUE);
			$dbukti		= $this->input->post('dbukti', TRUE);
			$etempat	= $this->input->post('etempat', TRUE);
			$epengguna	= $this->input->post('epengguna', TRUE);
			$icoa		= $this->input->post('icoa', TRUE);
			$ecoaname	= $this->input->post('ecoaname', TRUE);
			$enamatoko	= $this->input->post('enamatoko', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			$ibukti		= $this->input->post('ibukti', TRUE);
			if($edescription=="") $edescription=null;
			$ejamin		= $this->input->post('ejamin', TRUE);
			if($ejamin=="") $ejamin=null;
			$ejamout	= $this->input->post('ejamout', TRUE);
			if($ejamout=="") $ejamout=null;
			$nkm		= $this->input->post('nkm', TRUE);
			$nkm		= str_replace(',','',$nkm);
			if($nkm=="") $nkm=null;
			if($dbank!=''){
				$tmp=explode("-",$dbank);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbank=$th."-".$bl."-".$hr;
			}
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$fdebet='f';
			if (
				(isset($ikk) && $ikk != '') &&
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($vkk) && (($vkk != 0) || ($vkk != ''))) &&
				(isset($dbank) && $dbank != '') &&
				(isset($icoa) && $icoa != '')
			   )
			{
				$this->load->model('akt-bankin-multi/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dbank,$ecoaname,
						                   $edescription,$ejamin,$ejamout,$nkm,$etempat,$fdebet,$dbukti,
						                   $enamatoko,$epengguna,$ibukti);
				$nomor=$ikk;
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update Kas kecil No:'.$ikk.' Periode:'.$iperiode.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-bankin-multi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-bankin-multi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area	= $this->session->userdata('i_area');
      $baris=$this->uri->segment(4);			
      $config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/coa/'.$baris.'/';
      
      /*
			if($area!='00'){
			  $query = $this->db->query("select * from tr_coa where (not (i_coa like '110-2%') and not (i_coa like '110-4%'))
			                             and i_coa<>i_coa_ledger",false);
      }else{
			  $query = $this->db->query("select * from tr_coa where (not (i_coa like '110-2%') and not (i_coa like '110-4%'))
			                             and i_coa<>i_coa_ledger",false);
			}
			*/
			  if($area!='00'){
			 $query = $this->db->query("select * from tr_coa where (i_coa like '60%' or i_coa like '61%' or i_coa like '8%' or i_coa like '9%' 
			 or i_coa like '620-2200' or i_coa like '1%' or i_coa like '2%'
			  or i_coa like '3%' or i_coa like '4%'  or i_coa like '5%' or i_coa like '9%' )order by i_coa",false);
      }else{
			  $query = $this->db->query("select * from tr_coa where (i_coa like '60%' or i_coa like '61%' or i_coa like '8%' or i_coa like '9%'
			  or i_coa like '620-2200' or i_coa like '1%' or i_coa like '2%'
			  or i_coa like '3%' or i_coa like '4%'  or i_coa like '5%' or i_coa like '9%' )order by i_coa",false);
		  } 	  
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
      $data['baris']=$this->uri->segment(4);
			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('akt-bankin-multi/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){           
      $area	= $this->session->userdata('i_area');
      $baris 	= $this->input->post('xbaris', FALSE);
      $config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/coa/index/'.$baris.'';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			/*
			if($area!='00'){
			  $query 	= $this->db->query("select * from tr_coa
						      									where (upper(i_coa) like '%$cari%' or upper(e_coa_name) like '%$cari%')
																	  and ( not i_coa like '110-2%' and not i_coa like '110-4%') 
																	  and i_coa<>i_coa_ledger",false);
      }else{
			  $query 	= $this->db->query("select * from tr_coa
						      									where (upper(i_coa) like '%$cari%' or upper(e_coa_name) like '%$cari%')
																	  and (not i_coa like '110-2%' and not i_coa like '110-4%') 
																	  and i_coa<>i_coa_ledger",false);
      }
      */
      if($area!='00'){
				$query 	= $this->db->query("select  * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%'))
											and i_coa in (select i_coa from tr_coa where i_coa like '60%' or i_coa like '61%' 
											or i_coa like '8%' or i_coa like '9%' or i_coa like '620-2200' or i_coa like '1%' or i_coa like '2%'
											or i_coa like '3%' or i_coa like '4%'  or i_coa like '5%' or i_coa like '9%' )order by i_coa",false);
      }else {
		  $query 	= $this->db->query("select  * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%'))
											and i_coa in (select i_coa from tr_coa where i_coa like '60%' or i_coa like '61%' 
											or i_coa like '8%' or i_coa like '9%' or i_coa like '620-2200' or i_coa like '1%' or i_coa like '2%'
											or i_coa like '3%' or i_coa like '4%'  or i_coa like '5%' or i_coa like '9%' )order by i_coa",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
  
			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(6),$area);
 $data['baris']=$baris;
			$this->load->view('akt-bankin-multi/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){		
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$area	=$this->input->post('area', TRUE);
      $baris 	= $this->input->post('xbaris', FALSE);
			$periode=$this->input->post('periode', TRUE);
      if($area=='') $area	= $this->uri->segment(4);
			if($periode=='') $periode = $this->uri->segment(5);
      if($baris=='') $baris = $this->uri->segment(6);
      if($cari=='') $cari = $this->uri->segment(7);
      if($cari=='') $cari='sikasep';
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/kendaraan/'.$area.'/'.$periode.'/'.$baris.'/'.$cari.'/';
      if($cari=='sikasep'){
			  $query = $this->db->query("	select i_kendaraan from tr_kendaraan a
							                      inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							                      inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							                      where a.i_area='$area' and a.i_periode='$periode'",false);
      }else{
			  $query = $this->db->query("	select i_kendaraan from tr_kendaraan a
							                      inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							                      inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							                      where a.i_area='$area' and a.i_periode='$periode'
                                    and (upper(i_kendaraan) like '%$cari%' or upper(e_pengguna) like '%$cari%') ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
      $data['baris']=$this->uri->segment(6);
			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['area']	=$area;
			$data['periode']=$periode;
			$data['isi']=$this->mmaster->bacakendaraan($cari,$area,$periode,$config['per_page'],$this->uri->segment(8));
			$this->load->view('akt-bankin-multi/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	=$this->input->post('area', TRUE);
            $baris 	= $this->input->post('xbaris', FALSE);
			$periode=$this->input->post('periode', TRUE);
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/kendaraan/'.$area.'/'.$periode.'/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("	select * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
							and a.i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
        
			$this->pagination->initialize($config);
			$this->load->model('akt-bankin-multi/mmaster');
			$data['area']=$area;
            $data['baris']=$baris;
			$data['periode']=$periode;
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['isi']=$this->mmaster->carikendaraan($area,$periode,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bankin-multi/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea		= $this->input->post('iarea', TRUE);
			$irvtype  = $this->input->post('irvtype', TRUE);
			$iperiode	= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$tah		  = substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul		  = $this->input->post('iperiodebl', TRUE);
			$dbank	  = $this->input->post('dbank', TRUE);
			$ibank  	= $this->input->post('ibank', TRUE);
			$icoabank	= $this->input->post('icoabank', TRUE);
      $jml      = $this->input->post('jml', TRUE);
			if($dbank!=''){
				$tmp=explode("-",$dbank);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbank=$th."-".$bl."-".$hr;
				$drv=$th."-".$bl."-".$hr;
			}
			$fdebet='f';
      $nomor=array();
      $adaket=false;
      for ($a=1;$a<=$jml;$a++) {
        $ket=$this->input->post('edescription'.$a,TRUE);
        if($ket==''){
          $adaket=false;
          break;
        }else{
          $adaket=true;
        }
      }
			if (
          (isset($iperiode) && $iperiode != '') && (isset($ibank) && $ibank != '') && 
          (isset($iarea) && $iarea != '') && (isset($icoabank) && $icoabank != '') && 
          (isset($dbank) && $dbank != '') && (isset($jml) && $jml != '' && $jml != '0') && ($adaket)
			   )
			{
				$this->load->model('akt-bankin-multi/mmaster');
				$this->db->trans_begin();
				$tot=0;
				$irvb=$this->mmaster->runningnumberrvb($tah,$bul,$icoabank,$iarea);
        $irv=$this->mmaster->runningnumberrv($tah,$bul,$iarea,$irvtype);
        for ($a=1;$a<=$jml;$a++) {
          $icoa         = $this->input->post('icoa'.$a, TRUE);
          $ecoaname     = $this->input->post('ecoaname'.$a, TRUE);
          $iareax       = $this->input->post('iarea'.$a, TRUE);
          $dbukti       = $this->input->post('tgl'.$a, TRUE);
			    if($dbukti!=''){
				    $tmp=explode("-",$dbukti);
				    $xth=$tmp[2];
				    $xbl=$tmp[1];
				    $xhr=$tmp[0];
				    $dbukti=$xth."-".$xbl."-".$xhr;
			    }
          $eremark      = null;
          $vbank        = $this->input->post('vbank'.$a, TRUE);
          $vbank	      = str_replace(',','',$vbank);
          $tot=$tot+$vbank;
          $edescription = $this->input->post('edescription'.$a, TRUE);
          if($edescription=="") $edescription=null;
          $ikode=$this->mmaster->runningnumberbank($tah,$bul,$iareax,$icoabank);
          $this->mmaster->insert( $iareax,$ikode,$iperiode,$icoa,$vbank,$dbukti,$ecoaname,$edescription,$fdebet,$icoabank);
          $nomor[]=$ikode;
###########posting##########
          $eremark		= $edescription;
          $fclose			= 'f';
			    $this->mmaster->inserttransheader($ikode,$iareax,$eremark,$fclose,$dbank,$icoabank);
			    if($fdebet=='t'){
				    $accdebet		  = $icoa;
				    $namadebet		= $ecoaname;
				    $acckredit		= $icoabank;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
			    }else{
				    $accdebet		  = $icoabank;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
			    }
			    $this->mmaster->inserttransitemdebet($accdebet,$ikode,$namadebet,'t','t',$iareax,$eremark,$vbank,$dbank,$icoabank);
			    $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vbank);
			    $this->mmaster->inserttransitemkredit($acckredit,$ikode,$namakredit,'f','t',$iareax,$eremark,$vbank,$dbank,$icoabank);
			    $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vbank);
			    $this->mmaster->insertgldebet($accdebet,$ikode,$namadebet,'t',$iareax,$vbank,$dbank,$eremark,$icoabank);
			    $this->mmaster->insertglkredit($acckredit,$ikode,$namakredit,'f',$iareax,$vbank,$dbank,$eremark,$icoabank);
###########end of posting##########
          $this->mmaster->insertrvitem( $irv,$iarea,$icoa,$ecoaname,$vbank,$edescription,$ikode,$irvtype,$iareax,$icoabank);
        }
#				$icoa='111.2'.$iarea;
				$this->mmaster->insertrv( $irv,$iarea,$iperiode,$icoabank,$drv,$tot,$eremark,$irvtype);
				$this->mmaster->insertrvb( $irvb,$icoabank,$irv,$iarea,$irvtype);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Input Bank Masuk No:'.$ikode.' Periode:'.$iperiode.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );
#          $this->db->trans_rollback();					
					$this->db->trans_commit();

					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomorarray',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function start()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('iarea', TRUE);
      if($area=='') $area=$this->uri->segment(4);
			$periode= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
      if($periode=='') $periode=$this->uri->segment(5);
			$tanggal= $this->input->post('dbank', TRUE);
      if($tanggal=='') $tanggal=$this->uri->segment(6);
			if($tanggal!=''){
				$tmp=explode("-",$tanggal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$tgl=$th."-".$bl."-".$hr;
			}
			$ebankname= $this->input->post('ebankname', TRUE);
      if($ebankname=='') $ebankname=$this->uri->segment(7);
      $ebankname=str_replace('%20',' ',$ebankname);
			$ibank= $this->input->post('ibank', TRUE);
      if($ibank=='') $ibank=$this->uri->segment(8);
			$icoabank= $this->input->post('icoabank', TRUE);
      if($icoabank=='') $icoabank=$this->uri->segment(9);
			$this->load->model('akt-bankin-multi/mmaster');

			$tmp = explode("-", $tgl);
			$det	= $tmp[2];
			$mon	= $tmp[1];
			$yir 	= $tmp[0];
			$dsaldo	= $yir."/".$mon."/".$det;
			$dtos	= $this->mmaster->dateAdd("d",-1,$dsaldo);
			$tmp 	= explode("-", $dtos);
			$det1	= $tmp[2];
			$mon1	= $tmp[1];
			$yir1 	= $tmp[0];
			$dtos	= $yir1."-".$mon1."-".$det1;
 			$query=$this->db->query("	select i_periode from tm_periode ",false);
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $rw){
          $data['periode']=$rw->i_periode;
		    }
	    }
	    $sal= $this->mmaster->bacasaldo($area,$dtos,$icoabank);
			$data['saldo']	  = $sal;
			$data['page_title'] = $this->lang->line('bankmultiin');
			$data['iarea']	  = $area;
			$data['eareaname']= $this->mmaster->area($area);
			$data['iperiode'] = $periode;
			$data['tanggal']  = $tanggal;
			$data['ibank']=$ibank;
			$data['ikbank']='';
			$data['icoabank']=$icoabank;
  		$data['ebankname']=$ebankname;
			$data['ikk']	  = '';
			$data['lepel']	  = $this->session->userdata('i_area');
			$this->load->view('akt-bankin-multi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	
	
	function kkgroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
            $cari=strtoupper($this->input->post("cari"));
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/kkgroup/';
			$query = $this->db->query("	select * from tr_kk_group where upper(i_kk_group) like '%$cari%' or upper(e_kk_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('listkkgroup');
			$data['isi']=$this->mmaster->bacakkgroup($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('akt-bankin-multi/vlistkkgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
    	($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icoabank = $this->uri->segment(6);
			$iarea	  = $this->uri->segment(5);
			$irv	    = $this->uri->segment(4);
			$this->load->model('akt-bankin-multi/mmaster');
      $data['icoabank']= $icoabank;
      $data['iarea']= $iarea;
			$data['irv']  = $irv;
			$data['page_title'] = $this->lang->line('printrv');
			$data['isi']=$this->mmaster->bacarv($irv,$iarea,$icoabank);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$sql	= "select e_area_name from tr_area where i_area='$iarea'";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
			     $data['eareaname']=$tes->e_area_name;
			  }
			}
			$pesan='Cetak RV Area:'.$iarea.' No:'.$irv;
			$this->load->model('logger');
#			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('akt-bankin-multi/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function rv()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){		
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$area	=$this->input->post('area', TRUE);
			$periode=$this->input->post('periode', TRUE);
			$icoabank=$this->input->post('icoabank', TRUE);
      if($area=='') $area	= $this->uri->segment(4);
			if($periode=='') $periode = $this->uri->segment(5);
			if($icoabank=='') $icoabank = $this->uri->segment(6);
      if($cari=='') $cari = $this->uri->segment(7);
      if($cari=='') $cari='sikasep';
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/rv/'.$area.'/'.$periode.'/'.$icoabank.'/'.$cari.'/';
      if($cari=='sikasep'){
			  $query = $this->db->query("	select b.i_rvb as i_rv, a.v_rv from tm_rv a, tm_rvb b
							                      where a.i_area='$area' and a.i_periode='$periode' and a.i_rv_type='02'
							                      and a.i_area=b.i_area and a.i_rv=b.i_rv and a.i_rv_type=b.i_rv_type and b.i_coa_bank='$icoabank'",false);
      }else{
			  $query = $this->db->query("	select b.i_rvb as i_rv, a.v_rv from tm_rv a, tm_rvb b
							                      where a.i_area='$area' and a.i_periode='$periode' and a.i_rv_type='02'
							                      and a.i_area=b.i_area and a.i_rv=b.i_rv and a.i_rv_type=b.i_rv_type and b.i_coa_bank='$icoabank'
                                    and (upper(b.i_rvb) like '%$cari%') ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
      $data['baris']=$this->uri->segment(8);
			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_rv');
			$data['area']	=$area;
			$data['periode']=$periode;
			$data['icoabank']=$icoabank;
			$data['isi']=$this->mmaster->bacarvprint($icoabank,$cari,$area,$periode,$config['per_page'],$this->uri->segment(8));
			$this->load->view('akt-bankin-multi/vlistrv', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carirv()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	=$this->input->post('area', TRUE);
            $baris 	= $this->input->post('xbaris', FALSE);
			$periode=$this->input->post('periode', TRUE);
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/kendaraan/'.$area.'/'.$periode.'/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("	select * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)

							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
							and a.i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
        
			$this->pagination->initialize($config);
			$this->load->model('akt-bankin-multi/mmaster');
			$data['area']=$area;
            $data['baris']=$baris;
			$data['periode']=$periode;
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['isi']=$this->mmaster->carikendaraan($area,$periode,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bankin-multi/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/bank/index/';
 			$query = $this->db->query("select * from tr_bank",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bankin-multi/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		  $baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/area2/'.$baris.'/sikasep/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$area1,$area2,$area3,$area4,$area5);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->view('akt-bankin-multi/vlistarea2', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/cariarea2/'.$baris.'/'.$cari.'/';
  	  else
  			$config['base_url'] = base_url().'index.php/akt-bankin-multi/cform/cariarea2/'.$baris.'/sikasep/';
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
      $data['baris']=$baris;
			$data['cari']=$cari;
			$this->load->model('akt-bankin-multi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-bankin-multi/vlistarea2', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
