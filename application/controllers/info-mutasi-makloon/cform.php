<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-mutasi-makloon/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['isi'] = 'info-mutasi-makloon/vmainform';
	$data['jenis_makloon'] = $this->mmaster->get_jenis_makloon();
	$this->load->view('template',$data);

  }
  
  function view(){
    $data['isi'] = 'info-mutasi-makloon/vformview';
    $kode_brg = $this->input->post('kode_brg_makloon', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	if ($date_from == '' && $date_to == '' && $kode_brg == '') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
		$kode_brg 	= $this->uri->segment(7);
	}
	//echo $kode_brg; die();

    $jum_total = $this->mmaster->get_mutasi_stoktanpalimit($date_from, $date_to, $kode_brg);
			$config['base_url'] = base_url().'index.php/info-mutasi-makloon/cform/view/index/'.$date_from.'/'.$date_to.'/'.$kode_brg;
			//$config['total_rows'] = $query->num_rows(); 
			$config['total_rows'] = count($jum_total); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_mutasi_stok($config['per_page'],$this->uri->segment(8), $date_from, $date_to, $kode_brg);
	$data['jum_total'] = count($jum_total);

	$data['kode_brg'] = $kode_brg;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$this->load->view('template',$data);
  }
  
  // brg makloon
  function show_popup_brg_makloon(){
	// =======================
	// disini coding utk pengecekan user login
	//========================

	$jenis_makloon	= $this->uri->segment(4);

	if ($jenis_makloon == '') {
		$jenis_makloon 	= $this->input->post('jenis_makloon', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $jenis_makloon == '' ) {
			$jenis_makloon 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";

		$qjum_total = $this->mmaster->get_bahan_makloontanpalimit($keywordcari, $jenis_makloon);
		
		$config['base_url'] = base_url()."index.php/sj-keluar-makloon/cform/show_popup_brg_makloon/".$jenis_makloon."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan_makloon($config['per_page'],$config['cur_page'], $keywordcari, $jenis_makloon);

	$data['jum_total'] = count($qjum_total);
	$data['jenis_makloon'] = $jenis_makloon;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_jenis_makloon WHERE id = '$jenis_makloon' ");
	$hasilrow = $query3->row();
	$nama_jenis	= $hasilrow->nama;
	$data['nama_jenis'] = $nama_jenis;

	$this->load->view('info-mutasi-makloon/vpopupbrgmakloon',$data);
	
  }
  
}
