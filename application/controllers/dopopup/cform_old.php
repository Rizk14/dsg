<?php

class Cform extends Controller {

	function __construct() { 
		parent::Controller();
	}
	
	function index() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0) 
		{
			$data['form_nomor_do']	= $this->lang->line('form_nomor_do');
			$data['form_option_pel_do']	= $this->lang->line('form_option_pel_do');			
			$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');		
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['button_batal']	= $this->lang->line('button_batal');		
			$data['page_title_do']	= $this->lang->line('page_title_do');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
			
			$this->load->model('dopopup/mclass');
			
			$data['opt_cabang']	= $this->mclass->lcabang();
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			//if ($this->session->userdata('user_idx') != '1')
			//	print "<script>alert(\"Maaf, Lagi ada perbaikan dulu. Terimakasih.\");show(\"inisial/cform\",\"#content\");</script>";
			$this->load->view('dopopup/vmainform',$data);
		}
	}
	
	function listdetailop() {
		
		$ibranch	= $this->uri->segment(4);
		
		$data['ibranch']	= $ibranch;
		$data['page_title']	= "NOMOR OP";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('dopopup/mclass');

		$data['isi']	= $this->mclass->lopdetail($ibranch);
				
		$this->load->view('dopopup/vlistop',$data);			
	}
	
	function flistdetailop() {
		
	}
	
	function detailsimpan() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0) 
		{
			$data['form_nomor_do']	= $this->lang->line('form_nomor_do');
			$data['form_tgl_do']	= $this->lang->line('form_tgl_do');
			$data['form_cabang_do']	= $this->lang->line('form_cabang_do');
			$data['form_option_pel_do']	= $this->lang->line('form_option_pel_do');
			$data['form_option_cab_do']	= $this->lang->line('form_option_cab_do');
			$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
			$data['form_nomor_op_do']	= $this->lang->line('form_nomor_op_do');
			$data['form_kode_produk_do']	= $this->lang->line('form_kode_produk_do');
			$data['form_nm_produk_do']	= $this->lang->line('form_nm_produk_do');
			$data['form_jml_product_do']	= $this->lang->line('form_jml_product_do');
			$data['form_harga_product_do']	= $this->lang->line('form_harga_product_do');
			$data['form_ket_do']		= $this->lang->line('form_ket_do');
			
			$data['button_simpan']	= $this->lang->line('button_simpan');	
			$data['button_batal']	= $this->lang->line('button_batal');
			
			$data['page_title_do']	= $this->lang->line('page_title_do');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
			$data['lcabang']	= "";
		
			$tahun	= date("Y");
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
		
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgDO']	= $tgl."/".$bln."/".$thn;

			$List = "";
			
			$iopcode	= $this->input->post('i_op_code');
			$iop		= $this->input->post('i_op');
			$iproduct	= $this->input->post('i_product');
			$ibranch	= $this->input->post('i_branch');
			$icustomer	= $this->input->post('i_customer');
			$exp_iop	= explode("#",$iop,strlen($iop));
			$exp_iproduct	= explode("#",$iproduct,strlen($iproduct));
			$data['ibranch']	= $ibranch;
			$data['icustomer']	= $icustomer;
			
			$this->load->model('dopopup/mclass');

			$iter = 0;	
			
			foreach($exp_iop as $iop) {
				$query	= $this->mclass->detailsimpan($iop,$ibranch,$exp_iproduct[$iter]);
				if($query->num_rows()>0) {

					$row	= $query->row();

					$qhargaperpelanggan = $this->mclass->hargaperpelanggan($row->iproduct,$icustomer);
					$qhargadefault 	    = $this->mclass->hargadefault($row->iproduct);
						
					if($qhargaperpelanggan->num_rows()>0){
						$rhargaperpelanggan = $qhargaperpelanggan->row();
							
						$hargaperunit = $rhargaperpelanggan->v_price;
						$harga = $row->qtyakhir*$hargaperunit;
							
					}elseif($qhargadefault->num_rows()>0){
						$rhargadefault = $qhargadefault->row();
							
						$hargaperunit = $rhargadefault->v_price;
						$harga = $row->qtyakhir*$hargaperunit;
							
					}else{
						$hargaperunit = $row->unitprice;
						$harga = $row->qtyakhir*$hargaperunit;
					}
					
					$List .= "
						<tr>
							<td><div style=\"font:11px/24px;text-align:right;width:18px;margin-right:0px;\">".($iter+1)."</div></td>
							<td><DIV ID=\"ajax_i_op_tblItem_".$iter."\" >
							<input type=\"text\" ID=\"i_op_tblItem_".$iter."\" name=\"i_op_tblItem_".$iter."\" style=\"width:60px;\" value=\"".$row->iopcode."\" readonly >
							<input type=\"hidden\" name=\"qty_product_tblItem_".$iter."\" id=\"qty_product_tblItem_".$iter."\" value=\"".$row->qtyproduk."\" >
							<input type=\"hidden\" name=\"qty_op_tblItem_".$iter."\" id=\"qty_op_tblItem_".$iter."\" value=\"".$row->qtyakhir."\" ></DIV>
							</td>
							<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" ><input type=\"text\" ID=\"i_product_tblItem_".$iter."\" name=\"i_product_tblItem_".$iter."\" style=\"width:100px;\" value=\"".$row->iproduct."\" readonly ></DIV></td>
							<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" ><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\"  name=\"e_product_name_tblItem_".$iter."\" style=\"width:280px;\" value=\"".$row->productname."\" readonly ></DIV></td>
							<td><DIV ID=\"ajax_n_deliver_tblItem_".$iter."\" ><input type=\"text\" ID=\"n_deliver_tblItem_".$iter."\"  name=\"n_deliver_tblItem_".$iter."\" style=\"width:50px;text-align:right;\" value=\"".$row->qtyakhir."\" onkeyup=\"validNum('n_deliver_tblItem','".$iter."');validStok(".$iter.");validStok2(".$iter.");totalharga(this.value,".$iter.");\" ></DIV></td>
							
							<input type=\"hidden\" ID=\"price_tblItem_".$iter."\" name=\"price_tblItem_".$iter."\" style=\"width:50px;text-align:right;\" value=\"".$hargaperunit."\" >
							
								<input type='hidden' name='is_grosir_".$iter."' id='is_grosir_".$iter."' value='' >
								<input type=\"hidden\" ID=\"grosir_tblItem_".$iter."\" name=\"grosir_tblItem_".$iter."\" style=\"width:50px;text-align:right;\" value=\"".$row->harga_grosir."\" readonly='true' >
							
							
							<input type=\"hidden\" ID=\"v_do_gross_tblItem_".$iter."\"  name=\"v_do_gross_tblItem_".$iter."\" style=\"width:115px;text-align:right;\" value=\"".$harga."\" >
							
							<td><DIV ID=\"adaboneka_tblItem_".$iter."\" >
							<select id=\"adaboneka_tblItem_".$iter."\" name=\"adaboneka_tblItem_".$iter."\">
								<option value='0'>Tidak</option>
								<option value='1'>Ya</option>
							</select></DIV>
							</td>
							
							<td><DIV ID=\"ajax_e_note_tblItem_".$iter."\" >
							<input type=\"text\" ID=\"e_note_tblItem_".$iter."\" name=\"e_note_tblItem_".$iter."\" style=\"width:130px;\" >
							
							<input type=\"hidden\" ID=\"i_op_sebunyi_tblItem_".$iter."\" name=\"i_op_sebunyi_tblItem_".$iter."\" value=\"".$row->iop."\" >
							<input type=\"hidden\" ID=\"f_stp_tblItem_".$iter."\" name=\"f_stp_tblItem_".$iter."\" value=\"".$row->stp."\" >
							<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$iter."\"></DIV></td>
						</tr>
					";
				}
				$iter++;
			}
					
			$qryth	= $this->mclass->getthndo();
			$qrydo	= $this->mclass->getnomordo();
			
			$data['opt_cabang']	= $this->mclass->lcabang();	
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
			
			if($qryth->num_rows() > 0) {
				$th		= $qryth->row_array();
				$thn	= $th['thn'];
			}else{
				$thn	= $tahun;
			}
		
			if($thn==$tahun) {
				
				if($qrydo->num_rows() > 0)  {
					
					$row	= $qrydo->row_array();
					$do		= $row['idocode']+1;
							
					switch(strlen($do)) {
						case "1": $nomordo	= "0000".$do;
						break;
						case "2": $nomordo	= "000".$do;
						break;	
						case "3": $nomordo	= "00".$do;
						break;
						case "4": $nomordo	= "0".$do;
						break;
						case "5": $nomordo	= $do;
						break;	
					}
				}else{
					$nomordo		= "00001";
				}
				$nomor	= $tahun.$nomordo;
			}else{
				$nomor	= $tahun."00001";
			}
			
			$data['no']	= $nomor;
			
			$jumnya = $iter-1;
			$data['jumdata'] = $iter;
			if($jumnya<22) {
				$data['List']	= $List;
			}else{
				$data['List']	= "item Order tdk dpt lebih dari 22 item barang!";
			}
			
			$this->load->view('dopopup/vform',$data);
		}		
	}
	
	function listbarangjadi() {
		
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$icust	= $this->uri->segment(6);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['icust']	= $icust;
		
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('dopopup/mclass');

		$query	= $this->mclass->lop($ibranch,$icust);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/dopopup/cform/listbarangjadinext/'.$iterasi.'/'.$ibranch.'/'.$icust.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lopperpages($ibranch,$icust,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('dopopup/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$icust	= $this->uri->segment(6);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['icust']	= $icust;
		
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('dopopup/mclass');

		$query	= $this->mclass->lop($ibranch,$icust);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/dopopup/cform/listbarangjadinext/'.$iterasi.'/'.$ibranch.'/'.$icust.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lopperpages($ibranch,$icust,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('dopopup/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
		
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$icust	= $this->input->post('icust');

		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('dopopup/mclass');

		$query	= $this->mclass->flop($key,$icust);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){

				$qhargaperpelanggan = $this->mclass->hargaperpelanggan($row->iproduct,$icust);
				$qhargadefault 	    = $this->mclass->hargadefault($row->iproduct);
						
				if($qhargaperpelanggan->num_rows()>0){
					$rhargaperpelanggan = $qhargaperpelanggan->row();
							
					$hargaperunit = $rhargaperpelanggan->v_price;
					$harga = $row->qtyakhir*$hargaperunit;
				}elseif($qhargadefault->num_rows()>0){
					$rhargadefault = $qhargadefault->row();
							
					$hargaperunit = $rhargadefault->v_price;
					$harga = $row->qtyakhir*$hargaperunit;	
				}else{
					$hargaperunit = $row->unitprice;
					$harga = $row->qtyakhir*$hargaperunit;
				}

				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$hargaperunit','$row->unitprice','$row->iop','$row->stp')\">".$row->iopcode."</a></td>	 
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->iproduct."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->qtyproduk."</a></td>
				  <td><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->productname."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp')\">".$row->qtyakhir."</a></td>
				 </tr>";

				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
		
		echo $item;
	}
	
	function cari_cabang() {
		
		$i_customer	= $this->input->post('ibranch');
		$this->load->model('dopopup/mclass');
		$query	= $this->mclass->getcabang($i_customer);
		
		$c	= "";
		
		if($query->num_rows()>0) {
			
			$cabang	= $query->result();

			$c .= "<select name=\"i_branch\" id=\"i_branch\" >";
			$c .= "<option value=\"\">[Pilih Cabang Pelanggan]</option>";
			foreach ($cabang as $row) {
				$c .= "<option value=".$row->i_branch_code.">".$row->e_branch_name." ( ".$row->e_initial." ) "."</option>";
			}
			$c .= "</option>";
			echo $c;
		}
	}
	
	function simpan() {
		
		$iteration	= $this->input->post('iteration');
		$iterasi	= $iteration;
		$i_do	= $this->input->post('i_do');
		$d_do	= $this->input->post('d_do');
		$i_customer	= $this->input->post('icustomer');
		$i_branch	= $this->input->post('ibranch');
		
		$i_op_0	= $this->input->post('i_op_'.'tblItem'.'_'.'0');
		
		$ex_d_do = explode("/",$d_do,strlen($d_do)); // dd/mm/YYYY
		$nw_d_do = $ex_d_do[2]."-".$ex_d_do[1]."-".$ex_d_do[0];
		
		$i_op	= array();
		$i_product	= array();
		$e_product_name	= array();
		$n_deliver	= array();
		$vprice	= array();
		$harga_grosir	= array();
		$is_grosir	= array();
		$v_do_gross	= array();
		$e_note	= array();
		$qty_product	= array();
		$qty_op	= array();
		$i_op_sebunyi	= array();
		$f_stp	= array();
		
		$this->load->model('dopopup/mclass');
		
		for($cacah=0; $cacah<=$iteration; $cacah++) { // iterasi=2, 0<2, 1<2
		
			$i_op[$cacah]			= $this->input->post('i_op_'.'tblItem'.'_'.$cacah);
			$i_product[$cacah]		= $this->input->post('i_product_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem'.'_'.$cacah);
			$n_deliver[$cacah]		= $this->input->post('n_deliver_'.'tblItem'.'_'.$cacah);
			$vprice[$cacah]			= $this->input->post('price_'.'tblItem'.'_'.$cacah);
			$harga_grosir[$cacah]	= $this->input->post('grosir_'.'tblItem'.'_'.$cacah);
			$is_grosir[$cacah]	= $this->input->post('is_grosir_'.$cacah);
			
			$v_do_gross[$cacah]		= $this->input->post('v_do_gross_'.'tblItem'.'_'.$cacah);
			$e_note[$cacah]			= $this->input->post('e_note_'.'tblItem'.'_'.$cacah);
			$qty_product[$cacah]	= $this->input->post('qty_product_'.'tblItem'.'_'.$cacah);
			$qty_op[$cacah]			= $this->input->post('qty_op_'.'tblItem'.'_'.$cacah);
			$i_op_sebunyi[$cacah]	= $this->input->post('i_op_sebunyi_'.'tblItem'.'_'.$cacah);
			$f_stp[$cacah]			= $this->input->post('f_stp_tblItem_'.$cacah);
			
			// 10-05-2013
			$adaboneka[$cacah]	= $this->input->post('adaboneka_'.'tblItem'.'_'.$cacah);
			
			if($n_deliver[$cacah]=='') {
				$n_deliver[$cacah]	= 0;
			}
			
			if($v_do_gross[$cacah]==''){
				$v_do_gross[$cacah]	= 0;
			}
			
			if($qty_product[$cacah]==''){
				$qty_product[$cacah]	= 0;
			}
			
			if($qty_op[$cacah]==''){
				$qty_op[$cacah]	= 0;
			}
		}
		
		if(!empty($i_do) && 
		    !empty($i_customer) && 
		    !empty($i_branch)) {
			
			if(!empty($i_op_0)) {
				
				$qndo	= $this->mclass->cari_do($i_do);

				if($qndo->num_rows()>0) {
					print "<script>alert(\"Maaf, Nomor DO tsb sebelumnya telah diinput. Terimakasih.\");show(\"dopopup/cform\",\"#content\");</script>";
				}else{
					$this->mclass->msimpan($i_do,$nw_d_do,$i_customer,$i_branch,$i_op,$i_op_sebunyi,$i_product,$e_product_name,
					$n_deliver,$vprice,$v_do_gross,$e_note,$iterasi,$qty_product,$qty_op,$f_stp, $is_grosir, $harga_grosir,
					$adaboneka);
				}
				
			}else{
				print "<script>alert(\"Maaf, item DO harus terisi. Terimakasih.\");show(\"dopopup/cform\",\"#content\");</script>";
			}
			
		}else{
			$data['form_nomor_do']	= $this->lang->line('form_nomor_do');
			$data['form_tgl_do']	= $this->lang->line('form_tgl_do');
			$data['form_cabang_do']	= $this->lang->line('form_cabang_do');
			$data['form_option_pel_do']	= $this->lang->line('form_option_pel_do');
			$data['form_option_cab_do']	= $this->lang->line('form_option_cab_do');
			$data['form_title_detail_do']	= $this->lang->line('form_title_detail_do');
			$data['form_nomor_op_do']	= $this->lang->line('form_nomor_op_do');
			$data['form_kode_produk_do']	= $this->lang->line('form_kode_produk_do');
			$data['form_nm_produk_do']	= $this->lang->line('form_nm_produk_do');
			$data['form_jml_product_do']	= $this->lang->line('form_jml_product_do');
			$data['form_harga_product_do']	= $this->lang->line('form_harga_product_do');
			$data['form_ket_do']		= $this->lang->line('form_ket_do');
			$data['button_simpan']	= $this->lang->line('button_simpan');	
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['page_title_do']	= $this->lang->line('page_title_do');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['lpelanggan']	= "";
			$data['lcabang']	= "";
			$data['selected_cab']	= "";
			$tahun	= date("Y");		
	
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgDO']	= $tgl."/".$bln."/".$thn;
			
			$qryth	= $this->mclass->getthndo();
			$qrydo	= $this->mclass->getnomordo();
					
			$data['opt_pelanggan']	= $this->mclass->lpelanggan();
	
			if($qryth->num_rows() > 0) {
				$th		= $qryth->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}
			
			if($thn==$tahun) {
				if($qrydo->num_rows()>0)  {
					$row	= $qrydo->row_array();
					$do		= $row['idocode']+1;		
					switch(strlen($do)) {
						case "1": $nomordo	= "0000".$do;
						break;
						case "2": $nomordo	= "000".$do;
						break;	
						case "3": $nomordo	= "00".$do;
						break;
						case "4": $nomordo	= "0".$do;
						break;
						case "5": $nomordo	= $do;
						break;
					}
				} else {
					$nomordo		= "00001";
				}
				$nomor	= $tahun.$nomordo;
			} else {
				$nomor	= $tahun."00001";
			}
			$data['no']	= $nomor;
			
			print "<script>alert(\"Maaf, DO gagal disimpan. Terimakasih.\");show(\"dopopup/cform\",\"#content\");</script>";			
		}
	}
	
	function cari_do() {
		
		$ndo	= $this->input->post('ndo')?$this->input->post('ndo'):$this->input->get_post('ndo');
		
		$this->load->model('dopopup/mclass');
		
		$qndo	= $this->mclass->cari_do($ndo);
		
		if($qndo->num_rows()>0) {
			echo "Maaf, No. DO sudah ada!";
		}
	}
}
?>
