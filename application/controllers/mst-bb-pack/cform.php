<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-bb-pack/mmaster');
  }
  
  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['msg'] = '';
	$data['go_proses'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['unit_packing'] = $this->mmaster->get_unit_packing();
    $data['isi'] = 'mst-bb-pack/vmainform';
    
	$this->load->view('template',$data);

  }
  
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg 	= $this->uri->segment(4);
	if ($kel_brg == '')
		$kel_brg = $this->input->post('kel_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $kel_brg == '') {
		$kel_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barangtanpalimit($kel_brg, $keywordcari);
					$config['base_url'] = base_url()."index.php/mst-bb-pack/cform/show_popup_jenis/".$kel_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang($config['per_page'],$this->uri->segment(6), $kel_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['kel_brg'] = $kel_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_kel_brg_wip WHERE id = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('mst-bb-pack/vpopupjenis',$data);

  }
  function proseslistbrg() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_packing 	= $this->input->post('unit_packing', TRUE);
		$kel_brg 		= $this->input->post('kel_brg', TRUE);
		$kode_brg_jadi	=$this->input->post('kode_brg_jadi', TRUE);
		$kode_jenis 	= $this->input->post('kode_jenis', TRUE);
		$id_jenis 		= $this->input->post('id_jenis', TRUE);
		
		
		$data['list_brg'] = $this->mmaster->get_barang($kel_brg, $id_jenis, $unit_packing);
		$data['msg'] = '';	
		
		if (is_array($data['list_brg']) )
		$data['jum_data'] = count($data['list_brg'])+1;
	else
		$data['jum_data'] = 2;
		
		//$data['jum_data'] = count($data['list_brg']);
		//nm unit_packing
		$query	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");    
		$hasilrow = $query->row();
		$nama_ujh	= $hasilrow->kode_unit." - ".$hasilrow->nama;
		$kode_unit_packing = $hasilrow->kode_unit;
		
		//nm kel
		$query	= $this->db->query(" SELECT nama,kode FROM tm_kel_brg_wip WHERE id = '$kel_brg' ");    
		$hasilrow = $query->row();
		$nama_kel	= $hasilrow->nama;
		$kode_kel	= $hasilrow->kode;

		//nm jenis brg
		if ($id_jenis != '') {
			$query	= $this->db->query(" SELECT kode, nama FROM tm_jenis_brg_wip WHERE id = '$id_jenis' ");    
			$hasilrow = $query->row();
			$nama_jenis	= $hasilrow->kode." - ".$hasilrow->nama;
		}
		else
			$nama_jenis = '';
		
		
				
		$data['isi'] = 'mst-bb-pack/vmainform';
		$data['go_proses'] = '1';
		
		$data['kode_unit_packing'] = $kode_unit_packing;
		$data['id_unit_packing'] = $unit_packing;
		$data['nama_unit_packing'] = $nama_ujh;
		$data['kel_brg'] = $kel_brg;
		$data['nama_kel'] = $nama_kel;
		$data['kode_kel'] = $kode_kel;
		$data['nama_jenis'] = $nama_jenis;
		
		$this->load->view('template',$data);  
  }
  
  function caribrgwip(){
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil id, nama_brg
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_jadi."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_jadi = $hasilxx->id;
			$nama_brg_jadi = $hasilxx->nama_brg;
		}
		else {
			$id_brg_jadi = 0;
			$nama_brg_jadi = '';
		}
		
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['id_brg_jadi'] = $id_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('mst-bb-pack/vinfobrgwip', $data); 
		return true;
  }
  
  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$jum_data = $this->input->post('jum_data', TRUE);
		$jum_datakr=$jum_data-1;
		$id_unit_packing = $this->input->post('id_unit_packing', TRUE);  
		$kode_unit_packing = $this->input->post('kode_unit_packing', TRUE);  
		$tgl = date("Y-m-d");
		for ($i=1;$i<=$jum_datakr;$i++)
		{
			
			$this->db->query(" INSERT INTO tm_perb_harga_hasil_packing (kode_brg, id_unit_packing,id_brg_wip ,harga, tgl_input, tgl_update) 
							VALUES ('".$this->input->post('kode_brg_jadi_'.$i, TRUE)."','$id_unit_packing','".$this->input->post('id_brg_jadi_'.$i, TRUE)."',
							 '".$this->input->post('harga_'.$i, TRUE)."', '$tgl','$tgl') ");
			
			if (is_array($this->input->post('id_brg_jadi_'.$i)));{
				$cek_data = $this->mmaster->cek_data($id_unit_packing, $this->input->post('id_brg_jadi_'.$i, TRUE));
				print_r($jum_datakr);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id_unit_packing, $this->input->post('id_brg_jadi_'.$i, TRUE),
								$this->input->post('harga_'.$i, TRUE) , $kode_unit_packing ,$this->input->post('kode_brg_jadi_'.$i, TRUE));
				}
				else {
					if ($this->input->post('harga_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0)
						$this->db->query(" UPDATE tm_harga_brg_unit_packing SET harga = '".$this->input->post('harga_'.$i, TRUE)."'
								WHERE id_unit_packing = '$id_unit_packing' AND id_brg = '".$this->input->post('id_brg_jadi_'.$i, TRUE)."'
								 ");
				}
			
			}
		}
		redirect('mst-bb-pack/cform/view');
		
	}
		function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'mst-bb-pack/vformview';
    $keywordcari = "all";
    $unit_packing = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $unit_packing);	
							$config['base_url'] = base_url().'index.php/mst-bb-pack/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $unit_packing);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['eunit_packing'] = $unit_packing;
	$this->load->view('template',$data);
  }
  
  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $eunit_packing 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "mst-bb-pack/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-bb-pack/cform/cari/index/".$eunit_packing."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('mst-bb-pack/cform/view');
  }
  // extreme 21-05-2012
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$unit_packing 	= $this->input->post('unit_packing', TRUE);  
	//echo $unit_packing; die();
	if ($keywordcari == '' && $unit_packing == '') {
		$unit_packing 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($unit_packing == '')
		$unit_packing = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $unit_packing);
							$config['base_url'] = base_url().'index.php/mst-bb-pack/cform/cari/index/'.$unit_packing.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $unit_packing);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'mst-bb-pack/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['eunit_packing'] = $unit_packing;
	$this->load->view('template',$data);
  }
  function print_harga(){	  
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['isi'] = 'mst-bb-pack/vprintform';
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
   function do_exportexcel_harga() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  $unit_packing 	= $this->uri->segment(4);
	//  print_r($unit_packing);
	  $query= $this->mmaster->get_harga_for_print($unit_packing); 
	  
	  // ambil data nama unit_packing
	  if ($unit_packing != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
		$hasilrow = $query3->row();
		$kode_unit_packing	= $hasilrow->kode_unit;
		$nama_unit_packing	= $hasilrow->nama;
	  }
	  else {
		$kode_unit_packing = '';
		$nama_unit_packing = "All";
	  }
	  
	  $html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='4' align='center'>DATA HARGA PEMBELIAN UNIT packing</th>
				  </tr>
				<tr>	
					<th colspan='4' align='center'>Unit packing: ";
		
		if ($unit_packing != '0')
			$html_data.= "$kode_unit_packing - $nama_unit_packing";
		else
			$html_data.= "All";
		
		$html_data.="</th>
				 </tr>
				</table><br>";
				
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th>Unit packing</th>
					 
					 <th>Kode Barang - Nama Barang</th>
					 <th>Satuan</th>
					 <th>Harga (Rp.)</th>
					
				 </tr>
				</thead>
				<tbody>";
		
		if (is_array($query)) {
			$sup_temp = "";
			for($j=0;$j<count($query);$j++){
				$html_data.="<tr><td>";
				if ($sup_temp != $query[$j]['kode_unit_packing']) {
					$sup_temp = $query[$j]['kode_unit_packing'];
					$html_data.= $query[$j]['kode_unit_packing']." - ".$query[$j]['nama_unit_packing']; 
				}
				else
					$html_data.="&nbsp;";
				$html_data.="</td>
				
				<td>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>
				<td>"."Pieces"."</td>
				<td align='right'>".$query[$j]['harga']."</td>
				
				</tr> ";
			}
		}
		$html_data.= "</tbody></table>";
		
		$nama_file = "data_harga_brg_unit_packing";
		$export_excel1 = '1';
		//if ($export_excel1 != '')
			$nama_file.= ".xls";
		//else
		//	$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  }

