<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$data['page_title']	= "STATUS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('statusbrg/mclass');
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'statusbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();

		$qry_istatus	= $this->mclass->codestatus();
		if($qry_istatus->num_rows()>0) {
			$row_istatus	= $qry_istatus->row();
			$data['istatus']= $row_istatus->istatusproduct;
		} else {
			$data['istatus']= "";
		}
				
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']	= 'statusbrg/vmainform';
		$this->load->view('template',$data);
	}

	function tambah() {
		$data['page_title']	= "STATUS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('statusbrg/mclass');
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'statusbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();

		$qry_istatus	= $this->mclass->codestatus();
		if($qry_istatus->num_rows()>0) {
			$row_istatus	= $qry_istatus->row();
			$data['istatus']= $row_istatus->istatusproduct;
		} else {
			$data['istatus']= "";
		}
				
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']	= 'statusbrg/vmainformadd';
		$this->load->view('template',$data);
	}
	function pagesnext() {
		$data['page_title']	= "STATUS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('statusbrg/mclass');

		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'statusbrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		
		$qry_istatus	= $this->mclass->codestatus();
		if($qry_istatus->num_rows()>0) {
			$row_istatus	= $qry_istatus->row();
			$data['istatus']= $row_istatus->istatusproduct;
		} else {
			$data['istatus']= "";
		}
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'statusbrg/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail() {	
	$data['isi']	= 'statusbrg/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
		$vistatusproduct	= @$this->input->post('istatusproduct')?@$this->input->post('istatusproduct'):@$this->input->get_post('istatusproduct');
		$vestatusname		= @$this->input->post('estatusname')?@$this->input->post('estatusname'):@$this->input->get_post('estatusname');
		
		if( (isset($vistatusproduct) || !empty($vistatusproduct)) && 
		    (isset($vestatusname) || !empty($vestatusname)) &&
		    (($vistatusproduct!=0 || $vistatusproduct!="")) && ($vestatusname!=0 || $vestatusname!="") ) {
			$this->load->model('statusbrg/mclass');
			$this->mclass->msimpan($vistatusproduct,$vestatusname);
		} else {
			$data['page_title']	= "STATUS BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('statusbrg/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= 'statusbrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_links']	= $this->pagination_ori->create_links();
	
			$qry_istatus	= $this->mclass->codestatus();
			if($qry_istatus->num_rows()>0) {
				$row_istatus	= $qry_istatus->row();
				$data['istatus']= $row_istatus->istatusproduct;
			} else {
				$data['istatus']= "";
			}
					
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);			

			$data['isi']	= 'statusbrg/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	function edit() {
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
				
		$this->load->model('statusbrg/mclass');

		$qry_statusbrg		= $this->mclass->medit($id);
		if($qry_statusbrg->num_rows() > 0 ) {
			$row_statusbrg	= $qry_statusbrg->row();
			$data['i_status_product']	= $row_statusbrg->i_status_product;
			$data['e_statusname']	= $row_statusbrg->e_statusname;	
		} else {
			$data['i_status_product']	= "";
			$data['e_statusname']	= "";
		}
		$data['isi']	= 'statusbrg/veditform';
		$this->load->view('template',$data);
	}
	
	function actedit() {
		$istatusproduct	= $this->input->post('istatusproduct')?$this->input->post('istatusproduct'):$this->input->get_post('istatusproduct');
		$estatusname	= $this->input->post('estatusname')?$this->input->post('estatusname'):$this->input->get_post('estatusname');
		if( !empty($istatusproduct) && 
			!empty($estatusname) ) {
			$this->load->model('statusbrg/mclass');
			$this->mclass->mupdate($istatusproduct,$estatusname);
		} else {
			$data['page_title']	= "STATUS BARANG";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('statusbrg/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= 'statusbrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_links']	= $this->pagination_ori->create_links();
	
			$qry_istatus	= $this->mclass->codestatus();
			if($qry_istatus->num_rows()>0) {
				$row_istatus	= $qry_istatus->row();
				$data['istatus']= $row_istatus->istatusproduct;
			} else {
				$data['istatus']= "";
			}
					
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
			
			$data['isi']	= 'statusbrg/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	/* 20052011
	function actdelete() {
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_status_product',array('i_status_product'=>$id));
	}
	*/

	function actdelete() {
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('statusbrg/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$txt_i_status_product	= $this->input->post('txt_i_status_product')?$this->input->post('txt_i_status_product'):$this->input->get_post('txt_i_status_product');
		$txt_e_statusname	= $this->input->post('txt_e_statusname')?$this->input->post('txt_e_statusname'):$this->input->get_post('txt_e_statusname');

		$data['page_title']	= "STATUS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('statusbrg/mclass');
		
		$query	= $this->mclass->viewcari($txt_i_status_product,$txt_e_statusname);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'statusbrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->mcari($txt_i_status_product,$txt_e_statusname,$pagination['per_page'],$pagination['cur_page']);		
		$this->load->view('statusbrg/vcariform',$data);		
	}

	function carinext() {
		$txt_i_status_product	= $this->input->post('txt_i_status_product')?$this->input->post('txt_i_status_product'):$this->input->get_post('txt_i_status_product');
		$txt_e_statusname	= $this->input->post('txt_e_statusname')?$this->input->post('txt_e_statusname'):$this->input->get_post('txt_e_statusname');

		$data['page_title']	= "STATUS BARANG";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('statusbrg/mclass');
		
		$query	= $this->mclass->viewcari($txt_i_status_product,$txt_e_statusname);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'statusbrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
				
		$data['isi']	= $this->mclass->mcari($txt_i_status_product,$txt_e_statusname,$pagination['per_page'],$pagination['cur_page']);		
		$this->load->view('statusbrg/vcariform',$data);		
	}	
}
?>
