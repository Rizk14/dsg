<?php
class Cform extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{

		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_penjualanndo']	= $this->lang->line('page_title_penjualanndo');
		$data['form_title_detail_penjualanndo']	= $this->lang->line('form_title_detail_penjualanndo');
		$data['list_penjualanndo_kd_brg']	= $this->lang->line('list_penjualanndo_kd_brg');
		$data['list_penjualanndo_no_faktur']	= $this->lang->line('list_penjualanndo_no_faktur');
		$data['list_penjualanndo_jenis_brg']	= $this->lang->line('list_penjualanndo_jenis_brg');
		$data['list_penjualanndo_s_produksi'] = $this->lang->line('list_penjualanndo_s_produksi');
		$data['list_penjualanndo_no_do']	= $this->lang->line('list_penjualanndo_no_do');
		$data['list_penjualanndo_nm_brg']	= $this->lang->line('list_penjualanndo_nm_brg');
		$data['list_penjualanndo_qty']	= $this->lang->line('list_penjualanndo_qty');
		$data['list_penjualanndo_hjp']	= $this->lang->line('list_penjualanndo_hjp');
		$data['list_penjualanndo_amount']	= $this->lang->line('list_penjualanndo_amount');
		$data['list_penjualanndo_ndiscount']	= $this->lang->line('list_penjualanndo_ndiscount');
		$data['list_penjualanndo_vdiscount']	= $this->lang->line('list_penjualanndo_vdiscount');
		$data['list_penjualanndo_total_pengiriman']	= $this->lang->line('list_penjualanndo_total_pengiriman');
		$data['list_penjualanndo_total_penjualan']	= $this->lang->line('list_penjualanndo_total_penjualan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['limages']	= base_url();
		$this->load->model('listpenjualanndo/mclass');
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();

		$data['isi']	= 'listpenjualanndo/vmainform';
		$this->load->view('template', $data);
	}

	function listbarangjadi()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanndo/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();

		$pagination['base_url']		= base_url() . 'index.php/listpenjualanndo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4, 0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'], $pagination['cur_page']);

		$this->load->view('listpenjualanndo/vlistformbrgjadi', $data);
	}

	function listbarangjadinext()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanndo/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();

		$pagination['base_url']		= base_url() . 'index.php/listpenjualanndo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4, 0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'], $pagination['cur_page']);

		$this->load->view('listpenjualanndo/vlistformbrgjadi', $data);
	}

	function flistbarangjadi()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key') ? $this->input->post('key') : $this->input->get_post('key');

		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('listpenjualanndo/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();

		$list	= "";
		if ($jml > 0) {
			$cc	= 1;
			foreach ($query->result() as $row) {

				$list .= "
				 <tr>
				  <td width=\"2px;\">" . $cc . "</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">" . $row->ifakturcode . "</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">" . $row->dfaktur . "</a></td>
				 </tr>";

				$cc += 1;
			}
		}

		$item	=
			"<table class=\"listtable2\">
		<tbody>" .
			$list
			. "</tbody>
		</table>";

		echo $item;
	}

	function listsjp()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$is_pakai_sj	= $this->uri->segment(6);

		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['is_pakai_sj']	= $is_pakai_sj;

		if ($is_pakai_sj == '1')
			$data['page_title']	= "SURAT JALAN (SJ)";
		else
			$data['page_title']	= "DATA MASTER BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanndo/mclass');

		$query	= $this->mclass->lsjp($ibranch, $is_pakai_sj);
		$jml	= $query->num_rows();
		$result	= $query->result();

		$pagination['base_url'] = base_url() . 'index.php/listpenjualanndo/cform/listsjpnext/' . $iterasi . '/' . $ibranch . '/' . $is_pakai_sj;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7, 0);
		//$this->pagination->initialize($pagination);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lsjpperpages($ibranch, $is_pakai_sj, $pagination['per_page'], $pagination['cur_page']);

		$this->load->view('listpenjualanndo/vlistformsjp', $data);
	}

	function listsjpnext()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$is_pakai_sj	= $this->uri->segment(6);

		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['is_pakai_sj']	= $is_pakai_sj;

		if ($is_pakai_sj == '1')
			$data['page_title']	= "SURAT JALAN (SJ)";
		else
			$data['page_title']	= "DATA MASTER BARANG JADI";

		$data['page_title']	= "SURAT JALAN (SJ)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanndo/mclass');

		$query	= $this->mclass->lsjp($ibranch, $is_pakai_sj);
		$jml	= $query->num_rows();
		$result	= $query->result();

		$pagination['base_url'] = base_url() . 'index.php/listpenjualanndo/cform/listsjpnext/' . $iterasi . '/' . $ibranch . '/' . $is_pakai_sj;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6, 0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lsjpperpages($ibranch, $is_pakai_sj, $pagination['per_page'], $pagination['cur_page']);

		$this->load->view('listpenjualanndo/vlistformsjp', $data);
	}

	function flistsjp()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key') ? $this->input->post('key') : $this->input->get_post('key');
		$ibranchhidden	= $this->input->post('ibranchhidden') ? $this->input->post('ibranchhidden') : $this->input->get_post('ibranchhidden');

		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);

		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;

		$data['page_title']	= "SURAT JALAN (SJ)";
		$data['isi']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpenjualanndo/mclass');

		$query	= $this->mclass->flsjp($ibranchhidden, $key);
		$jml	= $query->num_rows();

		$list	= "";
		if ($jml > 0) {
			$cc	= 1;
			foreach ($query->result() as $row) {
				$list .= "
				 <tr>
					  <td>" . $cc . "</td>
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qty','$row->nilai','$row->isj','$row->qty');\">" . $row->isjcode . "</a></td>
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qty','$row->nilai','$row->isj','$row->qty');\">" . $row->imotif . "</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qty','$row->nilai','$row->isj','$row->qty');\">" . $row->productname . "</a></td>
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qty','$row->nilai','$row->isj','$row->qty');\">" . $row->qty . "</a></td>
				 </tr>";
				$cc += 1;
			}
		} else {
			$list .= "";
		}

		$item	=
			"<table class=\"listtable2\">
		<tbody>" .
			$list
			. "</tbody>
		</table>";

		echo $item;
	}

	function carilistpenjualanndo()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$nofaktur	= $this->input->post('no_faktur') ? $this->input->post('no_faktur') : $this->input->get_post('no_faktur');

		/* Disabled 09012011
		$ecategory	= $this->input->post('e_category')?$this->input->post('e_category'):$this->input->get_post('e_category');
		$fstopproduksi	= $this->input->post('f_stop_produksi')?$this->input->post('f_stop_produksi'):$this->input->get_post('f_stop_produksi');
		*/

		$data['nofaktur']	= $nofaktur;

		/* Disabled 09012011 
		$data['kategori']	= $ecategory;
		$data['sproduksi']	= $fstopproduksi=1?" checked ":"";
		*/

		$data['page_title_penjualanndo']	= $this->lang->line('page_title_penjualanndo');
		$data['form_title_detail_penjualanndo']	= $this->lang->line('form_title_detail_penjualanndo');
		$data['list_penjualanndo_kd_brg']	= $this->lang->line('list_penjualanndo_kd_brg');
		$data['list_penjualanndo_no_faktur']	= $this->lang->line('list_penjualanndo_no_faktur');
		$data['list_penjualanndo_jenis_brg']	= $this->lang->line('list_penjualanndo_jenis_brg');
		$data['list_penjualanndo_s_produksi'] = $this->lang->line('list_penjualanndo_s_produksi');
		$data['list_penjualanndo_no_do']	= $this->lang->line('list_penjualanndo_no_do');
		$data['list_penjualanndo_nm_brg']	= $this->lang->line('list_penjualanndo_nm_brg');
		$data['list_penjualanndo_qty']	= $this->lang->line('list_penjualanndo_qty');
		$data['list_penjualanndo_hjp']	= $this->lang->line('list_penjualanndo_hjp');
		$data['list_penjualanndo_amount']	= $this->lang->line('list_penjualanndo_amount');
		$data['list_penjualanndo_vdiscount']	= $this->lang->line('list_penjualanndo_vdiscount');
		$data['list_penjualanndo_ndiscount']	= $this->lang->line('list_penjualanndo_ndiscount');
		$data['list_penjualanndo_total_pengiriman']	= $this->lang->line('list_penjualanndo_total_pengiriman');
		$data['list_penjualanndo_total_penjualan']	= $this->lang->line('list_penjualanndo_total_penjualan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";
		$data['limages']	= base_url();

		$this->load->model('listpenjualanndo/mclass');

		$data['opt_jns_brg']	= $this->mclass->lklsbrg();

		/* Disabled 09012011
		$data['isi']	= $this->mclass->clistpenjualanndo($iproduct,$ecategory,$fstopproduksi);
		*/

		$data['query']	= $this->mclass->clistpenjualanndo($nofaktur);
		$data['isi']	= 'listpenjualanndo/vlistform';
		$this->load->view('template', $data);
	}

	function edit()
	{

		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
		$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
		$data['form_nomor_f_fpenjualanndo']	= $this->lang->line('form_nomor_f_fpenjualanndo');
		$data['form_tgl_f_fpenjualanndo']	= $this->lang->line('form_tgl_f_fpenjualanndo');
		$data['form_cabang_fpenjualanndo']	= $this->lang->line('form_cabang_fpenjualanndo');
		$data['form_pilih_cab_fpenjualanndo']	= $this->lang->line('form_pilih_cab_fpenjualanndo');
		$data['form_pilih_cab_manual_fpenjualanndo'] = $this->lang->line('form_pilih_cab_manual_fpenjualanndo');
		$data['form_detail_f_fpenjualanndo']	= $this->lang->line('form_detail_f_fpenjualanndo');
		$data['form_kd_brg_fpenjualanndo']	= $this->lang->line('form_kd_brg_fpenjualanndo');
		$data['form_nm_brg_fpenjualanndo']	= $this->lang->line('form_nm_brg_fpenjualanndo');
		$data['form_hjp_fpenjualanndo']		= $this->lang->line('form_hjp_fpenjualanndo');
		$data['form_qty_fpenjualanndo']		= $this->lang->line('form_qty_fpenjualanndo');
		$data['form_ndiscount_fpenjualanndo']	= $this->lang->line('form_ndiscount_fpenjualanndo');
		$data['form_vdiscount_fpenjualanndo']	= $this->lang->line('form_vdiscount_fpenjualanndo');
		$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
		$data['form_ket_f_fpenjualanndo']	= $this->lang->line('form_ket_f_fpenjualanndo');
		$data['form_tgl_jtempo_fpenjualanndo']	= $this->lang->line('form_tgl_jtempo_fpenjualanndo');
		$data['form_no_fpajak_fpenjualanndo']	= $this->lang->line('form_no_fpajak_fpenjualanndo');
		$data['form_tgl_fpajak_fpenjualanndo']	= $this->lang->line('form_tgl_fpajak_fpenjualanndo');
		$data['form_ket_cetak_fpenjualanndo']	= $this->lang->line('form_ket_cetak_fpenjualanndo');
		$data['form_tnilai_fpenjualanndo']	= $this->lang->line('form_tnilai_fpenjualanndo');
		$data['form_diskon_fpenjualanndo']	= $this->lang->line('form_diskon_fpenjualanndo');
		$data['form_dlm_fpenjualanndo']		= $this->lang->line('form_dlm_fpenjualanndo');
		$data['form_total_fpenjualanndo']	= $this->lang->line('form_total_fpenjualanndo');
		$data['form_ppn_fpenjualanndo']		= $this->lang->line('form_ppn_fpenjualanndo');
		$data['form_grand_t_fpenjualanndo']	= $this->lang->line('form_grand_t_fpenjualanndo');

		$data['button_update']	= $this->lang->line('button_update');
		$data['button_batal']	= $this->lang->line('button_batal');

		$data['isi']		= "";
		$data['list']		= "";
		$data['lcabang']	= "";
		$data['limages']	= base_url();
		$tahun	= date("Y");
		$data['tjthtempo']	= "";

		$ifakturcode	= $this->uri->segment('4');
		$data['ifakturcode']	= $ifakturcode;

		$this->load->model('listpenjualanndo/mclass');

		$qryfakheader	= $this->mclass->getfakheader($ifakturcode);

		if ($qryfakheader->num_rows() > 0) {

			$row_fakheader	= $qryfakheader->row();

			$i_faktur_code	= $row_fakheader->i_faktur_code;
			$data['is_pakai_sj']		= $row_fakheader->is_pakai_sj;
			$data['i_faktur']		= $row_fakheader->i_faktur;
			$data['i_faktur_code']	= $row_fakheader->i_faktur_code;
			$data['i_faktur_pajak']	= $row_fakheader->i_faktur_pajak;
			$tglfaktur	= explode("-", $row_fakheader->d_faktur, strlen($row_fakheader->d_faktur)); // YYYY-mm-dd
			$tjthtempo	= explode("-", $row_fakheader->d_due_date, strlen($row_fakheader->d_due_date)); // YYYY-mm-dd
			$tgPajak	= explode("-", $row_fakheader->d_pajak, strlen($row_fakheader->d_pajak)); // YYYY-mm-dd
			$data['f_printed']	= $row_fakheader->f_printed == 't' ? 'checked' : '';
			$data['tgFAKTUR']	= $tglfaktur[2] . "/" . $tglfaktur[1] . "/" . $tglfaktur[0];
			$data['tjthtempo']	= $tjthtempo[2] . "/" . $tjthtempo[1] . "/" . $tjthtempo[0];
			$data['tgPajak']	= $tgPajak[2] . "/" . $tgPajak[1] . "/" . $tgPajak[0];
			$data['n_discount']	= $row_fakheader->n_discount;
			$data['v_discount']	= $row_fakheader->v_discount;
			$ibranchname		= $row_fakheader->e_branch_name;
			$data['ibranchname']	= $ibranchname;
			$data['e_note_faktur']	= $row_fakheader->e_note_faktur;
			$qicustomer	= $this->mclass->geticustomer($ibranchname);

			if ($qicustomer->num_rows() > 0) {

				$row_icustomer	= $qicustomer->row();

				$icustomer	= $row_icustomer->i_customer;
				$ibranch	= $row_icustomer->i_branch;

				$data['icustomer']	= $icustomer;
				$data['ibranch']	= $ibranch;
			}

			$data['fakturitem']	= $this->mclass->lfakturitem($row_fakheader->i_faktur);
			$data['opt_cabang']	= $this->mclass->lcabang($icustomer);
		}
		$data['isi']	= 'listpenjualanndo/veditform';
		$this->load->view('template', $data);
	}

	function cari_fpenjualan()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$foriginal	= $this->input->post('foriginal') ? $this->input->post('foriginal') : $this->input->get_post('foriginal');
		$fpenj	= $this->input->post('fpenj') ? $this->input->post('fpenj') : $this->input->get_post('fpenj');

		$this->load->model('listpenjualanndo/mclass');

		$qnsop	= $this->mclass->cari_fpenjualan($fpenj);

		if ($qnsop->num_rows() > 0 && $foriginal != $fpenj) {
			echo "Maaf, No. Faktur sudah ada!" . $foriginal;
		}
	}

	function actedit()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_penjualanndo']		= $this->lang->line('page_title_penjualanndo');
		$data['form_title_detail_penjualanndo']		= $this->lang->line('form_title_detail_penjualanndo');
		$data['list_penjualanndo_kd_brg']		= $this->lang->line('list_penjualanndo_kd_brg');
		$data['list_penjualanndo_no_faktur']		= $this->lang->line('list_penjualanndo_no_faktur');
		$data['list_penjualanndo_jenis_brg']		= $this->lang->line('list_penjualanndo_jenis_brg');
		$data['list_penjualanndo_s_produksi'] 		= $this->lang->line('list_penjualanndo_s_produksi');
		$data['list_penjualanndo_no_do']		= $this->lang->line('list_penjualanndo_no_do');
		$data['list_penjualanndo_nm_brg']		= $this->lang->line('list_penjualanndo_nm_brg');
		$data['list_penjualanndo_qty']			= $this->lang->line('list_penjualanndo_qty');
		$data['list_penjualanndo_hjp']			= $this->lang->line('list_penjualanndo_hjp');
		$data['list_penjualanndo_amount']		= $this->lang->line('list_penjualanndo_amount');
		$data['list_penjualanndo_ndiscount']		= $this->lang->line('list_penjualanndo_ndiscount');
		$data['list_penjualanndo_vdiscount']		= $this->lang->line('list_penjualanndo_vdiscount');
		$data['list_penjualanndo_total_pengiriman']	= $this->lang->line('list_penjualanndo_total_pengiriman');
		$data['list_penjualanndo_total_penjualan']	= $this->lang->line('list_penjualanndo_total_penjualan');

		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');

		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['limages']	= base_url();

		$this->load->model('listpenjualanndo/mclass');

		$data['opt_jns_brg']	= $this->mclass->lklsbrg();

		/*** $this->load->view('listpenjualanndo/vmainform',$data); ***/

		/*** 05032012
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$i_faktur	= $this->input->post('i_faktur')?$this->input->post('i_faktur'):$this->input->get_post('i_faktur');
		$d_faktur	= $this->input->post('d_faktur')?$this->input->post('d_faktur'):$this->input->get_post('d_faktur');
		$i_branch	= $this->input->post('i_branch')?$this->input->post('i_branch'):$this->input->get_post('i_branch');
		$d_pajak	= $this->input->post('d_pajak')?$this->input->post('d_pajak'):$this->input->get_post('d_pajak');
		$ifakturcode	= $this->input->post('ifakturcodehiden')?$this->input->post('ifakturcodehiden'):$this->input->get_post('ifakturcodehiden');
		$ifaktur	= $this->input->post('ifakturhiden')?$this->input->post('ifakturhiden'):$this->input->get_post('ifakturhiden');
		 ***/

		$i_product	= array();
		$e_product_name	= array();
		$v_hjp		= array();
		$n_quantity	= array();
		$v_unit_price	= array();
		$isjcode	= array();
		$n_discounts	= array();
		$v_discounts	= array();


		$iteration	= $this->input->post('iteration') ? $this->input->post('iteration') : $this->input->get_post('iteration');
		$i_faktur	= $this->input->post('i_faktur');
		$d_faktur	= $this->input->post('d_faktur');
		$i_branch	= $this->input->post('i_branch');
		$d_pajak	= $this->input->post('d_pajak');
		$ifakturcode	= $this->input->post('ifakturcodehiden');
		$ifaktur	= $this->input->post('ifakturhiden');
		// 29-05-2013
		$is_pakai_sj	= $this->input->post('is_pakai_sj');

		$i_product_0	= $this->input->post('i_product' . '_' . 'tblItem' . '_' . '0');

		for ($cacah = 0; $cacah <= $iteration; $cacah++) {

			/*** 05032012
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_hjp[$cacah]	= $this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('v_hjp'.'_'.'tblItem'.'_'.$cacah);
			$n_quantity[$cacah]	= $this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('n_quantity'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
			$isjcode[$cacah]	= $this->input->post('isjcode'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('isjcode'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('isjcode'.'_'.'tblItem'.'_'.$cacah);
			 ***/

			$i_product[$cacah]	= $this->input->post('i_product' . '_' . 'tblItem' . '_' . $cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name' . '_' . 'tblItem' . '_' . $cacah);
			$v_hjp[$cacah]		= $this->input->post('v_hjp' . '_' . 'tblItem' . '_' . $cacah);
			$n_quantity[$cacah]	= $this->input->post('n_quantity' . '_' . 'tblItem' . '_' . $cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price' . '_' . 'tblItem' . '_' . $cacah);
			$v_discounts[$cacah]	= $this->input->post('n_discount' . '_' . 'tblItem' . '_' . $cacah);
			$n_discounts[$cacah]	= $this->input->post('v_discount' . '_' . 'tblItem' . '_' . $cacah);
			$isjcode[$cacah]	= $this->input->post('isjcode' . '_' . 'tblItem' . '_' . $cacah);
			//			echo $v_unit_price[$cacah];
			#			break;		
		}

		/*** 05032012
		$e_note_faktur	= $this->input->post('e_note_faktur')?$this->input->post('e_note_faktur'):$this->input->get_post('e_note_faktur');
		$v_total_nilai	= $this->input->post('v_total_nilai')?$this->input->post('v_total_nilai'):$this->input->get_post('v_total_nilai');
		$n_discount	= $this->input->post('n_discount')?$this->input->post('n_discount'):$this->input->get_post('n_discount');
		$v_discount	= $this->input->post('v_discount')?$this->input->post('v_discount'):$this->input->get_post('v_discount');
		$d_due_date	= $this->input->post('d_due_date')?$this->input->post('d_due_date'):$this->input->get_post('d_due_date');
		$v_total_faktur	= $this->input->post('v_total_faktur')?$this->input->post('v_total_faktur'):$this->input->get_post('v_total_faktur');
		$i_faktur_pajak	= $this->input->post('i_faktur_pajak')?$this->input->post('i_faktur_pajak'):$this->input->get_post('i_faktur_pajak');
		$n_ppn	= $this->input->post('n_ppn')?$this->input->post('n_ppn'):$this->input->get_post('n_ppn');
		$fcetak	= $this->input->post('f_cetak')?$this->input->post('f_cetak'):$this->input->get_post('f_cetak');
		$v_total_fppn	= $this->input->post('v_total_fppn')?$this->input->post('v_total_fppn'):$this->input->post('v_total_fppn');
		 ***/

		$e_note_faktur	= $this->input->post('e_note_faktur');
		$v_total_nilai	= $this->input->post('v_total_nilai');
		$v_discount	= $this->input->post('v_discount');
		$n_discount	= $this->input->post('n_discount');
		$d_due_date	= $this->input->post('d_due_date');
		$v_total_faktur	= $this->input->post('v_total_faktur');
		$i_faktur_pajak	= $this->input->post('i_faktur_pajak');
		$n_ppn	= $this->input->post('n_ppn');
		$fcetak	= $this->input->post('f_cetak');
		$v_total_fppn	= $this->input->post('v_total_fppn');

		$ex_d_pajak		= explode("/", $d_pajak, strlen($d_pajak)); // dd/mm/YYYY
		$ex_d_due_date	= explode("/", $d_due_date, strlen($d_due_date));
		$ex_d_faktur	= explode("/", $d_faktur, strlen($d_faktur));

		$nw_d_pajak	= $ex_d_pajak[2] . "-" . $ex_d_pajak[1] . "-" . $ex_d_pajak[0]; //YYYY-mm-dd
		$nw_d_due_date	= $ex_d_due_date[2] . "-" . $ex_d_due_date[1] . "-" . $ex_d_due_date[0]; //YYYY-mm-dd
		$nw_d_faktur	= $ex_d_faktur[2] . "-" . $ex_d_faktur[1] . "-" . $ex_d_faktur[0]; //YYYY-mm-dd

		$ex_v_total_faktur	= explode(".", $v_total_faktur, strlen($v_total_faktur));
		$ex_v_total_fppn	= explode(".", $v_total_fppn, strlen($v_total_fppn));
		$ex_v_discount	= explode(".", $v_discount, strlen($v_discount));

		$nw_v_total_faktur	= $ex_v_total_faktur[0];
		$nw_v_total_fppn	= $ex_v_total_fppn[0];
		$nw_v_discount		= $ex_v_discount[0];

		/*** $this->load->model('listpenjualanndo/mclass'); ***/
		if (
			!empty($i_faktur) &&
			!empty($d_faktur) &&
			!empty($i_branch)
		) {
			if (!empty($i_product_0)) {

				$qnsop	= $this->mclass->cari_fpenjualan($i_faktur);
				if (($qnsop->num_rows() < 0 || $qnsop->num_rows() == 0) || ($ifakturcode == $i_faktur)) {
					$this->mclass->mupdate($is_pakai_sj, $i_faktur, $nw_d_faktur, $i_branch, $e_note_faktur, $v_total_nilai, $n_discount, $nw_v_discount, $nw_d_due_date, $nw_v_total_faktur, $i_faktur_pajak, $nw_d_pajak, $n_ppn, $fcetak, $nw_v_total_fppn, $i_product, $e_product_name, $v_hjp, $n_quantity, $v_unit_price, $iteration, $isjcode, $ifakturcode, $ifaktur, $v_discounts, $n_discounts);
				} else {
					print "<script>alert(\"Maaf, Nomor Faktur tsb sebelumnya telah diinput. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
				}
			} else {
				print "<script>alert(\"Maaf, item Faktur Penjulan hrs terisi. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
	}

	function undo()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$i_faktur_code	= $this->uri->segment(5);
		$i_faktur	= $this->uri->segment(4);

		$this->load->model('listpenjualanndo/mclass');

		$this->mclass->mbatal($i_faktur, $i_faktur_code);
	}

	public function reprint()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}

		$ifakturcode = $this->uri->segment(4);
		$inota 		= $this->uri->segment(5);

		$this->db->trans_begin();

		$this->load->model('listpenjualanndo/mclass');

		$this->mclass->mreprint($ifakturcode, $inota);

		if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			$this->logger->write('Buka Ulang Cetak Nota No. Nota : ' . $ifakturcode . ' Id : ' . $inota);
		}
	}
}
