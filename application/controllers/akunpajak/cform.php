<?php

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		
		$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
		$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('akunpajak/mclass');
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'akunpajak/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		
		$qry_class				= $this->mclass->classcode();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['iclass']	= $row_class->classcode;
		} else {
			$data['iclass']	= "";
		}
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	= 'akunpajak/vmainform';
		$this->load->view('template',$data);
	}

function tambah() {
		
		$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
		$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('akunpajak/mclass');
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'akunpajak/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		
		$qry_class				= $this->mclass->classcode();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['iclass']	= $row_class->classcode;
		} else {
			$data['iclass']	= "";
		}
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	= 'akunpajak/vmainformadd';
		$this->load->view('template',$data);
	}
	function pagesnext() {
		
		$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
		$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('akunpajak/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'akunpajak/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		
		$qry_class				= $this->mclass->classcode();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['iclass']	= $row_class->classcode;
		} else {
			$data['iclass']	= "";
		}
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
			$data['isi']	= 'akunpajak/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail() {	
		$this->load->view('akunpajak/vmainform',$data);
	}
	
	function simpan() {
		
		$ekodeakunpajak	= @$this->input->post('ekodeakun')?@$this->input->post('ekodeakun'):@$this->input->get_post('ekodeakun');
		
		$this->load->model('akunpajak/mclass');
		
		$qkodeakunpajak	= $this->mclass->cekakunpajak($ekodeakunpajak);
		$nkodeakunpajak	= $qkodeakunpajak->num_rows();
		
		if((isset($ekodeakunpajak) || !empty($ekodeakunpajak)) &&
		    ($ekodeakunpajak!=0 || $ekodeakunpajak!="")) {
			if($nkodeakunpajak==0 || $nkodeakunpajak<=0){
				$this->mclass->msimpan($ekodeakunpajak);
			}else{
				$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
				$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
				$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
				$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
				$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');	
				$data['detail']		= "";
				$data['list']		= "";
				$data['not_defined']= "Maaf Data tdk dapat disimpan, terimakasih";
				$data['limages']	= base_url();
				
				$query	= $this->mclass->view();
				$jml	= $query->num_rows();
				
				$pagination['base_url'] = 'akunpajak/cform/pagesnext/';
				$pagination['total_rows']	= $jml;
				$pagination['per_page']		= 20;
				$pagination['first_link'] 	= 'Awal';
				$pagination['last_link'] 	= 'Akhir';
				$pagination['next_link'] 	= 'Selanjutnya';
				$pagination['prev_link'] 	= 'Sebelumnya';
				$pagination['cur_page'] 	= $this->uri->segment(4,0);
				$this->pagination_ori->initialize($pagination);
				$data['create_link']	= $this->pagination_ori->create_links();
				
				$qry_class				= $this->mclass->classcode();
				
				if($qry_class->num_rows()>0) {
					$row_class	= $qry_class->row();
					$data['iclass']	= $row_class->classcode;
				} else {
					$data['iclass']	= "";
				}
				
				$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
				
					$data['isi']	= 'akunpajak/vmainform';
		$this->load->view('template',$data);		
			}
		}else{
			
			$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
			$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
			$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
			$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
			$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'akunpajak/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();
			
			$qry_class				= $this->mclass->classcode();
			
			if($qry_class->num_rows()>0) {
				$row_class	= $qry_class->row();
				$data['iclass']	= $row_class->classcode;
			}else{
				$data['iclass']	= "";
			}
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
			
				$data['isi']	= 'akunpajak/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	function edit() {
		
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		
		$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
		$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$limages			= base_url();
		$data['list']		= "";
		
		$this->load->model('akunpajak/mclass');
		
		$qry_klsbrg		= $this->mclass->medit($id);
		
		if( $qry_klsbrg->num_rows() > 0 ) {
			$row_klsbrg	= $qry_klsbrg->row();
			$data['iakun']		= $row_klsbrg->i_akun;
			$data['eakunpajak']	= $row_klsbrg->i_akun_pajak;
		}else{
			$data['eakunpajak']	= "";		
		}
		
			$data['isi']	= 'akunpajak/veditform';
		$this->load->view('template',$data);
	}
	
	function actedit() {
		
		$iakun= @$this->input->post('iakun')?@$this->input->post('iakun'):@$this->input->get_post('iakun');
		$eakunpajak= @$this->input->post('ekodeakun')?@$this->input->post('ekodeakun'):@$this->input->get_post('ekodeakun');
		
		$this->load->model('akunpajak/mclass');
		
		if((isset($eakunpajak) || !empty($eakunpajak)) &&
		    ($eakunpajak!=0 || $eakunpajak!="")) {
			$this->mclass->mupdate($iakun,$eakunpajak);
		}else{
			$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
			$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
			$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
			$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
			$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'akunpajak/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();
			
			$qry_class				= $this->mclass->classcode();
			
			if($qry_class->num_rows()>0) {
				$row_class	= $qry_class->row();
				$data['iclass']	= $row_class->classcode;
			}else{
				$data['iclass']	= "";
			}
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
			
			$data['isi']	= 'akunpajak/vmainform';
		$this->load->view('template',$data);
		}
	}

	function actdelete() {
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('akunpajak/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		
		$txtakunpajak	= $this->input->post('txtakunpajak')?$this->input->post('txtakunpajak'):$this->input->get_post('txtakunpajak');

		$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
		$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('akunpajak/mclass');
		
		$query	= $this->mclass->viewcari($txtakunpajak);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'akunpajak/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txtakunpajak,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('akunpajak/vcariform',$data);
	}

	function carinext() {
		$txtakunpajak	= $this->input->post('txtakunpajak')?$this->input->post('txtakunpajak'):$this->input->get_post('txtakunpajak');
		$data['page_title_akun_pajak']	= $this->lang->line('page_title_akun_pajak');
		$data['form_panel_daftar_akun_pajak']	= $this->lang->line('form_panel_daftar_akun_pajak');
		$data['form_panel_form_akun_pajak']	= $this->lang->line('form_panel_form_akun_pajak');
		$data['form_panel_cari_akun_pajak']	= $this->lang->line('form_panel_cari_akun_pajak');
		$data['form_kode_akun_pajak']	= $this->lang->line('form_kode_akun_pajak');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('akunpajak/mclass');
		
		$query	= $this->mclass->viewcari($txtakunpajak);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'akunpajak/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txtakunpajak,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('akunpajak/vcariform',$data);
	}	
}
?>
