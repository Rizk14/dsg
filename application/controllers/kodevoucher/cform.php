<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_kodevoucher']		= $this->lang->line('page_title_kodevoucher');
		$data['form_panel_daftar_kodevoucher']	= $this->lang->line('form_panel_daftar_kodevoucher');
		$data['form_panel_form_kodevoucher']	= $this->lang->line('form_panel_form_kodevoucher');
		$data['form_panel_cari_kodevoucher']	= $this->lang->line('form_panel_cari_kodevoucher');
		$data['form_jns_kodevoucher']		= $this->lang->line('form_jns_kodevoucher');
		$data['form_kode_kodevoucher']		= $this->lang->line('form_kode_kodevoucher');
		$data['form_nama_kodevoucher']		= $this->lang->line('form_nama_kodevoucher');
		$data['form_ket_kodevoucher']		= $this->lang->line('form_ket_kodevoucher');
		
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		
		$data['limages']	= base_url();
		
		$this->load->model('kodevoucher/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'kodevoucher/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']	= 'kodevoucher/vmainform';
		$this->load->view('template',$data);
		
	}
function tambah() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_kodevoucher']		= $this->lang->line('page_title_kodevoucher');
		$data['form_panel_daftar_kodevoucher']	= $this->lang->line('form_panel_daftar_kodevoucher');
		$data['form_panel_form_kodevoucher']	= $this->lang->line('form_panel_form_kodevoucher');
		$data['form_panel_cari_kodevoucher']	= $this->lang->line('form_panel_cari_kodevoucher');
		$data['form_jns_kodevoucher']		= $this->lang->line('form_jns_kodevoucher');
		$data['form_kode_kodevoucher']		= $this->lang->line('form_kode_kodevoucher');
		$data['form_nama_kodevoucher']		= $this->lang->line('form_nama_kodevoucher');
		$data['form_ket_kodevoucher']		= $this->lang->line('form_ket_kodevoucher');
		
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		
		$data['limages']	= base_url();
		
		$this->load->model('kodevoucher/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'kodevoucher/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']	= 'kodevoucher/vmainformadd';
		$this->load->view('template',$data);
		
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_kodevoucher']		= $this->lang->line('page_title_kodevoucher');
		$data['form_panel_daftar_kodevoucher']	= $this->lang->line('form_panel_daftar_kodevoucher');
		$data['form_panel_form_kodevoucher']	= $this->lang->line('form_panel_form_kodevoucher');
		$data['form_panel_cari_kodevoucher']	= $this->lang->line('form_panel_cari_kodevoucher');
		$data['form_jns_kodevoucher']		= $this->lang->line('form_jns_kodevoucher');
		$data['form_kode_kodevoucher']		= $this->lang->line('form_kode_kodevoucher');
		$data['form_nama_kodevoucher']		= $this->lang->line('form_nama_kodevoucher');
		$data['form_ket_kodevoucher']		= $this->lang->line('form_ket_kodevoucher');
		
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('kodevoucher/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'kodevoucher/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		

		$data['isi']	= 'kodevoucher/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']	= 'kodevoucher/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ijnsvoucherhidden	= $this->input->post('ijnsvoucherhidden');
		$kodevoucher		= $this->input->post('kodevoucher');
		$nmvoucher			= $this->input->post('nmvoucher');
		$ketvoucher			= $this->input->post('ketvoucher');
		
		$this->load->model('kodevoucher/mclass');
		
		$this->mclass->msimpan($ijnsvoucherhidden,$kodevoucher,$nmvoucher,$ketvoucher);
	}
	
	function listjnsvoucher() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('kodevoucher/mclass');
		$data['query']	= $this->mclass->jnsvoucher();	
	
		$this->load->view('kodevoucher/vlistformjnsvoucher',$data);
		
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_kodevoucher']		= $this->lang->line('page_title_kodevoucher');
		$data['form_panel_daftar_kodevoucher']	= $this->lang->line('form_panel_daftar_kodevoucher');
		$data['form_panel_form_kodevoucher']	= $this->lang->line('form_panel_form_kodevoucher');
		$data['form_panel_cari_kodevoucher']	= $this->lang->line('form_panel_cari_kodevoucher');
		$data['form_jns_kodevoucher']		= $this->lang->line('form_jns_kodevoucher');
		$data['form_kode_kodevoucher']		= $this->lang->line('form_kode_kodevoucher');
		$data['form_nama_kodevoucher']		= $this->lang->line('form_nama_kodevoucher');
		$data['form_ket_kodevoucher']		= $this->lang->line('form_ket_kodevoucher');
		
		$data['txt_cari']	= $this->lang->line('text_cari');
			
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		
		$this->load->model('kodevoucher/mclass');

		$qkodevoucher = $this->mclass->medit($id);
		
		if($qkodevoucher->num_rows()>0) {
			
			$row_kodevoucher	= $qkodevoucher->row();
			
			$qjnsvoucher = $this->mclass->jnsvoucher2($row_kodevoucher->i_jenis_voucher);
			$rjnsvoucher = $qjnsvoucher->row();
			
			$data['i_voucher']	= $row_kodevoucher->i_voucher;
			$data['i_jenis_voucher']= $row_kodevoucher->i_jenis_voucher;
			$data['e_jenis_voucher']= $rjnsvoucher->e_jenis_voucher;
			$data['i_voucher_code']	= $row_kodevoucher->i_voucher_code;
			$data['e_voucher_name']	= $row_kodevoucher->e_voucher_name;
			$data['e_description']	= $row_kodevoucher->e_description;
			
		}else{
			$data['i_voucher']	= "";
			$data['i_jenis_voucher']= "";
			$data['i_voucher_code']	= "";
			$data['e_voucher_name']	= "";
			$data['e_description']	= "";
		}
		$data['isi']	= 'kodevoucher/veditform';
		$this->load->view('template',$data);
		
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$ivoucher			= $this->input->post('ivoucher');
		$ijnsvoucherhidden	= $this->input->post('ijnsvoucherhidden');
		$kodevoucher		= $this->input->post('kodevoucher');
		$nmvoucher			= $this->input->post('nmvoucher');
		$ketvoucher			= $this->input->post('ketvoucher');
		
		if(!empty($ijnsvoucherhidden) &&
		    !empty($kodevoucher) && 
		    !empty($ketvoucher)) {
			
			$this->load->model('kodevoucher/mclass');
			$this->mclass->mupdate($ivoucher,$ijnsvoucherhidden,$kodevoucher,$nmvoucher,$ketvoucher);
			
		} else {
			$data['page_title_kodevoucher']		= $this->lang->line('page_title_kodevoucher');
			$data['form_panel_daftar_kodevoucher']	= $this->lang->line('form_panel_daftar_kodevoucher');
			$data['form_panel_form_kodevoucher']	= $this->lang->line('form_panel_form_kodevoucher');
			$data['form_panel_cari_kodevoucher']	= $this->lang->line('form_panel_cari_kodevoucher');
			$data['form_jns_kodevoucher']		= $this->lang->line('form_jns_kodevoucher');
			$data['form_kode_kodevoucher']		= $this->lang->line('form_kode_kodevoucher');
			$data['form_nama_kodevoucher']		= $this->lang->line('form_nama_kodevoucher');
			$data['form_ket_kodevoucher']		= $this->lang->line('form_ket_kodevoucher');
	
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			
			$data['txt_cari']	= $this->lang->line('text_cari');
			$data['isi']		= "";
			$data['list']		= "";
			
			$data['limages']	= base_url();
			
			$this->load->model('kodevoucher/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= 'kodevoucher/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		

			
		$data['isi']	= 'kodevoucher/vmainform';
		$this->load->view('template',$data);
	
		}		
	}
	
	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('kodevoucher/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txtcar	= $this->input->post('txtcari')?$this->input->post('txtcari'):$this->input->get_post('txtcari');
		
		$data['page_title_kodevoucher']		= $this->lang->line('page_title_kodevoucher');
		$data['form_panel_daftar_kodevoucher']	= $this->lang->line('form_panel_daftar_kodevoucher');
		$data['form_panel_form_kodevoucher']	= $this->lang->line('form_panel_form_kodevoucher');
		$data['form_panel_cari_kodevoucher']	= $this->lang->line('form_panel_cari_kodevoucher');
		$data['form_jns_kodevoucher']		= $this->lang->line('form_jns_kodevoucher');
		$data['form_kode_kodevoucher']		= $this->lang->line('form_kode_kodevoucher');
		$data['form_nama_kodevoucher']		= $this->lang->line('form_nama_kodevoucher');
		$data['form_ket_kodevoucher']		= $this->lang->line('form_ket_kodevoucher');
		
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('kodevoucher/mclass');
		$query	= $this->mclass->viewcari($txtcar);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'kodevoucher/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txtcar,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('kodevoucher/vcariform',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txtcar	= $this->input->post('txtcari')?$this->input->post('txtcari'):$this->input->get_post('txtcari');
		
		$data['page_title_kodevoucher']		= $this->lang->line('page_title_kodevoucher');
		$data['form_panel_daftar_kodevoucher']	= $this->lang->line('form_panel_daftar_kodevoucher');
		$data['form_panel_form_kodevoucher']	= $this->lang->line('form_panel_form_kodevoucher');
		$data['form_panel_cari_kodevoucher']	= $this->lang->line('form_panel_cari_kodevoucher');
		$data['form_jns_kodevoucher']		= $this->lang->line('form_jns_kodevoucher');
		$data['form_kode_kodevoucher']		= $this->lang->line('form_kode_kodevoucher');
		$data['form_nama_kodevoucher']		= $this->lang->line('form_nama_kodevoucher');
		$data['form_ket_kodevoucher']		= $this->lang->line('form_ket_kodevoucher');
		
		$data['txt_cari']	= $this->lang->line('text_cari');
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('kodevoucher/mclass');
		$query	= $this->mclass->viewcari($txtcar);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'kodevoucher/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txtcar,$pagination['per_page'],$pagination['cur_page']);

		$this->load->view('kodevoucher/vcariform',$data);
	}	
}
?>
