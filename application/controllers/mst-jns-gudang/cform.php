<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-jns-gudang/mmaster');
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode_jenis;
			$enama = $row->nama_jenis;
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-jns-gudang/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode_jenis', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('mst-jns-gudang/cform');
		}
		else
		{
			$kode 	= $this->input->post('kode_jenis', TRUE);
			$id_jenis 	= $this->input->post('id_jenis', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit != 1) {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					redirect('mst-jns-gudang/cform');
				}
				else {
					$this->mmaster->save($kode, $id_jenis, $nama, $goedit);
					redirect('mst-jns-gudang/cform');
				}
			}
			else {
				$this->mmaster->save($kode, $id_jenis, $nama, $goedit);
				redirect('mst-jns-gudang/cform');
			}
		}
  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete($id);
    redirect('mst-jns-gudang/cform');
  }
  
  
}
