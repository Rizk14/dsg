<?php
class Master_barang_wip extends CI_Controller
{
    public $data = array(
        'halaman' => 'master_barang_wip',        
        'title' => 'Master Barang WIP',
        'isi' => 'master_barang_wip/master_barang_wip_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_barang_wip/master_barang_wip_model', 'master_barang_wip');
    }

   
    public function index()
    {
        $this->data['values'] = (object) $this->master_barang_wip->default_values;
		$this->load->view('template', $this->data);
			
    }
     public function sukses_input()
    {	
		
        $this->data['isi'] = 'master_barang_wip/master_barang_wip-sukses';
        $this->load->view('template', $this->data);
    }

    // Jika pendaftaran error, tampilkan informasi mengenai error.
    public function error_input()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Penginputan Master Barang WIP Error';
        $this->load->view('template', $this->data);
    }
   public function submit()
    {
		//$nama_barang_wip=$this->input->post('nama_barang_wip',TRUE);
		//$kode_barang_wip=$this->input->post('kode_barang_wip',TRUE);
		$no=$this->input->post('no',TRUE);
        
       
		 $jumlah_input=$no-1;
		// print_r($jumlah_input);
		 for($i=1; $i<=$jumlah_input; $i++){
		$this->master_barang_wip->input(	
		$this->input->post('kode_barang_wip_m_'.$i,TRUE),
		$this->input->post('nama_barang_wip_'.$i,TRUE)
		);
		
		}
		if(true)
		redirect('master_barang_wip/master_barang_wip/sukses_input');
		else 
		redirect('master_barang_wip/master_barang_wip/error_input');
    }
    
    
   public function view($offset= null)
    {	
		
       $master_barang_wip = $this->master_barang_wip->get_all_paged($offset);
        if ($master_barang_wip) {
            $this->data['master_barang_wip'] = $master_barang_wip;
            $this->data['paging'] = $this->master_barang_wip->paging('biasa', site_url('master_barang_wip/master_barang_wip/view/'),4);
        } else {
            $this->data['master_barang_wip'] = 'Tidak ada data Master Barang WIP, Silahkan Melakukan '.anchor('/master_barang_wip/', 'Proses penginputan.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('master_barang_wip/master_barang_wip/cari');
        $this->data['isi'] = 'master_barang_wip/master_barang_wip_list';
        $this->load->view('template', $this->data);
    }
    public function cari($offset = 0)
    {
        $master_barang_wip = $this->master_barang_wip->cari($offset);
        if ($master_barang_wip) {
            $this->data['master_barang_wip'] = $master_barang_wip;
            $this->data['paging'] = $this->master_barang_wip->paging('pencarian', site_url('master_barang_wip/master_barang_wip/cari/'), 4);
        } else {
            $this->data['master_barang_wip'] = 'Data tidak ditemukan.'. anchor('master_barang_wip/master_barang_wip/view', ' Tampilkan semua Master Barang WIP.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('master_barang_wip/master_barang_wip/cari');
        $this->data['isi'] = 'master_barang_wip/master_barang_wip_list';
        $this->load->view('template', $this->data);
    }
    
   
    
     public function hapus()
    {
       $id=$this->uri->segment(4,TRUE);
        //~ if ($this->session->userdata('user_bagian') != '2') {
            //~ $this->session->set_flashdata('pesan_error', 'Anda tidak berhak menghapus data Master Barang WIP. Kembali ke halaman ' . anchor('master_barang_wip/master_barang_wip', 'master_barang_wip.', 'class="alert-link"'));
            //~ redirect('master_barang_wip/master_barang_wip/error');
        //~ }

      
        if (! $this->master_barang_wip->get_id($id)) {
            $this->session->set_flashdata('pesan_error', 'Data Master Barang WIP tidak ada. Kembali ke halaman ' . anchor('master_barang_wip/master_barang_wip', 'master_barang_wip.', 'class="alert-link"'));
            redirect('master_barang_wip/master_barang_wip/error');
        }

        // Hapus
			if ($this->master_barang_wip->delete_aktif($id)) {
            $this->session->set_flashdata('pesan', 'Data berhasil dihapus. Kembali ke halaman '. anchor('master_barang_wip/master_barang_wip/view', 'View Master Barang WIP.', 'class="alert-link"'));
            redirect('master_barang_wip/master_barang_wip/sukses');
       
        } else {
            $this->session->set_flashdata('pesan_error', 'Data gagal dihapus. Kembali ke halaman '. anchor('master_barang_wip/master_barang_wip/view', 'View Master Barang WIP.', 'class="alert-link"'));
            redirect('master_barang_wip/master_barang_wip/error');
        }
    }
     public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Master Barang Wip';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }
    
    public function edit()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $master_barang_wip = $this->master_barang_wip->get($id);
        if (! $master_barang_wip) {
            $this->session->set_flashdata('pesan_error', 'Data Master Barang WIP tidak ada. Kembali ke halaman ' . anchor('master_barang_wip/master-barang/view', 'Master Barang WIP.', 'class="alert-link"'));
            redirect('master_barang_wip/master-barang/error');
        }

        // Data untuk form.
        if (!$_POST) {
            $data = (object) $master_barang_wip;     
        } else {
            $data = (object) $this->input->post(null, true);
        }
        $this->data['values'] = $data;
        
		$this->data['isi'] = 'master_barang_wip/master_barang_wip_form_edit';
        $this->load->view('template', $this->data);
	 }
	    public function updatedata(){
			$id =  $this->input->post('id',TRUE);
	        $nama_barang_wip =  $this->input->post('nama_barang_wip',TRUE);
			$kode_barang_wip =  $this->input->post('kode_barang_wip',TRUE);
	    
	     if (! $this->master_barang_wip->validate('form_rules')) {
            $this->data['isi'] = 'master_barang_wip/master_barang_wip_form_edit';
            $this->data['form_action'] = site_url('master_barang_wip/master_barang_wip/view');
            return;     
		}
    	 
		if ($this->master_barang_wip->edit($id,$nama_barang_wip,$kode_barang_wip)) {
            $this->session->set_flashdata('pesan', 'Data berhasil diupdate. Kembali ke halaman ' . anchor('master_barang_wip/master_barang_wip/view', 'Master Barang WIP.', 'class="alert-link"'));
            redirect('master_barang_wip/master_barang_wip/sukses');
        } else {
            $this->session->set_flashdata('pesan_error', 'Data tidak berhasil diupdate. Kembali ke halaman ' . anchor('master_barang_wip/master_barang_wip/view', 'Master Barang WIP.', 'class="alert-link"'));
            redirect('master_barang_wip/master_barang_wip/error');
	}
}
 public function cetak(){
 
		$nama = $this->session->userdata('nama');
		$today = date("YmdHis");  
		$master_barang_wip=$this->master_barang_wip->get_all();
    
        $data['master_barang_wip'] = $master_barang_wip;
		$html = $this->load->view('master_barang_wip/master_barang_wip_pdf', $data, true);
        require(APPPATH."/third_party/html2pdf_4_03/html2pdf.class.php");
        try {
            $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array('20', '5', '20', '5'));
            $html2pdf->WriteHTML($html);
            $html2pdf->Output('master_barang_wip_'.$nama.'_'.$today.'.pdf');
        } catch (HTML2PDF_exception $e) {
           
            $this->session->set_flashdata('pesan_error', 'Maaf, kami mengalami kendala teknis. Kembali ke halaman ' . anchor('master_barang_wip/master_barang_wip', 'master_barang_wip.', 'class="alert-link"'));
            redirect('master_barang_wip/error');
        }

}
 public function export(){
	 $nama = $this->session->userdata('nama');
	 $today = date("YmdHis");  
	 $master_barang_wip=$this->master_barang_wip->get_all();
	 $jumlah = $this->input->post('jumlah');
 error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');


// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator('$nama')
							 ->setLastModifiedBy('$nama')
							 ->setTitle("PHPExcel Master Barang WIP Document")
							 ->setSubject("PHPExcel Master Barang WIP Document")
							 ->setDescription("Test Master Barang WIP for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Master Barang WIP result file");

try {
	 $a   = 2;
	 $col = 0;
     $bar = 2;
     $col2= 2;
	
// Add some data
echo date('H:i:s') , " Add some data" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('A')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('B')->setRowHeight(1);
$objPHPExcel->getActiveSheet()->getRowDimension('C')->setRowHeight(1);



// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0);
 for($i=0;$i<=$jumlah;$i++){
	 
$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'DATA MASTER BARANG WIP  ');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				$a++;
				$bar++;
				
$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'CV. DUTA SETIA GARMEN ');
 $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $a++;
				  
$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'NO');
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('B'.$a, 'KODE BARANG');
				  $objPHPExcel->getActiveSheet()->getStyle('B'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				   $objPHPExcel->getActiveSheet()->setCellValue('C'.$a, 'NAMA BARANG');
				  $objPHPExcel->getActiveSheet()->getStyle('C'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						  )
						  )
				  );
				
	
}				  
	$no=0;	
			  
foreach ($master_barang_wip as $row){
	 $no++;
	 $a++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$a, $no, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' =>PHPExcel_Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$a, $row->kode_barang_wip, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('B'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' =>PHPExcel_Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    
				    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$a, $row->nama_barang_wip, PHPExcel_Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('C'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							      'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
						    )
						    )
				    );
				    $a++;
}


    
// Save Excel 2007 file
echo date('H:i:s') , " Write to Excel2007 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('excel/master_barang_wip_'.$nama.'_'.$today.'.xlsx');
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , 'excel/master_barang_wip_'.$nama.'_'.$today.'.xlsx' , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Save Excel 95 file
echo date('H:i:s') , " Write to Excel5 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('excel/master_barang_wip_'.$nama.'_'.$today.'.xls');
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

	
		
echo date('H:i:s') , " File written to " , 'excel/master_barang_wip_'.$nama.'_'.$today.'.xlsx' , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing files" , EOL;
echo 'Files have been created in ' , base_url('excel/') , EOL;

$this->session->set_flashdata('pesan', 'Data berhasil diexport. Kembali ke halaman '. anchor('master_barang_wip/view', 'View Master Barang WIP.', 'class="alert-link"'). 
'  Atau melihat data yang diexport. '. anchor(base_url('excel'), 'View Export Excel.', 'class="alert-link"')
);
            redirect('master_barang_wip/sukses');
} 
		catch (Exception $e) {
			$this->session->set_flashdata('pesan_error', 'Maaf, kami mengalami kendala teknis. Kembali ke halaman ' . anchor('master_barang_wip/master_barang_wip', 'master_barang_wip.', 'class="alert-link"'));
            redirect('master_barang_wip/error');
   }

     }
     
}

