<?php
class Cform extends CI_Controller { 

  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmmasuk-hsl-cutting/mmaster');
  }

  function index(){	
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['cari'] = '';
	$data['bonm_detail'] = $this->mmaster->get_detail_bonm();
	$th_now	= date("Y");
	$data['msg'] = '';
	$data['isi'] = 'bonmmasuk-hsl-cutting/vmainform';
	$this->load->view('template',$data);
  }
  // no longer used anymore
  /*function edit(){
	$id_apply_stok 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_bonm($id_apply_stok);
	$data['msg'] = '';
	$data['id_apply_stok'] = $id_apply_stok;

	$data['isi'] = 'bonmmasuk/veditform';
	$this->load->view('template',$data);

  } */

  function submit() {		
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	  $tgl = date("Y-m-d");
			$no_bonm	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm	= $this->input->post('tgl_bonm', TRUE);
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			
			$jumheader	= $this->input->post('jumheader', TRUE);
			$jumdetail	= $this->input->post('jumdetail', TRUE);
			
			$list_detail 	= $this->input->post('list_detail', TRUE);
			 
			$id_detail = explode(",", $list_detail);
			
			$ambil=0;
			foreach($id_detail as $row1) {
				
				if ($row1 != '') {
					$query1	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting_detail 
										WHERE id='$row1' AND status_stok='f' ");
					if($query1->num_rows()>0) {
						
						$hasilrow = $query1->row();
						$id_apply_stok	= $hasilrow->id_apply_stok;
						$kode_brg_jadi	= $hasilrow->kode_brg_jadi;
						$brg_jadi_manual = $hasilrow->brg_jadi_manual;
						$kode_brg		= $hasilrow->kode_brg;
						$qty			= $hasilrow->qty;
						$id_realisasi_cutting_detail	= $hasilrow->id_realisasi_cutting_detail;
						
						$this->db->query(" UPDATE tm_apply_stok_hasil_cutting_detail SET status_stok='t', no_bonm = '$no_bonm',
										tgl_bonm = '$tgl_bonm', tgl_update = '$tgl'
										WHERE id='$row1' ");

						$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting_detail WHERE status_stok='f' 
												AND id_apply_stok='$id_apply_stok' ");
						if ($query2->num_rows()==0){
							$this->db->query(" UPDATE tm_apply_stok_hasil_cutting SET status_stok='t' WHERE id='$id_apply_stok' ");
						}
						
						$query3	= $this->db->query(" SELECT a.is_dacron, b.* FROM tt_realisasi_cutting a, 
											tt_realisasi_cutting_detail b WHERE a.id = b.id_realisasi_cutting AND
											b.id='$id_realisasi_cutting_detail' ");
						
						if($query3->num_rows()>0){
							$ridrealisasi	= $query3->row();
							$id_realisasi	= $ridrealisasi->id_realisasi_cutting;
							//$qty_realisasi	= $ridrealisasi->qty_realisasi;
							$jum_gelar		= $ridrealisasi->jum_gelar;
							$diprint		= $ridrealisasi->diprint;
							$is_dacron		= $ridrealisasi->is_dacron;
							
							// no longer used anymore
							/*$query4	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_hasil_cutting WHERE id='$id_apply_stok' ");
							$row2	= $query4->row();
							
							$no_bonm	= $row2->no_bonm; */
							
							$this->db->query(" UPDATE tt_realisasi_cutting_detail SET status_apply_stok='t' WHERE id='$id_realisasi_cutting_detail' ");
										
							$query3	= $this->db->query(" SELECT id FROM tt_realisasi_cutting WHERE status_apply_stok='f' 
													AND id='$id_realisasi' ");
							if ($query3->num_rows()==0){
								$this->db->query(" UPDATE tt_realisasi_cutting SET status_apply_stok='t' WHERE id='$id_realisasi' ");
							}
							
							$sqlxx = " SELECT * FROM tm_stok_hasil_cutting 
										WHERE kode_brg_jadi='$kode_brg_jadi' AND kode_brg='$kode_brg' AND diprint = '$diprint'
												AND is_dacron = '$is_dacron'";
							if ($brg_jadi_manual != '')
								$sqlxx.= " AND brg_jadi_manual = '$brg_jadi_manual' ";
							else
								$sqlxx.= " AND (brg_jadi_manual is NULL OR brg_jadi_manual = '') ";
							
							$qstokhslc = $this->db->query($sqlxx);
							
							if($qstokhslc->num_rows()>0){
								$rstokhslc = $qstokhslc->row();
								$id_stok_cutting= $rstokhslc->id;
								$total_gelar	= $rstokhslc->total_gelar;
								//$jmlstokhslc 	= ($rstokhslc->stok + $qty_realisasi);
								$jmlstokhslc 	= ($rstokhslc->stok + $qty);
								$jmlgelarhslc	= ($total_gelar + $jum_gelar);
								
								$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok='$jmlstokhslc', total_gelar='$jmlgelarhslc' 
								WHERE id='$id_stok_cutting' ");	
								
								/*$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg_jadi,kode_brg,no_bukti,masuk,
												saldo,tgl_input, diprint, is_dacron) VALUES('$kode_brg_jadi','$kode_brg','$no_bonm',
												'$qty_realisasi','$jmlstokhslc','$tgl_bonm', '$diprint', '$is_dacron') "); */
							}else{
								$this->db->query(" INSERT INTO tm_stok_hasil_cutting(kode_brg_jadi,kode_brg,stok,total_gelar,
												tgl_update_stok, diprint, is_dacron, brg_jadi_manual) 
												VALUES('$kode_brg_jadi','$kode_brg','$qty','$jum_gelar',
												'$tgl', '$diprint', '$is_dacron', '$brg_jadi_manual') ");
								/*$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg_jadi,kode_brg,no_bukti,masuk,saldo,tgl_input, diprint, is_dacron) 
												VALUES('$kode_brg_jadi','$kode_brg','$no_bonm','$qty_realisasi','$jum_gelar','$tgl_bonm', '$diprint', '$is_dacron' ) "); */
							}
						}						
					}			
					
				}
			} 
			
			redirect('bonmmasuk-hsl-cutting/cform/view'); //

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $data['isi'] = 'bonmmasuk-hsl-cutting/vformview';
    $keywordcari = "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk-hsl-cutting/cform/view/index/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$config['cur_page'], $keywordcari);				
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari=='') {
		$keywordcari 	= $this->uri->segment(4);
	}
	
	if ($keywordcari=='')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk-hsl-cutting/cform/cari/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'], $config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'bonmmasuk-hsl-cutting/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function batal(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $no_bonm 	= $this->uri->segment(4);
    
    $this->mmaster->delete($no_bonm);
    
    /*if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "bonmmasuk/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "bonmmasuk/cform/cari/".$cgudang."/".$carinya."/".$cur_page;
	
	/redirect($url_redirectnya); */
	redirect('bonmmasuk/cform/view');
  }
  
}
