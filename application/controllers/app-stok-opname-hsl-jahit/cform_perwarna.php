<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('app-stok-opname-hsl-jahit/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['isi'] = 'app-stok-opname-hsl-jahit/vmainform';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$gudang = $this->input->post('gudang', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
	  $query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
								WHERE a.id = '$gudang' ");
	  $hasilrow = $query3->row();
	  $kode_gudang	= $hasilrow->kode_gudang;
	  $nama_gudang	= $hasilrow->nama;
	  $nama_lokasi	= $hasilrow->nama_lokasi;
		
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['gudang'] = $gudang;
		$data['nama_lokasi'] = $nama_lokasi;
		$data['kode_gudang'] = $kode_gudang;
		$data['nama_gudang'] = $nama_gudang;

	$cek_data = $this->mmaster->cek_so($bulan, $tahun, $gudang); 
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." belum diinput..!";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'app-stok-opname-hsl-jahit/vmainform';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah di-approve..!";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname-hsl-jahit/vmainform';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_ yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname($bulan, $tahun, $gudang);
	
			$data['jum_total'] = count($data['query']);
			
			$tgl_so = $cek_data['tgl_so'];
			$pisah1 = explode("-", $tgl_so);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_so = $tgl1."-".$bln1."-".$thn1;
			
			$data['tgl_so'] = $tgl_so;
			$data['jenis_hitung'] = $cek_data['jenis_perhitungan_stok'];
			if ($cek_data['jenis_perhitungan_stok'] == '1')
				$data['nama_jenis_hitung'] = "1. Sudah menghitung barang masuk dan barang keluar";
			else if ($cek_data['jenis_perhitungan_stok'] == '2')
				$data['nama_jenis_hitung'] = "2. Sudah menghitung barang masuk, barang keluar belum";
			else if ($cek_data['jenis_perhitungan_stok'] == '3')
				$data['nama_jenis_hitung'] = "3. Belum menghitung barang masuk, barang keluar sudah";
			else if ($cek_data['jenis_perhitungan_stok'] == '4')
				$data['nama_jenis_hitung'] = "4. Belum menghitung barang masuk dan barang keluar";
			$data['isi'] = 'app-stok-opname-hsl-jahit/vformview';
			$this->load->view('template',$data);
			
		}
	} // end else

  }
  
  function submit() {
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $gudang = $this->input->post('gudang', TRUE);
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  
	  // 04-11-2015
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan < 12) {
		$bulanplus1 = $bulan+1;
		$tahunplus1 = $tahun;
		if ($bulanplus1 < 10)
			$bulanplus1= '0'.$bulanplus1;
	  }
	  else {
		$bulanplus1 = '01';
		$tahunplus1 = $tahun+1;
	  }
	  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$gudang' ");
	  $hasilrow = $query3->row();
	  $kode_gudang	= $hasilrow->kode_gudang;
	  $nama_gudang	= $hasilrow->nama;
	  $nama_lokasi	= $hasilrow->nama_lokasi;
	  
	  $tgl = date("Y-m-d H:i:s"); 
		  	      
	      for ($i=1;$i<=$no;$i++)
		  {
			 $id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
			 $id_warna = $this->input->post('id_warna_'.$i, TRUE);
			 $stok_fisik = $this->input->post('stok_fisik_warna_'.$i, TRUE);
			 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
			 $idnya = $this->input->post('id_'.$i, TRUE);
			 
			 // ============== new skrip 06-02-2014 ================================================
			 //-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				$qtytotal_saldo_akhir = 0;
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					$qtytotal+= $stok_fisik[$xx];
				} // end for
			// ---------------------------------------------------------------------
			
			// 26-06-2014
			$totalxx = $qtytotal;
			
			// ======== update stoknya! =============
			// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			//1. hitung brg keluar dari gudang WIP ke packing/gd jadi, dari tm_sjkeluarwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip' AND a.id_gudang = '$gudang' ";
				
				$query3	= $this->db->query($sql3);
				// AND a.tgl_sj >= '$tglawalplus'
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$keluarnya = $hasilrow->jum_keluar;
					
					if ($keluarnya == '')
						$keluarnya = 0;
				}
				else
					$keluarnya = 0;
			
			// 2. hitung brg masuk ke gudang WIP, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip' AND a.id_gudang = '$gudang' ";
				$query3	= $this->db->query($sql3);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$masuknya = $hasilrow->jum_masuk;
					
					if ($masuknya == '')
						$masuknya = 0;
				}
				else
					$masuknya = 0;
			
			$jum_stok_akhir = $masuknya-$keluarnya;
			$totalxx = $totalxx + $jum_stok_akhir;			
			// 04-03-2015
			//$totalxx_saldo_akhir = $totalxx_saldo_akhir + $jum_stok_akhir;
			//------------------------ END HITUNG TRANSAKSI di sjkeluarwip dan sjmasukwip ------------------------------------------------
			
			// ======== update stoknya! =============
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE id_brg_wip = '$id_brg_wip'
							AND id_gudang='$gudang' ");
					if ($query3->num_rows() == 0){
						$data_stok = array(
							'id_brg_wip'=>$id_brg_wip,
							'id_gudang'=>$gudang,
							//'stok'=>$qtytotal,
							//04-03-2015 ini diganti jadi dari saldo akhir. 21-10-2015 AUTOSALDOAKHIR GA DIPAKE!
							'stok'=>$totalxx,
							//'stok'=>$totalxx_saldo_akhir,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$totalxx', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_gudang='$gudang' ");
					}
					
				// stok_hasil_jahit_warna
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					
					$totalxx = $stok_fisik[$xx];
					
					//------------------------------------------------------------------------------
					// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
					// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
					
					//1. hitung brg keluar dari gudang WIP ke packing/gd jadi, dari tm_sjkeluarwip
					$sql3 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
					if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
						$sql3.=" a.tgl_sj > '$tgl_so' ";
					else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
						$sql3.=" a.tgl_sj >= '$tgl_so' ";
					$sql3.=" AND b.id_brg_wip = '$id_brg_wip'
									AND a.id_gudang = '$gudang'
									AND c.id_warna = '".$id_warna[$xx]."' ";
					$query3	= $this->db->query($sql3);
					
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluarnya = $hasilrow->jum_keluar;
							
							if ($keluarnya == '')
								$keluarnya = 0;
						}
						else
							$keluarnya = 0;
					
					// 2. hitung brg masuk ke gudang WIP, dari tm_sjmasukwip
					$sql3 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE ";
					if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
						$sql3.=" a.tgl_sj > '$tgl_so' ";
					else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						$sql3.=" a.tgl_sj >= '$tgl_so' ";
					$sql3.=" AND b.id_brg_wip = '$id_brg_wip'
									AND a.id_gudang = '$gudang' 
									AND c.id_warna = '".$id_warna[$xx]."' ";
					$query3	= $this->db->query($sql3);
					
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuknya = $hasilrow->jum_masuk;
							
							if ($masuknya == '')
								$masuknya = 0;
						}
						else
							$masuknya = 0;
					
					$jum_stok_akhir = $masuknya-$keluarnya;
					$totalxx = $totalxx + $jum_stok_akhir;
					// 04-03-2015. 21-10-2015 GA DIPAKE!
					//$totalxx_saldo_akhir = $totalxx_saldo_akhir + $jum_stok_akhir;
					// ------------ end hitung transaksi keluar/masuk ------------------------------
					
					// ========================= stok per warna ===============================================
					//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_hasil_jahit='$id_stok' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_hasil_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						//26-06-2014, pake $totalxx dari hasil perhitungan transaksi keluar/masuk
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_jahit'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							//'stok'=>$stok_fisik[$xx],
							//04-03-2015 dikomen. 21-10-2015 balikin ke totalxx
							'stok'=>$totalxx,
							//'stok'=>$totalxx_saldo_akhir,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_jahit='$id_stok' ");
					}
				} // end for
				// ----------------------------------------------
			 // ====================================================================================
			 
			 $this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET status_approve = 't'
								where id = '$idnya' ");

		  } // end for
		  $this->db->query(" UPDATE tt_stok_opname_hasil_jahit SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  
		  //redirect('app-stok-opname-hsl-jahit/cform');
		    $data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah berhasil di-approve, dan otomatis mengupdate stok";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname-hsl-jahit/vmainform';
			$this->load->view('template',$data);
  }
  
  // 26-06-2014
  function autoupdatesohasiljahit() {
	  $tgl = date("Y-m-d H:i:s"); 
	  
		$sqlxx = " SELECT id, bulan, tahun, id_gudang FROM tt_stok_opname_hasil_jahit WHERE status_approve='t' ORDER BY id_gudang, tahun, bulan ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				//---------------------------------------
				if ($rowxx->bulan < 12) {
					$bulanplus1 = $rowxx->bulan+1;
					$tahunplus1 = $rowxx->tahun;
					if ($bulanplus1 < 10)
						$bulanplus1= '0'.$bulanplus1;
				  }
				  else {
					$bulanplus1 = '01';
					$tahunplus1 = $rowxx->tahun+1;
				  }
				  
				  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
				//---------------------------------------
				
				// detail item brg dan juga warnanya
				$sqlxx2 = " SELECT id, id_stok_opname_hasil_jahit, kode_brg_jadi, jum_stok_opname
							FROM tt_stok_opname_hasil_jahit_detail WHERE id_stok_opname_hasil_jahit='$rowxx->id' ";
				$queryxx2	= $this->db->query($sqlxx2);
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						
						//1. hitung brg keluar dari gudang WIP ke packing/gd jadi, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b
									WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.id_gudang = '$rowxx->id_gudang' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluarnya = $hasilrow->jum_keluar;
							
							if ($keluarnya == '')
								$keluarnya = 0;
						}
						else
							$keluarnya = 0;
					
					// 2. hitung brg masuk ke gudang WIP, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= '$tglawalplus'
									AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
									AND a.id_gudang = '$rowxx->id_gudang' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuknya = $hasilrow->jum_masuk;
							
							if ($masuknya == '')
								$masuknya = 0;
						}
						else
							$masuknya = 0;
					
					$jum_stok_akhir = $masuknya-$keluarnya;
					
					//------------------------ END HITUNG TRANSAKSI di sjkeluarwip dan sjmasukwip -----------------------------------------
						
						$qtytotal = 0;
						$query3	= $this->db->query(" SELECT sum(jum_stok_opname) as jum_so
									FROM tt_stok_opname_hasil_jahit_detail_warna WHERE id_stok_opname_hasil_jahit_detail='$rowxx2->id' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qtytotal = $hasilrow->jum_so;
						}
						$totalxx = $qtytotal;
						
						// tambahkan stok_akhir ini ke qty SO
						$totalxx = $totalxx + $jum_stok_akhir; //echo $totalxx." ";
						
						// update stok
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit 
								WHERE kode_brg_jadi = '$rowxx2->kode_brg_jadi' AND id_gudang='$rowxx->id_gudang' ");
						if ($query3->num_rows() == 0){
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrow	= $seqxx->row();
								$id_stok	= $seqrow->id+1;
							}else{
								$id_stok	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok,
								'kode_brg_jadi'=>$rowxx2->kode_brg_jadi,
								'id_gudang'=>$rowxx->id_gudang,
								'stok'=>$totalxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_jahit', $data_stok);
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
														
							$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$totalxx', tgl_update_stok = '$tgl' 
							where kode_brg_jadi= '$rowxx2->kode_brg_jadi' AND id_gudang='$rowxx->id_gudang' ");
						}
						// end update stok
						
						$sqlxx3 = " SELECT * FROM tt_stok_opname_hasil_jahit_detail_warna WHERE id_stok_opname_hasil_jahit_detail='$rowxx2->id' ";
						$queryxx3	= $this->db->query($sqlxx3);
						if ($queryxx3->num_rows() > 0){
							$hasilxx3 = $queryxx3->result();
							foreach ($hasilxx3 as $rowxx3) {
								$totalxx = $rowxx3->jum_stok_opname;

								//1. hitung brg keluar dari gudang WIP ke packing/gd jadi, dari tm_sjkeluarwip
								$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a, 
												tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c
												WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
												AND a.tgl_sj >= '$tglawalplus'
												AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
												AND a.id_gudang = '$rowxx->id_gudang'
												AND c.kode_warna = '".$rowxx3->kode_warna."' ");
								
									if ($query3->num_rows() > 0){
										$hasilrow = $query3->row();
										$keluarnya = $hasilrow->jum_keluar;
										
										if ($keluarnya == '')
											$keluarnya = 0;
									}
									else
										$keluarnya = 0;
								
								// 2. hitung brg masuk ke gudang WIP, dari tm_sjmasukwip
								$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a, 
												tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
												WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
												AND a.tgl_sj >= '$tglawalplus'
												AND b.kode_brg_jadi = '$rowxx2->kode_brg_jadi'
												AND a.id_gudang = '$rowxx->id_gudang' 
												AND c.kode_warna = '".$rowxx3->kode_warna."'");
								
									if ($query3->num_rows() > 0){
										$hasilrow = $query3->row();
										$masuknya = $hasilrow->jum_masuk;
										
										if ($masuknya == '')
											$masuknya = 0;
									}
									else
										$masuknya = 0;
								
								$jum_stok_akhir = $masuknya-$keluarnya;
								// ------------ end hitung transaksi keluar/masuk ------------------------------
								
								// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
								$totalxx = $totalxx + $jum_stok_akhir;
								
								//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE kode_warna = '".$rowxx3->kode_warna."'
										AND id_stok_hasil_jahit='$id_stok' ");
								if ($query3->num_rows() == 0){
									// jika blm ada data di tm_stok_hasil_jahit_warna, insert
									$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
									if($seq_stokwarna->num_rows() > 0) {
										$seq_stokwarnarow	= $seq_stokwarna->row();
										$id_stok_warna	= $seq_stokwarnarow->id+1;
									}else{
										$id_stok_warna	= 1;
									}
									
									$data_stok = array(
										'id'=>$id_stok_warna,
										'id_stok_hasil_jahit'=>$id_stok,
										'id_warna_brg_jadi'=>'0',
										'kode_warna'=>$rowxx3->kode_warna,
										'stok'=>$totalxx,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '".$totalxx."', 
									tgl_update_stok = '$tgl' 
									where kode_warna= '".$rowxx3->kode_warna."' AND id_stok_hasil_jahit='$id_stok' ");
								}
					
							} //end for3
						} // end if3

						
					} // end for2
				} // end if2
				
			} // end for1
		} // end if1
		echo "update stok hasil jahit sukses";
  }

}
