<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmmasukquilting/mmaster');
  }
  
  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}

	$date_from = $this->input->post('date_from', TRUE);  
	$date_to = $this->input->post('date_to', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	
	if ($proses_submit == "Proses") {
		$data['date_from']	= $date_from;
		$data['date_to']	= $date_to;
		$data['bonm_detail'] = $this->mmaster->get_detail_bonm($date_from, $date_to); 
		$data['go_proses'] = '1';
		$data['msg'] = '';
		
		$data['isi'] = 'bonmmasukquilting/vmainform';
		$this->load->view('template',$data);
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['isi'] = 'bonmmasukquilting/vmainform';
		$this->load->view('template',$data);
	}
  }
  
  function edit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_apply_stok 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_bonm($id_apply_stok);
	$data['msg'] = '';
	$data['id_apply_stok'] = $id_apply_stok;

	$data['isi'] = 'bonmmasuk/veditform';
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	  $tgl = date("Y-m-d");
		$jumdetail 	= $this->input->post('jumdetail', TRUE);
		$no_bonm 	= $this->input->post('no_bonm', TRUE);
		$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);
		//$id_gudang 	= $this->input->post('id_gudang', TRUE);
		
		$pisah1 = explode("-", $tgl_bonm);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
	// ======================================================================
	// NEW 16 AGUSTUS 2011
	
	$cek_data = $this->mmaster->cek_data($no_bonm);
	if (count($cek_data) > 0) { 
		$data['msg'] = "Data no Bon M ".$no_bonm." sudah ada..!";
		$data['go_proses'] = '';
		//$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'bonmmasukquilting/vmainform';
		$this->load->view('template',$data);
	}
			
	for ($i=1; $i<=$jumdetail; $i++) {
		//echo $this->input->post('cek_'.$i, TRUE)."<br>";
		if ($this->input->post('cek_'.$i, TRUE) == 'y') {
				$id_detail = $this->input->post('id_detail_'.$i, TRUE);
				$tgl_sj = $this->input->post('tgl_sj_'.$i, TRUE);
				$id_sj_hasil_makloon = $this->input->post('id_sj_hasil_makloon_'.$i, TRUE);
				$kode_unit = $this->input->post('kode_unit_'.$i, TRUE);
				$kode_brg = $this->input->post('kode_brg_'.$i, TRUE);
				$qty = $this->input->post('qty_'.$i, TRUE);
				$harga = $this->input->post('harga_'.$i, TRUE);
				
				if ($harga > 0) {
					// cek apakah no bonm sudah ada datanya. jika sudah ada, maka ga perlu ganti no bon M
					$query3	= $this->db->query(" SELECT no_bonm FROM tm_sj_hasil_makloon_detail 
											WHERE id = '$id_detail' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$bonm	= $hasilrow->no_bonm;
						if ($bonm == '') {
						/*	$sql = "UPDATE tm_sj_hasil_makloon_detail SET no_bonm = '$bonm', tgl_bonm = '$tgl_bonm'
										WHERE id = '$id_detail'"; echo $sql; die(); */
							$this->db->query(" UPDATE tm_sj_hasil_makloon_detail SET no_bonm = '$no_bonm', tgl_bonm = '$tgl_bonm'
										WHERE id = '$id_detail' ");
						}
					}
					
					// update statusnya!
					$this->db->query(" UPDATE tm_sj_hasil_makloon_detail SET status_stok = 't', 
										 tgl_update_stok = '$tgl' WHERE id = '$id_detail' ");
					
					//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
					$query3	= $this->db->query(" SELECT id FROM tm_sj_hasil_makloon_detail WHERE status_stok = 'f' 
										AND id_sj_hasil_makloon = '$id_sj_hasil_makloon' ");
					if ($query3->num_rows() == 0) {
						$this->db->query(" UPDATE tm_sj_hasil_makloon SET status_stok = 't' WHERE id= '$id_sj_hasil_makloon' ");
					}
									
					//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg = '$kode_brg' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$qty;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_makloon, insert
							$data_stok = array(
								'kode_brg'=>$kode_brg,
								'stok'=>$new_stok,
							//	'id_gudang'=>$id_gudang, //
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_makloon',$data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$kode_brg' ");
						}
						
					// 14-02-12, cek di tm_stok_harga apakah ada data harga yg 0 dan cuma 1 data. kalo ada, maka update data stok harganya pake harga yg ini
					$query3	= $this->db->query(" SELECT id FROM tm_stok_harga WHERE kode_brg_quilting = '$kode_brg' 
											AND harga = '0' AND quilting = 't' ");
					if ($query3->num_rows() == 1){
						$hasilrow = $query3->row();
						$id_stok_harga = $hasilrow->id;
						$this->db->query(" UPDATE tm_stok_harga SET harga = '$harga' WHERE id = '$id_stok_harga' ");
					}
					// &&&&&&&&&&&& end 14-02-12 &&&&&&&&&&&&&&&&&
						
						// 14 juni 2011, ada tabel baru, yaitu tm_stok_harga (utk membedakan stok berdasarkan harga)
						// 16-02-2012: warning, tabel tt_stok udh ga akan dipake lagi utk rekap stok
								//cek stok terakhir tm_stok_harga, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg_quilting = '$kode_brg' 
											AND quilting = 't' AND harga = '$harga' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty; // utk save ke tt_stok_hasil_makloon, $new_stok ini yg dipake karena per harga
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_harga, insert
									$data_stok = array(
										'kode_brg_quilting'=>$kode_brg,
										'quilting'=>'t',
										'stok'=>$new_stok,
										'harga'=>$harga,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_harga',$data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_quilting= '$kode_brg' AND harga = '$harga' ");
								}
						// #########################################################################################
						/*$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_makloon (kode_brg, kode_unit, no_bukti, masuk, saldo, tgl_input, harga) 
											VALUES ('$kode_brg', '$kode_unit', '$no_bonm', '$qty', '$new_stok', '$tgl_sj', '$harga' ) "); */
				} // end if ($harga > 0)
		}
	} // end for
	redirect('bonmmasukquilting/cform/view');
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'bonmmasukquilting/vformview';
    $keywordcari = "all";
    //$cgudang = '0';
	//$kode_bagian = '';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasukquilting/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '5';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['list_gudang'] = $this->mmaster->get_gudang();
	//$data['cgudang'] = $cgudang;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$cgudang = $this->input->post('gudang', TRUE);  
	
	if ($keywordcari == '' && $cgudang == '') {
		$cgudang 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($cgudang == '')
		$cgudang = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($cgudang, $keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk/cform/cari/'.$cgudang.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '5';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $cgudang, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'bonmmasuk/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['cgudang'] = $cgudang;
	$this->load->view('template',$data);
  }


  function batal(){
    $no_bonm 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $cari 	= $this->uri->segment(7);
    
    $no_bonmback = str_replace("gm","/",$no_bonm);
    $this->mmaster->delete($no_bonmback);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "bonmmasukquilting/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "bonmmasukquilting/cform/cari/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
  }
  
  // 14-01-2013
  function editdatastatusstok(){
	$tgl = date("Y-m-d");
    $query	= $this->db->query(" SELECT a.no_sj, a.kode_supplier, b.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
					WHERE a.id = b.id_apply_stok AND a.status_stok = 'f' AND a.status_aktif = 't'
					AND b.status_stok = 'f' AND a.stok_lain = 'f'
					ORDER BY a.kode_supplier, a.id DESC, b.id ASC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		$temp_id_pembelian_detail = 0;
		$jmldata = 0;
		foreach($hasil as $row1) {
				//ambil tgl SJ dan id pembelian detail							
				$query3	= $this->db->query(" SELECT a.id AS id_pembelian, a.tgl_sj, b.id AS id_detail FROM tm_pembelian a, tm_pembelian_detail b 
							WHERE a.id = b.id_pembelian AND a.no_sj = '$row1->no_sj' 
							AND a.kode_supplier = '$row1->kode_supplier' AND b.kode_brg = '$row1->kode_brg' 
							AND b.qty='$row1->qty' AND b.status_stok = 'f' AND a.status_aktif = 't' ORDER BY b.id DESC ");
				 if ($query3->num_rows() > 0) {
					 if ($query3->num_rows() == 1) {
						$hasilrow = $query3->row();
						$tgl_sj	= $hasilrow->tgl_sj;
						$id_pembelian	= $hasilrow->id_pembelian;
					 	$id_pembelian_detail	= $hasilrow->id_detail;
					 }
					 else if ($query3->num_rows() > 1) {
						 /* misalnya ada 3 data kode brg yg sama dan qty yg sama*/
						 $hasil3 = $query3->result();
						 foreach ($hasil3 as $row3) {
							 if ($row3->id_detail != $temp_id_pembelian_detail) {
								$temp_id_pembelian_detail = $row3->id_detail;
								$id_pembelian = $temp_id_pembelian_detail;
								$tgl_sj	= $row3->tgl_sj;
								$id_pembelian_detail = $row3->id_detail;
								break;
						     }
						 } // sampe sini 04:01 selasa
					 }
				 }
				 else {
					$tgl_sj = '';
					$id_pembelian = '0';
					$id_pembelian_detail = '0';
				}
				if ($tgl_sj!='') {
					$thnsj = substr($tgl_sj, 0, 4);
				} else
					$thnsj = '0';
				
				//salah
				 /* if (strtotime($tgl_sj) <= strtotime("2012-12-31"))
				{ echo "Less"; 
				}else
					echo "greater";
				*/
				//echo $tgl_sj." ";
				if ($thnsj <= 2012) {
					$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET no_bonm = 'xxx150113', tgl_bonm = '2012-12-31'
											WHERE id = '$row1->id' ");
					
					// update statusnya!
					$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET status_stok = 't', 
										 tgl_update = '$tgl' WHERE id = '$row1->id' ");
					
					//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
					$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE status_stok = 'f' 
										AND id_apply_stok = '$row1->id_apply_stok' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" UPDATE tm_apply_stok_pembelian SET status_stok = 't' WHERE id= '$row1->id_apply_stok' ");
					}
					
					// update lagi di tm_pembelian_detail
					$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 't' 
								WHERE id = '$id_pembelian_detail' ");
								
					//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
					$query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE status_stok = 'f' 
										AND id_pembelian = '$id_pembelian' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" UPDATE tm_pembelian SET status_stok = 't' WHERE id = '$id_pembelian' ");
					}
					$jmldata++;
				} // end if
			/*	$headernya[] = array(		
											'id'=> $row1->id,
											'id_apply_stok'=> $row1->id_apply_stok,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'id_pembelian'=> $id_pembelian,
											'id_pembelian_detail'=> $id_pembelian_detail,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row1->qty,
											'harga'=> $row1->harga,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm
									); */
		} // end foreach
		echo $jmldata;
	}
    // =====================================================================================
  }
  
//  function editdatastatusstok() {
	  /*
	   * select b.* from tm_apply_stok_pembelian a, tm_pembelian b, tm_pembelian_detail c, 
tm_apply_stok_pembelian_detail d where a.no_sj = b.no_sj AND a.id=d.id_apply_stok
AND b.id = c.id_pembelian AND c.kode_brg = d.kode_brg
 AND b.status_stok = 'f'
AND b.tgl_sj <='2012-12-01'
	   * 
	   * */  
  //}
  
}
