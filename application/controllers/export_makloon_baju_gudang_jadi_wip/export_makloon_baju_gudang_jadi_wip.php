<?php
class export_makloon_baju_gudang_jadi_wip extends CI_Controller
{
    public $data = array(
        'halaman' => 'export_makloon_baju_gudang_jadi_wip',        
        'title' => 'Export Makloon Baju Gudang Jadi Wip',
        'isi' => 'export_makloon_baju_gudang_jadi_wip/export_makloon_baju_gudang_jadi_wip_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('export_makloon_baju_gudang_jadi_wip/export_makloon_baju_gudang_jadi_wip_model', 'export_makloon_baju_gudang_jadi_wip');
    }

   
    public function index()
    {
		$no=$this->input->post('no');
		
        $this->data['values'] = (object) $this->export_makloon_baju_gudang_jadi_wip->default_values;
        $this->data['unit_jahit'] =  $this->export_makloon_baju_gudang_jadi_wip->get_unit_jahit();
        $this->data['unit_packing'] =  $this->export_makloon_baju_gudang_jadi_wip->get_unit_packing();
        $this->data['gudang'] =  $this->export_makloon_baju_gudang_jadi_wip->get_gudang();
        $this->data['no'] = $no;
		$this->load->view('template', $this->data);
			
    }
    public function view($offset=null)
    {
		$no=$this->input->post('no');
		$tanggal_sj_dari = $this->input->post('tanggal_sj_dari',TRUE);
		$tanggal_sj_ke = $this->input->post('tanggal_sj_ke',TRUE);
		$jenis_masuk = $this->input->post('jenis_masuk',TRUE);
		$unit_jahit = $this->input->post('unit_jahit',TRUE);
		$unit_packing = $this->input->post('unit_packing',TRUE);
		$gudang = $this->input->post('gudang',TRUE);
		
		$export_makloon_baju_gudang_jadi_wip = $this->export_makloon_baju_gudang_jadi_wip->get_all_inner_paged($offset,$tanggal_sj_dari,$tanggal_sj_ke,$jenis_masuk,$unit_jahit,$unit_packing,$gudang);
		
		if ($export_makloon_baju_gudang_jadi_wip) {
            $this->data['export_makloon_baju_gudang_jadi_wip'] = $export_makloon_baju_gudang_jadi_wip;  
        } else {
            $this->data['export_makloon_baju_gudang_jadi_wip'] = 'Tidak ada data Makloon Baju Gudang Jadi Wip, Silahkan Melakukan '.anchor('/export_makloon_baju_gudang_jadi_wip/export_makloon_baju_gudang_jadi_wip/view', 'Pencarian kembali.', 'class="alert-link"');
        }
		$this->data['tanggal_sj_dari'] = $tanggal_sj_dari;
		$this->data['tanggal_sj_ke'] = $tanggal_sj_ke;
		$this->data['jenis_masuk'] = $jenis_masuk;
		$this->data['unit_jahit'] = $unit_jahit;
		$this->data['unit_packing'] = $unit_packing;
		$this->data['gudang'] = $gudang;
		
		$this->data['isi'] = 'export_makloon_baju_gudang_jadi_wip/export_makloon_baju_gudang_jadi_wip_list';
		$this->load->view('template', $this->data);		
    }
    
     public function export_csv($offset=null)
    {
		$tanggal_sj_dari = $this->input->post('tanggal_sj_dari',TRUE);
		$tanggal_sj_ke = $this->input->post('tanggal_sj_ke',TRUE);
		$jenis_masuk = $this->input->post('jenis_masuk',TRUE);
		$unit_jahit = $this->input->post('unit_jahit',TRUE);
		$unit_packing = $this->input->post('unit_packing',TRUE);
		$gudang = $this->input->post('gudang',TRUE);
		
		$query = $this->export_makloon_baju_gudang_jadi_wip->get_all_inner_paged($offset,$tanggal_sj_dari,$tanggal_sj_ke,$jenis_masuk,$unit_jahit,$unit_packing,$gudang);
		
		$nama = $this->session->userdata('nama');
		$today = date("YmdHis");  
		$filename = "export_makloon_baju_gudang_jadi_wip_".$nama."_".$today.".csv";
		

	$now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");

		$out = fopen('php://output', 'w');
		if (is_array($query)) {
			foreach( $query as $row){
			
				fputcsv($out, array($row->id_header,$row->no_sj,$row->jenis_masuk,$row->id_unit_jahit,$row->id_unit_packing,
			$row->jenis_pembelian,$row->status_pembelian,$row->status_aktif_header,$row->status_edit_header,$row->created_at_header,
			$row->created_by_header,$row->updated_at_header,$row->updated_by_header,$row->keterangan_header,$row->id_gudang,$row->tanggal_sj,
			
			$row->id_detail,$row->id_makloon_baju_gudang_jadi_wip,$row->id_barang_wip,$row->qty,$row->harga,$row->diskon,
			$row->status_pembelian_detail,$row->keterangan_detail,$row->created_at_detail,$row->created_by_detail,
			$row->updated_at_detail,$row->updated_by_detail,$row->status_aktif_detail,));
				}
		} // end foreach
		
		fclose($out);
	
	die();
	$this->data['isi'] = 'export_makloon_baju_gudang_jadi_wip/export_makloon_baju_gudang_jadi_wip_list';
	$this->load->view('template', $this->data);		
	}
	
    
     
}

