<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('stok-opname-hsl-packing/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['isi'] = 'stok-opname-hsl-packing/vmainform';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	//$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);

	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

	$cek_data = $this->mmaster->cek_so($bulan, $tahun); 
	//print_r($cek_data); die();
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, maka ambil stok terkini dari tabel brg jadi
		$data['query'] = $this->mmaster->get_all_stok();
	
		$data['is_new'] = '1';
		if (is_array($data['query']) )
			$data['jum_total'] = count($data['query']);
		else
			$data['jum_total'] = 0;
		
		$data['isi'] = 'stok-opname-hsl-packing/vformview';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
			$data['isi'] = 'stok-opname-hsl-packing/vmainform';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_ yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname($bulan, $tahun);
	
			$data['is_new'] = '0';
			$data['jum_total'] = count($data['query']);
			$data['isi'] = 'stok-opname-hsl-packing/vformview';
			$this->load->view('template',$data);
			
		}
	} // end else

  }
  
  function submit() {
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $no = $this->input->post('no', TRUE);  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d"); 
	  
	  if ($is_new == '1') {
	      // insert ke tabel tt_stok_opname_
	      $data_header = array(
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
		  $this->db->insert('tt_stok_opname_hasil_packing',$data_header);
		  
		  // ambil data terakhir di tabel tt_stok_opname_
		 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_packing ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id; 
	      
	      for ($i=1;$i<=$no;$i++)
		  {
			 $this->mmaster->save($is_new, $id_stok, 
			 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE));
		  }
		  
		  redirect('stok-opname-hsl-packing/cform');
	  }
	  else {
		  // update ke tabel tt_stok_opname_
		   
		   // ambil data terakhir di tabel tt_stok_opname_
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_packing WHERE bulan = '$bulan' 
							AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 $this->db->query(" UPDATE tt_stok_opname_hasil_packing SET tgl_update = '$tgl' 
						where id = '$id_stok' ");
			 
		  for ($i=1;$i<=$no;$i++)
		  {
			 $this->mmaster->save($is_new, $id_stok, 
			 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE));
		  }
		  
		  redirect('stok-opname-hsl-packing/cform');
      }
  }
}
