<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
			$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
			$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
			$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
			$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
			$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
			$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
			$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');		
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['lcolor']	= "";
		
			$this->load->model('brgjadiplusmotif/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] = base_url().'index.php/brgjadiplusmotif/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']	= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
		
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['opt_color']	= $this->mclass->icolor();
		
			$data['isi']='brgjadiplusmotif/vmainform';
			$this->load->view('template',$data);
			
		}

function tambah() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
			$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
			$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
			$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
			$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
			$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
			$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
			$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');		
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['lcolor']	= "";
		
			$this->load->model('brgjadiplusmotif/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] = base_url().'index.php/brgjadiplusmotif/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']	= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
		
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['opt_color']	= $this->mclass->icolor();
		
			$data['isi']='brgjadiplusmotif/vmainformadd';
			$this->load->view('template',$data);
			
		}
	//~ 
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
		$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
		$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
		$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
		$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
		$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
		$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');		
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi');		
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$data['lcolor']	= "";
		
		$this->load->model('brgjadiplusmotif/mclass');
		$query	= $this->mclass->view();
		
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/brgjadiplusmotif/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['opt_color']	= $this->mclass->icolor();
		
		$data['isi']='brgjadiplusmotif/vmainform';	
		$this->load->view('template',$data);	
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$this->load->view('brgjadiplusmotif/vmainform',$data);
	}

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['uri']	= $this->uri->segment(4);
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']	= "";
		$data['list']	= "";
		$data['lurl']	= base_url();

		$this->load->model('brgjadiplusmotif/mclass');
		
		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'/index.php/brgjadiplusmotif/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('brgjadiplusmotif/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['uri']	= $this->uri->segment(4);
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']	= "";
		$data['list']	= "";
		$data['lurl']	= base_url();

		$this->load->model('brgjadiplusmotif/mclass');
		$this->load->library('pagination');
		
		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'/index.php/brgjadiplusmotif/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('brgjadiplusmotif/vlistformbrgjadi',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproduct		= $this->input->post('i_product2');
		$iproductmotif	= $this->input->post('i_product_motif');
		$nquantity		= $this->input->post('n_quantity');
		$color			= $this->input->post('icolor');
		$stupperiproduct		= strtoupper($iproduct);
		$stuppereproductmotif	= strtoupper($iproductmotif);
		
		if(!empty($iproduct) &&
		   !empty($iproductmotif) && 
		   $color!=0
		) {
			$this->load->model('brgjadiplusmotif/mclass');
			
			$qecolor = $this->mclass->ecolor($color);
			$recolor = $qecolor->row();
			
			$this->mclass->msimpan($stupperiproduct,$stuppereproductmotif,$nquantity,$color,$recolor->e_color_name);
		} else {
			$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
			$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
			$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
			$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
			$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
			$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
			$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');		
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			$data['lcolor']	= "";
			
			$this->load->model('brgjadiplusmotif/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] = base_url().'index.php/brgjadiplusmotif/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']	= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['opt_color']	= $this->mclass->icolor();
			
			$data['isi']='brgjadiplusmotif/vmainform';	
		$this->load->view('template',$data);	
					
		}
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4);
		$data['id']	= $id;

		$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
		$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
		$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
		$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
		$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
		$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
		$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');		
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_cari']	= $this->lang->line('button_cari');
		$data['text_cari']		= $this->lang->line('text_cari');
		$data['link_aksi']		= $this->lang->line('link_aksi');		
		
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$data['lcolor']		= "";
		
		$nquantity	= $this->input->post('n_quantity')?$this->input->post('n_quantity'):$this->input->get_post('n_quantity');
		
		$this->load->model('brgjadiplusmotif/mclass');
		$data['opt_color']	= $this->mclass->icolor();
		$qry_brgmotif	= $this->mclass->medit($id);
		
		if($qry_brgmotif->num_rows()>0) {
		
			$row_brgmotif	= $qry_brgmotif->row();
			
			$data['i_product_motif']	= $row_brgmotif->i_product_motif;
			$data['i_product']	= $row_brgmotif->i_product;
			$data['n_quantity']	= $row_brgmotif->n_quantity;
			$data['n_active']	= $row_brgmotif->n_active;
			$data['COL']		= $row_brgmotif->i_color;
			$data['isi'] = 'brgjadiplusmotif/veditform';
			$this->load->view('template',$data);
		}
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproduct		= $this->input->post('i_product2');
		$iproductmotif	= $this->input->post('i_product_motif');
		$nquantity		= $this->input->post('n_quantity');
		$color	= $this->input->post('icolor');
		
		if(!empty($iproduct) &&
		   !empty($iproductmotif) && ($nquantity > 0 || $nquantity=='0')) {
			
			$this->load->model('brgjadiplusmotif/mclass');
				
			$qecolor = $this->mclass->ecolor($color);
			$recolor = $qecolor->row();
			
			$this->mclass->mupdate($iproduct,$iproductmotif,$nquantity,$color,$recolor->e_color_name);		
		} else {
			$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
			$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
			$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
			$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');
			$data['form_qty_brgmotif']		= $this->lang->line('form_qty_brgmotif');
			$data['form_color_brgmotif']	= $this->lang->line('form_color_brgmotif');
			$data['form_pilih_color_brgmotif']	= $this->lang->line('form_pilih_color_brgmotif');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['text_cari']		= $this->lang->line('text_cari');
			$data['link_aksi']		= $this->lang->line('link_aksi');		
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			$data['lcolor']	= "";
			
			$this->load->model('brgjadiplusmotif/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			$result	= $query->result();
			
			$pagination['base_url'] = base_url().'index.php/brgjadiplusmotif/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']	= 50;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			$data['opt_color']	= $this->mclass->icolor();
					
			$this->load->view('brgjadiplusmotif/vmainform',$data);
		}
	}
	
	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4);
		$this->load->model('brgjadiplusmotif/mclass');
		$this->mclass->delete($id);
	}
	
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->input->get_post('cari');
		$tocari	= strtoupper($cari);
		
		$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
		$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
		$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
		$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');

		$data['link_aksi']		= $this->lang->line('link_aksi');		
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('brgjadiplusmotif/mclass');
		$query	= $this->mclass->viewcari($tocari);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = base_url().'index.php/brgjadiplusmotif/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($tocari,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('brgjadiplusmotif/vcariform',$data);
		
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->input->get_post('cari');
		$tocari	= strtoupper($cari);
		
		$data['form_title_brgmotif']	= $this->lang->line('form_title_brgmotif');
		$data['form_kd_brg_brgmotif']	= $this->lang->line('form_kd_brg_brgmotif');
		$data['form_kd_motif_brgmotif']	= $this->lang->line('form_kd_motif_brgmotif');
		$data['form_nm_motif_brgmotif']	= $this->lang->line('form_nm_motif_brgmotif');

		$data['link_aksi']		= $this->lang->line('link_aksi');		
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('brgjadiplusmotif/mclass');
		$query	= $this->mclass->viewcari($tocari);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/brgjadiplusmotif/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 50;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($tocari,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('brgjadiplusmotif/vcariform',$data);
	}

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$tokey	= strtoupper($key);
		
		$iterasi	= $this->uri->segment(4,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI";
		$data['isi']	= "";
		$data['list']	= "";
		$data['lurl']	= base_url();

		$this->load->model('brgjadiplusmotif/mclass');
		
		$query	= $this->mclass->flbarangjadi($tokey);
		$jml	= $query->num_rows();
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 	
			foreach($query->result() as $row) {
				$qimotif	= $this->db->query(" SELECT i_product_motif AS iproductmotif, n_quantity AS qty FROM tr_product_motif WHERE i_product='$row->iproductname' ORDER BY i_product_motif DESC LIMIT 1 ");
				$row2	= $qimotif->row();
							
				$iproduct	= substr($row2->iproductmotif,7,2);
				$iproductbase	= substr($row2->iproductmotif,0,7);
				if(substr($iproduct,0,1)==0) {
					$c_iproduct	= $iproduct+1;
					$new_iproduct	= '0'.$c_iproduct;
					$iproductmotif	= $iproductbase.$new_iproduct;
				} else {
					$new_iproduct	= $iproduct+1;
					$iproductmotif	= $iproductbase.$new_iproduct;
				}
				
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:setTextfield('$row->iproductname','$iproductmotif','$row2->qty')\">".$row->iproductname."</a></td>	 
				  <td><a href=\"javascript:setTextfield('$row->iproductname','$iproductmotif','$row2->qty')\">".$row->productname."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
		
		echo $item;
	}
	
}
?>
