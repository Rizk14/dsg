<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('set-stok-harga/mmaster');
  }
  
  //27-03-2012, skrip tersembunyi utk mengupdate stok keseluruhan dan stok berdasarkan harga
  // defaultnya itu harga = 0, stok = 2000
  function setstokharganol() {
	  $tgl = date("Y-m-d");
	  
	  $sql = " SELECT a.kode_brg, a.nama_brg, b.nama FROM tm_barang a, tm_satuan b where a.satuan = b.id
			AND a.status_aktif = 't' AND a.kode_brg NOT IN (select kode_brg FROM tm_stok) 
			UNION SELECT a.kode_brg, a.nama_brg, b.nama FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id
			AND a.status_aktif = 't' AND a.kode_brg NOT IN (select kode_brg FROM tm_stok_hasil_makloon)
			ORDER BY kode_brg "; //echo $sql; die();
	$query	= $this->db->query($sql);
	
	$data_brg = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			/*$pos = strpos($row1->nama_brg, "\"");
			  if ($pos > 0)
				$nama_brg_konv = str_replace("\"", "&quot;", $row1->nama_brg);
			  else
				$nama_brg_konv = str_replace("'", "\'", $row1->nama_brg); 
			
			$data_brg[] = array(			
								'kode_brg'=> $row1->kode_brg,
								'nama_brg'=> $nama_brg_konv,
								'satuan'=> $row1->nama
							); */
			
			$query3	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg = '".$row1->kode_brg."' ");
			if ($query3->num_rows() == 0){
				$quilting = 't';
			}
			else
				$quilting = 'f';
					
			if ($quilting == 'f') {
				$tm_stok = "tm_stok";
				//$tt_stok = "tt_stok";
			}
			else {
				$tm_stok = "tm_stok_hasil_makloon";
				//$tt_stok = "tt_stok_hasil_makloon";
			}
					
			// tm_stok
			$data = array(
					  'kode_brg'=>$row1->kode_brg,
					  'stok'=>'2000',
					  'tgl_update_stok'=>$tgl
					);
			$this->db->insert($tm_stok, $data); 
										
			if ($quilting == 'f') {
				$data = array(
					'kode_brg'=>$row1->kode_brg,
					'stok'=>'2000',
					'harga'=>'0',
					'tgl_update_stok'=>$tgl
				);
				$this->db->insert('tm_stok_harga',$data); 
			}
			else {
				$data = array(
					'kode_brg_quilting'=>$row1->kode_brg,
					'stok'=>'2000',
					'harga'=>'0',
					'tgl_update_stok'=>$tgl,
					'quilting'=>$quilting
					);
				$this->db->insert('tm_stok_harga',$data); 
			}
			
		} // end foreach
		echo "sukses";
	}
  }
  // end skrip
  
  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['supplier'] = $this->mmaster->get_supplier();
	$data['list_brg'] = $this->mmaster->get_barang_stok_zero();
    $data['isi'] = 'set-stok-harga/vmainform';
	$this->load->view('template',$data);

  }
    
  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  
		$jum = $this->input->post('jum_data', TRUE);
		$tgl = date("Y-m-d");

		for ($i=1; $i<=$jum; $i++)
		{
			if ($this->input->post('cek_'.$i, TRUE) == 'y') {
				//if ($this->input->post('stok_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0) {
					
					$query3	= $this->db->query(" SELECT * FROM tm_barang WHERE 
										kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
					if ($query3->num_rows() == 0){
						$quilting = 't';
					}
					else
						$quilting = 'f';
					
					if ($quilting == 'f') {
						$tm_stok = "tm_stok";
						$tt_stok = "tt_stok";
					}
					else {
						$tm_stok = "tm_stok_hasil_makloon";
						$tt_stok = "tt_stok_hasil_makloon";
					}
					
					// tm_stok
					$data = array(
					  'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
					  'stok'=>$this->input->post('stok_'.$i, TRUE),
					  'tgl_update_stok'=>$tgl
					);
					$this->db->insert($tm_stok, $data); 
					
					// tt_stok
					$data = array(
					  'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
					  'no_bukti'=>"SA",
					  'masuk'=>$this->input->post('stok_'.$i, TRUE),
					  'saldo'=>$this->input->post('stok_'.$i, TRUE),
					  'harga'=>$this->input->post('harga_'.$i, TRUE),
					  'tgl_input'=>$tgl
					);
					$this->db->insert($tt_stok, $data); 
					
					if ($quilting == 'f') {
						$data = array(
						  'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
						  'stok'=>$this->input->post('stok_'.$i, TRUE),
						  'harga'=>$this->input->post('harga_'.$i, TRUE),
						  'tgl_update_stok'=>$tgl
						);
						$this->db->insert('tm_stok_harga',$data); 
					}
					else {
						$data = array(
						  'kode_brg_quilting'=>$this->input->post('kode_'.$i, TRUE),
						  'stok'=>$this->input->post('stok_'.$i, TRUE),
						  'harga'=>$this->input->post('harga_'.$i, TRUE),
						  'tgl_update_stok'=>$tgl,
						  'quilting'=>$quilting
						);
						$this->db->insert('tm_stok_harga',$data); 
					}
					
					// harga brg supplier utk non quilting
					if ($quilting == 'f') {
						$query3	= $this->db->query(" SELECT id FROM tm_harga_brg_supplier WHERE 
											kode_brg = '".$this->input->post('kode_'.$i, TRUE)."'
											AND kode_supplier = '".$this->input->post('supplier_'.$i, TRUE)."' ");
						if ($query3->num_rows() == 0){
							$data = array(
							  'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
							  'kode_supplier'=>$this->input->post('supplier_'.$i, TRUE),
							  'harga'=>$this->input->post('harga_'.$i, TRUE),
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl
							);
							$this->db->insert('tm_harga_brg_supplier',$data); 
						}
						else {
							$hasilrow = $query3->row();
							$id_harga	= $hasilrow->id;
							$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '".$this->input->post('harga_'.$i, TRUE)."',
										tgl_update = '$tgl' WHERE id = '$id_harga' ");
						}
					} // end if 
					else { // utk bhn quilting
						$query3	= $this->db->query(" SELECT id FROM tm_harga_quilting WHERE 
											kode_brg_quilting = '".$this->input->post('kode_'.$i, TRUE)."'
											AND kode_unit = '".$this->input->post('supplier_'.$i, TRUE)."' ");
						if ($query3->num_rows() == 0){
							$data = array(
							  'kode_brg_quilting'=>$this->input->post('kode_'.$i, TRUE),
							  'kode_unit'=>$this->input->post('supplier_'.$i, TRUE),
							  'harga'=>$this->input->post('harga_'.$i, TRUE),
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl
							);
							$this->db->insert('tm_harga_quilting',$data); 
						}
						else {
							$hasilrow = $query3->row();
							$id_harga	= $hasilrow->id;
							$this->db->query(" UPDATE tm_harga_quilting SET harga = '".$this->input->post('harga_'.$i, TRUE)."',
										tgl_update = '$tgl' WHERE id = '$id_harga' ");
						}
					}
				//}
			}
		}

		//redirect('set-stok-harga/cform');
		$data['isi'] = 'set-stok-harga/vformsukses';
		$this->load->view('template',$data);
		
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'mst-bb-sup/vformview';
    $keywordcari = "all";
    $supplier = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $supplier);	
							$config['base_url'] = base_url().'index.php/mst-bb-sup/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $supplier);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['esupplier'] = $supplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$supplier 	= $this->input->post('supplier', TRUE);  
	//echo $supplier; die();
	if ($keywordcari == '' && $supplier == '') {
		$supplier 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($supplier == '')
		$supplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $supplier);
							$config['base_url'] = base_url().'index.php/mst-bb-sup/cform/cari/index/'.$supplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $supplier);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'mst-bb-sup/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['esupplier'] = $supplier;
	$this->load->view('template',$data);
  }
  
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg 	= $this->uri->segment(4);
	if ($kel_brg == '')
		$kel_brg = $this->input->post('kel_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $kel_brg == '') {
		$kel_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barangtanpalimit($kel_brg, $keywordcari);
					$config['base_url'] = base_url()."index.php/mst-bb-sup/cform/show_popup_jenis/".$kel_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang($config['per_page'],$this->uri->segment(6), $kel_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['kel_brg'] = $kel_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('mst-bb-sup/vpopupjenis',$data);

  }
  
  function show_popup_jenis_bhn(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jns_brg 	= $this->uri->segment(4);
	if ($jns_brg == '')
		$jns_brg = $this->input->post('jns_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $jns_brg == '') {
		$jns_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_bahantanpalimit($jns_brg, $keywordcari);
							$config['base_url'] = base_url()."index.php/mst-bb-sup/cform/show_popup_jenis_bhn/".$jns_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_bahan($config['per_page'],$this->uri->segment(6), $jns_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['jns_brg'] = $jns_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		$hasilrow = $query3->row();
		$nama_jns_brg	= $hasilrow->nama;
	$data['nama_jns_brg'] = $nama_jns_brg;

	$this->load->view('mst-bb-sup/vpopupjenisbhn',$data);

  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $esupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "mst-bb-sup/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-bb-sup/cform/cari/index/".$esupplier."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('mst-bb-sup/cform/view');
  }
  
  // 18-11-2014 SET STOK AWAL PERTAMA KALI
  function addstokawal(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$th_now	= date("Y");

	$data['isi'] = 'set-stok-harga/vform1stokawal';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);
  }
  
  function show_popup_bhnbaku_stokawal(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$gudang 	= $this->input->post('id_gudang', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '' && $gudang == '') {
			$gudang 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
	// +++++++++++++++

		$qjum_total = $this->mmaster->get_bhnbaku_stokawaltanpalimit($gudang, $keywordcari);
		
				$config['base_url'] = base_url()."index.php/set-stok-harga/cform/show_popup_bhnbaku_stokawal/".$gudang."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bhnbaku_stokawal($config['per_page'],$config['cur_page'], $gudang, $keywordcari);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	
	$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$kode_gudang = '';
		$nama_gudang = '';
	}
	$data['gudang'] = $gudang;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('set-stok-harga/vpopupbhnbakustokawal',$data);
  }
  
  function submitstokawal(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
		$id_gudang 	= $this->input->post('gudang', TRUE);  
		$no 	= $this->input->post('no', TRUE);
		$jumlah_input=$no-1;
			
		for ($i=1;$i<=$jumlah_input;$i++)
		{
			$this->mmaster->savestokawal($id_gudang, $this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
						$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE));
		}
		redirect('set-stok-harga/cform/viewstokawal');
  }
  
  function viewstokawal(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '')
			$keywordcari = $this->uri->segment(4);
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$jum_total = $this->mmaster->getAllstokawaltanpalimit($keywordcari);
								$config['base_url'] = base_url().'index.php/set-stok-harga/cform/viewstokawal/'.$keywordcari.'/';
								$config['total_rows'] = count($jum_total); 
								$config['per_page'] = '20';
								$config['first_link'] = 'Awal';
								$config['last_link'] = 'Akhir';
								$config['next_link'] = 'Selanjutnya';
								$config['prev_link'] = 'Sebelumnya';
								$config['cur_page'] = $this->uri->segment(5);
								$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllstokawal($config['per_page'],$this->uri->segment(5), $keywordcari);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['startnya'] = $config['cur_page'];
		
		$data['isi'] = 'set-stok-harga/vformviewstokawal';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		
		$this->load->view('template',$data);
  }
  
  // 13-01-2015
  function updatestokbhnbakudobel() {
		$query3	= $this->db->query(" select id, kode_brg, stok FROM tm_stok ORDER BY kode_brg ");
											
		if($query3->num_rows()>0) {
			$hasil3 = $query3->result();
			$temp_kode="";
			foreach ($hasil3 as $row3) {
				if ($temp_kode != $row3->kode_brg) {
					$temp_kode = $row3->kode_brg;
				}
				else {
					$this->db->query(" DELETE FROM tm_stok WHERE kode_brg='".$row3->kode_brg."' AND id <> '".$row3->id."' ");
				}
				
			}
		}
		
		// stok harga
		$query3	= $this->db->query(" select id, kode_brg, stok, harga FROM tm_stok_harga ORDER BY kode_brg ");
											
		if($query3->num_rows()>0) {
			$hasil3 = $query3->result();
			$temp_kode="";
			foreach ($hasil3 as $row3) {
				if ($temp_kode != $row3->kode_brg) {
					$temp_kode = $row3->kode_brg;
				}
				else {
					$this->db->query(" DELETE FROM tm_stok_harga WHERE kode_brg='".$row3->kode_brg."' 
					AND harga='".$row3->harga."' AND id <> '".$row3->id."' ");
				}
				
			}
		}
		
		echo "sukses";
  }
  
  function hapusdataso() {
		$query3	= $this->db->query(" select id FROM tt_stok_opname_bahan_baku ORDER BY id ");
											
		if($query3->num_rows()>0) {
			$hasil3 = $query3->result();
			foreach ($hasil3 as $row3) {
				$query31	= $this->db->query(" select id FROM tt_stok_opname_bahan_baku_detail 
								WHERE id_stok_opname_bahan_baku='".$row3->id."' ORDER BY id ");
				
				if($query31->num_rows()>0) {
					$hasil31 = $query31->result();
					foreach ($hasil31 as $row31) {
						$this->db->query(" DELETE FROM tt_stok_opname_bahan_baku_detail_harga WHERE 
											id_stok_opname_bahan_baku_detail='".$row31->id."' ");
					}
				}
				
				$this->db->query(" DELETE FROM tt_stok_opname_bahan_baku_detail WHERE 
											id_stok_opname_bahan_baku='".$row3->id."' ");
			} // end for1
		} // end if1
		
		$this->db->query(" DELETE FROM tt_stok_opname_bahan_baku ");
		echo "sukses";
  }
  
}
