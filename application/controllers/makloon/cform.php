<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('makloon/mmaster');
  }

  function addopbisbisan(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$no_bonmkeluar = $this->input->post('no_bonmkeluar', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	$id_bonmkeluar_detail = $this->input->post('id_brg', TRUE);  
	$id_supplier = $this->input->post('id_supplier', TRUE);  
	
	$list_brg = explode(";", $id_bonmkeluar_detail);
	//$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
		if ($no_bonmkeluar !='') {
			$data['bonmkeluar_detail'] = $this->mmaster->get_detail_bonmkeluar($list_brg);
			$data['msg'] = '';
			$data['no_bonmkeluar'] = $no_bonmkeluar;
		}
		else {
			$data['msg'] = 'Bon M Keluar harus dipilih';
			$data['no_bonmkeluar'] = '';
			$data['supplier'] = $this->mmaster->get_supplier();
		}
		$data['go_proses'] = '1';
		
		$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_supplier' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) {
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
		}
		else {
			$kode_supplier = '';
			$nama_supplier = '';
		}
			// generate no OP
			$th_now	= date("y");
			$bln_now = date("m");
			$romawi_now =  $this->mmaster->konversi_angka2romawi($bln_now);
			
			// bisbisan, jenis_makloon =1
			$query3	= $this->db->query(" SELECT no_op FROM tm_op_makloon where jenis_makloon = '1' ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_op	= $hasilrow->no_op;
			else
				$no_op = '';
			
			if ($no_op != '') {
				$pisah1 = explode("/", $no_op);
				$nomornya= $pisah1[0];
				$bulannya= $pisah1[1];
				$tahunnya= $pisah1[3];

				$noop_angka = (substr($nomornya, 0, 3))+1;

				if ($th_now == $tahunnya) {
					$jml_n_op = $noop_angka;
					switch(strlen($jml_n_op)) {
						case "1": $kodeop	= "00".$jml_n_op;
						break;
						case "2": $kodeop	= "0".$jml_n_op;
						break;	
						case "3": $kodeop	= $jml_n_op;
						break;
					}
					$nomorop = $kodeop; 
				}
				else {
					$nomorop = "001";
				}
			}	
			else {
				$nomorop	= "001";
			}
			$nomorop = $nomorop."/".$romawi_now."/OPM/".$th_now;
			
			// 15-08-2015
			$data['ukuran_bisbisan'] = $this->mmaster->get_ukuran_bisbisan();

			$data['no_op'] = $nomorop;
			$data['nama_supplier'] = $nama_supplier;
			$data['kode_supplier'] = $kode_supplier;
			$data['id_supplier'] = $id_supplier;
	}
	else {
		$data['msg'] = '';
		$data['id_bonmkeluar'] = '';
		$data['go_proses'] = '';
		$data['supplier'] = $this->mmaster->get_supplier();
	}
	$data['isi'] = 'makloon/vmainformopbisbisan';
	$this->load->view('template',$data);

  }
  
  function editopbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_op_makloon 	= $this->uri->segment(4);
	
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$csupplier 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
	
	$data['query'] = $this->mmaster->get_op_bisbisan($id_op_makloon, 0);
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['csupplier'] = $csupplier;
	$data['carinya'] = $carinya;
	$data['supplier'] = $this->mmaster->get_supplier();
	$data['ukuran_bisbisan'] = $this->mmaster->get_ukuran_bisbisan();
	$data['isi'] = 'makloon/veditopbisbisanform';
	$this->load->view('template',$data);
  }
    
  function show_popup_bonmkeluar(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '')
		$keywordcari 	= $this->uri->segment(4);
	
	if ($keywordcari == '')
		$keywordcari = "all";
	
	$jum_total = $this->mmaster->get_bonmkeluartanpalimit($keywordcari);
							$config['base_url'] = base_url()."index.php/makloon/cform/show_popup_bonmkeluar/".$keywordcari."/";
	$data['query'] = $this->mmaster->get_bonmkeluar($keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('makloon/vpopupbonmkeluar',$data);

  }

  function submitopbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

			$no_op 	= $this->input->post('no_op', TRUE);
			$tgl_op 	= $this->input->post('tgl_op', TRUE);  
			$pisah1 = explode("-", $tgl_op);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_op = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$id_supplier = $this->input->post('id_supplier', TRUE);  
			$ket = $this->input->post('ket', TRUE);  
			
			$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
			// jika edit, var ini ada isinya
			$id_op_makloon 	= $this->input->post('id_op_makloon', TRUE);
			
			if ($id_op_makloon == '') {
				$cek_data = $this->mmaster->cek_data_opbisbisan($no_op);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'makloon/vmainformopbisbisan';
					$data['msg'] = "Data no OP ".$no_op." sudah ada..!";
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$this->mmaster->saveopbisbisan($no_op,$tgl_op, $id_supplier, $jenis_pembelian,
								$ket,$this->input->post('id_bonmkeluar_detail_'.$i, TRUE), 
								$this->input->post('id_brg_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('qty_satawal_'.$i, TRUE), 
									$this->input->post('jenis_potong_'.$i, TRUE), $this->input->post('id_ukuran_'.$i, TRUE),
									$this->input->post('keterangan_'.$i, TRUE) );
					}
					redirect('makloon/cform/viewopbisbisan');
				}
			} // end if id_op == ''
			else { // update
				$id_supplier_lama = $this->input->post('id_supplier_lama', TRUE);  
				
				$tgl = date("Y-m-d H:i:s");
				$uid_update_by = $this->session->userdata('uid');
				$this->db->query(" UPDATE tm_op_makloon SET id_supplier='$id_supplier', tgl_op = '$tgl_op', jenis_pembelian='$jenis_pembelian', 
								keterangan = '$ket', tgl_update = '$tgl', uid_update_by = '$uid_update_by' where id= '$id_op_makloon' ");
				
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						// cek qtynya di bon M keluar ============================================
												
						$id_bonmkeluar_detail = $this->input->post('id_bonmkeluar_detail_'.$i, TRUE);
						$id_brg = $this->input->post('id_brg_'.$i, TRUE);
						
							//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di bon M keluar
							$query3	= $this->db->query(" SELECT qty FROM tm_bonmkeluar_detail WHERE id = '$id_bonmkeluar_detail' ");
							$hasilrow = $query3->row();
							$qty_bonm = $hasilrow->qty;
							
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_makloon_detail a INNER JOIN tm_op_makloon b 
									ON a.id_op_makloon = b.id WHERE a.id_bonmkeluar_detail = '$id_bonmkeluar_detail' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum;
						
						$jumtot = $jum_op - $this->input->post('qty_lama_'.$i, TRUE) + $this->input->post('qty_'.$i, TRUE);
						
						if ($jumtot > $qty_bonm) {
							$qty = $qty_bonm-($jum_op - $this->input->post('qty_lama_'.$i, TRUE));
							
							// konversi ke sat awal
							// hitung qty_satawal pake rumus konversi
							$query2	= $this->db->query(" SELECT id_satuan_konversi, rumus_konversi, 
												angka_faktor_konversi FROM tm_barang WHERE id = '$id_brg' ");
							$hasilrow = $query2->row();
							$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
							$rumus_konversi = $hasilrow->rumus_konversi;
							$angka_faktor_konversi = $hasilrow->angka_faktor_konversi;
							
							if ($id_satuan_konversi != 0) {						
								if ($rumus_konversi == '1') {
										$qty_satawal = $qty/$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '2') {
										$qty_satawal = $qty*$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '3') {
										$qty_satawal = $qty-$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '4') {
										$qty_satawal = $qty+$angka_faktor_konversi;
									}
									else
										$qty_satawal = $qty;
							}
							else {
								$qty_satawal = $qty;
							}
						}
						else {
							$qty = $this->input->post('qty_'.$i, TRUE);
							$qty_satawal = $this->input->post('qty_satawal_'.$i, TRUE);
						}
						//==============================================================
						
						$this->db->query(" UPDATE tm_op_makloon_detail SET 
						 qty = '".$qty."', qty_satawal = '".$qty_satawal."', 
						 keterangan = '".$this->input->post('keterangan_'.$i, TRUE)."' 
						 where id= '".$this->input->post('id_op_detail_'.$i, TRUE)."' ");
						
						// cek status_op_bisbisan =======================================
						if ($qty_bonm <= ($jum_op-$this->input->post('qty_lama_'.$i, TRUE)+$this->input->post('qty_'.$i, TRUE) )) {
							
							$this->db->query(" UPDATE tm_bonmkeluar_detail SET status_op_bisbisan = 't' where id= '$id_bonmkeluar_detail' ");
						} // end if
						else { // jika tidak sama, maka statusnya false
							$this->db->query(" UPDATE tm_bonmkeluar_detail SET status_op_bisbisan = 'f' where id= '$id_bonmkeluar_detail' ");
						}
						// =========================================================

					}
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "makloon/cform/viewopbisbisan/index/".$cur_page;
					else
						$url_redirectnya = "makloon/cform/cariopbisbisan/".$csupplier."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
			} // end if update
  }
  
  function viewopbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'makloon/vformviewopbisbisan';
    $keywordcari = "all";
    $csupplier = 'xx';
	
    $jum_total = $this->mmaster->getAllopbisbisantanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/makloon/cform/viewopbisbisan/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllopbisbisan($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cariopbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = 'xx';
	
    $jum_total = $this->mmaster->getAllopbisbisantanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/makloon/cform/cariopbisbisan/'.$csupplier.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllopbisbisan($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	$data['isi'] = 'makloon/vformviewopbisbisan';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }

  function deleteopbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $list_brg 	= $this->uri->segment(5);
    
    $cur_page 	= $this->uri->segment(6);
    $is_cari 	= $this->uri->segment(7);
    $csupplier 	= $this->uri->segment(8);
    $carinya 	= $this->uri->segment(9);
    
    $this->mmaster->deleteopbisbisan($id, $list_brg);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "makloon/cform/viewopbisbisan/index/".$cur_page;
	else
		$url_redirectnya = "makloon/cform/cariopbisbisan/".$csupplier."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('op/cform/view');
  }
  
  // update status aktif
  function updatestatusopbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $aksi = $this->uri->segment(5);
    $cur_page 	= $this->uri->segment(6);
    $is_cari 	= $this->uri->segment(7);
    $csupplier 	= $this->uri->segment(8);
    $carinya 	= $this->uri->segment(9);
    
    $this->mmaster->updatestatusopbisbisan($id, $aksi);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "makloon/cform/viewopbisbisan/index/".$cur_page;
	else
		$url_redirectnya = "makloon/cform/cariopbisbisan/".$csupplier."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
  }
  
  function editopbisbisanitem() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $iddetail 	= $this->uri->segment(4);  
    $curpage 	= $this->uri->segment(5);  
    $is_cari 	= $this->uri->segment(6);
    $csupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($iddetail == '' && $curpage == '' && $is_cari == '' && $csupplier == '' && $carinya == '') {
		$iddetail 	= $this->input->post('iddetail', TRUE);
		$curpage 	= $this->input->post('curpage', TRUE);
		$carinya 	= $this->input->post('carinya', TRUE);
		$is_cari 	= $this->input->post('is_cari', TRUE);
		$csupplier 	= $this->input->post('csupplier', TRUE);
	}
    
    if ($go_edit == '') {
		$data['query'] = $this->mmaster->get_item_op_bisbisan($iddetail);
		$data['supplier'] = $this->mmaster->get_supplier();
		$data['isi'] = 'makloon/veditopbisbisanitemform';
		$data['iddetail'] = $iddetail;
		$data['curpage'] = $curpage;
		$data['csupplier'] = $csupplier;
		$data['carinya'] = $carinya;
		$data['is_cari'] = $is_cari;
		$this->load->view('template',$data);
	}
	else { // update data detail
		$qty 	= $this->input->post('qty', TRUE);
		$qty_lama 	= $this->input->post('qty_lama', TRUE);
		$id_op_makloon 	= $this->input->post('id_op_makloon', TRUE);
		$id_bonmkeluar_detail 	= $this->input->post('id_bonmkeluar_detail', TRUE);
		$id_supplier 	= $this->input->post('id_supplier', TRUE);
		$tgl_op 	= $this->input->post('tgl_op', TRUE);
		
		$id_satuan_konversi 	= $this->input->post('id_satuan_konversi', TRUE);
		$rumus_konversi 	= $this->input->post('rumus_konversi', TRUE);
		$angka_faktor_konversi 	= $this->input->post('angka_faktor_konversi', TRUE);
				
		$pisah1 = explode("-", $tgl_op);
		$thn1= $pisah1[2];
		$bln1= $pisah1[1];
		$tgl1= $pisah1[0];
		
		$tgl_op = $thn1."-".$bln1."-".$tgl1;
		
		$tgl = date("Y-m-d H:i:s");
		
		$this->db->query(" UPDATE tm_op_makloon SET tgl_op = '$tgl_op', id_supplier = '$id_supplier', tgl_update = '$tgl' WHERE id = '$id_op_makloon' ");
		
		$id_brg  = $this->input->post('id_brg', TRUE);
		
		//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di PP
		$query3	= $this->db->query(" SELECT qty FROM tm_bonmkeluar_detail WHERE id = '$id_bonmkeluar_detail' ");
		$hasilrow = $query3->row();
		$qty_bonm = $hasilrow->qty;
							
		$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_makloon_detail a INNER JOIN tm_op_makloon b 
								ON a.id_op_makloon = b.id WHERE a.id_bonmkeluar_detail = '$id_bonmkeluar_detail' ");
		$hasilrow = $query3->row();
		$jum_op = $hasilrow->jum;
						
		$jumtot = $jum_op - $qty_lama + $qty;						
		if ($jumtot > $qty_bonm) {
			$qty = $qty_bonm-($jum_op - $qty_lama);
			
			if ($id_satuan_konversi != 0) {					
				if ($rumus_konversi == '1') {
					$qty_satawal = $qty/$angka_faktor_konversi;
				}
				else if ($rumus_konversi == '2') {
					$qty_satawal = $qty*$angka_faktor_konversi;
				}
				else if ($rumus_konversi == '3') {
					$qty_satawal = $qty-$angka_faktor_konversi;
				}
				else if ($rumus_konversi == '4') {
					$qty_satawal = $qty+$angka_faktor_konversi;
				}
				else
					$qty_satawal = $qty;
			}
		}
		else {
			$qty = $this->input->post('qty', TRUE);
			$qty_satawal = $this->input->post('qty_satawal', TRUE);
		}
		//==============================================================
		
		$this->db->query(" UPDATE tm_op_makloon_detail SET qty = '".$qty."', qty_satawal = '".$qty_satawal."' WHERE id= '".$iddetail."' ");
						
		// cek status_op_bisbisan =======================================
		if ($qty_bonm <= ($jum_op-$qty_lama + $qty)) {
			$this->db->query(" UPDATE tm_bonmkeluar_detail SET status_op_bisbisan = 't' 
							where id= '$id_bonmkeluar_detail' ");							
		} // end if
		else { // jika tidak sama, maka statusnya false
			$this->db->query(" UPDATE tm_bonmkeluar_detail SET status_op_bisbisan = 'f' where id= '$id_bonmkeluar_detail' ");
		}
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "makloon/cform/viewopbisbisan/index/".$curpage;
		else
			$url_redirectnya = "makloon/cform/cariopbisbisan/".$csupplier."/".$carinya."/".$curpage;
		
		redirect($url_redirectnya);
	}
  }
  
  function deleteopbisbisanitem(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $iddetail 	= $this->uri->segment(4);
    $curpage 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $csupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    $this->mmaster->deleteopbisbisanitem($iddetail);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "makloon/cform/viewopbisbisan/index/".$curpage;
	else
		$url_redirectnya = "makloon/cform/cariopbisbisan/".$csupplier."/".$carinya."/".$curpage;
    redirect($url_redirectnya);
  }
  
  function get_pkp_tipe_pajak() {
		$id_sup 	= $this->uri->segment(4);
		$rows = array();
		if(isset($id_sup)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			$rows = $this->mmaster->get_pkp_tipe_pajak_bykodesup($id_sup);
			
			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);

  }
  
  function print_op(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_op 	= $this->uri->segment(4);
	//$proses = $this->input->post('proses', TRUE);  
	if ($id_op == '')
		$id_op = $this->input->post('id_op', TRUE);  
	
	//if ($proses != "") {
		$nama_up = $this->input->post('nama_up', TRUE);  
		$bag_pembelian = $this->input->post('bag_pembelian', TRUE);  
		$bag_keuangan = $this->input->post('bag_keuangan', TRUE);  
		$mengetahui = $this->input->post('mengetahui', TRUE);  
		$data['query'] = $this->mmaster->get_op($id_op, 1);
		// 09-07-2015
		$data['datasetting'] = $this->mmaster->get_perusahaan();
	//}
	/*else {
		$nama_up = '';
		$bag_pembelian = '';
		$bag_keuangan = '';  
		$mengetahui = '';  
	} */

	$data['id_op'] = $id_op;
	//$data['proses'] = $proses;
	/*$data['nama_up'] = $nama_up;
	$data['bag_pembelian'] = $bag_pembelian;
	$data['bag_keuangan'] = $bag_keuangan;
	$data['mengetahui'] = $mengetahui; */

	$this->load->view('op/vprintop_baru',$data);

  }
  
  function print_op_nonharga(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_op 	= $this->uri->segment(4);
	if ($id_op == '')
		$id_op = $this->input->post('id_op', TRUE);  
	
		$nama_up = $this->input->post('nama_up', TRUE);  
		$bag_pembelian = $this->input->post('bag_pembelian', TRUE);  
		$bag_keuangan = $this->input->post('bag_keuangan', TRUE);  
		$mengetahui = $this->input->post('mengetahui', TRUE);  
		$data['query'] = $this->mmaster->get_op($id_op, 1);
		// 09-07-2015
		$data['datasetting'] = $this->mmaster->get_perusahaan();
		$data['id_op'] = $id_op;

	$this->load->view('op/vprintop_nonharga',$data);
  }
  
  // 18-08-2015
  function addbtbbisbisan(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	$kd_brg = $this->input->post('kd_brg', TRUE);  
	
	$proses_submit = $this->input->post('submit', TRUE); 
	$id_op_detail = $this->input->post('id_brg', TRUE);  
	$list_brg = explode(";", $id_op_detail);
	$id_supplier2 = $this->input->post('id_supplier2', TRUE);    
	$is_no_op = $this->input->post('is_no_op', TRUE);    
	$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);    
	
	if ($proses_submit == "Proses") {
		$data['kel_brg'] = $this->mmaster->get_kel_brg();
		$data['ukuran_bisbisan'] = $this->mmaster->get_ukuran_bisbisan();
		
	  if ($is_no_op == '') { // jika ambil dari op
		if ($kd_brg !='') {
				$data['id_supplier']	= $id_supplier2;
				$suppliernya = $id_supplier2;
				
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_supplier2' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
				}
				else {
					$kode_supplier = '';
					$nama_supplier = '';
				}
				
				$data['kode_supplier']	= $kode_supplier;
				$data['nama_supplier']	= $nama_supplier;
			

			$detail_opnya = $this->mmaster->get_detail_opbisbisan($list_brg, $suppliernya);
			$data['msg'] = '';
			$data['supplier'] = $this->mmaster->get_supplier();
			$data['kel_brg'] = $this->mmaster->get_kel_brg();
			
			$data['op_detail'] = $detail_opnya;
			
			$no_op = ""; $id_op = ""; $temp_no_op = ""; $temp_id_op = "";
			for($j=0;$j<count($detail_opnya);$j++){
				if ($detail_opnya[$j]['no_op'] != $temp_no_op)
					$no_op.= $detail_opnya[$j]['no_op']."; ";
					
				if ($detail_opnya[$j]['id_op'] != $temp_id_op)
					$id_op.= $detail_opnya[$j]['id_op'].";";
						
				$temp_no_op = $detail_opnya[$j]['no_op'];
				$temp_id_op = $detail_opnya[$j]['id_op'];
			}			
			
			$data['no_op'] = $no_op;
			$data['id_op'] = $id_op;
		}
		
		$data['go_proses'] = '1';
		$data['jenis_pembelian'] = $jenis_pembelian;
		$data['isi'] = 'makloon/vmainformbtbbisbisan';
		$this->load->view('template',$data);
      } // end if is_no_op == ''	
      else { // jika is_no_op == 't'
		$data['is_no_op'] = $is_no_op;
		$data['id_supplier']	= $id_supplier2;
		$data['supplier'] = $this->mmaster->get_supplier();
		
		$data['jenis_pembelian'] = $jenis_pembelian;
		$data['isi'] = 'makloon/vmainformbtbbisbisanmanual';
		$this->load->view('template',$data);
	  }

	}
	else {
		$data['msg'] = '';
		$data['id_op'] = '';
		$data['go_proses'] = '';
		$data['supplier2'] = $this->mmaster->get_supplier();
		
		$data['isi'] = 'makloon/vmainformbtbbisbisan';
		$this->load->view('template',$data);
	}
  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
	//$departemen = ambil infonya dari tabel user
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$kel_brg	= $this->uri->segment(4);
	$jenis_potong	= $this->uri->segment(5);
	$posisi 	= $this->uri->segment(6);
	$id_supplier 	= $this->uri->segment(7);

	if ($posisi == '' || $kel_brg =='' || $jenis_potong == '' || $id_supplier == '') {
		$posisi = $this->input->post('posisi', TRUE);  
		$kel_brg = $this->input->post('kel_brg', TRUE);  
		$jenis_potong = $this->input->post('jenis_potong', TRUE);  
		$id_supplier = $this->input->post('id_supplier', TRUE);  
	}	
	
		$keywordcari 	= $this->input->post('cari', TRUE);
		$id_jenis_brg = $this->input->post('id_jenis_brg', TRUE);    
		
		if ($keywordcari == '' && ($id_jenis_brg == '' || $posisi == '' || $kel_brg == '' || $jenis_potong == '' || $id_supplier == '') ) {
			$kel_brg 	= $this->uri->segment(4);
			$jenis_potong 	= $this->uri->segment(5);
			$posisi 	= $this->uri->segment(6);
			$id_supplier 	= $this->uri->segment(7);
			$id_jenis_brg 	= $this->uri->segment(8);
			$keywordcari 	= $this->uri->segment(9);
		}

		if ($keywordcari == '')
			$keywordcari 	= "all";
			
		if ($id_jenis_brg == '')
			$id_jenis_brg 	= '0';
		
		if ($keywordcari != '') {
			$ganti = str_replace("%20"," ", $keywordcari);
			$keywordcari = $ganti;
		}
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $id_jenis_brg);
		//$jum_total	= $qjum_total->num_rows(); 

			$config['base_url'] = base_url()."index.php/makloon/cform/show_popup_brg/".$kel_brg."/".$jenis_potong."/".$posisi."/".$id_supplier."/".$id_jenis_brg."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total); 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(10,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $id_jenis_brg);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	$data['kel_brg'] = $kel_brg;
	$data['jenis_potong'] = $jenis_potong;
	$data['id_supplier'] = $id_supplier;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;
	$data['jenis_brg'] = $this->mmaster->get_jenis_brg($kel_brg);
	$data['cjenis_brg'] = $id_jenis_brg;
	$data['startnya'] = $config['cur_page'];
	
	$this->load->view('makloon/vpopupbrg',$data);
  }
  
  function submitbtbbisbisan(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj = $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			$no 	= $this->input->post('no', TRUE);
			$id_supplier = $this->input->post('id_sup', TRUE);  
			
			$gtotal = $this->input->post('gtotal', TRUE);  
			$asligtotal = $this->input->post('asligtotal', TRUE);  
			
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$ket = $this->input->post('ket', TRUE);  
						
			$is_no_op 	= $this->input->post('is_no_op', TRUE);
						
			$cek_data = $this->mmaster->cek_data_btbbisbisan($no_sj, $id_supplier);
			if (count($cek_data) > 0) { 
				$data['isi'] = 'makloon/vmainformbtbbisbisan';
				$data['msg'] = "Data no SJ ".$no_sj." sudah ada..!";
				$data['id_pp'] = '';
				$data['go_proses'] = '';
				$data['supplier2'] = $this->mmaster->get_supplier();
				$this->load->view('template',$data);
			}
			else {
				$jumlah_input=$no-1;
					
				for ($i=1;$i<=$jumlah_input;$i++)
				{
						$this->mmaster->savebtbbisbisan($no_sj,$tgl_sj,$id_supplier,$gtotal, $asligtotal, $total_pajak, $dpp, 
						$ket, $is_no_op, $jenis_pembelian, $this->input->post('id_op_detail_'.$i, TRUE), $this->input->post('id_op_'.$i, TRUE), 
							$this->input->post('jenis_potong_'.$i, TRUE), $this->input->post('id_ukuran_'.$i, TRUE),
							$this->input->post('id_brg_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
							$this->input->post('qty_'.$i, TRUE), $this->input->post('qty_konv_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE), 
							$this->input->post('harga_lama_'.$i, TRUE), 
							$this->input->post('pajak_'.$i, TRUE), $this->input->post('diskon_'.$i, TRUE), 
							$this->input->post('total_'.$i, TRUE), $this->input->post('aslitotal_'.$i, TRUE) );
				}
				redirect('makloon/cform/viewbtbbisbisan');
			}
  }
  
  function viewbtbbisbisan(){
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'makloon/vformviewbtbbisbisan';
    $keywordcari = "all";
    $csupplier = '0';
	
	$date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
	$caribrg = "all";
	$filterbrg = "n";
	
    $jum_total = $this->mmaster->getAllbtbbisbisantanpalimit($csupplier, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/makloon/cform/viewbtbbisbisan/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllbtbbisbisan($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function caribtbbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
	
	if ($csupplier == '')
		$csupplier 	= $this->uri->segment(4);
	if ($date_from == '')
		$date_from = $this->uri->segment(5);
	if ($date_to == '')
		$date_to = $this->uri->segment(6);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(7);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(8);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(9);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($csupplier == '')
		$csupplier = '0';
	if ($filterbrg == '')
		$filterbrg = 'n';
	if ($caribrg == '')
		$caribrg = "all";
	
    $jum_total = $this->mmaster->getAllbtbbisbisantanpalimit($csupplier, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/makloon/cform/caribtbbisbisan/'.$csupplier.'/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(10);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllbtbbisbisan($config['per_page'],$this->uri->segment(10), $csupplier, $keywordcari, $date_from, $date_to, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'makloon/vformviewbtbbisbisan';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
		
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  // 19-08-2015
  function deletebtbbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $csupplier 	= $this->uri->segment(7);
    $tgl_awal 	= $this->uri->segment(8);
    $tgl_akhir 	= $this->uri->segment(9);
    $carinya 	= $this->uri->segment(10);
    $caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
    $this->mmaster->deletebtbbisbisan($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "makloon/cform/viewbtbbisbisan/index/".$cur_page;
	else
		$url_redirectnya = "makloon/cform/caribtbbisbisan/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
	
	redirect($url_redirectnya);
  }
  
  function editbtbbisbisan(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_pembelian 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$csupplier 	= $this->uri->segment(7);
	$tgl_awal 	= $this->uri->segment(8);
	$tgl_akhir 	= $this->uri->segment(9);
	$carinya 	= $this->uri->segment(10);
	$caribrg 	= $this->uri->segment(11);
	$filterbrg 	= $this->uri->segment(12);
	
	$data['query'] = $this->mmaster->get_pembelian_bisbisan($id_pembelian);
	$data['supplier'] = $this->mmaster->get_supplier();
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['csupplier'] = $csupplier;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['ukuran_bisbisan'] = $this->mmaster->get_ukuran_bisbisan();

	$data['isi'] = 'makloon/veditformbtbbisbisan';
	$this->load->view('template',$data);
  }
  
  function updatedatabtbbisbisan() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_pembelian 	= $this->input->post('id_pembelian', TRUE);
			$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj_lama 	= $this->input->post('no_sj_lama', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$id_supplier_lama = $this->input->post('hide_supplier', TRUE);  
			$id_supplier = $this->input->post('id_supplier', TRUE);  
			
			//$gtotal = $this->input->post('gtotal', TRUE);  
			$asligtotal = $this->input->post('asligtotal', TRUE);  
			
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$ket = $this->input->post('ket', TRUE);  
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
			
			$tgl = date("Y-m-d H:i:s");
							
			$submit2 = $this->input->post('submit2', TRUE);
				
			if ($submit2 != '') { // hapus per item
				/*	20-06-2015 ALGORITMA PROSES HAPUS ITEM (sama dgn penjelasan yg dibawah): DONE ALL
				* - reset status op/pp dan op / pp_detail menjadi 'f' utk yg ada acuan OP / PP. dan sambil hapus item brgnya
				* - Reset stoknya jika status_stok = 't' dan hapus dari tm_apply_stok_pembelian_detail
				* - Hapus dari tm_pembelian_detail
				* - Hitung ulang DPP, PPN, dan grand total
				* - Update header di tm_pembelian dan tm_apply_stok_pembelian
				*/		
				
				//1. ambil no_sj dan id_supplier
				$query4	= $this->db->query(" SELECT no_sj, id_supplier from tm_pembelian_makloon where id = '$id_pembelian' ");
				$hasilrow = $query4->row();
				$no_sj	= $hasilrow->no_sj;
				$id_supplier	= $hasilrow->id_supplier;

				$jumlah_input=$no-1;
				$hitungulangtotal = 0; $hitungulangppn = 0;
				for ($i=1;$i<=$jumlah_input;$i++) {
					if ($this->input->post('cek_'.$i, TRUE) == '') {
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							// ambil data total dan pajak dari tm_pembelian_detail
							$query4	= $this->db->query(" SELECT pajak, total from tm_pembelian_makloon_detail where id = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							$hasilrow = $query4->row();
							$pajaknya	= $hasilrow->pajak;
							$totalnya	= $hasilrow->total;
							
							$hitungulangtotal+= $totalnya;
							$hitungulangppn+= $pajaknya;
						}
					}
					
					if ($this->input->post('cek_'.$i, TRUE) == 'y') {
						/* algoritma:
						1. Cek apakah ada id_op_detail ataupun id_op di tm_pembelian/tm_pembelian_detail. 
						   Jika ada, maka edit status OP-nya menjadi 'f' di tm_op_detail dan di tm_op
						2. Reset stoknya jika status_stok = 't' dan hapus dari tm_apply_stok_pembelian_detail
						3. Hapus dari tm_pembelian_detail
						4. Hitung ulang DPP, PPN, dan grand total
						*/
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							
							//========= start here ==========
							$id_brgnya = $this->input->post('id_brg_'.$i, TRUE);
						    $id_brg_lama = $this->input->post('id_brg_lama_'.$i, TRUE); 
						    $id_op_detail = $this->input->post('id_op_detail_'.$i, TRUE); 
							
							if ($id_op_detail != '0') {
								$query4	= $this->db->query(" SELECT id_op_makloon FROM tm_op_makloon_detail where id = '$id_op_detail' ");
								$hasilrow = $query4->row();
								$id_opupdatestatus	= $hasilrow->id_op;
								
								$this->db->query("UPDATE tm_op_makloon set status_op = 'f' where id= '$id_opupdatestatus' ");
								$this->db->query("UPDATE tm_op_makloon_detail set status_op = 'f' where id= '$id_op_detail' ");
							}
																				 
							 // 2. Reset stoknya jika status_stok = 't' dan hapus dari tm_apply_stok_pembelian_detail
							 // 19-08-2015 SEMENTARA GA PAKE UPDATE STOK UTK BISBISAN
							/* $qty_lama = $this->input->post('qty_lama_'.$i, TRUE); 
							 $harga_lama = $this->input->post('harga_lama_'.$i, TRUE); 
							 $id_brg_lama = $this->input->post('id_brg_lama_'.$i, TRUE); 
							 
							 //ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg='$id_brg_lama' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							else
								$stok_lama = 0;
										
							$stokreset1 = $stok_lama-$qty_lama;
										
							//cek stok terakhir tm_stok_harga, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE id_brg = '$id_brg_lama' 
														AND harga = '$harga_lama' ");
							if ($query3->num_rows() == 0){
								$stok_harga_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_harga_lama	= $hasilrow->stok;
							}
							$stokreset2 = $stok_harga_lama-$qty_lama;
														
							$this->db->query(" UPDATE tm_stok SET stok = '$stokreset1', tgl_update_stok = '$tgl'
												where id_brg= '$id_brg_lama' ");
											
							$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl' 
												where id_brg= '$id_brg_lama' AND harga = '$harga_lama' ");
								
							$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail 
														WHERE id_pembelian_detail='".$this->input->post('id_detail_'.$i, TRUE)."' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_apply_stok_detail	= $hasilrow->id;
								$this->db->query("DELETE FROM tm_apply_stok_pembelian_detail WHERE id='$id_apply_stok_detail' ");
							} */
							
							$this->db->query("DELETE FROM tm_pembelian_makloon_detail WHERE id='".$this->input->post('id_detail_'.$i, TRUE)."' ");
						
						} // end if ($this->input->post('id_detail_'.$i, TRUE) != 'n')
					} // end if cek_
				} // end for
				// hitung DPP
				$hitungulangdpp = $hitungulangtotal*0.98;
				$hitungulangdpp = round($hitungulangdpp, 4);
				$hitungulangppn = round($hitungulangppn, 4);
				
				$this->db->query(" UPDATE tm_pembelian_makloon SET no_sj = '$no_sj', id_supplier = '$id_supplier', 
								tgl_sj = '$tgl_sj', jenis_pembelian='$jenis_pembelian', tgl_update = '$tgl', 
								total_pajak = '$hitungulangppn', dpp = '$hitungulangdpp', total = '$hitungulangtotal',
								keterangan='$ket' where id= '$id_pembelian' ");
				/*$this->db->query(" UPDATE tm_apply_stok_pembelian SET no_sj = '$no_sj', id_supplier = '$id_supplier', 
								tgl_update = '$tgl'
								where no_sj = '$no_sj_lama' AND id_supplier = '$id_supplier_lama' "); */
			}
			else {
				// UPDATE DATA
				// 20-06-2015
			/* ALGORITMA PROSES EDIT:
					- item lama tidak bisa dihapus via tombol (-). berarti asumsinya item2 brg yg udh ada itu cuma diedit aja, ga perlu dihapus
					- SEMUA ITEM BRG LAMA DICEK DULU apakah ada acuan OP/PP, trus tentukan status OPnya / PPnya udh terpenuhi blm dari pengecekan qtynya (on prog)
					- Item2 baru diinsert ke tm_pembelian_detail dan tm_apply_stok_pembelian_detail, dgn status stok = 'f' (OK)
					- Update stok utk item2 lama dan update juga qty-nya di tm_pembelian_detail dan tm_apply_stok_pembelian_detail dgn status_stok = 't'
					- update data harga juga
					- update data header di tm_pembelian dan tm_pembelian_detail 
			*/
					// ====================================================
					$uid_update_by = $this->session->userdata('uid');
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$id_brg = $this->input->post('id_brg_'.$i, TRUE);
						$id_brg_lama = $this->input->post('id_brg_lama_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$qty_lama = $this->input->post('qty_lama_'.$i, TRUE);
						$harga = $this->input->post('harga_'.$i, TRUE);
						$harga_lama = $this->input->post('harga_lama_'.$i, TRUE);
						
						$qty_konv = $this->input->post('qty_konv_'.$i, TRUE);
						$jenis_potong = $this->input->post('jenis_potong_'.$i, TRUE);
						$id_ukuran_bisbisan = $this->input->post('id_ukuran_'.$i, TRUE);
						
						$nama_brg = $this->input->post('nama_'.$i, TRUE);
							
						// bisa insert item brg yg baru
						if ($this->input->post('id_detail_'.$i, TRUE) == 'n') {
							// a. insert item detail
							$data_detail = array(
								'id_brg'=>$this->input->post('id_brg_'.$i, TRUE),
								//'nama_brg'=>$this->db->escape_str($nama_brg),
								'nama_brg'=>$nama_brg,
								'qty_satawal'=>$this->input->post('qty_'.$i, TRUE),
								'qty_konversi'=>$this->input->post('qty_konv_'.$i, TRUE),
								'jenis_potong'=>$jenis_potong,
								'id_ukuran_bisbisan'=>$id_ukuran_bisbisan,
								'harga'=>$this->input->post('harga_'.$i, TRUE),
								'pajak'=>$this->input->post('pajak_'.$i, TRUE),
								'diskon'=>$this->input->post('diskon_'.$i, TRUE),
								'total'=>$this->input->post('total_'.$i, TRUE),
								'id_pembelian_makloon'=>$id_pembelian,
								'id_op_makloon_detail'=>$this->input->post('id_op_detail_'.$i, TRUE)
							);
							$this->db->insert('tm_pembelian_makloon_detail',$data_detail);
							
							$query3	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_detail ORDER BY id DESC LIMIT 1 ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$id_pembelian_detail = $hasilrow->id;
							}
							else
								$id_pembelian_detail = 0;
							
							// 18-06-2015, save ke apply_stok digabung kesini. 19-08-2015 SEMENTARA GA DIPAKE
							$th_now	= date("Y");
	
							// =============================20-06-2015=======================
				
							// insert Bon M masuk secara otomatis di tm_apply_stok_pembelian
								// ambil header bon M
							/*	$query3	= $this->db->query(" SELECT id, no_bonm FROM tm_apply_stok_pembelian WHERE no_sj = '$no_sj_lama' 
												AND id_supplier = '$id_supplier_lama' AND status_aktif = 't' ");
								if ($query3->num_rows() > 0) {
									$hasilrow = $query3->row();
									$no_bonm = $hasilrow->no_bonm;
									$id_apply_stok = $hasilrow->id;
									
									//save ke tabel tm_apply_stok_pembelian_detail
									// jika semua data tdk kosong, insert ke tm_apply_stok_pembelian_detail
									$data_detail = array(
											'id_brg'=>$id_brg,
											'qty'=>$qty,
											'harga'=>$harga,
											'id_apply_stok'=>$id_apply_stok,
											'no_bonm'=>$no_bonm,
											'id_pembelian_detail'=>$id_pembelian_detail
									);
									$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
								} */
						}
						else {
							// update harga
							if (($harga != $harga_lama) && ($id_supplier != $id_supplier_lama)) {
								
								$query3	= $this->db->query(" SELECT harga FROM tm_harga_bisbisan WHERE jenis_potong = '$jenis_potong'
												AND id_supplier = '$id_supplier' ");
								if ($query3->num_rows() == 0){
									$this->db->query(" INSERT INTO tm_harga_bisbisan (jenis_potong, id_supplier, harga, 
									tgl_input, tgl_update) VALUES ('$jenis_potong', '$id_supplier', '$harga', '$tgl', '$tgl') ");
								}
								else {
									$this->db->query(" UPDATE tm_harga_bisbisan SET harga = '$harga', tgl_update='$tgl'
												where jenis_potong= '$jenis_potong' AND id_supplier = '$id_supplier' ");
								}
							}
							else if (($harga != $harga_lama) && ($id_supplier == $id_supplier_lama)) {								
								$this->db->query(" UPDATE tm_harga_bisbisan SET harga = '$harga', tgl_update='$tgl'
												where jenis_potong= '$jenis_potong' AND id_supplier = '$id_supplier' ");
							}
							
							// reset status OP
							// ambil id_op_detail
							$id_op_detail = $this->input->post('id_op_detail_'.$i, TRUE);
							
							if ($id_op_detail != '0') {
								// ambil qty di op_detail
								$query3	= $this->db->query(" SELECT id_op_makloon, qty FROM tm_op_makloon_detail WHERE id = '$id_op_detail' ");
								if ($query3->num_rows() > 0) {
									$hasilrow = $query3->row();
									$id_op = $hasilrow->id_op_makloon;
									$qty_op = $hasilrow->qty;
									
									//cek jumlah pembelian. jika > qty OP maka jika sudah t semua di tabel detail, maka update tabel tm_op_makloon di field status_op menjadi t
									$sqlxx = " SELECT sum(qty_konversi) as jumbeli FROM tm_pembelian_makloon_detail WHERE id_op_makloon_detail = '$id_op_detail' ";
									$queryxx	= $this->db->query($sqlxx);
									if ($queryxx->num_rows() > 0) {
										$hasilxx = $queryxx->row();
										$jumbeli = $hasilxx->jumbeli;
										
										$selisih = $jumbeli-$qty_lama+$qty;
										
										if ($selisih >= $qty_op) {
											$this->db->query(" UPDATE tm_op_makloon_detail SET status_op = 't' WHERE id = '$id_op_detail' ");
											
											// cek udh t semua blm, kalo udh, maka ganti jadi t headernya
											$sqlxx2 = " SELECT id FROM tm_op_makloon_detail WHERE status_op = 'f' ";
											$queryxx2	= $this->db->query($sqlxx2);
											if ($queryxx2->num_rows() == 0) {
												$this->db->query(" UPDATE tm_op_makloon SET status_op = 't' WHERE id='$id_op' ");
											}
										}
									}
									
								}
							}
						} // end if != n
						// ==================================================== =============================
					  
						   $sql = " UPDATE tm_pembelian_makloon_detail SET qty_satawal = '".$this->input->post('qty_'.$i, TRUE)."',
						   qty_konversi = '".$this->input->post('qty_konv_'.$i, TRUE)."', jenis_potong = '".$this->input->post('jenis_potong_'.$i, TRUE)."',
						   id_ukuran_bisbisan = '".$this->input->post('id_ukuran_'.$i, TRUE)."', ";
						   if ($id_brg_lama != $id_brg)
								$sql.= " id_brg = '$id_brg', ";
								
							$sql.= " nama_brg= '".$this->db->escape_str($this->input->post('nama_'.$i, TRUE))."', harga = '".$this->input->post('harga_'.$i, TRUE)."', diskon = '".$this->input->post('diskon_'.$i, TRUE)."',
								pajak = '".$this->input->post('pajak_'.$i, TRUE)."', total = '".$this->input->post('aslitotal_'.$i, TRUE)."'
								where id= '".$this->input->post('id_detail_'.$i, TRUE)."' ";
							$this->db->query($sql);
							// 29-10-2015 dikeluarin. ga jadi 
							//nama_brg= '".$this->db->escape_str($this->input->post('nama_'.$i, TRUE))
							
						// 13-07-2015 cek di apply_stok_pembelian_detail. Jika status_stok='t' maka hrs diupdate pake qty yg baru. 
						//setelah itu update juga item brg di tm_apply_stok_pembelian_detail
						
					/*	$queryxx	= $this->db->query(" SELECT status_stok
									FROM tm_apply_stok_pembelian_detail WHERE id_pembelian_detail = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$status_stok	= $hasilxx->status_stok;
							
							if ($status_stok == 't') {
								$queryxx2	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg = '".$id_brg."' ");
								if ($queryxx2->num_rows() > 0){
									$hasilxx2 = $queryxx2->row();
									$stok_lama	= $hasilxx2->stok;
									
									$stok_baru = $stok_lama-$qty_lama+$qty;
									$this->db->query(" UPDATE tm_stok SET stok='$stok_baru', tgl_update_stok='$tgl' 
													WHERE id_brg = '".$id_brg."' ");
								}
							} // end if status_Stok
						} // end cek status stok
						
						$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET qty = '".$this->input->post('qty_'.$i, TRUE)."', 
									harga = '".$this->input->post('harga_'.$i, TRUE)."', 
									tgl_update = '$tgl'
									where id_pembelian_detail = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
					  */ 
					} // end for
					
					// 20-06-2015
					//update headernya INI DI BELAKANG AJA
					$this->db->query(" UPDATE tm_pembelian_makloon SET no_sj = '$no_sj', id_supplier = '$id_supplier', 
										jenis_pembelian = '$jenis_pembelian',
										tgl_sj = '$tgl_sj', total = '$asligtotal',
										keterangan = '$ket', tgl_update = '$tgl', 
										total_pajak = '$total_pajak', dpp = '$dpp', uid_update_by = '$uid_update_by' 
										where id= '$id_pembelian' ");
				/*	$this->db->query(" UPDATE tm_apply_stok_pembelian SET no_sj = '$no_sj', id_supplier = '$id_supplier', 
									tgl_update = '$tgl'
									where no_sj = '$no_sj_lama' AND id_supplier = '$id_supplier_lama' "); */
				} // 20-07-2012, end if $submit2 == ''
				
				if ($carinya == '') $carinya = "all";
				if ($is_cari == 0)
					$url_redirectnya = "makloon/cform/viewbtbbisbisan/index/".$cur_page;
				else
					$url_redirectnya = "makloon/cform/caribtbbisbisan/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
					
				redirect($url_redirectnya);
  }
  
  // 20-08-2015
  function viewfakturbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'makloon/vformviewfakturbisbisan';
    $keywordcari = "all";
    $cjenis_beli = '0';
    $cstatus_lunas = '0';
    $csupplier = '0';
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAllfakturbisbisantanpalimit($cjenis_beli, $cstatus_lunas, $csupplier, $keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/makloon/cform/viewfakturbisbisan/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllfakturbisbisan($config['per_page'],$this->uri->segment(5), $cjenis_beli, $cstatus_lunas, $csupplier, $keywordcari, $date_from, $date_to);
				
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
		
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['cjenis_beli'] = $cjenis_beli;
	$data['cstatus_lunas'] = $cstatus_lunas;
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function carifakturbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$cjenis_beli = $this->input->post('jenis_beli', TRUE);  
	$cstatus_lunas = $this->input->post('status_lunas', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE);
	$date_to 	= $this->input->post('date_to', TRUE);
	
	if ($cjenis_beli == '')
		$cjenis_beli 	= $this->uri->segment(4);
	if ($cstatus_lunas == '')
		$cstatus_lunas 	= $this->uri->segment(5);
	if ($csupplier == '')
		$csupplier 	= $this->uri->segment(6);
	if ($date_from == '')
		$date_from = $this->uri->segment(7);
	if ($date_to == '')
		$date_to = $this->uri->segment(8);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(9);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($cjenis_beli == '')
		$cjenis_beli = '0';
	if ($cstatus_lunas == '')
		$cstatus_lunas = '0';
	if ($csupplier == '')
		$csupplier = '0';
			
    $jum_total = $this->mmaster->getAllfakturbisbisantanpalimit($cjenis_beli, $cstatus_lunas, $csupplier, $keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/makloon/cform/carifakturbisbisan/'.$cjenis_beli.'/'.$cstatus_lunas.'/'.$csupplier.'/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(10);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllfakturbisbisan($config['per_page'],$this->uri->segment(10), $cjenis_beli, $cstatus_lunas, $csupplier, $keywordcari, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'makloon/vformviewfakturbisbisan';
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['cjenis_beli'] = $cjenis_beli;
	$data['cstatus_lunas'] = $cstatus_lunas;
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function addfakturbisbisan(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'makloon/vmainformfakturbisbisan';
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function show_popup_pembelian(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jnsaction = $this->uri->segment(4); 
	$csupplier = $this->uri->segment(5); 
	$jenis_pembelian = $this->uri->segment(6);
	$no_fakturnya = $this->uri->segment(7); 
	
	if ($jnsaction == '')
		$jnsaction = $this->input->post('jnsaction', TRUE); 
	if ($csupplier == '')
		$csupplier = $this->input->post('csupplier', TRUE);  
	if ($jenis_pembelian == '')
		$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
	if ($no_fakturnya == '')
		$no_fakturnya = $this->input->post('no_fakturnya', TRUE);  
		
	if ($keywordcari == '' && ($csupplier == '' || $jenis_pembelian == '' || $jnsaction == '' || $no_fakturnya == '') ) {
		$jnsaction 	= $this->uri->segment(4);
		$csupplier 	= $this->uri->segment(5);
		$jenis_pembelian 	= $this->uri->segment(6);
		$no_fakturnya 	= $this->uri->segment(7);
		$keywordcari 	= $this->uri->segment(8);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	if ($jenis_pembelian == '')
		$jenis_pembelian = '0';
	
	$jum_total = $this->mmaster->get_btbbisbisantanpalimit($jnsaction, $csupplier, $jenis_pembelian, $no_fakturnya, $keywordcari);
	$data['query'] = $this->mmaster->get_btbbisbisan($jnsaction, $csupplier, $jenis_pembelian, $no_fakturnya, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$data['jenis_pembelian'] = $jenis_pembelian;
	$data['jnsaction'] = $jnsaction;
	$data['no_fakturnya'] = $no_fakturnya;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE id = '$csupplier' ");
	$hasilrow3 = $query3->row();
	$data['nama_supplier']	= $hasilrow3->nama;

	$this->load->view('makloon/vpopuppemb',$data);
  }
  
  function submitfakturbisbisan(){
	//$this->load->library('form_validation');
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

			$supplier 	= $this->input->post('supplier', TRUE);
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			$jum 	= $this->input->post('jum', TRUE);
			
			$id_sj 	= $this->input->post('id_sj', TRUE);
			$id_sj = trim($id_sj);
			
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;						

			$cek_data = $this->mmaster->cek_data_fakturbisbisan($no_fp, $supplier);
			if (count($cek_data) > 0) {
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$supplier' ");
				$hasilrow3 = $query3->row();
				$kode_supplier	= $hasilrow3->kode_supplier;
				$nama_supplier	= $hasilrow3->nama;
				
				$data['isi'] = 'makloon/vmainformfakturbisbisan';
				$data['list_supplier'] = $this->mmaster->get_supplier(); 
				$data['msg'] = "Data no faktur ".$no_fp." dari supplier ".$kode_supplier." - ".$nama_supplier." sudah ada..!";
				$this->load->view('template',$data);
			}
			else {
				$this->mmaster->savefakturbisbisan($no_fp, $tgl_fp, $supplier, $jenis_pembelian, $jum, $id_sj);
				redirect('makloon/cform/viewfakturbisbisan');
			}
  }
  
  function deletefakturbisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $cjenis_beli 	= $this->uri->segment(7);
    $cstatus_lunas 	= $this->uri->segment(8);
    $csupplier 	= $this->uri->segment(9);
    $tgl_awal 	= $this->uri->segment(10);
    $tgl_akhir 	= $this->uri->segment(11);
    $carinya 	= $this->uri->segment(12);
    $this->mmaster->deletefakturbisbisan($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "makloon/cform/viewfakturbisbisan/index/".$cur_page;
	else
		$url_redirectnya = "makloon/cform/carifakturbisbisan/".$cjenis_beli."/".$cstatus_lunas."/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
  }
  
  function editfakturbisbisan(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id_faktur 	= $this->uri->segment(4);	
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$cjenis_beli 	= $this->uri->segment(7);
	$cstatus_lunas 	= $this->uri->segment(8);
	$csupplier 	= $this->uri->segment(9);
	$tgl_awal 	= $this->uri->segment(10);
	$tgl_akhir 	= $this->uri->segment(11);
	$carinya 	= $this->uri->segment(12);
	
	$data['query'] = $this->mmaster->get_faktur_bisbisan($id_faktur);
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['msg'] = '';
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['cjenis_beli'] = $cjenis_beli;
	$data['cstatus_lunas'] = $cstatus_lunas;
	$data['csupplier'] = $csupplier;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['isi'] = 'makloon/veditformfakturbisbisan';
	$data['id_faktur'] = $id_faktur;
	$this->load->view('template',$data);
  }
  
  function updatedatafakturbisbisan() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_faktur 	= $this->input->post('id_faktur', TRUE);
			
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$no_sj_lama = $this->input->post('no_sj_lama', TRUE);
			$id_sj = $this->input->post('id_sj', TRUE);
			$id_sj_lama = $this->input->post('id_sj_lama', TRUE);
			$id_sj = trim($id_sj);
			$id_sj_lama = trim($id_sj_lama);
			
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$supplier = $this->input->post('supplier', TRUE);  
			$jenis_pembelian 	= $this->input->post('jenis_pembelian', TRUE);
			
			$no_faktur_lama = $this->input->post('no_faktur_lama', TRUE);
			$jum = $this->input->post('jum', TRUE);
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$cjenis_beli = $this->input->post('cjenis_beli', TRUE);
			$cstatus_lunas = $this->input->post('cstatus_lunas', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;
									
			//$tgl = date("Y-m-d");			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			if ($no_fp != $no_faktur_lama) {
				$cek_data = $this->mmaster->cek_data_fakturbisbisan($no_fp, $supplier);
				if (count($cek_data) == 0) {
					$this->db->query(" UPDATE tm_pembelian_makloon_faktur SET id_supplier = '$supplier', 
									jenis_pembelian = '$jenis_pembelian', no_faktur = '$no_fp', 
									tgl_faktur = '$tgl_fp', tgl_update= '$tgl', jumlah = '$jum', sisa ='$jum',
									uid_update_by='$uid_update_by' where id= '$id_faktur' "); 
									
					// reset dulu status_faktur menjadi FALSE --------------------------------
					$list_sj_lama = explode(",", $no_sj_lama);
					$list_id_sj_lama = explode(";", $id_sj_lama);
					
					for($j=0; $j<count($list_sj_lama)-1; $j++){
							$this->db->query(" UPDATE tm_pembelian_makloon SET status_faktur = 'f', no_faktur = '' 
									WHERE id = '".$list_id_sj_lama[$j]."' "); 
					}
					//-------------------------------------------------------------------------
					
					$this->db->query(" DELETE FROM tm_pembelian_makloon_faktur_detail where id_pembelian_makloon_faktur= '$id_faktur' ");
					
					// insert tabel detail sj-nya
					$list_sj = explode(",", $no_sj); 
					$list_id_sj = explode(";", $id_sj); 
					
					foreach($list_id_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') {
							$data_detail = array(
							  'id_pembelian_makloon_faktur'=>$id_faktur,
							  'id_pembelian_makloon'=>$row1,
							);
							$this->db->insert('tm_pembelian_makloon_faktur_detail',$data_detail);
							
							// update status_faktur dan jenis_pembelian di tabel tm_pembelian_makloon
							$this->db->query(" UPDATE tm_pembelian_makloon SET no_faktur = '$no_fp', status_faktur = 't',
												jenis_pembelian = '$jenis_pembelian'
												WHERE id = '$row1' ");
						}
					}
				
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "makloon/cform/viewfakturbisbisan/index/".$cur_page;
					else
						$url_redirectnya = "makloon/cform/carifakturbisbisan/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
				}
				else {
					$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$supplier' ");
					$hasilrow3 = $query3->row();
					$kode_supplier	= $hasilrow3->kode_supplier;
					$nama_supplier	= $hasilrow3->nama;
				
					$data['query'] = $this->mmaster->get_faktur_bisbisan($id_faktur);
					$data['list_supplier'] = $this->mmaster->get_supplier(); 
					$data['isi'] = 'makloon/veditformfakturbisbisan';
					$data['id_faktur'] = $id_faktur;
					$data['msg'] = "Update gagal. Data no faktur ".$no_fp." untuk supplier ".$kode_supplier." - ".$nama_supplier." sudah ada..!";
					$this->load->view('template',$data);
				}
			}
			else { // jika sama
				$this->db->query(" UPDATE tm_pembelian_makloon_faktur SET id_supplier = '$supplier', 
									jenis_pembelian = '$jenis_pembelian', no_faktur = '$no_fp', 
									tgl_faktur = '$tgl_fp', tgl_update= '$tgl', jumlah = '$jum', sisa ='$jum',
									uid_update_by='$uid_update_by' where id= '$id_faktur' "); 
				
					$list_sj_lama = explode(",", $no_sj_lama);
					$list_id_sj_lama = explode(";", $id_sj_lama);
					
					for($j=0; $j<count($list_sj_lama)-1; $j++){
							$this->db->query(" UPDATE tm_pembelian_makloon SET status_faktur = 'f', no_faktur = '' 
									WHERE id = '".$list_id_sj_lama[$j]."' "); 
					}
					//-------------------------------------------------------------------------
				
					$this->db->query(" DELETE FROM tm_pembelian_makloon_faktur_detail where id_pembelian_makloon_faktur= '$id_faktur' ");
					
					$list_sj = explode(",", $no_sj); 
					$list_id_sj = explode(";", $id_sj); 
					
					foreach($list_id_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') {
							$data_detail = array(
							  'id_pembelian_makloon_faktur'=>$id_faktur,
							  'id_pembelian_makloon'=>$row1,
							);
							$this->db->insert('tm_pembelian_makloon_faktur_detail',$data_detail);
							
							// update status_faktur dan jenis_pembelian di tabel tm_pembelian_makloon
							$this->db->query(" UPDATE tm_pembelian_makloon SET no_faktur = '$no_fp', status_faktur = 't',
												jenis_pembelian = '$jenis_pembelian'
												WHERE id = '$row1' ");
						}
					}
					
					if ($carinya == '') $carinya = "all";
					if ($is_cari == 0)
						$url_redirectnya = "makloon/cform/viewfakturbisbisan/index/".$cur_page;
					else
						$url_redirectnya = "makloon/cform/carifakturbisbisan/".$cjenis_beli."/".$cstatus_lunas."/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
			}	
  }
  
  // 28-08-2015
  function show_popup_op(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		
	$id_sup 	= $this->uri->segment(4);
	$jenis_pembelian 	= $this->uri->segment(5);
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' && ($id_sup == '' || $jenis_pembelian == '') ) {
		$id_sup 	= $this->uri->segment(4);
		$jenis_pembelian 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
		
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($id_sup == '')
		$id_sup = $this->input->post('id_sup', TRUE);  
	if ($jenis_pembelian == '')
		$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
	
	//$jum_total = $this->mmaster->get_opbisbisantanpalimit($id_sup, $jenis_pembelian, $keywordcari); 
	$data['query'] = $this->mmaster->get_opbisbisan_forbtb($id_sup, $jenis_pembelian, $keywordcari);
	//$data['jum_total'] = count($jum_total);
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['id_sup'] = $id_sup;
	$data['jenis_pembelian'] = $jenis_pembelian;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	// 04-07-2015
	$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_sup' ");
	$hasilrow = $query3->row();
	if ($query3->num_rows() != 0) {
		$kode_supplier	= $hasilrow->kode_supplier;
		$nama_supplier	= $hasilrow->nama;
	}
	else {
		$kode_supplier = '';
		$nama_supplier = "Lain-lain";
	}
	$data['kode_supplier'] = $kode_supplier;
	$data['nama_supplier'] = $nama_supplier;
	$this->load->view('makloon/vpopupop',$data);
  }
  
  // 14-09-2015, cetak bukti penerimaan brg
  function print_sj_bisbisan(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$id_sj 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_pembelian_bisbisan($id_sj);
	$data['datasetting'] = $this->mmaster->get_perusahaan();
	$datasj = $data['query'];
	
	$uid_updatestok_by = $datasj[0]['uid_updatestok_by'];

	// ambil nama usernya
	$sqlxx = " SELECT nama FROM tm_user WHERE uid = '$uid_updatestok_by' ";
	$queryxx	= $this->db->query($sqlxx);
	if ($queryxx->num_rows() > 0){
		$hasilxx = $queryxx->row();
		$nama_user = $hasilxx->nama;
		$data['namastaf'] = $nama_user;
		
	}
	else
		$data['namastaf'] = '';

	$data['id_sj'] = $id_sj;
	$data['uid_updatestok_by'] = $uid_updatestok_by;
	$this->load->view('makloon/vprintsj',$data);

  }
  
  // 30-09-2015
  function edittglbtbbisbisan(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	$is_simpan 	= $this->input->post('is_simpan', TRUE);
	
	if ($is_simpan == '') {
		$id_pembelian 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$csupplier 	= $this->uri->segment(7);
		$tgl_awal 	= $this->uri->segment(8);
		$tgl_akhir 	= $this->uri->segment(9);
		$carinya 	= $this->uri->segment(10);
		$caribrg 	= $this->uri->segment(11);
		$filterbrg 	= $this->uri->segment(12);
		
		$query3	= $this->db->query(" SELECT no_sj, tgl_sj, id_supplier FROM tm_pembelian_makloon WHERE id = '$id_pembelian' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$no_sj = $hasilrow->no_sj;
			$tgl_sj = $hasilrow->tgl_sj;
			$id_supplier = $hasilrow->id_supplier;
			
			$pisah1 = explode("-", $tgl_sj);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_sj = $tgl1."-".$bln1."-".$thn1;
			
			// ambil data nama supplier
			$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_supplier' ");
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
		}
		else {
			$no_sj = '';
			$tgl_sj = '';
			$kode_supplier = '';
			$nama_supplier = '';
		}
		
		$data['msg'] = '';
		$data['id_pembelian'] = $id_pembelian;
		$data['no_sj'] = $no_sj;
		$data['tgl_sj'] = $tgl_sj;
		$data['id_supplier'] = $id_supplier;
		$data['kode_supplier'] = $kode_supplier;
		$data['nama_supplier'] = $nama_supplier;
		
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['csupplier'] = $csupplier;
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['carinya'] = $carinya;
		$data['caribrg'] = $caribrg;
		$data['filterbrg'] = $filterbrg;

		$data['isi'] = 'makloon/vedittglbtbbisbisan';
		$this->load->view('template',$data);
	}
	else { // simpan
		$id_pembelian 	= $this->input->post('id_pembelian', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
		$pisah1 = explode("-", $tgl_sj);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_sj = $thn1."-".$bln1."-".$tgl1;
		$tgl = date("Y-m-d H:i:s");
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$csupplier = $this->input->post('csupplier', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
		
		// update tglnya
		$this->db->query(" UPDATE tm_pembelian_makloon SET tgl_sj = '$tgl_sj', tgl_update = '$tgl'
							WHERE id= '$id_pembelian' ");
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "makloon/cform/viewbtbbisbisan/index/".$cur_page;
		else
			$url_redirectnya = "makloon/cform/caribtbbisbisan/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
			
		redirect($url_redirectnya);
	}

  }
  
  // 03-10-2015
  function migrasinamabrg() {
	  $sql = " SELECT id, id_brg FROM tm_pembelian_makloon_detail ";
	  $query	= $this->db->query($sql);
	  if ($query->num_rows() > 0){
		$hasil=$query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
						
			$this->db->query(" UPDATE tm_pembelian_makloon_detail SET nama_brg = '$nama_brg' WHERE id = '$row1->id' ");
		}
	  }
	  echo "sukses migrasi nama_brg di tm_pembelian_makloon_detail <br>";
  }
}
