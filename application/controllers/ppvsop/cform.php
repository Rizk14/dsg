<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('ppvsop/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_jenis_barang'] = $this->mmaster->get_jenis_barang(); 
	$data['list_kelompok_barang'] = $this->mmaster->get_kelompok_barang();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'ppvsop/vmainform';
	$this->load->view('template',$data);
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);
	$kel_brg = $this->input->post('kel_brg', TRUE);  
	$jns_brg = $this->input->post('jns_brg', TRUE); 
	// 12-05-2015
	$jenis = $this->input->post('jenis', TRUE);
	// 07-12-2015
	//~ $kalkulasi_ulang_so = $this->input->post('kalkulasi_ulang_so', TRUE);  
	//echo $kalkulasi_ulang_so; die();
	//~ $format_harga = $this->input->post('format_harga', TRUE);  

	//~ if ($jenis == '1') {
		//~ $data['query'] = $this->mmaster->get_mutasi_stok_gudang($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		//~ $data['isi'] = 'ppvsop/vformviewgudang';
		//~ $data['nama_jenis'] = 'Stok saja (tanpa harga)';
	//~ }
	//~ else {
		$data['query'] = $this->mmaster->get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		$data['isi'] = 'ppvsop/vformview';
		//~ $data['nama_jenis'] = 'Lengkap (dengan harga)';
	//~ }
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	// 12-05-2015
	$data['jenis'] = $jenis;
	//~ $data['format_harga'] = $format_harga;
	//~ $data['kalkulasi_ulang_so'] = $kalkulasi_ulang_so;
	
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
									FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
	}
	else {
		$kode_gudang	= '';
		$nama_gudang	= '';
		$nama_lokasi	= '';
	}
	
	if ($kel_brg != '0') {
		$query6	= $this->db->query(" SELECT kode_perkiraan, nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		if ($query6->num_rows() > 0){
			$hasilrow6 = $query6->row();
			$kode_perkiraan	= $hasilrow6->kode_perkiraan;
			$nama_kelompok	= $hasilrow6->nama;
		}
		else {
			$nama_kelompok = '';
			$kode_perkiraan = '';
		}
	}
	else {
		$nama_kelompok = 'Semua';
			$kode_perkiraan = '';
	}
	
	if ($jns_brg != '0') {
		$query8	= $this->db->query(" SELECT kode, nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		if ($query8->num_rows() > 0){
			$hasilrow8 = $query8->row();
			$kode_jenis_brg	= $hasilrow8->kode;
			$nama_jenis_brg	= $hasilrow8->nama;
		}
		else {
			$nama_jenis_brg = '';
			$kode_jenis_brg = '';
		}
	}
	else {
		$kode_jenis_brg = '';
		$nama_jenis_brg = "Semua";
	}
	
	$data['kode_jenis_brg'] = $kode_jenis_brg;
	$data['nama_jenis_brg'] = $nama_jenis_brg;
	$data['kode_perkiraan'] = $kode_perkiraan;
	$data['nama_kelompok'] = $nama_kelompok;
	
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['jns_brg'] = $jns_brg;
	$data['kelompok'] = $kel_brg;
	$this->load->view('template',$data);
  }
  function exportexcel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$date_from = $this->input->post('date_from', TRUE);
			$date_to = $this->input->post('date_to', TRUE);  
			$gudang = $this->input->post('gudang', TRUE);
			$kel_brg = $this->input->post('kel_brg', TRUE);  
			$jns_brg = $this->input->post('jns_brg', TRUE);
			$export_excel1 = $this->input->post('export_excel', TRUE);  
			$export_ods = $this->input->post('jns_brg', TRUE);
				
		$query = $this->mmaster->get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='8' align='center'>DATA PP VS OP</th>
		 </tr>
		 <tr>	
			<th colspan='8' align='center'>Dari Tanggal $date_from s/d $date_to  </th>	
		 </tr>
		<tr >
		  <th>Nomor PP</th>
		 <th>Tanggal PP</th>
		 <th>Departemen</th>
		 <th>List Barang</th>
		 <th>Satuan</th>
	
		 <th>Qty</th>
		 <th>Qty Pemenuhan OP</th>
		 <th>Qty Sisa</th>
	 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++) {

			
						 
			 	 $bl	= array(
				 	'01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'November',
					'12'=>'Desember'
				 );
				 
			 	 $etgl	= explode("-",$query[$j]['tgl_pp'],strlen($query[$j]['tgl_pp'])); // YYYY-mm-dd
				 $hr	= substr($etgl[2],0,1)==0?substr($etgl[2],1,1):$etgl[2];
				 $nbl	= $etgl[1];
				 $ntgl	= $hr." ".$bl[$nbl]." ".$etgl[0];
				 
				 $etgl_last	= explode("-",$query[$j]['tgl_update'],strlen($query[$j]['tgl_update'])); // YYYY-mm-dd
				 $hr_last	= substr($etgl_last[2],0,1)==0?substr($etgl_last[2],1,1):$etgl_last[2];
				 $nbl_last	= $etgl_last[1];
				 $ntgl_last	= $hr_last." ".$bl[$nbl_last]." ".$etgl_last[0];
				 
				 $html_data .= "<tr class=\"record\">";
				 $html_data .= "<td nowrap>".$query[$j]['no_pp']."</td>";
				 $html_data .= "<td>".$ntgl."</td>";
				 $html_data .= "<td>".$query[$j]['nama_bagian']."</td>";
				 
				 $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data .= $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				 
				 $html_data .= "<td nowrap>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data .= $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
		
				 
				 $html_data .= "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data .= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				 
				 $html_data .= "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data .= number_format($var_detail[$k]['jum_op'],2,'.','');
						  if ($var_detail[$k]['count_op'] > 1)
							 $html_data .= " (".$var_detail[$k]['count_op']." OP)";
						  if ($k<$hitung-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				 
				 $html_data .= "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data .= number_format($var_detail[$k]['sisanya'],2,'.','');
						  if ($k<$hitung-1)
						     $html_data .= "<br> ";
					}
				 }
				 $html_data .= "</td>";
				 

				$html_data .= "</tr>";
					}
		   } 
			

		 $html_data.= "</tbody>
		</table>";

		$nama_file = "ppvsop";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
}
