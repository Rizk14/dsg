<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('listdofcperproduk/mclass');
	}
	
	function index() {
		
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['disabled']	= 'f';
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi']	= 'listdofcperproduk/vmainform';
			$this->load->view('template',$data);
			
		}	
	
	
	function viewdata() {
 	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$d_first	= $this->input->post('d_op_first');
		$d_last	= $this->input->post('d_op_last');

		$d_op_first1 	= 	$this->uri->segment(4);
		$d_op_last1 	= 	$this->uri->segment(5);
		//var_dump($d_op_first1);
		
		$data['tglmulai']	= $d_first;
		$data['tglakhir']	= $d_last;
		
		$e_d_do_first	= explode("/",$d_first,strlen($d_first));
		$e_d_do_last	= explode("/",$d_last,strlen($d_last));
		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";

		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;

		if ($d_first == null){
		$data['query']	= $this->mclass->getqtyperproduk($d_op_first1,$d_op_last1);
		}
		else
		{
		$data['query']	= $this->mclass->getqtyperproduk($n_d_do_first,$n_d_do_last);
		}
			$data['isi']	= 'listdofcperproduk/vlistform';
			$this->load->view('template',$data);
		
	}


	function viewdata2() {
 	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$d_first	= $this->input->post('d_op_first');
		$d_last		= $this->input->post('d_last');

		
		$data['tglmulai']	= $d_first;
		$data['tglakhir']	= $d_last;
		
		$e_d_do_first	= explode("/",$d_first,strlen($d_first));
		$e_d_do_last	= explode("/",$d_last,strlen($d_last));

		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";

		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;

		$pisah 	= explode("-", $n_d_do_first);
		$tgl 		= $pisah[0];
		$bln 		= $pisah[1];
		$thn 		= $pisah[2];
		$periode	= $tgl."".$bln;


		$data['periode']	= $periode;
		
		$data['query']	       = $this->mclass->getqtyperproduk($n_d_do_first,$n_d_do_last);
		$data['bonmmasukitem']	= $this->mclass->getbonmmasukitem($n_d_do_first,$n_d_do_last, $periode);
			$data['isi']	= 'listdofcperproduk/vlistform2';
			$this->load->view('template',$data);
		
	}
/*
	function update() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$jml				= $this->input->post('jml', TRUE);
			{
				$this->load->model('listdofcperproduk/mclass');
				
				for($iter=0;$iter<=$jml;$iter++){
					for ($xx=0; $xx<count($i_color[$x]); $xx++) {
					$periode		= $this->input->post('periode'.$iter.$xx, TRUE);
					$i_product		= $this->input->post('i_product_tblItem_'.$iter.$xx, TRUE);
					$e_product_name	= $this->input->post('e_product_name_tblItem_'.$iter.$xx, TRUE);
					$i_color		= $this->input->post('i_color_'.$iter.$xx, TRUE);
					$qty_warna		= $this->input->post('qty_warna_'.$iter.$xx, TRUE);	
	
				$this->mclass->update_saldo_awalfc($periode, $i_product, $e_product_name, $i_color, $qty_warna, $iter);		}
					}	
				
				
			}  
			$data['isi']	= 'listdofcperproduk/vmainform';
			$this->load->view('template',$data);
			
}

*/

function update() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iteration= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration;


		$date= $this->input->post('d_op_first', TRUE);
		$xx=explode("/",$date);
		$th=$xx[2];
		$bl=$xx[1];
		$periode	=$th.$bl;



		$i_product= array();
		$e_product_name	= array();
		$n_quantity_warna	= array();
    	$i_product_color = array();
    	$i_color = array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem'.'_'.'0');

		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_tblItem_'.$cacah)?$this->input->post('i_product_tblItem_'.$cacah):$this->input->get_post('i_product_tblItem_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_tblItem_'.$cacah)?$this->input->post
			('e_product_name_tblItem_'.$cacah):$this->input->get_post('e_product_name_tblItem_'.$cacah);
			$n_quantity_warna[$cacah]	= $this->input->post('qty_warna_'.$cacah);
			$i_product_color[$cacah]	= $this->input->post('i_product_color_'.$cacah);
			$i_color[$cacah]	= $this->input->post('i_color_'.$cacah);
			
		}

 		$this->load->model('listdofcperproduk/mclass');
 		$this->mclass->deletesfc($periode, $i_product, $e_product_name, $n_quantity_warna, $i_product_color, $i_color,$iterasi);

		$this->mclass->msimpanbaru($periode, $i_product, $e_product_name, $n_quantity_warna, $i_product_color, $i_color,$iterasi);
		//var_dump($e_product_name);

			}






//========================================================================================================================

function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['page_title']	= 'BARANG JADI';
		$iterasi	= $this->uri->segment(4);
		$data['iterasi']	= $iterasi;		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('listdofcperproduk/mclass');

		$query	= $this->mclass->listbrgjadi();
		
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = base_url().'index.php/listdofcperproduk/cform/listbarangjadinext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->listbrgjadiperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('listdofcperproduk/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$data['page_title']	= 'BARANG JADI';
		$iterasi	= $this->uri->segment(4);
		$data['iterasi']	= $iterasi;
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('listdofcperproduk/mclass');

		$query	= $this->mclass->listbrgjadi();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = base_url().'index.php/listdofcperproduk/cform/listbarangjadinext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->listbrgjadiperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('listdofcperproduk/vlistformbrgjadi',$data);
	}
	
	function flistbarangjadi(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$key	= strtoupper($this->input->post('key'));

		$data['page_title']	= "BARANG JADI";
		$data['lurl']		= base_url();

		$this->load->model('listdofcperproduk/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		
		if($jml>0) {
		
			$cc	= 1; 
			foreach($query->result() as $row) {
			
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->f_stop_produksi')\">".$row->imotif."</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->f_stop_produksi')\">".$row->motifname."</a></td>
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->f_stop_produksi')\">".$row->qty."</a></td>
				 </tr>";

				 $cc+=1;
			}
		} else {
			$list	= "";
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;		
	}
	

	function additemwarna(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$db2=$this->load->database('db_external', TRUE);
		$iproductmotif 	= $this->input->post('iproductmotif', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
									WHERE a.i_color = b.i_color AND a.i_product_motif = '".$iproductmotif."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'i_product_color'=> $rowxx->i_product_color,
										'i_color'=> $rowxx->i_color,
										'e_color_name'=> $rowxx->e_color_name
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['iproductmotif'] = $iproductmotif;
		$data['posisi'] = $posisi;
		$this->load->view('listdofcperproduk/vlistwarna', $data); 
		return true;
  }
}
?>
