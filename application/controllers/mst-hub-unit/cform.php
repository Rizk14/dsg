<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-hub-unit/mmaster');
  }
  
  
  function index(){
	 $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		
		$data['msg']= '';
		$data['isi']= 'mst-hub-unit/vmainform';
		$this->load->view('template',$data);
	  
	  }
	  
	  function edit(){
	 $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$id=$this->uri->segment(4);
		$data['msg']= '';
		$data['query']= $this->mmaster->getdataunit($id);
		
		$data['id']= $id;
		$data['isi']= 'mst-hub-unit/veditform';
		$this->load->view('template',$data);
	  
	  }
	  
	  
	 function show_popup_packing(){
		 $is_logged_in = $this->session->userdata('is_logged_in');
		 if(!isset($is_logged_in) || $is_logged_in!= true){
			 redirect ('loginform');
			 }
		 
		$data['unit_packing'] = $this->mmaster->pop_get_unit_packing();
		$this->load->view('mst-hub-unit/vpopuppacking',$data);
		 
		 }
		 
		 
	function show_popup_jahit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in!= true){
			redirect('loginform');
			}
			
		$data['unit_jahit'] =$this->mmaster->pop_get_unit_jahit();
		$this->load->view('mst-hub-unit/vpopupjahit',$data);	
		}
		
		
	function submit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in!= true){
			redirect('loginform');
			}
	
	$nama_kelompok =$this->input->post('nama_kelompok');
	
	$nama_unit_jahit= $this->input->post('nama_unit_jahit');
	$id_unit_jahit=$this->input->post('id_unit_jahit');
	$kode_unit_jahit=$this->input->post('kode_unit_jahit');
	
	$nama_unit_packing= $this->input->post('nama_unit_packing');
	$id_unit_packing=$this->input->post('id_unit_packing');
	$kode_unit_packing=$this->input->post('kode_unit_packing');

	$id_unit_packing=trim($id_unit_packing);
	$id_unit_jahit=trim($id_unit_jahit);
	
		
	$cekdata= $this->mmaster->cekdataunit($id_unit_jahit,$id_unit_packing);
	
	if(count($cekdata) > 0){
		redirect('mst-hub-unit/cform');
		$data['isi'] = 'mst-hub-unit/vmainform';
		$data['msg']="Data Unit Jahit".$nama_unit_jahit. "/".$nama_unit_packing."sudah ada !!" ;
		$this->load->view('template',$data);
		}
	else{
		
		$tgl=date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		$data_header = array(
			'updated_at'=>$tgl,
			'created_at'=>$tgl,
			'update_by'=>$uid_update_by,
			'nama'=>$nama_kelompok,
			'kode_unit_jahit'=>$kode_unit_jahit,
			'kode_unit_packing'=>$kode_unit_packing,
			'nama_unit_jahit'=>$nama_unit_jahit,
			'nama_unit_packing'=>$nama_unit_packing,
			'id_unit_jahit'=>$id_unit_jahit,
			'id_unit_packing'=>$id_unit_packing
			);

		$this->db->insert('tm_link_unit',$data_header);
		
		//~ $this->db->query("UPDATE tm_unit_packing SET status_link='t' where id='$id_unit_packing'");
		//~ $this->db->query("UPDATE tm_unit_jahit SET status_link='t' where id='$id_unit_jahit'");
				
		redirect('mst-hub-unit/cform/view');
	}
	}
	function update(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in!= true){
			redirect('loginform');
			}
	$id =$this->input->post('id');		
	$nama_kelompok =$this->input->post('nama_kelompok');
	
	$nama_unit_jahit= $this->input->post('nama_unit_jahit');
	$id_unit_jahit=$this->input->post('id_unit_jahit');
	$kode_unit_jahit=$this->input->post('kode_unit_jahit');
	
	$nama_unit_packing= $this->input->post('nama_unit_packing');
	$id_unit_packing=$this->input->post('id_unit_packing');
	$kode_unit_packing=$this->input->post('kode_unit_packing');

	$id_unit_packing=trim($id_unit_packing);
	$id_unit_jahit=trim($id_unit_jahit);
	
		
	$cekdata= $this->mmaster->cekdataunit($id_unit_jahit,$id_unit_packing);
	
		
		$tgl=date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		$data_header = array(
			'updated_at'=>$tgl,
			'created_at'=>$tgl,
			'update_by'=>$uid_update_by,
			'nama'=>$nama_kelompok,
			'kode_unit_jahit'=>$kode_unit_jahit,
			'kode_unit_packing'=>$kode_unit_packing,
			'nama_unit_jahit'=>$nama_unit_jahit,
			'nama_unit_packing'=>$nama_unit_packing,
			'id_unit_jahit'=>$id_unit_jahit,
			'id_unit_packing'=>$id_unit_packing
			);
		$this->db->where('id',$i);
		$this->db->update('tm_link_unit',$data_header);
		
		//~ $this->db->query("UPDATE tm_unit_packing SET status_link='t' where id='$id_unit_packing'");
		//~ $this->db->query("UPDATE tm_unit_jahit SET status_link='t' where id='$id_unit_jahit'");
				
		redirect('mst-hub-unit/cform/view');
	
	}
	function view(){
$is_logged_in=$this->session->userdata('is_logged_in');
if(!isset($is_logged_in)||($is_logged_in != TRUE)){
	redirect('loginform');
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$id_unit_jahit 	= $this->input->post('id_unit_jahit', TRUE);
	$id_unit_packing 	= $this->input->post('id_unit_packing', TRUE);
	
	if ($id_unit_jahit == '0')
		$id_unit_jahit = $this->uri->segment(4);
	if ($id_unit_packing == '0')
		$id_unit_packing = $this->uri->segment(5);
	if ($keywordcari == '0')
		$keywordcari = $this->uri->segment(6);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($id_unit_jahit == '')
		$id_unit_jahit = '0';
	if ($id_unit_packing == '')
		$id_unit_packing = '0';		
			
	$jum_total = $this->mmaster->getAllkeltanpalimit($keywordcari, $id_unit_jahit,$id_unit_packing);
		
					$config['base_url'] = base_url().'index.php/mst-hub-unit/cform/view/'.$id_unit_jahit.'/'.$id_unit_packing.'/'.$keywordcari;
					$config['total_rows'] = count($jum_total); 
					$config['per_page'] = '10';
					$config['first_link'] = 'Awal';
					$config['last_link'] = 'Akhir';
					$config['next_link'] = 'Selanjutnya';
					$config['prev_link'] = 'Sebelumnya';
					$config['cur_page'] = $this->uri->segment(7);
					$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllkel($config['per_page'],$this->uri->segment(7), $keywordcari, $id_unit_jahit,$id_unit_packing );
		$data['jum_total'] = count($jum_total);
			
	 if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;		
	
	if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;	
			
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();		
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();		
	
	$data['id_unit_jahit'] = $id_unit_jahit;		
	$data['id_unit_packing'] = $id_unit_packing;	
	$data['isi'] = 'mst-hub-unit/vformviewkelompok';
	$this->load->view('template',$data);

}


 function deletesjmasuk(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $id 	= $this->uri->segment(4);
    $id_unit_jahit 	= $this->uri->segment(5);
	$id_unit_packing 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
	$cur_page 	= $this->uri->segment(8);
    
    $this->mmaster->deletekel($id);
    
    if ($carinya == '') $carinya = "all";

	$url_redirectnya = "mst-hub-unit/cform/view/".$id_unit_jahit.'/'.$id_unit_packing."/".$carinya."/".$cur_page;
	redirect($url_redirectnya);
  }
}
