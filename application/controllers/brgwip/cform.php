<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('brgwip/mmaster');
  }
   function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jenis_brg_wip 	= $this->input->post('jenis_brg_wip', TRUE);  
	$cstatus = $this->input->post('statusnya', TRUE);  
	
	if ($keywordcari == '' && ($jenis_brg_wip == '' || $cstatus == '')) {
		$jenis_brg_wip 	= $this->uri->segment(5);
		$cstatus 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($jenis_brg_wip == '')
		$jenis_brg_wip = '0';
		
	if ($cstatus == '')
		$cstatus = '0';
					
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_brg_wip, $cstatus);
							$config['base_url'] = base_url().'index.php/brgwip/cform/cari/index/'.$jenis_brg_wip.'/'.$cstatus.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(8), $keywordcari, $jenis_brg_wip, $cstatus);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'brgwip/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['jns_brg_wip'] = $this->mmaster->getAlljenisbrgwip();
	$data['ejenis_brg_wip'] = $jenis_brg_wip;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  function viewmaterialbrgwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'brgwip/vformviewmaterial';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllmaterialbrgjaditanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgwip/cform/viewmaterialbrgwip/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllmaterialbrgjadi($config['per_page'],$this->uri->segment(5,0), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
// utk material brg jadi
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jumdata 	= $this->input->post('jumdata', TRUE);  
		
		if ($keywordcari == '' && $jumdata == '') {
			$jumdata 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		  
		$qjum_total = $this->mmaster->get_brgjaditanpalimit($keywordcari);

				$config['base_url'] = base_url()."index.php/brgwip/cform/show_popup_brg_jadi/".$jumdata."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$data['jumdata'] = $jumdata;
	$this->load->view('brgwip/vpopupbrgjadi2',$data);
  }
   function getlistwarna(){
		//$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$kode_brg_jadi 	= $this->uri->segment(4);
		 $db2=$this->load->database('db_external',TRUE);		
		$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
												WHERE a.i_color = b.i_color AND a.i_product_motif = '".$kode_brg_jadi."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			/*foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama
									);
			} */
		}
		//else
		//	$detailwarna = '';
		
		//$data['detailwarna'] = $detailwarna;
		//$data['kode_brg_jadi'] = $kode_brg_jadi;
		//$this->load->view('brgwip/vlistwarna', $data); 
		echo json_encode($hasilxx);
  }
   function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$is_quilting 	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);
	
	if ($is_quilting == '' || $posisi == '') {
		$is_quilting 	= $this->input->post('is_quilting', TRUE);  
		$posisi 	= $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || ($is_quilting == '' && $posisi == '') ) {
			$is_quilting 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $is_quilting);
		
				$config['base_url'] = base_url()."index.php/brgwip/cform/show_popup_brg/".$is_quilting."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $is_quilting);
	$data['jum_total'] = count($qjum_total);
	$data['is_quilting'] = $is_quilting;
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$this->load->view('brgwip/vpopupbrg',$data);
  }
  function submitmaterialbrgwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$jumdata	= $this->input->post('no')-1;
		$kode_bhn_baku = "";
		$kode_warna = "";
		
		for ($i=1; $i<=$jumdata; $i++) {
			$kode_bhn_baku	.= $this->input->post('kode_'.$i).";";
			$kode_warna	.= $this->input->post('kode_warna_'.$i).";";
		}
		
		$this->mmaster->savematerialbrgjadi($kode_brg_jadi,$kode_bhn_baku, $kode_warna);
		redirect('brgwip/cform/viewmaterialbrgwip');		
  }
  
   function addmaterialbrgjadi(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	$data['isi'] = 'brgwip/vmainformmaterial';
	$data['list_warna'] = $this->mmaster->get_warna();
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg_wip 	= $this->uri->segment(4);
	if ($kel_brg_wip == '')
		$kel_brg_wip = $this->input->post('kel_brg_wip', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' && $kel_brg_wip == '') {
		$kel_brg_wip 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barang_wiptanpalimit($kel_brg_wip, $keywordcari);
	
			$config['base_url'] = base_url()."index.php/brgwip/cform/show_popup_jenis/".$kel_brg_wip."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang_wip($config['per_page'],$this->uri->segment(6), $kel_brg_wip, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['kel_brg_wip'] = $kel_brg_wip;
	$query3	= $this->db->query(" SELECT nama FROM tm_kel_brg_wip WHERE id = '$kel_brg_wip' ");
	$hasilrow = $query3->row();
	$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('brgwip/vpopupjenis',$data);
  }
   function kelbrgwip(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get_kel_brg_wip($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
			$eket = $row->keterangan;
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$enama = '';
			$eket = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['eket'] = $eket;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAllkelbrgwip();
    $data['isi'] = 'brgwip/vmainformkelbrgwip';
	$this->load->view('template',$data);
  }
   function submitkelbrgwip(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('brgwip/cform/kelbrgwip');
		}
		else
		{
			$id 	= $this->input->post('id', TRUE);
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$ket 	= $this->input->post('ket', TRUE);
			
			if ($goedit == 1) {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data_kel_brg_wip($kode);
					if (count($cek_data) == 0) { 
						$this->mmaster->save_kel_brg_wip($kode, $kodeedit, $nama, $ket, $goedit);
					}
				}
				else {
					$this->mmaster->save_kel_brg_wip($kode, $kodeedit, $nama, $ket, $goedit);
				}
			}
			else {
				$cek_data = $this->mmaster->cek_data_kel_brg_wip($kode);
				if (count($cek_data) == 0) { 
					$this->mmaster->save_kel_brg_wip($kode, $kodeedit, $nama, $ket, $goedit);
				}
			}
			
			redirect('brgwip/cform/kelbrgwip');
		}
  }
  function deletekelbrgwip(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete_kel_brg_wip($id);
    redirect('brgwip/cform/kelbrgwip');
  }
  function jenisbrgwip(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get_jenis_brg_wip($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$eid_kel_brg_wip = $row->id_kel_brg_wip;
			$ekode = $row->kode;
			$enama = $row->nama;
		}
	}
	else {
			$eid='';
			$eid_kel_brg_wip = '';
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['eid_kel_brg_wip'] = $eid_kel_brg_wip;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAlljenisbrgwip();
	$data['kel_brg_wip'] = $this->mmaster->getAllkelbrgwip();
	$data['jumdata'] = count($data['query']);
    $data['isi'] = 'brgwip/vmainformjenisbrgwip';
	$this->load->view('template',$data);
  }
   function submitjenisbrgwip(){
			$goedit 	= $this->input->post('goedit', TRUE);
			$id_jenis 	= $this->input->post('id_jenis', TRUE);
			$kode 	= $this->input->post('kode', TRUE);
			$kodeeditjenis 	= $this->input->post('kodeeditjenis', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			
			// 10-09-2015
			$id_kel_brg_wip 	= $this->input->post('kel_brg_wip', TRUE);
			
			if ($goedit == '') { // insert
				$cek_data = $this->mmaster->cek_data_jenis_brg_wip($kode);
				if (count($cek_data) == 0) { 
					$this->mmaster->save_jenis_brg_wip($id_jenis, $kode, $nama, $goedit, $kodeeditjenis, $id_kel_brg_wip);
				}
				redirect('brgwip/cform/jenisbrgwip');
			
			}
			else { // edit
				$cek_data = $this->mmaster->cek_data_jenis_brg_wip($kode);

				if ($kode != $kodeeditjenis) {
					if (count($cek_data) == 0) {
						$this->mmaster->save_jenis_brg_wip($id_jenis, $kode,$nama, $goedit, $kodeeditjenis, $id_kel_brg_wip);
						redirect('brgwip/cform/jenisbrgwip');
					}
					else
						redirect('brgwip/cform/jenisbrgwip');
				}
				else {
					$this->mmaster->save_jenis_brg_wip($id_jenis, $kode,$nama, $goedit, $kodeeditjenis, $id_kel_brg_wip);
					redirect('brgwip/cform/jenisbrgwip');
				}
			}
	//	}
  }
   function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'brgwip/vformview';
    $keywordcari = "all";
    $jenis_brg_wip = '0';
    $cstatus = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_brg_wip, $cstatus);
							$config['base_url'] = base_url().'index.php/brgwip/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '100';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $jenis_brg_wip, $cstatus);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$data['jns_brg_wip'] = $this->mmaster->getAlljenisbrgwip();
	$data['ejenis_brg_wip'] = $jenis_brg_wip;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
   function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$cjenis_brg_wip = $this->uri->segment(7);
		$cstatus = $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);
		
		$hasil = $this->mmaster->get($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode_brg;
			$enama_brg = $row->nama_brg;
			$eid_kel_brg_wip = $row->id_kel_brg_wip;
			$eid_jenis_brg_wip = $row->id_jenis_brg_wip;
			
			$query3	= $this->db->query(" SELECT b.kode as kj_brg, b.nama as nj_brg  
			FROM tm_kel_brg_wip a INNER JOIN tm_jenis_brg_wip b ON a.id = b.id_kel_brg_wip
			WHERE b.id = '$eid_jenis_brg_wip' ");
			if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$ekode_jenis	= $hasilrow->kj_brg;
			$enama_jenis	= $hasilrow->nj_brg;
			$estatus_aktif =$row->status_aktif;
			}
			else{
			$ekode_jenis	= '';
			$enama_jenis	= '';
			$estatus_aktif = $row->status_aktif;
			}
		}
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['cjenis_brg_wip'] = $cjenis_brg_wip;
		$data['cstatus'] = $cstatus;
		$data['carinya'] = $carinya;
	}
	else {
			$eid = '';
			$ekode = '';
			$enama_brg = '';
			$eid_kel_brg_wip = '';
			$eid_jenis_brg_wip = '';
			$edit = '';
			$ekode_jenis	= '';
			$enama_jenis	= '';
			$estatus_aktif = '';
	}
	$data['list_kelbrgwip'] = $this->mmaster->getAllkelbrgwip();
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['enama_brg'] = $enama_brg;	
	$data['eid_kel_brg_wip'] = $eid_kel_brg_wip;	
	$data['eid_jenis_brg_wip'] = $eid_jenis_brg_wip;	
	$data['edit'] = $edit;
	$data['ekode_jenis'] = $ekode_jenis;
	$data['enama_jenis'] = $enama_jenis;
	$data['estatus_aktif'] = $estatus_aktif;
	$data['msg'] = '';
    $data['isi'] = 'brgwip/vmainform';
    
	$this->load->view('template',$data);
  }
  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		//$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');

		} */
		
		$kode 	= $this->input->post('kode', TRUE);
		$kode_lama 	= $this->input->post('kode_lama', TRUE);
		$id_brg 	= $this->input->post('id_brg', TRUE); 
		$nama 	= $this->input->post('nama', TRUE);
		$kel_brg_wip 	= $this->input->post('kel_brg_wip', TRUE);
		$kode_jenis 	= $this->input->post('kode_jenis', TRUE);
		$id_jenis 	= $this->input->post('id_jenis', TRUE);
		$status_aktif 	= $this->input->post('status_aktif', TRUE);
								
		if ($goedit == '') { // tambah data
			$cek_data = $this->mmaster->cek_data($kode);
			if (count($cek_data) > 0) { 
				$data['isi'] = 'brgwip/vmainform';
				$data['msg'] = "Data kode ".$kode." sudah ada..!";
				
				$eid = '';
				$ekode = '';
				$enama_brg = '';
				$eid_kel_brg_wip = '';
				$eid_jenis_brg_wip = '';
				$edit = '';
				$ekode_jenis	= '';
				$enama_jenis	= '';
				$estatus_aktif = '';
				$edit = '';
					
				$data['eid'] = $eid;
				$data['ekode'] = $ekode;
				$data['enama_brg'] = $enama_brg;	
				$data['eid_kel_brg_wip'] = $eid_kel_brg_wip;	
				$data['eid_jenis_brg_wip'] = $eid_jenis_brg_wip;	
				$data['edit'] = $edit;
				$data['ekode_jenis'] = $ekode_jenis;
				$data['enama_jenis'] = $enama_jenis;
				$data['estatus_aktif'] = $estatus_aktif;
				$data['edit'] = $edit;
				$data['list_kelbrgwip'] = $this->mmaster->getAllkelbrgwip();
				$this->load->view('template',$data);
			}
			else {
				$this->mmaster->save($kode,$nama, $kel_brg_wip, $id_jenis, $id_brg, $status_aktif, $goedit);
				$data['isi'] = 'brgwip/vmainform';
				$data['msg'] = "Data kode ".$kode." berhasil disimpan";
					
				$eid = '';
				$ekode = '';
				$enama_brg = '';
				$eid_kel_brg_wip = '';
				$eid_jenis_brg_wip = '';
				$edit = '';
				$ekode_jenis	= '';
				$enama_jenis	= '';
				$estatus_aktif = '';
				$edit = '';
					
				$data['eid'] = $eid;
				$data['ekode'] = $ekode;
				$data['enama_brg'] = $enama_brg;	
				$data['eid_kel_brg_wip'] = $eid_kel_brg_wip;	
				$data['eid_jenis_brg_wip'] = $eid_jenis_brg_wip;	
				$data['edit'] = $edit;
				$data['ekode_jenis'] = $ekode_jenis;
				$data['enama_jenis'] = $enama_jenis;
				$data['estatus_aktif'] = $estatus_aktif;
				$data['edit'] = $edit;
				$data['list_kelbrgwip'] = $this->mmaster->getAllkelbrgwip();
				$this->load->view('template',$data);
			}
		} // end if goedit == ''
		else {
			if ($kode != $kode_lama) {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'brgwip/vmainform';
					$data['msg'] = "Data kode ".$kode." sudah ada..!";
						
					$edit = '1';
					$data['eid'] = $eid;
					$data['ekode'] = $ekode;
					$data['enama_brg'] = $enama_brg;	
					$data['eid_kel_brg_wip'] = $eid_kel_brg_wip;	
					$data['eid_jenis_brg_wip'] = $eid_jenis_brg_wip;	
					$data['edit'] = $edit;
					$data['ekode_jenis'] = $ekode_jenis;
					$data['enama_jenis'] = $enama_jenis;
					$data['estatus_aktif'] = $estatus_aktif;
					$data['edit'] = $edit;
					$data['list_kelbrgwip'] = $this->mmaster->getAllkelbrgwip();
					$this->load->view('template',$data);
					}
				else {
					$this->mmaster->save($kode,$nama, $kel_brg_wip, $id_jenis, $id_brg, $status_aktif, $goedit);
					redirect('brgwip/cform/view');
				}
			}
			else {
				$this->mmaster->save($kode,$nama, $kel_brg_wip, $id_jenis, $id_brg, $status_aktif, $goedit);
				redirect('brgwip/cform/view');
			}
		}
  }
    function addwarnabrgwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['msg'] = '';
	$data['list_warna'] = $this->mmaster->get_warna();
    $data['isi'] = 'brgwip/vmainformwarna';
    
	$this->load->view('template',$data);
  }
  function show_popup_brgwip(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	$keywordcari	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari=='') {
		$keywordcari 	= $this->uri->segment(4);
	}	

	if ($keywordcari=='')
		$keywordcari = "all";
			
	$qjum_total = $this->mmaster->getbarangwiptanpalimit($keywordcari);

	$config['base_url'] 	= base_url()."index.php/brgwip/cform/show_popup_brgwip/".$keywordcari."/";
	$config['total_rows'] 	= count($qjum_total); 
	$config['per_page'] 	= 10;
	$config['first_link'] 	= 'Awal';
	$config['last_link'] 	= 'Akhir';
	$config['next_link'] 	= 'Selanjutnya';
	$config['prev_link'] 	= 'Sebelumnya';
	$config['cur_page']		= $this->uri->segment(5,0);
	$this->pagination->initialize($config);
	
	$data['query']		= $this->mmaster->getbarangwip($config['per_page'],$config['cur_page'], $keywordcari);	
	$data['jum_total']	= count($qjum_total);

	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
				
	$data['startnya']	= $config['cur_page'];
	$this->load->view('brgwip/vpopupbrgwip',$data);  
  }
  function deletewarnabrgwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
    $id_brg_wip 	= $this->uri->segment(4);
    $this->mmaster->deletewarnabrgwip($id_brg_wip);
    redirect('brgwip/cform/viewwarnabrgwip');
  }
   function submitwarnabrgwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$id_brg_wip 	= $this->input->post('id_brg_wip', TRUE);
		$jumwarna	= $this->input->post('jumwarna');
		$idwarna = "";
		
		for ($i=1; $i<=$jumwarna; $i++) {
			$idwarna	.= $this->input->post('id_warna_'.$i).";";
		}
		
		$this->mmaster->savewarnabrgwip($id_brg_wip,$idwarna);
		redirect('brgwip/cform/viewwarnabrgwip');		
  }
  function viewwarnabrgwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'brgwip/vformviewwarna';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAllwarnabrgwiptanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgwip/cform/viewwarnabrgwip/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllwarnabrgwip($config['per_page'],$this->uri->segment(5,0), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
     function cariwarnabrgwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari	= $this->input->post('cari', TRUE);
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

					
    $jum_total = $this->mmaster->getAllwarnabrgwiptanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/brgwip/cform/cariwarnabrgwip/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllwarnabrgwip($config['per_page'],$this->uri->segment(6), $keywordcari);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'brgwip/vformviewwarna';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }


########ddy
  function editwarnabrgwip() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_brg_wip	= $this->uri->segment(4);
		$data['id_brg_wip']	= $id_brg_wip;
		
		//--------------------------------------------------------------
		$sqlnya	= $this->db->query(" SELECT distinct a.id_brg_wip, b.kode_brg, b.nama_brg 
							FROM tm_warna_brg_wip a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id
							WHERE id_brg_wip = '$id_brg_wip'
							ORDER BY b.kode_brg ");
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
										WHERE a.id_brg_wip = '".$rownya->id_brg_wip."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
												'nama_warna'=> $rowxx->nama
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'id_brg_wip'=> $rownya->id_brg_wip,
										'kode_brg'=> $rownya->kode_brg,
										'nama_brg'=> $rownya->nama_brg,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		
		// ================ 12-02-2014, cek apakah data udh dipake di tiap2 tabel transaksi ==============================
		$ada = 0;
				 
		 // tabel bon M masuk hasil cutting
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail WHERE id_brg_wip = '".$id_brg_wip."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // tabel Sj keluar ke jahitan
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail WHERE id_brg_wip = '".$id_brg_wip."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // tabel Sj masuk retur bhn baku dari jahitan
				 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail WHERE id_brg_wip = '".$id_brg_wip."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // tabel Sj masuk WIP
				 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail WHERE id_brg_wip = '".$id_brg_wip."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // tabel Sj keluar WIP
				 $query3	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail WHERE id_brg_wip = '".$id_brg_wip."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
		// =====================================================================
		$data['ada'] = $ada;
		$data['msg'] = '';
		$data['list_warna'] = $this->mmaster->get_warna();
		$data['query'] = $outputdata;
		$data['isi'] = 'brgwip/veditformwarna';
		
		$this->load->view('template',$data);
		//--------------------------------------------------------------
	}
	
	function updatewarnabrgwip(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
				
		$id_brg_wip 	= $this->input->post('id_brg_wip', TRUE);
		$ada 	= $this->input->post('ada', TRUE);
		$jumwarna	= $this->input->post('jumwarna');
		$idwarna = "";
		
		for ($i=1; $i<=$jumwarna; $i++) {
			$idwarna	.= $this->input->post('id_warna_'.$i).";";
		}
		
		$this->mmaster->updatewarnabrgwip($id_brg_wip, $ada, $idwarna);
		redirect('brgwip/cform/viewwarnabrgwip');
  }

########end of ddy
}
