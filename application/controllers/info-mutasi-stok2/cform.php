<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-mutasi-stok2/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_jenis_barang'] = $this->mmaster->get_jenis_barang(); 
	$data['list_kelompok_barang'] = $this->mmaster->get_kelompok_barang();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-mutasi-stok2/vmainform';
	$this->load->view('template',$data);
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);
	$kel_brg = $this->input->post('kel_brg', TRUE);  
	$jns_brg = $this->input->post('jns_brg', TRUE); 
	// 12-05-2015
	$jenis = $this->input->post('jenis', TRUE);
	// 07-12-2015
	$kalkulasi_ulang_so = $this->input->post('kalkulasi_ulang_so', TRUE);  
	//echo $kalkulasi_ulang_so; die();
	$format_harga = $this->input->post('format_harga', TRUE);  

	if ($jenis == '1') {
		$data['query'] = $this->mmaster->get_mutasi_stok_gudang($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		$data['isi'] = 'info-mutasi-stok2/vformviewgudang2';
		$data['nama_jenis'] = 'Stok saja (tanpa harga)';
	}
	elseif ($jenis == '2'){
		$data['query'] = $this->mmaster->get_mutasi_stok($date_from, $date_to, $gudang, $format_harga, $kalkulasi_ulang_so,$kel_brg,$jns_brg);
		$data['isi'] = 'info-mutasi-stok2/vformview';
		$data['nama_jenis'] = 'Lengkap (dengan harga)';
	}else{
		$data['query'] = $this->mmaster->get_mutasi_stok2($date_from, $date_to, $gudang, $format_harga, $kalkulasi_ulang_so,$kel_brg,$jns_brg);
		$data['isi'] = 'info-mutasi-stok2/vformview3';
		$data['nama_jenis'] = 'Lengkap Update(dengan harga)';
	}
	//var_dump($data['query']);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	// 12-05-2015
	$data['jenis'] = $jenis;
	$data['format_harga'] = $format_harga;
	$data['kalkulasi_ulang_so'] = $kalkulasi_ulang_so;
	
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
									FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
	}
	else {
		$kode_gudang	= '';
		$nama_gudang	= '';
		$nama_lokasi	= '';
	}
	
	if ($kel_brg != '0') {
		$query6	= $this->db->query(" SELECT kode_perkiraan, nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		if ($query6->num_rows() > 0){
			$hasilrow6 = $query6->row();
			$kode_perkiraan	= $hasilrow6->kode_perkiraan;
			$nama_kelompok	= $hasilrow6->nama;
		}
		else {
			$nama_kelompok = '';
			$kode_perkiraan = '';
		}
	}
	else {
		$nama_kelompok = 'Semua';
			$kode_perkiraan = '';
	}
	
	if ($jns_brg != '0') {
		$query8	= $this->db->query(" SELECT kode, nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		if ($query8->num_rows() > 0){
			$hasilrow8 = $query8->row();
			$kode_jenis_brg	= $hasilrow8->kode;
			$nama_jenis_brg	= $hasilrow8->nama;
		}
		else {
			$nama_jenis_brg = '';
			$kode_jenis_brg = '';
		}
	}
	else {
		$kode_jenis_brg = '';
		$nama_jenis_brg = "Semua";
	}
	
	$data['kode_jenis_brg'] = $kode_jenis_brg;
	$data['nama_jenis_brg'] = $nama_jenis_brg;
	$data['kode_perkiraan'] = $kode_perkiraan;
	$data['nama_kelompok'] = $nama_kelompok;
	
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['jns_brg'] = $jns_brg;
	$data['kelompok'] = $kel_brg;
	$this->load->view('template',$data);
  }


  function wip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang_qc();
	$data['isi'] = 'info-mutasi-stok2/vmainformwip';
	$this->load->view('template',$data);

  }


  function viewwip(){ // modif 12-11-2015
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
    
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	$jenis_lap = $this->input->post('jenis_lap', TRUE);  
	
	if($jenis_lap==1){
	$data['query'] = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['isi'] = 'info-mutasi-stok2/vformview';
}
elseif($jenis_lap==2){
	$data['query'] = $this->mmaster->get_mutasi_stok_wip_dg_w($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['isi'] = 'info-mutasi-stok2/vformviewbarangbhn';
}
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	
	
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }





    function export_excel_mutasi2() {

		

		$date_from  = $this->uri->segment(4);
		$date_to = $this->uri->segment(5);
		$gudang = $this->uri->segment(6);


		$this->load->model('info-mutasi-stok2/mmaster');
		$query = $this->mmaster->get_mutasi_stok_wip_dg_w($date_from, $date_to, $gudang);

		$html_data = "
		<h4><b>Periode:</b> $date_from s.d. $date_to<br></h4> 
		<table class='table table-bordered' align='center' style='width:100%;' border='1'>
	<thead>
	 <tr class='judulnya'>
		 <th width='3%' rowspan='2'>Kode</th>
		 <th width='13%' rowspan='2'>Nama Brg WIP</th>
		 <th width='8%' rowspan='2'>Warna</th>
		 <!--<th width='8%' rowspan='2'>Satuan</th>-->
		 <th width='3%' rowspan='2'>Saldo<br>Awal</th>
		 <th colspan='6'>Masuk</th>
		 <th colspan='5'>Keluar</th>
		 <th width='3%' rowspan='2'>Saldo Akhir</th>
		 <th width='3%' rowspan='2'>Stok Opname</th>
		 <th width='3%' rowspan='2'>Selisih</th>
		
	 </tr>
	 <tr class='judulnya'>
			<th width='3%'>Bgs</th>
			<th width='3%'>Hsl <br> Perbaikan</th>
			 <th width='3%'>Retur <br>Unit<br> Packing</th>
			 <th width='3%'>Retur <br>Gdg <br>Jadi</th>
			 <th width='3%'>Lain2</th>
			 <th width='3%'>Total</th>
			 <th width='3%'>Bgs Packing</th>
			 <th width='3%'>Bgs Gdg Jadi</th>
			 <th width='3%'>Retur Perbaikan</th>
			 <th width='3%'>Lain2</th>
			 <th width='3%'>Total</th>
	 </tr>
	</thead>
	<tbody>";



		$group='';
		$group2='';
		$i=0;
		$x=0;		
		if($query){
		foreach ($query as $row){

		$product=$row->id_brg_wip; 



		  if($group2==''){
           $gtotsaldoawal=0;
           $gmasuk_bgs=0;
           $gmasuk_prbaikn=0;
           $gmasuk_rtrpacking=0;
           $gmasuk_rtrgdjdi=0;
           $gmasuk_lainother=0;
           $gtot_masuk=0;
           $gkeluar_bgspcking=0;
           $gkeluar_gdjdi=0;
           $gkeluar_rtrprbaikan=0;
           $gkeluar_lain=0;
           $gtot_keluar=0;
           $gsaldo_akhir=0;
           $gstokopname=0;
           $gselisih=0;

           $gtotsaldoawal=$gtotsaldoawal+$row->saldo_awal;
           $gmasuk_bgs=$gmasuk_bgs+$row->masuk_bgs;
           $gmasuk_prbaikn=$gmasuk_prbaikn+$row->masuk_prbaikn;
           $gmasuk_rtrpacking=$gmasuk_rtrpacking+$row->masuk_rtrpacking;
           $gmasuk_rtrgdjdi=$gmasuk_rtrgdjdi+$row->masuk_rtrgdjdi;
           $gmasuk_lainother=$gmasuk_lainother+$row->masuk_lainother;
           $gtot_masuk=$gtot_masuk+$row->tot_masuk;
           $gkeluar_bgspcking=$gkeluar_bgspcking+$row->keluar_bgspcking;
           $gkeluar_gdjdi=$gkeluar_gdjdi+$row->keluar_gdjdi;
           $gkeluar_rtrprbaikan=$gkeluar_rtrprbaikan+$row->keluar_rtrprbaikan;
           $gkeluar_lain=$gkeluar_lain+$row->keluar_lain;
           $gtot_keluar=$gtot_keluar+$row->tot_keluar;
           $gsaldo_akhir=$gsaldo_akhir+$row->saldo_akhir;
           $gstokopname=$gstokopname+$row->stokopname;
           $gselisih=$gselisih+$row->selisih;


          }else{

            if($group2!=$product){


$html_data.=
          	  	" <td colspan=3 align='left'>Total PerItem</td>
              	<td><b>$gtotsaldoawal</b></td>
              	<td><b>$gmasuk_bgs</b></td>
              	<td><b>$gmasuk_prbaikn</b></td>
              	<td><b>$gmasuk_rtrpacking</b></td>
              	<td><b>$gmasuk_rtrgdjdi</b></td>
              	<td><b>$gmasuk_lainother</b></td>
              	<td><b>$gtot_masuk</b></td>
              	<td><b>$gkeluar_bgspcking</b></td>
              	<td><b>$gkeluar_gdjdi</b></td>
              	<td><b>$gkeluar_rtrprbaikan</b></td>
              	<td><b>$gkeluar_lain</b></td>
              	<td><b>$gtot_keluar</b></td>
              	<td><b>$gsaldo_akhir</b></td>
              	<td><b>$gstokopname</b></td>
              	<td><b>$gselisih</b></td>";

$html_data.=  "</tr>";

               $gtotsaldoawal=0;
	           $gmasuk_bgs=0;
	           $gmasuk_prbaikn=0;
	           $gmasuk_rtrpacking=0;
	           $gmasuk_rtrgdjdi=0;
	           $gmasuk_lainother=0;
	           $gtot_masuk=0;
	           $gkeluar_bgspcking=0;
	           $gkeluar_gdjdi=0;
	           $gkeluar_rtrprbaikan=0;
	           $gkeluar_lain=0;
	           $gtot_keluar=0;
	           $gsaldo_akhir=0;
	           $gstokopname=0;
	           $gselisih=0;
                  }


               $gtotsaldoawal=$gtotsaldoawal+$row->saldo_awal;
	           $gmasuk_bgs=$gmasuk_bgs+$row->masuk_bgs;
	           $gmasuk_prbaikn=$gmasuk_prbaikn+$row->masuk_prbaikn;
	           $gmasuk_rtrpacking=$gmasuk_rtrpacking+$row->masuk_rtrpacking;
	           $gmasuk_rtrgdjdi=$gmasuk_rtrgdjdi+$row->masuk_rtrgdjdi;
	           $gmasuk_lainother=$gmasuk_lainother+$row->masuk_lainother;
	           $gtot_masuk=$gtot_masuk+$row->tot_masuk;
	           $gkeluar_bgspcking=$gkeluar_bgspcking+$row->keluar_bgspcking;
	           $gkeluar_gdjdi=$gkeluar_gdjdi+$row->keluar_gdjdi;
	           $gkeluar_rtrprbaikan=$gkeluar_rtrprbaikan+$row->keluar_rtrprbaikan;
	           $gkeluar_lain=$gkeluar_lain+$row->keluar_lain;
	           $gtot_keluar=$gtot_keluar+$row->tot_keluar;
	           $gsaldo_akhir=$gsaldo_akhir+$row->saldo_akhir;
	           $gstokopname=$gstokopname+$row->stokopname;
	           $gselisih=$gselisih+$row->selisih;
                }


		if($group=='')
          {
          	 $x=1;
   







$html_data.=         
        "<tr><td bgcolor=\"#FFFFF0\" colspan=18 style=\"font-size:12px;\" valign='middle' align='left'><b>".$x."&nbsp;&nbsp;&nbsp;".strtoupper($row->kode_brg)."&nbsp;&nbsp;&nbsp;".$row->nama_brg. "</b>";
$html_data.=  "</tr>";







			$x=1;
          }else{
            if($group!=$product)
                    {
                    	  $x++;

$html_data.=                 	       
       	  "<tr><td bgcolor=\"#FFFFF0\" colspan=18 style=\"font-size:12px;\" valign='middle' align='left'><b>".$x."&nbsp;&nbsp;&nbsp;".strtoupper($row->kode_brg)."&nbsp;&nbsp;&nbsp;".$row->nama_brg. "</b>";
$html_data.=  "</tr>";

                    }
                }

				$group=$product;
				$group2=$product;

				$i++;


$html_data.= "
			<tr>
			<td></td>
			<td></td>
			<td>$row->nama_warna</td>
			<td>$row->saldo_awal</td>
			<td>$row->masuk_bgs</td>
			<td>$row->masuk_prbaikn</td>
			<td>$row->masuk_rtrpacking</td>
			<td>$row->masuk_rtrgdjdi</td>
			<td>$row->masuk_lainother</td>
			<td>$row->tot_masuk</td>
			<td>$row->keluar_bgspcking</td>
			<td>$row->keluar_gdjdi</td>
			<td>$row->keluar_rtrprbaikan</td>
			<td>$row->keluar_lain</td>
			<td>$row->tot_keluar</td>
			<td>$row->saldo_akhir</td>
			<td>$row->stokopname</td>
			<td>$row->selisih</td>";
$html_data.=  "</tr>";

				
				 }

$html_data.= "
				<tr>
              	  	<td colspan=3 align='left'>Total PerItem</td>
                  	<td><b>$gtotsaldoawal</b></td>
                  	<td><b>$gmasuk_bgs</b></td>
                  	<td><b>$gmasuk_prbaikn</b></td>
                  	<td><b>$gmasuk_rtrpacking</b></td>
                  	<td><b>$gmasuk_rtrgdjdi</b></td>
                  	<td><b>$gmasuk_lainother</b></td>
                  	<td><b>$gtot_masuk</b></td>
                  	<td><b>$gkeluar_bgspcking</b></td>
                  	<td><b>$gkeluar_gdjdi</b></td>
                  	<td><b>$gkeluar_rtrprbaikan</b></td>
                  	<td><b>$gkeluar_lain</b></td>
                  	<td><b>$gtot_keluar</b></td>
                  	<td><b>$gsaldo_akhir</b></td>
                  	<td><b>$gstokopname</b></td>
                  	<td><b>$gselisih</b></td>";
$html_data.=  "</tr>";
				}


		

		$html_data.= "</tbody>
		</table>";
	   	$nama_file='Laporan Mutasi Gudang QC';
	    $export_excel1='xls';
	    $data=$html_data;
	    $dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
  }






    function export_excel_mutasi3() {

		

		$date_from  = $this->uri->segment(4);
		$date_to = $this->uri->segment(5);
		$gudang = $this->uri->segment(6);


		$this->load->model('info-mutasi-stok2/mmaster');
		$query = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $gudang);

		$html_data = "
		<h4><b>Periode:</b> $date_from s.d. $date_to<br></h4> 
		<table class='table table-bordered' align='center' style='width:100%;' border='1'>
	<thead>
	 <tr class='judulnya'>
	 	 <th width='1%' rowspan='2'>No</th>
		 <th width='3%' rowspan='2'>Kode</th>
		 <th width='13%' rowspan='2'>Nama Brg WIP</th>
		 <!--<th width='8%' rowspan='2'>Satuan</th>-->
		 <th width='3%' rowspan='2'>Saldo<br>Awal</th>
		 <th colspan='6'>Masuk</th>
		 <th colspan='5'>Keluar</th>
		 <th width='3%' rowspan='2'>Saldo Akhir</th>
		 <th width='3%' rowspan='2'>Stok Opname</th>
		 <th width='3%' rowspan='2'>Selisih</th>
		
	 </tr>
	 <tr class='judulnya'>
			<th width='3%'>Bgs</th>
			<th width='3%'>Hsl <br> Perbaikan</th>
			 <th width='3%'>Retur <br>Unit<br> Packing</th>
			 <th width='3%'>Retur <br>Gdg <br>Jadi</th>
			 <th width='3%'>Lain2</th>
			 <th width='3%'>Total</th>
			 <th width='3%'>Bgs Packing</th>
			 <th width='3%'>Bgs Gdg Jadi</th>
			 <th width='3%'>Retur Perbaikan</th>
			 <th width='3%'>Lain2</th>
			 <th width='3%'>Total</th>
	 </tr>
	</thead>
	<tbody>";



		$i=0;
		$gtotsaldoawal=0;
		$gmasuk_bgs=0;
		$gmasuk_prbaikn=0;
		$gmasuk_rtrpacking=0;
		$gmasuk_rtrgdjdi=0;
		$gmasuk_lainother=0;
		$gtot_masuk=0;
		$gkeluar_bgspcking=0;
		$gkeluar_gdjdi=0;
		$gkeluar_rtrprbaikan=0;
		$gkeluar_lain=0;
		$gtot_keluar=0;
		$gsaldo_akhir=0;
		$gstokopname=0;
		$gselisih=0;

		if($query){
		foreach ($query as $row){
		$i++;

$html_data.= "
			<tr>
				<td>$i</td>
				<td>$row->kode_brg</td>
				<td>$row->nama_brg</td>
				<td>$row->saldo_awal</td>
				<td>$row->masuk_bgs</td>
				<td>$row->masuk_prbaikn</td>
				<td>$row->masuk_rtrpacking</td>
				<td>$row->masuk_rtrgdjdi</td>
				<td>$row->masuk_lainother</td>
				<td>$row->tot_masuk</td>
				<td>$row->keluar_bgspcking</td>
				<td>$row->keluar_gdjdi</td>
				<td>$row->keluar_rtrprbaikan</td>
				<td>$row->keluar_lain</td>
				<td>$row->tot_keluar</td>
				<td>$row->saldo_akhir</td>
				<td>$row->stokopname</td>
				<td>$row->selisih</td>";
$html_data.=  "</tr>";

			   $gtotsaldoawal=$gtotsaldoawal+$row->saldo_awal;
	           $gmasuk_bgs=$gmasuk_bgs+$row->masuk_bgs;
	           $gmasuk_prbaikn=$gmasuk_prbaikn+$row->masuk_prbaikn;
	           $gmasuk_rtrpacking=$gmasuk_rtrpacking+$row->masuk_rtrpacking;
	           $gmasuk_rtrgdjdi=$gmasuk_rtrgdjdi+$row->masuk_rtrgdjdi;
	           $gmasuk_lainother=$gmasuk_lainother+$row->masuk_lainother;
	           $gtot_masuk=$gtot_masuk+$row->tot_masuk;
	           $gkeluar_bgspcking=$gkeluar_bgspcking+$row->keluar_bgspcking;
	           $gkeluar_gdjdi=$gkeluar_gdjdi+$row->keluar_gdjdi;
	           $gkeluar_rtrprbaikan=$gkeluar_rtrprbaikan+$row->keluar_rtrprbaikan;
	           $gkeluar_lain=$gkeluar_lain+$row->keluar_lain;
	           $gtot_keluar=$gtot_keluar+$row->tot_keluar;
	           $gsaldo_akhir=$gsaldo_akhir+$row->saldo_akhir;
	           $gstokopname=$gstokopname+$row->stokopname;
	           $gselisih=$gselisih+$row->selisih;

				
				 }

$html_data.= "
				<tr>
              	  	<td colspan=3 align='left'>Total PerItem</td>
                  	<td><b>$gtotsaldoawal</b></td>
                  	<td><b>$gmasuk_bgs</b></td>
                  	<td><b>$gmasuk_prbaikn</b></td>
                  	<td><b>$gmasuk_rtrpacking</b></td>
                  	<td><b>$gmasuk_rtrgdjdi</b></td>
                  	<td><b>$gmasuk_lainother</b></td>
                  	<td><b>$gtot_masuk</b></td>
                  	<td><b>$gkeluar_bgspcking</b></td>
                  	<td><b>$gkeluar_gdjdi</b></td>
                  	<td><b>$gkeluar_rtrprbaikan</b></td>
                  	<td><b>$gkeluar_lain</b></td>
                  	<td><b>$gtot_keluar</b></td>
                  	<td><b>$gsaldo_akhir</b></td>
                  	<td><b>$gstokopname</b></td>
                  	<td><b>$gselisih</b></td>";
$html_data.=  "</tr>";
				}


		

		$html_data.= "</tbody>
		</table>";
	   	$nama_file='Laporan Mutasi Gudang QC';
	    $export_excel1='ODS';
	    $data=$html_data;
	    $dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
  }
  

}
