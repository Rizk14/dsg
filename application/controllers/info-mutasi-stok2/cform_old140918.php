<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-mutasi-stok/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_jenis_barang'] = $this->mmaster->get_jenis_barang(); 
	$data['list_kelompok_barang'] = $this->mmaster->get_kelompok_barang();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-mutasi-stok/vmainform';
	$this->load->view('template',$data);
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);
	$kel_brg = $this->input->post('kel_brg', TRUE);  
	$jns_brg = $this->input->post('jns_brg', TRUE); 
	// 12-05-2015
	$jenis = $this->input->post('jenis', TRUE);
	// 07-12-2015
	$kalkulasi_ulang_so = $this->input->post('kalkulasi_ulang_so', TRUE);  
	//echo $kalkulasi_ulang_so; die();
	$format_harga = $this->input->post('format_harga', TRUE);  

	if ($jenis == '1') {
		$data['query'] = $this->mmaster->get_mutasi_stok_gudang($date_from, $date_to, $gudang,$kel_brg,$jns_brg);
		$data['isi'] = 'info-mutasi-stok/vformviewgudang';
		$data['nama_jenis'] = 'Stok saja (tanpa harga)';
	}
	elseif ($jenis == '2'){
		$data['query'] = $this->mmaster->get_mutasi_stok($date_from, $date_to, $gudang, $format_harga, $kalkulasi_ulang_so,$kel_brg,$jns_brg);
		$data['isi'] = 'info-mutasi-stok/vformview';
		$data['nama_jenis'] = 'Lengkap (dengan harga)';
	}else{
		$data['query'] = $this->mmaster->get_mutasi_stok2($date_from, $date_to, $gudang, $format_harga, $kalkulasi_ulang_so,$kel_brg,$jns_brg);
		$data['isi'] = 'info-mutasi-stok/vformview3';
		$data['nama_jenis'] = 'Lengkap Update(dengan harga)';
	}
	//var_dump($data['query']);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	// 12-05-2015
	$data['jenis'] = $jenis;
	$data['format_harga'] = $format_harga;
	$data['kalkulasi_ulang_so'] = $kalkulasi_ulang_so;
	
	if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
									FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
	}
	else {
		$kode_gudang	= '';
		$nama_gudang	= '';
		$nama_lokasi	= '';
	}
	
	if ($kel_brg != '0') {
		$query6	= $this->db->query(" SELECT kode_perkiraan, nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		if ($query6->num_rows() > 0){
			$hasilrow6 = $query6->row();
			$kode_perkiraan	= $hasilrow6->kode_perkiraan;
			$nama_kelompok	= $hasilrow6->nama;
		}
		else {
			$nama_kelompok = '';
			$kode_perkiraan = '';
		}
	}
	else {
		$nama_kelompok = 'Semua';
			$kode_perkiraan = '';
	}
	
	if ($jns_brg != '0') {
		$query8	= $this->db->query(" SELECT kode, nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		if ($query8->num_rows() > 0){
			$hasilrow8 = $query8->row();
			$kode_jenis_brg	= $hasilrow8->kode;
			$nama_jenis_brg	= $hasilrow8->nama;
		}
		else {
			$nama_jenis_brg = '';
			$kode_jenis_brg = '';
		}
	}
	else {
		$kode_jenis_brg = '';
		$nama_jenis_brg = "Semua";
	}
	
	$data['kode_jenis_brg'] = $kode_jenis_brg;
	$data['nama_jenis_brg'] = $nama_jenis_brg;
	$data['kode_perkiraan'] = $kode_perkiraan;
	$data['nama_kelompok'] = $nama_kelompok;
	
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['jns_brg'] = $jns_brg;
	$data['kelompok'] = $kel_brg;
	$this->load->view('template',$data);
  }
  
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		set_time_limit(36000);
		ini_set("memory_limit","512M");
		ini_set("max_execution_time","36000");
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE); 
		$kel_brg = $this->input->post('kelompok', TRUE);  
	$jns_brg = $this->input->post('jns_brg', TRUE);  
		// 12-05-2015
		$jenis = $this->input->post('jenis', TRUE);
		$format_harga = $this->input->post('format_harga', TRUE);  
		// 08-12-2015
		//$kalkulasi_ulang_so = $this->input->post('kalkulasi_ulang_so', TRUE);  
		$kalkulasi_ulang_so = '';
		
		if ($jenis == '1') { // GUDANG AJA, TANPA HARGA
			$query = $this->mmaster->get_mutasi_stok_gudang($date_from, $date_to, $id_gudang,$kel_brg,$jns_brg);
			$nama_file = "laporan_mutasi_stok_gudang";
			
			$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='13' align='center'>LAPORAN MUTASI STOK BAHAN BAKU/PEMBANTU</th>
		 </tr>
		 <tr>
			<th colspan='13' align='center'>Lokasi Gudang: ";
			if ($id_gudang != 0)
				$html_data.="[$nama_lokasi] $kode_gudang-$nama_gudang";
			else
				$html_data.="Semua";
		 $html_data.="</th></tr>
		 <tr>
			<th colspan='13' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
			 <tr>
				 <th>No</th>
				 <th>Kode Brg</th>
				 <th>Nama Barang</th>
				 <th>Sat Awal</th>
				 <th>Sat Konversi</th>
				 <th width='5%'>Saldo Awal</th>
				 <th width='5%'>Masuk</th>
				 <th width='5%'>Masuk Lain</th>
				 <th width='5%'>Keluar</th>
				 <th width='5%'>Keluar Lain</th>
				 <th width='5%'>Saldo Akhir</th>
				 <th width='5%'>SO</th>
				 <th width='5%'>Selisih</th>
			 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
				$tkodegudang="";
				$totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
				$totsaldoakhir=0; $totso = 0; $totselisih=0;
				
			 for($j=0;$j<count($query);$j++){
				 
				 if ($tkodegudang != $query[$j]['kode_gudang']) {
					 $tkodegudang = $query[$j]['kode_gudang'];
				 
				 $html_data.="<tr>
						<td colspan='13'><b>".$query[$j]['kode_gudang']." - ".$query[$j]['nama_gudang']."</b></td>
					</tr>";
				 }
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td>".$query[$j]['satuan']."</td>
				 <td>".$query[$j]['nama_satuan_konv']."</td>
				 <td align='right'>".$query[$j]['saldo_awal']."</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain']."</td>
				 <td align='right'>".$query[$j]['jum_keluar']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain']."</td> ";
				//$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				
				$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				 
				$html_data.= "<td align='right'>".$saldo_akhir."</td>";
				$html_data.= "<td align='right'>".$query[$j]['jum_stok_opname']."</td> ";
				
				$selisih = $query[$j]['jum_stok_opname']-$saldo_akhir;
				$selisih = round($selisih, 2);
				 
				$html_data.= "<td align='right'>".$selisih."</td> ";
				$html_data.=  "</tr>";			
								 
				 /*if (isset($query[$j+1]['kode_gudang']) && ($query[$j]['kode_gudang'] != $query[$j+1]['kode_gudang'])) {
					 $html_data.= "<tr>
						<td colspan='6' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".$totsaldoawal."</b></td>
						<td colspan='2' align='right'><b>".$totmasuk."</b></td>
						<td colspan='2' align='right'><b>".$totmasuklain."</b></td>
						<td colspan='2' align='right'><b>".$totkeluar."</b></td>
						<td colspan='2' align='right'><b>".$totkeluarlain."</b></td>
						<td colspan='2' align='right'><b>".$totsaldoakhir."</b></td>
						<td colspan='2' align='right'><b>".$totso."</b></td>
						<td colspan='2' align='right'><b>".$totselisih."</b></td>
					 </tr>";
					 
					 $totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
					$totsaldoakhir=0; $totso = 0; $totselisih=0;
				 }
				 else if (!isset($query[$j+1]['kode_gudang'])) {
					 $html_data.= "<tr>
						<td colspan='6' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".$totsaldoawal."</b></td>
						<td colspan='2' align='right'><b>".$totmasuk."</b></td>
						<td colspan='2' align='right'><b>".$totmasuklain."</b></td>
						<td colspan='2' align='right'><b>".$totkeluar."</b></td>
						<td colspan='2' align='right'><b>".$totkeluarlain."</b></td>
						<td colspan='2' align='right'><b>".$totsaldoakhir."</b></td>
						<td colspan='2' align='right'><b>".$totso."</b></td>
						<td colspan='2' align='right'><b>".$totselisih."</b></td>
					 </tr>";
				 } */
		 	}
		   }
		  
		   
		 $html_data.= "</tbody>
		</table>";
		}
		else {
			$query = $this->mmaster->get_mutasi_stok($date_from, $date_to, $id_gudang, $format_harga, $kalkulasi_ulang_so,$kel_brg,$jns_brg);
			$nama_file = "laporan_mutasi_stok";
			
			$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='23' align='center'>LAPORAN MUTASI STOK BAHAN BAKU/PEMBANTU</th>
		 </tr>
		 <tr>
			<th colspan='23' align='center'>Lokasi Gudang: ";
			if ($id_gudang != 0)
				$html_data.="[$nama_lokasi] $kode_gudang-$nama_gudang";
			else
				$html_data.="Semua";
		 $html_data.="</th></tr>
		 <tr>
			<th colspan='23' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
			 <tr>
				 <th rowspan='2'>No</th>
				 <th rowspan='2'>Kode Brg</th>
				 <th rowspan='2'>Nama Barang</th>
				 <th rowspan='2'>Sat Awal</th>
				 <th rowspan='2'>Sat Konversi</th>
				 <th rowspan='2'>Harga</th>
				 <th rowspan='2'>PKP</th>
				 <th width='5%' colspan='2'>Saldo Awal</th>
				 <th width='5%' colspan='2'>Masuk</th>
				 <th width='5%' colspan='2'>Masuk Lain</th>
				 <th width='5%' colspan='2'>Keluar</th>
				 <th width='5%' colspan='2'>Keluar Lain</th>
				 <th width='5%' colspan='2'>Saldo Akhir</th>
				 <th width='5%' colspan='2'>SO</th>
				 <th width='5%' colspan='2'>Selisih</th>
			 </tr>
	 <tr>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
	 </tr>
		 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
				$tkodegudang="";
				$totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
				$totsaldoakhir=0; $totso = 0; $totselisih=0;
				
			 for($j=0;$j<count($query);$j++){
				 
				 if ($tkodegudang != $query[$j]['kode_gudang']) {
					 $tkodegudang = $query[$j]['kode_gudang'];
				 
				 $html_data.="<tr>
						<td colspan='23'><b>".$query[$j]['kode_gudang']." - ".$query[$j]['nama_gudang']."</b></td>
					</tr>";
				 }
				 $html_data.= "<tr class=\"record\">
				 <td align='center' valign='middle'>".($j+1)."</td>
				 <td valign='middle'>".$query[$j]['kode_brg']."</td>
				 <td valign='middle'>".$query[$j]['nama_brg']."</td>";
				 
				 $html_data.="<td>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 //<td valign='middle'>".$query[$j]['satuan']."</td>
				 $html_data.="<td>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_satuan_konv'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 //<td valign='middle'>".$query[$j]['nama_satuan_konv']."</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['harga'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_pkp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['saldo_awal'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totsaldoawal+= $var_detail[$k]['saldo_awal_rp'];
						  $html_data.= $var_detail[$k]['saldo_awal_rp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['jum_masuk'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totmasuk+= $var_detail[$k]['jum_masuk_rp'];
						  $html_data.= $var_detail[$k]['jum_masuk_rp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['jum_masuk_lain'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totmasuklain+= $var_detail[$k]['jum_masuk_lain_rp'];
						  $html_data.= $var_detail[$k]['jum_masuk_lain_rp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['jum_keluar'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totkeluar+= $var_detail[$k]['jum_keluar_rp'];
						  $html_data.= $var_detail[$k]['jum_keluar_rp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['jum_keluar_lain'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totkeluarlain+= $var_detail[$k]['jum_keluar_lain_rp'];
						  $html_data.= $var_detail[$k]['jum_keluar_lain_rp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 				
				//$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				//$saldo_akhir_rp = $saldo_akhir*$query[$j]['harga'];
				
				$html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['saldo_akhir'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totsaldoakhir+= $var_detail[$k]['saldo_akhir_rp'];
						  $html_data.= $var_detail[$k]['saldo_akhir_rp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				
				$html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['jum_stok_opname'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totso+= $var_detail[$k]['jum_stok_opname_rp'];
						  $html_data.= $var_detail[$k]['jum_stok_opname_rp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				
				//$selisih = abs($query[$j]['jum_stok_opname']-$saldo_akhir);
				//$selisih_rp = $selisih*$query[$j]['harga'];
				
				$html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['selisih'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totselisih+= $var_detail[$k]['selisih_rp'];
						  $html_data.= $var_detail[$k]['selisih_rp'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
					}
				 }
				 $html_data.="</td>";
				 
				$html_data.=  "</tr>";			
				
				/* $totsaldoawal+= $query[$j]['saldo_awal_rp'];
				 $totmasuk+= $query[$j]['jum_masuk_rp'];
				 $totmasuklain+= $query[$j]['jum_masuk_lain_rp'];
				 $totkeluar+= $query[$j]['jum_keluar_rp'];
				 $totkeluarlain+= $query[$j]['jum_keluar_lain_rp'];
				 $totsaldoakhir+= $saldo_akhir_rp;
				 $totso+= $query[$j]['jum_stok_opname_rp'];
				 $totselisih+= $selisih_rp; */
				 
				 if (isset($query[$j+1]['kode_gudang']) && ($query[$j]['kode_gudang'] != $query[$j+1]['kode_gudang'])) {
					 $html_data.= "<tr>
						<td colspan='7' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".$totsaldoawal."</b></td>
						<td colspan='2' align='right'><b>".$totmasuk."</b></td>
						<td colspan='2' align='right'><b>".$totmasuklain."</b></td>
						<td colspan='2' align='right'><b>".$totkeluar."</b></td>
						<td colspan='2' align='right'><b>".$totkeluarlain."</b></td>
						<td colspan='2' align='right'><b>".$totsaldoakhir."</b></td>
						<td colspan='2' align='right'><b>".$totso."</b></td>
						<td colspan='2' align='right'><b>".$totselisih."</b></td>
					 </tr>";
					 
					 $totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
					$totsaldoakhir=0; $totso = 0; $totselisih=0;
				 }
				 else if (!isset($query[$j+1]['kode_gudang'])) {
					 $html_data.= "<tr>
						<td colspan='7' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".$totsaldoawal."</b></td>
						<td colspan='2' align='right'><b>".$totmasuk."</b></td>
						<td colspan='2' align='right'><b>".$totmasuklain."</b></td>
						<td colspan='2' align='right'><b>".$totkeluar."</b></td>
						<td colspan='2' align='right'><b>".$totkeluarlain."</b></td>
						<td colspan='2' align='right'><b>".$totsaldoakhir."</b></td>
						<td colspan='2' align='right'><b>".$totso."</b></td>
						<td colspan='2' align='right'><b>".$totselisih."</b></td>
					 </tr>";
				 }
		 	}
		   }
		  
		   
		 $html_data.= "</tbody>
		</table>";
		} // end else
	
		
		//print_r($query); die();
		/*<th width='5%'>No</th>
			 <th width='20%'>Kode Brg</th>
			 <th width='30%'>Nama Barang</th>
			 <th width='15%'>Sat Awal</th>
			 <th width='7%'>Saldo Awal</th>
			 <th width='7%'>Masuk</th>
			 <th width='7%'>Masuk Lain</th>
			 <th width='7%'>Keluar</th>
			 <th width='7%'>Keluar Lain</th>
			 <th width='7%'>Saldo Akhir</th>
			 <th width='7%'>SO</th>
			 <th width='7%'>Selisih</th> */
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  // ++++++++++++++++++ 14-03-2013, mutasi brg WIP, MODIF 12-11-2015 ++++++++++++++++++++++++++++
  function wip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang_qc();
	$data['isi'] = 'info-mutasi-stok/vmainformwip';
	$this->load->view('template',$data);

  }
  
  function viewwip(){ // modif 12-11-2015
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		set_time_limit(36000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","36000");
    
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	$jenis_lap = $this->input->post('jenis_lap', TRUE);  
	
	if($jenis_lap==1){
	$data['query'] = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['isi'] = 'info-mutasi-stok/vformviewwip';
}
elseif($jenis_lap==2){
	$data['query'] = $this->mmaster->get_mutasi_stok_wip_dg_w($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['isi'] = 'info-mutasi-stok/vformviewwipdgw';
}
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	
	
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }
  
  function export_excel_wip() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='23' align='center'>LAPORAN MUTASI STOK BARANG WIP</th>
		 </tr>
		 <tr>
			<th colspan='23' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='23' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
	 <tr class='judulnya'>
		 <th width='3%' rowspan='2'>No</th>
		 <th width='15%' rowspan='2'>Kode</th>
		 <th width='25%' rowspan='2'>Nama Brg WIP</th>
		 <!--<th width='8%' rowspan='2'>Satuan</th>-->
		 <th width='8%' rowspan='2'>Saldo<br>Awal</th>
		 <th colspan='8'>Masuk</th>
		 <th colspan='7'>Keluar</th>
		 <th width='8%' rowspan='2'>Saldo Akhir</th>
		 <th width='8%' rowspan='2'>Stok Opname</th>
		  <th width='8%' rowspan='2'>Adjustment</th>
		 <th width='8%' rowspan='2'>Selisih</th>
		
	 </tr>
	 <tr class='judulnya'>
		<th width='8%'>Bgs</th>
		<th width='8%'>Hsl Perbaikan</th>
			 <th width='8%'>Retur Unit Packing</th>
			 <th width='8%'>Retur Gdg Jadi</th>
			 <th width='8%'>Lain2</th>
			  <th width='8%'>Bagus QC Lain</th>
			  <th width='8%'>Retur QC Lain</th>
			 <th width='8%'>Total</th>
			 <th width='8%'>Bgs Packing</th>
			 <th width='8%'>Bgs Gdg Jadi</th>
			 <th width='8%'>Retur Perbaikan</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Bagus QC Lain</th>
			  <th width='8%'>Retur QC Lain</th>
			 <th width='8%'>Total</th>
		 </tr>
		</thead>
		<tbody>";
		// 12-09-2014, kolom setelah retur ke unit jahit dihilangkan
		// <th width='8%'>Lainnya</th>
		
			if (is_array($query)) {
				$temp_kodekel = "";
			 for($j=0;$j<count($query);$j++){
				 if ($temp_kodekel != $query[$j]['kode_kel']) {
					 $temp_kodekel = $query[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='23'>&nbsp;<b>".$query[$j]['kode_kel']." - ".$query[$j]['nama_kel']."</b></td>
					</tr>";
				 }
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td align='right'>".$query[$j]['saldo_awal']."</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_perb_unit']."</td>
				 <td align='right'>".$query[$j]['jum_ret_unit_pack']."</td>
				 <td align='right'>".$query[$j]['jum_ret_gd_jadi']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain']."</td>
				 <td align='center'> ".$query[$j]['jum_masuk_bgs_qc']."</td>
				<td align='center'> ".$query[$j]['jum_masuk_rtr_qc']."</td>";
				 $tot_masuk = $query[$j]['jum_masuk']+$query[$j]['jum_perb_unit']+$query[$j]['jum_ret_unit_pack']
				 +$query[$j]['jum_ret_gd_jadi']+$query[$j]['jum_masuk_lain']+$query[$j]['jum_masuk_bgs_qc']+$query[$j]['jum_masuk_rtr_qc'];
				 $html_data.="<td align='right'>".$tot_masuk."</td>";
				 
				 $html_data.=
				 "<td align='right'>".$query[$j]['jum_keluar_pack']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_gdjadi']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_perb']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain']."</td>
				 <td align='center'> ".$query[$j]['jum_keluar_bgs_qc']."</td>
				<td align='center'> ".$query[$j]['jum_keluar_rtr_qc']."</td>";
				 $tot_keluar = $query[$j]['jum_keluar_pack']+$query[$j]['jum_keluar_gdjadi']+
				 $query[$j]['jum_keluar_perb']+$query[$j]['jum_keluar_lain']+$query[$j]['jum_keluar_bgs_qc']+$query[$j]['jum_keluar_rtr_qc'];
				 $html_data.="<td align='right'>".$tot_keluar."</td>";
				 
				//$saldo_akhir = $query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain1']+$query[$j]['jum_masuk_lain2']+$query[$j]['jum_masuk_lain3']+$query[$j]['jum_masuk_lain4']-$query[$j]['jum_keluar1']-$query[$j]['jum_keluar2']-$query[$j]['jum_keluar_lain1']-$query[$j]['jum_keluar_lain2'];
				$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_perb_unit']+$query[$j]['jum_ret_unit_pack']+$query[$j]['jum_ret_gd_jadi']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar_pack']-$query[$j]['jum_keluar_gdjadi']-$query[$j]['jum_keluar_perb']-$query[$j]['jum_keluar_lain'];
				$selisih = $query[$j]['jum_stok_opname']-$saldo_akhir;
				$html_data.= "<td align='right'>".$saldo_akhir."</td>
					<td align='right'>".$query[$j]['jum_stok_opname']."</td>
				<td align='right'>".$query[$j]['jum_adjustment']."</td>
			
				<td align='right'>".$selisih."</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok_wip";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
   function export_excel_wip_w() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_wip_dg_w($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='19' align='center'>LAPORAN MUTASI STOK BARANG WIP DENGAN WARNA</th>
		 </tr>
		 <tr>
			<th colspan='19' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='19' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%' rowspan='2'>No</th>
			 <th width='15%' rowspan='2'>Kode Brg</th>
			 <th width='25%' rowspan='2'>Nama Brg</th>
			 <th width='8%' rowspan='2'>S.Awal</th>
			 <th colspan='6'>Masuk</th>
			 <th colspan='5'>Keluar</th>
			 <th width='8%' rowspan='2'>Saldo Akhir</th>
			 <th width='8%' rowspan='2'>Stok Opname</th>
			 <th width='8%' rowspan='2'>Adjustment</th>
			 <th width='8%' rowspan='2'>Selisih</th>
		 </tr>
		 <tr>
			 <th width='8%'>Bgs</th>
			 <th width='8%'>Hsl Perbaikan</th>
			 <th width='8%'>Retur Unit Packing</th>
			 <th width='8%'>Retur Gdg Jadi</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Total</th>
			 <th width='8%'>Bgs Packing</th>
			 <th width='8%'>Bgs Gdg Jadi</th>
			 <th width='8%'>Retur Perbaikan</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Total</th>
		 </tr>
		</thead>
		<tbody>";
			
			if (is_array($query)) {
				
				$temp_kodekel = "";
			 for($j=0;$j<count($query);$j++){
				 if ($temp_kodekel != $query[$j]['kode_kel']) {
					 $temp_kodekel = $query[$j]['kode_kel'];
		$html_data.="
					<tr>
						<td colspan='19'>&nbsp;<b>". $query[$j]['kode_kel']." - ".$query[$j]['nama_kel'] ."</b></td>
					</tr>";
		
				 }
				
				 $html_data.= "<tr class=\"record\">";
				 $html_data.=    "<td width='2%' align='center'>".($j+1)."</td>";
				 $html_data.=    "<td>".$query[$j]['kode_brg']."</td>";
				 $html_data.=    "<td>".$query[$j]['nama_brg']."</td>";
				$html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_saldo_awal_warna=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_saldo_awal_warna += $var_detail_warna[$k]['saldo_awal_warna'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['saldo_awal_warna'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_saldo_awal_warna;
					}	
				$html_data.= "</td>";
				
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_masuk=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_masuk += $var_detail_warna[$k]['jum_masuk'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_masuk'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_masuk;
					}	
				$html_data.= "</td>";
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_masuk_perb=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_masuk_perb += $var_detail_warna[$k]['jum_perb_unit'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_perb_unit'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_masuk_perb;
					}	
				$html_data.= "</td>";
				
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_ret_unit_pack=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_ret_unit_pack += $var_detail_warna[$k]['jum_ret_unit_pack'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_ret_unit_pack'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_ret_unit_pack;
					}	
				$html_data.= "</td>";
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_ret_gd_jadi=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_ret_gd_jadi += $var_detail_warna[$k]['jum_ret_gd_jadi'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_ret_gd_jadi'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_ret_gd_jadi;
					}	
				$html_data.= "</td>";
				
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_masuk_lain=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_masuk_lain += $var_detail_warna[$k]['jum_masuk_lain'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_masuk_lain'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_masuk_lain;
					}	
				$html_data.= "</td>";
				
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_saldo_masuk_warna=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_saldo_masuk_warna += $var_detail_warna[$k]['saldo_masuk_warna'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['saldo_masuk_warna'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_saldo_masuk_warna;
					}	
				$html_data.= "</td>";
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_pack=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_pack += $var_detail_warna[$k]['jum_keluar_pack'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_pack'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_keluar_pack;
					}	
				$html_data.= "</td>";
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_gdjadi=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_gdjadi += $var_detail_warna[$k]['jum_keluar_gdjadi'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_gdjadi'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_keluar_gdjadi;
					}	
				$html_data.= "</td>";
				
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_perb=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_perb += $var_detail_warna[$k]['jum_keluar_perb'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_perb'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_keluar_perb;
					}	
				$html_data.= "</td>";
				
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_lain=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_lain += $var_detail_warna[$k]['jum_keluar_lain'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_lain'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_keluar_lain;
					}	
				$html_data.= "</td>";
				
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_total=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_total += $var_detail_warna[$k]['jum_keluar_total'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_total'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_keluar_total;
					}	
				$html_data.= "</td>";
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_saldo_akhir=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_saldo_akhir += $var_detail_warna[$k]['jum_saldo_akhir'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_saldo_akhir'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_saldo_akhir;
					}	
				$html_data.= "</td>";
				
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_stok_opname=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_stok_opname += $var_detail_warna[$k]['jum_stok_opname'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_stok_opname'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_stok_opname;
					}	
				$html_data.= "</td>";
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_adjustment_b=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_adjustment_b += $var_detail_warna[$k]['jum_adjustment_b'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_adjustment_b'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_jum_adjustment_b;
					}	
				$html_data.= "</td>";
				 $html_data.=    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_selisih=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_selisih += $var_detail_warna[$k]['selisih'];
						 $html_data.= $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['selisih'] ;
						 if(count($var_detail_warna) > 0 ){
							 $html_data.= "<br>";
							 } 
						}
						$html_data.= "TOTAL = ". $total_selisih;
					}	
				$html_data.= "</td>";
				 $html_data.=  "</tr>";					
		 						
		 	}
		   }
		
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok_wip_warna";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  //============= 26-09-2013, mutasi brg HASIL CUTTING. PEMBAHARUAN 15-10-2015 ===============================
  function hasilcutting(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'info-mutasi-stok/vmainformhasilcutting';
	$this->load->view('template',$data);

  }
  
  // PEMBAHARUAN 15-10-2015
  function viewhasilcutting(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vformviewhasilcutting';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok_hasilcutting($date_from, $date_to);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	/*$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi; */
	$this->load->view('template',$data);
  }
  
  function export_excel_hasilcutting() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_hasilcutting($date_from, $date_to);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='16' align='center'>LAPORAN MUTASI STOK BAHAN HASIL CUTTING</th>
		 </tr>
		 <tr>
			<th colspan='16' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
		 <th width='3%' rowspan='2'>No</th>
		 <th width='15%' rowspan='2'>Kode Brg</th>
		 <th width='25%' rowspan='2'>Nama Brg</th>
		 <th width='8%' rowspan='2'>Satuan</th>
		 <th width='8%' rowspan='2'>S.Awal</th>
		 <th colspan='3'>Masuk</th>
		 <th width='8%' rowspan='2'>Total<br>Masuk</th>
		 <th colspan='3'>Keluar</th>
		 <th width='8%' rowspan='2'>Total<br>Keluar</th>
		 
		 <th width='8%' rowspan='2'>Saldo Akhir</th>
		 <th width='8%' rowspan='2'>Stok Opname</th>
		 <th width='8%' rowspan='2'>Selisih</th>
	 </tr>
	 <tr>
			 <th width='8%'>Gudang<br>Pengadaan</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Retur</th>
			 <th width='8%'>Kirim<br>Unit</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Retur</th>
	 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg_wip']."</td>
				 <td>".$query[$j]['nama_brg_wip']."</td>
				 <td>Set</td>
				 <td align='right'>".$query[$j]['saldo_awal']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_bgs']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_retur']."</td>";
				 
				 $tot_masuk = $query[$j]['jum_masuk_bgs']+$query[$j]['jum_masuk_lain']+$query[$j]['jum_masuk_retur'];

				 $html_data.="<td align='right'>".$tot_masuk."</td>
				 <td align='right'>".$query[$j]['jum_keluar_bgs']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_retur']."</td>";
				 $tot_keluar = $query[$j]['jum_keluar_bgs']+$query[$j]['jum_keluar_lain']+$query[$j]['jum_keluar_retur'];
				 
				 $html_data.="<td align='right'>".$tot_keluar."</td>";
				 
				 $saldo_akhir = $query[$j]['saldo_awal']+$tot_masuk-$tot_keluar;
				 $selisih = $query[$j]['jum_stok_opname']-$saldo_akhir;
				 
				$html_data.= "<td align='right'>".$saldo_akhir."</td>
				<td align='right'>".$query[$j]['jum_stok_opname']."</td>
				<td align='right'>".$selisih."</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok_hasil_cutting";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  // 04-06-2014
  function viewsobhnbaku(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
    $data['isi'] = 'info-mutasi-stok/vformviewsobhnbaku';
	$id_gudang 	= $this->input->post('gudang', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);  
	
	if ($id_gudang == '' && $bulan=='' && $tahun=='') {
		$id_gudang 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	
	if($this->session->userdata('gid') == 16) 
	{
	if ($id_gudang == '')
		$id_gudang = '16';
	}
	else
	{
	if ($id_gudang == '')
		$id_gudang = '0';
	}

    $jum_total = $this->mmaster->get_sobhnbakutanpalimit($id_gudang, $bulan, $tahun);

			$config['base_url'] = base_url().'index.php/info-mutasi-stok/cform/viewsobhnbaku/'.$id_gudang.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mmaster->get_sobhnbaku($config['per_page'],$this->uri->segment(7), $id_gudang, $bulan, $tahun);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['cid_gudang'] = $id_gudang;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  function editsobhnbaku(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cid_gudang 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cid_gudang'] = $cid_gudang;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mmaster->get_detail_sobhnbaku($id_so); 
	$data['querybrgbaru'] = $this->mmaster->get_detail_stokbrgbaru($id_so); 
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	if (is_array($data['querybrgbaru']))
		$data['jum_total'] += count($data['querybrgbaru']);
	$data['isi'] = 'info-mutasi-stok/veditsobhnbaku';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  function updatesobhnbaku() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $id_gudang = $this->input->post('id_gudang', TRUE);
	  $tgl_so = $this->input->post('tgl_so', TRUE);
		$pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
				
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cid_gudang = $this->input->post('cid_gudang', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  $tgl = date("Y-m-d H:i:s"); 
	  //$tgl = date("Y-m-d"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET tgl_so='$tgl_so', tgl_update = '$tgl', status_approve='t' 
							where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mmaster->savesobhnbaku($id_so, $tgl_so, $id_gudang,
			 $this->input->post('id_brg_'.$i, TRUE), 
			 $this->input->post('id_'.$i, TRUE),
				 // 25-11-2014, 27-05-2015 harga dan auto_saldo_akhir dihilangkan
				 $this->input->post('stok_'.$i, TRUE),
				 $this->input->post('stok_fisik_'.$i, TRUE)
				 //$this->input->post('harga_'.$i, TRUE),
				 // 17-03-2015
				 //$this->input->post('auto_saldo_akhir_'.$i, TRUE),
				 //$this->input->post('auto_saldo_akhir_harga_'.$i, TRUE)
			 );
		 }
			  
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "info-mutasi-stok/cform/viewsobhnbaku/".$cid_gudang."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
  // 06-11-2014
  function quilting(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	//$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-mutasi-stok/vmainformquilting';
	$this->load->view('template',$data);
  }
  
  function viewquilting(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vformviewquilting';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	//$gudang = $this->input->post('gudang', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok_quilting($date_from, $date_to);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	//$data['gudang'] = $gudang;
	
	/*if ($gudang != '0') {
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
									FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
	}
	else {
		$kode_gudang	= '';
		$nama_gudang	= '';
		$nama_lokasi	= '';
	}
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi; */
	$this->load->view('template',$data);
  }
  
  function export_excel_quilting() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		//$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		//$kode_gudang = $this->input->post('kode_gudang', TRUE);
		//$nama_gudang = $this->input->post('nama_gudang', TRUE);
		//$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_quilting($date_from, $date_to);
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='21' align='center'>LAPORAN MUTASI STOK BAHAN HASIL QUILTING</th>
		 </tr>
		 <tr>
			<th colspan='21' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
			 <tr>
				 <th rowspan='2'>No</th>
				 <th rowspan='2'>Kode Brg</th>
				 <th rowspan='2'>Nama Barang</th>
				 <th rowspan='2'>Sat Awal</th>
				 <th rowspan='2'>Harga</th>
				 <th width='5%' colspan='2'>Saldo Awal</th>
				 <th width='5%' colspan='2'>Masuk</th>
				 <th width='5%' colspan='2'>Masuk Lain</th>
				 <th width='5%' colspan='2'>Keluar</th>
				 <th width='5%' colspan='2'>Keluar Lain</th>
				 <th width='5%' colspan='2'>Saldo Akhir</th>
				 <th width='5%' colspan='2'>SO</th>
				 <th width='5%' colspan='2'>Selisih</th>
			 </tr>
	 <tr>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
	 </tr>
		 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
				$tkodegudang="";
				$totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
				$totsaldoakhir=0; $totso = 0; $totselisih=0;
				
			 for($j=0;$j<count($query);$j++){
				 
				 /*if ($tkodegudang != $query[$j]['kode_gudang']) {
					 $tkodegudang = $query[$j]['kode_gudang'];
				 
				 $html_data.="<tr>
						<td colspan='22'><b>".$query[$j]['kode_gudang']." - ".$query[$j]['nama_gudang']."</b></td>
					</tr>";
				 } */
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td>".$query[$j]['satuan']."</td>
				 <td align='right'>".$query[$j]['harga']."</td>
				 <td align='right'>".$query[$j]['saldo_awal']."</td>
				 <td align='right'>".$query[$j]['saldo_awal_rp']."</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_rp']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain_rp']."</td>
				 <td align='right'>".$query[$j]['jum_keluar']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_rp']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain_rp']."</td> ";
				//$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				
				$saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				$saldo_akhir_rp = $saldo_akhir*$query[$j]['harga'];
				 
				$html_data.= "<td align='right'>".$saldo_akhir."</td>";
				$html_data.= "<td align='right'>".$saldo_akhir_rp."</td>";
				$html_data.= "<td align='right'>".$query[$j]['jum_stok_opname']."</td> ";
				
				$selisih = abs($query[$j]['jum_stok_opname']-$saldo_akhir);
				$selisih_rp = $selisih*$query[$j]['harga'];
				 
				$html_data.= "<td align='right'>".$selisih."</td> ";
				$html_data.= "<td align='right'>".$selisih_rp."</td> ";
				$html_data.=  "</tr>";			
				
				 $totsaldoawal+= $query[$j]['saldo_awal_rp'];
				 $totmasuk+= $query[$j]['jum_masuk_rp'];
				 $totmasuklain+= $query[$j]['jum_masuk_lain_rp'];
				 $totkeluar+= $query[$j]['jum_keluar_rp'];
				 $totkeluarlain+= $query[$j]['jum_keluar_lain_rp'];
				 $totsaldoakhir+= $saldo_akhir_rp;
				 $totso+= $query[$j]['jum_stok_opname_rp'];
				 $totselisih+= $selisih_rp;
				 
				/* if (isset($query[$j+1]['kode_gudang']) && ($query[$j]['kode_gudang'] != $query[$j+1]['kode_gudang'])) {
					 $html_data.= "<tr>
						<td colspan='6' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".$totsaldoawal."</b></td>
						<td colspan='2' align='right'><b>".$totmasuk."</b></td>
						<td colspan='2' align='right'><b>".$totmasuklain."</b></td>
						<td colspan='2' align='right'><b>".$totkeluar."</b></td>
						<td colspan='2' align='right'><b>".$totkeluarlain."</b></td>
						<td colspan='2' align='right'><b>".$totsaldoakhir."</b></td>
						<td colspan='2' align='right'><b>".$totso."</b></td>
						<td colspan='2' align='right'><b>".$totselisih."</b></td>
					 </tr>";
					 
					 $totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
					$totsaldoakhir=0; $totso = 0; $totselisih=0;
				 }
				 else if (!isset($query[$j+1]['kode_gudang'])) { */
					 
				// }
		 	}
		 	$html_data.= "<tr>
						<td colspan='5' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".$totsaldoawal."</b></td>
						<td colspan='2' align='right'><b>".$totmasuk."</b></td>
						<td colspan='2' align='right'><b>".$totmasuklain."</b></td>
						<td colspan='2' align='right'><b>".$totkeluar."</b></td>
						<td colspan='2' align='right'><b>".$totkeluarlain."</b></td>
						<td colspan='2' align='right'><b>".$totsaldoakhir."</b></td>
						<td colspan='2' align='right'><b>".$totso."</b></td>
						<td colspan='2' align='right'><b>".$totselisih."</b></td>
					 </tr>";
		   }
		  
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok_quilting";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
	// 26-08-2015. fitur input stok opname berdasarkan harga sesuai bulan dan tahun yg dipilih
	function soharga(){
		// =======================
		// disini coding utk pengecekan user login	
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$data['isi'] = 'info-mutasi-stok/vmainformsoharga';
		$data['msg'] = '';
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$this->load->view('template',$data);
	 }
	 
  function viewsoharga(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$gudang = $this->input->post('gudang', TRUE);  
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE a.id = '$gudang' ");
			$hasilrow = $query3->row();
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
			$nama_lokasi	= $hasilrow->nama_lokasi;
			
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['gudang'] = $gudang;
			$data['kode_gudang'] = $kode_gudang;
			$data['nama_gudang'] = $nama_gudang;
			$data['nama_lokasi'] = $nama_lokasi;
	
	// get data dari tabel tt_stok_opname_bahan_baku yg statusnya 'f'
	$data['query'] = $this->mmaster->get_all_stok_opname_bahanbaku_forharga($bulan, $tahun, $gudang);
	$dataso = $data['query'];
	
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
		
	$id_so = $dataso[0]['id'];
	$tgl_so = $dataso[0]['tgl_so'];
	$pisah1 = explode("-", $tgl_so);
	$thn1= $pisah1[0];
	$bln1= $pisah1[1];
	$tgl1= $pisah1[2];
	$tgl_so = $tgl1."-".$bln1."-".$thn1;
					
	$data['tgl_so'] = $tgl_so;
	$data['id_so'] = $id_so;
	$data['isi'] = 'info-mutasi-stok/vformviewsoharga';
	$this->load->view('template',$data);
	// =====================================================================================================
  }
  
  function submitsoharga() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $gudang = $this->input->post('gudang', TRUE);  
	  $jum_total = $this->input->post('jum_total', TRUE);  
	  
	  $tgl = date("Y-m-d H:i:s"); 
	  	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
	  $hasilrow = $query3->row();
	  $kode_gudang	= $hasilrow->kode_gudang;
	  $nama_gudang	= $hasilrow->nama;
	  	      
	  for ($i=1;$i<=$jum_total;$i++)
	  {
			 /*$this->mmaster->save($is_new, $id_stok, $this->input->post('id_brg_'.$i, TRUE), 
			 $this->input->post('brgbaru_'.$i, TRUE),
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 ); */
			 $jum_data = $this->input->post('jum_data_'.$i, TRUE);
			 $iddetailx = $this->input->post('id_detail_'.$i, TRUE);
			 $this->db->query("DELETE FROM tt_stok_opname_bahan_baku_detail_harga WHERE id_stok_opname_bahan_baku_detail='$iddetailx'");
			 
			 for ($j=1;$j<=$jum_data-1;$j++) {
				// save ke tabel detail_harga
				$this->mmaster->savesoharga($iddetailx,
						 $this->input->post('harga_'.$i.'_'.$j, TRUE),
						 $this->input->post('is_pkp_'.$i.'_'.$j, TRUE),
						 $this->input->post('stok_harga_'.$i.'_'.$j, TRUE)
				 );
			} // end for detail
	  }
		  
		  $data['msg'] = "Input stok opname bahan baku/pembantu berdasarkan harga untuk bulan ".$nama_bln." ".$tahun." di gudang ".$kode_gudang." - ".$nama_gudang." sudah berhasil disimpan";
		  $data['list_gudang'] = $this->mmaster->get_gudang();
		  $data['isi'] = 'info-mutasi-stok/vmainformsoharga';
	      $this->load->view('template',$data);
  }
  
  // 05-09-2015 URUTAN KE-1
  function migrasisoharga2stokharga() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$this->db->query(" truncate tm_stok_harga1 restart identity cascade ");
	
	$sql = " SELECT a.id_brg, b.id, b.harga, b.jum_stok_opname, b.is_harga_pkp FROM tt_stok_opname_bahan_baku x1  
			INNER JOIN tt_stok_opname_bahan_baku_detail a ON x1.id = a.id_stok_opname_bahan_baku
			INNER JOIN tt_stok_opname_bahan_baku_detail_harga b ON a.id = b.id_stok_opname_bahan_baku_detail
			WHERE x1.bulan = '06' AND x1.tahun = '2015' AND x1.status_approve='t' AND a.status_approve='t'
			AND b.harga <> 0 ORDER BY a.id_brg, b.id ";
	$query	= $this->db->query($sql);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query->num_rows() > 0){
		$hasil = $query->result();
						
		foreach ($hasil as $row1) {	
			$id_brg = $row1->id_brg;
			$harga = $row1->harga;
			$jum_stok_opname = $row1->jum_stok_opname;
			$is_harga_pkp = $row1->is_harga_pkp;
			
			$data_stok_harga = array(
								'id_brg'=>$id_brg,
								'stok'=>$jum_stok_opname,
								'harga'=>$harga,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$is_harga_pkp
								);
			$this->db->insert('tm_stok_harga1', $data_stok_harga);
			
		}
		echo "insert ke tm_stok_harga1 dari SO berdasarkan harga di periode juni 2015 sukses <br>";
	} // end if
	
	// 2. insert data harga dari tabel transaksi masuk bln juli jika harga tsb blm ada di tm_stok_harga1, 
	//dan tambahkan stok di tm_stok_harga1 berdasarkan data brg masuk tsb 
	$sql2 = " SELECT a.id_supplier, b.id_brg, b.harga, b.qty FROM tm_pembelian a 
			INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
			WHERE a.tgl_sj >= '2015-07-01' AND a.tgl_sj <= '2015-07-31' AND a.status_aktif = 't' ORDER BY a.tgl_sj, a.id ";
	$query2	= $this->db->query($sql2);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->result();
						
		foreach ($hasil2 as $row2) {
			$id_brg = $row2->id_brg;
			$harga = $row2->harga;
			$qty = $row2->qty;
			$id_supplier = $row2->id_supplier;
			
			$sqlxx = " SELECT pkp FROM tm_supplier WHERE id = '$id_supplier' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$pkp	= $hasilxx->pkp;
			
			// cek apakah harga sudah ada di tabel tm_stok_harga
			$sqlxx = " SELECT id, harga, stok FROM tm_stok_harga1 WHERE id_brg = '$id_brg' AND harga = '$harga' AND is_harga_pkp = '$pkp' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data_stok_harga = array(
								'id_brg'=>$id_brg,
								'stok'=>$qty,
								'harga'=>$harga,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$pkp
								);
				$this->db->insert('tm_stok_harga1', $data_stok_harga);
			}
			else {
				$hasilxx = $queryxx->row();
				$id_stok_harga = $hasilxx->id;
				$stok_harga = $hasilxx->stok;
				
				$stok_baru = $stok_harga+$qty;
				$this->db->query(" UPDATE tm_stok_harga1 SET stok = '$stok_baru' WHERE id = '$id_stok_harga' ");
			}
		}
		echo "insert ke tm_stok_harga1 dari pembelian periode juli 2015 sukses <br>";
	}
	
  } // end function
  
  // 08-09-2015 INI HRSNYA URUTAN KE-3 (GA DIPAKE)
  function migrasihargapembelian2stokharga() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	// 1. insert data harga dari tabel transaksi masuk bln agustus jika harga tsb blm ada di tm_stok_harga1, 
	//dan tambahkan stok di tm_stok_harga1 berdasarkan data brg masuk tsb 
	$sql2 = " SELECT a.id_supplier, b.id_brg, b.harga, b.qty FROM tm_pembelian a 
			INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
			WHERE a.tgl_sj >= '2015-08-01' AND a.tgl_sj <= '2015-08-31' AND a.status_aktif = 't' ORDER BY a.tgl_sj, a.id ";
	$query2	= $this->db->query($sql2);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->result();
						
		foreach ($hasil2 as $row2) {
			$id_brg = $row2->id_brg;
			$harga = $row2->harga;
			$qty = $row2->qty;
			$id_supplier = $row2->id_supplier;
			
			$sqlxx = " SELECT pkp FROM tm_supplier WHERE id = '$id_supplier' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$pkp	= $hasilxx->pkp;
			
			// cek apakah harga sudah ada di tabel tm_stok_harga
			$sqlxx = " SELECT id, harga, stok FROM tm_stok_harga1 WHERE id_brg = '$id_brg' AND harga = '$harga' AND is_harga_pkp = '$pkp' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data_stok_harga = array(
								'id_brg'=>$id_brg,
								'stok'=>$qty,
								'harga'=>$harga,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$pkp
								);
				$this->db->insert('tm_stok_harga1', $data_stok_harga);
			}
			else {
				$hasilxx = $queryxx->row();
				$id_stok_harga = $hasilxx->id;
				$stok_harga = $hasilxx->stok;
				
				$stok_baru = $stok_harga+$qty;
				$this->db->query(" UPDATE tm_stok_harga1 SET stok = '$stok_baru' WHERE id = '$id_stok_harga' ");
			}
		}
		echo "insert ke tm_stok_harga1 dari pembelian periode agustus 2015 sukses <br>";
	}
	
  } // end function
  
  // 11-09-2015 URUTAN KE-2
  function migrasisohargajuli2stokharga() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	// 30-09-2015. insert data harga dari tabel transaksi masuk bln juli jika harga tsb blm ada di tm_stok_harga1, 
	//dan tambahkan stok di tm_stok_harga1 berdasarkan data brg masuk tsb 
	// UPDATE: GA PERLU LAGI AMBIL DARI TRANSAKSI JULI 2015
/*	$sql2 = " SELECT a.id_supplier, b.id_brg, b.harga, b.qty FROM tm_pembelian a 
			INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
			WHERE a.tgl_sj >= '2015-07-01' AND a.tgl_sj <= '2015-07-31' AND a.status_aktif = 't' ORDER BY a.tgl_sj, a.id ";
	$query2	= $this->db->query($sql2);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->result();
						
		foreach ($hasil2 as $row2) {
			$id_brg = $row2->id_brg;
			$harga = $row2->harga;
			$qty = $row2->qty;
			$id_supplier = $row2->id_supplier;
			
			$sqlxx = " SELECT pkp FROM tm_supplier WHERE id = '$id_supplier' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$pkp	= $hasilxx->pkp;
			
			// cek apakah harga sudah ada di tabel tm_stok_harga
			$sqlxx = " SELECT id, harga, stok FROM tm_stok_harga1 WHERE id_brg = '$id_brg' AND harga = '$harga' AND is_harga_pkp = '$pkp' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data_stok_harga = array(
								'id_brg'=>$id_brg,
								'stok'=>$qty,
								'harga'=>$harga,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$pkp
								);
				$this->db->insert('tm_stok_harga1', $data_stok_harga);
			}
			else {
				$hasilxx = $queryxx->row();
				$id_stok_harga = $hasilxx->id;
				$stok_harga = $hasilxx->stok;
				
				$stok_baru = $stok_harga+$qty;
				$this->db->query(" UPDATE tm_stok_harga1 SET stok = '$stok_baru' WHERE id = '$id_stok_harga' ");
			}
		}
		echo "insert ke tm_stok_harga1 dari pembelian periode juli 2015 sukses <br>";
	} */
	
	// 07-10-2015 dikomen KARENA MAU MIGRASIKAN YG TRANSAKSI AGUSTUS DAN SEPTEMBER. 1:31: dibuka lagi, mau migrasi ulang
/*	$sql = " SELECT a.id_brg, b.id, b.harga, b.jum_stok_opname, b.is_harga_pkp FROM tt_stok_opname_bahan_baku x1  
			INNER JOIN tt_stok_opname_bahan_baku_detail a ON x1.id = a.id_stok_opname_bahan_baku
			INNER JOIN tt_stok_opname_bahan_baku_detail_harga b ON a.id = b.id_stok_opname_bahan_baku_detail
			WHERE x1.bulan = '07' AND x1.tahun = '2015' AND x1.status_approve='t' AND a.status_approve='t'
			AND b.harga <> 0 ORDER BY a.id_brg, b.id ";
	$query	= $this->db->query($sql);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query->num_rows() > 0){
		$hasil = $query->result();
						
		foreach ($hasil as $row1) {	
			$id_brg = $row1->id_brg;
			$harga = $row1->harga;
			$jum_stok_opname = $row1->jum_stok_opname;
			$is_harga_pkp = $row1->is_harga_pkp;
			
			$sqlxx = " SELECT satuan, id_satuan_konversi FROM tm_barang WHERE id = '$id_brg' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$id_satuan	= $hasilxx->satuan;
			$id_satuan_konversi	= $hasilxx->id_satuan_konversi;
			
			$sqlxx = " SELECT id, harga, stok FROM tm_stok_harga WHERE id_brg = '$id_brg' AND harga = '$harga' 
					AND is_harga_pkp = '$is_harga_pkp' AND id_satuan = '$id_satuan' AND id_satuan_konversi = '$id_satuan_konversi' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data_stok_harga = array(
								'id_brg'=>$id_brg,
								//'stok'=>$jum_stok_opname,
								'stok'=>0,
								'harga'=>$harga,
								'id_satuan'=>$id_satuan,
								'id_satuan_konversi'=>$id_satuan_konversi,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$is_harga_pkp
								);
				$this->db->insert('tm_stok_harga', $data_stok_harga);
			}			
		}
		echo "insert ke tm_stok_harga dari SO berdasarkan harga di periode juli 2015 sukses <br>";
	} // end if
	*/
	
	// 2. insert data harga dari tabel transaksi masuk bln AGUSTUS/september jika harga tsb blm ada di tm_stok_harga, 
	//dan tambahkan stok di tm_stok_harga berdasarkan data brg masuk tsb 
/*	$sql2 = " SELECT a.id_supplier, b.id_brg, b.harga, b.qty FROM tm_pembelian a 
			INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
			WHERE a.tgl_sj >= '2015-09-01' AND a.tgl_sj <= '2015-09-30' AND a.status_aktif = 't' ORDER BY a.tgl_sj, a.id ";
	$query2	= $this->db->query($sql2);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->result();
						
		foreach ($hasil2 as $row2) {
			$id_brg = $row2->id_brg;
			$harga = $row2->harga;
			$qty = $row2->qty;
			$id_supplier = $row2->id_supplier;
			
			$sqlxx = " SELECT pkp FROM tm_supplier WHERE id = '$id_supplier' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$pkp	= $hasilxx->pkp;
			
			$sqlxx = " SELECT satuan, id_satuan_konversi FROM tm_barang WHERE id = '$id_brg' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$id_satuan	= $hasilxx->satuan;
			$id_satuan_konversi	= $hasilxx->id_satuan_konversi;
			
			// cek apakah harga sudah ada di tabel tm_stok_harga
			$sqlxx = " SELECT id, harga FROM tm_stok_harga WHERE id_brg = '$id_brg' AND harga = '$harga' 
					AND is_harga_pkp = '$pkp' AND id_satuan='$id_satuan' AND id_satuan_konversi = '$id_satuan_konversi' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data_stok_harga = array(
								'id_brg'=>$id_brg,
								'stok'=>0,
								'harga'=>$harga,
								'id_satuan'=>$id_satuan,
								'id_satuan_konversi'=>$id_satuan_konversi,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$pkp
								);
				$this->db->insert('tm_stok_harga', $data_stok_harga);
			}
			// ini ga perlu karena ga perlu ada data stok berdasarkan harga. cukup menampung data history harga aja
			//else {
			//	$hasilxx = $queryxx->row();
			//	$id_stok_harga = $hasilxx->id;
			//	$stok_harga = $hasilxx->stok;
				
			//	$stok_baru = $stok_harga+$qty;
			//	$this->db->query(" UPDATE tm_stok_harga1 SET stok = '$stok_baru' WHERE id = '$id_stok_harga' ");
			//}
		}
		echo "insert ke tm_stok_harga dari pembelian periode SEPTEMBER 2015 sukses <br>";
	} */
	
	$sql = " SELECT a.id_brg, b.id, b.harga, b.jum_stok_opname, b.is_harga_pkp FROM tt_stok_opname_bahan_baku x1  
			INNER JOIN tt_stok_opname_bahan_baku_detail a ON x1.id = a.id_stok_opname_bahan_baku
			INNER JOIN tt_stok_opname_bahan_baku_detail_harga b ON a.id = b.id_stok_opname_bahan_baku_detail
			WHERE x1.bulan = '08' AND x1.tahun = '2015' AND x1.status_approve='t' AND a.status_approve='t'
			AND b.harga <> 0 ORDER BY a.id_brg, b.id ";
	$query	= $this->db->query($sql);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query->num_rows() > 0){
		$hasil = $query->result();
						
		foreach ($hasil as $row1) {	
			$id_brg = $row1->id_brg;
			$harga = $row1->harga;
			$jum_stok_opname = $row1->jum_stok_opname;
			$is_harga_pkp = $row1->is_harga_pkp;
			
			$sqlxx = " SELECT satuan, id_satuan_konversi FROM tm_barang WHERE id = '$id_brg' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$id_satuan	= $hasilxx->satuan;
			$id_satuan_konversi	= $hasilxx->id_satuan_konversi;
			
			$sqlxx = " SELECT id, harga, stok FROM tm_stok_harga WHERE id_brg = '$id_brg' AND harga = '$harga' 
					AND is_harga_pkp = '$is_harga_pkp' AND id_satuan = '$id_satuan' AND id_satuan_konversi = '$id_satuan_konversi' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data_stok_harga = array(
								'id_brg'=>$id_brg,
								//'stok'=>$jum_stok_opname,
								'stok'=>0,
								'harga'=>$harga,
								'id_satuan'=>$id_satuan,
								'id_satuan_konversi'=>$id_satuan_konversi,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$is_harga_pkp
								);
				$this->db->insert('tm_stok_harga', $data_stok_harga);
			}			
		}
		echo "insert ke tm_stok_harga dari SO berdasarkan harga di periode agustus 2015 sukses <br>";
	} // end if
	
  } // end function
  
  // bismillah, fungsi utk migrasi histori harga
  function modifhistoriharga() {
	  // modif utk november 2015, modifnya tgl 11 nov 15 12:29
	  $sqlxx = " SELECT b.id_brg, c.kode_brg, b.id_satuan, b.id_satuan_konversi, b.harga, a.id_supplier, a.tgl_sj 
				FROM tm_pembelian a 
				INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
				INNER JOIN tm_barang c ON b.id_brg = c.id
				WHERE a.tgl_sj between '2015-11-01' AND '2015-11-30' AND a.status_aktif = 't'
				ORDER BY a.tgl_sj, b.id_brg ";
	  $queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			
			//$total = 0;
			$i=1;
			$tgl = date("Y-m-d H:i:s"); 
			foreach ($hasilxx as $rowxx) {	
				//$total+= $rowxx->subtotal;
				
				$sql2 = " SELECT pkp from tm_supplier WHERE id = '$rowxx->id_supplier' ";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->row();
					$pkp	= $hasil2->pkp;
				}
				
				$sqlxx2 = " SELECT id FROM tm_stok_harga WHERE id_brg = '$rowxx->id_brg' AND harga = '$rowxx->harga' 
						 AND id_satuan = '$rowxx->id_satuan' 
						 AND is_harga_pkp = '$pkp'";
				
				$queryxx2	= $this->db->query($sqlxx2);
				if ($queryxx2->num_rows() == 0){
					//echo $sqlxx2."<br>";
					//echo $i. " pkp ".$pkp." kode brg: ".$rowxx->kode_brg.", id_brg: ".$rowxx->id_brg." harga: ".$rowxx->harga." TIDAK ada dalam tm_stok_harga <br>";
					
					$this->db->query(" INSERT INTO tm_stok_harga (stok, harga, tgl_update_stok, id_brg, is_harga_pkp, 
								id_satuan, id_satuan_konversi)
								VALUES ('0', '$rowxx->harga', '$tgl', '$rowxx->id_brg', '$pkp', 
								'$rowxx->id_satuan', '$rowxx->id_satuan_konversi') ");
				}
				$i++;
			}
			echo "sukses modif ulang history harga november 2015 ";
		}
		
/*	$sql = " SELECT a.id_brg, b.id, b.harga, b.jum_stok_opname, b.is_harga_pkp FROM tt_stok_opname_bahan_baku x1  
			INNER JOIN tt_stok_opname_bahan_baku_detail a ON x1.id = a.id_stok_opname_bahan_baku
			INNER JOIN tt_stok_opname_bahan_baku_detail_harga b ON a.id = b.id_stok_opname_bahan_baku_detail
			WHERE x1.bulan = '08' AND x1.tahun = '2015' AND x1.status_approve='t' AND a.status_approve='t'
			AND b.harga <> 0 ORDER BY a.id_brg, b.id ";
	$query	= $this->db->query($sql);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query->num_rows() > 0){
		$hasil = $query->result();
						
		foreach ($hasil as $row1) {	
			$id_brg = $row1->id_brg;
			$harga = $row1->harga;
			$jum_stok_opname = $row1->jum_stok_opname;
			$is_harga_pkp = $row1->is_harga_pkp;
			
			$sqlxx = " SELECT satuan, id_satuan_konversi FROM tm_barang WHERE id = '$id_brg' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$id_satuan	= $hasilxx->satuan;
			$id_satuan_konversi	= $hasilxx->id_satuan_konversi;
			
			$sqlxx = " SELECT id, harga FROM tm_stok_harga WHERE id_brg = '$id_brg' AND harga = '$harga' 
					AND is_harga_pkp = '$is_harga_pkp' AND id_satuan = '$id_satuan' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data_stok_harga = array(
								'id_brg'=>$id_brg,
								//'stok'=>$jum_stok_opname,
								'stok'=>0,
								'harga'=>$harga,
								'id_satuan'=>$id_satuan,
								'id_satuan_konversi'=>$id_satuan_konversi,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$is_harga_pkp
								);
				$this->db->insert('tm_stok_harga', $data_stok_harga);
			}			
		}
		echo "insert ke tm_stok_harga dari SO berdasarkan harga di periode agustus 2015 sukses <br>";
	} // end if
	
	// 2. insert data harga dari tabel transaksi masuk bln AGUSTUS/september jika harga tsb blm ada di tm_stok_harga, 
	//dan tambahkan stok di tm_stok_harga berdasarkan data brg masuk tsb 
	// MODIF 09-11-2015: SAMPE sept dan okt
	$sql2 = " SELECT a.id_supplier, b.id_brg, b.harga, b.qty, b.id_satuan, b.id_satuan_konversi FROM tm_pembelian a 
			INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
			WHERE a.tgl_sj >= '2015-09-01' AND a.tgl_sj <= '2015-09-30' AND a.status_aktif = 't' ORDER BY a.tgl_sj, a.id ";
	$query2	= $this->db->query($sql2);
	
	$tgl = date("Y-m-d H:i:s"); 
			
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->result();
						
		foreach ($hasil2 as $row2) {
			$id_brg = $row2->id_brg;
			$harga = $row2->harga;
			$qty = $row2->qty;
			$id_supplier = $row2->id_supplier;
			$id_satuan = $row2->id_satuan;
			$id_satuan_konversi = $row2->id_satuan_konversi;
			
			$sqlxx = " SELECT pkp FROM tm_supplier WHERE id = '$id_supplier' ";
			$queryxx	= $this->db->query($sqlxx);
			$hasilxx = $queryxx->row();
			$pkp	= $hasilxx->pkp;
						
			// cek apakah harga sudah ada di tabel tm_stok_harga
			$sqlxx = " SELECT id, harga FROM tm_stok_harga WHERE id_brg = '$id_brg' AND harga = '$harga' 
					AND is_harga_pkp = '$pkp' AND id_satuan='$id_satuan' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data_stok_harga = array(
								'id_brg'=>$id_brg,
								'stok'=>0,
								'harga'=>$harga,
								'id_satuan'=>$id_satuan,
								'id_satuan_konversi'=>$id_satuan_konversi,
								'tgl_update_stok'=>$tgl,
								'is_harga_pkp'=>$pkp
								);
				$this->db->insert('tm_stok_harga', $data_stok_harga);
			}
			// ini ga perlu karena ga perlu ada data stok berdasarkan harga. cukup menampung data history harga aja
			//else {
			//	$hasilxx = $queryxx->row();
			//	$id_stok_harga = $hasilxx->id;
			//	$stok_harga = $hasilxx->stok;
				
			//	$stok_baru = $stok_harga+$qty;
			//	$this->db->query(" UPDATE tm_stok_harga1 SET stok = '$stok_baru' WHERE id = '$id_stok_harga' ");
			//}
		}
		echo "insert ke tm_stok_harga dari pembelian periode SEPTEMBER 2015 sukses <br>";
	} */
  } // END
  
  function cekitembyharga() {
		$sql = " SELECT distinct b.id_brg, e.kode_brg, e.nama_brg, b.harga, b.id_satuan, b.id_satuan_konversi
					FROM tm_apply_stok_pembelian a INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok  
					INNER JOIN tm_barang e ON b.id_brg = e.id 
					WHERE b.tgl_bonm >= '2015-10-01'
					AND b.tgl_bonm <= '2015-10-31'
					AND a.status_aktif = 't'
					ORDER BY e.nama_brg ASC ";
					
		$query	= $this->db->query($sql);
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$sql2 = " SELECT * FROM tm_stok_harga WHERE id_brg = '$row1->id_brg' AND harga='$row1->harga'
						AND id_satuan='$row1->id_satuan' AND id_satuan_konversi='$row1->id_satuan_konversi' ";
				//echo $sql2."<br> $row1->kode_brg, satuan_konversi= $row1->id_satuan_konversi <br><br>";
				$query2	= $this->db->query($sql2);
				
				if ($query2->num_rows() == 0){
					// ini jika tidak ada datanya
					echo "ga ada <br>";
					echo "kode brg: ".$row1->kode_brg.", id_brg: ".$row1->id_brg."<br>";
				}
				else
					echo "kode brg: ".$row1->kode_brg.", id_brg: ".$row1->id_brg."<br>";
			}
		}
  }

}
