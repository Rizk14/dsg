<?php
class Creport extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-mutasi-stok/mreport');
  }
  
  function transaksibhnbaku(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mreport->get_gudang();
	$data['isi'] = 'info-mutasi-stok/vformlaptransaksibhnbaku';
	$this->load->view('template',$data);
  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_gudang	= $this->uri->segment(4);
	
	if ($id_gudang == '') {
		$id_gudang = $this->input->post('id_gudang', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || $id_gudang == '') {
			$id_gudang	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		if ($id_gudang == '')
			$id_gudang = '0';
		
		$keywordcari = str_replace("%20"," ", $keywordcari);
		
		$qjum_total = $this->mreport->get_bahantanpalimit($keywordcari, $id_gudang);
		
		$config['base_url'] = base_url()."index.php/info-mutasi-stok/creport/show_popup_brg/".$id_gudang."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);
	$data['query'] = $this->mreport->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $id_gudang);
	$data['jum_total'] = count($qjum_total);
	$data['id_gudang'] = $id_gudang;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$id_gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$nama_gudang	= '';
	}
	$data['nama_gudang'] = $nama_gudang;

	$this->load->view('info-mutasi-stok/vpopupbrg',$data);
  }
  
  function viewtransaksibhnbaku(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vviewlaptransaksibhnbaku';
	$id_gudang = $this->input->post('id_gudang', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$id_brg = trim($this->input->post('id_brg', TRUE));
	
	$data['query'] = $this->mreport->get_transaksi_bhnbaku($bulan, $tahun, $id_gudang, $id_brg);
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
	
	if ($id_gudang != '0') {
		$query3	= $this->db->query(" SELECT a.nama as nama_lokasi, b.kode_gudang, b.nama as nama_gudang FROM tm_lokasi_gudang a 
				INNER JOIN tm_gudang b ON a.id = b.id_lokasi 
				WHERE b.id = '$id_gudang' ");
		$hasilrow = $query3->row();
		$nama_lokasi	= $hasilrow->nama_lokasi;
		$nama_gudang	= $hasilrow->nama_gudang;
		$kode_gudang	= $hasilrow->kode_gudang;
		$gudangnya = "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang;
	}
	else {
		$gudangnya = "Semua";
	}
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	
		// ambil nama brg dan satuannya							
		$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id 
								WHERE a.id = '$id_brg' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_brg = $hasilrow->kode_brg;
			$nama_brg = $hasilrow->nama_brg;
			$satuan_awal = $hasilrow->nama_satuan;
		}
		else {
			$kode_brg = '';
			$nama_brg = '';
			$satuan_awal = '';
		}
		
		$query3	= $this->db->query(" SELECT a.satuan, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							b.nama as nama_satuan_konv FROM tm_barang a 
							LEFT JOIN tm_satuan b ON a.id_satuan_konversi = b.id WHERE a.id = '$id_brg' ");
		if ($query3->num_rows() > 0){
			$hasilnya = $query3->row();
			$nama_satuan_konv = $hasilnya->nama_satuan_konv;
			
			if ($nama_satuan_konv == '')
				$nama_satuan_konv = "Tidak Ada";
		}
		else
			$nama_satuan_konv = "Tidak Ada";
						
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// ambil SO brg dari tt_stok_opname_bahan_baku
		$sqlso = " SELECT b.jum_stok_opname 
							FROM tt_stok_opname_bahan_baku a INNER JOIN tt_stok_opname_bahan_baku_detail b
							ON a.id = b.id_stok_opname_bahan_baku 
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg = '$id_brg' ";
		$queryso	= $this->db->query($sqlso);
		if ($queryso->num_rows() > 0){
			$hasilso = $queryso->row();
			$jum_stok_opname = $hasilso->jum_stok_opname;
			if ($jum_stok_opname == '')
				$jum_stok_opname = 0;
		}
		else
			$jum_stok_opname = 0;
	
	$data['id_brg'] = $id_brg;
	$data['kode_brg'] = $kode_brg;
	$data['nama_brg'] = $nama_brg;
	$data['satuan_awal'] = $satuan_awal;
	$data['satuan_konv'] = $nama_satuan_konv;
	$data['id_gudang'] = $id_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$data['jum_stok_opname'] = $jum_stok_opname;
	$this->load->view('template',$data);
  }
  
  // 16-10-2015
  // ++++++++++++++++++ laporan transaksi barang hasil cutting ++++++++++++++++++++++++++++
  function transaksihasilcutting(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'info-mutasi-stok/vformlaptransaksihasilcutting';
	$this->load->view('template',$data);
  }
  
  function viewtransaksihasilcutting(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vviewlaptransaksihasilcutting';
	$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	$data['query'] = $this->mreport->get_transaksi_hasil_cutting($bulan, $tahun, $id_brg_wip);
	$data['jum_total'] = count($data['query']);
		
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
	$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
  
  // 22-10-2015
  function hasilcuttingvssjkeluar() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	
	if (!isset($is_logged_in) || $is_logged_in!=true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'info-mutasi-stok/vformhasilcuttingvssjkeluar';
	$this->load->view('template',$data);
  }
  
  function viewhasilcuttingvssjkeluar() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	
	if (!isset($is_logged_in) || $is_logged_in!=true) {
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vviewhasilcuttingvssjkeluar';
    
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	if ($date_from=='' && $date_to=='') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
	}

    $jum_total = $this->mreport->get_all_cuttingvssjtanpalimit($date_from, $date_to);
						
	$data['query'] = $this->mreport->get_all_cuttingvssj($date_from, $date_to);
	$data['jum_total'] = count($jum_total);
		
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;	
	$this->load->view('template',$data);
  }
  
  function export_excel_hasilcuttingvssjkeluar() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  
		set_time_limit(36000);
		ini_set("memory_limit","512M");
		ini_set("max_execution_time","36000");
		
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
	
		$query = $this->mreport->get_all_cuttingvssj($date_from, $date_to);
	  		
	  $html_data = " 
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='5' align='center'>LAPORAN HASIL CUTTING vs SJ KELUAR KE JAHITAN</th>
		 </tr>
		 <tr>
			<th colspan='5' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>		 
		 <tr>
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang WIP</th>
		 <th>Jumlah Hasil Cutting</th>
		 <th>Jumlah Kirim Ke Jahitan</th>
	 </tr>
	</thead>
	<tbody>
	  ";
	  
	  $i = 1;
	  if (is_array($query)) {
		  for($j=0;$j<count($query);$j++) {
				$html_data.= "<tr class=\"record\" valign=\"top\">

				<td width='10%'>".$i."</td>
				<td width='15%'>".$query[$j]['kode_brg_wip']."</td>
				<td nowrap width='20%'>".$query[$j]['nama_brg_wip']."</td>
				<td align='right'>".$query[$j]['jum_cutting']."</td>
				<td align='right'>".$query[$j]['jum_kirim']."</td>
			</tr>";
			$i++;
		 } // end for
		 $html_data.="</tbody></table>";
	   }
		 $nama_file = "laporan_hasilcuttingvssjkeluar";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
			
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
	  
  }
  
}
