<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
	if(!isset($is_logged_in) || $is_logged_in != true){
		redirect ('loginform');
	}
		
			$data['page_title'] = $this->lang->line('labarugi');
			$data['periode']	= '';
			$data['isi']='akt-lr/vmainform';
			$this->load->view('template',$data);
			
		
	}
	function view()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
	if(!isset($is_logged_in) || $is_logged_in != true){
		redirect ('loginform');
	}
		
			$periode	= $this->uri->segment(4);
			if($periode!='')
			{
				$this->load->model('akt-lr/mmaster');
				$data['page_title'] = $this->lang->line('labarugi');
#				$tmp 	= explode("-", $periode);
#				$tahun	= $tmp[0];
#				$bulan	= $tmp[1];
#				$tanggal= $tmp[0];
#				$tahun2	= $tmp[0];
        $tahun=substr($periode,0,4);
        $bulan=substr($periode,4,2);
        $tahun2=substr($periode,0,4);
        $bulan2=substr($periode,4,2);
        $iperiode	= $this->input->post('iperiode');
        $tanggal='01';        
				if($bulan=='01'){
					$bulan2='12';
					$tahun2=$tahun-1;
				}else{
					$bulan2=$bulan-1;
					if(strlen($bulan2)<2){
						$bulan2='0'.$bulan2;
						$tahun2=$tahun;
					}
				}
				$periode=$tahun.$bulan;
				$periodekurang=$tahun2.$bulan2;
				$kiwari		= $tahun."/".$bulan."/01";
				$namabulan	= $this->mmaster->NamaBulan($bulan);
				$data['periode']	= $periode;#yyyymm
				$data['periodekurang']	= $periodekurang;#yyyymm
				$dfrom				= $tahun."-".$bulan."-01";
				$dto				= $tahun."-".$bulan."-".$tanggal;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $tahun."-".$bulan."-".$tanggal;
				$data['tanggal']	= $tanggal;
				$data['namabulan']	= $namabulan;
				$data['tahun']		= $tahun;
				$data['pendapatan']	= $this->mmaster->bacapendapatan($periode);
				$data['pendapatanlain']	= $this->mmaster->bacapendapatanlain($periode);
				$data['pembelian']	= $this->mmaster->bacapembelian($periode);
				$data['bebanadm']	= $this->mmaster->bacabebanadministrasi($periode);
				$data['bebanope']	= $this->mmaster->bacabebanoperasional($periode);
				$data['bebanlai']	= $this->mmaster->bacabebanlainnya($periode);
				$data['hasilpenj']	= $this->mmaster->bacahasilpenjualan($periode);
				//~ $data['returpenj']	= $this->mmaster->bacareturpenjualan($periode);
				$data['returpenj']	= 0;
				$data['potpejualan']	= $this->mmaster->bacapotonganpenjualan($periode);
				$data['pembelianbb']	= $this->mmaster->bacapembelianbb($periode);
				$data['returpembelian']	= $this->mmaster->bacareturpembelian($periode);
				$data['potonganpembelian']	= $this->mmaster->bacapotonganpembelian($periode);
				$data['biayabungabank']	= $this->mmaster->bacabiayabungabank($periode);
				$data['pendapatanlainlain']	= $this->mmaster->bacapendapatanlainlain($periode);
				$data['persediaanakhirbarangjadi']	= $this->mmaster->bacapersediaanakhirbarangjadi($periode);
				//~ $data['persediaanawalbarangjadi']	= $this->mmaster->bacapersediaanawalbarangjadi($periodekurang);
				$data['persediaanawalbarangjadi']	= $this->mmaster->bacapersediaanawalbarangjadi($periode);
				$data['hadiah']	= $this->mmaster->bacahadiah($periode);
				$data['biayapenjualan']	= $this->mmaster->bacabiayapenjualan($periode);
				$data['biayaadmumum']	= $this->mmaster->bacabiayaadmumum($periode);
	      //~ $sess=$this->session->userdata('session_id');
	      //~ $id=$this->session->userdata('user_id');
	      //~ $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      //~ $rs		= pg_query($sql);
	      //~ if(pg_num_rows($rs)>0){
		      //~ while($row=pg_fetch_assoc($rs)){
			      //~ $ip_address	  = $row['ip_address'];
			      //~ break;
		      //~ }
	      //~ }else{
		      //~ $ip_address='kosong';
	      //~ }
	      //~ $query 	= pg_query("SELECT current_timestamp as c");
	      //~ while($row=pg_fetch_assoc($query)){
		      //~ $now	  = $row['c'];
	      //~ }
	      //~ $pesan='Buka Laporan Laba-Rugi Periode:'.$periode;
	      //~ $this->load->model('logger');
	      //~ $this->logger->write($id, $ip_address, $now , $pesan );
				
				$data['isi']='akt-lr/vmainform';
			$this->load->view('template',$data);
				
			}
		
	}
}
?>
