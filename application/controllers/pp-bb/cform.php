<?php
class Cform extends CI_Controller {

  function __construct () {
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('pp-bb/mmaster');
  }

  function index() {
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
	$id_pp 	= $this->uri->segment(4);
	$jenis_brg	= 1;
	$th_now	= date("Y");
	
	if ($id_pp != '') {
		$hasil = $this->mmaster->get_header_pp($id_pp);
		$edit = 1;
				
		foreach ($hasil as $row) {
			$e_tgl_pp = explode("-",$row->tgl_pp,strlen($row->tgl_pp)); // Y-m-d
			$n_tgl_pp = $e_tgl_pp[2]."-".$e_tgl_pp[1]."-".$e_tgl_pp[0];
			
			$eid = $row->id;
			$eno_pp = $row->no_pp;
			$etgl_pp = $n_tgl_pp;
		}
		$data['pp_detail'] = $this->mmaster->get_detail_pp($id_pp);
	}
	else {

		$qnomorpp	= $this->mmaster->crepp($jenis_brg);
		
		if($qnomorpp->num_rows()>0) {
			$row_nomorpp	= $qnomorpp->row();
			$nopp	= $row_nomorpp->no_pp;
			
			if(strlen($nopp)==9) {
				$n_pp	= substr($nopp,4,5);
				$th_pp	= substr($nopp,0,4);
				if($th_now==$th_pp) {
					$jml_n_pp	= $n_pp;
					switch(strlen($jml_n_pp)) {
						case "1": $kodepp	= "0000".$jml_n_pp;
						break;
						case "2": $kodepp	= "000".$jml_n_pp;
						break;	
						case "3": $kodepp	= "00".$jml_n_pp;
						break;
						case "4": $kodepp	= "0".$jml_n_pp;
						break;
						case "5": $kodepp	= $jml_n_pp;
						break;	
					}
					$nomorpp = $th_now.$kodepp;
				} else {
					$nomorpp = $th_now."00001";
				}
			} else {
				$nomorpp	= $th_now."00001";
			}
		} else {
			$nomorpp	= $th_now."00001";
		}
			
		$eid = '';
		$eno_pp = $nomorpp;
		$etgl_pp = '';
		$edit = '';
	}
	
	$data['edit'] = $edit;
	$data['eid'] = $eid;
	$data['etgl_pp'] = $etgl_pp;
	$data['eno_pp'] = $eno_pp;
	$data['msg'] = '';
    $data['isi'] = 'pp-bb/vmainform';
    $data['jenis_brg'] = $jenis_brg;
	
	$this->load->view('template',$data);
  }
  
  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
	//$departemen = ambil infonya dari tabel user
//========================

	$posisi 	= $this->uri->segment(4);
	$jenis_brg	= $this->uri->segment(5);
	
	if ($posisi == '')
		$posisi = $this->input->post('posisi', TRUE);  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		$qjum_total = $this->mmaster->get_bahan_bakutanpalimit($keywordcari);
		$jum_total	= $qjum_total->num_rows();
		
							$config['base_url'] = base_url()."index.php/pp-bb/cform/show_popup_brg/".$posisi."/";
							$config['total_rows'] = $jum_total; 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan_baku($config['per_page'],$config['cur_page'], $keywordcari);						
	$data['jum_total'] = $jum_total;
	$data['posisi'] = $posisi;
	$data['cari'] = $keywordcari;

    //$data['isi'] = 'pp-bb/vpopupbrg';
	$this->load->view('pp-bb/vpopupbrg',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		
		//$kode_bagian = '01'; // ini nantinya ambil dari user login
		$jenis_brg = 1;
		 
			$this->form_validation->set_rules('no_faktur', 'Nomor PP', 'required');
			$this->form_validation->set_rules('tgl_retur', 'Tanggal PP', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$data['isi'] = 'pp-bb/vmainform';
			$data['msg'] = 'Field2 nomor PP, tanggal PP tidak boleh kosong..!';
			$eno_pp = '';
			$etgl_pp = '';
			$edit = '';
			$eid = '';
			
			$data['eno_pp'] = $eno_pp;
			$data['etgl_pp'] = $etgl_pp;
			$data['edit'] = $edit;
			$data['eid'] = $eid;
			$this->load->view('template',$data);
		}
		else
		{
			$no_pp 	= $this->input->post('no_faktur', TRUE);
			$tgl_pp 	= $this->input->post('tgl_retur', TRUE);  // d-m-Y
			$kodeedit 	= $this->input->post('kodeeditnopp', TRUE); 
			$no 	= $this->input->post('no', TRUE);
			$id_pp 	= $this->input->post('id_pp', TRUE);
			$e_tgl_pp = explode("-",$tgl_pp,strlen($tgl_pp)); // d-m-Y
			$n_tgl_pp = $e_tgl_pp[2]."-".$e_tgl_pp[1]."-".$e_tgl_pp[0]; // Y-m-d
			 
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($no_pp, $jenis_brg);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'pp-bb/vmainform';
					$data['msg'] = "Data PP nomor ".$no_pp." sudah ada..!";
					$eno_pp = '';
					$etgl_pp = '';
					$edit = '';
					$data['eno_pp'] = $eno_pp;
					$data['etgl_pp'] = $etgl_pp;
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
					// inspired from proIT inventory
						$this->mmaster->save($no_pp,$n_tgl_pp,$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('jumlah_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE), $jenis_brg, $goedit);
					}
					redirect('pp-bb/cform/view');
				}
			} // end if goedit == ''
			else { // UPDATE DATA
				$cek_data = $this->mmaster->cek_data($no_pp, $jenis_brg);
				if ($no_pp != $kodeedit) {
					if (count($cek_data) > 0) { 
						$data['isi'] = 'pp-bb/vmainform';
						$data['msg'] = "Data Nomer PP ".$no_pp." sudah ada..!";
						
						$edit = '1';
						$data['eno_pp'] = $no_pp;
						$data['etgl_pp'] = $tgl_pp;
						$data['edit'] = $edit;
						$this->load->view('template',$data);
					}
					else {
						$jumlah_input=$no-1;
					
						// ambil id di tabel tm_pp sesuai dgn no_pp yg lama
						//$query2	= $this->db->query(" SELECT id FROM tm_pp WHERE no_pp = '$kodeedit' AND kode_bagian= '$kode_bagian' ");
						//$hasilrow = $query2->row();
						//$id_pp_lama	= $hasilrow->id;
						$this->db->delete('tm_pp_detail', array('id_pp' => $id_pp));
						$tgl = date("Y-m-d");
						$data_header = array(
						  'no_pp'=>$no_pp,
						  'tgl_pp'=>$n_tgl_pp,
						  'kode_bagian'=>'0',
						  'jenis_brg'=>$jenis_brg,
						  'tgl_update'=>$tgl
						);
						$this->db->where('id',$id_pp);
						$this->db->update('tm_pp',$data_header);
										
						for ($i=1;$i<=$jumlah_input;$i++)
						{
						// inspired from proIT inventory
							$this->mmaster->update_pp($id_pp,$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
										$this->input->post('jumlah_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE), $jenis_brg, $goedit);
						}
						redirect('pp-bb/cform/view');
					}
				}
				else {
						$jumlah_input=$no-1;
						$tgl = date("Y-m-d");
						$this->db->delete('tm_pp_detail', array('id_pp' => $id_pp));
						$data_header = array(
						  'no_pp'=>$no_pp,
						  'tgl_pp'=>$n_tgl_pp,
						  'kode_bagian'=>'0',
						  'jenis_brg'=>$jenis_brg,
						  'tgl_update'=>$tgl
						);
						$this->db->where('id',$id_pp);
						$this->db->update('tm_pp',$data_header); // sampe siniii 29-01-2011
										
						for ($i=1;$i<=$jumlah_input;$i++)
						{
						// inspired from proIT inventory
							$this->mmaster->update_pp($id_pp,$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
										$this->input->post('jumlah_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE), $jenis_brg, $goedit);
						}
						redirect('pp-bb/cform/view');
				}
			}
			
		}
  }
  
  function view(){
  	/*
	jenis brg=> 1: bhn baku, 2: asesoris, 3: bahan pendukung, 4: alat perlengkapan
	*/
	
    $data['isi'] = 'pp-bb/vformview';
    $keywordcari = '';
    $jenis_brg = 1;
    
	$kode_bagian = ''; // ini nanti ambil dari login user
	
    $qjum_total = $this->mmaster->getAlltanpalimit($jenis_brg, $keywordcari);
	$jum_total	= $qjum_total->num_rows();
	
							$config['base_url'] = base_url().'index.php/pp-bb/cform/view/index/';
							$config['total_rows'] = $jum_total; 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$config['cur_page'], $jenis_brg, $keywordcari);						
	$data['jum_total'] = $jum_total;
	$data['cari'] = $keywordcari;
	
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
    // $kode_bagian = '';
    $jenis_brg = 1;
    $qjum_total = $this->mmaster->getAlltanpalimit($jenis_brg, $keywordcari);
	$jum_total	= $qjum_total->num_rows();
	
							$config['base_url'] = base_url().'index.php/pp-bb/cform/view/index/';
							$config['total_rows'] = $jum_total; 
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5,0);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$config['cur_page'], $jenis_brg, $keywordcari);						
	$data['jum_total'] = $jum_total;
	$data['isi'] = 'pp-bb/vformview';
	$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('pp-bb/cform/view');
  }
  
  function cari_pp() {
  	$nomorpp	= $this->input->post('nomorpp');
  	
	$jenis_brg	= 1;
	$query	= $this->mmaster->getpp($nomorpp,$jenis_brg);
	if($query->num_rows()>0) {
		echo "Maaf, No. PP utk Bahan Baku sudah ada!";
	}
  }
}
