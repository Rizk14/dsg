<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonm-bbc/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$no_pb_cutting = $this->input->post('no_pb_cutting', TRUE);  
	$id_pb = $this->input->post('id_pb', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	$id_pb_detail = $this->input->post('id_brg', TRUE);  
	
	$list_brg = explode(";", $id_pb_detail);
	$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
		if ($no_pb_cutting !='') {
			//$data['pb_detail'] = $this->mmaster->get_detail_pb($no_pb_cutting, $list_brg);
			$data['pb_detail'] = $this->mmaster->get_detail_pb($id_pb, $list_brg);
			$data['msg'] = '';
			$data['no_pb_cutting'] = $no_pb_cutting;
			$data['id_pb'] = $id_pb;
		}
		else {
			$data['msg'] = 'permintaan bahan harus dipilih';
			$data['no_pb_cutting'] = '';
			$data['id_pb'] = '';
		}
		$data['go_proses'] = '1';
				
			// generate no Bon M keluar
		// =======================================================	
			$query3	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_proses_cutting ORDER BY no_bonm DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==9) {
				$nobonm = substr($no_bonm, 0, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm;
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			}
			
			$data['no_bonm'] = $nomorbonm;
			
			$data['ukuran_bisbisan'] = $this->mmaster->get_ukuran_bisbisan();
		// =======================================================
	}
	else {
		$data['msg'] = '';
		$data['no_pb_cutting'] = '';
		$data['id_pb'] = '';
		$data['go_proses'] = '';
	}
	$data['isi'] = 'bonm-bbc/vmainform';
	$this->load->view('template',$data);

  }
  
  function edit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	$id_bonm 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$carinya 	= $this->uri->segment(7);
	
	$data['query'] = $this->mmaster->get_bonm($id_bonm);
	$data['msg'] = '';
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['carinya'] = $carinya;

	$data['isi'] = 'bonm-bbc/veditform';
	$this->load->view('template',$data);

  }
    
  function show_popup_pb_cutting(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '')
		$keywordcari 	= $this->uri->segment(4);
	
	if ($keywordcari == '')
		$keywordcari = "all";
	
	$jum_total = $this->mmaster->get_pb_cuttingtanpalimit($keywordcari);
							$config['base_url'] = base_url()."index.php/bonm-bbc/cform/show_popup_pb_cutting/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_pb_cutting($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('bonm-bbc/vpopuppb',$data);

  }

  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
			$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$no_pb_cutting 	= $this->input->post('no_pb_cutting', TRUE);			
			$id_pb 	= $this->input->post('id_pb', TRUE);
			
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$this->mmaster->save($no_bonm,$tgl_bonm,$no_pb_cutting, $id_pb,
								$this->input->post('id_pb_detail_'.$i, TRUE), 
								$this->input->post('kode_'.$i, TRUE), $this->input->post('qty_'.$i, TRUE),
								$this->input->post('pjg_kain_'.$i, TRUE), $this->input->post('gelar_pjg_kain_'.$i, TRUE),
								$this->input->post('jenis_proses_'.$i, TRUE), $this->input->post('kode_brg_quilting_'.$i, TRUE), 
								$this->input->post('ukuran_bisbisan_'.$i, TRUE), $this->input->post('kode_brg_jadi_'.$i, TRUE),
								$this->input->post('nama_brg_jadi_other_'.$i, TRUE),
								$this->input->post('kode_brg_bisbisan_'.$i, TRUE), $this->input->post('sc_'.$i, TRUE),
								$this->input->post('id_sc_detail_'.$i, TRUE),
								$this->input->post('bordir_'.$i, TRUE), $this->input->post('bscutting_'.$i, TRUE),
								$this->input->post('kcutting_'.$i, TRUE) );
					}
					redirect('bonm-bbc/cform/view');

  }
  
  function updatedata(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
		$id_bonm 	= $this->input->post('id_bonm', TRUE);  
		$no_bonm 	= $this->input->post('no_bonm', TRUE);  
	    $tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);
	    $pisah1 = explode("-", $tgl_bonm);
	    $tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
		
		$no 	= $this->input->post('no', TRUE);
		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		
		$tgl = date("Y-m-d");
		$this->db->query(" UPDATE tm_apply_stok_proses_cutting SET tgl_bonm = '$tgl_bonm', tgl_update = '$tgl'
						where id= '$id_bonm' ");
		
		// no pb cutting
		$query2	= $this->db->query(" SELECT no_pb_cutting FROM tm_apply_stok_proses_cutting WHERE id = '$id_bonm' ");
		$hasilrow = $query2->row();
		$no_pb_cutting	= $hasilrow->no_pb_cutting;
		
		$query2	= $this->db->query(" SELECT id FROM tm_pb_cutting WHERE no_pb_cutting = '$no_pb_cutting' ");
		$hasilrow = $query2->row();
		$id_pb	= $hasilrow->id;
		
		$jumlah_input=$no-1;
		for ($i=1;$i<=$jumlah_input;$i++)
		{
			//if ($this->input->post('pjg_kain_'.$i, TRUE) != $this->input->post('pjg_kain_lama_'.$i, TRUE) ) {
				$pjg_kain = $this->input->post('pjg_kain_'.$i, TRUE);
				$pjg_kain_lama = $this->input->post('pjg_kain_lama_'.$i, TRUE);
				$kode = $this->input->post('kode_'.$i, TRUE);
				$harga = $this->input->post('harga_'.$i, TRUE);
				
				// update tabel detailnya
				$this->db->query(" UPDATE tm_apply_stok_proses_cutting_detail SET 
							qty_pjg_kain = '".$this->input->post('pjg_kain_'.$i, TRUE)."', 
							 gelar_pjg_kain = '".$this->input->post('gelar_pjg_kain_'.$i, TRUE)."',
							 id_schedule_cutting_detail = '".$this->input->post('id_sc_detail_'.$i, TRUE)."'
							where id= '".$this->input->post('id_bonm_detail_'.$i, TRUE)."' ");
				
				// cek quilting
				/*$query3	= $this->db->query(" SELECT quilting FROM tm_pb_cutting_detail WHERE id_pb_cutting = '$id_pb'
										AND kode_brg = '$kode' ");
				$hasilrow = $query2->row();
				$quilting	= $hasilrow->quilting; */
				
				// 1. reset stoknya #############################################
				//09-01-2013 WARNING! reset stok ga dipake lagi, karena udh ada fitur bon M keluar
				// cek dulu satuan brgnya. jika satuannya yard, maka qty (m) yg tadi diambil itu konversikan lagi ke yard
				/*	$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$satuan	= '';
						}
						
						if ($satuan == "Yard") {
							$qty_sat_awal = $pjg_kain_lama / 0.90; // 07-03-2012, rubah dari 0.91 ke 0.90
						}
						else
							$qty_sat_awal = $pjg_kain_lama;
				
				//if ($quilting == 'f') {
					$nama_tabel_stok = "tm_stok";
					$nama_tabel_tt = "tt_stok";
				//}
				//else {
				//	$nama_tabel_stok = "tm_stok_hasil_makloon";
				//	$nama_tabel_tt = "tt_stok_hasil_makloon";
				//}
				
				//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama+$qty_sat_awal; // menambah stok krn reset
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$kode,
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert($nama_tabel_stok,$data_stok);
				}
				else {
					$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode' ");
				}
				
				// 16-02-2012: update stok harga for reset
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode' 
											AND harga = '$harga' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$stokreset = $stok_lama+$qty_sat_awal;
				$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
									where kode_brg= '$kode' AND harga = '$harga' "); //
				//&&&&&&& end 16-02-2012 &&&&&&&&&&&
								
				// 2. update lagi stoknya #############################################
				if ($satuan == "Yard") { // konversikan lagi ke yard
					$qty_sat_awal = $pjg_kain / 0.90; // 07-03-2012, rubah dari 0.91 ke 0.90
				}
				else
					$qty_sat_awal = $pjg_kain;
				
				//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama-$qty_sat_awal; // mengurangi stok
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$kode,
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert($nama_tabel_stok, $data_stok);
				}
				else {
					$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode' ");
				}
				
				// &&&&&&&&&&&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&
					$selisih = 0;
					$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE kode_brg = '$kode' ORDER BY id ASC ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
							
						$temp_selisih = 0;				
						foreach ($hasil2 as $row2) {
							$stoknya = $row2->stok; // data ke-1: 500		data ke-2: 10
							$harganya = $row2->harga; // data ke-1: 20000	data ke-2: 370000
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) // temp_selisih = -6
									$selisih = $stoknya-$qty_sat_awal; // selisih1 = 500-506 = -6
								else
									$selisih = $stoknya+$temp_selisih; // selisih2 = 10-6 = 4
								
								if ($selisih < 0) {
									$temp_selisih = $selisih; // temp_selisih = -6
									$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '$kode' AND harga = '$harganya' "); //
									
								}
									
								if ($selisih > 0) { // ke-2 masuk sini
									if ($temp_selisih == 0)
										$temp_selisih = $qty_sat_awal; // temp_selisih = -6
										
									break;
								}
							}
						} // end for
					}
					
					if ($selisih != 0) {
						$new_stok = $selisih; // new_stok = 4
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$kode' AND harga = '$harganya' ");
					}
				// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end modifikasi 5 okt 2011
				
				// end update stok
				*/
		//	}
			/*else {				
				// update tabel detailnya
				$this->db->query(" UPDATE tm_apply_stok_proses_cutting_detail SET 
							 gelar_pjg_kain = '".$this->input->post('gelar_pjg_kain_'.$i, TRUE)."'
							where id= '".$this->input->post('id_bonm_detail_'.$i, TRUE)."' ");
			} */
		} // end for
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "bonm-bbc/cform/view/index/".$cur_page;
		else
			$url_redirectnya = "bonm-bbc/cform/cari/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
		//redirect('bonm-bbc/cform/view');
  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
    $data['isi'] = 'bonm-bbc/vformview';
    $keywordcari = "all";
	//$kode_bagian = '';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonm-bbc/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/bonm-bbc/cform/cari/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'bonm-bbc/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('template',$data);
  }

  function delete(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
    $kode 	= $this->uri->segment(4);
    $list_brg 	= $this->uri->segment(5);
    $cur_page 	= $this->uri->segment(6);
	$is_cari 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($kode, $list_brg);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "bonm-bbc/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "bonm-bbc/cform/cari/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
    //redirect('bonm-bbc/cform/view');
  }
  
  // popup utk input detail gelar dan panjang kainnya
  function show_popup_gelar_pjg_kain(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi 	= $this->uri->segment(4);
	$gelaran 	= $this->uri->segment(5);
			
	$data['posisi'] = $posisi;
	$data['gelaran'] = $gelaran;
	$this->load->view('bonm-bbc/vpopupgelarkain',$data);
  }
  
  // 16-03-2012
  function show_popup_sched_cutting(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$posisi 	= $this->uri->segment(4);
	if ($posisi == '') {
		$posisi = $this->input->post('posisi', TRUE);  
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $posisi == '') {
		$posisi 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari = "all";
	
	$jum_total = $this->mmaster->get_sched_cuttingtanpalimit($keywordcari);
							$config['base_url'] = base_url()."index.php/bonm-bbc/cform/show_popup_sched_cutting/".$posisi."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_sched_cutting($config['per_page'],$this->uri->segment(6), $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$this->load->view('bonm-bbc/vpopupschedcutting',$data);

  }

}
