<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}			
			$data['page_title_ip']		= $this->lang->line('page_title_ip');
			$data['form_tbl_ip_name']	= $this->lang->line('form_tbl_ip_name');
			$data['form_tbl_ip_ket']	= $this->lang->line('form_tbl_ip_ket');
			$data['form_tbl_ip_status']	= $this->lang->line('form_tbl_ip_status');
			
			$data['link_aksi']		= $this->lang->line('link_aksi');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			
			$this->load->model('ipaddress/mclass');
			
			$data['query']	= $this->mclass->view();
			$data['isi']		= 'ipaddress/vmainform';
			$this->load->view('template',$data);
			
			
	}
		function tambah() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}			
			$data['page_title_ip']		= $this->lang->line('page_title_ip');
			$data['form_tbl_ip_name']	= $this->lang->line('form_tbl_ip_name');
			$data['form_tbl_ip_ket']	= $this->lang->line('form_tbl_ip_ket');
			$data['form_tbl_ip_status']	= $this->lang->line('form_tbl_ip_status');
			
			$data['link_aksi']		= $this->lang->line('link_aksi');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			
			$this->load->model('ipaddress/mclass');
			
			$data['query']	= $this->mclass->view();
			$data['isi']		= 'ipaddress/vmainformadd';
			$this->load->view('template',$data);
			
			
	}
	function simpan() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$this->load->model('ipaddress/mclass');
		
		$e_ip_name	= $this->input->post('e_ip_name');
		$e_note	= $this->input->post('e_note');
		$cstatus	= $this->input->post('fstatus');
		
		if(!empty($e_ip_name)) {
			
			$qry_cekprinter = $this->mclass->CekIP($e_ip_name);
			
			if($qry_cekprinter->num_rows()>0) {
				$data['page_title_ip']		= $this->lang->line('page_title_ip');
				$data['form_tbl_ip_name']	= $this->lang->line('form_tbl_ip_name');
				$data['form_tbl_ip_ket']	= $this->lang->line('form_tbl_ip_ket');
				$data['form_tbl_ip_status']	= $this->lang->line('form_tbl_ip_status');
				$data['link_aksi']		= $this->lang->line('link_aksi');
				$data['button_simpan']	= $this->lang->line('button_simpan');
				$data['button_batal']	= $this->lang->line('button_batal');
				$data['detail']		= "";
				$data['list']		= "";
				$data['not_defined']	= "Maaf, Alamat IP yg anda masukan telah tersedia. Terimakasih.";
				$data['limages']	= base_url();
				
				$data['isi']	= $this->mclass->view();

				$this->load->view('ipaddress/vmainform',$data);
			}else{
				$this->mclass->msimpan($e_ip_name,$e_note,$cstatus);
			}
		} else {
			$data['page_title_ip']		= $this->lang->line('page_title_ip');
			$data['form_tbl_ip_name']	= $this->lang->line('form_tbl_ip_name');
			$data['form_tbl_ip_ket']	= $this->lang->line('form_tbl_ip_ket');
			$data['form_tbl_ip_status']	= $this->lang->line('form_tbl_ip_status');
			
			$data['link_aksi']		= $this->lang->line('link_aksi');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih.";
			
			$data['isi']	= $this->mclass->view();
			
			$this->load->view('ipaddress/vmainform',$data);			
		}
	}
	
	function actdelete() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$id = $this->uri->segment(4);

		$this->load->model('ipaddress/mclass');
		$this->mclass->delete($id);
	}
	
	function edit() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$ip	= $this->uri->segment('4');

		$data['page_title_ip']		= $this->lang->line('page_title_ip');
		$data['form_tbl_ip_name']	= $this->lang->line('form_tbl_ip_name');
		$data['form_tbl_ip_ket']	= $this->lang->line('form_tbl_ip_ket');
		$data['form_tbl_ip_status']	= $this->lang->line('form_tbl_ip_status');
			
		$data['link_aksi']		= $this->lang->line('link_aksi');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']= "";
		$data['limages']	= base_url();
		
		$this->load->model('ipaddress/mclass');

		$query	= $this->mclass->get($ip);
		
		$data['e_ip_name']  = $query['e_ip'];   
		$data['e_note']		= $query['e_note'];
		$data['cstatus']	= $query['f_active'];
		$data['iip']		= $query['iip'];
		$data['isi']		= 'ipaddress/veditform';
			$this->load->view('template',$data);
	
	}
	
	function actedit() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
		$e_ip_name	= $this->input->post('e_ip_name');
		$e_note	= $this->input->post('e_note');
		$cstatus	= $this->input->post('fstatus');
		$ip	= $this->input->post('i_ip');
		
		$this->load->model('ipaddress/mclass');
		
		if(!empty($e_ip_name)) {
			$this->mclass->update($e_ip_name,$e_note,$cstatus,$ip);	
		}else{
			
			$data['page_title_ip']		= $this->lang->line('page_title_ip');
			$data['form_tbl_ip_name']	= $this->lang->line('form_tbl_ip_name');
			$data['form_tbl_ip_ket']	= $this->lang->line('form_tbl_ip_ket');
			$data['form_tbl_ip_status']	= $this->lang->line('form_tbl_ip_status');
		
			$data['link_aksi']		= $this->lang->line('link_aksi');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Maaf, Printer tsb gagal di update, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('ipaddress/mclass');
			$data['isi']	= $this->mclass->view();
			
			$this->load->view('ipaddress/vmainform',$data);			
		}
	}
}
?>
