<?php
class Creport extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-forecast-unit-jahit/mreport');
  }
  
  function index(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'info-forecast-unit-jahit/vformmutasiunit';
	$this->load->view('template',$data);
  }
  
   function viewmutasiunit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-forecast-unit-jahit/vviewmutasiunit';
    
	//~ $date_from = $this->input->post('date_from', TRUE);
	//~ $pisah1 = explode("-", $date_from);
	//~ $tanggal= $pisah1[0];
	//~ $bulan= $pisah1[1];
	//~ $tahun= $pisah1[2];
	//~ $tgldari= $tahun."-".$bulan."-".$tanggal;
	//~ 
	//~ $date_to = $this->input->post('date_to', TRUE); 
	//~ $pisah1 = explode("-", $date_to); 
	//~ $tanggal= $pisah1[0];
	//~ $bulan= $pisah1[1];
	//~ $tahun= $pisah1[2];
	//~ $tglke= $tahun."-".$bulan."-".$tanggal;
	
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	
	$data['query'] = $this->mreport->get_mutasi_unit($bulan, $tahun, $unit_jahit);
	$data['jum_total'] = count($data['query']);
	//~ $data['date_from'] = $date_from;
	//~ $data['date_to'] = $date_to;
	
	
	if ($unit_jahit != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;	
	$data['unit_jahit'] = $unit_jahit;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
	$this->load->view('template',$data);
  }
  function export_excel_funit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
   		set_time_limit(36000);
		ini_set("memory_limit","512M");
		ini_set("max_execution_time","36000");
		
		$export_excel1 = $this->input->post('export_excel', TRUE);
		$export_ods1 = $this->input->post('export_ods', TRUE);
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	
		$query = $this->mreport->get_mutasi_unit($bulan, $tahun, $unit_jahit);
	  		
	 $html_data = "  
	 <table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
	   <tr>
			<th colspan='16' align='center'>Laporan Forecast Unit Jahit</th>
		 </tr>
		 <tr>
			<th colspan='16' align='center'>Periode: $bulan - $tahun</th>
		 </tr>
		 </table>
		 ";
	
	  		if (is_array($query)) {
		for($a=0;$a<count($query);$a++){	
	  $html_data .= " 
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 ";
		 
	$html_data .= " <tr> 
			<th colspan='16' align='left'>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</th> 
		</tr>
		 	 
 <tr class='judulnya'>
		 <th width='1%' rowspan='2'>No</th>
		 <th width='4%'rowspan='2'>Kode</th>
		 <th width='16%'rowspan='2'>Nama Brg WIP</th>
		 <th width='2%' rowspan='2'>Forecast (FC)</th>
		<th colspan='4'>Kirim</th>
		<th colspan='4'>Terima</th>
		<th rowspan='2'>Sisa Forecast</th>
		<th colspan='3'>Persentase Pembulatan</th>
		
		
	 </tr>
	 <tr class='judulnya'>
		 <th width='7%'>Bahan Baku (BB)</th>
		 <th width='7%'>Sisa Bulan Sebelumnya</th>
		 <th width='7%'>Perbaikan (KP)</th>
		 <th width='7%'>Total</th>
		 <th width='7%'>Barang Jadi (BJ)</th>
		 <th width='7%'>Perbaikan (TP)</th>
		 <th width='7%'>Retur Bahan Baku</th>
		 <th width='7%'>Total</th>
		 <th width='7%'>KBJ / BB (%)</th>
		 <th width='7%'>BB / FC (%)</th>
		 <th width='7%'>KP / TP (%)</th>
	 </tr>
	</thead>
	<tbody>
	  ";
	  
	  $detail_fc= $query[$a]['data_detail'];
		if (is_array($detail_fc)) {
			$jum_forecast =0 ; $total_bb=0; $total_sfk= 0 ; $total_tp= 0 ; $total_tb = 0 ; $total_kp =0; $total_rbb = 0;
			$total_kel = 0 ; $total_msk = 0 ; $total_sf = 0 ; $per1 = 0 ; $per2 = 0 ; $per3 = 0 ;
				for($j=0;$j<count($detail_fc);$j++){
				
				 $html_data .= "
					<tr>	
					<td align='center'>". ($j+1) ."</td>
					<td>". $detail_fc[$j]['kode_brg'] ."</td>
					<td>". $detail_fc[$j]['nama_brg'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['jum_forecast'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_bb'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_sfk'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_kp'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_kel'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_tb'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_tp'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_rbb'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_msk'] ."</td>
					<td align ='center'>&nbsp;". $detail_fc[$j]['total_sf'] ."</td>
					<td align ='center'>&nbsp;". number_format($detail_fc[$j]['per1']) ."</td>
					<td align ='center'>&nbsp;". number_format($detail_fc[$j]['per2']) ."</td>
					<td align ='center'>&nbsp;". number_format($detail_fc[$j]['per3']) ."</td>
					</tr>";
									
					}
				}
		 $html_data.="</tbody></table>";
	 }
}		 
		 $nama_file = "laporan_mutasi_forecast_unit_jahit";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
			
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
	  
  }
  function caribrgwip_laptransaksi(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil kode, nama
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = '';
			$nama_brg_wip = '';
		}
		
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['id_brg_wip'] = $id_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('info-forecast-unit-jahit/vinfobrgwip2', $data); 
		return true;
  }
  function transaksi(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit_jahit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'info-forecast-unit-jahit/vformlaptransaksihasiljahit';
	$this->load->view('template',$data);
  }
   function viewtransaksiunitjahit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
   
	$unit_jahit = $this->input->post('unit_jahit', TRUE);
	//$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	//$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	//$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	
	
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	//~ $date_from = $this->input->post('date_from', TRUE);
	//~ $date_to = $this->input->post('date_to', TRUE);
	
	// 12-01-2016
	$jumbrg = $this->input->post('jumbrg', TRUE);  
	$list_id_brg_wip = "";
	for ($i=1; $i<=$jumbrg; $i++) {
		$list_id_brg_wip.= $this->input->post('id_brg_wip_'.$i, TRUE).";";
	}
	
	//$data['query'] = $this->mreport->get_transaksi_unit_jahit($unit_jahit, $bulan, $tahun, $list_id_brg_wip);
	$data['query'] = $this->mreport->get_transaksi_unit_jahit($unit_jahit, $bulan, $tahun, $list_id_brg_wip);
	//print_r($data['query']); die();
	$data['jum_total'] = count($data['query']);
	
	if ($unit_jahit != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_jahit'] = $unit_jahit;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
		
	/*if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember"; */
		
	/*$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip; */
	$data['list_id_brg_wip'] = $list_id_brg_wip;
	$data['jumbrg'] = $jumbrg;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	//$data['bulan'] = $bulan;
	//$data['tahun'] = $tahun;
	//$data['nama_bulan'] = $nama_bln;
	 $data['isi'] = 'info-forecast-unit-jahit/vviewlaptransaksiunitjahit';
	$this->load->view('template',$data);
  }
  }

