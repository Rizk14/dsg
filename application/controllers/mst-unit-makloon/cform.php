<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-unit-makloon/mmaster');
  }

  function index(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->kode_unit;
			$elokasi = $row->lokasi;
			$enama = $row->nama;
		}
	}
	else {
			$ekode = '';
			$elokasi = '';
			$enama = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['elokasi'] = $elokasi;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-unit-makloon/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode_unit', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode_unit', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			//$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			//redirect('mst-unit-makloon/cform');
			$data['isi'] = 'mst-unit-makloon/vmainform';
			$data['msg'] = 'Kode Unit dan Nama harus diisi..!';
			$ekode = '';
			$elokasi = '';
			$enama = '';
			$edit = '';
			$data['ekode'] = $ekode;
			$data['elokasi'] = $elokasi;
			$data['enama'] = $enama;
			$data['edit'] = $edit;
			$data['query'] = $this->mmaster->getAll();
			$this->load->view('template',$data);
		}
		else
		{
			$kode 	= $this->input->post('kode_unit', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$lokasi = $this->input->post('lokasi', TRUE);
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-unit-makloon/vmainform';
					$data['msg'] = 'Data sudah ada..!';
					$ekode = '';
					$elokasi = '';
					$enama = '';
					$edit = '';
					$data['ekode'] = $ekode;
					$data['elokasi'] = $elokasi;
					$data['enama'] = $enama;
					$data['edit'] = $edit;
					
					$data['query'] = $this->mmaster->getAll();
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($kode, $kodeedit, $lokasi, $nama, $goedit);
					redirect('mst-unit-makloon/cform');
				}
			} // end if goedit == ''
			else {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'mst-unit-makloon/vmainform';
						$data['msg'] = "Data kode ".$kode." sudah ada..!";
						$ekode = '';
						$elokasi = '';
						$enama = '';
						$edit = '';
						$data['ekode'] = $ekode;
						$data['elokasi'] = $elokasi;
						$data['enama'] = $enama;
						$data['edit'] = $edit;
						
						$data['query'] = $this->mmaster->getAll();
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kode, $kodeedit, $lokasi, $nama, $goedit);
						redirect('mst-unit-makloon/cform');
					}
				}
				else {
					$this->mmaster->save($kode, $kodeedit, $lokasi, $nama, $goedit);
					redirect('mst-unit-makloon/cform');
				}
			}
			
		}
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-unit-makloon/cform');
  }
  
  
}
