<?php
class Cform extends CI_Controller{
  	function __construct (){
	    parent::__construct();
	    $this->load->library('pagination');
	    $this->load->model('info-akunting/mmaster');
  	}

	function bukubesar(){	
		$is_logged_in=$this->session->userdata('is_logged_in');
		if (!isset($is_logged_in ) || $is_logged_in != TRUE){
			redirect('loginform');
			}
			
			$data['list_coa'] =$this->mmaster->getcoa();
			$data['isi'] = 'info-akunting/vmainformbukubesar';
			$this->load->view('template',$data);	
	}  
	
	function viewbukubesar(){
		
		$is_logged_in=$this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
		}
		
		$id_coa=$this->input->post('id_coa');
		$date_from=$this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		
		$coa=$this->mmaster->get_coa_item($id_coa);
		
		$saldoawal=$this->mmaster->get_saldoawal($date_from,$id_coa);
		$query=$this->mmaster->get_allbukubesar($id_coa,$date_from,$date_to,$coa[0]['kode']);		
		
		$data['coa']=$coa;
		$data['date_from']=$date_from;
		$data['date_to']=$date_to;
		$data['query']=$query;

		$data['saldoawal']=$saldoawal;
		$data['isi']='info-akunting/vformviewbukubesar';
		$this->load->view('template',$data);
	}

	function neracasaldo(){
		$is_logged_in =$this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){	
			redirect('loginform');
			}
			
		$data['isi']='info-akunting/vmainformneracasaldo';
		$this->load->view('template',$data);
	}

	function kk(){
		$is_logged_in =$this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){	
			redirect('loginform');
		}
		$data['list_area']=$this->mmaster->getarea();
		$data['isi']='info-akunting/vmainformkk';
		$this->load->view('template',$data);
	}

	function viewkk(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$date_from = $this->input->post('date_from');
		$date_to   = $this->input->post('date_to');
		$id_area   = $this->input->post('id_area');
		
		$query 			= $this->mmaster->viewkaskecil($date_from,$date_to,$id_area);
		$kode_coa 		= $this->mmaster->get_kodecoa($id_area);
		$saldoawal_k 	= $this->mmaster->saldoawal_k($date_from,$date_to, $id_area, $kode_coa);
		$kredit_k 		= $this->mmaster->kredit_k($date_from,$date_to, $id_area);
		$debet_k  		= $this->mmaster->debet_k($date_from,$date_to, $id_area);
		$totsaldo_k		= ($saldoawal_k+$debet_k)-$kredit_k;
		$jumlah 		= count($query);

		$sql2= " tm_area where id ='$id_area'  ";
		$query2 = $this->db->get($sql2);
		if($query2->num_rows > 0){
			$hasilrow =$query2->row();
			$nama_area = $hasilrow->nama;
		}else{
			$nama_area='';	
		}

		$data['totsaldo_k']= $totsaldo_k;
		$data['query']=$query;		
		$data['date_from']=$date_from;
		$data['date_to']=$date_to;		
		$data['jumlah']=$jumlah;		
		$data['nama_area']=$nama_area;
		$data['id_area']=$id_area;
		$data['isi']='info-akunting/vformviewkk';
		$this->load->view('template',$data);	
	}

	function kb(){
		$is_logged_in =$this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){	
			redirect('loginform');
		}
		$data['list_area']=$this->mmaster->getarea();
		$data['isi']='info-akunting/vmainformkb';
		$this->load->view('template',$data);
	}

	function viewkb(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$date_from = $this->input->post('date_from');
		$date_to   = $this->input->post('date_to');
		$id_area   = $this->input->post('id_area');
		
		$query 		= $this->mmaster->viewkasbesar($date_from,$date_to,$id_area);
		$saldoawal  = $this->mmaster->saldoawal($date_from,$date_to);
		$kredit  	= $this->mmaster->kredit($date_from,$date_to);
		$debet  	= $this->mmaster->debet($date_from,$date_to);
		$totsaldo	= ($saldoawal+$debet)-$kredit;
		$jumlah 	= count($query);
		
		$sql2= " tm_area where id ='$id_area'  ";
		$query2 = $this->db->get($sql2);
		if($query2->num_rows > 0){
			$hasilrow =$query2->row();
			$nama_area = $hasilrow->nama;
		}else{
			$nama_area='';	
		}

		$data['totsaldo'] 	= $totsaldo;
		$data['query']=$query;		
		$data['id_area']=$id_area;
		$data['date_from']=$date_from;
		$data['date_to']=$date_to;		
		$data['jumlah']=$jumlah;		
		$data['nama_area']=$nama_area;
		$data['isi']='info-akunting/vformviewkb';
		$this->load->view('template',$data);	
	}

	function bank(){
		$is_logged_in =$this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){	
			redirect('loginform');
		}
		$data['list_area']=$this->mmaster->getarea();
		$data['list_bank']=$this->mmaster->getbank();
		$data['isi']='info-akunting/vmainformbank';
		$this->load->view('template',$data);
	}

	function ju(){
		$is_logged_in =$this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){	
			redirect('loginform');
		}
		$data['list_area']=$this->mmaster->getarea();
		
		$data['isi']='info-akunting/vmainformju';
		$this->load->view('template',$data);
	}

	function viewju(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$date_from = $this->input->post('date_from');
		$date_to   = $this->input->post('date_to');
		$id_area   = $this->input->post('id_area');
		
		$query=$this->mmaster->viewju($date_from,$date_to,$id_area);
		$jumlah=count($query);
		
		$sql2= " tm_area where id ='$id_area'  ";
		$query2 = $this->db->get($sql2);
		if($query2->num_rows > 0){
			$hasilrow =$query2->row();
			$nama_area = $hasilrow->nama;
		}else{
			$nama_area='';	
		}

		$data['query']=$query;		
		$data['id_area']=$id_area;
		$data['date_from']=$date_from;
		$data['date_to']=$date_to;		
		$data['jumlah']=$jumlah;		
		$data['nama_area']=$nama_area;
		$data['isi']='info-akunting/vformviewju';
		$this->load->view('template',$data);	
	}

	function viewbank(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$date_from = $this->input->post('date_from');
		$date_to   = $this->input->post('date_to');
		$id_area   = $this->input->post('id_area');
		$id_bank   = $this->input->post('id_bank');
		
		$query 			= $this->mmaster->viewbank($date_from,$date_to,$id_area,$id_bank);
		$saldoawal_b 	= $this->mmaster->saldoawal_b($date_from,$date_to, $id_bank);
		$kredit_b  		= $this->mmaster->kredit_b($date_from,$date_to, $id_bank);
		$debet_b  		= $this->mmaster->debet_b($date_from,$date_to, $id_bank);
		$totsaldo_b		= ($saldoawal_b+$debet_b)-$kredit_b;
		$jumlah 		= count($query);
		
		// $sql2= " tm_area where id ='$id_area'  ";
		// $query2 = $this->db->get($sql2);
		// if($query2->num_rows > 0){
		// 	$hasilrow =$query2->row();
		// 	$nama_area = $hasilrow->nama;
		// 	}
		// 	else{
		// 	$nama_area='ALL';	
		// 		}
		$sql3= " tm_bank where id_coa ='$id_bank'  ";
		$query3 = $this->db->get($sql3);
		if($query3->num_rows > 0){
			$hasilrow3 =$query3->row();
			$nama_bank = $hasilrow3->nama;
		}else{
			$nama_bank='ALL';	
		}

		$data['totsaldo_b'] 	= $totsaldo_b;
		$data['query']=$query;		
		$data['date_from']=$date_from;
		$data['date_to']=$date_to;		
		$data['jumlah']=$jumlah;	
		//$data['id_area']=$id_area;
		$data['id_bank']=$id_bank;	
		// $data['nama_area']=$nama_area;
		$data['nama_bank']=$nama_bank;
		$data['isi']='info-akunting/vformviewbank';
		$this->load->view('template',$data);	
	}

	function exportkasbank(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$date_from = $this->input->post('date_from',TRUE);
		$date_to   = $this->input->post('date_to',TRUE);
		$id_area   = $this->input->post('id_area',TRUE);
		$id_bank   = $this->input->post('id_bank',TRUE);
		$export_excel1  = $this->input->post('export_excel',TRUE);
		$export_ods  = $this->input->post('export_ods',TRUE);

		$query 			= $this->mmaster->viewbank($date_from,$date_to,$id_area,$id_bank);
		$saldoawal_b 	= $this->mmaster->saldoawal_b($date_from,$date_to, $id_bank);
		$kredit_b  		= $this->mmaster->kredit_b($date_from,$date_to, $id_bank);
		$debet_b  		= $this->mmaster->debet_b($date_from,$date_to, $id_bank);
		$totsaldo_b		= ($saldoawal_b+$debet_b)-$kredit_b;
		$jumlah 		= count($query);
		
		
		$html_data = "
		<table border='1 cellpadding= '1' cellspacing = '1' width='100%'>
			<thead>
			<tr>
				<th colspan='7' align='center'> Laporan Akunting Kas Bank </th>
			</tr>		 
			<tr>
				<th colspan='7' align='center'>Periode: $date_from s.d $date_to</th>
			</tr>
			<tr class='judulnya'>
				<th>No</th>
				<th>No Transaksi</th>
				<th>Tanggal Transaksi</th>
				<th>No. Voucher</th>
				<th>Keterangan</th>
				<th>CoA</th>
				<th>Debet</th>
				<th>Kredit</th>
				<th>Saldo</th>
				<th>Keterangan</th>
			</tr>
			</thead>
			<tbody>";

				if(is_array($query)){
					
					$no=1;
					$totalsaldo = 0;
					$debet=0;
					$kredit=0;

					$tot_debet = 0;
					$tot_kredit = 0;
					$tot_saldo = 0;
					$total =0;
					$total_a=0;
					$total_b=0;

					$html_data.="<td align='center' colspan='8'><b>Saldo Awal</td>";
					$html_data.="<td><b>".number_format($totsaldo_b,2,',','.')."</td>";

					for($i=0;$i<count($query);$i++){
						$debet 	= $query[$i]['debet'];
						$kredit = $query[$i]['kredit'];

						$tot_debet  += $query[$i]['debet'];
						$tot_kredit += $query[$i]['kredit'];

						$html_data.="<tr>";
						$html_data.="<td>".$no."</td>" ;
						$html_data.="<td>".$query[$i]['no_transaksi']."</td>";
						$html_data.="<td>".$query[$i]['tanggal']."</td>";
						$html_data.="<td>".$query[$i]['irv']."</td>";
						$html_data.="<td>".$query[$i]['keterangan']."</td>";
						$html_data.="<td>".$query[$i]['kode_coa']."</td>";
						$html_data.="<td>".$query[$i]['debet']."</td>";
						$html_data.="<td>".$query[$i]['kredit']."</td>";
						$html_data.="<td>".number_format($totalsaldo=$totsaldo_b+$debet-$kredit,2,',','.')."</td>";
						$html_data.="<td>".$query[$i]['nama_coa']."</td>";
						$html_data.="</tr>";
						$total_a += $totalsaldo;
						$no++;
					}
					$total_b = $totsaldo_b + $total_a;			
					$html_data.="<td align='center' colspan='6'><b>Total</td>";
					$html_data.="<td><b>".number_format($tot_debet,2,',','.')."</td>";
					$html_data.="<td><b>".number_format($tot_kredit,2,',','.')."</td>";
					$html_data.="<td><b>".number_format($total_b,2,',','.')."</td>";	
				}
				$html_data.="
	 		</tbody>
		</table><br>";
			// ====================================================================
			$nama_file = "laporan_akunting_kas_bank ";
			if ($export_excel1 != '')
				$nama_file.= ".xls";
			else
				$nama_file.= ".ods";
			$data = $html_data;

			$dir=getcwd();
			include($dir."/application/libraries/generateExcelFile.php");
			return true;
	 }
  
  
	 function exportju(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$date_from = $this->input->post('date_from',TRUE);
		$date_to   = $this->input->post('date_to',TRUE);
		$id_area   = $this->input->post('id_area',TRUE);

		$export_excel1  = $this->input->post('export_excel',TRUE);
		$export_ods  = $this->input->post('export_ods',TRUE);
		
		$query=$this->mmaster->viewju($date_from,$date_to,$id_area);
		$jumlah=count($query);
		
		
		$html_data = "
		<table border='1 cellpadding= '1' cellspacing = '1' width='100%'>
			<thead>
			<tr>
					<th colspan='7' align='center'> Laporan Akunting Jurnal Umum </th>
				 </tr>
				 
				 <tr>
					<th colspan='7' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr>
			<tr class='judulnya'>
				<th>No</th>
				<th>Kode - Bank</th>
				<th>No Transaksi</th>
				<th>CoA</th>
				<th>Tanggal CoA</th>
				<th>Value</th>
				<th>Keterangan</th>
			</tr>
			</thead>
			<tbody>";
				if(is_array($query)){
					
					$no=1;
					for($i=0;$i<count($query);$i++){
						$html_data.="<tr>";
						$html_data.= "<td>".$no."</td>" ;
						$html_data.= "<td>".$query[$i]['no_jurnal']."</td>";
						$html_data.= "<td>".$query[$i]['tanggal']."</td>";
						$html_data.= "<td>".$query[$i]['kode_coa']." - ".$query[$i]['nama_coa']."</td>";
						
						$html_data.= "<td>".$query[$i]['value']."</td>";
						$html_data.= "<td>".$query[$i]['keterangan']."</td>";
						$html_data.="</tr>";
						$no++;
					}
				}
				$html_data.="
	 		</tbody>
		</table><br>";
			// ====================================================================
			$nama_file = "laporan_akunting_jurnal_umum ";
			if ($export_excel1 != '')
				$nama_file.= ".xls";
			else
				$nama_file.= ".ods";
			$data = $html_data;

			$dir=getcwd();
			include($dir."/application/libraries/generateExcelFile.php");
			return true;
	}

  
	function exportkaskecil(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$date_from = $this->input->post('date_from',TRUE);
		$date_to   = $this->input->post('date_to',TRUE);
		$id_area   = $this->input->post('id_area',TRUE);
		
		$export_excel1  = $this->input->post('export_excel',TRUE);
		$export_ods  = $this->input->post('export_ods',TRUE);
		
		$query 			= $this->mmaster->viewkaskecil($date_from,$date_to,$id_area);
		$kode_coa 		= $this->mmaster->get_kodecoa($id_area);
		$saldoawal_k 	= $this->mmaster->saldoawal_k($date_from,$date_to, $id_area, $kode_coa);
		$kredit_k 		= $this->mmaster->kredit_k($date_from,$date_to, $id_area);
		$debet_k  		= $this->mmaster->debet_k($date_from,$date_to, $id_area);
		$totsaldo_k		= ($saldoawal_k+$debet_k)-$kredit_k;
		$jumlah 		= count($query);	
		
		$html_data = "
		<table border='1 cellpadding= '1' cellspacing = '1' width='100%'>
			<thead>
				<tr>
					<th colspan='9' align='center'> Laporan Akunting Kas Kecil </th>
				</tr>
				<tr>
					<th colspan='9' align='center'> CV. Duta Setia Garmen</th>
					</tr>
				<tr>
					<th colspan='9' align='center'>Periode: $date_from s.d $date_to</th>
				</tr>
			 	<tr class='judulnya'>
					<th>No</th>
					<th>No Transaksi</th>
					<th>Tanggal Transaksi</th>
					<th>No. Voucher</th>
					<th>Keterangan</th>
					<th>CoA</th>
					<th>Debet</th>
					<th>Kredit</th>
					<th>Saldo</th>				
					<th>Keterangan</th>
				</tr>
			</thead>
			<tbody>";
				if(is_array($query)){
					
					$no=1;
					$totalsaldo = 0;
					$debet=0;
					$kredit=0;

					$tot_debet = 0;
					$tot_kredit = 0;
					$tot_saldo = 0;
					$total =0;
					$total_a=0;
					$total_b=0;

					$html_data.="<td align='center' colspan='8'><b>Saldo Awal</td>";
					$html_data.="<td><b>".number_format($totsaldo_k,2,',','.')."</td>";

					for($i=0;$i<count($query);$i++){
						$debet 	= $query[$i]['debet'];
						$kredit = $query[$i]['kredit'];

						$tot_debet  += $query[$i]['debet'];
						$tot_kredit += $query[$i]['kredit'];

						$html_data.="<tr>";
						$html_data.="<td>".$no."</td>" ;
						$html_data.= "<td>".$query[$i]['no_transaksi']."</td>";
							$html_data.= "<td>".$query[$i]['tanggal']."</td>";
							$html_data.= "<td>".$query[$i]['irv']."</td>";
							$html_data.= "<td>".$query[$i]['keterangan']."</td>";
							$html_data.= "<td>".$query[$i]['kode_coa']."</td>";
							$html_data.= "<td>".number_format($query[$i]['debet'],2,',','.')."</td>";	
							$html_data.= "<td>".number_format($query[$i]['kredit'],2,',','.')."</td>";	
							$html_data.= "<td>".number_format($totalsaldo=$totsaldo_k+$debet-$kredit,2,',','.')."</td>";
							$html_data.= "<td>".$query[$i]['nama_coa']."</td>";
						$html_data.="</tr>";
						$total_a += $totalsaldo;
						$no++;
					}
					$total_b = $totsaldo_k + $total_a;
					$html_data.= "<td align='center' colspan='6'><b>Total</td>";
					$html_data.= "<td><b>".number_format($tot_debet,2,',','.')."</td>";
					$html_data.= "<td><b>".number_format($tot_kredit,2,',','.')."</td>";
					$html_data.= "<td><b>".number_format($total_b,2,',','.')."</td>";
				}
				$html_data.="
		 	</tbody>
		</table><br>";
			// ====================================================================
			$nama_file = "laporan_akunting_kas_kecil ";
			if ($export_excel1 != '')
				$nama_file.= ".xls";
			else
				$nama_file.= ".ods";
			$data = $html_data;

			$dir=getcwd();
			include($dir."/application/libraries/generateExcelFile.php");
			return true;
	}
  
	 function exportkasbesar(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$date_from = $this->input->post('date_from',TRUE);
		$date_to   = $this->input->post('date_to',TRUE);
		$id_area   = $this->input->post('id_area',TRUE);
		
		$export_excel1  = $this->input->post('export_excel',TRUE);
		$export_ods  = $this->input->post('export_ods',TRUE);
		
		$query 		= $this->mmaster->viewkasbesar($date_from,$date_to,$id_area);
		$saldoawal  = $this->mmaster->saldoawal($date_from,$date_to);
		$kredit  	= $this->mmaster->kredit($date_from,$date_to);
		$debet  	= $this->mmaster->debet($date_from,$date_to);
		$totsaldo	= ($saldoawal+$debet)-$kredit;
		$jumlah 	= count($query);	
		
		$html_data = "
		<table border='1 cellpadding= '1' cellspacing = '1' width='100%'>
			<thead>
				<tr>
					<th colspan='9' align='center'> Laporan Akunting Kas Besar </th>
				</tr>
				<tr>
					<th colspan='9' align='center'> CV. Duta Setia Garmen</th>
				</tr>
				<tr>
					<th colspan='9' align='center'>Periode: $date_from s.d $date_to</th>
				</tr>
				<tr>
					<th colspan='9' ></th>
				</tr>
				<tr class='judulnya'>
					<th>No</th>
					<th>No Transaksi</th>
					<th>Tanggal Transaksi</th>
					<th>No. Voucher</th>
					<th>Keterangan</th>
					<th>CoA</th>
					<th>Debet</th>
					<th>Kredit</th>
					<th>Saldo</th>				
					<th>Keterangan</th>
				</tr>
			</thead>
			<tbody>
				";
				if(is_array($query)){
					
				$no=1;
				$totalsaldo = 0;
				$debet=0;
				$kredit=0;

				$tot_debet = 0;
				$tot_kredit = 0;
				$tot_saldo = 0;
				$total =0;
				$total_a=0;
				$total_b=0;

				$html_data.="<td align='center' colspan='8'><b>Saldo Awal</td>";
				$html_data.="<td><b>".number_format($totsaldo)."</td>";

					for($i=0;$i<count($query);$i++){
						$debet 	= $query[$i]['debet'];
						$kredit = $query[$i]['kredit'];

						$tot_debet  += $query[$i]['debet'];
						$tot_kredit += $query[$i]['kredit'];
						$html_data.= "<tr>";
						$html_data.= "<td>".$no."</td>" ;
						$html_data.= "<td>".$query[$i]['no_transaksi']."</td>";
						$html_data.= "<td>".$query[$i]['tanggal']."</td>";
						$html_data.= "<td>".$query[$i]['irv']."</td>";
						$html_data.= "<td>".$query[$i]['keterangan']."</td>";
						$html_data.= "<td>".$query[$i]['kode_coa']."</td>";
						$html_data.= "<td>".number_format($query[$i]['debet'])."</td>";	
						$html_data.= "<td>".number_format($query[$i]['kredit'])."</td>";	
						//$html_data.="<td>".number_format($query[$i]['value'],0,',','.')."</td>";
						$html_data.= "<td>".number_format($totalsaldo=$totsaldo+$debet-$kredit)."</td>";
						$html_data.= "<td>".$query[$i]['nama_coa']."</td>";
						$html_data.="</tr>";
						$total_a += $totalsaldo;					
						$no++;
					}
					$total_b = $totsaldo + $total_a;
					$html_data.= "<td align='center' colspan='6'><b>Total</td>";
					$html_data.= "<td><b>".number_format($tot_debet)."</td>";
					$html_data.= "<td><b>".number_format($tot_kredit)."</td>";
					$html_data.= "<td><b>".number_format($total_b)."</td>";
				}
				$html_data.="
		 	</tbody>
		</table><br>";
			// ====================================================================
			$nama_file = "laporan_akunting_kas_besar ";
			if ($export_excel1 != '')
				$nama_file.= ".xls";
			else
				$nama_file.= ".ods";
			$data = $html_data;

			$dir=getcwd();
			include($dir."/application/libraries/generateExcelFile.php");
			return true;
	}
  
	function exportbukubesar(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect ('loginform');
		}
		
		$id_coa=$this->input->post('id_coa');
		$date_from=$this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$export_excel1  = $this->input->post('export_excel',TRUE);
		$export_ods  = $this->input->post('export_ods',TRUE);
		
		$coa=$this->mmaster->get_coa_item($id_coa);
		
		$saldoawal=$this->mmaster->get_saldoawal($date_from,$id_coa);
		$query=$this->mmaster->get_allbukubesar($id_coa,$date_from,$date_to,$coa[0]['kode']);		
		$jumlah=count($query);
		
		
		$html_data = "
		<table border='1 cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
			<tr>
				<th colspan='7' align='center'> Laporan Akunting Buku Besar  </th>
				
			
			 <tr>
			 <th colspan='7' align='center'> Nama Coa ".$coa[0]['nama'] ."  </th>
			  </tr>
			 <tr>
				<th colspan='7' align='center'>Periode: $date_from s.d $date_to</th>
			 </tr>
			<tr>
				<th>No</th>
				<th>Tanggal</th>
				<th>No Transaksi</th>
				<th>Deskripsi</th>
				<th>Debet</th>
				<th>Kredit</th>
				<th>Saldo Akhir</th>
			</thead>
			</tr>	
					
			<tr>
				<th colspan='4'>".$coa[0]['kode']."</th>
				<th colspan='2'>Saldo Awal</th>
				<th>".$saldoawal."</th>
			</tr>
			</thead>
			<tbody>";

				if(is_array($query)){
					
					$no=1;
					$sakhir=$saldoawal;
					$totdeb=0;$totcre=0;

					for($a=0;$a<count($query);$a++){
						$html_data.= "<tr>";
						$html_data.= "<td>".$no."</td>" ;
						$html_data.= "<td>".$query[$a]['d_mutasi']."</td>";
						$html_data.= "<td>".$query[$a]['i_refference']."</td>";
						$html_data.= "<td>".$query[$a]['deskripsi']."</td>";
						switch($query[$a]['f_debet']) {
						    case 't' :
							    if($query[$a]['flagvmutasidebet']<$query[$a]['v_mutasi_kredit'])
							    {
								   $query[$a]['v_mutasi_kredit']=$query[$a]['flagvmutasidebet'];
							    }
							    break;
						    case 'f' :
							    if($query[$a]['flagvmutasikredit']<$query[$a]['v_mutasi_debet'])
							    {
								   $query[$a]['v_mutasi_debet']=$query[$a]['flagvmutasikredit'];
							    }
							    break;
					    }
						$saldoawal+=  $query[$a]['v_mutasi_kredit'] - $query[$a]['v_mutasi_debet'];
						$html_data.= "<td>".$query[$a]['v_mutasi_kredit']."</td>";
						$totdeb+=$query[$a]['v_mutasi_kredit'];
						$html_data.= "<td>".$query[$a]['v_mutasi_debet']."</td>";
						$totcre+=$query[$a]['v_mutasi_debet'];
						
						$html_data.= "<td>".$saldoawal."</td>";
						$html_data.= "</tr>";
						$no++;
					}
				}
				$html_data.="
				<tr>
					<td colspan='4' align ='center'>TOTAL</td>
					<td>". $totdeb ."</td>
					<td>". $totcre ."</td>
					<td>".$saldoawal."</td>
				</tr>
	 		</tbody>	 	
		</table><br>";
			// ====================================================================
			$nama_file = "laporan_akunting_buku_besar ";
			if ($export_excel1 != '')
				$nama_file.= ".xls";
			else
				$nama_file.= ".ods";
			$data = $html_data;
			
			$dir=getcwd();
			include($dir."/application/libraries/generateExcelFile.php");
			return true;
	}
}