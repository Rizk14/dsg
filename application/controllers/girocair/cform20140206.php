<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu202')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_giro_bcair');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('girocair/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu202')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/girocair/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($iarea=='NA'){
     			$query = $this->db->query("
					select tr_area.e_area_name, 
						a.i_giro, 
						a.d_giro, 
						a.i_rv, 
						a.d_rv, 
						tm_dt.i_dt, 
						tm_dt.d_dt, 
						tr_customer.i_customer, 
						tr_customer.e_customer_name, 
						a.e_giro_bank, 
						a.v_jumlah, 
						a.v_sisa, 
						a.f_posting, 
						a.f_giro_batal, 
						a.i_area, tm_pelunasan.i_pelunasan 
						from tm_giro a 
						inner join tr_area on(a.i_area=tr_area.i_area)
						inner join tr_customer on(a.i_customer=tr_customer.i_customer)
						left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area)
						left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area)
						where ((tm_pelunasan.i_jenis_bayar!='02' and 
								tm_pelunasan.i_jenis_bayar!='03' and 
								tm_pelunasan.i_jenis_bayar!='04' and 
								tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
								(a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
								a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
  							and a.f_giro_tolak='f' 
								order by a.d_giro desc, a.i_giro desc ",false);										
      }else{
    			  $query = $this->db->query("
					  select tr_area.e_area_name, 
						  a.i_giro, 
						  a.d_giro, 
						  a.i_rv, 
						  a.d_rv, 
						  tm_dt.i_dt, 
						  tm_dt.d_dt, 
						  tr_customer.i_customer, 
						  tr_customer.e_customer_name, 
						  a.e_giro_bank, 
						  a.v_jumlah, 
						  a.v_sisa, 
						  a.f_posting, 
						  a.f_giro_batal, 
						  a.i_area, tm_pelunasan.i_pelunasan 
						  from tm_giro a 
						  inner join tr_area on(a.i_area=tr_area.i_area)
						  inner join tr_customer on(a.i_customer=tr_customer.i_customer)
						  left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area)
						  left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area)
						  where ((tm_pelunasan.i_jenis_bayar!='02' and 
								  tm_pelunasan.i_jenis_bayar!='03' and 
								  tm_pelunasan.i_jenis_bayar!='04' and 
								  tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
								  a.i_area='$iarea' and
								  (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
								  a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
    							and a.f_giro_tolak='f' 
								  order by a.d_giro desc, a.i_giro desc ",false);										
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_giro_bcair');
			$this->load->model('girocair/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('girocair/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu202')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_giro_bcair');
			$this->load->view('girocair/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu202')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/girocair/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($cari!=''){
        if($iarea=='NA'){
		      $query = $this->db->query(" select tr_area.e_area_name, 
									      a.i_giro, 
									      a.d_giro, 
									      a.i_rv, 
									      a.d_rv, 
									      tm_dt.i_dt, 
									      tm_dt.d_dt, 
									      tr_customer.i_customer, 
									      tr_customer.e_customer_name, 
									      a.e_giro_bank, 
									      a.v_jumlah, 
									      a.v_sisa, 
									      a.f_posting, 
									      a.f_giro_batal, 
									      a.i_area, tm_pelunasan.i_pelunasan 
									      from tm_giro a inner join tr_area on(a.i_area=tr_area.i_area)
									      inner join tr_customer on(a.i_customer=tr_customer.i_customer)
									      left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area)
									      left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area)
									      where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' 
									      or upper(a.i_customer) like '%$cari%' or a.v_jumlah=$cari) and 
									      ((tm_pelunasan.i_jenis_bayar!='02' and 
									      tm_pelunasan.i_jenis_bayar!='03' and 
									      tm_pelunasan.i_jenis_bayar!='04' and 
									      tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and										
									      (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
									      a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
          							and a.f_giro_tolak='f' 
									      order by a.d_giro desc, a.i_giro desc ",false);
        }else{
		      $query = $this->db->query(" select tr_area.e_area_name, 
									      a.i_giro, 
									      a.d_giro, 
									      a.i_rv, 
									      a.d_rv, 
									      tm_dt.i_dt, 
									      tm_dt.d_dt, 
									      tr_customer.i_customer, 
									      tr_customer.e_customer_name, 
									      a.e_giro_bank, 
									      a.v_jumlah, 
									      a.v_sisa, 
									      a.f_posting, 
									      a.f_giro_batal, 
									      a.i_area, tm_pelunasan.i_pelunasan 
									      from tm_giro a inner join tr_area on(a.i_area=tr_area.i_area)
									      inner join tr_customer on(a.i_customer=tr_customer.i_customer)
									      left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area)
									      left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area)
									      where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' 
									      or upper(a.i_customer) like '%$cari%' or a.v_jumlah=$cari) and 
									      ((tm_pelunasan.i_jenis_bayar!='02' and 
									      tm_pelunasan.i_jenis_bayar!='03' and 
									      tm_pelunasan.i_jenis_bayar!='04' and 
									      tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and
                        a.i_area='$iarea' and
									      (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
									      a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
          							and a.f_giro_tolak='f' 
									      order by a.d_giro desc, a.i_giro desc ",false);
        }
      }else{
        if($iarea=='NA'){
			    $query = $this->db->query(" select tr_area.e_area_name, 
										    a.i_giro, 
										    a.d_giro, 
										    a.i_rv, 
										    a.d_rv, 
										    tm_dt.i_dt, 
										    tm_dt.d_dt, 
										    tr_customer.i_customer, 
										    tr_customer.e_customer_name, 
										    a.e_giro_bank, 
										    a.v_jumlah, 
										    a.v_sisa, 
										    a.f_posting, 
										    a.f_giro_batal, 
										    a.i_area, tm_pelunasan.i_pelunasan 
										    from tm_giro a inner join tr_area on(a.i_area=tr_area.i_area)
										    inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										    left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area)
										    left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area)
										    where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' 
										    or upper(a.i_customer) like '%$cari%') and 
										    ((tm_pelunasan.i_jenis_bayar!='02' and 
										    tm_pelunasan.i_jenis_bayar!='03' and 
										    tm_pelunasan.i_jenis_bayar!='04' and 
										    tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and										
										    (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
										    a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
          							and a.f_giro_tolak='f' 
										    order by a.d_giro desc, a.i_giro desc ",false);			
        }else{
			    $query = $this->db->query(" select tr_area.e_area_name, 
										    a.i_giro, 
										    a.d_giro, 
										    a.i_rv, 
										    a.d_rv, 
										    tm_dt.i_dt, 
										    tm_dt.d_dt, 
										    tr_customer.i_customer, 
										    tr_customer.e_customer_name, 
										    a.e_giro_bank, 
										    a.v_jumlah, 
										    a.v_sisa, 
										    a.f_posting, 
										    a.f_giro_batal, 
										    a.i_area, tm_pelunasan.i_pelunasan 
										
										    from tm_giro a inner join tr_area on(a.i_area=tr_area.i_area)
										    inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										    left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area)
										    left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area)
										
										    where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' 
										    or upper(a.i_customer) like '%$cari%') and 
										    ((tm_pelunasan.i_jenis_bayar!='02' and 
										    tm_pelunasan.i_jenis_bayar!='03' and 
										    tm_pelunasan.i_jenis_bayar!='04' and 
										    tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and										
										    a.i_area='$iarea' and
										    (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
										    a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
          							and a.f_giro_tolak='f' 
										    order by a.d_giro desc, a.i_giro desc ",false);			
        }
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_giro_bcair');
			$this->load->model('girocair/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->cariperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('girocair/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu202')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/girocair/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows()+1; 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('girocair/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('girocair/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu202')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/girocair/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows()+1;
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('girocair/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('girocair/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu202')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('master_giro')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5)
			  ){
				$igiro = $this->uri->segment(4);
				$igiro = str_replace('%20',' ',$igiro);
				$iarea = $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$ipl 	= $this->uri->segment(8);
				$idt 	= $this->uri->segment(9);
				$data['igiro'] = $igiro;
				$data['iarea'] = $iarea;
				$data['dfrom']   = $dfrom;
				$data['dto']	   = $dto;
				$this->load->model('girocair/mmaster');
				if($ipl!=0 && $idt!=0) {
					$data['detail']=$this->mmaster->bacadetailpl($iarea,$ipl,$idt);
				} else {
					$data['detail']='';	
				}
				$data['isi']=$this->mmaster->bacagiro($igiro,$iarea);
		 		$this->load->view('girocair/vformupdate',$data);
			}else{
				$this->load->view('girocair/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}	

	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu202')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$igiro 			= $this->input->post('igiro', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			
			$icustomer		= $this->input->post('icustomer', TRUE);
			$dgiro			= $this->input->post('dgiro', TRUE);
			
			$dgirocair	= $this->input->post('dgirocair', TRUE);
			if($dgirocair!=''){
				$fgirocair		= 't';
				$tmp=explode("-",$dgirocair);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgirocair=$th."-".$bl."-".$hr;
			}else{
				$fgirocair	= 'f';
				$dgirocair	= null;
			}

			if (
				(isset($igiro) && $igiro != '') &&
				(isset($iarea) && $iarea != '') &&
				(isset($icustomer) && $icustomer != '') &&
				(isset($dgiro) && $dgiro != '') &&
				(isset($dgirocair) && $dgirocair != '')
			   )
			{
				$this->db->trans_begin();
				$this->load->model('girocair/mmaster');
				
				$this->mmaster->update($igiro,$iarea,$dgirocair,$fgirocair);

				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Update Giro Cair '.$igiro.' Pelanggan:'.$icustomer;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );  

					$data['sukses']			= true;
					$data['inomor']			= $igiro;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}	
}
?>
