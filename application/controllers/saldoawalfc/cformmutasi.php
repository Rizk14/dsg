<?php
/*
v = variables
*/
class Cformmutasi extends  CI_Controller {


	function Cformmutasi() {
		parent::__construct();	
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			$data['page_intitle_stokopname']		= $this->lang->line('page_intitle_stokopname');
			$data['page_title_stokopnamemutasi']	= $this->lang->line('page_title_stokopnamemutasi');
			$data['form_cari_tgl_stokopname']		= $this->lang->line('form_cari_tgl_stokopname');
			$data['form_tgl_stokopname']			= $this->lang->line('form_tgl_stokopname');
			$data['form_title_detail_stokopname']	= $this->lang->line('form_title_detail_stokopname');
			$data['form_kode_product_opname']		= $this->lang->line('form_kode_product_opname');
			$data['form_nm_product_opname']			= $this->lang->line('form_nm_product_opname');
			$data['form_jml_product_opname']		= $this->lang->line('form_jml_product_opname');
			$data['form_jml_fisik_product_opname']	= $this->lang->line('form_jml_fisik_product_opname');
			$data['button_cari']	= $this->lang->line('button_cari');
			$data['button_mutasi']	= $this->lang->line('button_mutasi');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']			= "";
			$data['list']			= "";
			$data['limages']		= base_url();

			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgOpname']	= $tgl."/".$bln."/".$thn;
			$data['isi']	= 'stokopname/vmainformmutasi';	
			$this->load->view('template',$data);
		
		}
	
	
	function simpan() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iteration	= $this->input->post('jml')?$this->input->post('jml'):$this->input->get_post('jml');
		$iterasi	= $this->input->post('iterasi')?$this->input->post('iterasi'):$this->input->get_post('iterasi');
		//$iteration	= $iteration;
		$d_date_so_first	= $this->input->post('d_so')?$this->input->post('d_so'):$this->input->get_post('d_so');
		$exp_so_lama	= explode("/",$d_date_so_first,strlen($d_date_so_first)); // dd/mm/YYYY
		$d_so_lama		= $exp_so_lama[2]."-".$exp_so_lama[1]."-".$exp_so_lama[0];
		
		$d_date_so		= $this->input->post('d_so_now')?$this->input->post('d_so_now'):$this->input->get_post('d_so_now');
		$exp_so_baru	= explode("/",$d_date_so,strlen($d_date_so));
		$d_so_baru		= $exp_so_baru[2]."-".$exp_so_baru[1]."-".$exp_so_baru[0];
		
		$stopproduksi	= $this->input->post('stopproduksi')?$this->input->post('stopproduksi'):$this->input->get_post('stopproduksi');
		$i_product 			= array();
		$e_product_name 	= array();	
		$n_quantity_akhir 	= array();
		$n_quantity_fisik	= array();
		$stok_fisik	= array();
		$i_color	= array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem_'.'0');

		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_product[$cacah] = $this->input->post('i_product_'.'tblItem_'.$cacah)?$this->input->post('i_product_'.'tblItem_'.$cacah):$this->input->get_post('i_product_'.'tblItem_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_'.'tblItem_'.$cacah)?$this->input->post('e_product_name_'.'tblItem_'.$cacah):$this->input->get_post('e_product_name_'.'tblItem_'.$cacah);
			$n_quantity_akhir[$cacah] = $this->input->post('n_quantity_akhir_'.'tblItem_'.$cacah)?$this->input->post('n_quantity_akhir_'.'tblItem_'.$cacah):$this->input->get_post('n_quantity_akhir_'.'tblItem_'.$cacah);
			$n_quantity_fisik[$cacah] = $this->input->post('n_quantity_fisik_'.'tblItem_'.$cacah)?$this->input->post('n_quantity_fisik_'.'tblItem_'.$cacah):$this->input->get_post('n_quantity_fisik_'.'tblItem_'.$cacah);
			$stok_fisik[$cacah] = $this->input->post('stok_fisik_'.$cacah);
			$i_color[$cacah] = $this->input->post('i_color_'.$cacah);
		}
		
		$this->load->model('stokopname/mclass');
		
		/* Cek:Apakah telah dilakukan Stokopname ?? */
		$bl_skrng	= date("m");
		$th_skrng	= date("Y");
		
		$qcekstokopname	= $this->mclass->ckstokopname($stopproduksi);
		if($qcekstokopname->num_rows()>0) {
			$row_cekstokopname	= $qcekstokopname->row();
			$tstokopname	= $row_cekstokopname->d_so; // YYYY-mm-dd
			$e_tstokopname	= explode("-",$tstokopname,strlen($tstokopname));
			$bl_stokopname	= $e_tstokopname[1];
			$th_stokopname	= $e_tstokopname[0];
		} else {
			$bl_stokopname	= 0;
			$th_stokopname	= 0;		
		}
		/* End 0f Cek Stokopname */
		
		if(!empty($i_product_0)) {
			//if( ($bl_skrng!=$bl_stokopname) && ($bl_skrng!=$bl_stokopname || $th_skrng==$th_stokopname) ) {

				$this->mclass->mutasistockopname($d_so_baru,$d_so_lama,$i_product,$e_product_name,$n_quantity_akhir,$n_quantity_fisik,$iteration,$iterasi,$stopproduksi, $stok_fisik, $i_color);

			//} else {
			//	print "<script>alert('Maaf, tdk dpt melakukan 2 kali Stokopname dibln yg sama!');show(\"stokopname/cformmutasi\",\"#content\");</script>";
			//}
		} else {
			print "<script>alert(\"Maaf, tdk ada Item Barang yg akan di stokopname!\");
			window.open(\"index\", \"_self\");</script>";
			
		}
	}
	
	function lopnamebrgjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$db2=$this->load->database('db_external', TRUE);
		$d_date	= $this->input->post('d_date')?$this->input->post('d_date'):$this->input->get_post('d_date');		
		$s = $this->input->post('s')?$this->input->post('s'):$this->input->get_post('s');

        	/* dd/mm/YYYY -> YYYY-mm-dd */
		$d_exp	= explode("/",$d_date,strlen($d_date));
		$d_date_n	= $d_exp[2]."-".$d_exp[1]."-".$d_exp[0];
		
        	$stopproduksi = ($s==1 || $s=='1')?'TRUE':'FALSE';

		$this->load->model('stokopname/mclass');
		
		$query = $this->mclass->lso($d_date_n,$stopproduksi);
		$fields	= $query->field_data();
		$num	= $query->num_rows();
		$item	= "";
		$iteration	= 0;
		
		if($num>0) {
			$item="<table>
					<tr>
					 <td width='2%' class='tdatahead'>NO</td>
					 <td width='12%' class='tdatahead'>KODE BARANG</td>
					 <td width='25%' class='tdatahead'>NAMA BARANG</td>
					 <td width='10%' class='tdatahead'>SALDO</td>
					 <td width='10%' class='tdatahead'>SO <span style='color:#FF0000'>*</span></td>
					 <td width='13%' class='tdatahead'>SO<br>Per Warna <span style='color:#FF0000'>*</span></td>
					</tr>
					";
			
			/* foreach($fields as $field) { */
			foreach($query->result() as $field) {
				$no	= $iteration+1;
				$saldo	= (empty($field->akhir) || strlen($field->akhir)==0)?'0':$field->akhir;
				
				$detail_warna = array();
				// 10-10-2014, ambil data per warna dari tabel tm_stokopname_item_color
				$sqlxx = " SELECT a.*, c.e_color_name FROM tm_stokopname_item_color a
							INNER JOIN tr_color c ON a.i_color = c.i_color
							WHERE a.i_so_item = '$field->i_so_item' "; //echo $sqlxx."<br>";
				$queryxx	= $db2->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_jahit_warna
						/*$query3	= $db2->query(" SELECT b.stok, b.stok_bagus, b.stok_perbaikan
							FROM tm_stok_unit_jahit a, tm_stok_unit_jahit_warna b
							WHERE a.id = b.id_stok_unit_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit'
							AND b.kode_warna = '$rowxx->kode' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						} */
						// ===================================================
						
						$detail_warna[] = array(
									'i_so_item_color'=>$rowxx->i_so_item_color,
									'i_color'=> $rowxx->i_color,
									'e_color_name'=> $rowxx->e_color_name,
									'n_quantity_akhir'=> $rowxx->n_quantity_akhir
								);
					}
				}
				else {
					//$detail_warna = '';
					
					// 19-12-2015
					$sqlxx = " SELECT a.*, c.e_color_name FROM tr_product_color a
							INNER JOIN tr_color c ON a.i_color = c.i_color
							WHERE a.i_product_motif = '$field->iproduct' ";
					$queryxx	= $db2->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
							
						foreach ($hasilxx as $rowxx) {							
							$detail_warna[] = array(
										'i_so_item_color'=>0,
										'i_color'=> $rowxx->i_color,
										'e_color_name'=> $rowxx->e_color_name,
										'n_quantity_akhir'=> 0
									);
						}
					}
					else
						$detail_warna = '';
				}
				// ====================================================================
				
				$item .= "
				<tr>
					<td width='2%'>".($iteration+1)."</td>
					<td width='12%'>
						<div id=\"i_product_tblItem_".$no."\" style=\"width:120px;\"><input type=\"text\" ID=\"i_product_tblItem_".$iteration."\"  name=\"i_product_tblItem_".$iteration."\" style=\"width:110px; font-weight:bold;\" value=\"".$field->iproduct."\" readonly></div>
					</td>
					<td width='25%'>
						<div id=\"e_product_name_tblItem_".$iteration."\" style=\"width:275px;\"><input type=\"text\" ID=\"e_product_name_tblItem_".$iteration."\" name=\"e_product_name_tblItem_".$iteration."\" style=\"width:265px;\" value=\"".strtoupper($field->motifname)."\" readonly></div>
					</td>
					<td width='10%'>
						<div id=\"n_quantity_akhir_tblItem_".$iteration."\" name=\"n_quantity_akhir_tblItem_".$iteration."\" style=\"width:95px;\"><input type=\"text\" ID=\"n_quantity_akhir_tblItem_".$iteration."\" name=\"n_quantity_akhir_tblItem_".$iteration."\" style=\"width:90px;text-align:right;\" value=\"".$saldo."\" readonly></div>
					</td>
					<td width='10%'>
						<div id=\"n_quantity_fisik_tblItem_".$iteration."\" name=\"n_quantity_fisik_tblItem_".$iteration."\" style=\"width:95px;\"><input type=\"text\" ID=\"n_quantity_fisik_tblItem_".$iteration."\" name=\"n_quantity_fisik_tblItem_".$iteration."\" style=\"width:90px;text-align:right; font-weight:bold; color:#FF0000;\" value=\"".$saldo."\"><input type=\"hidden\" name=\"iterasi\" id=\"iterasi\" value=\"".$iteration."\"></div>
					</td>";
					
					$item .= "<td style='white-space:nowrap;' align='right' width='13%'>";
					if (is_array($detail_warna)) {
					//$detailwarna = $query[$j]['detail_warna'];
						for($zz=0;$zz<count($detail_warna);$zz++){
							$item.="<span style='white-space:nowrap;'>".$detail_warna[$zz]['e_color_name']."&nbsp;";
							$item.= "<input type='text' style=\"width:70px; text-align:right; font-weight:bold; color:#FF0000;\" name='stok_fisik_".$iteration."[]' value='".$detail_warna[$zz]['n_quantity_akhir']."'>
							<input type='hidden' name='i_color_".$iteration."[]' value='".$detail_warna[$zz]['i_color']."' ><br>";
						}
						$item.="<hr>";
					}
					else
						$item.="&nbsp;<hr>";
						
					$item .="</td>";
					
					$item.="<td>
						&nbsp;
					</td>					
				</tr>
				";
				$iteration+=1;
				if($iteration > $num) {
					$item	.= "<input type=\"hidden\" name=\"jml\" id=\"jml\" value=\"".$num."\">";
				}
			}
			$item.="<tr>
					  <td colspan='6'><div style='border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:245px; padding:5px;'>Saldo akhir dari perhitungan fisik (Saldo awal berikutnya)</div></td>
					</tr>
				</table>";
			$item	.= "<input type=\"hidden\" name=\"d_date_so_first\" id=\"d_date_so_first\" value=\"".$d_date_n."\">";
			echo $item;
			if($s!='1'){
				//print "<script>alert(\"BARANG yg STOP PRODUKSI dulu ya... \");</script>";
			}
		} else {
			$qtso	= $this->mclass->tso($stopproduksi);
			if($qtso->num_rows()>0) {
				$row_tso = $qtso->row();
				$sh_tso	= $row_tso->d_so; 
				$e_sh_tso = explode("-",$sh_tso,strlen($sh_tso)); // Y-m-d
				$n_sh_tso = $e_sh_tso[2]."/".$e_sh_tso[1]."/".$e_sh_tso[0]; 
				
			} else {
				$n_sh_tso = "";
			}
			print "<script>alert(\"Maaf,tgl Stokopname sebelumnya adalah: $n_sh_tso\");
			window.open(\"cformmutasi/index\", \"_self\");</script>";
		}
	}
	
	// 17-03-2015, backdoor skrip utk insert warna2 brg yg blm ada stok warnanya. ini SO feb 2015, id 258
	function backdoor_so_warna() {
		
		$query2	= $db2->query(" select i_so_item, i_product FROM tm_stokopname_item where i_so='258' 
						AND i_so_item not in (select i_so_item FROM tm_stokopname_item_color) order by i_product ");
		if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
			foreach ($hasil2 as $row2) {
				$queryxx = $db2->query(" select i_color FROM tr_product_color WHERE i_product_motif = '$row2->i_product' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$so_warna	= array(
							 'i_so_item'=>$row2->i_so_item,
							 'i_color'=>$rowxx->i_color,
							 'n_quantity_awal'=>0,
							 'n_quantity_akhir'=>0,
							 'e_note'=> 'manual'
						);
						$db2->insert('tm_stokopname_item_color',$so_warna);
					} // end for2
				} // end if2
			} // end for
		} // end if
		
		echo "sukses nambahin warna di stokopname_item_color";
		return true;
	} // end function
	
	// 18-06-2015, skrip utk update qty di tm_stokopname_item berdasarkan tm_stokopname_item_color
	function updateqtyitembycolor() {
		$sql2 = " select i_so_item FROM tm_stokopname_item WHERE i_so = '266' OR i_so = '267' ";
		//$sql2 = " SELECT i_so_item, n_quantity_awal, n_quantity_akhir FROM tm_stokopname_item_color WHERE i_so_item in (select i_so_item FROM tm_stokopname_item WHERE i_so = '266'
		//		OR i_so = '267') ";
		$query2	= $db2->query($sql2);
		if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
			foreach ($hasil2 as $row2) {
//echo " SELECT SUM(n_quantity_awal) as awal, SUM(n_quantity_akhir) as akhir 
//							FROM tm_stokopname_item_color WHERE i_so_item = '$row2->i_so_item' <br>";
				$queryxx	= $db2->query(" SELECT SUM(n_quantity_awal) as awal, SUM(n_quantity_akhir) as akhir 
							FROM tm_stokopname_item_color WHERE i_so_item = '$row2->i_so_item' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$awal	= $hasilxx->awal;
					$akhir	= $hasilxx->akhir;

					if ($awal != '' && $akhir != '')
						$db2->query(" UPDATE tm_stokopname_item SET n_quantity_awal = '$awal', n_quantity_akhir = '$akhir'	
								WHERE i_so_item = '$row2->i_so_item' ");
				}
			}
		}
		echo "berhasil";
		
	}
}
?>
