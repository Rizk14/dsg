<?php
/*
v = variables
*/
class Cformbaru extends CI_Controller {

	function Cformbaru() {
		parent::__construct();	
	}	
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			$data['page_title_saldoawalfc']		= $this->lang->line('page_title_saldoawalfc');
			$data['page_intitle_saldoawalfc']	= $this->lang->line('page_intitle_saldoawalfc');
			$data['form_tanggal_saldoawalfc']	= $this->lang->line('form_tanggal_saldoawalfc');
			$data['form_title_detail_saldoawalfc']	= $this->lang->line('form_title_detail_saldoawalfc');
			$data['form_kode_product_opname']	= $this->lang->line('form_kode_product_opname');
			$data['form_nm_product_opname']		= $this->lang->line('form_nm_product_opname');
			$data['form_jml_product_opname']	= $this->lang->line('form_jml_product_opname');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();

			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgOpname']	= $tgl."/".$bln."/".$thn;
			$data['isi']	= 'saldoawalfc/vmainformbaru';	
			$this->load->view('template',$data);
			
		}
	

	function simpanx() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iteration= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$iterasi	= $iteration;
		$tmp	    = $this->input->post('d_so')?$this->input->post('d_so'):$this->input->get_post('d_so');
		$xx=explode("/",$tmp);
		$th=$xx[2];
		$bl=$xx[1];
		
		$periode	=$th.$bl; 

		$i_product= array();
		$e_product_name	= array();
		$n_quantity_warna	= array();
    $i_product_color = array();
    $i_color = array();
		
		$i_product_0	= $this->input->post('i_product_'.'tblItem'.'_'.'0');

		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_tblItem_'.$cacah)?$this->input->post('i_product_tblItem_'.$cacah):$this->input->get_post('i_product_tblItem_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_tblItem_'.$cacah)?$this->input->post('e_product_name_tblItem_'.$cacah):$this->input->get_post('e_product_name_tblItem_'.$cacah);
			
			// 25-03-2015, diganti jadi per warna
			//$n_quantity_akhir[$cacah] = $this->input->post('n_quantity_akhir_tblItem_'.$cacah)?$this->input->post('n_quantity_akhir_tblItem_'.$cacah):$this->input->get_post('n_quantity_akhir_tblItem_'.$cacah);
			$n_quantity_warna[$cacah]	= $this->input->post('qty_warna_'.$cacah);
			$i_product_color[$cacah]	= $this->input->post('i_product_color_'.$cacah);
			$i_color[$cacah]	= $this->input->post('i_color_'.$cacah);

			
			
		}



		if(!empty($periode)) {
			if(!empty($i_product_0)) {
 				$this->load->model('saldoawalfc/mclass');
				$this->mclass->msimpanbaru($periode, $i_product, $e_product_name,	$n_quantity_warna, $i_product_color, $i_color,$iteration);
			} else {
				print "<script>alert(\"Maaf, item barang hrs terisi. Terimakasih .\");
				window.open(\"index\", \"_self\");</script>";
			}
		} else {
			$data['page_title_saldoawalfc']	= $this->lang->line('page_title_saldoawalfc');
			$data['page_intitle_saldoawalfc']	= $this->lang->line('page_intitle_saldoawalfc');
			$data['form_tanggal_saldoawalfc']	= $this->lang->line('form_tanggal_saldoawalfc');
			$data['form_title_detail_saldoawalfc']	= $this->lang->line('form_title_detail_saldoawalfc');
			$data['form_kode_product_opname']	= $this->lang->line('form_kode_product_opname');
			$data['form_nm_product_opname']	= $this->lang->line('form_nm_product_opname');
			$data['form_jml_product_opname']	= $this->lang->line('form_jml_product_opname');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();	

			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
		
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgOpname']	= $tgl."/".$bln."/".$thn;
					
			print "<script>alert(\"Maaf, Input saldoawalfc gagal disimpan. Terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
			
			$data['isi']	= 'saldoawalfc/vmainformbaru';	
			$this->load->view('template',$data);		
		}
				
	}
	
	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title']	= 'BARANG JADI';
		$iterasi	= $this->uri->segment(4);
		$data['iterasi']	= $iterasi;		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('saldoawalfc/mclass');

		$query	= $this->mclass->listbrgjadi();
		
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = base_url().'index.php/saldoawalfc/cformbaru/listbarangjadinext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->listbrgjadiperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('saldoawalfc/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$data['page_title']	= 'BARANG JADI';
		$iterasi	= $this->uri->segment(4);
		$data['iterasi']	= $iterasi;
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('saldoawalfc/mclass');

		$query	= $this->mclass->listbrgjadi();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = base_url().'index.php/saldoawalfc/cformbaru/listbarangjadinext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->listbrgjadiperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('saldoawalfc/vlistformbrgjadi',$data);
	}
	
	function flistbarangjadi(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$key	= strtoupper($this->input->post('key'));
		//$iterasi	= $this->uri->segment(4,0);
		
		//$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI";
		$data['lurl']		= base_url();

		$this->load->model('saldoawalfc/mclass');
#echo $key;die;
		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		
		if($jml>0) {
		
			$cc	= 1; 
			foreach($query->result() as $row) {
			
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->f_stop_produksi')\">".$row->imotif."</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->f_stop_produksi')\">".$row->motifname."</a></td>
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->f_stop_produksi')\">".$row->qty."</a></td>
				 </tr>";

				 $cc+=1;
			}
		} else {
			$list	= "";
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;		
	}
	
	// 25-03-2015
	function additemwarna(){
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$db2=$this->load->database('db_external', TRUE);
		$iproductmotif 	= $this->input->post('iproductmotif', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
									WHERE a.i_color = b.i_color AND a.i_product_motif = '".$iproductmotif."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'i_product_color'=> $rowxx->i_product_color,
										'i_color'=> $rowxx->i_color,
										'e_color_name'=> $rowxx->e_color_name
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['iproductmotif'] = $iproductmotif;
		$data['posisi'] = $posisi;
		$this->load->view('saldoawalfc/vlistwarna', $data); 
		//print_r($detailwarna); die();
		//echo $iproductmotif; die();
		return true;
  }
}
?>
