<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expofpenjualanbhnbaku/mclass');
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
	
			$data['page_title_penjualanbhnbaku']	= $this->lang->line('page_title_penjualanbhnbaku');
			$data['form_title_detail_penjualanbhnbaku']	= $this->lang->line('form_title_detail_penjualanbhnbaku');
			$data['list_penjualanbhnbaku_kd_brg']	= $this->lang->line('list_penjualanbhnbaku_kd_brg');
			$data['list_penjualanbhnbaku_tgl_faktur_mulai']	= $this->lang->line('list_penjualanbhnbaku_tgl_faktur_mulai');
			$data['list_fpenjualanbhnbaku_no_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_no_faktur');
			$data['list_fpenjualanbhnbaku_no_pajak']		= $this->lang->line('list_fpenjualanbhnbaku_no_pajak');
			$data['list_fpenjualanbhnbaku_nm_pelanggan']	= $this->lang->line('list_fpenjualanbhnbaku_nm_pelanggan');
			$data['list_fpenjualanbhnbaku_npwp']			= $this->lang->line('list_fpenjualanbhnbaku_npwp');
			$data['list_fpenjualanbhnbaku_tgl_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_tgl_faktur');
			$data['list_fpenjualanbhnbaku_tgl_bts_kirim']	= $this->lang->line('list_fpenjualanbhnbaku_tgl_bts_kirim');
			$data['list_fpenjualanbhnbaku_discount']		= $this->lang->line('list_fpenjualanbhnbaku_discount');
			$data['list_fpenjualanbhnbaku_total_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_total_faktur');
			$data['list_fpenjualanbhnbaku_grand_total']		= $this->lang->line('list_fpenjualanbhnbaku_grand_total');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			
			$data['opt_jns_brg']	= $this->mclass->lklsbrg();
				$data['isi']	= 'expofpenjualanbhnbaku/vmainform';	
			$this->load->view('template',$data);
		
		}	
	

	function listbarangjadi() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/expofpenjualanbhnbaku/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
				
		$this->load->view('expofpenjualanbhnbaku/vlistformbrgjadi',$data);			
	}

	function listbarangjadinext() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url']		= base_url().'index.php/expofpenjualanbhnbaku/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
				
		$this->load->view('expofpenjualanbhnbaku/vlistformbrgjadi',$data);			
	}

	function flistbarangjadi() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "NOMOR FAKTUR";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->ifakturcode."</a></td>
				  <td width=\"80px;\"><a href=\"javascript:settextfield('$row->ifakturcode')\">".$row->dfaktur."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	
	function carilistpenjualanndo() {	
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanbhnbaku']	= $this->lang->line('page_title_penjualanbhnbaku');
		$data['form_title_detail_penjualanbhnbaku']	= $this->lang->line('form_title_detail_penjualanbhnbaku');
		$data['list_penjualanbhnbaku_kd_brg']	= $this->lang->line('list_penjualanbhnbaku_kd_brg');
		$data['list_penjualanbhnbaku_tgl_faktur_mulai']	= $this->lang->line('list_penjualanbhnbaku_tgl_faktur_mulai');
		$data['list_fpenjualanbhnbaku_no_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_no_faktur');
		$data['list_fpenjualanbhnbaku_no_pajak']		= $this->lang->line('list_fpenjualanbhnbaku_no_pajak');
		$data['list_fpenjualanbhnbaku_nm_pelanggan']	= $this->lang->line('list_fpenjualanbhnbaku_nm_pelanggan');
		$data['list_fpenjualanbhnbaku_npwp']			= $this->lang->line('list_fpenjualanbhnbaku_npwp');
		$data['list_fpenjualanbhnbaku_tgl_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_tgl_faktur');
		$data['list_fpenjualanbhnbaku_tgl_bts_kirim']	= $this->lang->line('list_fpenjualanbhnbaku_tgl_bts_kirim');
		$data['list_fpenjualanbhnbaku_discount']		= $this->lang->line('list_fpenjualanbhnbaku_discount');
		$data['list_fpenjualanbhnbaku_total_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_total_faktur');
		$data['list_fpenjualanbhnbaku_grand_total']		= $this->lang->line('list_fpenjualanbhnbaku_grand_total');
				
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();

		$nofaktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->uri->segment(4);
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->uri->segment(5);
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->uri->segment(6);
		
		$data['tgldomulai']	= empty($d_do_first)?"":$d_do_first;
		$data['tgldoakhir']	= empty($d_do_last)?"":$d_do_last;
		$data['nofaktur']	= empty($nofaktur)?"":$nofaktur;

		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:date("Y-m-d");
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:date("Y-m-d");
		
		$nofak	= empty($nofaktur)?'0':$nofaktur;
		$dfirst	= empty($d_do_first)?'0':$n_d_do_first;
		$dlast	= empty($d_do_last)?'0':$n_d_do_last;
		
		$data['tfirst']	= $dfirst;
		$data['tlast']	= $dlast;
		$data['nofak']	= $nofak;
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();

		$query	= $this->mclass->clistpenjualanndo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expofpenjualanbhnbaku/cform/carilistpenjualanndoperpages/'.$nofak.'/'.$dfirst.'/'.$dlast.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
				
		$data['query']	= $this->mclass->clistpenjualanndoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'expofpenjualanbhnbaku/vlistform';	
			$this->load->view('template',$data);
		
	}

	function carilistpenjualanndoperpages() {	
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanbhnbaku']	= $this->lang->line('page_title_penjualanbhnbaku');
		$data['form_title_detail_penjualanbhnbaku']	= $this->lang->line('form_title_detail_penjualanbhnbaku');
		$data['list_penjualanbhnbaku_kd_brg']	= $this->lang->line('list_penjualanbhnbaku_kd_brg');
		$data['list_penjualanbhnbaku_tgl_faktur_mulai']	= $this->lang->line('list_penjualanbhnbaku_tgl_faktur_mulai');
		$data['list_fpenjualanbhnbaku_no_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_no_faktur');
		$data['list_fpenjualanbhnbaku_no_pajak']		= $this->lang->line('list_fpenjualanbhnbaku_no_pajak');
		$data['list_fpenjualanbhnbaku_nm_pelanggan']	= $this->lang->line('list_fpenjualanbhnbaku_nm_pelanggan');
		$data['list_fpenjualanbhnbaku_npwp']			= $this->lang->line('list_fpenjualanbhnbaku_npwp');
		$data['list_fpenjualanbhnbaku_tgl_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_tgl_faktur');
		$data['list_fpenjualanbhnbaku_tgl_bts_kirim']	= $this->lang->line('list_fpenjualanbhnbaku_tgl_bts_kirim');
		$data['list_fpenjualanbhnbaku_discount']		= $this->lang->line('list_fpenjualanbhnbaku_discount');
		$data['list_fpenjualanbhnbaku_total_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_total_faktur');
		$data['list_fpenjualanbhnbaku_grand_total']		= $this->lang->line('list_fpenjualanbhnbaku_grand_total');
				
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['ljnsbrg']	= "";
		$data['lpenjndo']	= "";			
		$data['limages']	= base_url();

		$no_faktur	= $this->input->post('no_faktur')?$this->input->post('no_faktur'):$this->uri->segment(4); // Y-m-d
		$d_do_first	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->uri->segment(5);
		$d_do_last	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->uri->segment(6);

		$e_d_do_first	= explode("-",$d_do_first,strlen($d_do_first)); // Y-m-d
		$e_d_do_last	= explode("-",$d_do_last,strlen($d_do_last));
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:date("d/m/Y"); // d/m/Y
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:date("d/m/Y");
				
		$data['tgldomulai']	= empty($n_d_do_first)?"":$n_d_do_first;
		$data['tgldoakhir']	= empty($n_d_do_last)?"":$n_d_do_first;
		$data['nofaktur']	= empty($no_faktur) || $no_faktur=='0'?"":$no_faktur;

		$nofak	= empty($no_faktur) || $no_faktur=='0' ?'0':$no_faktur;
		$dfirst	= empty($d_do_first) || $d_do_first=='0' ?'0':$d_do_first;
		$dlast	= empty($d_do_last) || $d_do_last=='0' ?'0':$d_do_last;
		
		$data['tfirst']	= $dfirst;
		$data['tlast']	= $dlast;
		$data['nofak']	= $nofak;
		
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();

		$query	= $this->mclass->clistpenjualanndo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expofpenjualanbhnbaku/cform/carilistpenjualanndoperpages/'.$nofak.'/'.$dfirst.'/'.$dlast.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
				
		$data['query']	= $this->mclass->clistpenjualanndoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);
		$data['isi']='expofpenjualanbhnbaku/vlistform';
		$this->load->view('template',$data);	
	
	}	
	
	function gexportpenjualannondo() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$db2=$this->load->database('db_external', TRUE);
		$data['page_title_penjualanbhnbaku']	= $this->lang->line('page_title_penjualanbhnbaku');
		$data['form_title_detail_penjualanbhnbaku']	= $this->lang->line('form_title_detail_penjualanbhnbaku');
		$data['list_penjualanbhnbaku_kd_brg']	= $this->lang->line('list_penjualanbhnbaku_kd_brg');
		$data['list_penjualanbhnbaku_tgl_faktur_mulai']	= $this->lang->line('list_penjualanbhnbaku_tgl_faktur_mulai');
		$data['list_fpenjualanbhnbaku_no_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_no_faktur');
		$data['list_fpenjualanbhnbaku_no_pajak']		= $this->lang->line('list_fpenjualanbhnbaku_no_pajak');
		$data['list_fpenjualanbhnbaku_nm_pelanggan']	= $this->lang->line('list_fpenjualanbhnbaku_nm_pelanggan');
		$data['list_fpenjualanbhnbaku_npwp']			= $this->lang->line('list_fpenjualanbhnbaku_npwp');
		$data['list_fpenjualanbhnbaku_tgl_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_tgl_faktur');
		$data['list_fpenjualanbhnbaku_tgl_bts_kirim']	= $this->lang->line('list_fpenjualanbhnbaku_tgl_bts_kirim');
		$data['list_fpenjualanbhnbaku_discount']		= $this->lang->line('list_fpenjualanbhnbaku_discount');
		$data['list_fpenjualanbhnbaku_total_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_total_faktur');
		$data['list_fpenjualanbhnbaku_grand_total']		= $this->lang->line('list_fpenjualanbhnbaku_grand_total');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$nofaktur	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		
		$nofak	= (empty($nofaktur) || $nofaktur=='0')?'0':$nofaktur;
		$dfirst	= (empty($tdofirst) || $tdofirst=='0')?'0':$tdofirst;
		$dlast	= (empty($tdolast) || $tdolast=='0')?'0':$tdolast;
		
		$extdofirst	= $tdofirst!=0?explode("-",$tdofirst,strlen($tdofirst)):$tdofirst;
		$extdolast	= $tdolast!=0?explode("-",$tdolast,strlen($tdolast)):$tdolast;
		
		$nwdofirst	= $tdofirst!=0?$extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0]:$tdofirst;
		$nwdolast	=$tdolast!=0? $extdolast[2]."/".$extdolast[1]."/".$extdolast[0]:$tdolast;
		
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= empty($nwdofirst)?"":$nwdofirst;
		$data['tgldoakhir']	= empty($nwdolast)?"":$nwdolast;
		$data['nofaktur']	= $nofaktur=='0'?"":$nofaktur;
		
		$query	= $this->mclass->clistpenjualanndo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expofpenjualanbhnbaku/cform/gexportpenjualannondonext/'.$nofak.'/'.$dfirst.'/'.$dlast;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
				
		$data['query']	= $this->mclass->clistpenjualanndoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);		
		

		$ObjPHPExcel = new PHPExcel();
		
		$qexppenjualando	= $this->mclass->explistpenjualanndo($nofak,$dfirst,$dlast);
		
		if($qexppenjualando->num_rows()>0) {
		
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Faktur Penjualan")
				->setSubject("Laporan Faktur Penjualan")
				->setDescription("Laporan Faktur Penjualan")
				->setKeywords("Laporan Faktur Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => false,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true

				)
			),
			'A1:T1'
			);
			
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(9);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(9);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'No');
			$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'Kode Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Kode Transaksi');
			$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'Kode Status');
			$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'Kode Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Flat VAT');
			$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'NPWP/ Nomor Paspor');
			$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'Nama Lawan Transaksi');
			$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'Nomor Faktur/ Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('J1', 'Jenis Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('K1', 'Nomor Faktur Pengganti/ Retur');
			$ObjPHPExcel->getActiveSheet()->setCellValue('L1', 'Jenis Dokumen Pengganti/ Retur');
			$ObjPHPExcel->getActiveSheet()->setCellValue('M1', 'Tanggal Faktur/ Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('N1', 'Tanggal SSP');
			$ObjPHPExcel->getActiveSheet()->setCellValue('O1', 'Masa Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('P1', 'Tahun Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('Q1', 'Pembetulan');
			$ObjPHPExcel->getActiveSheet()->setCellValue('R1', 'DPP');
			$ObjPHPExcel->getActiveSheet()->setCellValue('S1', 'PPN');
			$ObjPHPExcel->getActiveSheet()->setCellValue('T1', 'PPnBM');
			
			if($qexppenjualando->num_rows()>0) {
				
				$j	= 2;
				$nomor	= 1;
				$jml=1;
				
				$bruto	= array();
				$dpp	= array();
				$ppn	= array();
				$total	= array();

				$nomorpajak	= array();
				$tglpajak	= array();
				$tahunpajak	= array();
				$masapajak	= array();
				$thpajak	= array();
				$tglfaktur	= array();
								
				foreach($qexppenjualando->result() as $row) {

					$qttfaktur	= $db2->query(" SELECT sum(a.n_quantity*a.v_unit_price) AS totalfaktur FROM tm_faktur_bhnbaku_item a 
								INNER JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur WHERE b.i_faktur_code='$row->ifakturcode' ");
					if($qttfaktur->num_rows()>0){
						
						$rttfaktur	= $qttfaktur->row();
						
						/* APAKAH DIBULATKAN ATAU TIDAK ?? */
						/*
						if($row->f_include_ppn=='t') {
							$bruto[$jml]	= $rttfaktur->totalfaktur;
							$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
							$ppn[$jml]		= round(($dpp[$jml]*10)/100);
							$total[$jml]	= round($dpp[$jml]+$ppn[$jml]);
						}else{
							$bruto[$jml]	= $rttfaktur->totalfaktur;
							$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
							$ppn[$jml]		= 0;
							$total[$jml]	= round($dpp[$jml]+$ppn[$jml]);
						}
						*/ 
						if($row->f_include_ppn=='t') {
							$bruto[$jml]	= round($rttfaktur->totalfaktur,2);
							$dpp[$jml]		= round(($bruto[$jml]-($row->nilaidiscon)),2);
							$ppn[$jml]		= round((($dpp[$jml]*10)/100),2);
							$total[$jml]	= round(($dpp[$jml]+$ppn[$jml]),2);
						}else{
							$bruto[$jml]	= round($rttfaktur->totalfaktur,2);
							$dpp[$jml]		= round(($bruto[$jml]-($row->nilaidiscon)),2);
							$ppn[$jml]		= 0;
							$total[$jml]	= round(($dpp[$jml]+$ppn[$jml]),2);
						}
												
					}
										
					$dfaktur	= explode("-",$row->dfaktur,strlen($row->dfaktur)); // Y-m-d
					$tfaktur	= $dfaktur[2];
					$bfaktur	= $dfaktur[1];
					$thfaktur	= $dfaktur[0];
					$tglfaktur	= $tfaktur."/".$bfaktur."/".$thfaktur;
						
					$dduedate	= explode("-",$row->dduedate,strlen($row->dduedate)); // Y-m-d
					$tduedate	= $dduedate[2];
					$bduedate	= $dduedate[1];
					$thduedate	= $dduedate[0];
					$tglduedate	= $tduedate."/".$bduedate."/".$thduedate;

					$dpajak	= explode("-",$row->dpajak,strlen($row->dpajak)); // Y-m-d
					$tpajak			= $dpajak[2];
					$bpajak			= $dpajak[1];
					$thpajak[$jml]	= $dpajak[0];
					$tglpajak[$jml]	= $tpajak."/".$bpajak."/".$thpajak[$jml];
					$tahunpajak[$jml]	= substr($thpajak[$jml],2,2);
					$masapajak[$jml]	= str_repeat($dpajak[1],2);
					
					// Nomor Pajak 
					$nopajak		= $this->mclass->pajak($row->ifakturcode);
					if($nopajak->num_rows() > 0) {
						$row_pajak	= $nopajak->row();			
						$nomorpajak	= $row_pajak->ifakturpajak;			
						
						switch(strlen($nomorpajak)) {
							case "1": $nfaktur	= '0000000'.$nomorpajak; break;
							case "2": $nfaktur	= '000000'.$nomorpajak; break;
							case "3": $nfaktur	= '00000'.$nomorpajak; break;
							case "4": $nfaktur	= '0000'.$nomorpajak; break;
							case "5": $nfaktur	= '000'.$nomorpajak; break;
							case "6": $nfaktur	= '00'.$nomorpajak; break;
							case "7": $nfaktur	= '0'.$nomorpajak; break;
							default: $nfaktur	= $nomorpajak;
						}
						$nomorpajak	= "010"."."."000".".".$tahunpajak[$jml].".".$nfaktur;		
						
					} else {
						$nomorpajak	= "";
					}
					// End 0f Nomer Pajak
					
					//$bruto[$jml]	= $row->totalfaktur;
					//$dpp[$jml]		= ($bruto[$jml]-($row->nilaidiscon));
					
					// Pembulantan ke atas
					//$ppn[$jml]		= ceil(($dpp[$jml]*10)/100);
					//$total[$jml]	= ceil($dpp[$jml]+$ppn[$jml]);

					$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),
						'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
		
						)
					),
					'A'.$j.':'.'T'.$j
					);
					
					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, "A");
					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, "2");
					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, "1");
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, "1");
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, "0");
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->npwp);
					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->customername);
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->ifakturcode);
					$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, "0");
					$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, "");
					$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$j, ""); 
					$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$j, $tglfaktur[$jml], Cell_DataType::TYPE_STRING); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('N'.$j, ""); 
					$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$j, $masapajak[$jml], Cell_DataType::TYPE_STRING); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('P'.$j, $thpajak[$jml]); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('Q'.$j, "0"); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('R'.$j, $dpp[$jml]); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('S'.$j, $ppn[$jml]); 
					$ObjPHPExcel->getActiveSheet()->setCellValue('T'.$j, "0"); 
										
					$jml++;													
					$j++;																																																							
					$nomor++;
				}
			}
				
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."laporan_fak_pajak_penj_bhnbaku_".$dfirst."-".$dlast.".xls";
			$ObjWriter->save("files/".$files);
																							
		} else {

			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Faktur Penjualan")
				->setSubject("Laporan Faktur Penjualan")
				->setDescription("Laporan Faktur Penjualan")
				->setKeywords("Laporan Faktur Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => false,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true

				)
			),
			'A1:T1'
			);
			
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(9);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(9);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'No');
			$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'Kode Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'Kode Transaksi');
			$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'Kode Status');
			$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'Kode Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'Flat VAT');
			$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'NPWP/ Nomor Paspor');
			$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'Nama Lawan Transaksi');
			$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'Nomor Faktur/ Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('J1', 'Jenis Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('K1', 'Nomor Faktur Pengganti/ Retur');
			$ObjPHPExcel->getActiveSheet()->setCellValue('L1', 'Jenis Dokumen Pengganti/ Retur');
			$ObjPHPExcel->getActiveSheet()->setCellValue('M1', 'Tanggal Faktur/ Dokumen');
			$ObjPHPExcel->getActiveSheet()->setCellValue('N1', 'Tanggal SSP');
			$ObjPHPExcel->getActiveSheet()->setCellValue('O1', 'Masa Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('P1', 'Tahun Pajak');
			$ObjPHPExcel->getActiveSheet()->setCellValue('Q1', 'Pembetulan');
			$ObjPHPExcel->getActiveSheet()->setCellValue('R1', 'DPP');
			$ObjPHPExcel->getActiveSheet()->setCellValue('S1', 'PPN');
			$ObjPHPExcel->getActiveSheet()->setCellValue('T1', 'PPnBM');
				
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$files	= $this->session->userdata('gid')."laporan_fak_pajak_penj_bhnbaku_".$dfirst."-".$dlast.".xls";
			$ObjWriter->save("files/".$files);
									
		}

		$efilename = @substr($files,1,strlen($files));
	
		$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
		$data['isi']='expofpenjualanbhnbaku/vexpform';
		$this->load->view('template',$data);		
		
	}
	
	function gexportpenjualannondonext() {
			$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_penjualanbhnbaku']	= $this->lang->line('page_title_penjualanbhnbaku');
		$data['form_title_detail_penjualanbhnbaku']	= $this->lang->line('form_title_detail_penjualanbhnbaku');
		$data['list_penjualanbhnbaku_kd_brg']	= $this->lang->line('list_penjualanbhnbaku_kd_brg');
		$data['list_penjualanbhnbaku_tgl_faktur_mulai']	= $this->lang->line('list_penjualanbhnbaku_tgl_faktur_mulai');
		$data['list_fpenjualanbhnbaku_no_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_no_faktur');
		$data['list_fpenjualanbhnbaku_no_pajak']		= $this->lang->line('list_fpenjualanbhnbaku_no_pajak');
		$data['list_fpenjualanbhnbaku_nm_pelanggan']	= $this->lang->line('list_fpenjualanbhnbaku_nm_pelanggan');
		$data['list_fpenjualanbhnbaku_npwp']			= $this->lang->line('list_fpenjualanbhnbaku_npwp');
		$data['list_fpenjualanbhnbaku_tgl_faktur']		= $this->lang->line('list_fpenjualanbhnbaku_tgl_faktur');
		$data['list_fpenjualanbhnbaku_tgl_bts_kirim']	= $this->lang->line('list_fpenjualanbhnbaku_tgl_bts_kirim');
		$data['list_fpenjualanbhnbaku_discount']		= $this->lang->line('list_fpenjualanbhnbaku_discount');
		$data['list_fpenjualanbhnbaku_total_faktur']	= $this->lang->line('list_fpenjualanbhnbaku_total_faktur');
		$data['list_fpenjualanbhnbaku_grand_total']		= $this->lang->line('list_fpenjualanbhnbaku_grand_total');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$nofaktur	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		
		$nofak	= (empty($nofaktur) || $nofaktur=='0')?'0':$nofaktur;
		$dfirst	= (empty($tdofirst) || $tdofirst=='0')?'0':$tdofirst;
		$dlast	= (empty($tdolast) || $tdolast=='0')?'0':$tdolast;
		
		$extdofirst	= $tdofirst!=0?explode("-",$tdofirst,strlen($tdofirst)):$tdofirst;
		$extdolast	= $tdolast!=0?explode("-",$tdolast,strlen($tdolast)):$tdolast;
		
		$nwdofirst	= $tdofirst!=0?$extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0]:$tdofirst;
		$nwdolast	=$tdolast!=0? $extdolast[2]."/".$extdolast[1]."/".$extdolast[0]:$tdolast;
		
		$periode	= $nwdofirst." s.d ".$nwdolast;

		$data['tgldomulai']	= empty($nwdofirst)?"":$nwdofirst;
		$data['tgldoakhir']	= empty($nwdolast)?"":$nwdolast;
		$data['nofaktur']	= $nofaktur=='0'?"":$nofaktur;
		
		$query	= $this->mclass->clistpenjualanndo($nofak,$dfirst,$dlast);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= 'expofpenjualanbhnbaku/cform/gexportpenjualannondonext/'.$nofak.'/'.$dfirst.'/'.$dlast.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->clistpenjualanndoperpages($nofak,$dfirst,$dlast,$pagination['per_page'],$pagination['cur_page']);
		$data['isi']='expofpenjualanbhnbaku/vexpform';
		$this->load->view('template',$data);	
	}	
}
?>
