<?php
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title'] = $this->lang->line('master_giro');
			$this->load->model('giro/mmaster');
			$data['igiro']='';
			$data['query']=$this->mmaster->bacasemua();
			$data['isi']='giro/vmainform';
			$this->load->view('template', $data);
		
	}
	function insert_fail()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title'] = $this->lang->line('master_giro');
			$this->load->view('giro/vinsert_fail',$data);
		
	}
	function edit()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title'] = $this->lang->line('master_giro')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5)
			  ){
				$igiro = $this->uri->segment(4);
        $igiro = str_replace('%20',' ',$igiro);
				$iarea = $this->uri->segment(5);
				$dfrom = $this->uri->segment(6);
				$dto 	 = $this->uri->segment(7);
				$ipl 	 = $this->uri->segment(8);
				$idt 	 = $this->uri->segment(9);
				$data['igiro'] = $igiro;
				$data['iarea'] = $iarea;
				$data['dfrom']   = $dfrom;
				$data['dto']	   = $dto;
				$this->load->model('giro/mmaster');
				if($ipl!=0 && $idt!=0) {
					$data['detail']=$this->mmaster->bacadetailpl($iarea,$ipl,$idt);
				} else {
					$data['detail']='';	
				}
				$data['isi']=$this->mmaster->baca($igiro,$iarea);
########
				$query  = $this->mmaster->baca($igiro,$iarea);
			  $dgiro=substr($query->d_giro,0,4).substr($query->d_giro,5,2);
				$data['bisaedit']=false;
				$query=$this->db->query("	select i_periode from tm_periode ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $rw){
            $periode=$rw->i_periode;
				  }
				  if($periode<=$dgiro)$data['bisaedit']=true;
			  }
########
		 		$this->load->view('giro/vformupdate',$data);
			}else{
				$this->load->view('giro/vinsert_fail',$data);
			}
		
	}
	function update()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$igiro 			= $this->input->post('igiro', TRUE);
			$idt        = $this->input->post('idt', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$dgiro			= $this->input->post('dgiro', TRUE);
			if($dgiro!=''){
				$tmp=explode("-",$dgiro);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiro=$th."-".$bl."-".$hr;
			}
			$dsetor			= $this->input->post('dsetor', TRUE);
			if($dsetor!=''){
				$tmp=explode("-",$dsetor);
				$rvth=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsetor=$rvth."-".$bl."-".$hr;
			}
			$dgiroduedate	= $this->input->post('dgiroduedate', TRUE);
			if($dgiroduedate!=''){
				$tmp=explode("-",$dgiroduedate);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiroduedate=$th."-".$bl."-".$hr;
			}
			$dgirocair	= $this->input->post('dgirocair', TRUE);
			if($dgirocair!=''){
				$fgirocair		= 't';
				$tmp=explode("-",$dgirocair);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgirocair=$th."-".$bl."-".$hr;
			}else{
				$fgirocair	= 'f';
				$dgirocair	= null;
			}
			$dgiroterima	= $this->input->post('dgiroterima', TRUE);
			if($dgiroterima!=''){
				$tmp=explode("-",$dgiroterima);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiroterima=$th."-".$bl."-".$hr;
			}else{
				$dgiroterima	= null;
			}
			$egirodescription= $this->input->post('egirodescription', TRUE);
			$egirobank		= $this->input->post('egirobank', TRUE);
			if($this->input->post('fgirobatal')!=''){
				$fgirobatal		= 't';
			}else{
				$fgirobatal		= 'f';
			}
			if($this->input->post('fgirotolak')!=''){
				$fgirotolak ='t';
				$dgirotolak	= $this->input->post('dgirotolak', TRUE);
				$tmp=explode("-",$dgirotolak);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgirotolak=$th."-".$bl."-".$hr;
			}else{
				$fgirotolak = 'f';
				$dgirotolak	= null;
			}
			$vjumlah		= $this->input->post('vjumlah', TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$vsisa			= $this->input->post('vsisa', TRUE);
			$fgirouse		= $this->input->post('fgirouse', TRUE);
			$vsisa			= str_replace(',','',$vsisa);
			if (
				(isset($igiro) && $igiro != '') &&
				(isset($iarea) && $iarea != '') &&
				(isset($icustomer) && $icustomer != '') &&
				(isset($dgiro) && $dgiro != '') &&
				(isset($dsetor) && $dsetor != '') &&
				(isset($dgiroduedate) && $dgiroduedate != '') &&
				(isset($vjumlah) && $vjumlah != '')
			   )
			{
				$this->db->trans_begin();
				$this->load->model('giro/mmaster');
				$irv = $this->mmaster->runningnumberrv($rvth);
				$this->mmaster->update(	$igiro,$iarea,$icustomer,$irv,$dgiro,$dsetor,$dgiroduedate,$dgirocair,$egirodescription,$egirobank,
								       					$fgirotolak,$fgirocair,$vjumlah,$vsisa,$dgirotolak,$fgirobatal,$idt,$dgiroterima);
################
				if($fgirouse=='t'){
					$this->mmaster->updatepelunasan($igiro,$iarea,$fgirotolak,$vjumlah,$vsisa,$fgirotolak,$fgirobatal);
				}
################
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Update Giro No:'.$igiro.' Pelanggan:'.$icustomer;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );  

					$data['sukses']			= true;
					$data['inomor']			= $igiro;
					$this->load->view('nomor',$data);
				}
			}
		
	}
	function delete()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$igiro		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$this->load->model('giro/mmaster');
			$this->mmaster->delete($igiro,$iarea);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
		  }
		  $pesan='Hapus Giro No:'.$igiro.' Area:'.$iarea;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );  

			$data['page_title'] = $this->lang->line('master_giro');
			$data['isi']=$this->mmaster->bacasemua();
			$data['igiro']='';
			$this->load->view('giro/vmainform', $data);
		
	}
	function page()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$arr = array('vformlist','vform');
		$page=$this->uri->segment(4);
		if(!in_array($page,$arr)){ return false; }
		$config['base_url'] = base_url().'index.php/giro/cform/paging/';
		$cari 				= $this->input->post('cari', TRUE);
		$cari				= strtoupper($cari);
		$query				= $this->db->query("select * from tm_giro
												where upper(i_giro) like '%$cari%' 
												or upper(i_customer) like '%$cari%'",false);
		$config['total_rows'] = $query->num_rows();
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(4);
		$this->pagination->initialize($config);

		$data['page_title'] = $this->lang->line('master_giro');
		$data['igiro']='';
		$this->load->model('giro/mmaster');
		$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
		$this->load->view('giro/'.$page, $data);
	}
	function area()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		
			$config['base_url'] = base_url().'index.php/giro/cform/area/index/';
      //~ $iuser   = $this->session->userdata('user_id');
      //~ $query = $this->db->query(" select * from tm_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
		$query = $this->db->query(" select * from tm_area order by kode_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('giro/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('giro/vlistarea', $data);
		
	}
	function cariarea()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$config['base_url'] = base_url().'index.php/giro/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                                    where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                    and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('giro/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('giro/vlistarea', $data);
		
	}
	function customer()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}		$db2=$this->load->database('db_external',TRUE);
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/giro/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			//~ $query = $this->db->query("select * from tr_customer
									   //~ where i_area = '$iarea'",false);
			$query = $db2->query("select * from tr_customer",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('giro/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('giro/vlistcustomer', $data);
		
	}
	function caricustomer()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$config['base_url'] = base_url().'index.php/giro/cform/customer/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$iarea = $this->uri->segment(4);
			$query = $this->db->query("select * from tr_customer
						   where i_area='$iarea' 
							 and (upper(i_customer) like '%$cari%' 
						      	  or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('giro/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('giro/vlistcustomer', $data);
		
	}
	function simpan()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$igiro 			= $this->input->post('igiro', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			//~ $idt   			= $this->input->post('idt', TRUE);
			$idt			="";
			$icustomer	= $this->input->post('icustomer', TRUE);
			$dgiro			= $this->input->post('dgiro', TRUE);
			if($dgiro!=''){
				$tmp=explode("-",$dgiro);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiro=$th."-".$bl."-".$hr;
			}
			$dsetor			= $this->input->post('dsetor', TRUE);
			if($dsetor!=''){
				$tmp=explode("-",$dsetor);
				$rvth=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsetor=$rvth."-".$bl."-".$hr;
			}
			$dgiroduedate	= $this->input->post('dgiroduedate', TRUE);
			if($dgiroduedate!=''){
				$tmp=explode("-",$dgiroduedate);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiroduedate=$th."-".$bl."-".$hr;
			}
			$dgiroterima			= $this->input->post('dgiroterima', TRUE);
			if($dgiroterima!=''){
				$tmp=explode("-",$dgiroterima);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiroterima=$th."-".$bl."-".$hr;
			}
			$egirodescription= $this->input->post('egirodescription', TRUE);
			$egirobank		= $this->input->post('egirobank', TRUE);
			$vjumlah		= $this->input->post('vjumlah', TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$vsisa			= $vjumlah;

			if (
				(isset($igiro) && $igiro != '') &&
				//~ (isset($idt) && $idt != '') &&
				(isset($dgiroduedate) && $dgiroduedate != '') &&
				(isset($dgiro) && $dgiro != '') &&
				(isset($iarea) && $iarea != '') &&
				(isset($icustomer) && $icustomer != '') && 
				(isset($dgiro) && $dgiro != '') && 
#				(isset($dsetor) && $dsetor != '') && 
				(isset($dgiroduedate) && $dgiroduedate != '') && 
				(isset($vjumlah) && $vjumlah != '')
			   )
			{
				$this->load->model('giro/mmaster');
				$irv = $this->mmaster->runningnumberrv($rvth);
				$this->db->trans_begin();
				if($dsetor=='')$dsetor=null;
				$this->mmaster->insert($igiro,$iarea,$icustomer,$irv,$dgiro,$dsetor,$dgiroduedate,$egirodescription,$egirobank,
									             $vjumlah,$vsisa,$idt,$dgiroterima);
        if ($this->db->trans_status() === FALSE)
			  {
		      $this->db->trans_rollback();
			  }else{
				  $this->db->trans_commit();
		
				print "<script>alert(\"Sukses no Giro ".$igiro."\");window.open(\"index\", \"_self\");</script>";
			    //~ $sess=$this->session->userdata('session_id');
			    //~ $id=$this->session->userdata('user_id');
			    //~ $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    //~ $rs		= pg_query($sql);
			    //~ if(pg_num_rows($rs)>0){
//~ 
				    //~ while($row=pg_fetch_assoc($rs)){
					    //~ $ip_address	  = $row['ip_address'];
					    //~ break;
				    //~ }
			    //~ }else{
				    //~ $ip_address='kosong';
			    //~ }
			    //~ $query 	= pg_query("SELECT current_timestamp as c");
	        //~ while($row=pg_fetch_assoc($query)){
	        	//~ $now	  = $row['c'];
			    //~ }
			    //~ $pesan='Input Giro No:'.$igiro.' Pelanggan:'.$icustomer;
			    //~ $this->load->model('logger');
			    //~ $this->logger->write($id, $ip_address, $now , $pesan );  

				  //~ $data['sukses']			= true;
				  //~ $data['inomor']			= $igiro;
				  //~ $this->load->view('nomor',$data);
			  }
			}
			else{
				print "<script>alert(\"Maaf, Data masih ada yang salah!\");window.open(\"index\", \"_self\");</script>";
				
				}
		
	}
	function dt()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		  $cari = strtoupper($this->input->post("cari"));
			$area = $this->input->post("area");
 	    if($area=='')$area = $this->uri->segment(4);
			if($cari=='' && $this->uri->segment(5)!='sikasep'){
  			$cari = strtoupper($this->uri->segment(5));
  	  }
  	  if($cari==''){
  			$config['base_url'] = base_url().'index.php/giro/cform/dt/'.$area.'/sikasep/';
  	  }else{
  			$config['base_url'] = base_url().'index.php/giro/cform/dt/'.$area.'/'.$cari.'/';
  	  }
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tm_dt where i_area = '$area'and i_dt like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('giro/mmaster');
			$data['page_title'] = $this->lang->line('list_dt');
			$data['isi']=$this->mmaster->bacadt($cari,$area,$config['per_page'],$this->uri->segment(6));
			$data['area']=$area;
			$data['cari']=$cari;
			$this->load->view('giro/vlistdt', $data);
		
	}
}
?>
