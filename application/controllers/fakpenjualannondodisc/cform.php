<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}

	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo').' Discount Item';
			$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo').' Discount Item';
			$data['form_nomor_f_fpenjualanndo']	= $this->lang->line('form_nomor_f_fpenjualanndo');
			$data['form_tgl_f_fpenjualanndo']	= $this->lang->line('form_tgl_f_fpenjualanndo');
			$data['form_cabang_fpenjualanndo']	= $this->lang->line('form_cabang_fpenjualanndo');
			$data['form_pilih_cab_fpenjualanndo']	= $this->lang->line('form_pilih_cab_fpenjualanndo');
			$data['form_pilih_cab_manual_fpenjualanndo'] = $this->lang->line('form_pilih_cab_manual_fpenjualanndo');
			$data['form_detail_f_fpenjualanndo']	= $this->lang->line('form_detail_f_fpenjualanndo');
			$data['form_kd_brg_fpenjualanndo']	= $this->lang->line('form_kd_brg_fpenjualanndo');
			$data['form_nm_brg_fpenjualanndo']	= $this->lang->line('form_nm_brg_fpenjualanndo');
			$data['form_hjp_fpenjualanndo']	= $this->lang->line('form_hjp_fpenjualanndo');
			$data['form_qty_fpenjualanndo']	= $this->lang->line('form_qty_fpenjualanndo');
			$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
			$data['form_ket_f_fpenjualanndo']	= $this->lang->line('form_ket_f_fpenjualanndo');
			$data['form_tgl_jtempo_fpenjualanndo']	= $this->lang->line('form_tgl_jtempo_fpenjualanndo');
			$data['form_no_fpajak_fpenjualanndo']	= $this->lang->line('form_no_fpajak_fpenjualanndo');
			$data['form_tgl_fpajak_fpenjualanndo']	= $this->lang->line('form_tgl_fpajak_fpenjualanndo');
			$data['form_ket_cetak_fpenjualanndo']	= $this->lang->line('form_ket_cetak_fpenjualanndo');
			$data['form_tnilai_fpenjualanndo']	= $this->lang->line('form_tnilai_fpenjualanndo');
			$data['form_diskon_fpenjualanndo']	= $this->lang->line('form_diskon_fpenjualanndo');
			$data['form_dlm_fpenjualanndo']	= $this->lang->line('form_dlm_fpenjualanndo');
			$data['form_total_fpenjualanndo']	= $this->lang->line('form_total_fpenjualanndo');
			$data['form_ppn_fpenjualanndo']		= $this->lang->line('form_ppn_fpenjualanndo');
			$data['form_grand_t_fpenjualanndo']	= $this->lang->line('form_grand_t_fpenjualanndo');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$data['tjthtempo']	= "";
		
			$tgl	= date("d");
			$bln	= date("m");
			$tahun	= date("Y");
			
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$tahun;
			$data['tgPajak']	= $tgl."/".$bln."/".$tahun;
			
			$this->load->model('fakpenjualannondodisc/mclass');

			$qryfakpajak	= $this->mclass->nofakturpajak();								
			$qryth		= $this->mclass->getthnfaktur();
			$qryfaktur	= $this->mclass->getnomorfaktur();		

			$qryfakpajak_do	= $this->mclass->nofakturpajak_do();
			$qryth_do	= $this->mclass->getthnfaktur_do();
			$qryfaktur_do	= $this->mclass->getnomorfaktur_do();

			$qryfakpajak_bhnbaku	= $this->mclass->nofakturpajak_bhnbaku();
			$qryth_bhnbaku		= $this->mclass->getthnfaktur_bhnbaku();
			$qryfaktur_bhnbaku	= $this->mclass->getnomorfaktur_bhnbaku();
						
			$data['opt_cabang']	= $this->mclass->lcabang();

			if($qryfaktur->num_rows()>0){
				$row	= $qryfaktur->row_array();
				$faktur	= $row['ifaktur']+1;	
			}else{
				$faktur	= 1;
			}
			
			if($qryfaktur_do->num_rows()>0){
				$row2	= $qryfaktur_do->row_array();
				$faktur_do = $row2['ifaktur']+1;
			}else{
				$faktur_do = 1;
			}

			if($qryfaktur_bhnbaku->num_rows()>0){
				$row3	= $qryfaktur_bhnbaku->row_array();
				$faktur_bhnbaku = $row3['ifaktur']+1;
			}else{
				$faktur_bhnbaku = 1;
			}
			
			if($faktur > $faktur_do && $faktur > $faktur_bhnbaku) {
				$th	= $qryth->row_array();
				$thn	= $th['thn'];
			}elseif($faktur_do > $faktur && $faktur_do > $faktur_bhnbaku) {
				if($qryth_do->num_rows()>0){
					$th	= $qryth_do->row_array();
					$thn	= $th['thn'];						
				}else{
					$thn	= $tahun;		
				}
			}else{
				if($qryth_bhnbaku->num_rows()>0){
					$th	= $qryth_bhnbaku->row_array();
					$thn	= $th['thn'];					
				}else{
					$thn	= $tahun;		
				}
			}
			
			if($thn==$tahun) {

				if($faktur > $faktur_do && $faktur > $faktur_bhnbaku) {
					$row_fakpajak	= $qryfakpajak->row();
					$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
				}elseif($faktur_do > $faktur && $faktur_do > $faktur_bhnbaku) {
					if($qryfakpajak_do->num_rows()>0){
						$row_fakpajak	= $qryfakpajak_do->row();
						$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
					}else{
						$data['nofakturpajak']	= 1;
					}
				}else{
					if($qryfakpajak_bhnbaku->num_rows()>0){
						$row_fakpajak	= $qryfakpajak_bhnbaku->row();
						$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
					}else{
						$data['nofakturpajak']	= 1;
					}						
				}

			}else{
				$data['nofakturpajak']	= 1;
			}

			if($thn==$tahun){
			
				if($faktur!=1 || $faktur_do!=1 || $faktur_bhnbaku!=1)  {
					
					if($faktur > $faktur_do && $faktur > $faktur_bhnbaku) {
						$Zz	= $faktur;
					}elseif($faktur_do > $faktur && $faktur_do > $faktur_bhnbaku) {
						$Zz	= $faktur_do;
					}else{
						$Zz	= $faktur_bhnbaku;
					}
							
					switch(strlen($Zz)) {
						case "1": $nomorfaktur	= "000".$Zz;
						break;
						case "2": $nomorfaktur	= "00".$Zz;
						break;	
						case "3": $nomorfaktur	= "0".$Zz;
						break;
						case "4": $nomorfaktur	= $Zz;
						break;
					}
				}else{
					$nomorfaktur = "0001";
				}
				$nomor	= $tahun.$nomorfaktur;
			}else{
				$nomor	= $tahun."0001";
			}
			
			$data['no']	= $nomor;
			
			$data['isi']='fakpenjualannondodisc/vmainform';		
			$this->load->view('template',$data);
			
		
	}
	
	
	function index_old() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
			$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
			$data['form_nomor_f_fpenjualanndo']	= $this->lang->line('form_nomor_f_fpenjualanndo');
			$data['form_tgl_f_fpenjualanndo']	= $this->lang->line('form_tgl_f_fpenjualanndo');
			$data['form_cabang_fpenjualanndo']	= $this->lang->line('form_cabang_fpenjualanndo');
			$data['form_pilih_cab_fpenjualanndo']	= $this->lang->line('form_pilih_cab_fpenjualanndo');
			$data['form_pilih_cab_manual_fpenjualanndo'] = $this->lang->line('form_pilih_cab_manual_fpenjualanndo');
			$data['form_detail_f_fpenjualanndo']	= $this->lang->line('form_detail_f_fpenjualanndo');
			$data['form_kd_brg_fpenjualanndo']	= $this->lang->line('form_kd_brg_fpenjualanndo');
			$data['form_nm_brg_fpenjualanndo']	= $this->lang->line('form_nm_brg_fpenjualanndo');
			$data['form_hjp_fpenjualanndo']	= $this->lang->line('form_hjp_fpenjualanndo');
			$data['form_qty_fpenjualanndo']	= $this->lang->line('form_qty_fpenjualanndo');
			$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
			$data['form_ket_f_fpenjualanndo']	= $this->lang->line('form_ket_f_fpenjualanndo');
			$data['form_tgl_jtempo_fpenjualanndo']	= $this->lang->line('form_tgl_jtempo_fpenjualanndo');
			$data['form_no_fpajak_fpenjualanndo']	= $this->lang->line('form_no_fpajak_fpenjualanndo');
			$data['form_tgl_fpajak_fpenjualanndo']	= $this->lang->line('form_tgl_fpajak_fpenjualanndo');
			$data['form_ket_cetak_fpenjualanndo']	= $this->lang->line('form_ket_cetak_fpenjualanndo');
			$data['form_tnilai_fpenjualanndo']	= $this->lang->line('form_tnilai_fpenjualanndo');
			$data['form_diskon_fpenjualanndo']	= $this->lang->line('form_diskon_fpenjualanndo');
			$data['form_dlm_fpenjualanndo']	= $this->lang->line('form_dlm_fpenjualanndo');
			$data['form_total_fpenjualanndo']	= $this->lang->line('form_total_fpenjualanndo');
			$data['form_ppn_fpenjualanndo']		= $this->lang->line('form_ppn_fpenjualanndo');
			$data['form_grand_t_fpenjualanndo']	= $this->lang->line('form_grand_t_fpenjualanndo');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$data['tjthtempo']	= "";
		
			$tgl	= date("d");
			$bln	= date("m");
			$tahun	= date("Y");
			
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$tahun;
			$data['tgPajak']	= $tgl."/".$bln."/".$tahun;
			
			$this->load->model('fakpenjualannondodisc/mclass');

			$qryfakpajak	= $this->mclass->nofakturpajak();								
			$qryth		= $this->mclass->getthnfaktur();
			$qryfaktur	= $this->mclass->getnomorfaktur();		

			$qryfakpajak_do	= $this->mclass->nofakturpajak_do();
			$qryth_do	= $this->mclass->getthnfaktur_do();
			$qryfaktur_do	= $this->mclass->getnomorfaktur_do();

			$qryfakpajak_bhnbaku	= $this->mclass->nofakturpajak_bhnbaku();
			$qryth_bhnbaku		= $this->mclass->getthnfaktur_bhnbaku();
			$qryfaktur_bhnbaku	= $this->mclass->getnomorfaktur_bhnbaku();
						
			$data['opt_cabang']	= $this->mclass->lcabang();

			if($qryfaktur->num_rows()>0){
				$row	= $qryfaktur->row_array();
				$faktur	= $row['ifaktur']+1;	
			}else{
				$faktur	= 1;
			}
			
			if($qryfaktur_do->num_rows()>0){
				$row2	= $qryfaktur_do->row_array();
				$faktur_do = $row2['ifaktur']+1;
			}else{
				$faktur_do = 1;
			}

			if($qryfaktur_bhnbaku->num_rows()>0){
				$row3	= $qryfaktur_bhnbaku->row_array();
				$faktur_bhnbaku = $row3['ifaktur']+1;
			}else{
				$faktur_bhnbaku = 1;
			}
			
			//if($qryth->num_rows()>0) {
				if($faktur > $faktur_do && $faktur > $faktur_bhnbaku) {
					$th	= $qryth->row_array();
					$thn	= $th['thn'];
				}elseif($faktur_do > $faktur && $faktur_do > $faktur_bhnbaku) {
					if($qryth_do->num_rows()>0){
						$th	= $qryth_do->row_array();
						$thn	= $th['thn'];						
					}else{
						$thn	= $tahun;		
					}
				}else{
					if($qryth_bhnbaku->num_rows()>0){
						$th	= $qryth_bhnbaku->row_array();
						$thn	= $th['thn'];					
					}else{
						$thn	= $tahun;		
					}
				}
			//}else{
			//	$thn	= $tahun;
			//}
			
			if($thn==$tahun) {
				//if($qryfakpajak->num_rows()>0) {
					if($faktur > $faktur_do && $faktur > $faktur_bhnbaku) {
						$row_fakpajak	= $qryfakpajak->row();
						$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
					}elseif($faktur_do > $faktur && $faktur_do > $faktur_bhnbaku) {
						if($qryfakpajak_do->num_rows()>0){
							$row_fakpajak	= $qryfakpajak_do->row();
							$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
						}else{
							$data['nofakturpajak']	= 1;
						}
					}else{
						if($qryfakpajak_bhnbaku->num_rows()>0){
							$row_fakpajak	= $qryfakpajak_bhnbaku->row();
							$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
						}else{
							$data['nofakturpajak']	= 1;
						}						
					}
				//}else{
				//	$data['nofakturpajak']	= 1;
				//}
			}else{
				$data['nofakturpajak']	= 1;
			}

			if($thn==$tahun){
			
				if($faktur!=1 || $faktur_do!=1 || $faktur_bhnbaku!=1)  {
					
					if($faktur > $faktur_do && $faktur > $faktur_bhnbaku) {
						$Zz	= $faktur;
					}elseif($faktur_do > $faktur && $faktur_do > $faktur_bhnbaku) {
						$Zz	= $faktur_do;
					}else{
						$Zz	= $faktur_bhnbaku;
					}
							
					switch(strlen($Zz)) {
						case "1": $nomorfaktur	= "000".$Zz;
						break;
						case "2": $nomorfaktur	= "00".$Zz;
						break;	
						case "3": $nomorfaktur	= "0".$Zz;
						break;
						case "4": $nomorfaktur	= $Zz;
						break;
					}
				}else{
					$nomorfaktur = "0001";
				}
				$nomor	= $tahun.$nomorfaktur;
			}else{
				$nomor	= $tahun."0001";
			}
									
			/* 29072011
			if($qryth->num_rows() > 0) {
				$th		= $qryth->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}
			
			if($thn==$tahun) {
				if($qryfakpajak->num_rows()>0) {
					$row_fakpajak	= $qryfakpajak->row();
					$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
				} else {
					$data['nofakturpajak']	= 1;
				}
			}else{
				$data['nofakturpajak']	= 1;
			}
		
			if($thn==$tahun) {
				if($qryfaktur->num_rows() > 0)  {
					$row	= $qryfaktur->row_array();
					$faktur		= $row['ifaktur']+1;		
					switch(strlen($faktur)) {
						case "1": $nomorfaktur	= "000".$faktur;
						break;
						case "2": $nomorfaktur	= "00".$faktur;
						break;	
						case "3": $nomorfaktur	= "0".$faktur;
						break;
						case "4": $nomorfaktur	= $faktur;
						break;
					}
				} else {
					$nomorfaktur = "0001";
				}
				$nomor	= $tahun.$nomorfaktur;
			} else {
				$nomor	= $tahun."0001";
			}
			*/
			
			$data['no']	= $nomor;
			
			$this->load->view('fakpenjualannondodisc/vmainform',$data);	
		
	}

	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$is_pakai_sj	= $this->uri->segment(6);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['is_pakai_sj']	= $is_pakai_sj;
		
		if ($is_pakai_sj == '1')
			$data['page_title']	= "SURAT JALAN (SJ)";
		else
			$data['page_title']	= "DATA MASTER BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualannondodisc/mclass');

		$query	= $this->mclass->lbarangjadi($ibranch, $is_pakai_sj);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/fakpenjualannondodisc/cform/listbarangjadinext/'.$iterasi.'/'.$ibranch.'/'.$is_pakai_sj;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($ibranch,$is_pakai_sj, $pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('fakpenjualannondodisc/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$is_pakai_sj	= $this->uri->segment(6);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['is_pakai_sj']	= $is_pakai_sj;
		
		if ($is_pakai_sj == '1')
			$data['page_title']	= "SURAT JALAN (SJ)";
		else
			$data['page_title']	= "DATA MASTER BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualannondodisc/mclass');

		$query	= $this->mclass->lbarangjadi($ibranch, $is_pakai_sj);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/fakpenjualannondodisc/cform/listbarangjadinext/'.$iterasi.'/'.$ibranch.'/'.$is_pakai_sj;
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($ibranch,$is_pakai_sj, $pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('fakpenjualannondodisc/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$ibranchhidden	= $this->input->post('ibranchhidden')?$this->input->post('ibranchhidden'):$this->input->get_post('ibranchhidden');
		$is_pakai_sj	= $this->input->post('is_pakai_sj')?$this->input->post('is_pakai_sj'):$this->input->get_post('is_pakai_sj');
		
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$is_pakai_sj	= $this->uri->segment(6);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['is_pakai_sj']	= $is_pakai_sj;
		
		$data['page_title']	= "SURAT JALAN (SJ)";
		$data['isi']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualannondodisc/mclass');

		$query	= $this->mclass->flbarangjadi($ibranchhidden,$is_pakai_sj,$key);
		$jml	= $query->num_rows();

		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				if ($is_pakai_sj == '1') {
					$list .= "
					 <tr>
					  <td width=\"2px;\">".$cc."</td>
					  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qtyakhir','$row->nilai','$row->isj','$row->qtyakhir');\">".$row->imotif."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','$row->hjp','$row->qtyakhir','$row->nilai','$row->isj','$row->qtyakhir');\">".$row->productname."</a></td>
					 </tr>";
				}
				else {
					$list .= "
					 <tr>
					  <td width=\"2px;\">".$cc."</td>
					  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->imotif','$row->productname','0','0','0','0','0');\">".$row->imotif."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->imotif','$row->productname','0','0','0','0','0');\">".$row->productname."</a></td>
					 </tr>";
				}
				 $cc+=1;
			}
		}else{
			$list .= "";
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
	
	/* Disabled 19-02-2011
	// Awalnya sebagai acuan, bahwa faktur penjualan berasal dari BBK.
	// diubah ke tabel SJ sbg acuannya
	*/
	/*
	function listpelanggan() {
		
		$data['page_title']	= "BUKTI BARANG KELUAR/ SJ";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualannondodisc/mclass');

		$query	= $this->mclass->lpelanggan();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/fakpenjualannondodisc/cform/listpelanggannext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		$data['isi']	= $this->mclass->lpelangganperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('fakpenjualannondodisc/vlistformpelanggan',$data);	
	}

	function listpelanggannext() {
		
		$data['page_title']	= "BUKTI BARANG KELUAR/ SJ";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualannondodisc/mclass');

		$query	= $this->mclass->lpelanggan();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/fakpenjualannondodisc/cform/listpelanggannext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		$data['isi']	= $this->mclass->lpelangganperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('fakpenjualannondodisc/vlistformpelanggan',$data);	
	}
	
	function flistpelanggan() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "BUKTI BARANG KELUAR/ SJ";
		$data['isi']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualannondodisc/mclass');

		$query	= $this->mclass->fpelanggan($key);
		$jml	= $query->num_rows();

		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				$list .= "
					<tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->nomorbbk');\">".$row->nomorbbk."</a></td>
					  <td><a href=\"javascript:settextfield('$row->nomorbbk');\">".$row->kepada."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->nomorbbk');\">".$row->alamat."</a></td>
					  <td><a href=\"javascript:settextfield('$row->nomorbbk');\">".$row->tglbbk."</a></td>
					 </tr>				 
				";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}
	*/	
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
		$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
		$data['form_nomor_f_fpenjualanndo']	= $this->lang->line('form_nomor_f_fpenjualanndo');
		$data['form_tgl_f_fpenjualanndo']	= $this->lang->line('form_tgl_f_fpenjualanndo');
		$data['form_cabang_fpenjualanndo']	= $this->lang->line('form_cabang_fpenjualanndo');
		$data['form_pilih_cab_fpenjualanndo']	= $this->lang->line('form_pilih_cab_fpenjualanndo');
		$data['form_detail_f_fpenjualanndo']	= $this->lang->line('form_detail_f_fpenjualanndo');
		$data['form_kd_brg_fpenjualanndo']	= $this->lang->line('form_kd_brg_fpenjualanndo');
		$data['form_nm_brg_fpenjualanndo']	= $this->lang->line('form_nm_brg_fpenjualanndo');
		$data['form_hjp_fpenjualanndo']	= $this->lang->line('form_hjp_fpenjualanndo');
		$data['form_qty_fpenjualanndo']	= $this->lang->line('form_qty_fpenjualanndo');
		$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
		$data['form_ket_f_fpenjualanndo']	= $this->lang->line('form_ket_f_fpenjualanndo');
		$data['form_tgl_jtempo_fpenjualanndo']	= $this->lang->line('form_tgl_jtempo_fpenjualanndo');
		$data['form_no_fpajak_fpenjualanndo']	= $this->lang->line('form_no_fpajak_fpenjualanndo');
		$data['form_tgl_fpajak_fpenjualanndo']	= $this->lang->line('form_tgl_fpajak_fpenjualanndo');
		$data['form_ket_cetak_fpenjualanndo']	= $this->lang->line('form_ket_cetak_fpenjualanndo');
		$data['form_tnilai_fpenjualanndo']	= $this->lang->line('form_tnilai_fpenjualanndo');
		$data['form_diskon_fpenjualanndo']	= $this->lang->line('form_diskon_fpenjualanndo');
		$data['form_dlm_fpenjualanndo']	= $this->lang->line('form_dlm_fpenjualanndo');
		$data['form_total_fpenjualanndo']	= $this->lang->line('form_total_fpenjualanndo');
		$data['form_ppn_fpenjualanndo']		= $this->lang->line('form_ppn_fpenjualanndo');
		$data['form_grand_t_fpenjualanndo']	= $this->lang->line('form_grand_t_fpenjualanndo');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['isi']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		
		/* 20012012		
		$iteration	= $this->input->post('iteration')?$this->input->post('iteration'):$this->input->get_post('iteration');
		$i_faktur	= $this->input->post('i_faktur')?$this->input->post('i_faktur'):$this->input->get_post('i_faktur');
		$d_faktur	= $this->input->post('d_faktur')?$this->input->post('d_faktur'):$this->input->get_post('d_faktur');
		$i_branch	= $this->input->post('i_branch')?$this->input->post('i_branch'):$this->input->get_post('i_branch');
		$d_pajak	= $this->input->post('d_pajak')?$this->input->post('d_pajak'):$this->input->get_post('d_pajak');
		*/

		$iteration	= $this->input->post('iteration');
		$i_faktur	= $this->input->post('i_faktur');
		$d_faktur	= $this->input->post('d_faktur');
		$i_branch	= $this->input->post('i_branch');
		
		// 28-05-2013
		$is_pakai_sj	= $this->input->post('is_pakai_sj');
		
		$d_pajak	= $this->input->post('d_pajak');
				
		/* Disabled 19-02-2011
		$txt_pel_manual	= $this->input->post('txt_pel_manual')?$this->input->post('txt_pel_manual'):$this->input->get_post('txt_pel_manual');
		$ibbk	= $this->input->post('id_pel_manual')?$this->input->post('id_pel_manual'):$this->input->get_post('id_pel_manual');
		*/
		
		$i_product	= array();
		$e_product_name	= array();
		$v_hjp	= array();
		$n_quantity	= array();
		$v_unit_price	= array();
		$ndisc	= array();
		$vdisc	= array();
		$isjcode	= array();
		
		$e_product_name_0	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iteration;$cacah++) {
			/* 20012012
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_hjp[$cacah]	= $this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('v_hjp'.'_'.'tblItem'.'_'.$cacah);
			$n_quantity[$cacah]	= $this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('n_quantity'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
			$isjcode[$cacah]	= $this->input->post('isjcode'.'_'.'tblItem'.'_'.$cacah)?$this->input->post('isjcode'.'_'.'tblItem'.'_'.$cacah):$this->input->get_post('isjcode'.'_'.'tblItem'.'_'.$cacah);
			*/ 

			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_hjp[$cacah]	= $this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah);
			$n_quantity[$cacah]	= $this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
			$ndisc[$cacah]	= $this->input->post('n_discount'.'_'.'tblItem'.'_'.$cacah);
			$vdisc[$cacah]	= $this->input->post('v_discount'.'_'.'tblItem'.'_'.$cacah);
			$isjcode[$cacah]	= $this->input->post('isjcode'.'_'.'tblItem'.'_'.$cacah);
						
		}
		
		/* 20012012
		$e_note_faktur	= $this->input->post('e_note_faktur')?$this->input->post('e_note_faktur'):$this->input->get_post('e_note_faktur');
		$v_total_nilai	= $this->input->post('v_total_nilai')?$this->input->post('v_total_nilai'):$this->input->get_post('v_total_nilai');
		$n_discount	= $this->input->post('n_discount')?$this->input->post('n_discount'):$this->input->get_post('n_discount');
		$v_discount	= $this->input->post('v_discount')?$this->input->post('v_discount'):$this->input->get_post('v_discount');
		$d_due_date	= $this->input->post('d_due_date')?$this->input->post('d_due_date'):$this->input->get_post('d_due_date');
		$v_total_faktur	= $this->input->post('v_total_faktur')?$this->input->post('v_total_faktur'):$this->input->get_post('v_total_faktur');
		$i_faktur_pajak	= $this->input->post('i_faktur_pajak')?$this->input->post('i_faktur_pajak'):$this->input->get_post('i_faktur_pajak');
		$n_ppn	= $this->input->post('n_ppn')?$this->input->post('n_ppn'):$this->input->get_post('n_ppn');
		$f_cetak	= $this->input->post('f_cetak')?$this->input->post('f_cetak'):$this->input->get_post('f_cetak');
		$v_total_fppn	= $this->input->post('v_total_fppn')?$this->input->post('v_total_fppn'):$this->input->post('v_total_fppn');
		*/

		$e_note_faktur	= $this->input->post('e_note_faktur');
		$v_total_nilai	= $this->input->post('v_total_nilai');
		$n_discount	= $this->input->post('n_discount');
		$v_discount	= $this->input->post('v_discount');
		$d_due_date	= $this->input->post('d_due_date');
		$v_total_faktur	= $this->input->post('v_total_faktur');
		$i_faktur_pajak	= $this->input->post('i_faktur_pajak');
		$n_ppn	= $this->input->post('n_ppn');
		$f_cetak	= $this->input->post('f_cetak');
		$v_total_fppn	= $this->input->post('v_total_fppn');
		
		$ex_d_pajak		= explode("/",$d_pajak,strlen($d_pajak)); // dd/mm/YYYY
		$ex_d_due_date	= explode("/",$d_due_date,strlen($d_due_date));
		$ex_d_faktur	= explode("/",$d_faktur,strlen($d_faktur));
		
		$nw_d_pajak	= $ex_d_pajak[2]."-".$ex_d_pajak[1]."-".$ex_d_pajak[0]; //YYYY-mm-dd
		$nw_d_due_date	= $ex_d_due_date[2]."-".$ex_d_due_date[1]."-".$ex_d_due_date[0]; //YYYY-mm-dd
		$nw_d_faktur	= $ex_d_faktur[2]."-".$ex_d_faktur[1]."-".$ex_d_faktur[0]; //YYYY-mm-dd
		
		$ex_v_total_faktur	= explode(".",$v_total_faktur,strlen($v_total_faktur));
		$ex_v_total_fppn	= explode(".",$v_total_fppn,strlen($v_total_fppn));
		$ex_v_discount	= explode(".",$v_discount,strlen($v_discount));
		
		$nw_v_total_faktur	= $ex_v_total_faktur[0];
		$nw_v_total_fppn	= $ex_v_total_fppn[0];
		$nw_v_discount		= $ex_v_discount[0];
		
		$this->load->model('fakpenjualannondodisc/mclass');
		
		if(!empty($i_faktur) &&
		   !empty($d_faktur) && 
		   !empty($i_branch)
		) {
			//if($this->load->model('fakpenjualannondodisc/mclass')) {
				if(!empty($e_product_name_0)) {
					/* Disabled 19-02-2011 
					if($i_branch==1 && $txt_pel_manual!="" ) {
					*/
						/* Disabled 19-02-2011
						$this->mclass->msimpan($i_faktur,$nw_d_faktur,$i_branch,$e_note_faktur,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$nw_v_total_fppn,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$txt_pel_manual,$ibbk);
						*/
					$qnsop	= $this->mclass->cari_fpenjualan($i_faktur);	
					if($qnsop->num_rows() < 0 || $qnsop->num_rows()==0) {
					$this->mclass->msimpan($is_pakai_sj, $i_faktur,$nw_d_faktur,$i_branch,$e_note_faktur,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$nw_v_total_fppn,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$isjcode,$ndisc,$vdisc);
					}else{
						print "<script>alert(\"Maaf, Nomor Faktur tsb sebelumnya telah diinput. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
					}
					/* Disabled 19-02-2011
					} else {
						print "<script>alert(\"Maaf, Pelanggan yg dimasukan hrs dari Input Manual BBK \");window.open(\"index\", \"_self\");</script>";	
					}
					*/
				}else{
					print "<script>alert(\"Maaf, item Faktur Penjulan hrs terisi. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
				}
			/*	
			} else {
				$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
				$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
				$data['form_nomor_f_fpenjualanndo']	= $this->lang->line('form_nomor_f_fpenjualanndo');
				$data['form_tgl_f_fpenjualanndo']	= $this->lang->line('form_tgl_f_fpenjualanndo');
				$data['form_cabang_fpenjualanndo']	= $this->lang->line('form_cabang_fpenjualanndo');
				$data['form_pilih_cab_fpenjualanndo']	= $this->lang->line('form_pilih_cab_fpenjualanndo');
				$data['form_detail_f_fpenjualanndo']	= $this->lang->line('form_detail_f_fpenjualanndo');
				$data['form_kd_brg_fpenjualanndo']	= $this->lang->line('form_kd_brg_fpenjualanndo');
				$data['form_nm_brg_fpenjualanndo']	= $this->lang->line('form_nm_brg_fpenjualanndo');
				$data['form_hjp_fpenjualanndo']	= $this->lang->line('form_hjp_fpenjualanndo');
				$data['form_qty_fpenjualanndo']	= $this->lang->line('form_qty_fpenjualanndo');
				$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
				$data['form_ket_f_fpenjualanndo']	= $this->lang->line('form_ket_f_fpenjualanndo');
				$data['form_tgl_jtempo_fpenjualanndo']	= $this->lang->line('form_tgl_jtempo_fpenjualanndo');
				$data['form_no_fpajak_fpenjualanndo']	= $this->lang->line('form_no_fpajak_fpenjualanndo');
				$data['form_tgl_fpajak_fpenjualanndo']	= $this->lang->line('form_tgl_fpajak_fpenjualanndo');
				$data['form_ket_cetak_fpenjualanndo']	= $this->lang->line('form_ket_cetak_fpenjualanndo');
				$data['form_tnilai_fpenjualanndo']	= $this->lang->line('form_tnilai_fpenjualanndo');
				$data['form_diskon_fpenjualanndo']	= $this->lang->line('form_diskon_fpenjualanndo');
				$data['form_dlm_fpenjualanndo']	= $this->lang->line('form_dlm_fpenjualanndo');
				$data['form_total_fpenjualanndo']	= $this->lang->line('form_total_fpenjualanndo');
				$data['form_ppn_fpenjualanndo']		= $this->lang->line('form_ppn_fpenjualanndo');
				$data['form_grand_t_fpenjualanndo']	= $this->lang->line('form_grand_t_fpenjualanndo');
				$data['button_simpan']	= $this->lang->line('button_simpan');
				$data['button_batal']	= $this->lang->line('button_batal');
				$data['isi']		= "";
				$data['list']		= "";
				$data['lcabang']	= "";
				$data['limages']	= base_url();
				$tahun	= date("Y");
				$data['tjthtempo']	= "";
				
				$tgl	= date("d");
				$bln	= date("m");
				$thn	= date("Y");
			
				$data['dateTime']	= date("m/d/Y",time());
				$data['tgFaktur']	= $tgl."/".$bln."/".$thn;
				$data['tgPajak']	= $tgl."/".$bln."/".$thn;
						
				$this->load->model('fakpenjualannondodisc/mclass');
								
				$qryth	= $this->mclass->getthnfaktur();
				$qryfaktur	= $this->mclass->getnomorfaktur();		
				$data['opt_cabang']	= $this->mclass->lcabang();
		
				if($qryth->num_rows() > 0) {
					$th		= $qryth->row_array();
					$thn	= $th['thn'];
				} else {
					$thn	= $tahun;
				}
				
				if($thn==$tahun) {
					if($qryfaktur->num_rows() > 0)  {
						$row	= $qryfaktur->row_array();
						$faktur		= $row['ifaktur']+1;		
						switch(strlen($faktur)) {
							case "1": $nomorfaktur	= "0000".$faktur;
							break;
							case "2": $nomorfaktur	= "000".$faktur;
							break;	
							case "3": $nomorfaktur	= "00".$faktur;
							break;
							case "4": $nomorfaktur	= "0".$faktur;
							break;
							case "5": $nomorfaktur	= $faktur;
							break;	
						}
					} else {
						$nomorfaktur = "00001";
					}
					$nomor	= $tahun.$nomorfaktur;
				} else {
					$nomor	= $tahun."00001";
				}
				$data['no']	= $nomor;
				
				print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
				
				$this->load->view('fakpenjualannondodisc/vmainform',$data);			
			}
			*/
		} else {
			$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
			$data['page_title_fpenjualanndo']	= $this->lang->line('page_title_fpenjualanndo');
			$data['form_nomor_f_fpenjualanndo']	= $this->lang->line('form_nomor_f_fpenjualanndo');
			$data['form_tgl_f_fpenjualanndo']	= $this->lang->line('form_tgl_f_fpenjualanndo');
			$data['form_cabang_fpenjualanndo']	= $this->lang->line('form_cabang_fpenjualanndo');
			$data['form_pilih_cab_fpenjualanndo']	= $this->lang->line('form_pilih_cab_fpenjualanndo');
			$data['form_detail_f_fpenjualanndo']	= $this->lang->line('form_detail_f_fpenjualanndo');
			$data['form_kd_brg_fpenjualanndo']	= $this->lang->line('form_kd_brg_fpenjualanndo');
			$data['form_nm_brg_fpenjualanndo']	= $this->lang->line('form_nm_brg_fpenjualanndo');
			$data['form_hjp_fpenjualanndo']	= $this->lang->line('form_hjp_fpenjualanndo');
			$data['form_qty_fpenjualanndo']	= $this->lang->line('form_qty_fpenjualanndo');
			$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
			$data['form_ket_f_fpenjualanndo']	= $this->lang->line('form_ket_f_fpenjualanndo');
			$data['form_tgl_jtempo_fpenjualanndo']	= $this->lang->line('form_tgl_jtempo_fpenjualanndo');
			$data['form_no_fpajak_fpenjualanndo']	= $this->lang->line('form_no_fpajak_fpenjualanndo');
			$data['form_tgl_fpajak_fpenjualanndo']	= $this->lang->line('form_tgl_fpajak_fpenjualanndo');
			$data['form_ket_cetak_fpenjualanndo']	= $this->lang->line('form_ket_cetak_fpenjualanndo');
			$data['form_tnilai_fpenjualanndo']	= $this->lang->line('form_tnilai_fpenjualanndo');
			$data['form_diskon_fpenjualanndo']	= $this->lang->line('form_diskon_fpenjualanndo');
			$data['form_dlm_fpenjualanndo']	= $this->lang->line('form_dlm_fpenjualanndo');
			$data['form_total_fpenjualanndo']	= $this->lang->line('form_total_fpenjualanndo');
			$data['form_ppn_fpenjualanndo']		= $this->lang->line('form_ppn_fpenjualanndo');
			$data['form_grand_t_fpenjualanndo']	= $this->lang->line('form_grand_t_fpenjualanndo');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$tahun	= date("Y");
			$data['tjthtempo']	= "";
			
			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
		
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$thn;
			$data['tgPajak']	= $tgl."/".$bln."/".$thn;
					
			$this->load->model('fakpenjualannondodisc/mclass');
							
			$qryth	= $this->mclass->getthnfaktur();
			$qryfaktur	= $this->mclass->getnomorfaktur();		
			$data['opt_cabang']	= $this->mclass->lcabang();
	
			if($qryth->num_rows() > 0) {
				$th		= $qryth->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}
			
			if($thn==$tahun) {
				if($qryfaktur->num_rows() > 0)  {
					$row	= $qryfaktur->row_array();
					$faktur		= $row['ifaktur']+1;		
					switch(strlen($faktur)) {
						case "1": $nomorfaktur	= "000".$faktur;
						break;
						case "2": $nomorfaktur	= "00".$faktur;
						break;	
						case "3": $nomorfaktur	= "0".$faktur;
						break;
						case "4": $nomorfaktur	= $faktur;
						break;
					}
				}else{
					$nomorfaktur = "0001";
				}
				$nomor	= $tahun.$nomorfaktur;
			}else{
				$nomor	= $tahun."0001";
			}
			$data['no']	= $nomor;
			
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			
			/* Hrsnya didisable */
			//$this->load->view('fakpenjualannondodisc/vmainform',$data);			
		}
	}

	function cari_fpenjualan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$fpenj	= $this->input->post('fpenj')?$this->input->post('fpenj'):$this->input->get_post('fpenj');
		$this->load->model('fakpenjualannondodisc/mclass');
		$qnsop	= $this->mclass->cari_fpenjualan($fpenj);
		if($qnsop->num_rows()>0) {
			echo "Maaf, No. Faktur sudah ada!";
		}		
	}	

	function cari_fpajak() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$tahun	= date("Y");
		$fpajak	= $this->input->post('fpajak')?$this->input->post('fpajak'):$this->input->get_post('fpajak');
		$this->load->model('fakpenjualannondodisc/mclass');
		$qnsop	= $this->mclass->cari_fpajak($fpajak,$tahun);
		if($qnsop->num_rows()>0) {
			echo "Maaf, No. Pajak sudah ada!";
		}
	}	
}
?>
