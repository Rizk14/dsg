<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "KATEGORI BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		
		$data['limages']	= base_url();
		
		$this->load->model('kategoribrg/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/kategoribrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']='kategoribrg/vmainform';
		$this->load->view('template',$data);
	}
	function tambah() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "KATEGORI BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		
		$data['limages']	= base_url();
		
		$this->load->model('kategoribrg/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/kategoribrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
		$data['isi']='kategoribrg/vmainformadd';
		$this->load->view('template',$data);
	}

	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "KATEGORI BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('kategoribrg/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/kategoribrg/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
	$data['isi']='kategoribrg/vmainform';
		$this->load->view('template',$data);
		
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']='kategoribrg/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$i_category		= @$this->input->post('i_category')?@$this->input->post('i_category'):@$this->input->get_post('i_category');
		$e_category_name	= @$this->input->post('e_category_name')?@$this->input->post('e_category_name'):@$this->input->get_post('e_category_name');
		$i_class		= @$this->input->post('i_class')?@$this->input->post('i_class'):@$this->input->get_post('i_class');
		if( (isset($i_category) && !empty($i_category)) &&
		    (isset($e_category_name) && !empty($e_category_name)) && 
		    (isset($i_class) && !empty($i_class)) ) {
			$this->load->model('kategoribrg/mclass');
			$this->mclass->msimpan($i_category,$e_category_name,$i_class);
		} else {
			$data['page_title']	= "KATEGORI BARANG";
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			
			$data['limages']	= base_url();
			
			$this->load->model('kategoribrg/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= base_url().'index.php/kategoribrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);			

			$data['isi']='kategoribrg/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	function listklsbarang() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "KELAS BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('kategoribrg/mclass');
		$data['query']	= $this->mclass->listpquery($tabel="tr_class",$order="ORDER BY e_class_name ",$filter="");	
		
		$this->load->view('kategoribrg/vlistformklsbarang',$data);
			
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
				
		$this->load->model('kategoribrg/mclass');

		$qry_kategoribrg		= $this->mclass->medit($id);
		if($qry_kategoribrg->num_rows() > 0 ) {
			$row_kategoribrg	= $qry_kategoribrg->row();
			$data['i_category']	= $row_kategoribrg->i_category;
			$data['e_category_name']	= $row_kategoribrg->e_category_name;
			$data['i_category_code']	= $row_kategoribrg->i_category_code;
			$i_class	= $row_kategoribrg->i_class;
			$data['iclass']	= $i_class;
			$qry_katbrg	= $this->mclass->katbarang($i_class);
			if($qry_katbrg->num_rows()>0) {
				$row_katbrg	= $qry_katbrg->row();
				$data['classname']	= $row_katbrg->e_class_name;
			} else {
				$data['classname']	= "";
			}
			
		} else {
			$data['i_category']	= "";
			$data['e_category_name']	= "";
			$data['i_category_code']	= "";
			$data['i_class']	= "";
		}
		$data['isi']='kategoribrg/veditform';
		$this->load->view('template',$data);
		
	}
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$icategorycode	= @$this->input->post('i_category')?@$this->input->post('i_category'):@$this->input->get_post('i_category');
		$e_category_name = @$this->input->post('e_category_name')?@$this->input->post('e_category_name'):@$this->input->get_post('e_category_name');
		$icategory	= @$this->input->post('icategory')?@$this->input->post('icategory'):@$this->input->get_post('icategory');
		$i_class	= @$this->input->post('i_class')?@$this->input->post('i_class'):@$this->input->get_post('i_class');
		$iclass		= @$this->input->post('iclass')?@$this->input->post('iclass'):@$this->input->get_post('iclass');
		
		if( (isset($icategorycode) && !empty($icategorycode)) &&
		    (isset($e_category_name) && !empty($e_category_name)) && 
		    (isset($i_class) && !empty($i_class)) ) {
			$this->load->model('kategoribrg/mclass');
			$this->mclass->mupdate($icategory,$e_category_name,$icategorycode,$i_class,$iclass);
		} else {
			$data['page_title']	= "KATEGORI BARANG";
			$data['isi']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
			
			$data['limages']	= base_url();
			
			$this->load->model('kategoribrg/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] 	= 'kategoribrg/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_links']	= $this->pagination->create_links();
			
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
	
			$this->load->view('kategoribrg/vmainform',$data);
		}		
	}
	
	/* 20052011
	function actdelete() {
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_categories',array('i_category'=>$id));
	}	
	*/
	
	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('kategoribrg/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_category	= $this->input->post('txt_i_category')?$this->input->post('txt_i_category'):$this->input->get_post('txt_i_category');
		$txt_e_class_name	= $this->input->post('txt_e_class_name')?$this->input->post('txt_e_class_name'):$this->input->get_post('txt_e_class_name');
	
		$data['page_title']	= "KATEGORI BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('kategoribrg/mclass');
		$query	= $this->mclass->viewcari($txt_i_category,$txt_e_class_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'kategoribrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txt_i_category,$txt_e_class_name,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('kategoribrg/vcariform',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_category	= $this->input->post('txt_i_category')?$this->input->post('txt_i_category'):$this->input->get_post('txt_i_category');
		$txt_e_class_name	= $this->input->post('txt_e_class_name')?$this->input->post('txt_e_class_name'):$this->input->get_post('txt_e_class_name');
	
		$data['page_title']	= "KATEGORI BARANG";
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('kategoribrg/mclass');
		$query	= $this->mclass->viewcari($txt_i_category,$txt_e_class_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= base_url().'index.php/kategoribrg/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_links']	= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->mcari($txt_i_category,$txt_e_class_name,$pagination['per_page'],$pagination['cur_page']);

		$this->load->view('kategoribrg/vcariform',$data);
	}	
}
?>
