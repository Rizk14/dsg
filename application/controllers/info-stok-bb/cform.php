<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-stok-bb/mmaster');
  }

/*  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['isi'] = 'info-opname-hutang/vmainform';
	$this->load->view('template',$data);

  } */
  
  function view(){
    $data['isi'] = 'info-stok-bb/vformview';
	$id_gudang = $this->input->post('id_gudang', TRUE);
	$cstatus = $this->input->post('statusnya', TRUE);
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' && ($id_gudang == '' || $cstatus == '' )) {
		$id_gudang 	= $this->uri->segment(5);
		$cstatus 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($id_gudang == '')
		$id_gudang = '0';
	
	if ($cstatus == '')
		$cstatus = 't';

    $jum_total = $this->mmaster->get_stoktanpalimit($id_gudang, $cstatus, $keywordcari);

			$config['base_url'] = base_url().'index.php/info-stok-bb/cform/view/index/'.$id_gudang.'/'.$cstatus.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mmaster->get_stok($config['per_page'],$this->uri->segment(8), $id_gudang, $cstatus, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['cgudang'] = $id_gudang;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }

}
