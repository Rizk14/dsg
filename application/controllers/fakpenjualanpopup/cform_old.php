<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}

	function index() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0 ) 
		{
			$data['page_title_fpenjualando']	= $this->lang->line('page_title_fpenjualando');
			$data['form_detail_f_fpenjualando']	= $this->lang->line('form_detail_f_fpenjualando');
			$data['form_nomor_f_fpenjualando']	= $this->lang->line('form_nomor_f_fpenjualando');
			$data['form_cabang_fpenjualando']	= $this->lang->line('form_cabang_fpenjualando');
			$data['form_pilih_cab_fpenjualando']	= $this->lang->line('form_pilih_cab_fpenjualando');
			$data['form_nomor_do_f_fpenjualando']	= $this->lang->line('form_nomor_do_f_fpenjualando');
			
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['button_batal']	= $this->lang->line('button_batal');
			
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();		
		
			$this->load->model('fakpenjualanpopup/mclass');

			$tgl	= date("d");
			$bln	= date("m");
			$tahun	= date("Y");
			
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$tahun;
			$data['tgPajak']	= $tgl."/".$bln."/".$tahun;
			
			$data['opt_cabang']	= $this->mclass->lcabang();
		
			$this->load->view('fakpenjualanpopup/vmainform',$data);
		}	
	}

	function listdetaildo() {
		
		$ibranch	= $this->uri->segment(4);
		
		$data['ibranch']	= $ibranch;
		$data['page_title']	= "NOMOR DO";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$this->load->model('fakpenjualanpopup/mclass');

		$data['isi']	= $this->mclass->ldodetail($ibranch);
				
		$this->load->view('fakpenjualanpopup/vlistdo',$data);	
				
	}
	
	function flistdetaildo() {
		
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$ibranch	= $this->input->post('ibranch')?$this->input->post('ibranch'):$this->input->get_post('ibranch');

		$this->load->model('fakpenjualanpopup/mclass');

		$query	= $this->mclass->fldodetail($key,$ibranch);
		$jml	= $query->num_rows();
		
		$list	= "";
		$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
		
		if($jml>0) {
			
			$cc	= 1;
			$tanggal	= array();
			
			foreach($query->result() as $row) {

				$tgl	= (!empty($row->ddo) || strlen($row->ddo)!=0)?@explode("-",$row->ddo,strlen($row->ddo)):"";
				$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
				$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";

				$idocode	= trim($row->ido_code);
				$iproduct	= trim($row->iproduct);
				
				$tglfaktur	= $tgl[2]."/".$tgl[1]."/".$tgl[0];
				
				$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->ido_code."</a></td>	 
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$tanggal[$cc]."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->iproduct."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->motifname."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->qty."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->qtyakhir."</a></td>					  
					 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
				
	}
			
	function detailsimpan() {
		
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0 ) 
		{
			
			$data['page_title_fpenjualando']	= $this->lang->line('page_title_fpenjualando');
			$data['form_detail_f_fpenjualando']	= $this->lang->line('form_detail_f_fpenjualando');
			$data['form_nomor_f_fpenjualando']	= $this->lang->line('form_nomor_f_fpenjualando');
			$data['form_tgl_f_fpenjualando']	= $this->lang->line('form_tgl_f_fpenjualando');	
			$data['form_cabang_fpenjualando']	= $this->lang->line('form_cabang_fpenjualando');
			$data['form_pilih_cab_fpenjualando']	= $this->lang->line('form_pilih_cab_fpenjualando');
			$data['form_nomor_do_f_fpenjualando']	= $this->lang->line('form_nomor_do_f_fpenjualando');
			$data['form_tgl_do_f_fpenjualando']	= $this->lang->line('form_tgl_do_f_fpenjualando');
			$data['form_kd_brg_fpenjualando']	= $this->lang->line('form_kd_brg_fpenjualando');
			$data['form_nm_brg_fpenjualando']	= $this->lang->line('form_nm_brg_fpenjualando');
			$data['form_hjp_fpenjualando']	= $this->lang->line('form_hjp_fpenjualando');
			$data['form_qty_fpenjualando']	= $this->lang->line('form_qty_fpenjualando');
			$data['form_nilai_fpenjualando']	= $this->lang->line('form_nilai_fpenjualando');
			$data['form_tgl_jtempo_fpenjualando']	= $this->lang->line('form_tgl_jtempo_fpenjualando');
			$data['form_tnilai_fpenjualando']	= $this->lang->line('form_tnilai_fpenjualando');
			$data['form_diskon_fpenjualando']	= $this->lang->line('form_diskon_fpenjualando');
			$data['form_total_fpenjualando']	= $this->lang->line('form_total_fpenjualando');
			$data['form_no_fpajak_fpenjualando']	= $this->lang->line('form_no_fpajak_fpenjualando');
			$data['form_tgl_fpajak_fpenjualando']	= $this->lang->line('form_tgl_fpajak_fpenjualando');
			$data['form_ppn_fpenjualando']	= $this->lang->line('form_ppn_fpenjualando');
			$data['form_ket_cetak_fpenjualando']	= $this->lang->line('form_ket_cetak_fpenjualando');
			$data['form_grand_t_fpenjualando']	= $this->lang->line('form_grand_t_fpenjualando');
			$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
			$data['form_dlm_fpenjualando']	= $this->lang->line('form_dlm_fpenjualando');
			
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();		
			$data['tjthtempo']	= "";
			
			$List = "";
			
			$idocode	= $this->input->post('i_do_code');
			$ido		= $this->input->post('i_do');
			$iproduct	= $this->input->post('i_product');
			$ibranch	= $this->input->post('ibranch');
			$exp_ido	= explode("#",$ido,strlen($ido));
			$exp_iproduct	= explode("#",$iproduct,strlen($iproduct));
			
			$this->load->model('fakpenjualanpopup/mclass');

			$tgl	= date("d");
			$bln	= date("m");
			$tahun	= date("Y");
			
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$tahun;
			$data['tgPajak']	= $tgl."/".$bln."/".$tahun;
			
			$data['opt_cabang']	= $this->mclass->lcabang();
			
			$qryfakpajak	= $this->mclass->nofakturpajak();	
			$qryth	= $this->mclass->getthnfaktur();
			$qryfaktur	= $this->mclass->getnomorfaktur();		
			
			$qryfakpajak_non_do	= $this->mclass->nofakturpajak_non_do();	
			$qryth_non_do	= $this->mclass->getthnfaktur_non_do();
			$qryfaktur_non_do	= $this->mclass->getnomorfaktur_non_do();	

			$qryfakpajak_bhnbaku	= $this->mclass->nofakturpajak_bhnbaku();	
			$qryth_bhnbaku			= $this->mclass->getthnfaktur_bhnbaku();
			$qryfaktur_bhnbaku		= $this->mclass->getnomorfaktur_bhnbaku();
			
			if($qryfaktur->num_rows()>0){
				$row	= $qryfaktur->row_array();
				$faktur	= $row['ifaktur']+1;	
			}else{
				$faktur	= 1;
			}
			
			if($qryfaktur_non_do->num_rows()>0){
				$row2	= $qryfaktur_non_do->row_array();
				$faktur_non_do = $row2['ifaktur']+1;
			}else{
				$faktur_non_do = 1;
			}

			if($qryfaktur_bhnbaku->num_rows()>0){
				$row3	= $qryfaktur_bhnbaku->row_array();
				$faktur_bhnbaku = $row3['ifaktur']+1;
			}else{
				$faktur_bhnbaku = 1;
			}

			if($faktur > $faktur_non_do && $faktur > $faktur_bhnbaku) {
				$th	= $qryth->row_array();
				$thn	= $th['thn'];
			}elseif($faktur_non_do > $faktur && $faktur_non_do > $faktur_bhnbaku) {
				if($qryth_non_do->num_rows() > 0){
					$th	= $qryth_non_do->row_array();
					$thn	= $th['thn'];						
				}else{
					$thn	= $tahun;		
				}
			}else{
				if($qryth_bhnbaku->num_rows() > 0) {
					$th	= $qryth_bhnbaku->row_array();
					$thn	= $th['thn'];					
				}else{
					$thn	= $tahun;		
				}					
			}
			
			if($thn==$tahun) {

				if($faktur > $faktur_non_do && $faktur > $faktur_bhnbaku) {
					$row_fakpajak	= $qryfakpajak->row();
					$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
				}elseif($faktur_non_do > $faktur && $faktur_non_do > $faktur_bhnbaku) {
					if($qryfakpajak_non_do->num_rows()>0){
						$row_fakpajak	= $qryfakpajak_non_do->row();
						$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;						
					}else{
						$data['nofakturpajak']	= 1;
					}
				}else{
					if($qryfakpajak_bhnbaku->num_rows()>0){
						$row_fakpajak	= $qryfakpajak_bhnbaku->row();
						$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;						
					}else{
						$data['nofakturpajak']	= 1;
					}
				}

			}else{
				$data['nofakturpajak']	= 1;
			}

			if($thn==$tahun) {
			
				if($faktur!=1 || $faktur_non_do!=1 || $faktur_bhnbaku!=1)  {
					
					if($faktur > $faktur_non_do && $faktur > $faktur_bhnbaku) {
						$Zz	= $faktur;
					}elseif($faktur_non_do > $faktur && $faktur_non_do > $faktur_bhnbaku) {
						$Zz	= $faktur_non_do;
					}else{
						$Zz	= $faktur_bhnbaku;
					}
							
					switch(strlen($Zz)) {
						case "1": $nomorfaktur	= "000".$Zz;
						break;
						case "2": $nomorfaktur	= "00".$Zz;
						break;
						case "3": $nomorfaktur	= "0".$Zz;
						break;
						case "4": $nomorfaktur	= $Zz;
						break;
					}
				}else{
					$nomorfaktur = "0001";
				}
				$nomor	= $tahun.$nomorfaktur;
			}else{
				$nomor	= $tahun."0001";
			}
						
			$data['no']	= $nomor;
			
			$qebranchname = $this->mclass->ebranchname($ibranch);
			if($qebranchname->num_rows()>0) {
				$rebranchname	= $qebranchname->row();
				$data['einisial']	= $rebranchname->e_initial;
			}else{
				$data['einisial']	= "";
			}
			
			$iter = 0;	
			$tharga	= 0;	
			$totaln = 0;
			
			foreach($exp_ido as $ido) {
				
				$query	= $this->mclass->detailsimpan($ido,$ibranch,$exp_iproduct[$iter]);
				
				if($query->num_rows()>0) {

					$rdetail	= $query->row();

					$tharga	= $rdetail->price*$rdetail->qtyakhir;
					$totaln+=$tharga;	
													
					$List .= "
					<tr>
					<td><div style=\"font:11px/24px;text-align:right;width:18px;margin-right:0px;\">".($iter+1)."</div></td>
					<td><DIV ID=\"ajax_i_do_tblItem_".$iter."\" ><input type=\"text\" ID=\"i_do_tblItem_".$iter."\"  name=\"i_do_tblItem_".$iter."\" style=\"width:82px;\" value=\"$rdetail->ido_code\" readonly ></DIV></td>
					<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" ><input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:120px;\" value=\"$rdetail->iproduct\" readonly ></DIV></td>
					<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" ><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\"  name=\"e_product_name_tblItem_".$iter."\" style=\"width:200px;\" value=\"".strtoupper($rdetail->productname)."\" readonly ></DIV></td>
					<td><DIV ID=\"ajax_v_hjp_tblItem_".$iter."\" ><input type=\"text\" ID=\"v_hjp_tblItem_".$iter."\"  name=\"v_hjp_tblItem_".$iter."\" style=\"width:90px;text-align:right;\" value=\"$rdetail->price\" readonly ></DIV></td>
					
					<td>
						<input type='checkbox' name='is_grosir_".$iter."' id='is_grosir_".$iter."' value='y' disabled ";
					if ($rdetail->is_grosir == 't')
						$List.= " checked='true' ";
					$List.=" > <input type='hidden' name='is_grosir2_".$iter."' id='is_grosir2_".$iter."' value='$rdetail->is_grosir'>
					</td>
					
					<td><DIV ID=\"ajax_n_quantity_tblItem_".$iter."\" >
					<input type=\"text\" ID=\"n_quantity_tblItem_".$iter."\"  name=\"n_quantity_tblItem_".$iter."\" style=\"width:95px;text-align:right;\" value=\"$rdetail->qtyakhir\" onkeyup=\"validNum('n_quantity_tblItem','n_quantity_tblItem_hidden','".$iter."');total(".$iter.");test();\" >
					<input type=\"hidden\" ID=\"n_quantity_tblItem_hidden_".$iter."\" name=\"n_quantity_tblItem_hidden_".$iter."\" value=\"".$rdetail->qtyakhir."\" > 
					</DIV></td>
					<td><DIV ID=\"ajax_v_unit_price_tblItem_".$iter."\" text-align:right;\" ><input type=\"text\" ID=\"v_unit_price_tblItem_".$iter."\"  name=\"v_unit_price_tblItem_".$iter."\" style=\"width:85px;text-align:right;\" onkeyup=\"total(".$iter.");test();validNum('v_unit_price_tblItem','".$iter."');\" value=\"$rdetail->nilai\" readonly ><input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$iter."\" ></DIV></td>
					</tr>";
				}
				
				$iter++;
			}
			
			$nilai_ppn	= (($totaln*10)/100);
			$total_grand	= $totaln + $nilai_ppn;
			
			$total_sblm_ppn	= round($totaln); // DPP
			$nilai_ppn2	= round($nilai_ppn);
			$total_grand2	= round($total_grand);	
																		
			$data['List']	= $List;
			
			$data['total']	= $totaln;
			$data['total_sblm_ppn']	= $total_sblm_ppn;
			$data['nilai_ppn']	= $nilai_ppn2;
			$data['total_grand']	= $total_grand2;
			
			$this->load->view('fakpenjualanpopup/vform',$data);
		}
	}

	function listbarangjadi() {
		
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$brs		= $this->uri->segment(6);

		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['brs']		= $brs;

		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualanpopup/mclass');

		$query	= $this->mclass->lbarangjadi($ibranch);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/fakpenjualanpopup/cform/listbarangjadinext/'.$iterasi.'/'.$ibranch.'/'.$brs.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($ibranch,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('fakpenjualanpopup/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$iterasi	= $this->uri->segment(4);
		$ibranch	= $this->uri->segment(5);
		$brs		= $this->uri->segment(6);
		
		$data['iterasi']	= $iterasi;
		$data['ibranch']	= $ibranch;
		$data['brs']		= $brs;
		
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualan/mclass');

		$query	= $this->mclass->lbarangjadi($ibranch);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/fakpenjualanpopup/cform/listbarangjadinext/'.$iterasi.'/'.$ibranch.'/'.'/'.$brs.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']		= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($ibranch,$pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('fakpenjualanpopup/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$cab	= $this->input->post('cab')?$this->input->post('cab'):$this->input->get_post('cab');
		
		$iterasi	= $this->uri->segment(4,0);
		$ibranch	= $this->uri->segment(5,0);
		$brs		= $this->uri->segment(6,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualanpopup/mclass');

		$query	= $this->mclass->flbarangjadi($key,$cab);
		$jml	= $query->num_rows();
		
		$list	= "";
		$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
		
		if($jml>0) {
			$cc	= 1;
			$tanggal	= array();
			
			foreach($query->result() as $row){

				$tgl	= (!empty($row->ddo) || strlen($row->ddo)!=0)?@explode("-",$row->ddo,strlen($row->ddo)):"";
				$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
				$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";

				$idocode	= trim($row->ido_code);
				$iproduct	= trim($row->iproduct);
				
				$tglfaktur	= $tgl[2]."/".$tgl[1]."/".$tgl[0];
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$row->ido_code."</a></td>	 
				  <td width=\"130px;\"><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$tanggal[$cc]."</a></td>
				   <td><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$row->iproduct."</a></td>
				  <td><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$row->motifname."</a></td>	
				  <td><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$row->qtyakhir."</a></td>				  
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
		
	function listdetailbrgjadi() {
		
		$data['iterasi']	= $this->uri->segment(4);
		
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('fakpenjualanpopup/mclass');
		$data['isi']	= $this->mclass->lbarangjadi2();
		$this->load->view('fakpenjualanpopup/vlistformbrgjadi2',$data);	
	}
	
	function simpan() {
		
		$data['page_title_fpenjualando']	= $this->lang->line('page_title_fpenjualando');
		$data['form_detail_f_fpenjualando']	= $this->lang->line('form_detail_f_fpenjualando');
		$data['form_nomor_f_fpenjualando']	= $this->lang->line('form_nomor_f_fpenjualando');
		$data['form_tgl_f_fpenjualando']	= $this->lang->line('form_tgl_f_fpenjualando');	
		$data['form_cabang_fpenjualando']	= $this->lang->line('form_cabang_fpenjualando');
		$data['form_pilih_cab_fpenjualando']	= $this->lang->line('form_pilih_cab_fpenjualando');
		$data['form_nomor_do_f_fpenjualando']	= $this->lang->line('form_nomor_do_f_fpenjualando');
		$data['form_kd_brg_fpenjualando']	= $this->lang->line('form_kd_brg_fpenjualando');
		$data['form_nm_brg_fpenjualando']	= $this->lang->line('form_nm_brg_fpenjualando');
		$data['form_hjp_fpenjualando']	= $this->lang->line('form_hjp_fpenjualando');
		$data['form_qty_fpenjualando']	= $this->lang->line('form_qty_fpenjualando');
		$data['form_nilai_fpenjualando']	= $this->lang->line('form_nilai_fpenjualando');
		$data['form_tgl_jtempo_fpenjualando']	= $this->lang->line('form_tgl_jtempo_fpenjualando');
		$data['form_tnilai_fpenjualando']	= $this->lang->line('form_tnilai_fpenjualando');
		$data['form_diskon_fpenjualando']	= $this->lang->line('form_diskon_fpenjualando');
		$data['form_total_fpenjualando']	= $this->lang->line('form_total_fpenjualando');
		$data['form_no_fpajak_fpenjualando']	= $this->lang->line('form_no_fpajak_fpenjualando');
		$data['form_tgl_fpajak_fpenjualando']	= $this->lang->line('form_tgl_fpajak_fpenjualando');
		$data['form_ppn_fpenjualando']	= $this->lang->line('form_ppn_fpenjualando');
		$data['form_ket_cetak_fpenjualando']	= $this->lang->line('form_ket_cetak_fpenjualando');
		$data['form_grand_t_fpenjualando']	= $this->lang->line('form_grand_t_fpenjualando');
		$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
		$data['form_dlm_fpenjualando']	= $this->lang->line('form_dlm_fpenjualando');
		
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		
		$data['isi']		= "";
		$data['list']		= "";
		$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih";
		$data['limages']	= base_url();
			
		$iterasi	= $this->input->post('iteration');
		$i_faktur	= $this->input->post('i_faktur');
		$d_faktur	= $this->input->post('d_faktur');
		$i_branch	= $this->input->post('i_branch_hidden');
		$v_total_nilai	= $this->input->post('v_total_nilai');
		$n_discount	= $this->input->post('n_discount');
		$v_discount	= $this->input->post('v_discount');
		$d_due_date	= $this->input->post('d_due_date');
		$v_total_faktur	= $this->input->post('v_total_faktur');
		$i_faktur_pajak	= $this->input->post('i_faktur_pajak');
		$d_pajak	= $this->input->post('d_pajak');
		$n_ppn	= $this->input->post('n_ppn');
		$f_cetak	= $this->input->post('f_cetak');
		$v_total_fppn	= $this->input->post('v_total_fppn');
		
		$ex_d_pajak		= explode("/",$d_pajak,strlen($d_pajak)); // dd/mm/YYYY
		$ex_d_due_date	= explode("/",$d_due_date,strlen($d_due_date));
		$ex_d_faktur	= explode("/",$d_faktur,strlen($d_faktur));
		
		$nw_d_pajak	= $ex_d_pajak[2]."-".$ex_d_pajak[1]."-".$ex_d_pajak[0]; //YYYY-mm-dd
		$nw_d_due_date	= $ex_d_due_date[2]."-".$ex_d_due_date[1]."-".$ex_d_due_date[0]; //YYYY-mm-dd
		$nw_d_faktur	= $ex_d_faktur[2]."-".$ex_d_faktur[1]."-".$ex_d_faktur[0]; //YYYY-mm-dd
		
		// 28-06-2012, no need this anymore, langsung aja ambil dari form inputan
		/*$ex_v_total_faktur	= explode(".",$v_total_faktur,strlen($v_total_faktur));
		$ex_v_total_fppn	= explode(".",$v_total_fppn,strlen($v_total_fppn));
		$ex_v_discount	= explode(".",$v_discount,strlen($v_discount));
		
		$nw_v_total_faktur	= $ex_v_total_faktur[0];
		$nw_v_total_fppn	= $ex_v_total_fppn[0];
		$nw_v_discount		= $ex_v_discount[0]; */
		
		$i_do	= array();
		$i_product	= array();
		$e_product_name	= array();
		$v_hjp	= array();
		$n_quantity	= array();
		$v_unit_price	= array();
		$is_grosir2	= array();
		
		$i_do_0	= $this->input->post('i_do'.'_'.'tblItem'.'_'.'0');
		
		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$i_do[$cacah]	= $this->input->post('i_do'.'_'.'tblItem'.'_'.$cacah);
			$i_product[$cacah]	= $this->input->post('i_product'.'_'.'tblItem'.'_'.$cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name'.'_'.'tblItem'.'_'.$cacah);
			$v_hjp[$cacah]	= $this->input->post('v_hjp'.'_'.'tblItem'.'_'.$cacah);
			//is_grosir2_".$iter."'
			$is_grosir2[$cacah]	= $this->input->post('is_grosir2_'.$cacah);
			
			$n_quantity[$cacah]	= $this->input->post('n_quantity'.'_'.'tblItem'.'_'.$cacah);
			$v_unit_price[$cacah]	= $this->input->post('v_unit_price'.'_'.'tblItem'.'_'.$cacah);
		}
		
		$this->load->model('fakpenjualanpopup/mclass');
		
		if(!empty($i_faktur) &&
		   !empty($d_faktur) && 
		   !empty($i_branch)
		) {

				if(!empty($i_do_0)) {
					$qnsop	= $this->mclass->cari_fpenjualan($i_faktur);
					if($qnsop->num_rows()==0) {
						/*$this->mclass->msimpan($i_faktur,$nw_d_faktur,$i_branch,$v_total_nilai,$n_discount,$nw_v_discount,
								$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,
								$nw_v_total_fppn,$i_do,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iterasi,
								$is_grosir2); */
						$this->mclass->msimpan($i_faktur,$nw_d_faktur,$i_branch,$v_total_nilai,$n_discount,$v_discount,
								$nw_d_due_date,$v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,
								$v_total_fppn,$i_do,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iterasi,
								$is_grosir2);
					}else{
						print "<script>alert(\"Maaf, Nomor Faktur tsb sebelumnya telah diinput. Terimakasih.\");show(\"fakpenjualanpopup/cform\",\"#content\");</script>";
					}
				}else{
					print "<script>alert(\"Maaf, item Faktur Penjulan hrs terisi. Terimakasih.\");show(\"fakpenjualanpopup/cform\",\"#content\");</script>";
				}

		}else{
			
			$data['page_title_fpenjualando']	= $this->lang->line('page_title_fpenjualando');
			$data['form_detail_f_fpenjualando']	= $this->lang->line('form_detail_f_fpenjualando');
			$data['form_nomor_f_fpenjualando']	= $this->lang->line('form_nomor_f_fpenjualando');
			$data['form_tgl_f_fpenjualando']	= $this->lang->line('form_tgl_f_fpenjualando');	
			$data['form_cabang_fpenjualando']	= $this->lang->line('form_cabang_fpenjualando');
			$data['form_pilih_cab_fpenjualando']	= $this->lang->line('form_pilih_cab_fpenjualando');
			$data['form_nomor_do_f_fpenjualando']	= $this->lang->line('form_nomor_do_f_fpenjualando');
			$data['form_tgl_do_f_fpenjualando']	= $this->lang->line('form_tgl_do_f_fpenjualando');
			$data['form_kd_brg_fpenjualando']	= $this->lang->line('form_kd_brg_fpenjualando');
			$data['form_nm_brg_fpenjualando']	= $this->lang->line('form_nm_brg_fpenjualando');
			$data['form_hjp_fpenjualando']	= $this->lang->line('form_hjp_fpenjualando');
			$data['form_qty_fpenjualando']	= $this->lang->line('form_qty_fpenjualando');
			$data['form_nilai_fpenjualando']	= $this->lang->line('form_nilai_fpenjualando');
			$data['form_tgl_jtempo_fpenjualando']	= $this->lang->line('form_tgl_jtempo_fpenjualando');
			$data['form_tnilai_fpenjualando']	= $this->lang->line('form_tnilai_fpenjualando');
			$data['form_diskon_fpenjualando']	= $this->lang->line('form_diskon_fpenjualando');
			$data['form_total_fpenjualando']	= $this->lang->line('form_total_fpenjualando');
			$data['form_no_fpajak_fpenjualando']	= $this->lang->line('form_no_fpajak_fpenjualando');
			$data['form_tgl_fpajak_fpenjualando']	= $this->lang->line('form_tgl_fpajak_fpenjualando');
			$data['form_ppn_fpenjualando']	= $this->lang->line('form_ppn_fpenjualando');
			$data['form_ket_cetak_fpenjualando']	= $this->lang->line('form_ket_cetak_fpenjualando');
			$data['form_grand_t_fpenjualando']	= $this->lang->line('form_grand_t_fpenjualando');
			$data['form_nilai_fpenjualanndo']	= $this->lang->line('form_nilai_fpenjualanndo');
			$data['form_dlm_fpenjualando']	= $this->lang->line('form_dlm_fpenjualando');
			
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();		
			$data['tjthtempo']	= "";

			$tgl	= date("d");
			$bln	= date("m");
			$tahun	= date("Y");
			
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgFaktur']	= $tgl."/".$bln."/".$tahun;
			$data['tgPajak']	= $tgl."/".$bln."/".$tahun;
			
			$qryfakpajak	= $this->mclass->nofakturpajak();	
			$qryth	= $this->mclass->getthnfaktur();
			$qryfaktur	= $this->mclass->getnomorfaktur();		
		
			$data['opt_cabang']	= $this->mclass->lcabang();

			if($qryth->num_rows() > 0) {
				$th		= $qryth->row_array();
				$thn	= $th['thn'];
			}else{
				$thn	= $tahun;
			}
			
			if($thn==$tahun){
				if($qryfakpajak->num_rows()>0) {
					$row_fakpajak	= $qryfakpajak->row();
					$data['nofakturpajak']	= $row_fakpajak->nofakturpajak;
				}else{
					$data['nofakturpajak']	= 1;
				}
			}else{
				$data['nofakturpajak']	= 1;
			}
			
			if($thn==$tahun) {
				if($qryfaktur->num_rows() > 0) {
					
					$row	= $qryfaktur->row_array();
					$faktur		= $row['ifaktur']+1;		
					
					switch(strlen($faktur)) {
						case "1": $nomorfaktur	= "000".$faktur;
						break;
						case "2": $nomorfaktur	= "00".$faktur;
						break;	
						case "3": $nomorfaktur	= "0".$faktur;
						break;
						case "4": $nomorfaktur	= $faktur;
						break;
					}
				}else{
					$nomorfaktur = "0001";
				}
				$nomor	= $tahun.$nomorfaktur;
			}else{
				$nomor	= $tahun."0001";
			}
			
			$data['no']	= $nomor;

			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"fakpenjualanpopup/cform\",\"#content\");</script>";

		}
	}
	
	function cari_fpenjualan() {
		
		$fpenj	= $this->input->post('fpenj')?$this->input->post('fpenj'):$this->input->get_post('fpenj');
		
		$this->load->model('fakpenjualanpopup/mclass');
		
		$qnsop	= $this->mclass->cari_fpenjualan($fpenj);
		
		if($qnsop->num_rows()>0) {
			echo "Maaf, No. Faktur sudah ada!";
		}
	}
	
	function cari_fpajak() {
		
		$tahun	= date("Y");
		$fpajak	= $this->input->post('fpajak')?$this->input->post('fpajak'):$this->input->get_post('fpajak');
		
		$this->load->model('fakpenjualanpopup/mclass');
		
		$qnsop	= $this->mclass->cari_fpajak($fpajak,$tahun);
		if($qnsop->num_rows()>0) {
			echo "Maaf, No. Pajak sudah ada!";
		}	
	}
}
?>
