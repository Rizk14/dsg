<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-mutasi-stok/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-mutasi-stok/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vformview';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }
  
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='9' align='center'>LAPORAN MUTASI STOK BAHAN BAKU/PEMBANTU</th>
		 </tr>
		 <tr>
			<th colspan='9' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='9' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%'>No</th>
			 <th width='20%'>Kode Brg</th>
			 <th width='30%'>Nama Barang</th>
			 <th width='15%'>Sat Awal</th>
			 <th width='10%'>Masuk</th>
			 <th width='10%'>Masuk Lain</th>
			 <th width='10%'>Keluar</th>
			 <th width='10%'>Keluar Lain</th>
			 <th width='10%'>Saldo Akhir</th>
		 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td>".$query[$j]['satuan']."</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain']."</td>
				 <td align='right'>".$query[$j]['jum_keluar']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain']."</td> ";
				$saldo_akhir = $query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				$html_data.= "<td align='right'>".$saldo_akhir."</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../dutaproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }
  
  // ++++++++++++++++++ 14-03-2013, mutasi brg WIP ++++++++++++++++++++++++++++
  function wip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['isi'] = 'info-mutasi-stok/vmainformwip';
	$this->load->view('template',$data);

  }
  
  function viewwip(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-mutasi-stok/vformviewwip';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }
  
  function export_excel_wip() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='15' align='center'>LAPORAN MUTASI STOK BARANG WIP</th>
		 </tr>
		 <tr>
			<th colspan='15' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='15' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%' rowspan='2'>No</th>
			 <th width='20%' rowspan='2'>Kode Brg</th>
			 <th width='30%' rowspan='2'>Nama Brg</th>
			 <th width='10%' rowspan='2'>Satuan</th>
			 <th width='10%' rowspan='2'>Masuk</th>
			 <th colspan='4'>Masuk Lain</th>
			 <th colspan='2'>Keluar</th>
			 <th colspan='2'>Keluar Lain</th>
			 <th width='10%' rowspan='2'>Saldo Akhir</th>
			 <th width='10%' rowspan='2'>Stok Opname</th>
		 </tr>
		 <tr>
			 <th width='10%'>Perbaikan Unit Jht</th>
			 <th width='10%'>Retur dr Packing</th>
			 <th width='10%'>Retur dr Gd Jadi</th>
			 <th width='10%'>Lainnya</th>
			 <th width='10%'>Gd Jadi</th>
			 <th width='10%'>Packing</th>
			 <th width='10%'>Retur ke Unit Jht</th>
			 <th width='10%'>Lainnya</th>
		 </tr>
		</thead>
		<tbody>";
		
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_brg']."</td>
				 <td>".$query[$j]['nama_brg']."</td>
				 <td>Pieces</td>
				 <td align='right'>".$query[$j]['jum_masuk']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain1']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain2']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain3']."</td>
				 <td align='right'>".$query[$j]['jum_masuk_lain4']."</td>
				 <td align='right'>".$query[$j]['jum_keluar1']."</td>
				 <td align='right'>".$query[$j]['jum_keluar2']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain1']."</td>
				 <td align='right'>".$query[$j]['jum_keluar_lain2']."</td>";
				$saldo_akhir = $query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain1']+$query[$j]['jum_masuk_lain2']+$query[$j]['jum_masuk_lain3']+$query[$j]['jum_masuk_lain4']-$query[$j]['jum_keluar1']-$query[$j]['jum_keluar2']-$query[$j]['jum_keluar_lain1']-$query[$j]['jum_keluar_lain2'];
				$html_data.= "<td align='right'>".$saldo_akhir."</td>
				<td align='right'>0</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_mutasi_stok_wip";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../dutaproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
  }

}
