<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-rekap-pembelian-persup/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'info-rekap-pembelian-persup/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'info-rekap-pembelian-persup/vformview';
    $jenis_beli = $this->input->post('jenis_beli', TRUE);
   
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	if ($date_from == '' && $date_to == '') {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
	}

	$data['query'] = $this->mmaster->get_all_pembelian($jenis_beli, $date_from, $date_to);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['jenis_beli'] = $jenis_beli;
	$this->load->view('template',$data);
  }
  
  // 26-04-2012
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$jenis_beli = $this->input->post('jenis_beli', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_pembelian($jenis_beli, $date_from, $date_to);
		if ($jenis_beli == '1')
			$nama_jenis = "Cash";
		else
			$nama_jenis = "Kredit";
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='10' align='center'>REKAP PEMBELIAN PER SUPPLIER</th>
		 </tr>
		 <tr>
			<th colspan='10' align='center'>Jenis Pembelian: $nama_jenis</th>
		 </tr>
		 <tr>
			<th colspan='10' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th width='5%' rowspan='2'>No</th>
			 <th width='10%' rowspan='2'>Kode Supplier</th>
			 <th width='30%' rowspan='2'>Nama Supplier</th>
			 <th width='10%' colspan='3'>Bahan Baku</th>
			 <th width='10%' colspan='3'>Bahan Pembantu</th>
			 <th width='15%' rowspan='2'>Jumlah</th>
		 </tr>
		 <tr>
			<th>Pembelian</th>
			<th>DPP</th>
			<th>PPN</th>
			<th>Pembelian</th>
			<th>DPP</th>
			<th>PPN</th>
		 </tr>
		</thead>
		<tbody>";
		
			$total_baku = 0;
			$total_pembantu = 0;
			$total_semua = 0;
			if (is_array($query)) {
				$total_baku = 0;
				$total_pembantu = 0;
				$total_semua = 0;
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_supplier']."</td>
				 <td>".$query[$j]['nama_supplier']."</td>
				 <td align='right'>".$query[$j]['jum_total_baku']."</td>
				 <td align='right'>".$query[$j]['dpp_baku']."</td>
				 <td align='right'>".$query[$j]['ppn_baku']."</td>
				 <td align='right'>".$query[$j]['jum_total_pembantu']."</td>
				 <td align='right'>".$query[$j]['dpp_pembantu']."</td>
				 <td align='right'>".$query[$j]['ppn_pembantu']."</td>";

				 $totalnya = $query[$j]['jum_total_baku'] + $query[$j]['jum_total_pembantu'];
				 $html_data.=   "<td align='right'>".$totalnya."</td>";
				 $total_baku += $query[$j]['jum_total_baku'];
				 $total_pembantu += $query[$j]['jum_total_pembantu'];
				 $total_semua += $totalnya;
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><b>TOTAL</b></td>
			<td align='right' colspan='3'><b>".$total_baku."</b></td>
			<td align='right' colspan='3'><b>".$total_pembantu."</b></td>
			<td align='right'><b>".$total_semua."</b></td>
		 </tr></tbody>
		</table>";

		$nama_file = "rekap_pembelian_persupplier";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }

}
