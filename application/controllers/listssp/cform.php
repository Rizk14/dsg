<?php
include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}		
			$data['page_title_spp']	= $this->lang->line('page_title_spp');
			$data['form_title_detail_ssp']	= $this->lang->line('form_title_detail_ssp');
			$data['list_laporansok_nmwp_ssp']	= $this->lang->line('list_laporansok_nmwp_ssp');
			$data['list_laporansok_akunpajak_ssp']	= $this->lang->line('list_laporansok_akunpajak_ssp');
			$data['list_laporansok_jns_setor_spp']	= $this->lang->line('list_laporansok_jns_setor_spp');
			$data['list_laporansok_jmlbayar_spp']	= $this->lang->line('list_laporansok_jmlbayar_spp');
			$data['list_laporansok_penyetor_ssp']	= $this->lang->line('list_laporansok_penyetor_ssp');
			$data['list_laporansok_urai_spp']		= $this->lang->line('list_laporansok_urai_spp');
			$data['button_batal']					= $this->lang->line('button_batal');
			$data['button_detail']					= $this->lang->line('button_detail');		
			$data['detail']							= "";
			$data['list']							= "";
			$data['limages']						= base_url();
			
			$blnarr	= array(
				"01"=>"Januari",
				"02"=>"Februari",
				"03"=>"Maret",
				"04"=>"April",
				"05"=>"Mei",
				"06"=>"Juni",
				"07"=>"Juli",
				"08"=>"Agustus",
				"09"=>"September",
				"10"=>"Oktober",
				"11"=>"Nopember",
				"12"=>"Desember");
				
			$cari	= $this->input->post('cari')?$this->input->post('cari'):$this->uri->segment(4);
			
			$uri1	= ($cari!='')?($cari):('kosong');
			
			$this->load->model('listssp/mclass');
				
			$query	= $this->mclass->view($cari);
			$jml	= $query->num_rows();
			
			$data['cari']		= ($cari!='' && $cari!='kosong')?($cari):('');
	
			$pagination['base_url'] 	= '/listssp/cform/index/'.$uri1.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(5,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page'],$cari);
					
			$this->load->model('listssp/mclass');
			$data['isi']	= 'listssp/vlistform';	
			$this->load->view('template',$data);	
			
		}	
	

	function actdelete() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$db2=$this->load->database('db_external', TRUE);
		$this->load->model('listssp/mclass');
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$db2->query(" UPDATE tm_ssp SET f_ssp_cancel='t' WHERE i_ssp='$id' ");
		redirect('listssp/cform/');
	}	
	
}

?>
