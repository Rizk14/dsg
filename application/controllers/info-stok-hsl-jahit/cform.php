<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-stok-hsl-jahit/mmaster');
  }
  
  function view(){
    $data['isi'] = 'info-stok-hsl-jahit/vformview';
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$gudang 	= $this->input->post('gudang', TRUE);  
	$cstatus = $this->input->post('statusnya', TRUE);
	//$export_excel1 	= $this->input->post('export_excel', TRUE);
	//$export_ods1 	= $this->input->post('export_ods', TRUE);
	
	if ($keywordcari == '' && ($gudang == '' || $cstatus == '')) {
		$gudang 	= $this->uri->segment(5);
		$cstatus 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	//if ($gudang == '')
	//	$gudang = 22;
	if ($gudang == '')
		$gudang = 0;
	
	if ($cstatus == '')
		$cstatus = 't';
	
	//if ($export_excel1 == "" && $export_ods1 == "") {
		$jum_total = $this->mmaster->get_stoktanpalimit($gudang, $cstatus, $keywordcari);

				$config['base_url'] = base_url().'index.php/info-stok-hsl-jahit/cform/view/index/'.$gudang.'/'.$cstatus.'/'.$keywordcari.'/';
								//$config['total_rows'] = $query->num_rows(); 
								$config['total_rows'] = count($jum_total); 
								$config['per_page'] = '10';
								$config['first_link'] = 'Awal';
								$config['last_link'] = 'Akhir';
								$config['next_link'] = 'Selanjutnya';
								$config['prev_link'] = 'Sebelumnya';
								$config['cur_page'] = $this->uri->segment(8);
								$this->pagination->initialize($config);
							
		$data['query'] = $this->mmaster->get_stok($config['per_page'],$this->uri->segment(8), $gudang, $cstatus, $keywordcari);
		$data['jum_total'] = count($jum_total);
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['cgudang'] = $gudang;
		$data['cstatus'] = $cstatus;
		$data['startnya'] = $config['cur_page'];
		$this->load->view('template',$data);
	//}
	/*else {
		$query = $this->mmaster->get_stok_gudang_setengah_jadi($gudang, $keywordcari);
		//print_r($data_export); die();
		//print_r($query); die();
		$tgl_skrg = date("d-m-Y");
		
		if ($gudang != 0) {
			$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
					$lokasi_gabung = "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang;
		}
		else {
			$lokasi_gabung = "Semua";
		}
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='8' align='center'>DATA STOK $tgl_skrg</th>
		 </tr>
		 <tr>
			<th colspan='8' align='center'>Lokasi Gudang: ".$lokasi_gabung."</th>
		 </tr>
		 <tr>
			 <th width='5%' rowspan='2'>No</th>
			 <th width='10%' rowspan='2'>Kelompok Brg</th>
			 <th width='10%' rowspan='2'>Kode</th>
			 <th width='30%' rowspan='2'>Nama Barang</th>
			 <th width='7%' rowspan='2'>Gudang Jadi</th>
			 <th colspan='3'>Gudang 1/2 Jadi</th>
		 </tr>
		 <tr>
			 <th width='7%'>Per Warna</th>
			 <th width='7%'>Total</th>
			 <th width='7%'>Pack</th>
		 </tr>
		</thead>
		<tbody>";
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr class=\"record\">
				 <td align='center'>".($j+1)."</td>
				 <td>".$query[$j]['kode_kel']." - ".$query[$j]['nama_kel']."</td>
				 <td>".$query[$j]['kode_brg_jadi']."</td>
				 <td>".$query[$j]['nama_brg_jadi']."</td>
				 <td align='right'>".$query[$j]['stok_gudang_jadi']."</td>
				 <td align='right'>";
				 
				 if (is_array($query[$j]['detailwarna'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailwarna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['nama_warna']." : ".$var_detail[$k]['stok'];
						  if ($k<$hitung-1)
						     $html_data.= "<br> ";
					}
				 }
				 
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right'>".$query[$j]['stok']."</td>
				 <td align='right'>&nbsp;</td>";
				 $html_data.=  "</tr>";					
		 	}
		   }
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan_stok";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		include("../blnproduksi/system/application/libraries/generateExcelFile.php"); 
		return true;
	} // end if
	*/
  }
  
  // 13-02-2014, stok di unit jahit
  function viewstokunit(){
    $data['isi'] = 'info-stok-hsl-jahit/vformviewstokunit';
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$unit_jahit 	= $this->input->post('id_unit_jahit', TRUE); 
	$cstatus = $this->input->post('statusnya', TRUE);
	
	if ($keywordcari == '' && ($unit_jahit == '' || $cstatus == '')) {
		$unit_jahit 	= $this->uri->segment(5);
		$cstatus 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($unit_jahit == '')
		$unit_jahit = 0;
	if ($cstatus == '')
		$cstatus = 't';

    $jum_total = $this->mmaster->get_stokunittanpalimit($unit_jahit, $cstatus, $keywordcari);

			$config['base_url'] = base_url().'index.php/info-stok-hsl-jahit/cform/viewstokunit/index/'.$unit_jahit.'/'.$cstatus.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mmaster->get_stokunit($config['per_page'],$this->uri->segment(8), $unit_jahit, $cstatus, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['cunit_jahit'] = $unit_jahit;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  // 06-11-2015 STOK UNIT PACKING
  function viewstokunitpacking(){
    $data['isi'] = 'info-stok-hsl-jahit/vformviewstokunitpacking';
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$unit_packing 	= $this->input->post('id_unit_packing', TRUE); 
	$cstatus = $this->input->post('statusnya', TRUE);
	
	if ($keywordcari == '' && ($unit_packing == '' || $cstatus == '')) {
		$unit_packing 	= $this->uri->segment(5);
		$cstatus 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($unit_packing == '')
		$unit_packing = 0;
	if ($cstatus == '')
		$cstatus = 't';

    $jum_total = $this->mmaster->get_stokunitpackingtanpalimit($unit_packing, $cstatus, $keywordcari);

			$config['base_url'] = base_url().'index.php/info-stok-hsl-jahit/cform/viewstokunitpacking/index/'.$unit_packing.'/'.$cstatus.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mmaster->get_stokunitpacking($config['per_page'],$this->uri->segment(8), $unit_packing, $cstatus, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['cunit_packing'] = $unit_packing;
	$data['cstatus'] = $cstatus;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  // 14-02-2014, update kode unit di tm_stok_unit_jahit utk kode < 10
  function editkodeunitdibawah10(){
	  $sqlxx = $this->db->query(" SELECT * FROM tm_stok_unit_jahit ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				if ($rowxx->kode_unit < 10) {
					$kodebaru = "0".$rowxx->kode_unit;
					$this->db->query(" UPDATE tm_stok_unit_jahit SET kode_unit = '$kodebaru' WHERE id = '".$rowxx->id."' ");
				}
			} // end foreach
	  }
	  echo "edit kode unit jahit di tm_stok_unit_jahit yg kodenya < 10 sukses <br>";
	  echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
  }
  
  // 28-08-2014, fungsi utk hapus data2 stok yg gudangnya selain 1/2 jadi
  function hapus_stok_wip_gudanglain(){
	  $this->db->query(" delete from tm_stok_hasil_jahit_warna where id_stok_hasil_jahit in (select id from tm_stok_hasil_jahit where id_gudang <> '22') ");
	  $this->db->query(" delete from tm_stok_hasil_jahit where id_gudang <> '22' ");
	  echo "sukses hapus";
  }
  
  // 04-04-2015, fungsi utk edit data kode2 baru ke kode2 lama. setelah itu kode lama tsb diupdate semua ke kode baru
  //MBG201000 = MBG7001,  BJT101500 = BJT9001 ,  BJG301500 = BJG700100
  function editkodebrgjadibaru() {
	  // A. ambil stok2 utk kode2 baru
	/*  $sqlxx = $this->db->query(" SELECT * FROM tm_stok_unit_jahit WHERE kode_brg_jadi = 'MBG700100' OR kode_brg_jadi='BJT900100'
								OR kode_brg_jadi='BJG700100' ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$kode_brg_jadi = $rowxx->kode_brg_jadi;
				$id_stok_unit_jahit = $rowxx->id;
				$kode_unit = $rowxx->kode_unit;
				$stok = $rowxx->stok;
				$stok_bagus = $rowxx->stok_bagus;
				$stok_perbaikan = $rowxx->stok_perbaikan;
				//echo $kode_brg_jadi." : ".$stok." ".$stok_bagus." ".$stok_perbaikan."<br>";
				
				if ($kode_brg_jadi == 'MBG700100')
					$kode_brg_jadi_lama = 'MBG201000';
				else if ($kode_brg_jadi == 'BJT900100')
					$kode_brg_jadi_lama = 'BJT101500';
				else if ($kode_brg_jadi == 'BJG700100')
					$kode_brg_jadi_lama = 'BJG301500';
				
				$sqlxx2 = $this->db->query(" SELECT * FROM tm_stok_unit_jahit_warna WHERE id_stok_unit_jahit = '".$id_stok_unit_jahit."' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						$kode_warna = $rowxx2->kode_warna;
						if ($kode_brg_jadi == 'MBG700100' && $kode_warna == '35')
							$kode_warna_lama = '51';
						else if ($kode_brg_jadi == 'BJT900100' && $kode_warna == '18')
							$kode_warna_lama = '16';
						else if ($kode_brg_jadi == 'BJG700100' && $kode_warna == '18')
							$kode_warna_lama = '16';
						else
							$kode_warna_lama = $kode_warna;
						
						$stok2 = $rowxx2->stok;
						$stok_bagus2 = $rowxx2->stok_bagus;
						$stok_perbaikan2 = $rowxx2->stok_perbaikan;
						
						// tambahkan stok2nya ke kode lama
						// 1. ambil stok terkini by warna dgn kode lama
						$query3	= $this->db->query(" SELECT a.id, b.stok, b.stok_bagus, b.stok_perbaikan FROM tm_stok_unit_jahit a
									INNER JOIN tm_stok_unit_jahit_warna b ON a.id = b.id_stok_unit_jahit
									WHERE a.kode_unit='".$kode_unit."' AND a.kode_brg_jadi='".$kode_brg_jadi_lama."' 
									AND b.kode_warna='".$kode_warna_lama."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$stok_lama2	= $hasilrow->stok;
							$stok_bagus_lama2	= $hasilrow->stok_bagus;
							$stok_perbaikan_lama2	= $hasilrow->stok_perbaikan;
							$id_stok_unit_jahit2	= $hasilrow->id;
							
							$stok_baru2 = $stok_lama2 + $stok2;
							$stok_bagus_baru2 = $stok_bagus_lama2 + $stok_bagus2;
							$stok_perbaikan_baru2 = $stok_perbaikan_lama2 + $stok_perbaikan2;
							
							// 2. update stok per warna ke kode lama
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok='$stok_baru2', 
										stok_bagus='$stok_bagus_baru2', stok_perbaikan='$stok_perbaikan_baru2'
										WHERE kode_warna = '$kode_warna_lama' AND id_stok_unit_jahit='$id_stok_unit_jahit2' ");
							// 3. dan stok kode baru = 0
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok='0', 
										stok_bagus='0', stok_perbaikan='0'
										WHERE kode_warna = '$kode_warna' AND id_stok_unit_jahit='$id_stok_unit_jahit' "); 
						} // end if ambil stok kode lama
					}
				} // end if stok warna
				
				// 4. ambil stok terkini kode lama
				$query3	= $this->db->query(" SELECT id, stok, stok_bagus, stok_perbaikan FROM tm_stok_unit_jahit
									WHERE kode_unit='".$kode_unit."' AND kode_brg_jadi='".$kode_brg_jadi_lama."' ");
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
					$stok_bagus_lama	= $hasilrow->stok_bagus;
					$stok_perbaikan_lama	= $hasilrow->stok_perbaikan;
					$id_stok_unit_jahit2	= $hasilrow->id;
						
					$stok_baru = $stok_lama + $stok;
					$stok_bagus_baru = $stok_bagus_lama + $stok_bagus;
					$stok_perbaikan_baru = $stok_perbaikan_lama + $stok_perbaikan;
							
					// 2. update stok per warna ke kode lama
					$this->db->query(" UPDATE tm_stok_unit_jahit SET stok='$stok_baru', 
								stok_bagus='$stok_bagus_baru', stok_perbaikan='$stok_perbaikan_baru'
								WHERE id='$id_stok_unit_jahit2' ");
					// 3. dan stok kode baru = 0
					$this->db->query(" UPDATE tm_stok_unit_jahit SET stok='0', 
								stok_bagus='0', stok_perbaikan='0'
								WHERE id='$id_stok_unit_jahit' "); 
				}
				
				// 5. hapus data kode baru dari stok per warna
				$this->db->query(" DELETE FROM tm_stok_unit_jahit_warna WHERE id_stok_unit_jahit='$id_stok_unit_jahit' ");
				// -------------------------------------------------------------
			} // end foreach
			// 6. hapus data kode baru dari stok unit jahit
			$this->db->query(" DELETE FROM tm_stok_unit_jahit WHERE kode_brg_jadi = 'MBG700100' OR kode_brg_jadi='BJT900100'
								OR kode_brg_jadi='BJG700100' ");
			
	  } // end if PROSES A
	  
	  // B. UPDATE SEMUA KODE2 LAMA MENJADI KODE BARU di tabel bonmkeluarcutting_detail.
	  // yg proses A hrs dikomen dulu karena utk hapus data2 stok kode baru sebelumnya. ga usah. langsung aja lanjut
		if ($kode_brg_jadi == 'MBG700100')
			$kode_brg_jadi_lama = 'MBG201000';
		else if ($kode_brg_jadi == 'BJT900100')
			$kode_brg_jadi_lama = 'BJT101500';
		else if ($kode_brg_jadi == 'BJG700100')
			$kode_brg_jadi_lama = 'BJG301500';
	  
		// 1. di data kode baru, ganti kode warnanya utk yg tidak sesuai
		$sqlxx = $this->db->query(" select * from tm_bonmkeluarcutting_detail where kode_brg_jadi='MBG700100'
								OR kode_brg_jadi='BJT900100' OR kode_brg_jadi='BJG700100' ");
		if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$kode_brg_jadi = $rowxx->kode_brg_jadi;
				$id_bonmkeluarcutting_detail = $rowxx->id;
				
				$sqlxx2 = $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail_warna WHERE id_bonmkeluarcutting_detail = '".$id_bonmkeluarcutting_detail."' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						$kode_warna = $rowxx2->kode_warna;
						$id_bonmkeluarcutting_detail_warna = $rowxx2->id;
						if ($kode_brg_jadi == 'MBG700100' && $kode_warna == '35') {
							$kode_warna_lama = '51';
							$this->db->query(" UPDATE tm_bonmkeluarcutting_detail_warna SET kode_warna='$kode_warna_lama'
												WHERE id='$id_bonmkeluarcutting_detail_warna' ");
						}
						else if ($kode_brg_jadi == 'BJT900100' && $kode_warna == '18') {
							$kode_warna_lama = '16';
							$this->db->query(" UPDATE tm_bonmkeluarcutting_detail_warna SET kode_warna='$kode_warna_lama'
												WHERE id='$id_bonmkeluarcutting_detail_warna' ");
						}
						else if ($kode_brg_jadi == 'BJG700100' && $kode_warna == '18') {
							$kode_warna_lama = '16';
							$this->db->query(" UPDATE tm_bonmkeluarcutting_detail_warna SET kode_warna='$kode_warna_lama'
												WHERE id='$id_bonmkeluarcutting_detail_warna' ");
						}
						//else
						//	$kode_warna_lama = $kode_warna;
					}
				}
			} // end for utama
		} // end if utama
	  
	  // 2. update kode brg lama ke kode baru
	  $this->db->query(" UPDATE tm_bonmkeluarcutting_detail SET kode_brg_jadi='MBG700100' WHERE kode_brg_jadi='MBG201000' ");
	  $this->db->query(" UPDATE tm_sjmasukwip_detail SET kode_brg_jadi='MBG700100' WHERE kode_brg_jadi='MBG201000' ");
	  $this->db->query(" UPDATE tm_sjkeluarwip_detail SET kode_brg_jadi='MBG700100' WHERE kode_brg_jadi='MBG201000' ");
	  $this->db->query(" UPDATE tm_sjmasukbhnbakupic_detail SET kode_brg_jadi='MBG700100' WHERE kode_brg_jadi='MBG201000' ");
	  
	  $this->db->query(" UPDATE tm_bonmkeluarcutting_detail SET kode_brg_jadi='BJT900100' WHERE kode_brg_jadi='BJT101500' ");
	  $this->db->query(" UPDATE tm_sjmasukwip_detail SET kode_brg_jadi='BJT900100' WHERE kode_brg_jadi='BJT101500' ");
	  $this->db->query(" UPDATE tm_sjkeluarwip_detail SET kode_brg_jadi='BJT900100' WHERE kode_brg_jadi='BJT101500' ");
	  $this->db->query(" UPDATE tm_sjmasukbhnbakupic_detail SET kode_brg_jadi='BJT900100' WHERE kode_brg_jadi='BJT101500' ");
	  
	  $this->db->query(" UPDATE tm_bonmkeluarcutting_detail SET kode_brg_jadi='BJG700100' WHERE kode_brg_jadi='BJG301500' ");
	  $this->db->query(" UPDATE tm_sjmasukwip_detail SET kode_brg_jadi='BJG700100' WHERE kode_brg_jadi='BJG301500' ");
	  $this->db->query(" UPDATE tm_sjkeluarwip_detail SET kode_brg_jadi='BJG700100' WHERE kode_brg_jadi='BJG301500' ");
	  $this->db->query(" UPDATE tm_sjmasukbhnbakupic_detail SET kode_brg_jadi='BJG700100' WHERE kode_brg_jadi='BJG301500' ");
	  */
	  $this->db->query(" UPDATE tm_stok_unit_jahit SET kode_brg_jadi='MBG700100' WHERE kode_brg_jadi='MBG201000' ");
	  $this->db->query(" UPDATE tm_stok_hasil_jahit SET kode_brg_jadi='MBG700100' WHERE kode_brg_jadi='MBG201000' ");
	  
	  $this->db->query(" UPDATE tm_stok_unit_jahit SET kode_brg_jadi='BJT900100' WHERE kode_brg_jadi='BJT101500' ");
	  $this->db->query(" UPDATE tm_stok_hasil_jahit SET kode_brg_jadi='BJT900100' WHERE kode_brg_jadi='BJT101500' ");
	  
	  $this->db->query(" UPDATE tm_stok_unit_jahit SET kode_brg_jadi='BJG700100' WHERE kode_brg_jadi='BJG301500' ");
	  $this->db->query(" UPDATE tm_stok_hasil_jahit SET kode_brg_jadi='BJG700100' WHERE kode_brg_jadi='BJG301500' ");
	  
	  // next: kode lama dihapus. ntar senin. 06-04-2015: udah, kode lama dinonaktifkan
	  echo "sukses";
  }
  
  // 07-04-2015. update di tabel stok hasil cutting
  //MBG201000 = MBG7001,  BJT101500 = BJT9001 ,  BJG301500 = BJG700100
  function editkodebrgjadibaruhasilcutting() {
	  // A. ambil stok2 utk kode2 baru
	  $sqlxx = $this->db->query(" SELECT * FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = 'MBG700100' OR kode_brg_jadi='BJT900100'
								OR kode_brg_jadi='BJG700100' ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$kode_brg_jadi = $rowxx->kode_brg_jadi;
				$id_stok_hasil_cutting = $rowxx->id;
				$stok = $rowxx->stok;
				
				if ($kode_brg_jadi == 'MBG700100')
					$kode_brg_jadi_lama = 'MBG201000';
				else if ($kode_brg_jadi == 'BJT900100')
					$kode_brg_jadi_lama = 'BJT101500';
				else if ($kode_brg_jadi == 'BJG700100')
					$kode_brg_jadi_lama = 'BJG301500';
				
				$sqlxx2 = $this->db->query(" SELECT * FROM tm_stok_hasil_cutting_warna WHERE id_stok_hasil_cutting = '".$id_stok_hasil_cutting."' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						$kode_warna = $rowxx2->kode_warna;
						if ($kode_brg_jadi == 'MBG700100' && $kode_warna == '35')
							$kode_warna_lama = '51';
						else if ($kode_brg_jadi == 'BJT900100' && $kode_warna == '18')
							$kode_warna_lama = '16';
						else if ($kode_brg_jadi == 'BJG700100' && $kode_warna == '18')
							$kode_warna_lama = '16';
						else
							$kode_warna_lama = $kode_warna;
						
						$stok2 = $rowxx2->stok;
						
						// tambahkan stok2nya ke kode lama
						// 1. ambil stok terkini by warna dgn kode lama
						$query3	= $this->db->query(" SELECT a.id, b.stok FROM tm_stok_hasil_cutting a
									INNER JOIN tm_stok_hasil_cutting_warna b ON a.id = b.id_stok_hasil_cutting
									WHERE a.kode_brg_jadi='".$kode_brg_jadi_lama."' 
									AND b.kode_warna='".$kode_warna_lama."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$stok_lama2	= $hasilrow->stok;
							$id_stok_hasil_cutting2	= $hasilrow->id;
							
							$stok_baru2 = $stok_lama2 + $stok2;
							
							// 2. update stok per warna ke kode lama
							$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok='$stok_baru2'
										WHERE kode_warna = '$kode_warna_lama' AND id_stok_hasil_cutting='$id_stok_hasil_cutting2' ");
							// 3. dan stok kode baru = 0
							$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok='0'
										WHERE kode_warna = '$kode_warna' AND id_stok_hasil_cutting='$id_stok_hasil_cutting' "); 
						} // end if ambil stok kode lama
					}
				} // end if stok warna
				
				// 4. ambil stok terkini kode lama
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting
									WHERE kode_brg_jadi='".$kode_brg_jadi_lama."' ");
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
					$id_stok_hasil_cutting2	= $hasilrow->id;
						
					$stok_baru = $stok_lama + $stok;
							
					// 2. update stok ke kode lama
					$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok='$stok_baru'
								WHERE id='$id_stok_hasil_cutting2' ");
					// 3. dan stok kode baru = 0
					$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok='0'
								WHERE id='$id_stok_hasil_cutting' "); 
				}
				
				// 5. hapus data kode baru dari stok per warna
				$this->db->query(" DELETE FROM tm_stok_hasil_cutting_warna WHERE id_stok_hasil_cutting='$id_stok_hasil_cutting' ");
				// -------------------------------------------------------------
			} // end foreach
			// 6. hapus data kode baru dari stok hasil cutting
			$this->db->query(" DELETE FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = 'MBG700100' OR kode_brg_jadi='BJT900100'
								OR kode_brg_jadi='BJG700100' ");
			
	  } // end if PROSES A
	  
	  // B. UPDATE SEMUA KODE2 LAMA MENJADI KODE BARU di tabel bonmmasukcutting_detail.
		if ($kode_brg_jadi == 'MBG700100')
			$kode_brg_jadi_lama = 'MBG201000';
		else if ($kode_brg_jadi == 'BJT900100')
			$kode_brg_jadi_lama = 'BJT101500';
		else if ($kode_brg_jadi == 'BJG700100')
			$kode_brg_jadi_lama = 'BJG301500';
	  
		// 1. di data kode baru, ganti kode warnanya utk yg tidak sesuai
		$sqlxx = $this->db->query(" select * from tm_bonmmasukcutting_detail where kode_brg_jadi='MBG700100'
								OR kode_brg_jadi='BJT900100' OR kode_brg_jadi='BJG700100' ");
		if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$kode_brg_jadi = $rowxx->kode_brg_jadi;
				$id_bonmmasukcutting_detail = $rowxx->id;
				
				$sqlxx2 = $this->db->query(" SELECT * FROM tm_bonmmasukcutting_detail_warna WHERE id_bonmmasukcutting_detail = '".$id_bonmmasukcutting_detail."' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						$kode_warna = $rowxx2->kode_warna;
						$id_bonmmasukcutting_detail_warna = $rowxx2->id;
						if ($kode_brg_jadi == 'MBG700100' && $kode_warna == '35') {
							$kode_warna_lama = '51';
							$this->db->query(" UPDATE tm_bonmmasukcutting_detail_warna SET kode_warna='$kode_warna_lama'
												WHERE id='$id_bonmmasukcutting_detail_warna' ");
						}
						else if ($kode_brg_jadi == 'BJT900100' && $kode_warna == '18') {
							$kode_warna_lama = '16';
							$this->db->query(" UPDATE tm_bonmmasukcutting_detail_warna SET kode_warna='$kode_warna_lama'
												WHERE id='$id_bonmmasukcutting_detail_warna' ");
						}
						else if ($kode_brg_jadi == 'BJG700100' && $kode_warna == '18') {
							$kode_warna_lama = '16';
							$this->db->query(" UPDATE tm_bonmmasukcutting_detail_warna SET kode_warna='$kode_warna_lama'
												WHERE id='$id_bonmmasukcutting_detail_warna' ");
						}
						//else
						//	$kode_warna_lama = $kode_warna;
					}
				}
			} // end for utama
		} // end if utama
	  
	  // 2. update kode brg lama ke kode baru
	  $this->db->query(" UPDATE tm_bonmmasukcutting_detail SET kode_brg_jadi='MBG700100' WHERE kode_brg_jadi='MBG201000' ");
	  $this->db->query(" UPDATE tm_bonmmasukcutting_detail SET kode_brg_jadi='BJT900100' WHERE kode_brg_jadi='BJT101500' ");
	  $this->db->query(" UPDATE tm_bonmmasukcutting_detail SET kode_brg_jadi='BJG700100' WHERE kode_brg_jadi='BJG301500' ");
	  
	  $this->db->query(" UPDATE tm_stok_hasil_cutting SET kode_brg_jadi='MBG700100' WHERE kode_brg_jadi='MBG201000' ");
	  $this->db->query(" UPDATE tm_stok_hasil_cutting SET kode_brg_jadi='BJT900100' WHERE kode_brg_jadi='BJT101500' ");
	  $this->db->query(" UPDATE tm_stok_hasil_cutting SET kode_brg_jadi='BJG700100' WHERE kode_brg_jadi='BJG301500' ");
	  
	  echo "sukses2";
  }
  
  // 07-04-2015
  function recalculatesaldoakhirhasilcutting() {
	  // patokan awalnya: SO desember 2014
	  
	  // A. ambil data2 item barang, query ke tabel tt_stok_opname_hasil_cutting DESEMBER 2014
	  $sqlxx = $this->db->query(" SELECT b.id, b.id_stok_opname_hasil_cutting, b.kode_brg_jadi, b.jum_stok_opname 
							FROM tt_stok_opname_hasil_cutting a
							INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting 
							WHERE bulan = '12' AND tahun='2014' ");
	  if ($sqlxx->num_rows() > 0){
	  		$hasilxx = $sqlxx->result();
	  		foreach ($hasilxx as $rowxx) {
	  			$id_stok_opname_hasil_cutting = $rowxx->id_stok_opname_hasil_cutting;
	  			$id_stok_opname_hasil_cutting_detail = $rowxx->id;
	  			$kode_brg_jadi = $rowxx->kode_brg_jadi;
	  			$jum_stok_opname = $rowxx->jum_stok_opname;
	  			
	  			$sqlxx2 = $this->db->query(" SELECT id, jum_stok_opname, kode_warna FROM tt_stok_opname_hasil_cutting_detail_warna 
										WHERE id_stok_opname_hasil_cutting_detail = '".$id_stok_opname_hasil_cutting_detail."' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->result();
					foreach ($hasilxx2 as $rowxx2) {
						$id_stok_opname_hasil_cutting_detail_warna = $rowxx2->id;
						$jum_stok_opname_warna = $rowxx2->jum_stok_opname;
						$kode_warna = $rowxx2->kode_warna;
						
						// perulangan dari bulan 1 s/d 2
						for ($bulan=1; $bulan<=2; $bulan++) {
							// query brg masuk dan keluar di bulan tsb
							
							// ambil tgl terakhir di bln tsb
							$timeStamp            =    mktime(0,0,0,$bulan,1,2015);    //Create time stamp of the first day from the give date.
							$firstDay            =     date('d',$timeStamp);    //get first day of the given month
							list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
							$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
							$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
							
							$tahun=2015;
							// ambil bulan lalu
							if ($bulan == 1) {
								$bln_query = 12;
								$thn_query = $tahun-1;
							}
							else {
								$bln_query = $bulan-1;
								$thn_query = $tahun;
								if ($bln_query < 10)
									$bln_query = "0".$bln_query;
							}
							
							$bulanx = "0".$bulan;
							
							// 07-04-2015
							// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
							// 1. ambil saldo awal bln lalu, dari tabel SO field jum_stok_opname / auto_saldo_akhir
							$queryx	= $this->db->query(" SELECT c.jum_stok_opname, c.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a 
												INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
												INNER JOIN tt_stok_opname_hasil_cutting_detail_warna c ON b.id = c.id_stok_opname_hasil_cutting_detail
												WHERE b.kode_brg_jadi = '$kode_brg_jadi' 
												AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
												AND a.status_approve = 't' AND c.kode_warna = '".$kode_warna."' ");
							if ($queryx->num_rows() > 0){
								$hasilrow = $queryx->row();
								if ($bulan == 1)
									$saldo_awal_warna = $hasilrow->jum_stok_opname;
								else
									$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
							}
							else
								$saldo_awal_warna = 0;
							
							// 2. hitung masuk bln ini
							//	2.1. masuk bonmmasukcutting
							$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmmasukcutting a 
										INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
										INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
										WHERE a.tgl_bonm >='".$tahun."-".$bulanx."-01' 
										AND a.tgl_bonm <='".$tahun."-".$bulanx."-".$lastDay."' 
										AND b.kode_brg_jadi = '$kode_brg_jadi' AND c.kode_warna = '".$kode_warna."' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$jum_masuk_warna = $hasilrow->jum_masuk;
							}
							else
								$jum_masuk_warna = 0;
							
							//	2.2. masuk retur bhn baku
							$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
										INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
										INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
										WHERE a.tgl_sj >='".$tahun."-".$bulanx."-01' 
										AND a.tgl_sj <='".$tahun."-".$bulanx."-".$lastDay."' 
										AND b.kode_brg_jadi = '$kode_brg_jadi' AND c.kode_warna= '".$kode_warna."' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$jum_masuk_warna+= $hasilrow->jum_masuk;
							}
							
							// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
							$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
										INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
										INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
										WHERE a.tgl_bonm >='".$tahun."-".$bulanx."-01' 
										AND a.tgl_bonm <='".$tahun."-".$bulanx."-".$lastDay."' 
										AND b.kode_brg_jadi = '$kode_brg_jadi' AND c.kode_warna= '".$kode_warna."' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$jum_keluar_warna = $hasilrow->jum_keluar;
							}
							else
								$jum_keluar_warna = 0;
							
							// 4. hitung saldo awal+masuk-keluar
							$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna;
							//-------------------------------------------------------------------------------------
							
							// update data ke tabel tt_stok_opname_hasil_cutting_warna di field auto_saldo_akhir
							// berdasarkan bulan dan tahunnya
							// - ambil id detail warna berdasarkan bln, thn, kode brg jadi, dan kode warna
							$query3	= $this->db->query(" SELECT c.id FROM tt_stok_opname_hasil_cutting a 
										INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
										INNER JOIN tt_stok_opname_hasil_cutting_detail_warna c ON b.id = c.id_stok_opname_hasil_cutting_detail
										WHERE a.bulan='$bulanx' AND a.tahun='2015' AND b.kode_brg_jadi='$kode_brg_jadi'
										AND c.kode_warna='$kode_warna' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_so_warna_update = $hasilrow->id;
								
								// update ke field auto_saldo_akhir
								$query3	= $this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail_warna SET 
											auto_saldo_akhir = '$auto_saldo_akhir_warna' WHERE id='$id_so_warna_update' ");
							}
							
						} // end for BULAN
					} // end foreach 2
				} // end if 2
				
				// DETAIL PER KODE BRG JADI
				// perulangan dari bulan 1 s/d 2
				for ($bulan=1; $bulan<=2; $bulan++) {
					// query brg masuk dan keluar di bulan tsb
					
					// ambil tgl terakhir di bln tsb
					$timeStamp            =    mktime(0,0,0,$bulan,1,2015);    //Create time stamp of the first day from the give date.
					$firstDay            =     date('d',$timeStamp);    //get first day of the given month
					list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
					$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
					$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
							
							// ambil bulan lalu
							if ($bulan == 1) {
								$bln_query = 12;
								$thn_query = $tahun-1;
							}
							else {
								$bln_query = $bulan-1;
								$thn_query = $tahun;
								if ($bln_query < 10)
									$bln_query = "0".$bln_query;
							}
							
							$bulanx = "0".$bulan;
							
							// 07-04-2015
							// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
							// 1. ambil saldo awal bln lalu, dari tabel SO field jum_stok_opname / auto_saldo_akhir
							$queryx	= $this->db->query(" SELECT b.jum_stok_opname, b.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a 
												INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
												WHERE b.kode_brg_jadi = '$kode_brg_jadi' 
												AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
												AND a.status_approve = 't' ");
							if ($queryx->num_rows() > 0){
								$hasilrow = $queryx->row();
								if ($bulan == 1)
									$saldo_awal = $hasilrow->jum_stok_opname;
								else
									$saldo_awal = $hasilrow->auto_saldo_akhir;
							}
							else
								$saldo_awal = 0;
							
							// 2. hitung masuk bln ini
							//	2.1. masuk bonmmasukcutting
							$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a 
										INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
										WHERE a.tgl_bonm >='".$tahun."-".$bulanx."-01' 
										AND a.tgl_bonm <='".$tahun."-".$bulanx."-".$lastDay."' 
										AND b.kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$jum_masuk = $hasilrow->jum_masuk;
							}
							else
								$jum_masuk = 0;
							
							//	2.2. masuk retur bhn baku
							$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
										INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
										WHERE a.tgl_sj >='".$tahun."-".$bulanx."-01' 
										AND a.tgl_sj <='".$tahun."-".$bulanx."-".$lastDay."' 
										AND b.kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$jum_masuk+= $hasilrow->jum_masuk;
							}
							
							// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
							$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
										INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
										WHERE a.tgl_bonm >='".$tahun."-".$bulanx."-01' 
										AND a.tgl_bonm <='".$tahun."-".$bulanx."-".$lastDay."' 
										AND b.kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$jum_keluar = $hasilrow->jum_keluar;
							}
							else
								$jum_keluar = 0;
							
							// 4. hitung saldo awal+masuk-keluar
							$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar;
							//-------------------------------------------------------------------------------------
							
							// update data ke tabel tt_stok_opname_hasil_cutting_detail di field auto_saldo_akhir
							// berdasarkan bulan dan tahunnya
							// - ambil id detail berdasarkan bln, thn, kode brg jadi
							$query3	= $this->db->query(" SELECT b.id FROM tt_stok_opname_hasil_cutting a 
										INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
										WHERE a.bulan='$bulanx' AND a.tahun='2015' AND b.kode_brg_jadi='$kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_so_update = $hasilrow->id;
								
								// update ke field auto_saldo_akhir
								$query3	= $this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail SET 
											auto_saldo_akhir = '$auto_saldo_akhir' WHERE id='$id_so_update' ");
							}
							
				} // end for BULAN
				
	  		} // end foreach utama
	  } // end if utama
	  echo "sukses hasil cutting";
  }
  
  // 08-04-2015
  function recalculatesaldoakhirwip() {
	  // ada perbedaan teknik alurnya dibandingkan dgn yg hasil cutting diatas, tapi pada prinsipnya sama
	  // A. ambil data2 item barang, query perulangan ke tabel tt_stok_opname_hasil_jahit
	  $sqlxx = $this->db->query(" SELECT a.bulan, a.id_gudang, b.id, b.id_stok_opname_hasil_jahit, b.kode_brg_jadi, b.jum_stok_opname 
							FROM tt_stok_opname_hasil_jahit a
							INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit 
							WHERE tahun='2015' ORDER BY a.bulan, a.id_gudang ");
	  if ($sqlxx->num_rows() > 0){
	  	$hasilxx = $sqlxx->result();
	  	foreach ($hasilxx as $rowxx) {
			$bulan = $rowxx->bulan;
			$id_gudang = $rowxx->id_gudang;
			$id_stok_opname_hasil_jahit = $rowxx->id_stok_opname_hasil_jahit;
	  		$id_stok_opname_hasil_jahit_detail = $rowxx->id;
	  		$kode_brg_jadi = $rowxx->kode_brg_jadi;
	  		$jum_stok_opname = $rowxx->jum_stok_opname;
	  		
	  		// ambil tgl terakhir di bln tsb
			$timeStamp            =    mktime(0,0,0,$bulan,1,2015);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
						
			$tahun=2015;
			// ambil bulan lalu
			if ($bulan == 1) {
				$bln_query = 12;
				$thn_query = $tahun-1;
			}
			else {
				$bln_query = $bulan-1;
				$thn_query = $tahun;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			$sqlxx2 = $this->db->query(" SELECT id, jum_stok_opname, kode_warna FROM tt_stok_opname_hasil_jahit_detail_warna 
										WHERE id_stok_opname_hasil_jahit_detail = '".$id_stok_opname_hasil_jahit_detail."' ");
			if ($sqlxx2->num_rows() > 0){
				$hasilxx2 = $sqlxx2->result();
				foreach ($hasilxx2 as $rowxx2) {
					$id_stok_opname_hasil_jahit_detail_warna = $rowxx2->id;
					$jum_stok_opname_warna = $rowxx2->jum_stok_opname;
					$kode_warna = $rowxx2->kode_warna;
					
					// ==================================================================================
					// 08-04-2015
					// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
					// 1. ambil saldo awal bln lalu, dari tabel SO field jum_stok_opname / auto_saldo_akhir
					$queryx	= $this->db->query(" SELECT c.jum_stok_opname, c.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a 
										INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
										INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
										WHERE b.kode_brg_jadi = '$kode_brg_jadi' AND a.id_gudang= '$id_gudang'
										AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
										AND a.status_approve = 't' AND c.kode_warna = '".$kode_warna."' ");
					if ($queryx->num_rows() > 0){
						$hasilrow = $queryx->row();
						if ($bulan == '01')
							$saldo_awal_warna = $hasilrow->jum_stok_opname;
						else
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
					}
					else
						$saldo_awal_warna = 0;
							
					// 2. hitung masuk bln ini
					$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.id_gudang = '$id_gudang' AND c.kode_warna = '".$kode_warna."' ");
					
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk_warna = $hasilrow->jum_masuk;
					}
					else
						$jum_masuk_warna = 0;
							
					// 3. hitung keluar bln ini
					$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.id_gudang = '$id_gudang' AND c.kode_warna = '".$kode_warna."' ");
									
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_keluar_warna = $hasilrow->jum_keluar;
					}
					else
						$jum_keluar_warna = 0;
							
					// 4. hitung saldo awal+masuk-keluar
					$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna;
					//-------------------------------------------------------------------------------------
							
					// update data ke tabel tt_stok_opname_hasil_jahit_detail_warna di field auto_saldo_akhir
					// berdasarkan bulan dan tahunnya
					// - ambil id detail warna berdasarkan bln, thn, kode brg jadi, dan kode warna
							$query3	= $this->db->query(" SELECT c.id FROM tt_stok_opname_hasil_jahit a 
										INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
										INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
										WHERE a.bulan='$bulan' AND a.tahun='2015' AND b.kode_brg_jadi='$kode_brg_jadi'
										AND a.id_gudang='$id_gudang' AND c.kode_warna='$kode_warna' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_so_warna_update = $hasilrow->id;
								
								// update ke field auto_saldo_akhir
								$query3	= $this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail_warna SET 
											auto_saldo_akhir = '$auto_saldo_akhir_warna' WHERE id='$id_so_warna_update' ");
							}
					// ==================================================================================
				} // end foreach 2
			} // end if 2
	  		
	  		// DETAIL PER BRG JADI
	  		// ==================================================================================
			// 08-04-2015
			// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
					// 1. ambil saldo awal bln lalu, dari tabel SO field jum_stok_opname / auto_saldo_akhir
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname, b.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a 
										INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
										WHERE b.kode_brg_jadi = '$kode_brg_jadi' AND a.id_gudang= '$id_gudang'
										AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
										AND a.status_approve = 't' ");
					if ($queryx->num_rows() > 0){
						$hasilrow = $queryx->row();
						if ($bulan == '01')
							$saldo_awal = $hasilrow->jum_stok_opname;
						else
							$saldo_awal = $hasilrow->auto_saldo_akhir;
					}
					else
						$saldo_awal = 0;
							
					// 2. hitung masuk bln ini
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.id_gudang = '$id_gudang' ");
					
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk = $hasilrow->jum_masuk;
					}
					else
						$jum_masuk = 0;
							
					// 3. hitung keluar bln ini
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.id_gudang = '$id_gudang' ");
									
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_keluar = $hasilrow->jum_keluar;
					}
					else
						$jum_keluar = 0;
							
					// 4. hitung saldo awal+masuk-keluar
					$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar;
					//-------------------------------------------------------------------------------------
							
					// update data ke tabel tt_stok_opname_hasil_jahit_detail di field auto_saldo_akhir
					// berdasarkan bulan dan tahunnya
					// - ambil id detail warna berdasarkan bln, thn, kode brg jadi
							$query3	= $this->db->query(" SELECT b.id FROM tt_stok_opname_hasil_jahit a 
										INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
										WHERE a.bulan='$bulan' AND a.tahun='2015' AND b.kode_brg_jadi='$kode_brg_jadi'
										AND a.id_gudang='$id_gudang' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_so_update = $hasilrow->id;
								
								// update ke field auto_saldo_akhir
								$query3	= $this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET 
											auto_saldo_akhir = '$auto_saldo_akhir' WHERE id='$id_so_update' ");
							}
			// ==================================================================================
	  		
		} // end foreach 1
	  } // end if 1
	  echo "sukses hasil jahit";
  }
  
  function recalculatesaldoakhirunitjahit() {
	  // A. ambil data2 item barang, query perulangan ke tabel tt_stok_opname_unit_jahit
	  $sqlxx = $this->db->query(" SELECT a.bulan, a.kode_unit, b.id, b.id_stok_opname_unit_jahit, b.kode_brg_jadi, b.jum_stok_opname 
							FROM tt_stok_opname_unit_jahit a
							INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit 
							WHERE tahun='2015' ORDER BY a.bulan, a.kode_unit ");
	  if ($sqlxx->num_rows() > 0){
	  	$hasilxx = $sqlxx->result();
	  	foreach ($hasilxx as $rowxx) {
			$bulan = $rowxx->bulan;
			$kode_unit = $rowxx->kode_unit;
			$id_stok_opname_unit_jahit = $rowxx->id_stok_opname_unit_jahit;
	  		$id_stok_opname_unit_jahit_detail = $rowxx->id;
	  		$kode_brg_jadi = $rowxx->kode_brg_jadi;
	  		//$jum_stok_opname = $rowxx->jum_stok_opname;
	  		
	  		// ambil tgl terakhir di bln tsb
			$timeStamp            =    mktime(0,0,0,$bulan,1,2015);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
						
			$tahun=2015;
			// ambil bulan lalu
			if ($bulan == 1) {
				$bln_query = 12;
				$thn_query = $tahun-1;
			}
			else {
				$bln_query = $bulan-1;
				$thn_query = $tahun;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			$sqlxx2 = $this->db->query(" SELECT id, jum_stok_opname, kode_warna FROM tt_stok_opname_unit_jahit_detail_warna 
										WHERE id_stok_opname_unit_jahit_detail = '".$id_stok_opname_unit_jahit_detail."' ");
			if ($sqlxx2->num_rows() > 0){
				$hasilxx2 = $sqlxx2->result();
				foreach ($hasilxx2 as $rowxx2) {
					//$id_stok_opname_unit_jahit_detail_warna = $rowxx2->id;
					//$jum_stok_opname_warna = $rowxx2->jum_stok_opname;
					$kode_warna = $rowxx2->kode_warna;
					
					// ==================================================================================
					// 08-04-2015
					// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
					// 1. ambil saldo awal bln lalu, dari tabel SO field jum_stok_opname / auto_saldo_akhir
					$queryx	= $this->db->query(" SELECT c.jum_stok_opname, c.jum_bagus, c.jum_perbaikan, 
										c.auto_saldo_akhir, c.auto_saldo_akhir_bagus, c.auto_saldo_akhir_perbaikan 
										FROM tt_stok_opname_unit_jahit a 
										INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
										INNER JOIN tt_stok_opname_unit_jahit_detail_warna c ON b.id = c.id_stok_opname_unit_jahit_detail
										WHERE b.kode_brg_jadi = '$kode_brg_jadi' AND a.kode_unit='$kode_unit'
										AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
										AND a.status_approve = 't' AND c.kode_warna = '".$kode_warna."' ");
					if ($queryx->num_rows() > 0){
						$hasilrow = $queryx->row();
						if ($bulan == '01') {
							$saldo_awal_warna = $hasilrow->jum_stok_opname;
							$saldo_awal_bagus_warna = $hasilrow->jum_bagus;
							$saldo_awal_perbaikan_warna = $hasilrow->jum_perbaikan;
						}
						else {
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
							$saldo_awal_bagus_warna = $hasilrow->auto_saldo_akhir_bagus;
							$saldo_awal_perbaikan_warna = $hasilrow->auto_saldo_akhir_perbaikan;
						}
					}
					else {
						$saldo_awal_warna = 0;
						$saldo_awal_bagus_warna = 0;
						$saldo_awal_perbaikan_warna = 0;
					}
					
					// 2. hitung keluar bln ini
						// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$kode_unit' AND c.kode_warna = '".$kode_warna."'
									AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_bagus_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_bagus_warna = 0;
						
						// 2.1.2 dari tabel tm_sjmasukwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$kode_unit' AND c.kode_warna = '".$kode_warna."'
									AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_perbaikan_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_perbaikan_warna = 0;
						
						// 2.2. dari tabel tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit = '$kode_unit' AND c.kode_warna = '".$kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_bagus_warna+= $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_bagus_warna+= 0;
						
						// 3. hitung masuk bln ini
						// 3.1.1 hitung dari tm_sjkeluarwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$kode_unit' AND c.kode_warna = '".$kode_warna."'
									AND a.jenis_keluar <>'3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_bagus_warna= 0;
						
						// 3.1.2 hitung dari tm_sjkeluarwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit_jahit = '$kode_unit' AND c.kode_warna = '".$kode_warna."'
									AND a.jenis_keluar ='3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_perbaikan_warna= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_perbaikan_warna= 0;
						
						// 3.2. dari tabel tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
									AND a.kode_unit = '$kode_unit' AND c.kode_warna = '".$kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna+= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_bagus_warna+= 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_bagus_warna+$saldo_awal_perbaikan_warna+$jum_masuk_bagus_warna+$jum_masuk_perbaikan_warna-$jum_keluar_bagus_warna-$jum_keluar_perbaikan_warna;
						$auto_saldo_akhir_bagus_warna = $saldo_awal_bagus_warna+$jum_masuk_bagus_warna-$jum_keluar_bagus_warna;
						$auto_saldo_akhir_perbaikan_warna = $saldo_awal_perbaikan_warna+$jum_masuk_perbaikan_warna-$jum_keluar_perbaikan_warna;
						
						//$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna;
						//-------------------------------------------------------------------------------------
						// ===================================================
							
					// update data ke tabel tt_stok_opname_unit_jahit_detail_warna di field auto_saldo_akhir
					// berdasarkan bulan dan tahunnya
					// - ambil id detail warna berdasarkan bln, thn, kode brg jadi, dan kode warna
							$query3	= $this->db->query(" SELECT c.id FROM tt_stok_opname_unit_jahit a 
										INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
										INNER JOIN tt_stok_opname_unit_jahit_detail_warna c ON b.id = c.id_stok_opname_unit_jahit_detail
										WHERE a.bulan='$bulan' AND a.tahun='2015' AND b.kode_brg_jadi='$kode_brg_jadi'
										AND a.kode_unit='$kode_unit' AND c.kode_warna='$kode_warna' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_so_warna_update = $hasilrow->id;
								
								// update ke field auto_saldo_akhir
								$query3	= $this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail_warna SET 
											auto_saldo_akhir = '$auto_saldo_akhir_warna', 
											auto_saldo_akhir_bagus = '$auto_saldo_akhir_bagus_warna',
											auto_saldo_akhir_perbaikan = '$auto_saldo_akhir_perbaikan_warna'
											WHERE id='$id_so_warna_update' ");
							}
					// ==================================================================================
				} // end foreach 2
			} // end if 2
	  		
	  		// DETAIL PER BRG JADI
	  		// ==================================================================================
			// 08-04-2015
			// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
					// 1. ambil saldo awal bln lalu, dari tabel SO field jum_stok_opname / auto_saldo_akhir
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname, b.jum_bagus, b.jum_perbaikan, 
										b.auto_saldo_akhir, b.auto_saldo_akhir_bagus, b.auto_saldo_akhir_perbaikan 
										FROM tt_stok_opname_unit_jahit a 
										INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
										WHERE b.kode_brg_jadi = '$kode_brg_jadi' AND a.kode_unit='$kode_unit' 
										AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
										AND a.status_approve = 't' ");
					if ($queryx->num_rows() > 0){
						$hasilrow = $queryx->row();
						/*if ($bulan == '01')
							$saldo_awal = $hasilrow->jum_stok_opname;
						else
							$saldo_awal = $hasilrow->auto_saldo_akhir; */
						if ($bulan == '01') {
							$saldo_awal = $hasilrow->jum_stok_opname;
							$saldo_awal_bagus = $hasilrow->jum_bagus;
							$saldo_awal_perbaikan = $hasilrow->jum_perbaikan;
						}
						else {
							$saldo_awal = $hasilrow->auto_saldo_akhir;
							$saldo_awal_bagus = $hasilrow->auto_saldo_akhir_bagus;
							$saldo_awal_perbaikan = $hasilrow->auto_saldo_akhir_perbaikan;
						}
					}
					else {
						$saldo_awal = 0;
						$saldo_awal_bagus = 0;
						$saldo_awal_perbaikan = 0;
					}
							
				// 2. hitung keluar bln ini
				// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
							AND a.kode_unit_jahit = '$kode_unit' AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus = 0;
				
				// 2.1.2 tm_sjmasukwip, jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
							AND a.kode_unit_jahit = '$kode_unit' AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_perbaikan = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perbaikan = 0;
				
				// 2.2. dari tabel tm_sjmasukbhnbakupic
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
							AND a.kode_unit = '$kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus+= $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus+= 0;
				
				// 3. hitung masuk bln ini
				// 3.1.1 dari tabel tm_sjkeluarwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
							AND a.kode_unit_jahit = '$kode_unit' AND a.jenis_keluar<>'3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus = 0;
				
				// 3.1.2 dari tabel tm_sjkeluarwip. jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
							AND a.kode_unit_jahit = '$kode_unit' AND a.jenis_keluar='3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_perbaikan = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_perbaikan = 0;
				
				// 3.2. dari tabel tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$kode_brg_jadi'
							AND a.kode_unit = '$kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus+= $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus+= 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal_bagus+$saldo_awal_perbaikan+$jum_masuk_bagus+$jum_masuk_perbaikan-$jum_keluar_bagus-$jum_keluar_perbaikan;
				$auto_saldo_akhir_bagus = $saldo_awal_bagus+$jum_masuk_bagus-$jum_keluar_bagus;
				$auto_saldo_akhir_perbaikan = $saldo_awal_perbaikan+$jum_masuk_perbaikan-$jum_keluar_perbaikan;
				//-------------------------------------------------------------------------------------
							
					// update data ke tabel tt_stok_opname_unit_jahit_detail di field auto_saldo_akhir
					// berdasarkan bulan dan tahunnya
					// - ambil id detail warna berdasarkan bln, thn, kode brg jadi
							$query3	= $this->db->query(" SELECT b.id FROM tt_stok_opname_unit_jahit a 
										INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
										WHERE a.bulan='$bulan' AND a.tahun='2015' AND b.kode_brg_jadi='$kode_brg_jadi'
										AND a.kode_unit='$kode_unit' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_so_update = $hasilrow->id;
								
								// update ke field auto_saldo_akhir
								$query3	= $this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET 
											auto_saldo_akhir = '$auto_saldo_akhir', 
											auto_saldo_akhir_bagus = '$auto_saldo_akhir_bagus',
											auto_saldo_akhir_perbaikan = '$auto_saldo_akhir_perbaikan' WHERE id='$id_so_update' ");
							}
			// ==================================================================================
	  		
		} // end foreach 1
	  } // end if 1
	  echo "sukses unit jahit";
  }

}
