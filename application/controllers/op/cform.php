<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('op/mmaster');
	}

	function index()
	{
		// =======================
		// disini coding utk pengecekan user login
		// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_pp = $this->input->post('id_pp', TRUE);
		$no_pp = $this->input->post('no_pp', TRUE);
		$proses_submit = $this->input->post('submit', TRUE);
		$id_pp_detail = $this->input->post('id_brg', TRUE);
		$id_supplier = $this->input->post('id_supplier', TRUE);

		$list_brg = explode(";", $id_pp_detail);
		//$th_now	= date("Y");

		if ($proses_submit == "Proses") {
			if ($id_pp != '') {
				//$data['pp_detail'] = $this->mmaster->get_detail_pp($id_pp, $list_brg, $kode_supplier);
				$data['pp_detail'] = $this->mmaster->get_detail_pp($list_brg, $id_supplier);
				$data['msg'] = '';
				$data['id_pp'] = $id_pp;
				$data['no_pp'] = $no_pp;
				//$data['supplier'] = $this->mmaster->get_supplier();
			} else {
				$data['msg'] = 'PP harus dipilih';
				$data['id_pp'] = '';
				$data['no_pp'] = '';
				$data['supplier'] = $this->mmaster->get_supplier();
			}
			$data['go_proses'] = '1';

			$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_supplier' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) {
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
			} else {
				$kode_supplier = '';
				$nama_supplier = "Lain-lain";
			}
			// generate no OP
			$th_now	= date("y");
			$bln_now = date("m");
			$romawi_now =  $this->mmaster->konversi_angka2romawi($bln_now);

			$query3	= $this->db->query(" SELECT no_op FROM tm_op ORDER BY id DESC LIMIT 1 ");

			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0)
				$no_op	= $hasilrow->no_op;
			else
				$no_op = '';

			if ($no_op != '') {
				$pisah1 = explode("/", $no_op);
				$nomornya = $pisah1[0];
				$bulannya = $pisah1[1];
				$tahunnya = $pisah1[3];

				$noop_angka = (substr($nomornya, 0, 4)) + 1;


				if ($th_now == $tahunnya) {
					$jml_n_op = $noop_angka;

					switch (strlen($jml_n_op)) {
						case "1":
							$kodeop	= "000" . $jml_n_op;
							break;
						case "2":
							$kodeop	= "00" . $jml_n_op;
							break;
						case "3":
							$kodeop	= "0" . $jml_n_op;
							break;
						case "4":
							$kodeop	= $jml_n_op;
							break;
					}
					$nomorop = $kodeop;
				} else {
					$nomorop = "001";
				}
			} else {
				$nomorop	= "001";
			}
			$nomorop = $nomorop . "/" . $romawi_now . "/OP/" . $th_now;
			//var_dump($nomorop);

			$data['no_op'] = $nomorop;
			$data['nama_supplier'] = $nama_supplier;
			$data['kode_supplier'] = $kode_supplier;
			$data['id_supplier'] = $id_supplier;
		} else {
			$data['msg'] = '';
			$data['id_pp'] = '';
			$data['go_proses'] = '';
			$data['supplier'] = $this->mmaster->get_supplier();
		}
		$data['isi'] = 'op/vmainform';
		$this->load->view('template', $data);
	}

	function edit()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_op 	= $this->uri->segment(4);

		$cur_page 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$csupplier 	= $this->uri->segment(7);
		$carinya 	= $this->uri->segment(8);

		$data['query'] = $this->mmaster->get_op($id_op, 0);
		$data['msg'] = '';

		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['csupplier'] = $csupplier;
		$data['carinya'] = $carinya;
		$data['supplier'] = $this->mmaster->get_supplier();
		$data['isi'] = 'op/veditform';
		$this->load->view('template', $data);
	}

	function show_popup_pp()
	{
		// =======================
		// disini coding utk pengecekan user login
		//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$keywordcari 	= $this->input->post('cari', TRUE);

		if ($keywordcari == '')
			$keywordcari 	= $this->uri->segment(4);

		if ($keywordcari == '')
			$keywordcari = "all";

		$jum_total = $this->mmaster->get_pptanpalimit($keywordcari);
		$config['base_url'] = base_url() . "index.php/op/cform/show_popup_pp/" . $keywordcari . "/";
		//$config['total_rows'] = $query->num_rows(); 
		/*	$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_pp($config['per_page'],$this->uri->segment(5), $keywordcari);	*/
		$data['query'] = $this->mmaster->get_pp($keywordcari);
		$data['jum_total'] = count($jum_total);
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$this->load->view('op/vpopuppp', $data);
	}

	function submit()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$no_fb 	= $this->input->post('no_op', TRUE);
		$tgl_fb 	  = $this->input->post('tgl_op', TRUE);
		$tgl_delivery = $this->input->post('tgl_delivery', TRUE);
		$tgl_expire 	= $this->input->post('tgl_expire', TRUE);

		$pisah1 = explode("-", $tgl_fb);
		$tgl1 = $pisah1[0];
		$bln1 = $pisah1[1];
		$thn1 = $pisah1[2];
		$tgl_fb = $thn1 . "-" . $bln1 . "-" . $tgl1;

		$pisah2 = explode("-", $tgl_delivery);
		$tgl2 = $pisah2[0];
		$bln2 = $pisah2[1];
		$thn2 = $pisah2[2];
		$tgl_delivery = $thn2 . "-" . $bln2 . "-" . $tgl2;

		$pisah3 = explode("-", $tgl_expire);
		$tgl3 = $pisah3[0];
		$bln3 = $pisah3[1];
		$thn3 = $pisah3[2];
		$tgl_expire = $thn3 . "-" . $bln3 . "-" . $tgl3;


		$no 	= $this->input->post('no', TRUE);
		//$id_pp 	= $this->input->post('id_pp', TRUE);
		$id_supplier = $this->input->post('id_supplier', TRUE);
		$ket = $this->input->post('ket', TRUE);
		$hide_pkp = $this->input->post('hide_pkp', TRUE);
		$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);

		// 02-07-2015
		$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);

		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$csupplier = $this->input->post('csupplier', TRUE);
		$carinya = $this->input->post('carinya', TRUE);

		// jika edit, var ini ada isinya
		$id_op 	= $this->input->post('id_op', TRUE);

		if ($id_op == '') {
			$cek_data = $this->mmaster->cek_data($no_fb);
			if (count($cek_data) > 0) {
				$data['isi'] = 'op/vmainform';
				$data['msg'] = "Data no OP " . $no_fb . " sudah ada..!";
				$data['id_pp'] = '';
				$data['go_proses'] = '';
				$this->load->view('template', $data);
			} else {
				$jumlah_input = $no - 1;

				for ($i = 1; $i <= $jumlah_input; $i++) {
					$this->mmaster->save(
						$no_fb,
						$tgl_fb,
						$tgl_delivery,
						$tgl_expire,
						$id_supplier,
						$jenis_pembelian,
						$ket,
						$hide_pkp,
						$hide_tipe_pajak,
						$this->input->post('id_pp_detail_' . $i, TRUE),
						$this->input->post('id_brg_' . $i, TRUE),
						$this->input->post('nama_' . $i, TRUE),
						$this->input->post('id_satuan_' . $i, TRUE),
						$this->input->post('qty_' . $i, TRUE),
						$this->input->post('harga_' . $i, TRUE),
						$this->input->post('diskon_' . $i, TRUE),
						$this->input->post('harga_lama_' . $i, TRUE),
						$this->input->post('keterangan_' . $i, TRUE),
						$this->input->post('id_pp_' . $i, TRUE)
					);
					// 25-06-2015 $this->input->post('satuan_lain_'.$i, TRUE) DIBUANG
				}
				redirect('op/cform/view');
			}
		} // end if id_op == ''
		else { // update
			$id_supplier_lama = $this->input->post('id_supplier_lama', TRUE);
			$uid_update_by = $this->session->userdata('uid');
			$tgl = date("Y-m-d H:i:s");

			$this->db->query(" UPDATE tm_op SET id_supplier='$id_supplier', tgl_op = '$tgl_fb', tgl_delivery = '$tgl_delivery', tgl_expire = '$tgl_expire',  jenis_pembelian='$jenis_pembelian', 
								keterangan = '$ket', uid_update_by='$uid_update_by', tgl_update = '$tgl' where id= '$id_op' ");

			$jumlah_input = $no - 1;
			for ($i = 1; $i <= $jumlah_input; $i++) {
				// cek qtynya di PP ============================================

				// ambil id_pp_detail
				$id_pp_detail = $this->input->post('id_pp_detaileuy_' . $i, TRUE);
				// 17-06-2015 DIKOMEN
				/*if ($id_pp_detail == 0) {
							$query3	= $this->db->query(" SELECT id, id_pp FROM tm_pp_detail WHERE id_pp = '".$id_pp."'
										AND kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
							$hasilrow = $query3->row();
							$id_pp_detail = $hasilrow->id;
							$id_pp = $hasilrow->id_pp;
						
							//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di PP
							$query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a, tm_pp b 
										WHERE b.id = '$id_pp' AND a.id_pp = b.id AND a.kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
							$hasilrow = $query3->row();
							$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
							
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$id_pp' AND a.kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan kode brg tsb
						} */
				//else {
				//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di PP
				$query3	= $this->db->query(" SELECT id_pp, qty FROM tm_pp_detail WHERE id = '$id_pp_detail' ");
				$hasilrow = $query3->row();
				$id_pp = $hasilrow->id_pp;
				$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya

				$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b 
									ON a.id_op = b.id WHERE a.id_pp_detail = '$id_pp_detail' ");
				$hasilrow = $query3->row();
				$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan id_pp_detail
				//}

				// 19-12-2011, cek apakah ada id_pp_detail
				$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_pp_detail = '" . $id_pp_detail . "' ");
				if ($query4->num_rows() > 0) {
					// 25-06-2015 SATUAN LAIN GA DIPAKE
					//if ($this->input->post('is_satuan_lain_'.$i, TRUE) == 'f')
					$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b 
									ON a.id_pembelian = b.id WHERE a.id_pp_detail = '$id_pp_detail' AND b.status_aktif = 't' 
										AND a.id_brg = '" . $this->input->post('id_brg_' . $i, TRUE) . "' ";
					/*else
								$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b 
										ON a.id_pembelian = b.id WHERE a.id_pp_detail = '$id_pp_detail' AND b.status_aktif = 't' 
										AND a.id_brg = '".$this->input->post('id_brg_'.$i, TRUE)."' "; */

					$query3	= $this->db->query($sql3);
					$hasilrow = $query3->row();
					$jum_ppsj = $hasilrow->jum;
				} else {
					// 17-06-2015 DIKOMEN
					// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
					/*	if ($this->input->post('is_satuan_lain_'.$i, TRUE) == 'f')
								$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
										WHERE a.id_pembelian = b.id AND c.id = d.id_pp ";
							else
								$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
										WHERE a.id_pembelian = b.id AND c.id = d.id_pp ";
								
							$sql3.= " AND b.id_pp like '%$id_pp%' AND c.id = '$id_pp' AND b.status_aktif = 't' ";
							$sql3.= " AND d.kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' AND a.kode_brg = d.kode_brg "; //
							
							$query3	= $this->db->query($sql3);
							$hasilrow = $query3->row();
							$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
							*/
					$jum_ppsj = 0;
				}
				// #######################################################

				$jumtot = $jum_op - $this->input->post('qty_lama_' . $i, TRUE) + $this->input->post('qty_' . $i, TRUE) + $jum_ppsj;
				//echo $jumtot."<br>"; 
				//$tes = $jum_op-$this->input->post('qty_lama_'.$i, TRUE)+$this->input->post('qty_'.$i, TRUE);
				//echo $tes; die();

				if ($jumtot > $qty_pp)
					$qty = $qty_pp - ($jum_op - $this->input->post('qty_lama_' . $i, TRUE) + $jum_ppsj);
				else
					$qty = $this->input->post('qty_' . $i, TRUE);
				//==============================================================

				$this->db->query(" UPDATE tm_op_detail SET id_brg = '" . $this->input->post('id_brg_' . $i, TRUE) . "', 
						nama_brg = '" . $this->db->escape_str($this->input->post('nama_' . $i, TRUE)) . "', 
						id_satuan = '" . $this->input->post('id_satuan_' . $i, TRUE) . "', 
						 qty = '" . $qty . "', keterangan = '" . $this->input->post('keterangan_' . $i, TRUE) . "', 
						 harga = '" . $this->input->post('harga_' . $i, TRUE) . "',
						 diskon = '" . $this->input->post('diskon_' . $i, TRUE) . "'
						 where id= '" . $this->input->post('id_pp_detail_' . $i, TRUE) . "' ");

				// cek status_faktur =======================================
				if ($qty_pp <= ($jum_op - $this->input->post('qty_lama_' . $i, TRUE) + $this->input->post('qty_' . $i, TRUE) + $jum_ppsj)) {

					$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 't' where id= '$id_pp_detail' ");

					//cek di tabel tm_pp_detail, apakah status_faktur sudah 't' semua?
					$this->db->select("id from tm_pp_detail WHERE status_faktur = 'f' AND id_pp = '$id_pp' ", false);
					$query = $this->db->get();
					//jika sudah t semua, maka update tabel tm_pp di field status_faktur menjadi t
					if ($query->num_rows() == 0) {
						$this->db->query(" UPDATE tm_pp SET status_faktur = 't' where id= '$id_pp' ");
					}
				} // end if
				else { // jika tidak sama, maka statusnya false
					$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 'f' where id= '$id_pp_detail' ");
					$this->db->query(" UPDATE tm_pp SET status_faktur = 'f' where id= '$id_pp' ");
				}
				// =========================================================

				if ($id_supplier != '0') {
					// 04-07-2015 jika ada pergantian supplier, maka harga lama dihapus berdasarkan supplier lama
					if ($id_supplier != $id_supplier_lama) {
						$this->db->query(" DELETE FROM tm_harga_brg_supplier WHERE id_brg = '" . $this->input->post('id_brg_' . $i, TRUE) . "'
											AND id_supplier = '$id_supplier_lama' AND harga = '" . $this->input->post('harga_lama_' . $i, TRUE) . "' 
											AND id_satuan='" . $this->input->post('id_satuan_' . $i, TRUE) . "' ");

						//$this->db->query(" DELETE FROM tt_harga WHERE id_brg = '".$this->input->post('id_brg_'.$i, TRUE)."'
						//			AND id_supplier = '$id_supplier_lama' AND harga = '".$this->input->post('harga_lama_'.$i, TRUE)."' ");
					}

					$query2	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE id_brg = '" . $this->input->post('id_brg_' . $i, TRUE) . "'
									AND id_supplier = '$id_supplier' AND id_satuan= '" . $this->input->post('id_satuan_' . $i, TRUE) . "' ");
					if ($query2->num_rows() > 0) {
						if (($this->input->post('harga_' . $i, TRUE) != $this->input->post('harga_lama_' . $i, TRUE))
							&& ($id_supplier == $id_supplier_lama)
						)
							$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '" . $this->input->post('harga_' . $i, TRUE) . "',
											tgl_update = '$tgl' where id_brg= '" . $this->input->post('id_brg_' . $i, TRUE) . "' 
											AND id_supplier= '$id_supplier' AND id_satuan= '" . $this->input->post('id_satuan_' . $i, TRUE) . "' ");
					} else
						$this->db->query(" INSERT INTO tm_harga_brg_supplier (id_brg, id_supplier, id_satuan, harga, tgl_input, tgl_update) 
												VALUES ('" . $this->input->post('id_brg_' . $i, TRUE) . "','$id_supplier', 
												'" . $this->input->post('id_satuan_' . $i, TRUE) . "', 
												'" . $this->input->post('harga_' . $i, TRUE) . "', 
												'$tgl', '$tgl') ");

					// 29-10-2015 GA DIPAKE LG!
					/*if (($this->input->post('harga_'.$i, TRUE) != $this->input->post('harga_lama_'.$i, TRUE))
							&& ($id_supplier == $id_supplier_lama))
								$this->db->query(" INSERT INTO tt_harga (id_brg, id_supplier, id_satuan, harga, tgl_input) 
												VALUES ('".$this->input->post('id_brg_'.$i, TRUE)."','$id_supplier', 
												'".$this->input->post('id_satuan_'.$i, TRUE)."', 
												'".$this->input->post('harga_'.$i, TRUE)."', '$tgl') "); */
				}
			}
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "op/cform/view/index/" . $cur_page;
			else
				$url_redirectnya = "op/cform/cari/" . $csupplier . "/" . $carinya . "/" . $cur_page;

			redirect($url_redirectnya);
			//redirect('op/cform/view');
		} // end if update
	}

	function view()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$data['isi'] = 'op/vformview';
		$keywordcari = "all";
		$csupplier = 'xx';
		//$kode_bagian = '';

		$jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
		//var_dump($jum_total);
		$config['base_url'] = base_url() . 'index.php/op/cform/view/index/';
		//$config['total_rows'] = $query->num_rows(); 
		$config['total_rows'] = count($jum_total);
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(5);
		$this->pagination->initialize($config);
		$data['query'] = $this->mmaster->getAll($config['per_page'], $this->uri->segment(5), $csupplier, $keywordcari);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = 0;
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		$data['list_supplier'] = $this->mmaster->get_supplier();
		$data['csupplier'] = $csupplier;
		$this->load->view('template', $data);
	}

	function cari()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$keywordcari 	= $this->input->post('cari', TRUE);
		$csupplier = $this->input->post('supplier', TRUE);

		if ($keywordcari == '' && $csupplier == '') {
			$csupplier 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}

		if ($keywordcari == '')
			$keywordcari 	= "all";

		if ($csupplier == '')
			$csupplier = 'xx';

		$jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
		$config['base_url'] = base_url() . 'index.php/op/cform/cari/' . $csupplier . '/' . $keywordcari . '/';
		$config['total_rows'] = count($jum_total);
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(6);
		$this->pagination->initialize($config);
		$data['query'] = $this->mmaster->getAll($config['per_page'], $this->uri->segment(6), $csupplier, $keywordcari);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = 1;
		$data['isi'] = 'op/vformview';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;

		$data['list_supplier'] = $this->mmaster->get_supplier();
		$data['csupplier'] = $csupplier;
		$this->load->view('template', $data);
	}

	function delete()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id 	= $this->uri->segment(4);
		$list_brg 	= $this->uri->segment(5);

		$cur_page 	= $this->uri->segment(6);
		$is_cari 	= $this->uri->segment(7);
		$csupplier 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);

		$this->mmaster->delete($id, $list_brg);

		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "op/cform/view/index/" . $cur_page;
		else
			$url_redirectnya = "op/cform/cari/" . $csupplier . "/" . $carinya . "/" . $cur_page;

		redirect($url_redirectnya);
		//redirect('op/cform/view');
	}

	// 18 jan 2012, update status aktif
	function updatestatus()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id 	= $this->uri->segment(4);
		$aksi = $this->uri->segment(5);
		$cur_page 	= $this->uri->segment(6);
		$is_cari 	= $this->uri->segment(7);
		$csupplier 	= $this->uri->segment(8);
		$carinya 	= $this->uri->segment(9);

		$this->mmaster->updatestatus($id, $aksi);

		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "op/cform/view/index/" . $cur_page;
		else
			$url_redirectnya = "op/cform/cari/" . $csupplier . "/" . $carinya . "/" . $cur_page;

		redirect($url_redirectnya);
		//redirect('op/cform/view');
	}

	function edititem()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$iddetail 	= $this->uri->segment(4);
		$curpage 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$csupplier 	= $this->uri->segment(7);
		$carinya 	= $this->uri->segment(8);
		$go_edit 	= $this->input->post('go_edit', TRUE);

		if ($iddetail == '' && $curpage == '' && $is_cari == '' && $csupplier == '' && $carinya == '') {
			$iddetail 	= $this->input->post('iddetail', TRUE);
			$curpage 	= $this->input->post('curpage', TRUE);
			$carinya 	= $this->input->post('carinya', TRUE);
			$is_cari 	= $this->input->post('is_cari', TRUE);
			$csupplier 	= $this->input->post('csupplier', TRUE);
		}

		if ($go_edit == '') {
			$data['query'] = $this->mmaster->get_item_op($iddetail);
			$data['supplier'] = $this->mmaster->get_supplier();
			$data['isi'] = 'op/vedititemform';
			$data['iddetail'] = $iddetail;
			$data['curpage'] = $curpage;
			$data['csupplier'] = $csupplier;
			$data['carinya'] = $carinya;
			$data['is_cari'] = $is_cari;
			$this->load->view('template', $data);
		} else { // update data detail
			$qty 	= $this->input->post('qty', TRUE);
			$qty_lama 	= $this->input->post('qty_lama', TRUE);
			$id_op 	= $this->input->post('id_op', TRUE);
			$id_pp_detail 	= $this->input->post('id_pp_detail', TRUE);
			$id_supplier 	= $this->input->post('id_supplier', TRUE);
			$tgl_op 	= $this->input->post('tgl_op', TRUE);

			$pisah1 = explode("-", $tgl_op);
			$thn1 = $pisah1[2];
			$bln1 = $pisah1[1];
			$tgl1 = $pisah1[0];

			$tgl_op = $thn1 . "-" . $bln1 . "-" . $tgl1;

			$tgl = date("Y-m-d H:i:s");

			//if ($qty != $qty_lama) {
			//$this->db->query(" UPDATE tm_op_detail SET qty = '$qty' WHERE id = '$iddetail' ");
			$this->db->query(" UPDATE tm_op SET tgl_op = '$tgl_op', id_supplier = '$id_supplier', tgl_update = '$tgl' WHERE id = '$id_op' ");
			//}
			//$url_redirectnya = "pp-new/cform/view/index/".$curpage;

			// 11-07-2012, cek di tm_pp_detail apakah terpenuhi semua atau ga
			$id_pp 	= $this->input->post('id_pp', TRUE);
			$id_brg  = $this->input->post('id_brg', TRUE);
			// 17-06-2015 DIKOMEN GA DIPAKE LG
			/*if ($id_pp_detail == 0) {
							$query3	= $this->db->query(" SELECT id FROM tm_pp_detail WHERE id_pp = '".$id_pp."'
										AND kode_brg = '".$kode_brg."' ");
							$hasilrow = $query3->row();
							$id_pp_detail = $hasilrow->id;
							//$id_pp = $hasilrow->id_pp;
						
							//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di PP
							$query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a, tm_pp b 
										WHERE b.id = '$id_pp' AND a.id_pp = b.id AND a.kode_brg = '".$kode_brg."' ");
							$hasilrow = $query3->row();
							$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
							
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$id_pp' AND a.kode_brg = '".$kode_brg."' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan kode brg tsb
						} */
			//else {
			//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di PP
			$query3	= $this->db->query(" SELECT id_pp, qty FROM tm_pp_detail WHERE id = '$id_pp_detail' ");
			$hasilrow = $query3->row();
			//$id_pp = $hasilrow->id_pp;
			$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya

			$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b 
									ON a.id_op = b.id WHERE a.id_pp_detail = '$id_pp_detail' ");
			$hasilrow = $query3->row();
			$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan id_pp_detail
			//}

			// 19-12-2011, cek apakah ada id_pp_detail
			$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_pp_detail = '" . $id_pp_detail . "' ");
			if ($query4->num_rows() > 0) {
				// 23-06-2015 DIKOMEN, SATUAN LAIN GA DIPAKE LG
				//if ($this->input->post('is_satuan_lain_'.$i, TRUE) == 'f')
				$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b 
									ON a.id_pembelian = b.id WHERE a.id_pp_detail = '$id_pp_detail' AND b.status_aktif = 't' 
										AND a.id_brg = '" . $id_brg . "' ";
				/*else
								$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b
										ON a.id_pembelian = b.id WHERE a.id_pp_detail = '$id_pp_detail' AND b.status_aktif = 't' 
										AND a.id_brg = '".$id_brg."' "; */

				$query3	= $this->db->query($sql3);
				$hasilrow = $query3->row();
				$jum_ppsj = $hasilrow->jum;
			} else {
				// 17-06-2015 dikomen, GA DIPAKE
				// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
				/*	if ($this->input->post('is_satuan_lain_'.$i, TRUE) == 'f')
								$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
										WHERE a.id_pembelian = b.id AND c.id = d.id_pp ";
							else
								$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
										WHERE a.id_pembelian = b.id AND c.id = d.id_pp ";
								
							$sql3.= " AND b.id_pp like '%$id_pp%' AND c.id = '$id_pp' AND b.status_aktif = 't' ";
							$sql3.= " AND d.kode_brg = '".$kode_brg."' AND a.kode_brg = d.kode_brg "; //
							
							$query3	= $this->db->query($sql3);
							$hasilrow = $query3->row();
							$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
							*/
				$jum_ppsj = 0;
			}
			// #######################################################
			$jumtot = $jum_op - $qty_lama + $qty + $jum_ppsj;
			if ($jumtot > $qty_pp)
				$qty = $qty_pp - ($jum_op - $qty_lama + $jum_ppsj);
			else
				$qty = $this->input->post('qty', TRUE);
			//==============================================================

			// $this->db->query(" UPDATE tm_op_detail SET qty = '$qty' WHERE id = '$iddetail' ");
			$this->db->query(" UPDATE tm_op_detail SET qty = '" . $qty . "' WHERE id= '" . $iddetail . "' ");

			// cek status_faktur =======================================
			if ($qty_pp <= ($jum_op - $qty_lama + $this->input->post('qty', TRUE) + $jum_ppsj)) {
				$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 't' 
							where id= '$id_pp_detail' ");

				//cek di tabel tm_pp_detail, apakah status_faktur sudah 't' semua?
				$this->db->select("id from tm_pp_detail WHERE status_faktur = 'f' AND id_pp = '$id_pp' ", false);
				$query = $this->db->get();
				//jika sudah t semua, maka update tabel tm_pp di field status_faktur menjadi t
				if ($query->num_rows() == 0) {
					$this->db->query(" UPDATE tm_pp SET status_faktur = 't' where id= '$id_pp' ");
				}
			} // end if
			else { // jika tidak sama, maka statusnya false
				$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 'f' where id= '$id_pp_detail' ");
				$this->db->query(" UPDATE tm_pp SET status_faktur = 'f' where id= '$id_pp' ");
			}
			// xxxxxxxxxxxxxxx
			//==============================================================
			// ============ end 11-07-2012 ==================================

			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "op/cform/view/index/" . $curpage;
			else
				$url_redirectnya = "op/cform/cari/" . $csupplier . "/" . $carinya . "/" . $curpage;

			redirect($url_redirectnya);
		}
	}

	function deleteitem()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$iddetail 	= $this->uri->segment(4);
		$curpage 	= $this->uri->segment(5);
		$is_cari 	= $this->uri->segment(6);
		$csupplier 	= $this->uri->segment(7);
		$carinya 	= $this->uri->segment(8);
		$this->mmaster->deleteitem($iddetail);

		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "op/cform/view/index/" . $curpage;
		else
			$url_redirectnya = "op/cform/cari/" . $csupplier . "/" . $carinya . "/" . $curpage;
		redirect($url_redirectnya);
	}

	function get_pkp_tipe_pajak()
	{
		$id_sup 	= $this->uri->segment(4);
		$rows = array();
		if (isset($id_sup)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			$rows = $this->mmaster->get_pkp_tipe_pajak_bykodesup($id_sup);

			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);
	}

	function print_op()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_op 	= $this->uri->segment(4);
		//$proses = $this->input->post('proses', TRUE);  
		if ($id_op == '')
			$id_op = $this->input->post('id_op', TRUE);

		//if ($proses != "") {
		$nama_up = $this->input->post('nama_up', TRUE);
		$bag_pembelian = $this->input->post('bag_pembelian', TRUE);
		$bag_keuangan = $this->input->post('bag_keuangan', TRUE);
		$mengetahui = $this->input->post('mengetahui', TRUE);
		$data['query'] = $this->mmaster->get_op($id_op, 1);
		//var_dump($data['query']);
		// 09-07-2015
		$data['datasetting'] = $this->mmaster->get_perusahaan();
		//}
		/*else {
		$nama_up = '';
		$bag_pembelian = '';
		$bag_keuangan = '';  
		$mengetahui = '';  
	} */

		$data['id_op'] = $id_op;
		// 18-01-2016
		$data['uid_update_by'] = $this->session->userdata('uid');

		//$data['proses'] = $proses;
		/*$data['nama_up'] = $nama_up;
	$data['bag_pembelian'] = $bag_pembelian;
	$data['bag_keuangan'] = $bag_keuangan;
	$data['mengetahui'] = $mengetahui; */

		$this->load->view('op/vprintop_baru', $data);
	}

	function print_op_nonharga()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

		$id_op 	= $this->uri->segment(4);
		if ($id_op == '')
			$id_op = $this->input->post('id_op', TRUE);

		$nama_up = $this->input->post('nama_up', TRUE);
		$bag_pembelian = $this->input->post('bag_pembelian', TRUE);
		$bag_keuangan = $this->input->post('bag_keuangan', TRUE);
		$mengetahui = $this->input->post('mengetahui', TRUE);
		$data['query'] = $this->mmaster->get_op($id_op, 1);
		// 09-07-2015
		$data['datasetting'] = $this->mmaster->get_perusahaan();
		$data['id_op'] = $id_op;
		// 18-01-2016
		$data['uid_update_by'] = $this->session->userdata('uid');

		$this->load->view('op/vprintop_nonharga', $data);
	}

	// 01-10-2015
	function migrasinamabrg()
	{
		$sql = " SELECT id, id_brg FROM tm_op_detail ";
		$query	= $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query3	= $this->db->query(" SELECT a.nama_brg, a.satuan FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
				$hasilrow = $query3->row();
				$nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
				$id_satuan	= $hasilrow->satuan;

				$this->db->query(" UPDATE tm_op_detail SET nama_brg = '$nama_brg', id_satuan='$id_satuan' WHERE id = '$row1->id' ");
			}
		}
		echo "sukses migrasi nama_brg di tm_op_detail <br>";
	}

	function perbaikinamabrg()
	{
		$sql = " SELECT id, id_brg FROM tm_op_detail where nama_brg like UPPER('%elastic%') ";
		$query	= $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query3	= $this->db->query(" SELECT a.nama_brg, a.satuan FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
				$hasilrow = $query3->row();
				$nama_brg	= $this->db->escape_str($hasilrow->nama_brg);
				$id_satuan	= $hasilrow->satuan;

				$this->db->query(" UPDATE tm_op_detail SET nama_brg = '$nama_brg', id_satuan='$id_satuan' WHERE id = '$row1->id' ");
			}
		}
		echo "sukses migrasi nama_brg di tm_op_detail <br>";
	}
}
