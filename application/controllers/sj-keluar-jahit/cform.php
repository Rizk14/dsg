<?php
class Cform extends CI_Controller {

  function __construct () {
    parent::__construct();
    $this->load->library('pagination'); 
    $this->load->model('sj-keluar-jahit/mmaster');
  }

  function index() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		
	$id_jahit 	= $this->uri->segment(4);
	$th_now	= date("Y");
	
	if ($id_jahit!='') {
		$hasil = $this->mmaster->get_header_jahit($id_jahit);
		$edit = 1;
				
		foreach ($hasil as $row) {
			$e_tgl_pp = explode("-",$row->tgl_sj,strlen($row->tgl_sj)); // Y-m-d
			$n_tgl_pp = $e_tgl_pp[2]."-".$e_tgl_pp[1]."-".$e_tgl_pp[0];
			
			$eid 		= $row->id;
			$eno_sj 	= $row->no_sj;
			$ekode_unit = $row->kode_unit;
			$etgl_sj	= $n_tgl_pp;
			$ket		= $row->keterangan;
			//$makloon_internal	= $row->makloon_internal;
			//$ekel_bhn	= $row->kel_bhn;
		}
		
		//$data['jahit_detail'] = $this->mmaster->get_detail_jahit($id_jahit, $ekel_bhn);
		$data['jahit_detail'] = $this->mmaster->get_detail_jahit($id_jahit);
		
	}
	else {
			$query3	= $this->db->query(" SELECT no_sj FROM tm_sj_proses_jahit ORDER BY no_sj DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			
			if ($query3->num_rows() != 0) 
				$no_sj	= $hasilrow->no_sj;
			else
				$no_sj = '';
				
			if(strlen($no_sj)==12) {
				$nosj = substr($no_sj, 4, 9);
				$n_sj	= (substr($nosj,4,5))+1;
				$th_sj	= substr($nosj,0,4);
				if($th_now==$th_sj) {
						$jml_n_sj	= $n_sj;
						switch(strlen($jml_n_sj)) {
							case "1": $kodesj	= "0000".$jml_n_sj;
							break;
							case "2": $kodesj	= "000".$jml_n_sj;
							break;	
							case "3": $kodesj	= "00".$jml_n_sj;
							break;
							case "4": $kodesj	= "0".$jml_n_sj;
							break;
							case "5": $kodesj	= $jml_n_sj;
							break;	
						}
						$nomorsj = $th_now.$kodesj;
				}
				else {
					$nomorsj = $th_now."00001";
				}
			}
			else {
				$nomorsj	= $th_now."00001";
			}
			$nomorsj = "SJJ-".$nomorsj;
			$data['no_sj'] = $nomorsj;
					
		$eid 		= '';
		$eno_sj 	= $nomorsj;
		$etgl_sj 	= '';
		$ekode_unit = '';
		//$ekel_bhn = '';
		$edit 		= '';
		$ket		= '';
		$makloon_internal	= '';
	}
	
	$data['edit']	= $edit;
	$data['eid']	= $eid;
	$data['etgl_sj']= $etgl_sj;
	$data['eno_sj'] = $eno_sj;
	$data['ekode_unit'] = $ekode_unit;
	//$data['ekel_bhn'] 	= $ekel_bhn;
	$data['ket'] 	= $ket;
	//$data['makloon']= $makloon_internal=='t'?' checked ':'';
	
	$data['msg'] 	= '';
	$data['kode_unit'] = $this->mmaster->kode_unit();
    $data['isi'] 	= 'sj-keluar-jahit/vmainform';
	
	$this->load->view('template',$data);
  }
  
  function show_popup_brg() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$kel_brg	= $this->uri->segment(4);
	$posisi 	= $this->uri->segment(5);
	$kode_brg_jadi 	= $this->uri->segment(6);

	if ($posisi=='' || $kel_brg=='' || $kode_brg_jadi == '') {
		$posisi		= $this->input->post('posisi', TRUE);  
		$kel_brg	= $this->input->post('kel_brg', TRUE);  
		$kode_brg_jadi	= $this->input->post('kode_brg_jadi', TRUE);  
	}	
	
	$keywordcari 	= $this->input->post('cari', TRUE);    
		
	if ($keywordcari== '' && ($posisi=='' || $kel_brg=='' || $kode_brg_jadi=='')) {
		$kel_brg 	= $this->uri->segment(4);
		$posisi 	= $this->uri->segment(5);
		$kode_brg_jadi 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}

	if ($keywordcari=='')
		$keywordcari	= "all";
	
	$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $kel_brg, $kode_brg_jadi);

	$config['base_url'] 	= base_url()."index.php/sj-keluar-jahit/cform/show_popup_brg/".$kel_brg."/".$posisi."/".$kode_brg_jadi."/".$keywordcari."/";
	$config['total_rows'] 	= count($qjum_total); 
	$config['per_page'] 	= 10;
	$config['first_link'] 	= 'Awal';
	$config['last_link'] 	= 'Akhir';
	$config['next_link'] 	= 'Selanjutnya';
	$config['prev_link'] 	= 'Sebelumnya';
	$config['cur_page']		= $this->uri->segment(7,0);
	$this->pagination->initialize($config);
	
	$data['query']		= $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $kel_brg, $kode_brg_jadi);
	$data['jum_total']	= count($qjum_total);
	$data['posisi']		= $posisi;
	$data['kel_brg']	= $kel_brg;
	$data['kode_brg_jadi']	= $kode_brg_jadi;
		
	if($kel_brg=='1')
		$data['nama_kel']	= "Bisbisan";
	/*else if($kel_brg=='2')
		$data['nama_kel']	= "Bordir";
	else if($kel_brg=='3')
		$data['nama_kel']	= "Print"; */
	else if($kel_brg=='4')
		$data['nama_kel']	= "Asesoris";
	else if($kel_brg=='5')
		$data['nama_kel']	= "Asesoris Murni";
	else if($kel_brg=='6')
		$data['nama_kel']	= "Hasil Cutting";
		
	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['startnya']	= $config['cur_page'];
	
	$this->load->view('sj-keluar-jahit/vpopupbrg',$data);
  }
	
  function show_popup_brg_jadi(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$posisi		= $this->input->post('posisi', TRUE);  
	$kel_brg	= $this->input->post('kel_brg', TRUE);  
	$keywordcari	= $this->input->post('cari', TRUE);  
	
	if ($posisi=='' || $kel_brg=='' || $keywordcari=='') {
		$kel_brg	= $this->uri->segment(4);
		$posisi 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}	

	if ($keywordcari=='')
		$keywordcari = "all";
			
	$qjum_total = $this->mmaster->getbarangjaditanpalimit($kel_brg,$keywordcari);

	$config['base_url'] 	= base_url()."index.php/sj-keluar-jahit/cform/show_popup_brg_jadi/".$kel_brg."/".$posisi."/".$keywordcari."/";
	$config['total_rows'] 	= count($qjum_total); 
	$config['per_page'] 	= 10;
	$config['first_link'] 	= 'Awal';
	$config['last_link'] 	= 'Akhir';
	$config['next_link'] 	= 'Selanjutnya';
	$config['prev_link'] 	= 'Sebelumnya';
	$config['cur_page']		= $this->uri->segment(7,0);
	$this->pagination->initialize($config);
	
	$data['query']		= $this->mmaster->getbarangjadi($config['per_page'],$config['cur_page'], $kel_brg,$keywordcari);	
	$data['jum_total']	= count($qjum_total);
	$data['posisi']		= $posisi;
	$data['kel_brg']	= $kel_brg;

	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
				
	if($kel_brg=='1')
		$data['nama_kel']	= "Bisbisan";
	/*else if($kel_brg=='2')
		$data['nama_kel']	= "Bordir"; */
	else if($kel_brg=='3')
		$data['nama_kel']	= "Print";
	else if($kel_brg=='4')
		$data['nama_kel']	= "Asesoris";
	else if($kel_brg=='5')
		$data['nama_kel']	= "Asesoris Murni";
	else if($kel_brg=='6')
		$data['nama_kel']	= "Hasil Cutting";

	$data['startnya']	= $config['cur_page'];
	
	$this->load->view('sj-keluar-jahit/vpopupbrgjadi',$data);  
  }
  	
  function submit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$goedit 	= $this->input->post('goedit', TRUE);
	$kode_unit	= $this->input->post('kode_unit', TRUE);
	//$kel_brg		= $this->input->post('kel_brg', TRUE);
	$nosj_edit 	= $this->input->post('nosj', TRUE);
	$no_sj		= $this->input->post('no_sj', TRUE);
	$id_edit	= $this->input->post('id', TRUE);
	$tgl_sj		= $this->input->post('tgl_sj', TRUE);
	$no			= $this->input->post('no', TRUE); // status edit ;
	$e_tgl_sj	= explode("-",$tgl_sj,strlen($tgl_sj));
	$n_tgl_sj 	= $e_tgl_sj[2]."-".$e_tgl_sj[1]."-".$e_tgl_sj[0];
	//$makloon_internal	= $this->input->post('makloon_internal', TRUE);
	$ket		= $this->input->post('ket', TRUE);
	
	//if($makloon_internal=='')
	//	$makloon_internal	= 'f';
				
	if ($goedit=='') {
		$cek_data = $this->mmaster->cek_data($no_sj);
		if (count($cek_data) > 0) { 
			$data['isi'] = 'sj-keluar-jahit/vmainform';
			$data['msg'] = "Data SJ nomor ".$no_sj." sudah ada..!";
			$eno_sj 	= '';
			$etgl_sj 	= '';
			$edit 		= '';
			$data['eno_sj'] = $no_sj;
			$data['etgl_sj'] = $tgl_sj;
			$data['ekode_unit'] = $kode_unit;
			$data['edit'] = $edit;
			$this->load->view('template',$data);
		}
		else {
			
			$jumlah_input=$no-1;
			for ($i=1;$i<=$jumlah_input;$i++) {
				$this->mmaster->save($id_edit,$nosj_edit, $no_sj, $n_tgl_sj, $kode_unit, $ket, 
						$this->input->post('kel_brg_'.$i, TRUE),
						$this->input->post('kode_bhn_'.$i, TRUE),
						$this->input->post('nm_brg_'.$i, TRUE), $this->input->post('kode_brg_jd_'.$i, TRUE), 
						$this->input->post('qty_'.$i, TRUE), $this->input->post('keterangan_'.$i, TRUE), 
						$this->input->post('diprint_'.$i, TRUE), $this->input->post('is_dacron_'.$i, TRUE), 
						$this->input->post('ukuran_'.$i, TRUE), $this->input->post('id_stok_'.$i, TRUE),
						$this->input->post('id_'.$i, TRUE), $goedit);
			}
			redirect('sj-keluar-jahit/cform/view');
		}
	}
	else { // fungsi update
		$jumlah_input=$no-1;
		$tgl = date("Y-m-d");

		$data_header = array(
			  'no_sj' => $no_sj,
			  'tgl_sj' => $n_tgl_sj,
			  'kode_unit' => $kode_unit,
			  //'makloon_internal' =>$makloon_internal,
			  'keterangan' =>$ket,
			  'status_edit' =>'t',
			  'tgl_update' => $tgl
		);

		$this->db->where('id',$id_edit);
		$this->db->update('tm_sj_proses_jahit',$data_header);
			
		// sementara update data detailnya dinonaktifkan dulu
		/*for ($i=1;$i<=$jumlah_input;$i++) {
			$this->mmaster->update_sj_jahit($id_edit,$this->input->post('kel_brg_'.$i, TRUE),$this->input->post('kode_bhn_'.$i, TRUE),$this->input->post('nm_brg_'.$i, TRUE),$this->input->post('kode_brg_jd_'.$i, TRUE),$this->input->post('qty_'.$i, TRUE),$this->input->post('qty_lama_'.$i, TRUE),$this->input->post('keterangan_'.$i, TRUE),$this->input->post('id_'.$i, TRUE),$goedit);
		} */
						
		redirect('sj-keluar-jahit/cform/view');
	}
				
  }
  
  function view(){	
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $data['isi'] = 'sj-keluar-jahit/vformview';
    $keywordcari = "all";
    $kode_unit = "0";
	
    $qjum_total = $this->mmaster->getAlltanpalimit($keywordcari, $kode_unit);
	
	$config['base_url'] = base_url().'index.php/sj-keluar-jahit/cform/view/index/';
	$config['total_rows'] = count($qjum_total); 
	$config['per_page'] = 10;
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5,0);
	$this->pagination->initialize($config);		
	
	$data['query'] = $this->mmaster->getAll($config['per_page'],$config['cur_page'], $keywordcari, $kode_unit);	
	$data['jum_total'] = count($qjum_total);
	$data['list_unit'] = $this->mmaster->kode_unit();
	$data['kode_unit'] = $kode_unit;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$this->load->view('template',$data);
  }
  
  function cari(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$kode_unit 	= $this->input->post('kode_unit', TRUE);  
	
	if ($keywordcari == '' && $kode_unit == '') {
		$kode_unit 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
    $qjum_total = $this->mmaster->getAlltanpalimit($keywordcari, $kode_unit);
	
	$config['base_url'] = base_url().'index.php/sj-keluar-jahit/cform/cari/index/'.$kode_unit.'/'.$keywordcari.'/';
	$config['total_rows'] = count($qjum_total); 
	$config['per_page'] = 10;
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(7,0);
	$this->pagination->initialize($config);
	
	$data['query'] = $this->mmaster->getAll($config['per_page'],$config['cur_page'], $keywordcari, $kode_unit);
	$data['jum_total'] = count($qjum_total);
	$data['list_unit'] = $this->mmaster->kode_unit();
	$data['kode_unit'] = $kode_unit;
	$data['isi'] = 'sj-keluar-jahit/vformview';
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$this->load->view('template',$data);
  }

  function delete(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('sj-keluar-jahit/cform/view');
  }
  
  function cari_sj() {
  	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
  	$nomorsj	= $this->input->post('nomorsj');
	$query	= $this->mmaster->getsj($nomorsj);
	if($query->num_rows()>0) {
		echo "Maaf, No. SJ sudah ada!";
	}
  }
}
