<?php

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {

		$data['page_title_satuan']		= $this->lang->line('page_title_satuan');
		$data['form_panel_daftar_satuan']	= $this->lang->line('form_panel_daftar_satuan');
		$data['form_panel_form_satuan']	= $this->lang->line('form_panel_form_satuan');
		$data['form_panel_cari_satuan']	= $this->lang->line('form_panel_cari_satuan');
		$data['form_kode_satuan']		= $this->lang->line('form_kode_satuan');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('satuanbhnbaku/mclass');
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'satuanbhnbaku/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']='satuanbhnbaku/vmainform';
		$this->load->view('template',$data);
	}
function tambah() {

		$data['page_title_satuan']		= $this->lang->line('page_title_satuan');
		$data['form_panel_daftar_satuan']	= $this->lang->line('form_panel_daftar_satuan');
		$data['form_panel_form_satuan']	= $this->lang->line('form_panel_form_satuan');
		$data['form_panel_cari_satuan']	= $this->lang->line('form_panel_cari_satuan');
		$data['form_kode_satuan']		= $this->lang->line('form_kode_satuan');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('satuanbhnbaku/mclass');
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'satuanbhnbaku/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']='satuanbhnbaku/vmainformadd';
		$this->load->view('template',$data);
	}
	function pagesnext() {
		
		$data['page_title_satuan']		= $this->lang->line('page_title_satuan');
		$data['form_panel_daftar_satuan']	= $this->lang->line('form_panel_daftar_satuan');
		$data['form_panel_form_satuan']	= $this->lang->line('form_panel_form_satuan');
		$data['form_panel_cari_satuan']	= $this->lang->line('form_panel_cari_satuan');
		$data['form_kode_satuan']		= $this->lang->line('form_kode_satuan');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('satuanbhnbaku/mclass');
		
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'satuanbhnbaku/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']='satuanbhnbaku/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail() {	
		$data['isi']='satuanbhnbaku/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {

		$esatuan	= $this->input->post('esatuan');
		
		$this->load->model('satuanbhnbaku/mclass');

		if(!empty($esatuan)) {
			
			$this->mclass->msimpan($esatuan);
			
		} else {
			$data['page_title_satuan']		= $this->lang->line('page_title_satuan');
			$data['form_panel_daftar_satuan']	= $this->lang->line('form_panel_daftar_satuan');
			$data['form_panel_form_satuan']	= $this->lang->line('form_panel_form_satuan');
			$data['form_panel_cari_satuan']	= $this->lang->line('form_panel_cari_satuan');
			$data['form_kode_satuan']		= $this->lang->line('form_kode_satuan');
		
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('satuanbhnbaku/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'satuanbhnbaku/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();

			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);		
			
			$data['isi']='satuanbhnbaku/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	function edit() {
		
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		
		$data['page_title_satuan']		= $this->lang->line('page_title_satuan');
		$data['form_panel_daftar_satuan']	= $this->lang->line('form_panel_daftar_satuan');
		$data['form_panel_form_satuan']	= $this->lang->line('form_panel_form_satuan');
		$data['form_panel_cari_satuan']	= $this->lang->line('form_panel_cari_satuan');
		$data['form_kode_satuan']		= $this->lang->line('form_kode_satuan');
		
		$limages			= base_url();
		$data['list']		= "";
		
		$this->load->model('satuanbhnbaku/mclass');
		
		$qjnsvoucher		= $this->mclass->medit($id);
		
		if( $qjnsvoucher->num_rows() > 0 ) {
			$row_jnsvoucher		= $qjnsvoucher->row();
			$data['isatuan']		= $row_jnsvoucher->i_satuan;
			$data['esatuan']	= $row_jnsvoucher->e_satuan;
		} else {
			$data['esatuan']	= "";		
		}
		$data['isi']='satuanbhnbaku/veditform';
		$this->load->view('template',$data);
		
	}
	
	function actedit() {
		
		$isatuan = $this->input->post('isatuan');
		$esatuan = $this->input->post('esatuan');
		
		if(!empty($esatuan)) {
			
			$this->load->model('satuanbhnbaku/mclass');
			
			$this->mclass->mupdate($isatuan,$esatuan);
			
		} else {
			
			$data['page_title_satuan']		= $this->lang->line('page_title_satuan');
			$data['form_panel_daftar_satuan']	= $this->lang->line('form_panel_daftar_satuan');
			$data['form_panel_form_satuan']	= $this->lang->line('form_panel_form_satuan');
			$data['form_panel_cari_satuan']	= $this->lang->line('form_panel_cari_satuan');
			$data['form_kode_satuan']		= $this->lang->line('form_kode_satuan');
		
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('satuanbhnbaku/mclass');
			
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'satuanbhnbaku/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();

			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);	
			
			$data['isi']='satuanbhnbaku/vmainform';
		$this->load->view('template',$data);
		}
	}

	function actdelete() {
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('satuanbhnbaku/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$txtsatuan	= $this->input->post('txtsatuan');

		$data['page_title_satuan']		= $this->lang->line('page_title_satuan');
		$data['form_panel_daftar_satuan']	= $this->lang->line('form_panel_daftar_satuan');
		$data['form_panel_form_satuan']	= $this->lang->line('form_panel_form_satuan');
		$data['form_panel_cari_satuan']	= $this->lang->line('form_panel_cari_satuan');
		$data['form_kode_satuan']		= $this->lang->line('form_kode_satuan');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('satuanbhnbaku/mclass');
		
		$query	= $this->mclass->viewcari($txtsatuan);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'satuanbhnbaku/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txtsatuan,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('satuanbhnbaku/vcariform',$data);
	}

	function carinext() {
		$txtsatuan	= $this->input->post('txtsatuan')?$this->input->post('txtsatuan'):$this->input->get_post('txtsatuan');

		$data['page_title_satuan']		= $this->lang->line('page_title_satuan');
		$data['form_panel_daftar_satuan']	= $this->lang->line('form_panel_daftar_satuan');
		$data['form_panel_form_satuan']	= $this->lang->line('form_panel_form_satuan');
		$data['form_panel_cari_satuan']	= $this->lang->line('form_panel_cari_satuan');
		$data['form_kode_satuan']		= $this->lang->line('form_kode_satuan');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('satuanbhnbaku/mclass');
		
		$query	= $this->mclass->viewcari($txtsatuan);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'satuanbhnbaku/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txtsatuan,$pagination['per_page'],$pagination['cur_page']);
		
		$this->load->view('satuanbhnbaku/vcariform',$data);
	}	
}
?>
