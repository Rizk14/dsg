<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MASTER TRANSAKSI BON M (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bonmkeluar/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'trans_mst_bonkeluar/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		$qry_class				= $this->mclass->code();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['istatusbonmkeluar']	= $row_class->code;
		} else {
			$data['istatusbonmkeluar']	= 1;
		}
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	= 'trans_mst_bonmkeluar/vmainform';
		$this->load->view('template',$data);
		
	}
function tambah() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MASTER TRANSAKSI BON M (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bonmkeluar/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'trans_mst_bonkeluar/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		$qry_class				= $this->mclass->code();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['istatusbonmkeluar']	= $row_class->code;
		} else {
			$data['istatusbonmkeluar']	= 1;
		}
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	= 'trans_mst_bonmkeluar/vmainformadd';
		$this->load->view('template',$data);
		
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MASTER TRANSAKSI BON M (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bonmkeluar/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'trans_mst_bonmkeluar/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();
		$qry_class		= $this->mclass->code();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['istatusbonmkeluar'] = $row_class->code;
		} else {
			$data['istatusbonmkeluar'] = 1;
		}	
		$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'trans_mst_bonmkeluar/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']	= 'trans_mst_bonmkeluar/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$istatusbonmkeluar	= @$this->input->post('i_status_bonmkeluar')?@$this->input->post('i_status_bonmkeluar'):@$this->input->get_post('i_status_bonmkeluar');
		$estatusname= @$this->input->post('e_statusname')?@$this->input->post('e_statusname'):@$this->input->get_post('e_statusname');
		
		if((isset($istatusbonmkeluar) || !empty($istatusbonmkeluar)) && 
		    (isset($estatusname) || !empty($estatusname)) &&
		    (($istatusbonmkeluar!=0 || $istatusbonmkeluar!="")) && ($estatusname!=0 || $estatusname!="")) {
			$this->load->model('trans_mst_bonmkeluar/mclass');
			$this->mclass->msimpan($istatusbonmkeluar,$estatusname);
		} else {
			$data['page_title']	= "MASTER TRANSAKSI BON M (KETERANGAN)";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('trans_mst_bonmkeluar/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'trans_mst_bonmkeluar/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();
			$qry_class		= $this->mclass->code();
			if($qry_class->num_rows()>0) {
				$row_class	= $qry_class->row();
				$data['istatusbonmkeluar'] = $row_class->code;
			} else {
				$data['istatusbonmkeluar'] = 1;
			}
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			
			$data['isi']	= 'trans_mst_bonmkeluar/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		$data['page_title']	= "MASTER TRANSAKSI BON M (KETERANGAN)";
		$limages			= base_url();
		$data['list']		= "";
		$this->load->model('trans_mst_bonmkeluar/mclass');
		$qry_klsbrg		= $this->mclass->medit($id);
		
		if( $qry_klsbrg->num_rows() > 0 ) {
			$row_klsbrg	= $qry_klsbrg->row();
			$data['i_status_bonmkeluar']	= $row_klsbrg->i_to_outbonm;
			$data['e_statusname']	= $row_klsbrg->e_to;
		} else {
			$data['i_status_bonmkeluar']	= "";
			$data['e_statusname']		= "";
		}
		$data['isi']	= 'trans_mst_bonmkeluar/veditform';
		$this->load->view('template',$data);
		
	}
	
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$istatusbonmkeluar	= @$this->input->post('i_status_bonmkeluar')?@$this->input->post('i_status_bonmkeluar'):@$this->input->get_post('i_status_bonmkeluar');
		$estatusname		= @$this->input->post('e_statusname')?@$this->input->post('e_statusname'):@$this->input->get_post('e_statusname');
		
		if((isset($istatusbonmkeluar) || !empty($istatusbonmkeluar)) && 
		    (isset($estatusname) || !empty($estatusname)) &&
		    (($istatusbonmkeluar!=0 || $istatusbonmkeluar!="")) && ($estatusname!=0 || $estatusname!="")) {
			$this->load->model('trans_mst_bonmkeluar/mclass');
			$this->mclass->mupdate($istatusbonmkeluar,$estatusname);
		} else {
			$data['page_title']	= "MASTER TRANSAKSI BON M (KETERANGAN)";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('trans_mst_bonmkeluar/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'trans_mst_bonmkeluar/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination_ori->initialize($pagination);
			$data['create_link']	= $this->pagination_ori->create_links();
			$qry_class		= $this->mclass->code();
			if($qry_class->num_rows()>0) {
				$row_class	= $qry_class->row();
				$data['istatusbonmkeluar']	= $row_class->code;
			} else {
				$data['istatusbonmkeluar']	= 1;
			}
			$data['isi']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			
			$data['isi']	= 'trans_mst_bonmkeluar/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	/* 20052011
	function actdelete() {
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_to_outbomm',array('i_to_outbonm'=>$id));
	}
	*/

	function actdelete(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('trans_mst_bonmkeluar/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_status_bonmkeluar	= $this->input->post('txt_i_status_bonmkeluar')?$this->input->post('txt_i_status_bonmkeluar'):$this->input->get_post('txt_i_status_bonmkeluar');
		$txt_e_statusname	= $this->input->post('txt_e_statusname')?$this->input->post('txt_e_statusname'):$this->input->get_post('txt_e_statusname');

		$data['page_title']	= "MASTER TRANSAKSI BON M (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bonmkeluar/mclass');
		$query	= $this->mclass->viewcari($txt_i_status_bonmkeluar,$txt_e_statusname);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'trans_mst_bonmkeluar/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txt_i_status_bonmkeluar,$txt_e_statusname,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('trans_mst_bonmkeluar/vcariform',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_class	= $this->input->post('txt_i_class')?$this->input->post('txt_i_class'):$this->input->get_post('txt_i_class');
		$txt_e_class_name	= $this->input->post('txt_e_class_name')?$this->input->post('txt_e_class_name'):$this->input->get_post('txt_e_class_name');

		$data['page_title']	= "MASTER TRANSAKSI BON M (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bonmkeluar/mclass');
		$query	= $this->mclass->viewcari($txt_i_class,$txt_e_class_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'trans_mst_bonmkeluar/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_link']	= $this->pagination_ori->create_links();

		$data['isi']	= $this->mclass->mcari($txt_i_class,$txt_e_class_name,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('trans_mst_bonmkeluar/vcariform',$data);
	}	
}
?>
