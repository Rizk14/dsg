<?php
/*
v = variables
*/
class Cformbaru extends CI_Controller {

	function Cformbaru() {
		parent::__construct();	
	}	
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			$data['page_title_barangbhn']		= $this->lang->line('page_title_barangbhn');
			$data['page_intitle_barangbhn']	= $this->lang->line('page_intitle_barangbhn');
			$data['form_tanggal_barangbhn']	= $this->lang->line('form_tanggal_barangbhn');
			$data['form_title_detail_barangbhn']	= $this->lang->line('form_title_detail_barangbhn');
			$data['form_kode_product_opname']	= $this->lang->line('form_kode_product_opname');
			$data['form_nm_product_opname']		= $this->lang->line('form_nm_product_opname');
			$data['form_jml_product_opname']	= $this->lang->line('form_jml_product_opname');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();

			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");
	
			$data['dateTime']	= date("m/d/Y",time());
			$data['tgOpname']	= $tgl."/".$bln."/".$thn;
			$data['isi']	= 'barangbhn/vmainformbaru';	
			$this->load->view('template',$data);
			
	}


	function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		
			$data['page_title']	= 'Barang VS Bhnbku Pembantu';
			$this->load->model('barangbhn/mclass');



		$query  = $this->mclass->get_view();
		$jml 	= $query;
	
		$config['base_url'] = base_url().'index.php/barangbhn/cformbaru/view';
		$config['total_rows'] =count($jml);
		$config['per_page'] = 20;
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page']  = $this->uri->segment(4,0);
		$this->pagination->initialize($config);
		$data['query']	= $this->mclass->get_view2($config['per_page'],$config['cur_page']);
		$data['isi']	= 'barangbhn/vformview';
		$this->load->view('template',$data);
  }


  function view2(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

		$date_from= $this->input->post('date_from');


		$date_to= $this->input->post('date_to');
		
		$data['page_title']	= 'Barang VS Bhnbku Pembantu';
		$this->load->model('barangbhn/mclass');

		$query  = $this->mclass->get_viewx();
		$jml 	= $query;
	
		$config['base_url'] = base_url().'index.php/barangbhn/cformbaru/view2';
		$config['total_rows'] =count($jml);
		$config['per_page'] = 1000;
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page']  = $this->uri->segment(4,0);
		$this->pagination->initialize($config);
		$data['query']	= $this->mclass->get_viewx2($date_from, $date_to);
		$data['date_to'] = $date_to;
		$data['date_from'] = $date_from;
		$data['isi']	= 'barangbhn/vformviewbarangbhn';
		$this->load->view('template',$data);
  }

  	function mainform(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$data['isi'] = 'barangbhn/vmainform';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	//$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }



   function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $iproduct 	= $this->uri->segment(4);
    $id_warna 	= $this->uri->segment(5);

    $this->load->model('barangbhn/mclass');
    $this->mclass->delete($iproduct,$id_warna);
    
    redirect('barangbhn/cformbaru/view');
  }

	function product(){
  	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['page_title']	= 'BARANG JADI';
		$iterasi	= $this->uri->segment(4);

		$data['iterasi']	= $iterasi;		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('barangbhn/mclass');
		$query  = $this->mclass->listdetailbrg();
		$jml 	= $query;
	
		$config['base_url'] = base_url().'index.php/barangbhn/cformbaru/product';
		$config['total_rows'] =count($jml);
		$config['per_page'] = 10;
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page']  = $this->uri->segment(4,0);
		$this->pagination->initialize($config);
		$data['isi']	= $this->mclass->listdetailbrgperpage($config['per_page'],$config['cur_page']);

	$this->load->view('barangbhn/vlistbarang',$data);
	
	}


	function flistbarangjadi(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$key	= strtoupper($this->input->post('key'));
		$data['page_title']	= "BARANG JADI";
		$data['lurl']		= base_url();

		$this->load->model('barangbhn/mclass');

		$query	= $this->mclass->flbarangjadi($key);
		$jml	= $query->num_rows();
		$list	= "";
		
		if($jml>0) {
		
			$cc	= 1; 
			foreach($query->result() as $row) {
			
				$list .= "
					 <tr>
					  <td>".$cc."</td>
			 		  <td><a href=\"javascript:settextfield('$row->id_product', '$row->i_product','$row->e_product_name','$row->e_color_name','$row->id_warna')\" style=\"
			 		      font-size: 12px; \" >".$row->i_product."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->id_product', '$row->i_product','$row->e_product_name','$row->e_color_name','$row->id_warna')\"  style=\"font-size: 12px; \" >".$row->e_product_name."</a></td>
					  <td><a href=\"javascript:settextfield('$row->id_product', '$row->i_product','$row->e_product_name','$row->e_color_name','$row->id_warna')\"  style=\"font-size: 12px; \" >".$row->e_color_name."</a></td>
					 </tr>";
					 $cc+=1;
			}
		} else {
			$list	= "";
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;		
	}
	
	
	function listbhn() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title']	= 'BARANG JADI';
		$iterasi	= $this->uri->segment(4);
		$data['iterasi']	= $iterasi;		
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('barangbhn/mclass');

		$query	= $this->mclass->listbhn();
		
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = base_url().'index.php/barangbhn/cformbaru/listbhnnext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->listbhnperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('barangbhn/vlistformbrgjadi',$data);
	}

	function listbhnnext() {
		$data['page_title']	= 'BARANG JADI';
		$iterasi	= $this->uri->segment(4);
		$data['iterasi']	= $iterasi;
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$this->load->model('barangbhn/mclass');

		$query	= $this->mclass->listbhn();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = base_url().'index.php/barangbhn/cformbaru/listbhnnext/'.$iterasi.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->listbhnperpages($pagination['per_page'],$pagination['cur_page']);		
		
		$this->load->view('barangbhn/vlistformbrgjadi',$data);
	}
	
	function flistbahan(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$key	= strtoupper($this->input->post('key'));
		$data['page_title']	= "BARANG JADI";
		$data['lurl']		= base_url();

		$this->load->model('barangbhn/mclass');

		$query	= $this->mclass->flbahan($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		
		if($jml>0) {
		
			$cc	= 1; 
			foreach($query->result() as $row) {
	

			$ematerialname = str_replace(array("'", "\""), "&quot;", htmlspecialchars($row->nama_brg ) );

					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->id_brg','$row->kode_brg','$ematerialname')\">".$row->kode_brg."</a></td>	 
					  <td><a href=\"javascript:settextfield('$row->id_brg','$row->kode_brg','$ematerialname')\">".$ematerialname."</a></td>
					 </tr>";
					 $cc+=1;
			}
		} else {
			$list	= "";
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;		
	}
	

	function simpanx() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iteration= $this->input->post('iteration');
		$idproduct= $this->input->post('idproduct');
		$iproduct= $this->input->post('iproduct');
		$icolor= $this->input->post('icolor');


		$iterasi	= $iteration;
		$id_brg= array();
		$i_material= array();
		$e_material_name	= array();

		for($cacah=0;$cacah<=$iterasi;$cacah++) {
			$id_brg[$cacah]	= $this->input->post('id_brg_tblItem_'.$cacah);
			$i_material[$cacah]	= $this->input->post('i_material_tblItem_'.$cacah);
		}

 		$this->load->model('barangbhn/mclass');
		$this->mclass->msimpanbaru($idproduct,$iproduct,$icolor,$id_brg,$i_material,$iteration);
		$data['isi']	= 'barangbhn/vmainformbaru';	
		$this->load->view('template',$data);		
		
				
	}


	function export_excel_mutasi() {

		$this->load->model('barangbhn/mclass');
		$query  = $this->mclass->get_view();
	 
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
			<tr class='judulnya'>
				<th style='width: 90px;white-space: nowrap;'>Kode Barang</th>
				<th style='width: 100px;white-space: nowrap;'>Nama Barang</th>
				<th style='width: 95px;white-space: nowrap;'>Warna Barang</th>
				<th >Kode Bahan</th>
				<th >Nama Bahan</th>
		 	</tr>
		</thead>
		<tbody>";

		$group='';
		$i=0;		
		if($query){
		foreach ($query as $row){

			$product=$row->i_product.$row->id_warna;  

			       if($group=='')
          {
            ?>

        <?php
        $html_data.= 
        "<tr><td bgcolor=\"#FFFFF0\" colspan=5 style=\"font-size:12px;\" valign='middle' align='left'><b>".strtoupper($row->i_product." - ".$row->e_product_name." ( ".$row->e_color_name)." )". "</b>";

			$html_data.=  "</tr>";

          }else{
            if($group!=$product)
                    {
        $html_data.=  	       
        "<tr><td bgcolor=\"#FFFFF0\" colspan=5 style=\"font-size:12px;\" valign='middle' align='left'><b>".strtoupper($row->i_product." - ".$row->e_product_name." ( ".$row->e_color_name)." )". "</b>";
		$html_data.=  "</tr>";
                    }
                }

				$group=$product;
		$html_data.= "
				<tr>
				 <td></td>
				 <td></td>
				 <td></td>
				 <td>$row->i_material</td>
				 <td>$row->e_material_name</td>";
				 $html_data.=  "</tr>";
			
				 }
				}

		$html_data.= "</tbody>
		</table>";
	   	$nama_file='Laporan Mutasi Gudang Jadi';
	    $export_excel1='xls';
	    $data=$html_data;
	    $dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
  }


  function export_excel_mutasi2() {

		$this->load->model('barangbhn/mclass');

		$date_from  = $this->uri->segment(4);
		$date_to = $this->uri->segment(5);

		// $date_from= $this->input->post('date_from');
		// $date_to= $this->input->post('date_to');

		// var_dump($date_from);
		// die;

		$query  = $this->mclass->get_viewx2($date_from, $date_to);

		$html_data = "
		<h4><b>Periode:</b> $date_from s.d. $date_to<br></h4> 
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
			<tr class='judulnya'>
				<th style='width: 90px;white-space: nowrap;'>Kode Barang</th>
				<th style='width: 100px;white-space: nowrap;'>Nama Barang</th>
				<th style='width: 95px;white-space: nowrap;'>Warna Barang</th>
				<th >Kode Bahan</th>
				<th >Nama Bahan</th>
				<th >QTY Bahan</th>
		 		<th >Sisa</th>
		 	</tr>
		</thead>
		<tbody>";

		$group='';
		$i=0;		
		if($query){
		foreach ($query as $row){

			$product=$row->i_product.$row->id_warna;  

			       if($group=='')
          {
            ?>

        <?php
        $html_data.= 
        "<tr><td bgcolor=\"#FFFFF0\" colspan=5 style=\"font-size:12px;\" valign='middle' align='left'><b>".strtoupper($row->i_product." - ".$row->e_product_name." ( ".$row->e_color_name)." )". "</b>";

			$html_data.=  "</tr>";

          }else{
            if($group!=$product)
                    {
        $html_data.=  	       
        "<tr><td bgcolor=\"#FFFFF0\" colspan=5 style=\"font-size:12px;\" valign='middle' align='left'><b>".strtoupper($row->i_product." - ".$row->e_product_name." ( ".$row->e_color_name)." )". "</b>";
		$html_data.=  "</tr>";
                    }
                }
				$group=$product;
		$html_data.= "
				<tr>
				 <td></td>
				 <td></td>
				 <td></td>
				 <td>$row->i_material</td>
				 <td>$row->e_material_name</td>
				 <td>$row->tot_compare</td>
				 <td>$row->qty_akhir</td>";

		$html_data.=  "</tr>";
			
				 }
				}

		$html_data.= "</tbody>
		</table>";
	   	$nama_file='Laporan Mutasi Gudang Jadi';
	    $export_excel1='xls';
	    $data=$html_data;
	    $dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
  }


}
?>
