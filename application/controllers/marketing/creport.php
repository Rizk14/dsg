<?php
class Creport extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('marketing/mreport');
  }
   // start 18-04-2015
  function sinkronkelbrgjadi(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$db2 = $this->load->database('db_external', TRUE);
		// Database External
			
		$queryxx = $this->db->query(" SELECT i_product_motif, id_kel_brg_jadi FROM tr_product_motif WHERE n_active='1' ");
		if($queryxx->num_rows() > 0) {
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$i_product_motif = $rowxx->i_product_motif;
			$id_kel_brg_jadi = $rowxx->id_kel_brg_jadi;
				
				// update data kel brg jadi dari database sysdbduta
				$db2->query(" UPDATE tr_product_motif SET id_kel_brg_jadi='$id_kel_brg_jadi' WHERE i_product_motif='$i_product_motif' ");
			} // end foreach
		} // end if
		
		$html_data.= "sukses update kel brg jadi";
		return true;
  }
  
  function laprealisasi(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_pelanggan'] = $this->mreport->get_pelanggan();
	$data['isi'] = 'marketing/vformlaprealisasi';
	$this->load->view('template',$data);
  }
  
  function laprealisasibrand(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_brand'] = $this->mreport->get_brand();
	$data['isi'] = 'marketing/vformlaprealisasibrand';
	$this->load->view('template',$data);
  }
  
   function viewlaprealisasibrand(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	set_time_limit(3600000);
	ini_set("memory_limit","512M");
	ini_set("max_execution_time","3600000");
	

    $data['isi'] = 'marketing/vviewlaprealisasibrand';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$i_brand = $this->input->post('i_brand', TRUE);  
	
	$data['query'] = $this->mreport->get_laprealisasi_brand($date_from, $date_to, $i_brand, 0);
	$data['query2'] = $this->mreport->get_laprealisasi_brand($date_from, $date_to, $i_brand, 1);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['i_brand'] = $i_brand;
	
	if ($i_brand != '0') {
		$db2 = $this->load->database('db_external', TRUE);
		$query3	= $db2->query(" SELECT c_brand_code, e_brand_name FROM tr_brand WHERE i_brand = '$i_brand' ");
		$hasilrow = $query3->row();
		$i_brand_code	= $hasilrow->c_brand_code;
		$e_brand_name	= $i_brand_code." - ".$hasilrow->e_brand_name;
	}
	else {
		$e_brand_name = "Semua";
	}
		
	$data['i_brand'] = $i_brand;
	$data['e_brand_name'] = $e_brand_name;
	$this->load->view('template',$data);
  }
  
  function viewlaprealisasi(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'marketing/vviewlaprealisasi';
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	$i_customer = $this->input->post('i_customer', TRUE);  
	
	$data['query'] = $this->mreport->get_laprealisasi($date_from, $date_to, $i_customer, 0);
	
	$data['query2'] = $this->mreport->get_laprealisasi($date_from, $date_to, $i_customer, 1);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['i_customer'] = $i_customer;
	//print_r()
	if ($i_customer != '0') {
		$db2 = $this->load->database('db_external', TRUE);
		$query3	= $db2->query(" SELECT i_customer_code, e_customer_name FROM tr_customer WHERE i_customer = '$i_customer' ");
		$hasilrow = $query3->row();
		$i_customer_code	= $hasilrow->i_customer_code;
		$e_customer_name	= $i_customer_code." - ".$hasilrow->e_customer_name;
	}
	else {
		$e_customer_name = "Semua";
	}
		
	$data['i_customer'] = $i_customer;
	$data['e_customer_name'] = $e_customer_name;
	$this->load->view('template',$data);
  }
  
   function export_excel_laprealisasi_brand_old() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$i_brand = $this->input->post('i_brand', TRUE);
		$e_brand_name = $this->input->post('e_brand_name', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_laprealisasi_brand($date_from, $date_to, $i_brand, 0);
		$query2 = $this->mreport->get_laprealisasi_brand($date_from, $date_to, $i_brand, 1);
				$query3 =$this->mreport->get_perusahaan();
		
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='17' align='center'>".$query3[0]['e_initial_name']."</th>
				 </tr>
				 <tr>
					<th colspan='17' align='center'>DATA REALISASI FC vs OP vs DO berdasarkan Brand ".$e_brand_name."</th>
				 </tr>
				 <tr>
					<th colspan='17' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr>
				 <tr>
					<th colspan='17'>&nbsp;</th>
				 </tr>
				 <tr>
					<th colspan='17' align='left'><b>Barang Reguler</b></th>
				 </tr>
				 
				 ";
		$i=1;
		if (is_array($query)) {
			$temp_kodebrand = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
			$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
			
			// 09-05-2015
			$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
			$tcdata=0; $tsumfc=0; $tsumop=0; $tsumoprp=0; $tsumdo=0; $tsumdorp=0; 
			$tsumpersendovsop=0; $tsumpersenopvsfc=0; $tsumpersendovsfc=0; $tcdatapersendovsop=0; $tsumpendingan=0; $tsumpendinganrp=0;
			// 11-05-2015
			$tcdatapersenopvsfc=0; $tcdatapersendovsfc=0;	
			$sumdropping=0;$tsumdropping=0;$sumdroppingrp=0;$tsumdroppingrp=0;
			for($j=0;$j<count($query);$j++){
				if ($temp_kodebrand != $query[$j]['kode_brand']) {
					$temp_kodebrand = $query[$j]['kode_brand'];
					 
					 $html_data.= "<tr>
						<td colspan='17' align ='center'>&nbsp;<b>"."Brand ".$query[$j]['kode_brand']." - ".$query[$j]['nama_brand']."</b></td>
					</tr>";
					
					$html_data.= "<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					  <th width='5%' colspan='2'>Dropping</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
				 }
				 
				$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td>".$query[$j]['kode_brg_jadi']."</td>
				<td>".$query[$j]['nama_brg_jadi']."</td>
				<td>".$query[$j]['hjp']."</td>
				<td>".$query[$j]['fc']."</td>
				<td>".$query[$j]['jum_op']."</td>
				<td>".$query[$j]['jum_op_rp']."</td>
				<td>";
				if ($query[$j]['persenopvsfc'] != '')
					$html_data.= $query[$j]['persenopvsfc'];
				else
					$html_data.= "";
				$html_data.="</td>
				<td>".$query[$j]['jum_do']."</td>
				<td>".$query[$j]['jum_do_rp']."</td>
				<td>";
				if ($query[$j]['persendovsfc'] != '')
					$html_data.= $query[$j]['persendovsfc'];
				else
					$html_data.="";
				$html_data.="</td>
				
				<td>";
				if ($query[$j]['persendovsop'] != '')
					$html_data.= $query[$j]['persendovsop'];
				else
					$html_data.= "";
				$html_data.="</td>
				<td>".$query[$j]['pendingan']."</td>
				<td>".$query[$j]['pendingan_rp']."</td>
				<td>".$query[$j]['dropping'] ."</td>
				<td>".$query[$j]['dropping_rp'] ."</td>
				<td>&nbsp;</td>
				</tr>";
				
					$sumfc+=$query[$j]['fc'];
					$sumop+=$query[$j]['jum_op'];
					$sumoprp+=$query[$j]['jum_op_rp'];
					$sumdo+=$query[$j]['jum_do'];
					$sumdorp+=$query[$j]['jum_do_rp'];
					$sumdropping+=$query[$j]['dropping'];
					$sumdroppingrp+=$query[$j]['dropping_rp'];
					/*$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
					$sumpersendovsfc+=$query[$j]['persendovsfc'];
					$sumpersendovsop+=$query[$j]['persendovsop']; */
					//$cdatapersenopvsfc, $cdatapersendovsfc, $cdatapersendovsop
					if ($query[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query[$j]['pendingan'];
					$sumpendinganrp+=$query[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc+=$query[$j]['fc'];
					$tsumop+=$query[$j]['jum_op'];
					$tsumoprp+=$query[$j]['jum_op_rp'];
					$tsumdo+=$query[$j]['jum_do'];
					$tsumdorp+=$query[$j]['jum_do_rp'];
					$tsumpendingan+=$query[$j]['pendingan'];
					$tsumpendinganrp+=$query[$j]['pendingan_rp'];
					$tsumdropping+=$query[$j]['dropping'];
					$tsumdroppingrp+=$query[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					
					if (isset($query[$j+1]['kode_brand']) && ($query[$j]['kode_brand'] != $query[$j+1]['kode_brand'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						if ($cdata != 0) {
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumop."</b></td>
							<td align='right'><b>".$sumoprp."</b></td>
							<td align='right'><b>".$ratapersenopvsfc."</b></td>
							<td align='right'><b>".$sumdo."</b></td>
							<td align='right'><b>".$sumdorp."</b></td>
							<td align='right'><b>".$ratapersendovsfc."</b></td>
							<td align='right'><b>".$ratapersendovsop."</b></td>
							<td align='right'><b>".$sumpendingan."</b></td>
							<td align='right'><b>".$sumpendinganrp."</b></td>
							<td align='right'><b>". $sumdropping ."</b></td>
							<td align='right'><b>". $sumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping =0;$sumdroppingrp =0;
						$i=1;
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					else if (!isset($query[$j+1]['kode_brand'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						/*$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersendovsop;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsop; */
						// 11-05-2015
					if ($tcdatapersendovsop != 0)
							$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						else
							$ratapersen = 0;
						
						if ($tcdatapersenopvsfc != 0)	
							$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersenopvsfc;
						else
							$ratapersenopfc = 0;
						
						if ($tcdatapersendovsfc != 0)
							$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsfc;
						else
							$ratapersendofc = 0;
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumop."</b></td>
							<td align='right'><b>".$sumoprp."</b></td>
							<td align='right'><b>".$ratapersenopvsfc."</b></td>
							<td align='right'><b>".$sumdo."</b></td>
							<td align='right'><b>".$sumdorp."</b></td>
							<td align='right'><b>".$ratapersendovsfc."</b></td>
							<td align='right'><b>".$ratapersendovsop."</b></td>
							<td align='right'><b>".$sumpendingan."</b></td>
							<td align='right'><b>".$sumpendinganrp."</b></td>
							<td align='right'><b>".$sumdropping ."</b></td>
							<td align='right'><b>".$sumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL BARANG REGULER</b></td>
							<td align='right'><b>".$tsumfc."</b></td>
							<td align='right'><b>".$tsumop."</b></td>
							<td align='right'><b>".$tsumoprp."</b></td>
							<td align='right'><b>".$ratapersenopfc."</b></td>
							<td align='right'><b>".$tsumdo."</b></td>
							<td align='right'><b>".$tsumdorp."</b></td>
							<td align='right'><b>".$ratapersendofc."</b></td>
							<td align='right'><b>".$ratapersen."</b></td>
							<td align='right'><b>".$tsumpendingan."</b></td>
							<td align='right'><b>".$tsumpendinganrp."</b></td>
							<td align='right'><b>". $tsumdropping ."</b></td>
							<td align='right'><b>". $tsumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
			}
				
		} // end if1
		
		$html_data.="<tr>
					<th colspan='17'>&nbsp;</th>
				 </tr>
				 <tr>
					<th colspan='17' align='left'><b>Barang STP</b></th>
				 </tr>
				 
				 ";
		
		$i=1;
		if (is_array($query2)) {
			$temp_kodebrand = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
				
				// 09-05-2015
				$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
				$tcdata2=0; $tsumfc2=0; $tsumop2=0; $tsumoprp2=0; $tsumdo2=0; $tsumdorp2=0; 
				$tsumpersendovsop2=0; $tsumpersenopvsfc2=0; $tsumpersendovsfc2=0; $tcdatapersendovsop2=0; $tsumpendingan2=0; $tsumpendinganrp2=0;
				// 11-05-2015
				$tcdatapersenopvsfc2=0; $tcdatapersendovsfc2=0;
				$tsumdropping2=0;
				$tsumdroppingrp2=0;
			for($j=0;$j<count($query2);$j++){
				if ($temp_kodebrand != $query2[$j]['kode_brand']) {
					$temp_kodebrand = $query2[$j]['kode_brand'];
					 
					 $html_data.= "<tr>
						<td colspan='17' align ='center'>&nbsp;<b>"."Brand ".$query2[$j]['kode_brand']." - ".$query2[$j]['nama_brand']."</b></td>
					</tr>";
					$html_data.="<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					 <th width='5%' colspan='2'>Dropping</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
				 }
				 
				$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td>".$query2[$j]['kode_brg_jadi']."</td>
				<td>".$query2[$j]['nama_brg_jadi']."</td>
				<td>".$query2[$j]['hjp']."</td>
				<td>".$query2[$j]['fc']."</td>
				<td>".$query2[$j]['jum_op']."</td>
				<td>".$query2[$j]['jum_op_rp']."</td>
				<td>";
				/*if ($query2[$j]['persenopvsfc'] != '')
					$html_data.= $query2[$j]['persenopvsfc'];
				else
					$html_data.= ""; */
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td>".$query2[$j]['jum_do']."</td>
				<td>".$query2[$j]['jum_do_rp']."</td>
				<td>";
				/*if ($query2[$j]['persendovsfc'] != '')
					$html_data.= $query2[$j]['persendovsfc'];
				else
					$html_data.= ""; */
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td>";
				/*if ($query2[$j]['persendovsop'] != '')
					$html_data.=$query2[$j]['persendovsop'];
				else
					$html_data.= ""; */
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td>".$query2[$j]['pendingan']."</td>
				<td>".$query2[$j]['pendingan_rp']."</td>
				 <td align='right'>".$query2[$j]['dropping'] ."</td>
				  <td align='right'>".$query2[$j]['dropping_rp'] ."</td>
				</tr>";
				
					$sumfc+=$query2[$j]['fc'];
					$sumop+=$query2[$j]['jum_op'];
					$sumoprp+=$query2[$j]['jum_op_rp'];
					$sumdo+=$query2[$j]['jum_do'];
					$sumdorp+=$query2[$j]['jum_do_rp'];
					$sumdropping+=$query2[$j]['dropping'];
					$sumdroppingrp+=$query2[$j]['dropping_rp'];
					/*$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
					$sumpersendovsfc+=$query2[$j]['persendovsfc'];
					$sumpersendovsop+=$query2[$j]['persendovsop']; */
					if ($query2[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query2[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query2[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query2[$j]['pendingan'];
					$sumpendinganrp+=$query2[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc2+=$query2[$j]['fc'];
					$tsumop2+=$query2[$j]['jum_op'];
					$tsumoprp2+=$query2[$j]['jum_op_rp'];
					$tsumdo2+=$query2[$j]['jum_do'];
					$tsumdorp2+=$query2[$j]['jum_do_rp'];
					$tsumpendingan2+=$query2[$j]['pendingan'];
					$tsumpendinganrp2+=$query2[$j]['pendingan_rp'];
					$tsumdropping2+=$query2[$j]['dropping'];
					$tsumdroppingrp2+=$query2[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					
					if (isset($query2[$j+1]['kode_brand']) && ($query2[$j]['kode_brand'] != $query2[$j+1]['kode_brand'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						// <td align='right'><b>".$ratapersenopvsfc."</b></td>
						//<td align='right'><b>".$ratapersendovsfc."</b></td>
						//<td align='right'><b>".$ratapersendovsop."</b></td>
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumop."</b></td>
							<td align='right'><b>".$sumoprp."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$sumdo."</b></td>
							<td align='right'><b>".$sumdorp."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$sumpendingan."</b></td>
							<td align='right'><b>".$sumpendinganrp."</b></td>
							<td align='right'><b>". $sumdropping ."</b></td>
							<td align='right'><b>". $sumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping=0;$sumdroppingrp=0;
						$i=1;
					}
					else if (!isset($query2[$j+1]['kode_brand'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						
						$tsumpersendovsop+= $ratapersendovsop;
						$tcdatapersendovsop++;
						
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop; */
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						$ratapersen2 = $tsumpersendovsop2/$tcdatapersendovsop2;
						$ratapersenopfc2 = $tsumpersenopvsfc2/$tcdatapersendovsop2;
						$ratapersendofc2 = $tsumpersendovsfc2/$tcdatapersendovsop2;
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumop."</b></td>
							<td align='right'><b>".$sumoprp."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$sumdo."</b></td>
							<td align='right'><b>".$sumdorp."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$sumpendingan."</b></td>
							<td align='right'><b>".$sumpendinganrp."</b></td>
							<td align=right'><b>". $sumdropping ."</b></td>
							<td align=right'><b>". $sumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						// <td align='right'><b>".$ratapersenopfc2."</b></td>
						// <td align='right'><b>".$ratapersendofc2."</b></td>
						// <td align='right'><b>".$ratapersen2."</b></td>
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL BARANG STP</b></td>
							<td align='right'><b>".$tsumfc2."</b></td>
							<td align='right'><b>".$tsumop2."</b></td>
							<td align='right'><b>".$tsumoprp2."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$tsumdo2."</b></td>
							<td align='right'><b>".$tsumdorp2."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$tsumpendingan2."</b></td>
							<td align='right'><b>".$tsumpendinganrp2."</b></td>
							<td align='right'><b>". $tsumdropping2."</b></td>
							<td align='right'><b>". $tsumdroppingrp2."</b></td>
							<td>&nbsp;</td>
						</tr>";
					}
			} // end for
			
				$allsumfc = $tsumfc + $tsumfc2;
				$allsumop = $tsumop + $tsumop2;
				$allsumoprp = $tsumoprp + $tsumoprp2;
				$allsumdo = $tsumdo + $tsumdo2;
				$allsumdorp = $tsumdorp + $tsumdorp2;
				$allsumpendingan = $tsumpendingan + $tsumpendingan2;
				$allsumpendinganrp = $tsumpendinganrp + $tsumpendinganrp2;
				$allsumdropping = $tsumdropping + $tsumdropping2;
				$allsumdroppingrp = $tsumdroppingrp + $tsumdroppingrp2;
				
				// 11-05-2015
				/*$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				$allratapersen = ($ratapersen+$ratapersen2)/2; */
			
				// 11-05-2015, 25-05-2015 acuan non-stp aja
				//if ($ratapersenopfc!=0 && $ratapersenopfc2!=0)
				//	$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				//else if ($ratapersenopfc2 == 0)
					$allratapersenopfc = $ratapersenopfc;
				//else if ($ratapersenopfc == 0)
				//	$allratapersenopfc = $ratapersenopfc2;
				
				//$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				//if ($ratapersendofc!=0 && $ratapersendofc2!=0)
				//	$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				//else if ($ratapersendofc2 == 0)
					$allratapersendofc = $ratapersendofc;
				//else if ($ratapersendofc == 0)
				//	$allratapersendofc = $ratapersendofc2;
				
				//$allratapersen = ($ratapersen+$ratapersen2)/2;
				//if ($ratapersen!=0 && $ratapersen2!=0)
				//	$allratapersen = ($ratapersen+$ratapersen2)/2;
				//else if ($ratapersen2 == 0)
					$allratapersen = $ratapersen;
				//else if ($ratapersen == 0)
				//	$allratapersen = $ratapersen2;
				
				$html_data.="<tr>
							<td colspan='4' align='center'><b>GRAND TOTAL BARANG AKTIF + STP</b></td>
							<td align='right'><b>".$allsumfc."</b></td>
							<td align='right'><b>".$allsumop."</b></td>
							<td align='right'><b>".$allsumoprp."</b></td>
							<td align='right'><b>".$allratapersenopfc."</b></td>
							<td align='right'><b>".$allsumdo."</b></td>
							<td align='right'><b>".$allsumdorp."</b></td>
							<td align='right'><b>".$allratapersendofc."</b></td>
							<td align='right'><b>".$allratapersen."</b></td>
							<td align='right'><b>".$allsumpendingan."</b></td>
							<td align='right'><b>".$allsumpendinganrp."</b></td>
							<td align='right'><b>". $allsumdropping ."</b></td>
							<td align='right'><b>". $allsumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
				
		} // end if1
		
		$html_data.="</tbody></table><br><br>";
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_realisasi_berdasarkan_brand";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  function export_excel_laprealisasi_brand() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$i_brand = $this->input->post('i_brand', TRUE);
		$e_brand_name = $this->input->post('e_brand_name', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_laprealisasi_brand($date_from, $date_to, $i_brand, 0);
		$query2 = $this->mreport->get_laprealisasi_brand($date_from, $date_to, $i_brand, 1);
			$query3 =$this->mreport->get_perusahaan();
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='17' align='center'>".$query3[0]['e_initial_name']."</th>
				 </tr>
				 <tr>
					<th colspan='17' align='center'>DATA REALISASI FC vs OP vs DO BERDASARKAN BRAND ".$e_brand_name."</th>
				 </tr>
				 <tr>
					<th colspan='17' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr>
				 <tr>
					<th colspan='17'>&nbsp;</th>
				 </tr>
				 <tr>
					<th colspan='17' align='left'><b>Barang Reguler</b></th>
				 </tr>
				 
				 ";
		$i=1;
		if (is_array($query)) {
			$temp_kodekel = ""; $temp_kodebrand = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
			$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
			
			// 09-05-2015
			$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
			$tcdata=0; $tsumfc=0; $tsumop=0; $tsumoprp=0; $tsumdo=0; $tsumdorp=0; 
			$tsumpersendovsop=0; $tsumpersenopvsfc=0; $tsumpersendovsfc=0; $tcdatapersendovsop=0; $tsumpendingan=0; $tsumpendinganrp=0;
			// 11-05-2015
			$tcdatapersenopvsfc=0; $tcdatapersendovsfc=0;	
			$sumdropping=0;$tsumdropping=0;$sumdroppingrp=0;$tsumdroppingrp=0;
			for($j=0;$j<count($query);$j++){
				
				if ($temp_kodebrand != $query[$j]['kode_brand']) {
						$temp_kodebrand = $query[$j]['kode_brand'];
						
			 $html_data.= "<tr>
						<td colspan='17' align ='center'>&nbsp;<b> "."Brand ".$query[$j]['kode_brand']." - ".$query[$j]['nama_brand']." </b></td>
				</tr>";
					
				}
				
				if ($temp_kodekel != $query[$j]['kode_kel']) {
					$temp_kodekel = $query[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='17'>&nbsp;<b>".$query[$j]['kode_kel']." - ".$query[$j]['nama_kel']."</b></td>
					</tr>";
					
					$html_data.= "<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					  <th width='5%' colspan='2'>Dropping</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
				 }
				 
					$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td align='left'>".$query[$j]['kode_brg_jadi']."</td>
				<td align='left'>".$query[$j]['nama_brg_jadi']."</td>
				<td align='left'>"."Rp. ".number_format($query[$j]['hjp'], 2, '.',',')."</td>
				<td align='center'>".$query[$j]['fc']."</td>
				<td align='center'>".$query[$j]['jum_op']."</td>
				<td align='left'>"."Rp. ".number_format($query[$j]['jum_op_rp'], 2, '.',',')."</td>
				<td align='center'>";
				if ($query[$j]['persenopvsfc'] != '')
					$html_data.= number_format($query[$j]['persenopvsfc'], 2, '.',',')."%";
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td align='center'>".$query[$j]['jum_do']."</td>
				<td align='left'>"."Rp. ".number_format($query[$j]['jum_do_rp'], 2, '.',',')."</td>
				<td align='center'>";
				if ($query[$j]['persendovsfc'] != '')
					$html_data.= number_format($query[$j]['persendovsfc'], 2, '.',',')."%";
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>";
				if ($query[$j]['persendovsop'] != '')
					$html_data.=number_format($query[$j]['persendovsop'], 2, '.',',')."%";
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>".$query[$j]['pendingan']."</td>
				<td align='left'>"."Rp. ".number_format($query[$j]['pendingan_rp'], 2, '.',',')."</td>
				 <td align='center'>".$query[$j]['dropping'] ."</td>
				  <td align='left'>"."Rp. ".number_format($query[$j]['dropping_rp'], 2, '.',',') ."</td>
				</tr>";
				
				
					$sumfc+=$query[$j]['fc'];
					$sumop+=$query[$j]['jum_op'];
					$sumoprp+=$query[$j]['jum_op_rp'];
					$sumdo+=$query[$j]['jum_do'];
					$sumdorp+=$query[$j]['jum_do_rp'];
					$sumdropping+=$query[$j]['dropping'];
					$sumdroppingrp+=$query[$j]['dropping_rp'];
					/*$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
					$sumpersendovsfc+=$query[$j]['persendovsfc'];
					$sumpersendovsop+=$query[$j]['persendovsop']; */
					//$cdatapersenopvsfc, $cdatapersendovsfc, $cdatapersendovsop
					if ($query[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query[$j]['pendingan'];
					$sumpendinganrp+=$query[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc+=$query[$j]['fc'];
					$tsumop+=$query[$j]['jum_op'];
					$tsumoprp+=$query[$j]['jum_op_rp'];
					$tsumdo+=$query[$j]['jum_do'];
					$tsumdorp+=$query[$j]['jum_do_rp'];
					$tsumpendingan+=$query[$j]['pendingan'];
					$tsumpendinganrp+=$query[$j]['pendingan_rp'];
					$tsumdropping+=$query[$j]['dropping'];
					$tsumdroppingrp+=$query[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					
					if (isset($query[$j+1]['kode_kel']) && ($query[$j]['kode_kel'] != $query[$j+1]['kode_kel'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						if ($cdata != 0) {
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='center'><b>".$sumfc."</b></td>
							<td align='center'><b>".$sumop."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumoprp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersenopvsfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$sumdo."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumdorp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendovsfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".number_format($ratapersendovsop, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$sumpendingan."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumpendinganrp, 2, '.',',')."</b></td>
							<td align='center'><b>". $sumdropping ."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumdroppingrp, 2, '.',',') ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping =0;$sumdroppingrp =0;
						$i=1;
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					else if (!isset($query[$j+1]['kode_kel'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						/*$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersendovsop;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsop; */
						// 11-05-2015
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersenopvsfc;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsfc;
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='center'><b>".$sumfc."</b></td>
							<td align='center'><b>".$sumop."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumoprp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersenopvsfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$sumdo."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumdorp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendovsfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".number_format($ratapersendovsop, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$sumpendingan."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumpendinganrp, 2, '.',',')."</b></td>
							<td align='center'><b>". $sumdropping ."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumdroppingrp, 2, '.',',') ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL BARANG STP</b></td>
							<td align='center'><b>".$tsumfc."</b></td>
							<td align='center'><b>".$tsumop."</b></td>
							<td align='left'><b>"."Rp. ".number_format($tsumoprp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersenopfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$tsumdo."</b></td>
							<td align='left'><b>"."Rp. ".number_format($tsumdorp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendofc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".number_format($ratapersen, 2, '.',',')."%"."</b></td>	
							<td align='center'><b>".$tsumpendingan."</b></td>
							<td align='left'><b>"."Rp. ".number_format($tsumpendinganrp, 2, '.',',')."</b></td>
							<td align='center'><b>". $tsumdropping."</b></td>
							<td align='left'><b>"."Rp. ".number_format($tsumdroppingrp, 2, '.',',')."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
			}
				
		} // end if1
		
		$html_data.="<tr>
					<th colspan='17'>&nbsp;</th>
				 </tr>
				 <tr>
					<th colspan='17' align='left'><b>Barang STP</b></th>
				 </tr>
				 
				 ";
		
		$i=1;
		if (is_array($query2)) {
			$temp_kodekel = ""; $temp_kodebrand="";$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
				
				// 09-05-2015
				$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
				$tcdata2=0; $tsumfc2=0; $tsumop2=0; $tsumoprp2=0; $tsumdo2=0; $tsumdorp2=0; 
				$tsumpersendovsop2=0; $tsumpersenopvsfc2=0; $tsumpersendovsfc2=0; $tcdatapersendovsop2=0; $tsumpendingan2=0; $tsumpendinganrp2=0;
				// 11-05-2015
				$tcdatapersenopvsfc2=0; $tcdatapersendovsfc2=0;
				$tsumdropping2=0;
				$tsumdroppingrp2=0;
			for($j=0;$j<count($query2);$j++){
				
				if ($temp_kodebrand != $query2[$j]['kode_brand']) {
						$temp_kodebrand = $query2[$j]['kode_brand'];
						
			 $html_data.= "<tr>
						<td colspan='17' align ='center'>&nbsp;<b> "."Brand ".$query2[$j]['kode_brand']." - ".$query2[$j]['nama_brand']." </b></td>
				</tr>";
					
				}
				if ($temp_kodekel != $query2[$j]['kode_kel']) {
					$temp_kodekel = $query2[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='17'>&nbsp;<b>".$query2[$j]['kode_kel']." - ".$query2[$j]['nama_kel']."</b></td>
					</tr>";
					$html_data.="<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					 <th width='5%' colspan='2'>Dropping</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
				 }
				 
				$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td align='left'>".$query2[$j]['kode_brg_jadi']."</td>
				<td align='left'>".$query2[$j]['nama_brg_jadi']."</td>
				<td align='left'>"."Rp. ".number_format($query2[$j]['hjp'], 2, '.',',')."</td>
				<td align='center'>".$query2[$j]['fc']."</td>
				<td align='center'>".$query2[$j]['jum_op']."</td>
				<td align='left'>"."Rp. ".number_format($query2[$j]['jum_op_rp'], 2, '.',',')."</td>
				<td align='center'>";
				if ($query2[$j]['persenopvsfc'] != '')
					$html_data.= number_format($query2[$j]['persenopvsfc'], 2, '.',',')."%";
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td align='center'>".$query2[$j]['jum_do']."</td>
				<td align='left'>"."Rp. ".number_format($query2[$j]['jum_do_rp'], 2, '.',',')."</td>
				<td align='center'>";
				if ($query2[$j]['persendovsfc'] != '')
					$html_data.= number_format($query2[$j]['persendovsfc'], 2, '.',',')."%";
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>";
				if ($query2[$j]['persendovsop'] != '')
					$html_data.=number_format($query2[$j]['persendovsop'], 2, '.',',')."%";
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>".$query2[$j]['pendingan']."</td>
				<td align='left'>"."Rp. ".number_format($query2[$j]['pendingan_rp'], 2, '.',',')."</td>
				 <td align='center'>".$query2[$j]['dropping'] ."</td>
				  <td align='left'>"."Rp. ".number_format($query2[$j]['dropping_rp'], 2, '.',',') ."</td>
				</tr>";
				
					$sumfc+=$query2[$j]['fc'];
					$sumop+=$query2[$j]['jum_op'];
					$sumoprp+=$query2[$j]['jum_op_rp'];
					$sumdo+=$query2[$j]['jum_do'];
					$sumdorp+=$query2[$j]['jum_do_rp'];
					$sumdropping+=$query2[$j]['dropping'];
					$sumdroppingrp+=$query2[$j]['dropping_rp'];
					//$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
					//$sumpersendovsfc+=$query2[$j]['persendovsfc'];
					//$sumpersendovsop+=$query2[$j]['persendovsop']; 
					if ($query2[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query2[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query2[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query2[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query2[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
				
					$sumpendingan+=$query2[$j]['pendingan'];
					$sumpendinganrp+=$query2[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc2+=$query2[$j]['fc'];
					$tsumop2+=$query2[$j]['jum_op'];
					$tsumoprp2+=$query2[$j]['jum_op_rp'];
					$tsumdo2+=$query2[$j]['jum_do'];
					$tsumdorp2+=$query2[$j]['jum_do_rp'];
					$tsumpendingan2+=$query2[$j]['pendingan'];
					$tsumpendinganrp2+=$query2[$j]['pendingan_rp'];
					$tsumdropping2+=$query2[$j]['dropping'];
					$tsumdroppingrp2+=$query2[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					
					if (isset($query2[$j+1]['kode_kel']) && ($query2[$j]['kode_kel'] != $query2[$j+1]['kode_kel'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0){
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
								}
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
					//	$tsumpersendovsop2+= $ratapersendovsop;
					//	$tsumpersenopvsfc2+= $ratapersenopvsfc;
					//	$tsumpersendovsfc2+= $ratapersendovsfc;
					//	$tcdatapersendovsop2++; 
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						// <td align='left'><b>".$ratapersenopvsfc."</b></td>
						//<td align='left'><b>".$ratapersendovsfc."</b></td>
						//<td align='left'><b>".$ratapersendovsop."</b></td>
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='center'><b>".$sumfc."</b></td>
							<td align='center'><b>".$sumop."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumoprp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersenopvsfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$sumdo."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumdorp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendovsfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".number_format($ratapersendovsop, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$sumpendingan."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumpendinganrp, 2, '.',',')."</b></td>
							<td align='center'><b>". $sumdropping ."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumdroppingrp, 2, '.',',') ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping=0;$sumdroppingrp=0;
						$i=1;
							$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					else if (!isset($query2[$j+1]['kode_kel'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						
						$tsumpersendovsop+= $ratapersendovsop;
						$tcdatapersendovsop++;
						
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop; */
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						$ratapersen2 = $tsumpersendovsop2/$tcdatapersendovsop2;
						$ratapersenopfc2 = $tsumpersenopvsfc2/$tcdatapersendovsop2;
						$ratapersendofc2 = $tsumpersendovsfc2/$tcdatapersendovsop2;
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='center'><b>".$sumfc."</b></td>
							<td align='center'><b>".$sumop."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumoprp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersenopvsfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$sumdo."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumdorp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendovsfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".number_format($ratapersendovsop, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$sumpendingan."</b></td>
							<td align='left'><b>"."Rp. ".number_format($sumpendinganrp, 2, '.',',')."</b></td>
							<td align='center'><b>". $sumdropping ."</b></td>
							<td align='left'><b>"."Rp. ".number_format( $sumdroppingrp , 2, '.',',')."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						// <td align='left'><b>".$ratapersenopfc2."</b></td>
						// <td align='left'><b>".$ratapersendofc2."</b></td>
						// <td align='left'><b>".$ratapersen2."</b></td>
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL BARANG STP</b></td>
							<td align='center'><b>".$tsumfc2."</b></td>
							<td align='center'><b>".$tsumop2."</b></td>
							<td align='left'><b>"."Rp. ".number_format($tsumoprp2, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersenopfc2, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$tsumdo2."</b></td>
							<td align='left'><b>"."Rp. ".number_format($tsumdorp2, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendofc2, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".number_format($ratapersen2, 2, '.',',')."%"."</b></td>	
							<td align='center'><b>".$tsumpendingan2."</b></td>
							<td align='left'><b>"."Rp. ".number_format($tsumpendinganrp2, 2, '.',',')."</b></td>
							<td align='center'><b>". $tsumdropping2."</b></td>
							<td align='left'><b>"."Rp. ".number_format($tsumdroppingrp2, 2, '.',',')."</b></td>
							<td>&nbsp;</td>
						</tr>";
					}
			} // end for
			
				$allsumfc = $tsumfc + $tsumfc2;
				$allsumop = $tsumop + $tsumop2;
				$allsumoprp = $tsumoprp + $tsumoprp2;
				$allsumdo = $tsumdo + $tsumdo2;
				$allsumdorp = $tsumdorp + $tsumdorp2;
				$allsumpendingan = $tsumpendingan + $tsumpendingan2;
				$allsumpendinganrp = $tsumpendinganrp + $tsumpendinganrp2;
				$allsumdropping = $tsumdropping + $tsumdropping2;
				$allsumdroppingrp = $tsumdroppingrp + $tsumdroppingrp2;
				
				// 11-05-2015
				/*$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				$allratapersen = ($ratapersen+$ratapersen2)/2; */
			
				// 11-05-2015, 25-05-2015 acuan non-stp aja
				if ($ratapersenopfc!=0 && $ratapersenopfc2!=0)
					$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				else if ($ratapersenopfc2 == 0)
					$allratapersenopfc = $ratapersenopfc;
				else if ($ratapersenopfc == 0)
					$allratapersenopfc = $ratapersenopfc2;
				
				//$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				if ($ratapersendofc!=0 && $ratapersendofc2!=0)
					$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				else if ($ratapersendofc2 == 0)
					$allratapersendofc = $ratapersendofc;
				else if ($ratapersendofc == 0)
					$allratapersendofc = $ratapersendofc2;
				
				//$allratapersen = ($ratapersen+$ratapersen2)/2;
				if ($ratapersen!=0 && $ratapersen2!=0)
					$allratapersen = ($ratapersen+$ratapersen2)/2;
				else if ($ratapersen2 == 0)
					$allratapersen = $ratapersen;
				else if ($ratapersen == 0)
					$allratapersen = $ratapersen2;
				
				$html_data.="<tr>
							<td colspan='4' align='center'><b>GRAND TOTAL BARANG REGULER + STP</b></td>
							<td align='center'><b>".$allsumfc."</b></td>
							<td align='center'><b>".$allsumop."</b></td>
							<td align='left'><b>"."Rp. ".number_format($allsumoprp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($allratapersenopfc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$allsumdo."</b></td>
							<td align='left'><b>"."Rp. ".number_format($allsumdorp, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($allratapersendofc, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".number_format($allratapersen, 2, '.',',')."%"."</b></td>
							<td align='center'><b>".$allsumpendingan."</b></td>
							<td align='left'><b>"."Rp. ".number_format($allsumpendinganrp, 2, '.',',')."</b></td>
							<td align='center'><b>". $allsumdropping ."</b></td>
							<td align='left'><b>"."Rp. ". number_format($allsumdroppingrp, 2, '.',',') ."</b></td>
							<td>&nbsp;</td>
						</tr>";
				
		} // end if1
		
		$html_data.="</tbody></table><br><br>";
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_realisasi_berdasarkan_brand";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
   function export_excel_laprealisasi() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$i_customer = $this->input->post('i_customer', TRUE);
		$e_customer_name = $this->input->post('e_customer_name', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_laprealisasi($date_from, $date_to, $i_customer, 0);
		$query2 = $this->mreport->get_laprealisasi($date_from, $date_to, $i_customer, 1);
		$query3 =$this->mreport->get_perusahaan();
		
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='17' align='center'>".$query3[0]['e_initial_name']."</th>
				 </tr>
				 <tr>
					<th colspan='17' align='center'>DATA REALISASI FC vs OP vs DO BERDASARKAN PELANGGAN ".$e_customer_name."</th>
				 </tr>
				 <tr>
					<th colspan='17' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr>
				 <tr>
					<th colspan='17'>&nbsp;</th>
				 </tr>
				 <tr>
					<th colspan='17' align='left'><b>Barang Reguler</b></th>
				 </tr>
				 
				 ";
		$i=1;
		if (is_array($query)) {
			$temp_kodekel = ""; $temp_kodebrand = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
			$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
			
			// 09-05-2015
			$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
			$tcdata=0; $tsumfc=0; $tsumop=0; $tsumoprp=0; $tsumdo=0; $tsumdorp=0; 
			$tsumpersendovsop=0; $tsumpersenopvsfc=0; $tsumpersendovsfc=0; $tcdatapersendovsop=0; $tsumpendingan=0; $tsumpendinganrp=0;
			// 11-05-2015
			$tcdatapersenopvsfc=0; $tcdatapersendovsfc=0;	
			$sumdropping=0;$tsumdropping=0;$sumdroppingrp=0;$tsumdroppingrp=0;
			for($j=0;$j<count($query);$j++){
				
				if ($temp_kodebrand != $query[$j]['kode_brand']) {
						$temp_kodebrand = $query[$j]['kode_brand'];
						
			 $html_data.= "<tr>
						<td colspan='17' align ='center'>&nbsp;<b> "."Brand ".$query[$j]['kode_brand']." - ".$query[$j]['nama_brand']." </b></td>
				</tr>";
					
				}
				
				if ($temp_kodekel != $query[$j]['kode_kel']) {
					$temp_kodekel = $query[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='17'>&nbsp;<b>".$query[$j]['kode_kel']." - ".$query[$j]['nama_kel']."</b></td>
					</tr>";
					
					$html_data.= "<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					  <th width='5%' colspan='2'>Dropping</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
				 }
				 
					$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td align='left'>".$query[$j]['kode_brg_jadi']."</td>
				<td align='left'>".$query[$j]['nama_brg_jadi']."</td>
				<td align='left'>".$query[$j]['hjp']."</td>
				<td align='center'>".$query[$j]['fc']."</td>
				<td align='center'>".$query[$j]['jum_op']."</td>
				<td align='left'>". number_format($query[$j]['jum_op_rp'], 0, '','')."</td>
				<td align='center'>";
				if ($query[$j]['persenopvsfc'] != '')
					$html_data.= number_format($query[$j]['persenopvsfc'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td align='center'>".$query[$j]['jum_do']."</td>
				<td align='left'>".$query[$j]['jum_do_rp']."</td>
				<td align='center'>";
				if ($query[$j]['persendovsfc'] != '')
					$html_data.= number_format($query[$j]['persendovsfc'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>";
				if ($query[$j]['persendovsop'] != '')
					$html_data.=number_format($query[$j]['persendovsop'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>".$query[$j]['pendingan']."</td>
				<td align='left'>".$query[$j]['pendingan_rp']."</td>";
				 if($query[$j]['fc']==0 &&$query[$j]['jum_op'] != 0 ){
						 $query[$j]['dropping'] =0; $query[$j]['dropping_rp'] =0;
					  $html_data.= "
					  <td align='center' >".  $query[$j]['dropping'] ."&nbsp;</td>
					  <td align='center'  >".  $query[$j]['dropping_rp'] ."&nbsp;</td>";
					 }
					  elseif($query[$j]['fc'] < $query[$j]['jum_op'] ){
						   $query[$j]['dropping'] =0; $query[$j]['dropping_rp'] =0;
					  $html_data.= "
					  <td align='center'  >". $query[$j]['dropping'] ."&nbsp;</td>
					  <td align='center'  >". number_format($query[$j]['dropping_rp'], 2, ',','.') ."&nbsp;</td>";
					 }
					  else{
					$html_data.= "
					 <td align='right' >". $query[$j]['dropping'] ."&nbsp;</td>
					 <td align='right' >". number_format($query[$j]['dropping_rp'], 2, ',','.') ."&nbsp;</td>";
				}
			$html_data.="</tr>";
				
				
					$sumfc+=$query[$j]['fc'];
					$sumop+=$query[$j]['jum_op'];
					$sumoprp+=$query[$j]['jum_op_rp'];
					$sumdo+=$query[$j]['jum_do'];
					$sumdorp+=$query[$j]['jum_do_rp'];
					$sumdropping+=$query[$j]['dropping'];
					$sumdroppingrp+=$query[$j]['dropping_rp'];
					/*$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
					$sumpersendovsfc+=$query[$j]['persendovsfc'];
					$sumpersendovsop+=$query[$j]['persendovsop']; */
					//$cdatapersenopvsfc, $cdatapersendovsfc, $cdatapersendovsop
					if ($query[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query[$j]['pendingan'];
					$sumpendinganrp+=$query[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc+=$query[$j]['fc'];
					$tsumop+=$query[$j]['jum_op'];
					$tsumoprp+=$query[$j]['jum_op_rp'];
					$tsumdo+=$query[$j]['jum_do'];
					$tsumdorp+=$query[$j]['jum_do_rp'];
					$tsumpendingan+=$query[$j]['pendingan'];
					$tsumpendinganrp+=$query[$j]['pendingan_rp'];
					$tsumdropping+=$query[$j]['dropping'];
					$tsumdroppingrp+=$query[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					
					if (isset($query[$j+1]['kode_kel']) && ($query[$j]['kode_kel'] != $query[$j+1]['kode_kel'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						if ($cdata != 0) {
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='center'><b>".$sumfc."</b></td>
							<td align='center'><b>".$sumop."</b></td>
							<td align='left'><b>".$sumoprp."</b></td>
							<td align='center'><b>".number_format($ratapersenopvsfc, 2, '.',',')."</b></td>
							<td align='center'><b>".$sumdo."</b></td>
							<td align='left'><b>".$sumdorp."</b></td>
							<td align='center'><b>".number_format($ratapersendovsfc, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendovsop, 2, '.',',')."</b></td>
							<td align='center'><b>".$sumpendingan."</b></td>
							<td align='left'><b>".$sumpendinganrp."</b></td>
							<td align='center'><b>". $sumdropping ."</b></td>
							<td align='left'><b>".$sumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping =0;$sumdroppingrp =0;
						$i=1;
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					else if (!isset($query[$j+1]['kode_kel'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						/*$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersendovsop;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsop; */
						// 11-05-2015
						if($tcdatapersendovsop != 0){
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
					}
					if($tcdatapersenopvsfc != 0){
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersenopvsfc;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsfc;
					}
					else{
					$ratapersenopfc=0;
					$ratapersendofc=0;
				}
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='center'><b>".$sumfc."</b></td>
							<td align='center'><b>".$sumop."</b></td>
							<td align='left'><b>".$sumoprp."</b></td>
							<td align='center'><b>".number_format($ratapersenopvsfc, 2, '.',',')."</b></td>
							<td align='center'><b>".$sumdo."</b></td>
							<td align='left'><b>".$sumdorp."</b></td>
							<td align='center'><b>".number_format($ratapersendovsfc, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendovsop, 2, '.',',')."</b></td>
							<td align='center'><b>".$sumpendingan."</b></td>
							<td align='left'><b>".$sumpendinganrp."</b></td>
							<td align='center'><b>". $sumdropping ."</b></td>
							<td align='left'><b>".$sumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL BARANG REGULER</b></td>
							<td align='center'><b>".$tsumfc."</b></td>
							<td align='center'><b>".$tsumop."</b></td>
							<td align='left'><b>".$tsumoprp."</b></td>
							<td align='center'><b>".number_format($ratapersenopfc, 2, '.',',')."</b></td>
							<td align='center'><b>".$tsumdo."</b></td>
							<td align='left'><b>".$tsumdorp."</b></td>
							<td align='center'><b>".number_format($ratapersendofc, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersen, 2, '.',',')."</b></td>	
							<td align='center'><b>".$tsumpendingan."</b></td>
							<td align='left'><b>".$tsumpendinganrp."</b></td>
							<td align='center'><b>". $tsumdropping."</b></td>
							<td align='left'><b>".$tsumdroppingrp."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
			}
				
		} // end if1
		
		$html_data.="<tr>
					<th colspan='17'>&nbsp;</th>
				 </tr>
				 <tr>
					<th colspan='17' align='left'><b>Barang STP</b></th>
				 </tr>
				 
				 ";
		
		$i=1;
		if (is_array($query2)) {
			$temp_kodekel = ""; $temp_kodebrand="";$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
				
				// 09-05-2015
				$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
				$tcdata2=0; $tsumfc2=0; $tsumop2=0; $tsumoprp2=0; $tsumdo2=0; $tsumdorp2=0; 
				$tsumpersendovsop2=0; $tsumpersenopvsfc2=0; $tsumpersendovsfc2=0; $tcdatapersendovsop2=0; $tsumpendingan2=0; $tsumpendinganrp2=0;
				// 11-05-2015
				$tcdatapersenopvsfc2=0; $tcdatapersendovsfc2=0;
				$tsumdropping2=0;
				$tsumdroppingrp2=0;
			for($j=0;$j<count($query2);$j++){
				
				if ($temp_kodebrand != $query2[$j]['kode_brand']) {
						$temp_kodebrand = $query2[$j]['kode_brand'];
						
			 $html_data.= "<tr>
						<td colspan='17' align ='center'>&nbsp;<b> "."Brand ".$query2[$j]['kode_brand']." - ".$query2[$j]['nama_brand']." </b></td>
				</tr>";
					
				}
				if ($temp_kodekel != $query2[$j]['kode_kel']) {
					$temp_kodekel = $query2[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='17'>&nbsp;<b>".$query2[$j]['kode_kel']." - ".$query2[$j]['nama_kel']."</b></td>
					</tr>";
					$html_data.="<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					 <th width='5%' colspan='2'>Dropping</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
				 }
				 
				$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td align='left'>".$query2[$j]['kode_brg_jadi']."</td>
				<td align='left'>".$query2[$j]['nama_brg_jadi']."</td>
				<td align='left'>".$query2[$j]['hjp']."</td>
				<td align='center'>".$query2[$j]['fc']."</td>
				<td align='center'>".$query2[$j]['jum_op']."</td>
				<td align='left'>".$query2[$j]['jum_op_rp']."</td>
				<td align='center'>";
				if ($query2[$j]['persenopvsfc'] != '')
					$html_data.= number_format($query2[$j]['persenopvsfc'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td align='center'>".$query2[$j]['jum_do']."</td>
				<td align='left'>".$query2[$j]['jum_do_rp']."</td>
				<td align='center'>";
				if ($query2[$j]['persendovsfc'] != '')
					$html_data.= number_format($query2[$j]['persendovsfc'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>";
				if ($query2[$j]['persendovsop'] != '')
					$html_data.=number_format($query2[$j]['persendovsop'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>".$query2[$j]['pendingan']."</td>
				<td align='left'>".$query2[$j]['pendingan_rp']."</td>
				 <td align='center'>".$query2[$j]['dropping'] ."</td>
				  <td align='left'>".$query2[$j]['dropping_rp'] ."</td>
				</tr>";
				
					$sumfc+=$query2[$j]['fc'];
					$sumop+=$query2[$j]['jum_op'];
					$sumoprp+=$query2[$j]['jum_op_rp'];
					$sumdo+=$query2[$j]['jum_do'];
					$sumdorp+=$query2[$j]['jum_do_rp'];
					$sumdropping+=$query2[$j]['dropping'];
					$sumdroppingrp+=$query2[$j]['dropping_rp'];
					//$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
					//$sumpersendovsfc+=$query2[$j]['persendovsfc'];
					//$sumpersendovsop+=$query2[$j]['persendovsop']; 
					if ($query2[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query2[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query2[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query2[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query2[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
				
					$sumpendingan+=$query2[$j]['pendingan'];
					$sumpendinganrp+=$query2[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc2+=$query2[$j]['fc'];
					$tsumop2+=$query2[$j]['jum_op'];
					$tsumoprp2+=$query2[$j]['jum_op_rp'];
					$tsumdo2+=$query2[$j]['jum_do'];
					$tsumdorp2+=$query2[$j]['jum_do_rp'];
					$tsumpendingan2+=$query2[$j]['pendingan'];
					$tsumpendinganrp2+=$query2[$j]['pendingan_rp'];
					$tsumdropping2+=$query2[$j]['dropping'];
					$tsumdroppingrp2+=$query2[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					
					if (isset($query2[$j+1]['kode_kel']) && ($query2[$j]['kode_kel'] != $query2[$j+1]['kode_kel'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0){
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
								}
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
					//	$tsumpersendovsop2+= $ratapersendovsop;
					//	$tsumpersenopvsfc2+= $ratapersenopvsfc;
					//	$tsumpersendovsfc2+= $ratapersendovsfc;
					//	$tcdatapersendovsop2++; 
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						// <td align='left'><b>".$ratapersenopvsfc."</b></td>
						//<td align='left'><b>".$ratapersendovsfc."</b></td>
						//<td align='left'><b>".$ratapersendovsop."</b></td>
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='center'><b>".$sumfc."</b></td>
							<td align='center'><b>".$sumop."</b></td>
							<td align='left'><b>".$sumoprp."</b></td>
							<td align='center'><b>".number_format($ratapersenopvsfc, 2, '.',',')."</b></td>
							<td align='center'><b>".$sumdo."</b></td>
							<td align='left'><b>".$sumdorp."</b></td>
							<td align='center'><b>".number_format($ratapersendovsfc, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendovsop, 2, '.',',')."</b></td>
							<td align='center'><b>".$sumpendingan."</b></td>
							<td align='left'><b>".$sumpendinganrp."</b></td>
							<td align='center'><b>". $sumdropping ."</b></td>
							<td align='left'><b>".$sumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping=0;$sumdroppingrp=0;
						$i=1;
							$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					else if (!isset($query2[$j+1]['kode_kel'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						
						$tsumpersendovsop+= $ratapersendovsop;
						$tcdatapersendovsop++;
						
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop; */
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						$ratapersen2 = $tsumpersendovsop2/$tcdatapersendovsop2;
						$ratapersenopfc2 = $tsumpersenopvsfc2/$tcdatapersendovsop2;
						$ratapersendofc2 = $tsumpersendovsfc2/$tcdatapersendovsop2;
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='center'><b>".$sumfc."</b></td>
							<td align='center'><b>".$sumop."</b></td>
							<td align='left'><b>".$sumoprp."</b></td>
							<td align='center'><b>".number_format($ratapersenopvsfc, 2, '.',',')."</b></td>
							<td align='center'><b>".$sumdo."</b></td>
							<td align='left'><b>".$sumdorp."</b></td>
							<td align='center'><b>".number_format($ratapersendovsfc, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersendovsop, 2, '.',',')."</b></td>
							<td align='center'><b>".$sumpendingan."</b></td>
							<td align='left'><b>".$sumpendinganrp."</b></td>
							<td align='center'><b>". $sumdropping ."</b></td>
							<td align='left'><b>". $sumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						// <td align='left'><b>".$ratapersenopfc2."</b></td>
						// <td align='left'><b>".$ratapersendofc2."</b></td>
						// <td align='left'><b>".$ratapersen2."</b></td>
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL BARANG STP</b></td>
							<td align='center'><b>".$tsumfc2."</b></td>
							<td align='center'><b>".$tsumop2."</b></td>
							<td align='left'><b>".$tsumoprp2."</b></td>
							<td align='center'><b>".number_format($ratapersenopfc2, 2, '.',',')."</b></td>
							<td align='center'><b>".$tsumdo2."</b></td>
							<td align='left'><b>".$tsumdorp2."</b></td>
							<td align='center'><b>".number_format($ratapersendofc2, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($ratapersen2, 2, '.',',')."</b></td>	
							<td align='center'><b>".$tsumpendingan2."</b></td>
							<td align='left'><b>".$tsumpendinganrp2."</b></td>
							<td align='center'><b>". $tsumdropping2."</b></td>
							<td align='left'><b>".$tsumdroppingrp2."</b></td>
							<td>&nbsp;</td>
						</tr>";
					}
			} // end for
			
				$allsumfc = $tsumfc + $tsumfc2;
				$allsumop = $tsumop + $tsumop2;
				$allsumoprp = $tsumoprp + $tsumoprp2;
				$allsumdo = $tsumdo + $tsumdo2;
				$allsumdorp = $tsumdorp + $tsumdorp2;
				$allsumpendingan = $tsumpendingan + $tsumpendingan2;
				$allsumpendinganrp = $tsumpendinganrp + $tsumpendinganrp2;
				$allsumdropping = $tsumdropping + $tsumdropping2;
				$allsumdroppingrp = $tsumdroppingrp + $tsumdroppingrp2;
				
				// 11-05-2015
				/*$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				$allratapersen = ($ratapersen+$ratapersen2)/2; */
			
				// 11-05-2015, 25-05-2015 acuan non-stp aja
				if ($ratapersenopfc!=0 && $ratapersenopfc2!=0)
					$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				else if ($ratapersenopfc2 == 0)
					$allratapersenopfc = $ratapersenopfc;
				else if ($ratapersenopfc == 0)
					$allratapersenopfc = $ratapersenopfc2;
				
				//$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				if ($ratapersendofc!=0 && $ratapersendofc2!=0)
					$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				else if ($ratapersendofc2 == 0)
					$allratapersendofc = $ratapersendofc;
				else if ($ratapersendofc == 0)
					$allratapersendofc = $ratapersendofc2;
				
				//$allratapersen = ($ratapersen+$ratapersen2)/2;
				if ($ratapersen!=0 && $ratapersen2!=0)
					$allratapersen = ($ratapersen+$ratapersen2)/2;
				else if ($ratapersen2 == 0)
					$allratapersen = $ratapersen;
				else if ($ratapersen == 0)
					$allratapersen = $ratapersen2;
				
				$html_data.="<tr>
							<td colspan='4' align='center'><b>GRAND TOTAL BARANG REGULER + STP</b></td>
							<td align='center'><b>".$allsumfc."</b></td>
							<td align='center'><b>".$allsumop."</b></td>
							<td align='left'><b>".$allsumoprp."</b></td>
							<td align='center'><b>".number_format($allratapersenopfc, 2, '.',',')."</b></td>
							<td align='center'><b>".$allsumdo."</b></td>
							<td align='left'><b>".$allsumdorp."</b></td>
							<td align='center'><b>".number_format($allratapersendofc, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($allratapersen, 2, '.',',')."</b></td>
							<td align='center'><b>".$allsumpendingan."</b></td>
							<td align='left'><b>".$allsumpendinganrp."</b></td>
							<td align='center'><b>". $allsumdropping ."</b></td>
							<td align='left'><b>". $allsumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
				
		} // end if1
		
		$html_data.="</tbody></table><br><br>";
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_realisasi_berdasarkan_pelanggan";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  function export_excel_laprealisasi_tot() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$i_customer = $this->input->post('i_customer', TRUE);
		$e_customer_name = $this->input->post('e_customer_name', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_laprealisasi($date_from, $date_to, $i_customer, 0);
		$query2 = $this->mreport->get_laprealisasi($date_from, $date_to, $i_customer, 1);
		$query3 =$this->mreport->get_perusahaan();
		
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='17' align='center'>".$query3[0]['e_initial_name']."</th>
				 </tr>
				 <tr>
					<th colspan='17' align='center'>DATA REALISASI FC vs OP vs DO BERDASARKAN PELANGGAN ".$e_customer_name."</th>
				 </tr>
				 <tr>
					<th colspan='17' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr>
				 <tr>
					<th colspan='17'>&nbsp;</th>
				 </tr>
				 
				 
				 ";
		$i=1;
		if (is_array($query)) {
			$temp_kodekel = ""; $temp_kodebrand = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
			$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
			
			// 09-05-2015
			$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
			$tcdata=0; $tsumfc=0; $tsumop=0; $tsumoprp=0; $tsumdo=0; $tsumdorp=0; 
			$tsumpersendovsop=0; $tsumpersenopvsfc=0; $tsumpersendovsfc=0; $tcdatapersendovsop=0; $tsumpendingan=0; $tsumpendinganrp=0;
			// 11-05-2015
			$tcdatapersenopvsfc=0; $tcdatapersendovsfc=0;	
			$sumdropping=0;$tsumdropping=0;$sumdroppingrp=0;$tsumdroppingrp=0;
			
				
			
			
					
					$html_data.= "<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					  <th width='5%' colspan='2'>Dropping</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
			
				 for($j=0;$j<count($query);$j++){
					$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td align='left'>".$query[$j]['kode_brg_jadi']."</td>
				<td align='left'>".$query[$j]['nama_brg_jadi']."</td>
				<td align='left'>".$query[$j]['hjp']."</td>
				<td align='center'>".$query[$j]['fc']."</td>
				<td align='center'>".$query[$j]['jum_op']."</td>
				<td align='left'>". number_format($query[$j]['jum_op_rp'], 0, '','')."</td>
				<td align='center'>";
				if ($query[$j]['persenopvsfc'] != '')
					$html_data.= number_format($query[$j]['persenopvsfc'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td align='center'>".$query[$j]['jum_do']."</td>
				<td align='left'>".$query[$j]['jum_do_rp']."</td>
				<td align='center'>";
				if ($query[$j]['persendovsfc'] != '')
					$html_data.= number_format($query[$j]['persendovsfc'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>";
				if ($query[$j]['persendovsop'] != '')
					$html_data.=number_format($query[$j]['persendovsop'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>".$query[$j]['pendingan']."</td>
				<td align='left'>".$query[$j]['pendingan_rp']."</td>";
				 if($query[$j]['fc']==0 &&$query[$j]['jum_op'] != 0 ){
						 $query[$j]['dropping'] =0; $query[$j]['dropping_rp'] =0;
					  $html_data.= "
					  <td align='center' >".$query[$j]['dropping']."</td>
					  <td align='center'  >".$query[$j]['dropping_rp']."</td>";
					 }
					  elseif($query[$j]['fc'] < $query[$j]['jum_op'] ){
						   $query[$j]['dropping'] =0; $query[$j]['dropping_rp'] =0;
					  $html_data.= "
					  <td align='center'  >".$query[$j]['dropping']."</td>
					  <td align='center'  >".number_format($query[$j]['dropping_rp'], 2, ',','.')."</td>";
					 }
					  else{
					$html_data.= "
					 <td align='right' >".$query[$j]['dropping']."</td>
					 <td align='right' >".number_format($query[$j]['dropping_rp'], 2, ',','.')."</td>";
				}
			$html_data.="</tr>";
				
				
					$sumfc+=$query[$j]['fc'];
					$sumop+=$query[$j]['jum_op'];
					$sumoprp+=$query[$j]['jum_op_rp'];
					$sumdo+=$query[$j]['jum_do'];
					$sumdorp+=$query[$j]['jum_do_rp'];
					$sumdropping+=$query[$j]['dropping'];
					$sumdroppingrp+=$query[$j]['dropping_rp'];
					/*$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
					$sumpersendovsfc+=$query[$j]['persendovsfc'];
					$sumpersendovsop+=$query[$j]['persendovsop']; */
					//$cdatapersenopvsfc, $cdatapersendovsfc, $cdatapersendovsop
					if ($query[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query[$j]['pendingan'];
					$sumpendinganrp+=$query[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc+=$query[$j]['fc'];
					$tsumop+=$query[$j]['jum_op'];
					$tsumoprp+=$query[$j]['jum_op_rp'];
					$tsumdo+=$query[$j]['jum_do'];
					$tsumdorp+=$query[$j]['jum_do_rp'];
					$tsumpendingan+=$query[$j]['pendingan'];
					$tsumpendinganrp+=$query[$j]['pendingan_rp'];
					$tsumdropping+=$query[$j]['dropping'];
					$tsumdroppingrp+=$query[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					
					
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						if ($cdata != 0) {
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping =0;$sumdroppingrp =0;

						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					
					
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						/*$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersendovsop;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsop; */
						// 11-05-2015
						if($tcdatapersendovsop != 0){
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
					}
					if($tcdatapersenopvsfc != 0){
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersenopvsfc;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsfc;
					}
					else{
					$ratapersenopfc=0;
					$ratapersendofc=0;
				}
					
						
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					
			}
				
		} // end if1
	
	
		if (is_array($query2)) {
			$temp_kodekel = ""; $temp_kodebrand="";$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
				
				// 09-05-2015
				$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
				$tcdata2=0; $tsumfc2=0; $tsumop2=0; $tsumoprp2=0; $tsumdo2=0; $tsumdorp2=0; 
				$tsumpersendovsop2=0; $tsumpersenopvsfc2=0; $tsumpersendovsfc2=0; $tcdatapersendovsop2=0; $tsumpendingan2=0; $tsumpendinganrp2=0;
				// 11-05-2015
				$tcdatapersenopvsfc2=0; $tcdatapersendovsfc2=0;
				$tsumdropping2=0;
				$tsumdroppingrp2=0;
			
			for($j=0;$j<count($query2);$j++){
				 
				$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td align='left'>".$query2[$j]['kode_brg_jadi']."</td>
				<td align='left'>".$query2[$j]['nama_brg_jadi']."</td>
				<td align='left'>".$query2[$j]['hjp']."</td>
				<td align='center'>".$query2[$j]['fc']."</td>
				<td align='center'>".$query2[$j]['jum_op']."</td>
				<td align='left'>".$query2[$j]['jum_op_rp']."</td>
				<td align='center'>";
				if ($query2[$j]['persenopvsfc'] != '')
					$html_data.= number_format($query2[$j]['persenopvsfc'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td align='center'>".$query2[$j]['jum_do']."</td>
				<td align='left'>".$query2[$j]['jum_do_rp']."</td>
				<td align='center'>";
				if ($query2[$j]['persendovsfc'] != '')
					$html_data.= number_format($query2[$j]['persendovsfc'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='center'>";
				if ($query2[$j]['persendovsop'] != '')
					$html_data.=number_format($query2[$j]['persendovsop'], 2, '.',',');
				else
					$html_data.= ""; 
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td align='center'>".$query2[$j]['pendingan']."</td>
				<td align='left'>".$query2[$j]['pendingan_rp']."</td>";
				
				
					 if($query2[$j]['fc']==0 &&$query2[$j]['jum_op'] != 0 ){
						 $query2[$j]['dropping'] =0; $query2[$j]['dropping_rp'] =0;
					  $html_data.= "
					  <td align='center'  style='background-color:orange;' >".  $query2[$j]['dropping'] ."&nbsp;</td>
					  <td align='center'  style='background-color:orange;' >".  $query2[$j]['dropping_rp'] ."&nbsp;</td>";
					 }
					  elseif($query2[$j]['fc'] < $query2[$j]['jum_op'] ){
						   $query2[$j]['dropping'] =0; $query2[$j]['dropping_rp'] =0;
					  $html_data.= "
					  <td align='center'  style='background-color:#33cc33;' >". $query2[$j]['dropping'] ."&nbsp;</td>
					  <td align='center'  style='background-color:#33cc33;' >". number_format($query2[$j]['dropping_rp'], 2, ',','.') ."&nbsp;</td>";
					 }
				 else{
					$html_data.= " 
					 <td align='right' style='white-space:nowrap;'>".$query2[$j]['dropping'] ."</td>
					  <td align='right' style='white-space:nowrap;'>".number_format($query2[$j]['dropping_rp'], 2, ',','.') ."</td>
				 ";
					 }
			$html_data.="	</tr>";
				
					$sumfc+=$query2[$j]['fc'];
					$sumop+=$query2[$j]['jum_op'];
					$sumoprp+=$query2[$j]['jum_op_rp'];
					$sumdo+=$query2[$j]['jum_do'];
					$sumdorp+=$query2[$j]['jum_do_rp'];
					$sumdropping+=$query2[$j]['dropping'];
					$sumdroppingrp+=$query2[$j]['dropping_rp'];
					//$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
					//$sumpersendovsfc+=$query2[$j]['persendovsfc'];
					//$sumpersendovsop+=$query2[$j]['persendovsop']; 
					if ($query2[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query2[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query2[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query2[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query2[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
				
					$sumpendingan+=$query2[$j]['pendingan'];
					$sumpendinganrp+=$query2[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc2+=$query2[$j]['fc'];
					$tsumop2+=$query2[$j]['jum_op'];
					$tsumoprp2+=$query2[$j]['jum_op_rp'];
					$tsumdo2+=$query2[$j]['jum_do'];
					$tsumdorp2+=$query2[$j]['jum_do_rp'];
					$tsumpendingan2+=$query2[$j]['pendingan'];
					$tsumpendinganrp2+=$query2[$j]['pendingan_rp'];
					$tsumdropping2+=$query2[$j]['dropping'];
					$tsumdroppingrp2+=$query2[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					
					
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0){
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
								}
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
					//	$tsumpersendovsop2+= $ratapersendovsop;
					//	$tsumpersenopvsfc2+= $ratapersenopvsfc;
					//	$tsumpersendovsfc2+= $ratapersendovsfc;
					//	$tcdatapersendovsop2++; 
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						// <td align='left'><b>".$ratapersenopvsfc."</b></td>
						//<td align='left'><b>".$ratapersendovsfc."</b></td>
						//<td align='left'><b>".$ratapersendovsop."</b></td>
						
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping=0;$sumdroppingrp=0;
					
							$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					
				
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						
						$tsumpersendovsop+= $ratapersendovsop;
						$tcdatapersendovsop++;
						
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop; */
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						//$ratapersen2 = $tsumpersendovsop2/$tcdatapersendovsop2;
						//$ratapersenopfc2 = $tsumpersenopvsfc2/$tcdatapersendovsop2;
						//$ratapersendofc2 = $tsumpersendovsfc2/$tcdatapersendovsop2;

						if($ratapersen2=''){
							$ratapersen2 = $tsumpersendovsop2/$tcdatapersendovsop2;
						}
						else
						{
							$ratapersenopfc2 = 0;
						}
						if($ratapersenopfc2=''){
							$ratapersenopfc2 = $tsumpersenopvsfc2/$tcdatapersendovsop2;
						}
						else
						{
							$ratapersenopfc2 = 0;
						}

						if($ratapersendofc2=''){
							$ratapersendofc2 = $tsumpersendovsfc2/$tcdatapersendovsop2;
						}
						else
						{
							$ratapersendofc2 = 0;
						}


						
					
						
						// <td align='left'><b>".$ratapersenopfc2."</b></td>
						// <td align='left'><b>".$ratapersendofc2."</b></td>
						// <td align='left'><b>".$ratapersen2."</b></td>
						
					
			} // end for
			
				$allsumfc = $tsumfc + $tsumfc2;
				$allsumop = $tsumop + $tsumop2;
				$allsumoprp = $tsumoprp + $tsumoprp2;
				$allsumdo = $tsumdo + $tsumdo2;
				$allsumdorp = $tsumdorp + $tsumdorp2;
				$allsumpendingan = $tsumpendingan + $tsumpendingan2;
				$allsumpendinganrp = $tsumpendinganrp + $tsumpendinganrp2;
				$allsumdropping = $tsumdropping + $tsumdropping2;
				$allsumdroppingrp = $tsumdroppingrp + $tsumdroppingrp2;
				
				// 11-05-2015
				/*$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				$allratapersen = ($ratapersen+$ratapersen2)/2; */
			
				// 11-05-2015, 25-05-2015 acuan non-stp aja
				if ($ratapersenopfc!=0 && $ratapersenopfc2!=0)
					$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				else if ($ratapersenopfc2 == 0)
					$allratapersenopfc = $ratapersenopfc;
				else if ($ratapersenopfc == 0)
					$allratapersenopfc = $ratapersenopfc2;
				
				//$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				if ($ratapersendofc!=0 && $ratapersendofc2!=0)
					$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				else if ($ratapersendofc2 == 0)
					$allratapersendofc = $ratapersendofc;
				else if ($ratapersendofc == 0)
					$allratapersendofc = $ratapersendofc2;
				
				//$allratapersen = ($ratapersen+$ratapersen2)/2;
				if ($ratapersen!=0 && $ratapersen2!=0)
					$allratapersen = ($ratapersen+$ratapersen2)/2;
				else if ($ratapersen2 == 0)
					$allratapersen = $ratapersen;
				else if ($ratapersen == 0)
					$allratapersen = $ratapersen2;
				
				$html_data.="<tr>
							<td colspan='4' align='center'><b> TOTAL </b></td>
							<td align='center'><b>".$allsumfc."</b></td>
							<td align='center'><b>".$allsumop."</b></td>
							<td align='left'><b>".$allsumoprp."</b></td>
							<td align='center'><b>".number_format($allratapersenopfc, 2, '.',',')."</b></td>
							<td align='center'><b>".$allsumdo."</b></td>
							<td align='left'><b>".$allsumdorp."</b></td>
							<td align='center'><b>".number_format($allratapersendofc, 2, '.',',')."</b></td>
							<td align='center'><b>".number_format($allratapersen, 2, '.',',')."</b></td>
							<td align='center'><b>".$allsumpendingan."</b></td>
							<td align='left'><b>".$allsumpendinganrp."</b></td>
							<td align='center'><b>". $allsumdropping ."</b></td>
							<td align='left'><b>". $allsumdroppingrp ."</b></td>
							<td>&nbsp;</td>
						</tr>";
				
		} // end if1
		
		$html_data.="</tbody></table><br><br>";
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_realisasi_berdasarkan_pelanggan";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  //===================================================================================================================
  // 20-04-2015
  function export_excel_laprealisasi_x() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$i_customer = $this->input->post('i_customer', TRUE);
		$e_customer_name = $this->input->post('e_customer_name', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_laprealisasi_x($date_from, $date_to, $i_customer, 0);
		$query2 = $this->mreport->get_laprealisasi_x($date_from, $date_to, $i_customer, 1);
		$query3 =$this->mreport->get_perusahaan();
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='15' align='center'>".$query3[0]['e_initial_name']."</th>
				 </tr>
				 <tr>
					<th colspan='15' align='center'>DATA REALISASI FC vs OPvsDO ".$e_customer_name."</th>
				 </tr>
				 <tr>
					<th colspan='15' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr>
				 <tr>
					<th colspan='15'>&nbsp;</th>
				 </tr>
				 <tr>
					<th colspan='15' align='left'><b>Barang Reguler</b></th>
				 </tr>
				 
				 ";
		$i=1;
		if (is_array($query)) {
			$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
			$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
			
			// 09-05-2015
			$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
			$tcdata=0; $tsumfc=0; $tsumop=0; $tsumoprp=0; $tsumdo=0; $tsumdorp=0; 
			$tsumpersendovsop=0; $tsumpersenopvsfc=0; $tsumpersendovsfc=0; $tcdatapersendovsop=0; $tsumpendingan=0; $tsumpendinganrp=0;
			// 11-05-2015
			$tcdatapersenopvsfc=0; $tcdatapersendovsfc=0;	
			
			for($j=0;$j<count($query);$j++){
				if ($temp_kodekel != $query[$j]['kode_kel']) {
					$temp_kodekel = $query[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='15'>&nbsp;<b>".$query[$j]['kode_kel']." - ".$query[$j]['nama_kel']."</b></td>
					</tr>";
					
					$html_data.= "<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
				 }
				 
				$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td>".$query[$j]['kode_brg_jadi']."</td>
				<td>".$query[$j]['nama_brg_jadi']."</td>
				<td>".$query[$j]['hjp']."</td>
				<td>".$query[$j]['fc']."</td>
				<td>".$query[$j]['jum_op']."</td>
				<td>".$query[$j]['jum_op_rp']."</td>
				<td>";
				if ($query[$j]['persenopvsfc'] != '')
					$html_data.= $query[$j]['persenopvsfc'];
				else
					$html_data.= "";
				$html_data.="</td>
				<td>".$query[$j]['jum_do']."</td>
				<td>".$query[$j]['jum_do_rp']."</td>
				<td>";
				if ($query[$j]['persendovsfc'] != '')
					$html_data.= $query[$j]['persendovsfc'];
				else
					$html_data.="";
				$html_data.="</td>
				
				<td>";
				if ($query[$j]['persendovsop'] != '')
					$html_data.= $query[$j]['persendovsop'];
				else
					$html_data.= "";
				$html_data.="</td>
				<td>".$query[$j]['pendingan']."</td>
				<td>".$query[$j]['pendingan_rp']."</td>
				<td>&nbsp;</td>
				</tr>";
				
					$sumfc+=$query[$j]['fc'];
					$sumop+=$query[$j]['jum_op'];
					$sumoprp+=$query[$j]['jum_op_rp'];
					$sumdo+=$query[$j]['jum_do'];
					$sumdorp+=$query[$j]['jum_do_rp'];
					
					/*$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
					$sumpersendovsfc+=$query[$j]['persendovsfc'];
					$sumpersendovsop+=$query[$j]['persendovsop']; */
					//$cdatapersenopvsfc, $cdatapersendovsfc, $cdatapersendovsop
					if ($query[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query[$j]['pendingan'];
					$sumpendinganrp+=$query[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc+=$query[$j]['fc'];
					$tsumop+=$query[$j]['jum_op'];
					$tsumoprp+=$query[$j]['jum_op_rp'];
					$tsumdo+=$query[$j]['jum_do'];
					$tsumdorp+=$query[$j]['jum_do_rp'];
					$tsumpendingan+=$query[$j]['pendingan'];
					$tsumpendinganrp+=$query[$j]['pendingan_rp'];
					$tcdata++;
					
					$i++;
					
					if (isset($query[$j+1]['kode_kel']) && ($query[$j]['kode_kel'] != $query[$j+1]['kode_kel'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						if ($cdata != 0) {
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumop."</b></td>
							<td align='right'><b>".$sumoprp."</b></td>
							<td align='right'><b>".$ratapersenopvsfc."</b></td>
							<td align='right'><b>".$sumdo."</b></td>
							<td align='right'><b>".$sumdorp."</b></td>
							<td align='right'><b>".$ratapersendovsfc."</b></td>
							<td align='right'><b>".$ratapersendovsop."</b></td>
							<td align='right'><b>".$sumpendingan."</b></td>
							<td align='right'><b>".$sumpendinganrp."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$i=1;
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					else if (!isset($query[$j+1]['kode_kel'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						/*$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersendovsop;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsop; */
						// 11-05-2015
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersenopvsfc;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsfc;
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumop."</b></td>
							<td align='right'><b>".$sumoprp."</b></td>
							<td align='right'><b>".$ratapersenopvsfc."</b></td>
							<td align='right'><b>".$sumdo."</b></td>
							<td align='right'><b>".$sumdorp."</b></td>
							<td align='right'><b>".$ratapersendovsfc."</b></td>
							<td align='right'><b>".$ratapersendovsop."</b></td>
							<td align='right'><b>".$sumpendingan."</b></td>
							<td align='right'><b>".$sumpendinganrp."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL BARANG REGULER</b></td>
							<td align='right'><b>".$tsumfc."</b></td>
							<td align='right'><b>".$tsumop."</b></td>
							<td align='right'><b>".$tsumoprp."</b></td>
							<td align='right'><b>".$ratapersenopfc."</b></td>
							<td align='right'><b>".$tsumdo."</b></td>
							<td align='right'><b>".$tsumdorp."</b></td>
							<td align='right'><b>".$ratapersendofc."</b></td>
							<td align='right'><b>".$ratapersen."</b></td>
							<td align='right'><b>".$tsumpendingan."</b></td>
							<td align='right'><b>".$tsumpendinganrp."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
			}
				
		} // end if1
		
		$html_data.="<tr>
					<th colspan='15'>&nbsp;</th>
				 </tr>
				 <tr>
					<th colspan='15' align='left'><b>Barang STP</b></th>
				 </tr>
				 
				 ";
		
		$i=1;
		if (is_array($query2)) {
			$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
				
				// 09-05-2015
				$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
				$tcdata2=0; $tsumfc2=0; $tsumop2=0; $tsumoprp2=0; $tsumdo2=0; $tsumdorp2=0; 
				$tsumpersendovsop2=0; $tsumpersenopvsfc2=0; $tsumpersendovsfc2=0; $tcdatapersendovsop2=0; $tsumpendingan2=0; $tsumpendinganrp2=0;
				// 11-05-2015
				$tcdatapersenopvsfc2=0; $tcdatapersendovsfc2=0;
				
			for($j=0;$j<count($query2);$j++){
				if ($temp_kodekel != $query2[$j]['kode_kel']) {
					$temp_kodekel = $query2[$j]['kode_kel'];
					 
					 $html_data.= "<tr>
						<td colspan='15'>&nbsp;<b>".$query2[$j]['kode_kel']." - ".$query2[$j]['nama_kel']."</b></td>
					</tr>";
					$html_data.="<tr>
					<th width='3%' rowspan='2'>No </th>
					 <th width='10%' rowspan='2'>Kode</th>
					 <th width='15%' rowspan='2'>Nama Barang Jadi</th>
					 <th width='5%' rowspan='2'>HJP</th>
					 <th width='5%' rowspan='2'>FC</th>
					 <th width='5%' colspan='2'>Order Pembelian (OP)</th>
					 <th width='5%' rowspan='2'>% OP-FC</th>
					 <th width='5%' colspan='2'>Delivery Order (DO)</th>
					 <th width='5%' rowspan='2'>% DO-FC</th>
					 <th width='5%' rowspan='2'>% DO-OP</th>
					 <th width='5%' colspan='2'>Pendingan</th>
					 <th width='15%'>Keterangan</th>
				 </tr>
				 <tr>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>";
				 }
				 
				$html_data.= "<tr>
				<td align='center'>".$i."</td>
				<td>".$query2[$j]['kode_brg_jadi']."</td>
				<td>".$query2[$j]['nama_brg_jadi']."</td>
				<td>".$query2[$j]['hjp']."</td>
				<td>".$query2[$j]['fc']."</td>
				<td align='right'>".$query2[$j]['jum_op']."</td>
				<td align='right'>".$query2[$j]['jum_op_rp']."</td>
				<td align='right'>";
				/*if ($query2[$j]['persenopvsfc'] != '')
					$html_data.= $query2[$j]['persenopvsfc'];
				else
					$html_data.= ""; */
				$html_data.= "&nbsp;";
				$html_data.="</td>
				
				<td align='right'>".$query2[$j]['jum_do']."</td>
				<td align='right'>".$query2[$j]['jum_do_rp']."</td>
				<td align='right'>";
				/*if ($query2[$j]['persendovsfc'] != '')
					$html_data.= $query2[$j]['persendovsfc'];
				else
					$html_data.= ""; */
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='right'>";
				/*if ($query2[$j]['persendovsop'] != '')
					$html_data.=$query2[$j]['persendovsop'];
				else
					$html_data.= ""; */
				$html_data.= "&nbsp;";
				$html_data.="</td>
				<td align='right'>".$query2[$j]['pendingan']."</td>
				<td align='right'>".$query2[$j]['pendingan_rp']."</td>
				</tr>";
				
					$sumfc+=$query2[$j]['fc'];
					$sumop+=$query2[$j]['jum_op'];
					$sumoprp+=$query2[$j]['jum_op_rp'];
					$sumdo+=$query2[$j]['jum_do'];
					$sumdorp+=$query2[$j]['jum_do_rp'];
					
					/*$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
					$sumpersendovsfc+=$query2[$j]['persendovsfc'];
					$sumpersendovsop+=$query2[$j]['persendovsop']; */
					if ($query2[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query2[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query2[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query2[$j]['pendingan'];
					$sumpendinganrp+=$query2[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc2+=$query2[$j]['fc'];
					$tsumop2+=$query2[$j]['jum_op'];
					$tsumoprp2+=$query2[$j]['jum_op_rp'];
					$tsumdo2+=$query2[$j]['jum_do'];
					$tsumdorp2+=$query2[$j]['jum_do_rp'];
					$tsumpendingan2+=$query2[$j]['pendingan'];
					$tsumpendinganrp2+=$query2[$j]['pendingan_rp'];
					$tcdata++;
					
					$i++;
					
					if (isset($query2[$j+1]['kode_kel']) && ($query2[$j]['kode_kel'] != $query2[$j+1]['kode_kel'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						// <td align='right'><b>".$ratapersenopvsfc."</b></td>
						//<td align='right'><b>".$ratapersendovsfc."</b></td>
						//<td align='right'><b>".$ratapersendovsop."</b></td>
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumop."</b></td>
							<td align='right'><b>".$sumoprp."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$sumdo."</b></td>
							<td align='right'><b>".$sumdorp."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$sumpendingan."</b></td>
							<td align='right'><b>".$sumpendinganrp."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$i=1;
					}
					else if (!isset($query2[$j+1]['kode_kel'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						
						$tsumpersendovsop+= $ratapersendovsop;
						$tcdatapersendovsop++;
						
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop; */
						
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata; */
						
						// 11-05-2015 dikomen
						/*if ($cdata != 0) {
							$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						} */
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						$ratapersen2 = $tsumpersendovsop2/$tcdatapersendovsop2;
						$ratapersenopfc2 = $tsumpersenopvsfc2/$tcdatapersendovsop2;
						$ratapersendofc2 = $tsumpersendovsfc2/$tcdatapersendovsop2;
						
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL</b></td>
							<td align='right'><b>".$sumfc."</b></td>
							<td align='right'><b>".$sumop."</b></td>
							<td align='right'><b>".$sumoprp."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$sumdo."</b></td>
							<td align='right'><b>".$sumdorp."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$sumpendingan."</b></td>
							<td align='right'><b>".$sumpendinganrp."</b></td>
							<td>&nbsp;</td>
						</tr>";
						
						// <td align='right'><b>".$ratapersenopfc2."</b></td>
						// <td align='right'><b>".$ratapersendofc2."</b></td>
						// <td align='right'><b>".$ratapersen2."</b></td>
						$html_data.="<tr>
							<td colspan='4' align='center'><b>TOTAL BARANG STP</b></td>
							<td align='right'><b>".$tsumfc2."</b></td>
							<td align='right'><b>".$tsumop2."</b></td>
							<td align='right'><b>".$tsumoprp2."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$tsumdo2."</b></td>
							<td align='right'><b>".$tsumdorp2."</b></td>
							<td align='right'>&nbsp;</td>
							<td align='right'>&nbsp;</td>
							<td align='right'><b>".$tsumpendingan2."</b></td>
							<td align='right'><b>".$tsumpendinganrp2."</b></td>
							<td>&nbsp;</td>
						</tr>";
					}
			} // end for
			
				$allsumfc = $tsumfc + $tsumfc2;
				$allsumop = $tsumop + $tsumop2;
				$allsumoprp = $tsumoprp + $tsumoprp2;
				$allsumdo = $tsumdo + $tsumdo2;
				$allsumdorp = $tsumdorp + $tsumdorp2;
				$allsumpendingan = $tsumpendingan + $tsumpendingan2;
				$allsumpendinganrp = $tsumpendinganrp + $tsumpendinganrp2;
				
				// 11-05-2015
				/*$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				$allratapersen = ($ratapersen+$ratapersen2)/2; */
			
				// 11-05-2015, 25-05-2015 acuan non-stp aja
				//if ($ratapersenopfc!=0 && $ratapersenopfc2!=0)
				//	$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				//else if ($ratapersenopfc2 == 0)
					$allratapersenopfc = $ratapersenopfc;
				//else if ($ratapersenopfc == 0)
				//	$allratapersenopfc = $ratapersenopfc2;
				
				//$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				//if ($ratapersendofc!=0 && $ratapersendofc2!=0)
				//	$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				//else if ($ratapersendofc2 == 0)
					$allratapersendofc = $ratapersendofc;
				//else if ($ratapersendofc == 0)
				//	$allratapersendofc = $ratapersendofc2;
				
				//$allratapersen = ($ratapersen+$ratapersen2)/2;
				//if ($ratapersen!=0 && $ratapersen2!=0)
				//	$allratapersen = ($ratapersen+$ratapersen2)/2;
				//else if ($ratapersen2 == 0)
					$allratapersen = $ratapersen;
				//else if ($ratapersen == 0)
				//	$allratapersen = $ratapersen2;
				
				$html_data.="<tr>
							<td colspan='4' align='center'><b>GRAND TOTAL BARANG AKTIF + STP</b></td>
							<td align='right'><b>".$allsumfc."</b></td>
							<td align='right'><b>".$allsumop."</b></td>
							<td align='right'><b>".$allsumoprp."</b></td>
							<td align='right'><b>".$allratapersenopfc."</b></td>
							<td align='right'><b>".$allsumdo."</b></td>
							<td align='right'><b>".$allsumdorp."</b></td>
							<td align='right'><b>".$allratapersendofc."</b></td>
							<td align='right'><b>".$allratapersen."</b></td>
							<td align='right'><b>".$allsumpendingan."</b></td>
							<td align='right'><b>".$allsumpendinganrp."</b></td>
							<td>&nbsp;</td>
						</tr>";
				
		} // end if1
		
		$html_data.="</tbody></table><br><br>";
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_realisasi";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }

  // 22-04-2015
  function addforecastdistributor(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$submit = $this->input->post('submit', TRUE);
	
	if ($submit == "Proses") {
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$i_customer = $this->input->post('i_customer', TRUE);
		
		// Database External
		$db2 = $this->load->database('db_external', TRUE);
		
		$query3	= $db2->query(" SELECT i_customer_code, e_customer_name FROM tr_customer WHERE i_customer = '$i_customer' ");
		$hasilrow = $query3->row();
		$i_customer_code	= $hasilrow->i_customer_code;
		$e_customer_name	= $hasilrow->e_customer_name;

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
						
		$cek_data = $this->mreport->cek_forecast_distributor($bulan, $tahun, $i_customer); 
			
		if ($cek_data['idnya'] == '' ) { 
			$data['query'] = '';
			
			$data['is_new'] = '1';
			$data['jum_total'] = 0;
			
			$data['i_customer'] = $i_customer;
			$data['e_customer_name'] = $i_customer_code." - ".$e_customer_name;
			$data['isi'] = 'marketing/vform2forecastdistributor';
			$this->load->view('template',$data);
		}
		else {
				// get data dari tabel tm_forecast_distributor
				$data['query'] = $this->mreport->get_forecast_distributor($bulan, $tahun, $i_customer);
				$data['is_new'] = '0';
				$data['jum_total'] = count($data['query']);
				$data['i_customer'] = $i_customer;
				$data['e_customer_name'] = $i_customer_code." - ".$e_customer_name;
				$data['isi'] = 'marketing/vform2forecastdistributor';
				$this->load->view('template',$data);
		} // end else
	}
	else {
		$data['isi'] = 'marketing/vformforecastdistributor';
		$data['msg'] = '';
		//$data['bulan_skrg'] = date("m");
		$data['list_pelanggan'] = $this->mreport->get_pelanggan();
		$this->load->view('template',$data);
	}
  }
  
  function caribrgjadi(){
	  // Database External
		$db2 = $this->load->database('db_external', TRUE);
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		
		// query ke tabel brg jadi utk ambil kode, nama
		$queryxx = $db2->query(" SELECT e_product_motifname FROM tr_product_motif
									WHERE i_product_motif = '".$kode_brg_jadi."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$nama_brg_jadi = $hasilxx->e_product_motifname;
		}
		else
			$nama_brg_jadi = '';
		
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$this->load->view('marketing/vinfobrgjadi2', $data); 
		return true;
  }
  
  function submitforecastdistributor() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  // Database External
	  $db2 = $this->load->database('db_external', TRUE);
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $i_customer = $this->input->post('i_customer', TRUE);
	  $e_customer_name = $this->input->post('e_customer_name', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE)-1;  
	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit3 = $this->input->post('submit3', TRUE);
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  	  
	  if ($is_new == '1') {
	      // insert ke tabel tm_forecast_distributor
	        $seq	= $db2->query(" SELECT id FROM tm_forecast_distributor ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$id_fc	= $seqrow->id+1;
			}else{
				$id_fc	= 1;
			}
	      $data_header = array(
			  'id'=>$id_fc,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'i_customer'=>$i_customer,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
		  $db2->insert('tm_forecast_distributor',$data_header);
		  	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mreport->saveforecastdistributor($is_new, $id_fc, $this->input->post('iddetail_'.$i, TRUE),
			 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
			 $this->input->post('fc_'.$i, TRUE),
			 $this->input->post('ket_'.$i, TRUE)
			 );
		  }
		  
		$data['msg'] = "Input forecast pelanggan ".$e_customer_name." untuk bulan ".$nama_bln." ".$tahun." sudah berhasil disimpan";
		$data['isi'] = 'marketing/vformforecastdistributor';
		$data['list_pelanggan'] = $this->mreport->get_pelanggan();
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit3 != '') { // function hapus			  
			  $sql = " SELECT id FROM tm_forecast_distributor WHERE bulan = '$bulan' AND tahun = '$tahun'
						AND i_customer = '$i_customer' ";
			  $query2	= $db2->query($sql);
			  $hasilrow = $query2->row();
			  $id_fc	= $hasilrow->id; 
			 
			 
			$db2->query(" delete from tm_forecast_distributor_detail where id_forecast_distributor = '$id_fc' ");
			$db2->query(" delete from tm_forecast_distributor where id = '$id_fc' ");
			
			$data['msg'] = "Input forecast pelanggan ".$e_customer_name." untuk bulan ".$nama_bln." ".$tahun." sudah berhasil dihapus";
			$data['isi'] = 'marketing/vformforecastdistributor';
			$data['list_pelanggan'] = $this->mreport->get_pelanggan();
			$this->load->view('template',$data);
		  }
		  else { // function update
			  // update ke tabel tm_forecast_wip_unit
			   // ambil data terakhir di tabel tm_forecast_distributor
				 $query2	= $db2->query(" SELECT id FROM tm_forecast_distributor WHERE bulan = '$bulan' AND tahun = '$tahun'
							AND i_customer = '$i_customer' ");
				 $hasilrow = $query2->row();
				 $id_fc	= $hasilrow->id;
				 
				 $db2->query(" UPDATE tm_forecast_distributor SET tgl_update = '$tgl' 
							where id = '$id_fc' ");
				 $db2->query(" DELETE FROM tm_forecast_distributor_detail WHERE id_forecast_distributor='$id_fc' ");
			  for ($i=1;$i<=$jum_data;$i++)
			  {
				 $this->mreport->saveforecastdistributor($is_new, $id_fc, $this->input->post('iddetail_'.$i, TRUE),
				 $this->input->post('kode_brg_jadi_'.$i, TRUE), 
				 $this->input->post('fc_'.$i, TRUE),
				 $this->input->post('ket_'.$i, TRUE)
				 );
			  }
			  
			$data['msg'] = "Input forecast pelanggan ".$e_customer_name." untuk bulan ".$nama_bln." ".$tahun." sudah berhasil diupdate";
			$data['isi'] = 'marketing/vformforecastdistributor';
			$data['list_pelanggan'] = $this->mreport->get_pelanggan();
			$this->load->view('template',$data);
		}
      }
  }
  
  // 27-04-2015
  function laprealisasiperiode(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_pelanggan'] = $this->mreport->get_pelanggan();
	$data['isi'] = 'marketing/vformlaprealisasiperiode';
	$this->load->view('template',$data);
  }
  
  function viewlaprealisasiperiode(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'marketing/vviewlaprealisasiperiode';
	$bulan1 = $this->input->post('bulan1', TRUE);
	$bulan2 = $this->input->post('bulan2', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$i_customer = $this->input->post('i_customer', TRUE);  
	
	$data['query'] = $this->mreport->get_laprealisasiperiode($bulan1, $bulan2, $tahun, $i_customer, 0);
	$data['query2'] = $this->mreport->get_laprealisasiperiode($bulan1, $bulan2, $tahun, $i_customer, 1);
	$data['jum_total'] = count($data['query']);
	$data['bulan1'] = $bulan1;
	$data['bulan2'] = $bulan2;
	$data['tahun'] = $tahun;
	$data['i_customer'] = $i_customer;
	
	if ($i_customer != '0') {
		$db2 = $this->load->database('db_external', TRUE);
		$query3	= $db2->query(" SELECT i_customer_code, e_customer_name FROM tr_customer WHERE i_customer = '$i_customer' ");
		$hasilrow = $query3->row();
		$i_customer_code	= $hasilrow->i_customer_code;
		$e_customer_name	= $i_customer_code." - ".$hasilrow->e_customer_name;
	}
	else {
		$e_customer_name = "Semua";
	}
	
	if ($bulan1 == '01')
		$nama_bln1 = "Januari";
	else if ($bulan1 == '02')
		$nama_bln1 = "Februari";
	else if ($bulan1 == '03')
		$nama_bln1 = "Maret";
	else if ($bulan1 == '04')
		$nama_bln1 = "April";
	else if ($bulan1 == '05')
		$nama_bln1 = "Mei";
	else if ($bulan1 == '06')
		$nama_bln1 = "Juni";
	else if ($bulan1 == '07')
		$nama_bln1 = "Juli";
	else if ($bulan1 == '08')
		$nama_bln1 = "Agustus";
	else if ($bulan1 == '09')
		$nama_bln1 = "September";
	else if ($bulan1 == '10')
		$nama_bln1 = "Oktober";
	else if ($bulan1 == '11')
		$nama_bln1 = "November";
	else if ($bulan1 == '12')
		$nama_bln1 = "Desember";
		
	if ($bulan2 == '01')
		$nama_bln2 = "Januari";
	else if ($bulan2 == '02')
		$nama_bln2 = "Februari";
	else if ($bulan2 == '03')
		$nama_bln2 = "Maret";
	else if ($bulan2 == '04')
		$nama_bln2 = "April";
	else if ($bulan2 == '05')
		$nama_bln2 = "Mei";
	else if ($bulan2 == '06')
		$nama_bln2 = "Juni";
	else if ($bulan2 == '07')
		$nama_bln2 = "Juli";
	else if ($bulan2 == '08')
		$nama_bln2 = "Agustus";
	else if ($bulan2 == '09')
		$nama_bln2 = "September";
	else if ($bulan2 == '10')
		$nama_bln2 = "Oktober";
	else if ($bulan2 == '11')
		$nama_bln2 = "November";
	else if ($bulan2 == '12')
		$nama_bln2 = "Desember";
		
	$data['i_customer'] = $i_customer;
	$data['e_customer_name'] = $e_customer_name;
	$data['namabulan1'] = $nama_bln1;
	$data['namabulan2'] = $nama_bln2;
	$this->load->view('template',$data);
  }
  
  // 29-04-2015
  function export_excel_laprealisasiperiode() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$i_customer = $this->input->post('i_customer', TRUE);
		$e_customer_name = $this->input->post('e_customer_name', TRUE);
		$bulan1 = $this->input->post('bulan1', TRUE);
		$bulan2 = $this->input->post('bulan2', TRUE);
		$namabulan1 = $this->input->post('namabulan1', TRUE);
		$namabulan2 = $this->input->post('namabulan2', TRUE);
		$tahun = $this->input->post('tahun', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_laprealisasiperiode($bulan1, $bulan2, $tahun, $i_customer, 0);
		$query2 = $this->mreport->get_laprealisasiperiode($bulan1, $bulan2, $tahun, $i_customer, 1);
		$query3 =$this->mreport->get_perusahaan();
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "".$query3[0]['e_initial_name']."<br>
					DATA REALISASI FC vs OPvsDO ".$e_customer_name."<br>
					Periode: $namabulan1 s.d $namabulan2 $tahun<br><br>
				<b>Barang Reguler</b><br>
			<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>";
		$i=1;
		if (is_array($query)) {
			$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumop=0;$sumdo=0;
			$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
			
			// 29-04-2015
				for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
					if (strlen($xx) < 2)
						$bulannya = "0".$xx;
					else
						$bulannya = $xx;
					
					$tsumalldatabulan[] = array('bulanxx'=>$bulannya,
													'tsumfc'=> 0,
													'tsumop'=> 0,
													'tsumdo'=> 0,
													//'tcdata'=> 0,
													'tsumpersendovsop'=> 0,
													'tsumpersenopvsfc'=> 0,
													'tsumpersendovsfc'=> 0,
													'tcdatapersendovsop'=> 0
													);
				}
				
				for($j=0;$j<count($query);$j++){
					$databulanan = $query[$j]['databulanan'];
					$jmlcolspan = (count($databulanan)*6)+3;
					if ($temp_kodekel != $query[$j]['kode_kel']) {
						$temp_kodekel = $query[$j]['kode_kel'];
								
						$html_data.= "<tr>
							<td colspan='".$jmlcolspan."'>&nbsp;<b>".$query[$j]['kode_kel']." - ".$query[$j]['nama_kel']."</b></td>
						</tr>
						<tr>
							<th width='3%' rowspan='2'>No </th>
							<th width='10%' rowspan='2'>Kode</th>
							<th width='15%' rowspan='2'>Nama Barang Jadi</th>";
								
						for($xx=0; $xx<count($databulanan); $xx++){
							$html_data.="<th colspan='6'>".$databulanan[$xx]['namabulanxx']."</th>";
						}
						$html_data.="</tr> <tr>";
						
						for($xx=0; $xx<count($databulanan); $xx++){
							$html_data.="<th>Qty FC</th>
										 <th>Qty OP</th>
										 <th>% OP-FC</th>
										 <th>Qty DO</th>
										 <th>% DO-FC</th>
										 <th>% DO-OP</th>";
						}
						$html_data.="</tr>";
					}
					
					$html_data.= "<tr>
					<td align='center'>".$i."</td>
					<td>".$query[$j]['kode_brg_jadi']."</td>
					<td>".$query[$j]['nama_brg_jadi']."</td>";
					
					for($xx=0; $xx<count($databulanan); $xx++){
						$html_data.="<td>".$databulanan[$xx]['fc']."</td>
						<td>".$databulanan[$xx]['jum_op']."</td>
						<td>";
						if ($databulanan[$xx]['persenopvsfc'] != '')
							$html_data.=$databulanan[$xx]['persenopvsfc'];
						else
							$html_data.="";
						$html_data.="</td>
						<td>".$databulanan[$xx]['jum_do']."</td>
						<td>";
						if ($databulanan[$xx]['persendovsfc'] != '')
							$html_data.=$databulanan[$xx]['persendovsfc'];
						else
							$html_data.="";
						$html_data.="</td>
						<td>";
						if ($databulanan[$xx]['persendovsop'] != '')
							$html_data.=$databulanan[$xx]['persendovsop'];
						else
							$html_data.="";
						$html_data.="</td>
						";
						
						$alldatabulan[] = array('bulanxx'=> $databulanan[$xx]['bulanxx'],
										'fc'=> $databulanan[$xx]['fc'],
										'jum_op'=> $databulanan[$xx]['jum_op'],
										'persenopvsfc'=> $databulanan[$xx]['persenopvsfc'],
										'jum_do'=> $databulanan[$xx]['jum_do'],
										'persendovsfc'=> $databulanan[$xx]['persendovsfc'],
										'persendovsop'=> $databulanan[$xx]['persendovsop']
										);
					
						if ($databulanan[$xx]['bulanxx'] == $tsumalldatabulan[$xx]['bulanxx']) {
							$tsumalldatabulan[$xx]['tsumfc']+= $databulanan[$xx]['fc'];
							$tsumalldatabulan[$xx]['tsumop']+= $databulanan[$xx]['jum_op'];
							$tsumalldatabulan[$xx]['tsumdo']+= $databulanan[$xx]['jum_do'];
						}
					}
					$html_data.="</tr>";
					$i++;
					
					if (isset($query[$j+1]['kode_kel']) && ($query[$j]['kode_kel'] != $query[$j+1]['kode_kel'])) {
						// 28-04-2015
						$thitung=0;
						for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
							if (strlen($xx) < 2)
								$bulannya = "0".$xx;
							else
								$bulannya = $xx;
						
							for($xx1=0; $xx1<count($alldatabulan); $xx1++){
								if($bulannya == $alldatabulan[$xx1]['bulanxx']) {
									$sumfc+=$alldatabulan[$xx1]['fc'];
									$sumop+=$alldatabulan[$xx1]['jum_op'];
									$sumdo+=$alldatabulan[$xx1]['jum_do'];
									$sumpersenopvsfc+=$alldatabulan[$xx1]['persenopvsfc'];
									$sumpersendovsfc+=$alldatabulan[$xx1]['persendovsfc'];
									$sumpersendovsop+=$alldatabulan[$xx1]['persendovsop'];
									
									if ($alldatabulan[$xx1]['jum_op'] != 0)
										$cdata++;
									
									/*$tsumfc+=$alldatabulan[$xx1]['fc'];
									$tsumop+=$alldatabulan[$xx1]['jum_op'];
									$tsumdo+=$alldatabulan[$xx1]['jum_do'];
									$tcdata++; */
																		
								} // end if
							} // end for alldatabulan
							
							if ($cdata != 0) {
								$ratapersendovsop= $sumpersendovsop/$cdata;
								$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
								$ratapersendovsfc= $sumpersendovsfc/$cdata;
							}
							else {
								$ratapersendovsop= 0;
								$ratapersenopvsfc= 0;
								$ratapersendovsfc= 0;
							}
							
							/*$tsumpersendovsop+= $ratapersendovsop;
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsop++; */
							
							if ($bulannya == $tsumalldatabulan[$thitung]['bulanxx']) {
								/*$tsumalldatabulan[] = array('bulanxx'=>$bulannya,
													'tsumfc'=> 0,
													'tsumop'=> 0,
													'tsumdo'=> 0,
													'tcdata'=> 0,
													'tsumpersendovsop'=> 0,
													'tsumpersenopvsfc'=> 0,
													'tsumpersendovsfc'=> 0,
													'tcdatapersendovsop'=> 0
													); */
								$tsumalldatabulan[$thitung]['tsumpersendovsop']+= $ratapersendovsop;
								$tsumalldatabulan[$thitung]['tsumpersenopvsfc']+= $ratapersenopvsfc;
								$tsumalldatabulan[$thitung]['tsumpersendovsfc']+= $ratapersendovsfc;
								$tsumalldatabulan[$thitung]['tcdatapersendovsop']++;
							}
							
							$sumalldatabulan[] = array('bulanxx'=>$bulannya,
													'sumfc'=> $sumfc,
													'sumop'=> $sumop,
													'sumdo'=> $sumdo,
													'sumpersenopvsfc'=> $sumpersenopvsfc,
													'sumpersendovsfc'=> $sumpersendovsfc,
													'sumpersendovsop'=> $sumpersendovsop,
													'cdata'=> $cdata,
													'ratapersendovsop'=> $ratapersendovsop,
													'ratapersenopvsfc'=> $ratapersenopvsfc,
													'ratapersendovsfc'=> $ratapersendovsfc
													);
							
							$cdata=0; $sumfc=0; $sumop=0; $sumdo=0;
							$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
							
							$thitung++;
						} // end for bulan
						
						$html_data.="<tr>
							<td colspan='3' align='center'><b>TOTAL</b></td>";
						$hitung=0;
						for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
							$html_data.="<td align='right'><b>".$sumalldatabulan[$hitung]['sumfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['sumop']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersenopvsfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['sumdo']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersendovsfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersendovsop']."</b></td>";
							$hitung++;
						}
						$html_data.="</tr>";
						
						$sumalldatabulan = array();
						$alldatabulan = array();
						$i=1;
					}
					else if (!isset($query[$j+1]['kode_kel'])) {
						// ============ 29-04-2015 ===========================================
						$thitung=0;
						for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
							if (strlen($xx) < 2)
								$bulannya = "0".$xx;
							else
								$bulannya = $xx;
						
							for($xx1=0; $xx1<count($alldatabulan); $xx1++){
								if($bulannya == $alldatabulan[$xx1]['bulanxx']) {
									$sumfc+=$alldatabulan[$xx1]['fc'];
									$sumop+=$alldatabulan[$xx1]['jum_op'];
									$sumdo+=$alldatabulan[$xx1]['jum_do'];
									$sumpersenopvsfc+=$alldatabulan[$xx1]['persenopvsfc'];
									$sumpersendovsfc+=$alldatabulan[$xx1]['persendovsfc'];
									$sumpersendovsop+=$alldatabulan[$xx1]['persendovsop'];
									
									if ($alldatabulan[$xx1]['jum_op'] != 0)
										$cdata++;
																		
								} // end if
							} // end for alldatabulan
							
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							if ($cdata != 0) {
								$ratapersendovsop= $sumpersendovsop/$cdata;
								$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
								$ratapersendovsfc= $sumpersendovsfc/$cdata;
							}
							else {
								$ratapersendovsop= 0;
								$ratapersenopvsfc= 0;
								$ratapersendovsfc= 0;
							}
														
							if ($bulannya == $tsumalldatabulan[$thitung]['bulanxx']) {
								$tsumalldatabulan[$thitung]['tsumpersendovsop']+= $ratapersendovsop;
								$tsumalldatabulan[$thitung]['tsumpersenopvsfc']+= $ratapersenopvsfc;
								$tsumalldatabulan[$thitung]['tsumpersendovsfc']+= $ratapersendovsfc;
								$tsumalldatabulan[$thitung]['tcdatapersendovsop']++;
							}
							
							$sumalldatabulan[] = array('bulanxx'=>$bulannya,
													'sumfc'=> $sumfc,
													'sumop'=> $sumop,
													'sumdo'=> $sumdo,
													'sumpersenopvsfc'=> $sumpersenopvsfc,
													'sumpersendovsfc'=> $sumpersendovsfc,
													'sumpersendovsop'=> $sumpersendovsop,
													'cdata'=> $cdata,
													'ratapersendovsop'=> $ratapersendovsop,
													'ratapersenopvsfc'=> $ratapersenopvsfc,
													'ratapersendovsfc'=> $ratapersendovsfc
													);
							
							$cdata=0; $sumfc=0; $sumop=0; $sumdo=0;
							$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
							
							$thitung++;
						}
						
						$html_data.="<tr>
							<td colspan='3' align='center'><b>TOTAL</b></td>";
						$hitung=0;
						for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
							$html_data.="<td align='right'><b>".$sumalldatabulan[$hitung]['sumfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['sumop']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersenopvsfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['sumdo']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersendovsfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersendovsop']."</b></td>";
							$hitung++;
						}
						$html_data.="</tr>";
						
						$html_data.="<tr>
							<td colspan='3' align='center'><b>TOTAL BARANG REGULER</b></td>";
						$thitung=0;
						for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
							$ratapersen = $tsumalldatabulan[$thitung]['tsumpersendovsop']/$tsumalldatabulan[$thitung]['tcdatapersendovsop'];
							$ratapersenopfc = $tsumalldatabulan[$thitung]['tsumpersenopvsfc']/$tsumalldatabulan[$thitung]['tcdatapersendovsop'];
							$ratapersendofc = $tsumalldatabulan[$thitung]['tsumpersendovsfc']/$tsumalldatabulan[$thitung]['tcdatapersendovsop'];
							$html_data.="<td align='right'><b>".$tsumalldatabulan[$thitung]['tsumfc']."</b></td>
							<td align='right'><b>".$tsumalldatabulan[$thitung]['tsumop']."</b></td>
							<td align='right'><b>".$ratapersenopfc."</b></td>
							<td align='right'><b>".$tsumalldatabulan[$thitung]['tsumdo']."</b></td>
							<td align='right'><b>".$ratapersendofc."</b></td>
							<td align='right'><b>".$ratapersen."</b></td>";
							
							$thitung++;
						}
						$html_data.= "</tr>";
						
					}
					 
				} // end for
			}
			$html_data.="</table><br><br>";
			
			$alldatabulan = array();
			$sumalldatabulan = array();
			
			$html_data.="<b>Barang STP</b><br>
			<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>";
			
			$i=1;
		if (is_array($query2)) {
			$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumop=0;$sumdo=0;
			$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
			
			// 29-04-2015
				for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
					if (strlen($xx) < 2)
						$bulannya = "0".$xx;
					else
						$bulannya = $xx;
					
					$tsumalldatabulan2[] = array('bulanxx'=>$bulannya,
													'tsumfc'=> 0,
													'tsumop'=> 0,
													'tsumdo'=> 0,
													//'tcdata'=> 0,
													'tsumpersendovsop'=> 0,
													'tsumpersenopvsfc'=> 0,
													'tsumpersendovsfc'=> 0,
													'tcdatapersendovsop'=> 0
													);
				}
				
				for($j=0;$j<count($query2);$j++){
					$databulanan = $query2[$j]['databulanan'];
					$jmlcolspan = (count($databulanan)*6)+3;
					if ($temp_kodekel != $query2[$j]['kode_kel']) {
						$temp_kodekel = $query2[$j]['kode_kel'];
								
						$html_data.= "<tr>
							<td colspan='".$jmlcolspan."'>&nbsp;<b>".$query2[$j]['kode_kel']." - ".$query2[$j]['nama_kel']."</b></td>
						</tr>
						<tr>
							<th width='3%' rowspan='2'>No </th>
							<th width='10%' rowspan='2'>Kode</th>
							<th width='15%' rowspan='2'>Nama Barang Jadi</th>";
								
						for($xx=0; $xx<count($databulanan); $xx++){
							$html_data.="<th colspan='6'>".$databulanan[$xx]['namabulanxx']."</th>";
						}
						$html_data.="</tr> <tr>";
						
						for($xx=0; $xx<count($databulanan); $xx++){
							$html_data.="<th>Qty FC</th>
										 <th>Qty OP</th>
										 <th>% OP-FC</th>
										 <th>Qty DO</th>
										 <th>% DO-FC</th>
										 <th>% DO-OP</th>";
						}
						$html_data.="</tr>";
					}
					
					$html_data.= "<tr>
					<td align='center'>".$i."</td>
					<td>".$query2[$j]['kode_brg_jadi']."</td>
					<td>".$query2[$j]['nama_brg_jadi']."</td>";
					
					for($xx=0; $xx<count($databulanan); $xx++){
						$html_data.="<td>".$databulanan[$xx]['fc']."</td>
						<td>".$databulanan[$xx]['jum_op']."</td>
						<td>";
						if ($databulanan[$xx]['persenopvsfc'] != '')
							$html_data.=$databulanan[$xx]['persenopvsfc'];
						else
							$html_data.="";
						$html_data.="</td>
						<td>".$databulanan[$xx]['jum_do']."</td>
						<td>";
						if ($databulanan[$xx]['persendovsfc'] != '')
							$html_data.=$databulanan[$xx]['persendovsfc'];
						else
							$html_data.="";
						$html_data.="</td>
						<td>";
						if ($databulanan[$xx]['persendovsop'] != '')
							$html_data.=$databulanan[$xx]['persendovsop'];
						else
							$html_data.="";
						$html_data.="</td>
						";
						
						$alldatabulan[] = array('bulanxx'=> $databulanan[$xx]['bulanxx'],
										'fc'=> $databulanan[$xx]['fc'],
										'jum_op'=> $databulanan[$xx]['jum_op'],
										'persenopvsfc'=> $databulanan[$xx]['persenopvsfc'],
										'jum_do'=> $databulanan[$xx]['jum_do'],
										'persendovsfc'=> $databulanan[$xx]['persendovsfc'],
										'persendovsop'=> $databulanan[$xx]['persendovsop']
										);
					
						if ($databulanan[$xx]['bulanxx'] == $tsumalldatabulan2[$xx]['bulanxx']) {
							$tsumalldatabulan2[$xx]['tsumfc']+= $databulanan[$xx]['fc'];
							$tsumalldatabulan2[$xx]['tsumop']+= $databulanan[$xx]['jum_op'];
							$tsumalldatabulan2[$xx]['tsumdo']+= $databulanan[$xx]['jum_do'];
						}
					}
					$html_data.="</tr>";
					$i++;
					
					if (isset($query2[$j+1]['kode_kel']) && ($query2[$j]['kode_kel'] != $query2[$j+1]['kode_kel'])) {
						// 28-04-2015
						$thitung=0;
						for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
							if (strlen($xx) < 2)
								$bulannya = "0".$xx;
							else
								$bulannya = $xx;
						
							for($xx1=0; $xx1<count($alldatabulan); $xx1++){
								if($bulannya == $alldatabulan[$xx1]['bulanxx']) {
									$sumfc+=$alldatabulan[$xx1]['fc'];
									$sumop+=$alldatabulan[$xx1]['jum_op'];
									$sumdo+=$alldatabulan[$xx1]['jum_do'];
									$sumpersenopvsfc+=$alldatabulan[$xx1]['persenopvsfc'];
									$sumpersendovsfc+=$alldatabulan[$xx1]['persendovsfc'];
									$sumpersendovsop+=$alldatabulan[$xx1]['persendovsop'];
									
									if ($alldatabulan[$xx1]['jum_op'] != 0)
										$cdata++;
									
									/*$tsumfc+=$alldatabulan[$xx1]['fc'];
									$tsumop+=$alldatabulan[$xx1]['jum_op'];
									$tsumdo+=$alldatabulan[$xx1]['jum_do'];
									$tcdata++; */
																		
								} // end if
							} // end for alldatabulan
							
							if ($cdata != 0) {
								$ratapersendovsop= $sumpersendovsop/$cdata;
								$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
								$ratapersendovsfc= $sumpersendovsfc/$cdata;
							}
							else {
								$ratapersendovsop= 0;
								$ratapersenopvsfc= 0;
								$ratapersendovsfc= 0;
							}
							
							/*$tsumpersendovsop+= $ratapersendovsop;
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsop++; */
							
							if ($bulannya == $tsumalldatabulan2[$thitung]['bulanxx']) {
								/*$tsumalldatabulan[] = array('bulanxx'=>$bulannya,
													'tsumfc'=> 0,
													'tsumop'=> 0,
													'tsumdo'=> 0,
													'tcdata'=> 0,
													'tsumpersendovsop'=> 0,
													'tsumpersenopvsfc'=> 0,
													'tsumpersendovsfc'=> 0,
													'tcdatapersendovsop'=> 0
													); */
								$tsumalldatabulan2[$thitung]['tsumpersendovsop']+= $ratapersendovsop;
								$tsumalldatabulan2[$thitung]['tsumpersenopvsfc']+= $ratapersenopvsfc;
								$tsumalldatabulan2[$thitung]['tsumpersendovsfc']+= $ratapersendovsfc;
								$tsumalldatabulan2[$thitung]['tcdatapersendovsop']++;
							}
							
							$sumalldatabulan[] = array('bulanxx'=>$bulannya,
													'sumfc'=> $sumfc,
													'sumop'=> $sumop,
													'sumdo'=> $sumdo,
													'sumpersenopvsfc'=> $sumpersenopvsfc,
													'sumpersendovsfc'=> $sumpersendovsfc,
													'sumpersendovsop'=> $sumpersendovsop,
													'cdata'=> $cdata,
													'ratapersendovsop'=> $ratapersendovsop,
													'ratapersenopvsfc'=> $ratapersenopvsfc,
													'ratapersendovsfc'=> $ratapersendovsfc
													);
							
							$cdata=0; $sumfc=0; $sumop=0; $sumdo=0;
							$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
							
							$thitung++;
						} // end for bulan
						
						$html_data.="<tr>
							<td colspan='3' align='center'><b>TOTAL</b></td>";
						$hitung=0;
						for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
							$html_data.="<td align='right'><b>".$sumalldatabulan[$hitung]['sumfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['sumop']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersenopvsfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['sumdo']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersendovsfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersendovsop']."</b></td>";
							$hitung++;
						}
						$html_data.="</tr>";
						
						$sumalldatabulan = array();
						$alldatabulan = array();
						$i=1;
					}
					else if (!isset($query2[$j+1]['kode_kel'])) {
						// ============ 29-04-2015 ===========================================
						$thitung=0;
						for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
							if (strlen($xx) < 2)
								$bulannya = "0".$xx;
							else
								$bulannya = $xx;
						
							for($xx1=0; $xx1<count($alldatabulan); $xx1++){
								if($bulannya == $alldatabulan[$xx1]['bulanxx']) {
									$sumfc+=$alldatabulan[$xx1]['fc'];
									$sumop+=$alldatabulan[$xx1]['jum_op'];
									$sumdo+=$alldatabulan[$xx1]['jum_do'];
									$sumpersenopvsfc+=$alldatabulan[$xx1]['persenopvsfc'];
									$sumpersendovsfc+=$alldatabulan[$xx1]['persendovsfc'];
									$sumpersendovsop+=$alldatabulan[$xx1]['persendovsop'];
									
									if ($alldatabulan[$xx1]['jum_op'] != 0)
										$cdata++;
																		
								} // end if
							} // end for alldatabulan
							
							if ($cdata != 0) {
								$ratapersendovsop= $sumpersendovsop/$cdata;
								$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
								$ratapersendovsfc= $sumpersendovsfc/$cdata;
							}
							else {
								$ratapersendovsop= 0;
								$ratapersenopvsfc= 0;
								$ratapersendovsfc= 0;
							}
														
							if ($bulannya == $tsumalldatabulan2[$thitung]['bulanxx']) {
								$tsumalldatabulan2[$thitung]['tsumpersendovsop']+= $ratapersendovsop;
								$tsumalldatabulan2[$thitung]['tsumpersenopvsfc']+= $ratapersenopvsfc;
								$tsumalldatabulan2[$thitung]['tsumpersendovsfc']+= $ratapersendovsfc;
								$tsumalldatabulan2[$thitung]['tcdatapersendovsop']++;
							}
							
							$sumalldatabulan[] = array('bulanxx'=>$bulannya,
													'sumfc'=> $sumfc,
													'sumop'=> $sumop,
													'sumdo'=> $sumdo,
													'sumpersenopvsfc'=> $sumpersenopvsfc,
													'sumpersendovsfc'=> $sumpersendovsfc,
													'sumpersendovsop'=> $sumpersendovsop,
													'cdata'=> $cdata,
													'ratapersendovsop'=> $ratapersendovsop,
													'ratapersenopvsfc'=> $ratapersenopvsfc,
													'ratapersendovsfc'=> $ratapersendovsfc
													);
							
							$cdata=0; $sumfc=0; $sumop=0; $sumdo=0;
							$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
							
							$thitung++;
						}
						
						$html_data.="<tr>
							<td colspan='3' align='center'><b>TOTAL</b></td>";
						$hitung=0;
						for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
							$html_data.="<td align='right'><b>".$sumalldatabulan[$hitung]['sumfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['sumop']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersenopvsfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['sumdo']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersendovsfc']."</b></td>
							<td align='right'><b>".$sumalldatabulan[$hitung]['ratapersendovsop']."</b></td>";
							$hitung++;
						}
						$html_data.="</tr>";
						
						$html_data.="<tr>
							<td colspan='3' align='center'><b>TOTAL BARANG REGULER</b></td>";
						$thitung=0;
						for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
							$ratapersen = $tsumalldatabulan2[$thitung]['tsumpersendovsop']/$tsumalldatabulan2[$thitung]['tcdatapersendovsop'];
							$ratapersenopfc = $tsumalldatabulan2[$thitung]['tsumpersenopvsfc']/$tsumalldatabulan2[$thitung]['tcdatapersendovsop'];
							$ratapersendofc = $tsumalldatabulan2[$thitung]['tsumpersendovsfc']/$tsumalldatabulan2[$thitung]['tcdatapersendovsop'];
							$html_data.="<td align='right'><b>".$tsumalldatabulan2[$thitung]['tsumfc']."</b></td>
							<td align='right'><b>".$tsumalldatabulan2[$thitung]['tsumop']."</b></td>
							<td align='right'><b>".$ratapersenopfc."</b></td>
							<td align='right'><b>".$tsumalldatabulan2[$thitung]['tsumdo']."</b></td>
							<td align='right'><b>".$ratapersendofc."</b></td>
							<td align='right'><b>".$ratapersen."</b></td>";
							
							$thitung++;
						}
						$html_data.= "</tr>";
						
					}
					 
				} // end for
			}
			$html_data.="</table><br><br>";
			// ========================================================================================================					
								
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_realisasi_periode";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
}
