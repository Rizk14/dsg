<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('marketing/mmaster');
  }

	// 23-04-2015
  function addbrgjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->i_product;
			$enama_brg = $row->e_product_motifname;
			$ekel_brg_jadi = $row->id_kel_brg_jadi;
		}
	}
	else {
			$ekode = '';
			$enama_brg = '';
			$ekel_brg_jadi = '';
			$edit = '';
	}
	$data['list_kelbrgjadi'] = $this->mmaster->getAllkelbrgjadi();
	$data['ekode'] = $ekode;
	$data['enama_brg'] = $enama_brg;	
	$data['ekel_brg_jadi'] = $ekel_brg_jadi;	
	$data['edit'] = $edit;
	$data['msg'] = '';
    $data['isi'] = 'marketing/vmainformbrgjadi';
    
	$this->load->view('template',$data);
  }

  function submitbrgjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		//$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');

		} */
		
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$kel_brg_jadi 	= $this->input->post('kel_brg_jadi', TRUE);
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'marketing/vmainformbrgjadi';
					$data['msg'] = "Data kode ".$kode." sudah ada..!";
					
					$ekode = '';
					$enama_brg = '';
					$ekel_brg_jadi = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama_brg'] = $enama_brg;
					$data['ekel_brg_jadi'] = $ekel_brg_jadi;
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($kode,$nama, $kel_brg_jadi, $kodeedit, $goedit);
					$data['isi'] = 'marketing/vmainformbrgjadi';
					$data['msg'] = "Data kode ".$kode." berhasil disimpan";
					
					$ekode = '';
					$enama_brg = '';
					$ekel_brg_jadi = '';
					$edit = '';
					
					$data['ekode'] = $ekode;
					$data['enama_brg'] = $enama_brg;
					$data['ekel_brg_jadi'] = $ekel_brg_jadi;
					$data['edit'] = $edit;
					$this->load->view('template',$data);
				}
			} // end if goedit == ''
			else {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'marketing/vmainformbrgjadi';
						$data['msg'] = "Data kode ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama_brg'] = $nama;
						$data['ekel_brg_jadi'] = $kel_brg_jadi;
						$data['edit'] = $edit;
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kode,$nama, $kel_brg_jadi, $kodeedit, $goedit);
						redirect('marketing/cform/viewbrgjadi');
					}
				}
				else {
					$this->mmaster->save($kode,$nama, $kel_brg_jadi, $kodeedit, $goedit);
					redirect('marketing/cform/viewbrgjadi');
				}
			}
			
  }
  
  function viewbrgjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'marketing/vformviewbrgjadi';
    $keywordcari = "all";
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/marketing/cform/viewbrgjadi/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }
  
  function caribrgjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
			
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/marketing/cform/caribrgjadi/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '20';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $keywordcari);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'marketing/vformviewbrgjadi';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];
	$this->load->view('template',$data);
  }

  function deletebrgjadi(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('marketing/cform/viewbrgjadi');
  }
  
  // 23-04-2015
  function kelbrgjadi(){
	$id 	= $this->uri->segment(4);
	
	if ($id != '') {
		$hasil = $this->mmaster->get_kel_brg_jadi($id);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode;
			$enama = $row->nama;
			$eket = $row->keterangan;
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$enama = '';
			$eket = '';
			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['eket'] = $eket;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAllkelbrgjadi();
    $data['isi'] = 'marketing/vmainformkelbrgjadi';
	$this->load->view('template',$data);
  }
  
  function submitkelbrgjadi(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('marketing/cform/kelbrgjadi');
		}
		else
		{
			$id 	= $this->input->post('id', TRUE);
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$ket 	= $this->input->post('ket', TRUE);
			
			if ($goedit == 1) {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data_kel_brg_jadi($kode);
					if (count($cek_data) == 0) { 
						$this->mmaster->save_kel_brg_jadi($kode, $kodeedit, $nama, $ket, $goedit);
					}
				}
				else {
					$this->mmaster->save_kel_brg_jadi($kode, $kodeedit, $nama, $ket, $goedit);
				}
			}
			else {
				$cek_data = $this->mmaster->cek_data_kel_brg_jadi($kode);
				if (count($cek_data) == 0) { 
					$this->mmaster->save_kel_brg_jadi($kode, $kodeedit, $nama, $ket, $goedit);
				}
			}
			
			redirect('marketing/cform/kelbrgjadi');
		}
  }

  function deletekelbrgjadi(){
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete_kel_brg_jadi($id);
    redirect('marketing/cform/kelbrgjadi');
  }
  
}
