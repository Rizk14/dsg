<?php
include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {
	
	function __construct() { 
		parent::__construct();
	}
	
	function index() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$this->load->model('prntkontrabonnondo/mclass');
			/*$data['page_title_fpenjualanperdo']	= $this->lang->line('page_title_penjualanperdo');
			$data['form_title_detail_fpenjualanperdo']	= $this->lang->line('form_title_detail_penjualanperdo');
			$data['list_fpenjperno_faktur']	= $this->lang->line("list_fpenjperno_faktur");
			$data['list_fpenjperdo_kd_brg']	= $this->lang->line('list_penjperdo_kd_brg');
			$data['list_fpenjperdo_tgl_mulai']	= $this->lang->line('list_penjperdo_tgl_mulai');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail'); */

			$data['detail']		= "";
			$data['list']		= "";
			$data['toko']		= $this->mclass->getcustomer();
			$data['limages']	= base_url();
			
				$data['isi']	='prntkontrabonnondo/vmainform';
		$this->load->view('template',$data);
		
	}
	
	// 15-06-2012
	function listkontrabon() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$data['page_title']	= "DAFTAR KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('prntkontrabonnondo/mclass');

		$query	= $this->mclass->getkontrabon();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/prntkontrabonnondo/cform/listkontrabon/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->getkontrabonperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('prntkontrabonnondo/vlistformkontrabon',$data);
	}
	
	// 16-06-2012
	function getkontrabonbynomor() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$db2=$this->load->database('db_external', TRUE);
	//	$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lkontrabon']	= "";		
		$data['limages']	= base_url();
		
		$nokontrabon		= $this->input->post('no_kontrabon');
		$ikontrabon			= $this->input->post('i_kontrabon');
		$data['pelanggan']	= $this->input->post('pelanggan');
		
		
		if ($ikontrabon == '' && $nokontrabon == '') {
			$nokontrabon = $this->uri->segment(4);
			$ikontrabon = $this->uri->segment(5);
		}
		
		$data['nokontrabon']	= (!empty($nokontrabon))?$nokontrabon:'';
		$data['ikontrabon']	= (!empty($ikontrabon))?$ikontrabon:'';

				
		$turi1	= ($nokontrabon!='' && $nokontrabon!='0')?$nokontrabon:'0';
		$turi2	= ($ikontrabon!='' && $ikontrabon!='0')?$ikontrabon:'0';

		
		$data['turi1']	= $nokontrabon;
		$data['turi2']	= $ikontrabon;
		
		$this->load->model('prntkontrabonnondo/mclass');
		
		$pagination['base_url'] 	= '/prntkontrabonnondo/cform/getkontrabonbynomor/'.$turi1.'/'.$turi2.'/';
		
		$qlistkontrabon	= $this->mclass->getkontrabonbynomor($ikontrabon);
		
		/*if(($dkontrabonfirst!='') && ($dkontrabonlast!='')) {
			$qlistkontrabon	= $this->mclass->clistkontrabon($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 1;
		}else{
			$qlistkontrabon	= $this->mclass->clistkontrabon2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 2;
		} */
		//echo $qlistkontrabon->num_rows();
		$pagination['total_rows']	= $qlistkontrabon->num_rows();
		$pagination['per_page']		= 30;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		$data['idtitem']	= $this->mclass->getkontrabonbynomorperpages($pagination['per_page'],$pagination['cur_page'],$ikontrabon);
		//echo count($data['idtitem']);
		/*if(($dkontrabonfirst!='') && ($dkontrabonlast!='')) {
			$data['isi']	= $this->mclass->clistkontrabonperpages($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}else{
			$data['isi']	= $this->mclass->clistkontrabonperpages2($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		} */
		
		$query3	= $db2->query(" SELECT d_dt FROM tm_dt WHERE i_dt = '$ikontrabon' ");
		if ($query3->num_rows() != 0) {
			$hasilrow = $query3->row();
			$exp_ddt = explode("-",$hasilrow->d_dt);
			$data['ddt'] = $exp_ddt[2]."/".$exp_ddt[1]."/".$exp_ddt[0];
			//$data['ddt']	= $hasilrow->d_dt;
		}
			$data['isi']	='prntkontrabonnondo/vformdetail';
		$this->load->view('template',$data);
	
	}
	// end 16-06-2012
		
	// 19-06-2012
	/* Cetak Kontrabon Windows Popup
	*/
	function cetakkontrabon() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$db2=$this->load->database('db_external', TRUE);
		$iduserid	= $this->session->userdata('user_idx');
		$remote		= $_SERVER['REMOTE_ADDR'];
		$host		= '192.168.0.194';
		//$host		= '192.168.0.134';
		$uri		= '/printers/EpsonLX300';
		$ldo		= '';
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;	

		//$data['charset']	= 'us-ascii';
		//$data['setLanguage']= 'en_us';
		//$data['mime_media_type']	= 'application/octet-stream';
			
		/*$data['page_title_fpenjualanperdo']	= $this->lang->line('page_title_fpenjualanperdo');
		$data['form_title_detail_fpenjualanperdo']	= $this->lang->line('form_title_detail_fpenjualanperdo');
		$data['list_fpenjperno_faktur']	= $this->lang->line("list_fpenjperno_faktur");		
		$data['list_fpenjperdo_kd_brg']	= $this->lang->line('list_fpenjperdo_kd_brg');
		$data['list_fpenjperdo_tgl_mulai']	= $this->lang->line('list_fpenjperdo_tgl_mulai');
		$data['list_fpenjperdo_no_do']	= $this->lang->line('list_fpenjperdo_no_do');
		$data['list_fpenjperdo_nm_brg']	= $this->lang->line('list_fpenjperdo_nm_brg');
		$data['list_fpenjperdo_qty']	= $this->lang->line('list_fpenjperdo_qty');
		$data['list_fpenjperdo_hjp']	= $this->lang->line('list_fpenjperdo_hjp');
		$data['list_fpenjperdo_amount']	= $this->lang->line('list_fpenjperdo_amount');
		$data['list_fpenjperdo_total_pengiriman']	= $this->lang->line('list_fpenjperdo_total_pengiriman');
		$data['list_fpenjperdo_total_penjualan']	= $this->lang->line('list_fpenjperdo_total_penjualan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['button_cetak']	= $this->lang->line('button_cetak'); */
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpenjperdo']	= "";
		$data['limages']	= base_url();
		$data['lfaktur']	= "";
				
		$ddt	= $this->uri->segment(6); // DD/MM/YYYY
		$nokontrabon	= $this->uri->segment(5);
		$ikontrabon	= $this->uri->segment(4);
		$pelanggan = $this->uri->segment(9);
		
		$data['pelanggan'] = rawurldecode($pelanggan);
		$data['nokontrabon']	= $nokontrabon;
		$data['ddt']	= $ddt;
		
		$this->load->model('prntkontrabonnondo/mclass');

		$qry_source_remote	= $this->mclass->remote($iduserid);
		if($qry_source_remote->num_rows()>0) {
			$row_source_remote	= $qry_source_remote->row();
			$source_printer_name= $row_source_remote->e_printer_name;
			$source_ip_remote	= $row_source_remote->ip;
			$source_uri_remote	= $row_source_remote->e_uri;
		} else {
			$source_printer_name= "Default Printer";
			$source_ip_remote	= $host;
			$source_uri_remote	= $uri;
		}
		
		$data['printer_name']	= $source_printer_name;
		$data['host']		= $source_ip_remote;
		$data['uri']		= $source_uri_remote;
		$data['ldo']		= $ldo;
		$data['log_destination']= 'logs/'.$logfile;

		//$qry_infoheader		= $this->mclass->clistfpenjperdo($nofaktur,$d_do_first,$d_do_last);
		// ambil faktur awal dan faktur akhir
		$sqlxx = " SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
					FROM tm_faktur b
					
					INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
					INNER JOIN tm_dt_item d ON d.i_nota=b.i_faktur
					INNER JOIN tm_dt e ON e.i_dt=d.i_dt
					INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='t' AND e.i_dt='$ikontrabon' AND e.f_dt_cancel='f'
					GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn 
					ORDER BY b.i_faktur_code ASC "; 
		$queryxx = $db2->query($sqlxx);					
		if ($queryxx->num_rows() > 0) {
			$hasilxx = $queryxx->result();
			$hitung = 1;
			$faktur_awal = ""; $faktur_akhir = "";
			foreach ($hasilxx as $rowfaktur) {
				if ($hitung == 1)
					$faktur_awal = $rowfaktur->i_faktur_code;
				$faktur_akhir = $rowfaktur->i_faktur_code;
				$hitung++;
			}
		}

		/*$qcabang	= $this->mclass->getcabang($nofaktur);
		$qinitial	= $this->mclass->getinitial();
		
		if($qcabang->num_rows()>0) {
			$row_cabang	= $qcabang->row_array();
			$data['nmcabang']	= $row_cabang['cabangname'];
			$data['alamatcabang']	= $row_cabang['address'];
		} else {
			$data['nmcabang']	= "";
			$data['alamatcabang']	= "";
		}
				
		if($qinitial->num_rows()>0) {
			$row_initial	= $qinitial->row_array();
			$data['nminitial']	= $row_initial['e_initial_name'];
		} else {
			$data['nminitial']	= "";
		}
		
		if($qry_infoheader->num_rows() > 0){

			$bglobal	= array(
			'01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
			'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
			
			$row_infoheader	= $qry_infoheader->row();
			$nofaktur	= $row_infoheader->ifakturcode;
			$tglfaktur	= $row_infoheader->dfaktur;
			$tgljth_tempo	= $row_infoheader->ddue;
			$nmcabang	= $row_infoheader->branchname;

			$exp_tfaktur	= explode("-",$tglfaktur,strlen($tglfaktur)); // YYYY-MM-DD
			$exp_tjthtempo	= explode("-",$tgljth_tempo,strlen($tgljth_tempo));	
			$new_tglfaktur	= substr($exp_tfaktur[2],0,1)==0?substr($exp_tfaktur[2],1,1):$exp_tfaktur[2];
			$new_tgljthtempo	= substr($exp_tjthtempo[2],0,1)==0?substr($exp_tjthtempo[2],1,1):$exp_tjthtempo[2];
			$new_tfaktur	= $new_tglfaktur." ".$bglobal[$exp_tfaktur[1]]." ".$exp_tfaktur[0]; // 17 Januari 1989
			$new_tjthtempo	= $new_tgljthtempo." ".$bglobal[$exp_tjthtempo[1]]." ".$exp_tjthtempo[0];

			$data['nomorfaktur']	= $nofaktur;
			$data['tglfaktur']	= $new_tfaktur;
			$data['tgljthtempo']	= $new_tjthtempo;
			$data['namacab']	= $nmcabang;			
		} */
		
		/*$qry_jml	= $this->mclass->clistfpenjperdo_jml($nofaktur,$d_do_first,$d_do_last);
		$qry_totalnyaini	= $this->mclass->clistfpenjperdo_totalnyaini($nofaktur,$d_do_first,$d_do_last);
		
		if($qry_jml->num_rows() > 0 ) {
			$row_jml = $qry_jml->row_array();
			$row_totalnyaini = $qry_totalnyaini->row_array();
			$total	 = $row_totalnyaini['totalnyaini'];
			$n_disc	 = $row_jml['n_disc'];
			$v_disc	 = $row_jml['v_disc'];
			
			$total_pls_disc	= $total - $v_disc; // DPP
			$nilai_ppn		= (($total_pls_disc*10) / 100 ); // PPN
			$nilai_faktur	= $total_pls_disc + $nilai_ppn;
			
			$data['jumlah']		= $total; // Jumlah
			$data['dpp']		= $total_pls_disc; // DPP
			$data['diskon']		= $v_disc; // Diskon
			$data['nilai_ppn']	= $nilai_ppn; // PPN
			//$nilaifaktur		= explode(".",$nilai_faktur,strlen($nilai_faktur)); // Nilai Faktur
			//$data['nilai_faktur']	= $nilaifaktur[0];
			$data['nilai_faktur']	= round($nilai_faktur);
		} else {
			$data['jumlah']		= 0;
			$data['dpp']		= 0;
			$data['diskon']		= 0;
			$data['nilai_ppn']	= 0;
			$data['nilai_faktur']	= 0;
		} */
		
		/*$data['isi']	= $this->mclass->clistfpenjbrgjadiperdo2($nofaktur,$d_do_first,$d_do_last);
		$data['iopdo']	= $this->mclass->clistfpenjperdo_detail_opdo($nofaktur,$d_do_first,$d_do_last); */
		
		$qlistkontrabon	= $this->mclass->getkontrabonbynomor($ikontrabon);
		$data['total_semua']	= $qlistkontrabon->num_rows();
		$hasilnya = $qlistkontrabon->result();
		$data['query']	= $hasilnya;
		$data['faktur_awal']	= $faktur_awal;
		$data['faktur_akhir']	= $faktur_akhir;
		$this->load->view('prntkontrabonnondo/vprintkontrabon',$data);
	}
			
	// 15-06-2012
	function fgetkontrabon() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		//$fnotasederhana = $this->input->post('fnotasederhana')?$this->input->post('fnotasederhana'):$this->input->get_post('fnotasederhana');
		
		$data['page_title']	= "DAFTAR KONTRA BON";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('prntkontrabonnondo/mclass');

		//$query	= $this->mclass->flbarangjadi($key);
		$query	= $this->mclass->fgetkontrabon($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				//$f_nota_sederhana = ($row->f_nota_sederhana=='t')?'Faktur Non DO':'';
				
				$ikontrabon	= trim($row->idtcode);
					
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->idt','$ikontrabon')\">".$row->idtcode."</a></td>
				  <td><a href=\"javascript:settextfield('$row->idt','$ikontrabon')\">".$row->ddt."</a></td>
				  
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
}
?>
