<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_pelunasan']		= $this->lang->line('page_title_pelunasan');
			$data['form_title_detail_pelunasan']= $this->lang->line('form_title_detail_pelunasan');
			$data['list_pelunasan_no_voucher']	= $this->lang->line('list_pelunasan_no_voucher');
			$data['list_pelunasan_tgl_voucher']	= $this->lang->line('list_pelunasan_tgl_voucher');
			$data['list_pelunasan_tgl_pelunasan']	= $this->lang->line('list_pelunasan_tgl_pelunasan');
			$data['list_pelunasan_recieve']		= $this->lang->line('list_pelunasan_recieve');
			$data['list_pelunasan_approve']		= $this->lang->line('list_pelunasan_approve');
			$data['list_pelunasan_piutang']		= $this->lang->line('list_pelunasan_piutang');
			$data['list_pelunasan_total_voucher']	= $this->lang->line('list_pelunasan_total_voucher');
			$data['list_pelunasan_nilai_voucher']	= $this->lang->line('list_pelunasan_nilai_voucher');
			$data['list_pelunasan_nilai_pelunasan']	= $this->lang->line('list_pelunasan_nilai_pelunasan');
			$data['list_pelunasan_no_kontrabon']	= $this->lang->line('list_pelunasan_no_kontrabon');
			$data['list_pelunasan_tgl_kontrabon']	= $this->lang->line('list_pelunasan_tgl_kontrabon');
			$data['list_pelunasan_total_pelunasan']	= $this->lang->line('list_pelunasan_total_pelunasan');
			$data['list_pelunasan_nilai_faktur']	= $this->lang->line('list_pelunasan_nilai_faktur');
			$data['list_pelunasan_tgl_faktur']		= $this->lang->line('list_pelunasan_tgl_faktur');
			$data['list_pelunasan_pelangga_faktur']	= $this->lang->line('list_pelunasan_pelangga_faktur');
			$data['list_pelunasan_total_faktur']	= $this->lang->line('list_pelunasan_total_faktur');
			$data['list_pelunasan_total_kontrabon'] = $this->lang->line('list_pelunasan_total_kontrabon');
			$data['list_pelunasan_keterangan']	= $this->lang->line('list_pelunasan_keterangan');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$data['isi']	= 'listpelunasanfaktur/vmainform';	
			$this->load->view('template',$data);	
		
		
	}
	
	function carilistvoucher() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_pelunasan']		= $this->lang->line('page_title_pelunasan');
		$data['form_title_detail_pelunasan']= $this->lang->line('form_title_detail_pelunasan');
		$data['list_pelunasan_no_voucher']	= $this->lang->line('list_pelunasan_no_voucher');
		$data['list_pelunasan_tgl_voucher']	= $this->lang->line('list_pelunasan_tgl_voucher');
		$data['list_pelunasan_tgl_pelunasan']	= $this->lang->line('list_pelunasan_tgl_pelunasan');
		$data['list_pelunasan_recieve']		= $this->lang->line('list_pelunasan_recieve');
		$data['list_pelunasan_approve']		= $this->lang->line('list_pelunasan_approve');
		$data['list_pelunasan_piutang']		= $this->lang->line('list_pelunasan_piutang');
		$data['list_pelunasan_total_voucher']	= $this->lang->line('list_pelunasan_total_voucher');
		$data['list_pelunasan_nilai_voucher']	= $this->lang->line('list_pelunasan_nilai_voucher');
		$data['list_pelunasan_nilai_pelunasan']	= $this->lang->line('list_pelunasan_nilai_pelunasan');
		$data['list_pelunasan_no_faktur']	= $this->lang->line('list_pelunasan_no_faktur');
		$data['list_pelunasan_tgl_kontrabon']	= $this->lang->line('list_pelunasan_tgl_kontrabon');
		$data['list_pelunasan_total_pelunasan']	= $this->lang->line('list_pelunasan_total_pelunasan');
		$data['list_pelunasan_nilai_faktur']	= $this->lang->line('list_pelunasan_nilai_faktur');
		$data['list_pelunasan_tgl_faktur']		= $this->lang->line('list_pelunasan_tgl_faktur');
		$data['list_pelunasan_pelangga_faktur']	= $this->lang->line('list_pelunasan_pelangga_faktur');
		$data['list_pelunasan_total_faktur']	= $this->lang->line('list_pelunasan_total_faktur');
		$data['list_pelunasan_total_kontrabon'] = $this->lang->line('list_pelunasan_total_kontrabon');
		$data['list_pelunasan_keterangan']	= $this->lang->line('list_pelunasan_keterangan');
		$data['list_pelunasan_sumber_faktur'] = $this->lang->line('list_pelunasan_sumber_faktur');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpelunasan']	= "";		
		$data['limages']	= base_url();
		
		$no_voucher	= $this->input->post('no_voucher');
		$i_voucher	= $this->input->post('i_voucher');
		$dvoucherfirst= $this->input->post('d_voucher_first');
		$dvoucherlast	= $this->input->post('d_voucher_last');
		
		$data['tglvouchermulai'] = (!empty($dvoucherfirst) && $dvoucherfirst!='0')?$dvoucherfirst:'';
		$data['tglvoucherakhir'] = (!empty($dvoucherlast) && $dvoucherlast!='0')?$dvoucherlast:'';
		$data['novoucher']		 = (!empty($no_voucher) && $no_voucher!='0')?$no_voucher:'';
		$data['ivoucher']		 = (!empty($i_voucher) && $i_voucher!='0')?$i_voucher:'';
		
		$e_d_voucher_first= explode("/",$dvoucherfirst,strlen($dvoucherfirst));
		$e_d_voucher_last	= explode("/",$dvoucherlast,strlen($dvoucherlast));
		
		$ndvoucherfirst	= !empty($dvoucherfirst)?$e_d_voucher_first[2].'-'.$e_d_voucher_first[1].'-'.$e_d_voucher_first[0]:'0';
		$ndvoucherlast	= !empty($dvoucherlast)?$e_d_voucher_last[2].'-'.$e_d_voucher_last[1].'-'.$e_d_voucher_last[0]:'0';
		
		$turi1	= ($no_voucher!='' && $no_voucher!='0')?$no_voucher:'0';
		$turi2	= ($i_voucher!='' && $i_voucher!='0')?$i_voucher:'0';
		$turi3	= $ndvoucherfirst;
		$turi4	= $ndvoucherlast;
		
		$this->load->model('listpelunasanfaktur/mclass');
		
		$pagination['base_url'] 	= '/listpelunasanfaktur/cform/carilistvouchernext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/';
		
		if($dvoucherfirst!='' && $dvoucherlast!='') {
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage1($i_voucher,$ndvoucherfirst,$ndvoucherlast);
			$data['template'] = 1;
		}else{
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage2($i_voucher,$ndvoucherfirst,$ndvoucherlast);
			$data['template'] = 2;
		}
		
		$pagination['total_rows']	= $qlistvoucherallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dvoucherfirst!='' && $dvoucherlast!='') {
			$data['query']	= $this->mclass->clistvoucher1($pagination['per_page'],$pagination['cur_page'],$i_voucher,$ndvoucherfirst,$ndvoucherlast);
		}else{
			$data['query']	= $this->mclass->clistvoucher2($pagination['per_page'],$pagination['cur_page'],$i_voucher,$ndvoucherfirst,$ndvoucherlast);
		}
		$data['isi']	= 'listpelunasanfaktur/vlistform';	
			$this->load->view('template',$data);	
	
	}

	function carilistvouchernext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_pelunasan']		= $this->lang->line('page_title_pelunasan');
		$data['form_title_detail_pelunasan']= $this->lang->line('form_title_detail_pelunasan');
		$data['list_pelunasan_no_voucher']	= $this->lang->line('list_pelunasan_no_voucher');
		$data['list_pelunasan_tgl_voucher']	= $this->lang->line('list_pelunasan_tgl_voucher');
		$data['list_pelunasan_tgl_pelunasan']	= $this->lang->line('list_pelunasan_tgl_pelunasan');
		$data['list_pelunasan_recieve']		= $this->lang->line('list_pelunasan_recieve');
		$data['list_pelunasan_approve']		= $this->lang->line('list_pelunasan_approve');
		$data['list_pelunasan_piutang']		= $this->lang->line('list_pelunasan_piutang');
		$data['list_pelunasan_total_voucher']	= $this->lang->line('list_pelunasan_total_voucher');
		$data['list_pelunasan_nilai_voucher']	= $this->lang->line('list_pelunasan_nilai_voucher');
		$data['list_pelunasan_nilai_pelunasan']	= $this->lang->line('list_pelunasan_nilai_pelunasan');
		$data['list_pelunasan_no_kontrabon']	= $this->lang->line('list_pelunasan_no_kontrabon');
		$data['list_pelunasan_tgl_kontrabon']	= $this->lang->line('list_pelunasan_tgl_kontrabon');
		$data['list_pelunasan_total_pelunasan']	= $this->lang->line('list_pelunasan_total_pelunasan');
		$data['list_pelunasan_nilai_faktur']	= $this->lang->line('list_pelunasan_nilai_faktur');
		$data['list_pelunasan_tgl_faktur']		= $this->lang->line('list_pelunasan_tgl_faktur');
		$data['list_pelunasan_pelangga_faktur']	= $this->lang->line('list_pelunasan_pelangga_faktur');
		$data['list_pelunasan_total_faktur']	= $this->lang->line('list_pelunasan_total_faktur');
		$data['list_pelunasan_total_kontrabon'] = $this->lang->line('list_pelunasan_total_kontrabon');
		$data['list_pelunasan_keterangan']	= $this->lang->line('list_pelunasan_keterangan');
		$data['list_pelunasan_sumber_kontrabon'] = $this->lang->line('list_pelunasan_sumber_kontrabon');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpelunasan']	= "";		
		$data['limages']	= base_url();

		$no_voucher	= $this->uri->segment(4);
		$i_voucher	= $this->uri->segment(5);
		$dvoucherfirst	= $this->uri->segment(6);
		$dvoucherlast	= $this->uri->segment(7);

		$e_d_voucher_first = ($dvoucherfirst!='0')?explode("-",$dvoucherfirst,strlen($dvoucherfirst)):'';
		$e_d_voucher_last  = ($dvoucherlast!='0')?explode("-",$dvoucherlast,strlen($dvoucherlast)):'';

		$ndvoucherfirst	= $dvoucherfirst!='0'?$e_d_voucher_first[2].'/'.$e_d_voucher_first[1].'/'.$e_d_voucher_first[0]:'0';
		$ndvoucherlast	= $dvoucherlast!='0'?$e_d_voucher_last[2].'/'.$e_d_voucher_last[1].'/'.$e_d_voucher_last[0]:'0';
						
		$data['tglvouchermulai']= (!empty($ndvoucherfirst) && $ndvoucherfirst!='0')?$ndvoucherfirst:'';
		$data['tglvoucherakhir']= (!empty($ndvoucherlast) && $ndvoucherlast!='0')?$ndvoucherlast:'';
		$data['novoucher']		= (!empty($no_voucher) && $no_voucher!='0')?$no_voucher:'';
		$data['ivoucher']		= (!empty($i_voucher) && $i_voucher!='0')?$i_voucher:'';
		
		$turi1	= ($no_voucher!='' && $no_voucher!='0')?$no_voucher:'0';
		$turi2	= ($i_voucher!='' && $i_voucher!='0')?$i_voucher:'0';
		$turi3	= $dvoucherfirst;
		$turi4	= $dvoucherlast;
		
		$this->load->model('listpelunasanfaktur/mclass');
		
		$pagination['base_url'] 	= '/listpelunasanfaktur/cform/carilistvouchernext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/';
		
		if($dvoucherfirst!='0' && $dvoucherlast!='0') {
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage1($i_voucher,$dvoucherfirst,$dvoucherlast);
		}else{
			$qlistvoucherallpage	= $this->mclass->clistvoucherallpage2($i_voucher,$dvoucherfirst,$dvoucherlast);
		}
		
		$pagination['total_rows']	= $qlistvoucherallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(8,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dvoucherfirst!='0' && $dvoucherlast!='0') {
			$data['isi']	= $this->mclass->clistvoucher1($pagination['per_page'],$pagination['cur_page'],$i_voucher,$dvoucherfirst,$dvoucherlast);
			$data['template'] = 1;
		}else{
			$data['isi']	= $this->mclass->clistvoucher2($pagination['per_page'],$pagination['cur_page'],$i_voucher,$dvoucherfirst,$dvoucherlast);
			$data['template'] = 2;
		}
		
		$this->load->view('listpelunasanfaktur/vlistform',$data);
					
	}
	
	function listvoucher() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "VOUCHER";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpelunasanfaktur/mclass');

		$query	= $this->mclass->lvoucher();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpelunasanfaktur/cform/listvouchernext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lvoucherperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('listpelunasanfaktur/vlistvoucher',$data);			
	}


	function listvouchernext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "VOUCHER";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listpelunasanfaktur/mclass');

		$query	= $this->mclass->lvoucher();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/listpelunasanfaktur/cform/listvouchernext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lvoucherperpages($pagination['per_page'],$pagination['cur_page']);		
				
		$this->load->view('listpelunasanfaktur/vlistvoucher',$data);			
	}		

	function flistvoucher() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');

		$data['page_title']	= "VOUCHER";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;
		
		$this->load->model('listpelunasanfaktur/mclass');
		
		$query	= $this->mclass->flvoucher($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		
		if($jml>0) {
			
			$cc	= 1;
			
			foreach($query->result() as $row){
				
				$exp_d_voucher = explode('-',$row->d_voucher,strlen($row->d_voucher));
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_voucher_code','$row->i_voucher','$row->i_voucher_no')\">".$row->i_voucher_code." - ".$row->i_voucher_no."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_voucher_code','$row->i_voucher','$row->i_voucher_no')\">".$exp_d_voucher[2].'/'.$exp_d_voucher[1].'/'.$exp_d_voucher[0]."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
}
?>
