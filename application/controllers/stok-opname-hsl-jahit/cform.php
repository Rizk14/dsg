<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('stok-opname-hsl-jahit/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$data['isi'] = 'stok-opname-hsl-jahit/vmainform';
	$data['msg'] = '';
	$data['bulan_skrg'] = date("m");
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    // =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$gudang = $this->input->post('gudang', TRUE);  

	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
			
	if ($bulan > 1) {
		$bulan_sebelumnya = $bulan-1;
		$tahun_sebelumnya = $tahun;
	}
	else if ($bulan == 1) {
		$bulan_sebelumnya = 12;
		$tahun_sebelumnya = $tahun-1;
	}
	// 1 Cek lokasi gudang
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi=b.id WHERE 
								a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	
	//2 Cek Stok Opname Hasil Jahit by id gudang
	$sql = "SELECT id FROM tt_stok_opname_hasil_jahit WHERE status_approve = 'f' AND id_gudang = '$gudang' ";
		
	if ($bulan == 1) // 14-04-2014, adopsi dari SO unit jahit
		$sql.= " AND bulan<='12' AND tahun<'$tahun' ";
	else
		$sql.= " AND ((bulan < '$bulan' AND tahun ='$tahun') OR (bulan <='12' AND tahun <'$tahun')) ";
	
	$query3	= $this->db->query($sql);
	
	if ($query3->num_rows() > 0){
		$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." tidak dapat diproses karena SO di bulan sebelumnya belum beres..!";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'stok-opname-hsl-jahit/vmainform';
		$this->load->view('template',$data);
	}
	else {
		// 3 cek apakah sudah ada data SO di bulan setelahnya
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit WHERE bulan > '$bulan' AND tahun >= '$tahun' 
							AND id_gudang = '$gudang' ");

		if ($query3->num_rows() > 0){
			$data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." tidak dapat diproses karena di bulan berikutnya sudah ada SO..!";
			$data['list_gudang'] = $this->mmaster->get_gudang(); // tidak ada respon apa apa, abaikan
			$data['isi'] = 'stok-opname-hsl-jahit/vmainform';
			$this->load->view('template',$data);
		}
		else {
			$data['nama_bulan'] = $nama_bln;
			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['gudang'] = $gudang;
			$data['kode_gudang'] = $kode_gudang;
			$data['nama_gudang'] = $nama_gudang;
			$data['nama_lokasi'] = $nama_lokasi;

			// 4 Cek SO nya 
			$cek_data = $this->mmaster->cek_so($bulan, $tahun, $gudang); 
			
			if ($cek_data['idnya'] == '' ) {
				// 20-10-2015, CEK APAKAH DATA DI TABEL SO MASIH KOSONG? JIKA MASIH KOSONG, MAKA MUNCULKAN FORM INPUT BARU
				$is_sokosong = $this->mmaster->cek_sokosong($gudang); 
				if ($is_sokosong == 'f') { // JIKA SUDAH ADA SO
					// jika data so blm ada, maka ambil stok terkini dari tabel tm_stok_hasil_jahit_warna
					$data['query'] = $this->mmaster->get_all_stok_hasil_jahit($bulan, $tahun, $gudang); //
				
					$data['is_new'] = '1';
					if (is_array($data['query']) )
						$data['jum_total'] = count($data['query']);
					else
						$data['jum_total'] = 0;
					
					$data['tgl_so'] = '';
					$data['jenis_perhitungan_stok'] = '';
					$data['isi'] = 'stok-opname-hsl-jahit/vformview';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 20-10-2015 ---------------
					$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					$data['is_new'] = '1';
					$data['tgl_so'] = '';
					$data['id_so'] = $id_so;
					$data['isi'] = 'stok-opname-hsl-jahit/vformviewfirst';
					$this->load->view('template',$data);
				}
			}
			else { // jika sudah diapprove maka munculkan msg
				if ($cek_data['status_approve'] == 't') {
					$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah di-approve..!";
					$data['list_gudang'] = $this->mmaster->get_gudang();
					$data['isi'] = 'stok-opname-hsl-jahit/vmainform';
					$this->load->view('template',$data);
				}
				else {
					// ---------- 20-10-2015 ---------------
					$query3	= $this->db->query("SELECT id FROM tt_stok_opname_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang='$gudang' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_so	= $hasilrow->id;
					}
					else
						$id_so = 0;
					// -------------------------------------
					
					// get data dari tabel tt_stok_opname_hasil_jahit yg statusnya 'f'
					$data['query'] = $this->mmaster->get_all_stok_opname($bulan, $tahun, $gudang);
			
					$data['is_new'] = '0';
					$data['jum_total'] = count($data['query']);
					
					$tgl_so = $cek_data['tgl_so'];
					$pisah1 = explode("-", $tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
					
					$data['tgl_so'] = $tgl_so;
					$data['id_so'] = $id_so;
					$data['jenis_perhitungan_stok'] = $cek_data['jenis_perhitungan_stok'];
					
					// 20-10-2015 sementara dikomen dulu, karena utk input SO pertama kali
					// 30-01-2016 kasih pengecekan, apakah ini SO satu2nya yg pernah diinput. kalo ya, maka munculkan firstedit
					$is_satu2nya = $this->mmaster->cek_sopertamakali($gudang);
					if ($is_satu2nya == 'f')
						$data['isi'] = 'stok-opname-hsl-jahit/vformview';
					else
						$data['isi'] = 'stok-opname-hsl-jahit/vformviewfirstedit';
					$this->load->view('template',$data);
				}
			} // end else
		} // end else bln setelahnya
	} // end else bln sebelumnya
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++

  }
  
  function submit() {
	  // =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $gudang = $this->input->post('gudang', TRUE);  
	  // $no ga dipake lg, ganti jadi $jum_data
	  //$no = $this->input->post('no', TRUE);  	  
	  
	  // 20-10-2015. 04-02-2016 diganti pake pengecekan yg dibawah ini
	  //$jum_data = $this->input->post('jum_data', TRUE);
	  
	  // utk pertama kali, pake yg ini
	  //$jum_data = $this->input->post('no', TRUE)-1; 

	  $is_new = $this->input->post('is_new', TRUE);  
	  $tgl = date("Y-m-d H:i:s"); 
	  $submit2 = $this->input->post('submit2', TRUE);
	  
	  $is_pertamakali = $this->input->post('is_pertamakali', TRUE);
	  // 04-11-2015
	  $jenis_hitung 	= $this->input->post('jenis_hitung', TRUE);
	  
	  // 30-01-2016
	  if ($is_pertamakali == '1')
		$jum_data = $this->input->post('no', TRUE)-1; 
	  else
		$jum_data = $this->input->post('jum_data', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$gudang' ");
	  $hasilrow = $query3->row();
	  $kode_gudang	= $hasilrow->kode_gudang;
	  $nama_gudang	= $hasilrow->nama;
	  $nama_lokasi	= $hasilrow->nama_lokasi;
	  
	  if ($is_new == '1') {
		  $uid_update_by = $this->session->userdata('uid');
	      // insert ke tabel tt_stok_opname_
	      $data_header = array(
			'id_gudang'=>$gudang,
			  'bulan'=>$bulan,
			  'tahun'=>$tahun,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'tgl_so'=>$tgl_so,
			  'jenis_perhitungan_stok'=>$jenis_hitung,
			  'uid_update_by'=>$uid_update_by
			);
		  $this->db->insert('tt_stok_opname_hasil_jahit',$data_header); 
		  
		  // ambil data terakhir di tabel tt_stok_opname_
		 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit ORDER BY id DESC LIMIT 1 ");
		 $hasilrow = $query2->row();
		 $id_stok	= $hasilrow->id;
	      
	      for ($i=1;$i<=$jum_data;$i++)
		  {
			 $this->mmaster->save($is_new, $id_stok, $is_pertamakali, $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_'.$i, TRUE),
			 $this->input->post('stok_fisik_warna_'.$i, TRUE),
			 // 01-12-2015
			 $this->input->post('id_warna2_'.$i, TRUE),
			 $this->input->post('saldo_akhir_warna_'.$i, TRUE),
			 
			 // 26-01-2016, item brg yg mau dihapus dari tm_stok_hasil_jahit
			 $gudang, $this->input->post('hapusitem_'.$i, TRUE)
			 );
		  }
		  //die();
		 // redirect('stok-opname-hsl-jahit/cform');
		$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah berhasil disimpan. Silahkan lakukan approval untuk mengupdate stok";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'stok-opname-hsl-jahit/vmainform';
		$this->load->view('template',$data);
	  }
	  else {
		  if ($submit2 != '') { // function hapus
			  // ambil data id dari tabel tt_stok_opname_hasil_jahit
			 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit WHERE id_gudang = '$gudang'
							AND bulan = '$bulan' AND tahun = '$tahun' ");
			 $hasilrow = $query2->row();
			 $id_stok	= $hasilrow->id; 
			 
			 // 06-02-2014, query ke tabel tt_stok_opname_hasil_jahit_detail utk hapus tiap2 item warnanya
			 $sqlxx	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail
								WHERE id_stok_opname_hasil_jahit = '$id_stok' ");
			if ($sqlxx->num_rows() > 0){
				$hasilxx = $sqlxx->result();
				foreach ($hasilxx as $rowxx) {
					$this->db->query(" DELETE FROM tt_stok_opname_hasil_jahit_detail_warna WHERE id_stok_opname_hasil_jahit_detail='$rowxx->id' ");
				} // end foreach detail
			} // end if
			 //==============================================================================================
			 
			$this->db->query(" delete from tt_stok_opname_hasil_jahit_detail where id_stok_opname_hasil_jahit = '$id_stok' ");
			$this->db->query(" delete from tt_stok_opname_hasil_jahit where id = '$id_stok' ");
			//redirect('stok-opname-hsl-jahit/cform');
			
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah berhasil dihapus";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'stok-opname-hsl-jahit/vmainform';
			$this->load->view('template',$data);
		  }
		  else { // function update
			  if ($is_pertamakali == '1') {
				  $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit WHERE
								bulan = '$bulan' AND tahun = '$tahun' AND id_gudang='$gudang' ");
					$hasilrow = $query2->row();
					$id_stok	= $hasilrow->id;
					
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tt_stok_opname_hasil_jahit SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								jenis_perhitungan_stok='$jenis_hitung', uid_update_by='$uid_update_by' where id = '$id_stok' ");
					
					// 1. update data yg sudah ada
					$jum_data_ada = $this->input->post('jum_data', TRUE); 
					for ($i=1;$i<=$jum_data_ada;$i++)
					 {									
						$id_brg_wip1 = $this->input->post('id_brg_wip1_'.$i, TRUE);
						$id_warna1 = $this->input->post('id_warna1_'.$i, TRUE);
						$qty_warna1 = $this->input->post('qty_warna1_'.$i, TRUE);
						$stok1 = $this->input->post('stok1_'.$i, TRUE);
						// 01-12-2015
						$id_warna12 = $this->input->post('id_warna12_'.$i, TRUE);
						$qty_warna12 = $this->input->post('qty_warna12_'.$i, TRUE);
						
						$queryxx	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail WHERE
								id_stok_opname_hasil_jahit = '$id_stok' AND id_brg_wip = '$id_brg_wip1' ");
						$hasilxx = $queryxx->row();
						$iddetail	= $hasilxx->id;
						
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna1); $xx++) {
							$id_warna1[$xx] = trim($id_warna1[$xx]);
							$stok1[$xx] = trim($stok1[$xx]);
							$qtytotalstokawal+= $stok1[$xx];				
							$qty_warna1[$xx] = trim($qty_warna1[$xx]);
							$qtytotalstokfisikwarna+= $qty_warna1[$xx];
							
							// 01-12-2015
							$id_warna12[$xx] = trim($id_warna12[$xx]);
							$qty_warna12[$xx] = trim($qty_warna12[$xx]);
							$qtytotalsaldoakhirwarna+= $qty_warna12[$xx];
							
							// 04-02-2016 update di tabel perwarna
							$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail_warna SET jum_stok_opname = '".$qty_warna1[$xx]."',
									saldo_akhir = '".$qty_warna12[$xx]."' WHERE id_stok_opname_hasil_jahit_detail = '$iddetail'
									AND id_warna = '".$id_warna1[$xx]."' ");
							
						} // end for
						
						$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '".$qtytotalstokfisikwarna."', 
									saldo_akhir = '".$qtytotalsaldoakhirwarna."' where id_brg_wip = '$id_brg_wip1' AND id_stok_opname_hasil_jahit = '$id_stok' ");
						
					 } // end for
					// --------------------------------------------------------
						 
					 // 2. insert data item brg baru
					 for ($i=1;$i<=$jum_data;$i++)
					 {						
						$tgl = date("Y-m-d H:i:s"); 
					  //-------------- hitung total qty dari detail tiap2 warna -------------------
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_warna = $this->input->post('id_warna_'.$i, TRUE);
						$stok_fisik_warna = $this->input->post('stok_fisik_warna_'.$i, TRUE);
						$stok = $this->input->post('stok_'.$i, TRUE);
						// 01-12-2015
						$id_warna2 = $this->input->post('id_warna2_'.$i, TRUE);
						$saldo_akhir_warna = $this->input->post('saldo_akhir_warna_'.$i, TRUE);
												
						$qtytotalstokawal = 0;
						$qtytotalstokfisikwarna = 0;
						$qtytotalsaldoakhirwarna = 0;
						for ($xx=0; $xx<count($id_warna); $xx++) {
							$id_warna[$xx] = trim($id_warna[$xx]);
							$stok[$xx] = trim($stok[$xx]);
							$qtytotalstokawal+= $stok[$xx];				
							$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
							$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
							
							// 01-12-2015
							$id_warna2[$xx] = trim($id_warna2[$xx]);
							$saldo_akhir_warna[$xx] = trim($saldo_akhir_warna[$xx]);
							$qtytotalsaldoakhirwarna+= $saldo_akhir_warna[$xx];
						} // end for
						
						// 04-02-2016
						if ($id_brg_wip != '') {
							$data_detail = array(
										'id_stok_opname_hasil_jahit'=>$id_stok,
										'id_brg_wip'=>$id_brg_wip, 
										'stok_awal'=>$qtytotalstokawal,
										'jum_stok_opname'=>$qtytotalstokfisikwarna,
										'saldo_akhir'=>$qtytotalsaldoakhirwarna
									);
						   $this->db->insert('tt_stok_opname_hasil_jahit_detail',$data_detail);
						   
						   $seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
						   if($seq_detail->num_rows() > 0) {
								$seqrow	= $seq_detail->row();
								$iddetail = $seqrow->id;
						   }
						   else
								$iddetail = 0;
								
						   // ------------------stok berdasarkan warna brg wip----------------------------
						   if (isset($id_warna) && is_array($id_warna)) {
							for ($xx=0; $xx<count($id_warna); $xx++) {
								$id_warna[$xx] = trim($id_warna[$xx]);
								$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
								//01-12-2015
								$id_warna2[$xx] = trim($id_warna2[$xx]);
								$saldo_akhir_warna[$xx] = trim($saldo_akhir_warna[$xx]);
								
								$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
								if($seq_warna->num_rows() > 0) {
									$seqrow	= $seq_warna->row();
									$idbaru	= $seqrow->id+1;
								}else{
									$idbaru	= 1;
								}
								
								$tt_stok_opname_hasil_jahit_detail_warna	= array(
										 'id'=>$idbaru,
										 'id_stok_opname_hasil_jahit_detail'=>$iddetail,
										 'id_warna'=>$id_warna[$xx],
										 'jum_stok_opname'=>$stok_fisik_warna[$xx],
										 'saldo_akhir'=>$saldo_akhir_warna[$xx]
									);
								$this->db->insert('tt_stok_opname_hasil_jahit_detail_warna',$tt_stok_opname_hasil_jahit_detail_warna);
							} // end for
						  } // end if
						}
						// ----- end lanjutan 07-10-2015 ----------
					// ---------------------------------------------------------------------
				} // END FOR
			  }
			  else { 
				   // update ke tabel tt_stok_opname_
				   // ambil data terakhir di tabel tt_stok_opname_
					 $query2	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit WHERE id_gudang = '$gudang' 
									AND bulan = '$bulan' AND tahun = '$tahun' ");
					 $hasilrow = $query2->row();
					 $id_stok	= $hasilrow->id; 
					 
					 $uid_update_by = $this->session->userdata('uid');
					 $this->db->query(" UPDATE tt_stok_opname_hasil_jahit SET tgl_so = '$tgl_so', tgl_update = '$tgl', 
								jenis_perhitungan_stok='$jenis_hitung', uid_update_by='$uid_update_by' where id = '$id_stok' ");
					 
				  for ($i=1;$i<=$jum_data;$i++)
				  {
					 $this->mmaster->save($is_new, $id_stok, $is_pertamakali,
					 $this->input->post('id_brg_wip_'.$i, TRUE), 
					 $this->input->post('id_warna_'.$i, TRUE),
					 $this->input->post('stok_'.$i, TRUE),
					 $this->input->post('stok_fisik_warna_'.$i, TRUE),
					 // 01-12-2015
					 $this->input->post('id_warna2_'.$i, TRUE),
					 $this->input->post('saldo_akhir_warna_'.$i, TRUE),
					 // 26-01-2016, item brg yg mau dihapus dari tm_stok_hasil_jahit
					$gudang, $this->input->post('hapusitem_'.$i, TRUE)
					 );
				  }
			  }
			//  redirect('stok-opname-hsl-jahit/cform');
			$data['msg'] = "Input stok opname WIP untuk bulan ".$nama_bln." ".$tahun." di gudang [".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." sudah berhasil diupdate. Silahkan lakukan approval untuk mengupdate stok";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'stok-opname-hsl-jahit/vmainform';
			$this->load->view('template',$data);
		}
      }
  }
  
  // 20-10-2015
  function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$isedit 	= $this->input->post('isedit', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		if ($isedit == '0') {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
										WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		else {
			// query ke tabel tm_barang_wip utk ambil kode, nama
			$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '".$kode_brg_wip."' ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
			}
			else {
				$id_brg_wip = '0';
			}
			
			$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
					WHERE id = '".$id_brg_wip."' AND id NOT IN 
					(SELECT b.id_brg_wip FROM tt_stok_opname_hasil_jahit a 
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
					WHERE a.id = '$id_so' ) ");
					
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_brg_wip = $hasilxx->id;
				$nama_brg_wip = $hasilxx->nama_brg;
			}
			else {
				$id_brg_wip = '';
				$nama_brg_wip = '';
			}
		}
		
		$data['id_brg_wip'] = $id_brg_wip;
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('stok-opname-hsl-jahit/vinfobrgwip', $data); 
		return true;
  }
  
  function additemwarna(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		if ($id_so != '') {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							AND a.id_brg_wip NOT IN 
						(SELECT b.id_brg_wip FROM tt_stok_opname_hasil_jahit a 
						INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
						WHERE a.id = '$id_so' )
							ORDER BY b.nama");
		}
		else {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							ORDER BY b.nama");
		}
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('stok-opname-hsl-jahit/vlistwarna', $data); 
		return true;
  }
  
  // 01-12-2015
  function additemwarna2(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$id_so 	= $this->input->post('id_so', TRUE);
		
		$queryxx = $this->db->query(" SELECT id FROM tm_barang_wip WHERE kode_brg = '$kode_brg_wip' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
		}
		else
			$id_brg_wip = 0;
		
		// query ambil data2 warna berdasarkan kode brgnya
		if ($id_so != '') {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							AND a.id_brg_wip NOT IN 
						(SELECT b.id_brg_wip FROM tt_stok_opname_hasil_jahit a 
						INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
						WHERE a.id = '$id_so' )
							ORDER BY b.nama");
		}
		else {
			$queryxx = $this->db->query(" SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_brg_wip = '".$id_brg_wip."' 
							ORDER BY b.nama");
		}
		
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_wip'] = $kode_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('stok-opname-hsl-jahit/vlistwarna2', $data); 
		return true;
  }
  
  // 01-02-2016 backdoor utk tambahin item brg yg ga ada di tm_stok_hasil_jahit, relasi ke tt_stok_opname_hasil_jahit
  function backdoor_insert_item_brg_tm_stok_hasil_jahit() {
	  $tgl = date("Y-m-d H:i:s"); 
		$queryxx = $this->db->query(" select b.* from tt_stok_opname_hasil_jahit a inner join tt_stok_opname_hasil_jahit_detail b 
ON a.id = b.id_stok_opname_hasil_jahit where a.bulan='12' AND a.tahun='2015' AND b.id_brg_wip not in (
select id_brg_wip from tm_stok_hasil_jahit)
");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE id_brg_wip = '$rowxx->id_brg_wip' ");
				if ($query3->num_rows() == 0){
					echo "Des<br>"; echo $rowxx->id_brg_wip."<br>";
					$data_stok = array(
							'id_brg_wip'=>$rowxx->id_brg_wip,
							'id_gudang'=>'7',
							'stok'=>0,
							'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok_hasil_jahit', $data_stok);
				}
				else
					echo "udah ada<br>";
			}
		}
   }
   
   // 02-02-2016 backdoor utk tambahin item brg yg ga ada di tm_stok_unit_jahit, relasi ke tt_stok_opname_unit_jahit
  function backdoor_insert_item_brg_tm_stok_unit_jahit() {
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  $sql = " SELECT id FROM tm_unit_jahit ORDER BY id "; 
	  $query = $this->db->query($sql);
	  if ($query->num_rows() > 0){
		  $hasil = $query->result();
			foreach ($hasil as $row) {
				$queryxx = $this->db->query(" select b.* from tt_stok_opname_unit_jahit a 
				inner join tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit where a.bulan='12' 
				AND a.tahun='2015' AND a.id_unit = '$row->id'
				AND b.id_brg_wip not in (select id_brg_wip from tm_stok_unit_jahit WHERE id_unit='$row->id') ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_jahit WHERE 
												id_brg_wip = '$rowxx->id_brg_wip' AND id_unit = '$row->id' ");
						if ($query3->num_rows() == 0){
							echo $rowxx->id_brg_wip."<br>";
							
							$seq	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seq->num_rows() > 0) {
								$seqrow	= $seq->row();
								$id_stok	= $seqrow->id+1;
							}else{
								$id_stok	= 1;
							}
					
							$data_stok = array(
									'id'=>$id_stok,
									'id_brg_wip'=>$rowxx->id_brg_wip,
									'id_unit'=>$row->id,
									'stok'=>0,
									'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else
							echo "udah ada<br>";
					}
				}
			} // END FOR
	  } // END IF		
   }
}
