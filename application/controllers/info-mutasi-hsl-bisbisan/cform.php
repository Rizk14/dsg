<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-mutasi-hsl-bisbisan/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['isi'] = 'info-mutasi-hsl-bisbisan/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
    $data['isi'] = 'info-mutasi-hsl-bisbisan/vformview';
    //$kode_brg_jadi = $this->input->post('kode_brg_jadi', TRUE);
    $kode_brg = $this->input->post('kode_brg', TRUE); // ini kode_brg_bisbisan
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	//$brg_jadi = $this->input->post('brg_jadi', TRUE);  
	$brg_baku = $this->input->post('brg_baku', TRUE);  
	
	if ($date_from == '' && $date_to == '' && $kode_brg == '' && $brg_baku == '' ) {
		$date_from 	= $this->uri->segment(5);
		$date_to 	= $this->uri->segment(6);
		$kode_brg 	= $this->uri->segment(7);
		$brg_baku 	= $this->uri->segment(8);
	}

    $jum_total = $this->mmaster->get_mutasi_stoktanpalimit($date_from, $date_to, $kode_brg);
			$config['base_url'] = base_url().'index.php/info-mutasi-hsl-bisbisan/cform/view/index/'.$date_from.'/'.$date_to.'/'.$kode_brg.'/'.$brg_baku.'/';
			//$config['total_rows'] = $query->num_rows(); 
			$config['total_rows'] = count($jum_total); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_mutasi_stok($config['per_page'],$this->uri->segment(11), $date_from, $date_to, $kode_brg);
	$data['jum_total'] = count($jum_total);
	$data['kode_brg'] = $kode_brg;
	
	$data['brg_baku'] = $brg_baku;
	//$data['brg_jadi'] = $brg_jadi;
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$this->load->view('template',$data);
  }
  
  function show_popup_brg_jadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================

		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' ) {
			$keywordcari 	= $this->uri->segment(4);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
					  
		$qjum_total = $this->mmaster->get_brg_jaditanpalimit($keywordcari);
		
		$config['base_url'] = base_url()."index.php/info-mutasi-hsl-bisbisan/cform/show_popup_brg_jadi/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brg_jadi($config['per_page'],$config['cur_page'], $keywordcari);

	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$data['startnya'] = $config['cur_page'];

	$this->load->view('info-mutasi-hsl-bisbisan/vpopupbrgjadi',$data);
  }
  
  // popup bhn hsl bisbisan
function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		//$kode_brg_jadi	= $this->uri->segment(4);
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		/*if ($kode_brg_jadi == '') {
			$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);  
		} */
	
		//if ($keywordcari == '' && $kode_brg_jadi == '') {
		if ($keywordcari == '') {
			$keywordcari 	= $this->uri->segment(4);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
					  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari);
		
		$config['base_url'] = base_url()."index.php/info-mutasi-hsl-bisbisan/cform/show_popup_brg/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari);

	$data['jum_total'] = count($qjum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	//$data['kode_brg_jadi'] = $kode_brg_jadi;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('info-mutasi-hsl-bisbisan/vpopupbrg',$data);
  }
  
}
