<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('app-stok-opname-hsl-packing/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$data['isi'] = 'app-stok-opname-hsl-packing/vmainform';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

	$cek_data = $this->mmaster->cek_so($bulan, $tahun); 
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." belum diinput..!";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'app-stok-opname-hsl-packing/vmainform';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname-hsl-packing/vmainform';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_ yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname($bulan, $tahun);
	
			$data['jum_total'] = count($data['query']);
			$data['isi'] = 'app-stok-opname-hsl-packing/vformview';
			$this->load->view('template',$data);
			
		}
	} // end else
  }
  
  function submit() {
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  
	  $tgl = date("Y-m-d"); 
		  	      
	      for ($i=1;$i<=$no;$i++)
		  {
			 // cek ada selisih ga.
			 $kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
			 $stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE);
			 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
			 $idnya = $this->input->post('id_'.$i, TRUE);
			 
			 if ($jum_stok != $stok_fisik) {
				$selisih = $jum_stok-$stok_fisik;
				
			  // jika ada selisih, maka:
			  // insert ke tabel tt_stok,  kalo ada selisih (+ atau -) maka insert. kalo ga ada ya nggak perlu
				if ($selisih < 0) {
					// ============ reset stok =====================
							//cek stok terakhir tm_stok, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_packing 
													WHERE kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+(abs($selisih));
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_packing, insert
								$data_stok = array(
									'kode_brg_jadi'=>$kode_brg_jadi,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_packing',$data_stok);
							}
							else {
								//update ke tm_stok_hasil_packing dan history stoknya, 
								$this->db->query(" UPDATE tm_stok_hasil_packing SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg_jadi= '$kode_brg_jadi' ");
							}
														
							$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_packing (kode_brg_jadi, no_bukti, masuk, saldo, tgl_input) 
								VALUES ('$kode_brg_jadi','SO-".$bulan.$tahun."', '".abs($selisih)."', '$new_stok', '$tgl') ");
							
							//dan update statusnya di tt_stok_opname_hasil_packing detailnya
							$this->db->query(" UPDATE tt_stok_opname_hasil_packing_detail SET status_approve = 't'
								where id = '$idnya' ");
							
					// ==============================================
				}
				else if ($selisih > 0) {
					// ============ reset stok =====================
							//cek stok terakhir tm_stok_, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_packing 
												WHERE kode_brg_jadi = '$kode_brg_jadi' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$selisih;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_packing, insert
								$data_stok = array(
									'kode_brg_jadi'=>$kode_brg_jadi,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_packing',$data_stok);
							}
							else {
								//update ke tm_stok_hasil_packing dan history stoknya, 
								$this->db->query(" UPDATE tm_stok_hasil_packing SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg_jadi= '$kode_brg_jadi' ");
							}
														
							$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_packing (kode_brg_jadi, no_bukti, keluar, saldo, tgl_input) 
								VALUES ('$kode_brg_jadi','SO-".$bulan.$tahun."', '".$selisih."', '$new_stok', '$tgl') ");
							
							//dan update statusnya di tt_stok_opname_hasil_packing detailnya
							$this->db->query(" UPDATE tt_stok_opname_hasil_packing_detail SET status_approve = 't'
								where id = '$idnya' ");
							
					// ==============================================
				}
			 } // end if jika ada selisih
			 
			 $this->db->query(" UPDATE tt_stok_opname_hasil_packing_detail SET status_approve = 't'
								where id = '$idnya' ");

		  } // end for
		  $this->db->query(" UPDATE tt_stok_opname_hasil_packing SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  
		  redirect('app-stok-opname-hsl-packing/cform');
  }

}
