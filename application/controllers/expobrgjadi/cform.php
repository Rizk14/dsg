<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expobrgjadi/mclass');
	}
	
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			
			$data['isi']	= 'expobrgjadi/vmainform';	
			$this->load->view('template',$data);	
	}
	
	function carilistopvsdo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title_opvsdo']	= $this->lang->line('page_title_opvsdo');
		$data['form_title_detail_opvsdo']	= $this->lang->line('form_title_detail_opvsdo');
		$data['list_opvsdo_tgl_mulai_op']	= $this->lang->line('list_opvsdo_tgl_mulai_op');
		$data['list_opvsdo_t_permintaan']	= $this->lang->line('list_opvsdo_t_permintaan');
		$data['list_opvsdo_t_pengiriman']	= $this->lang->line('list_opvsdo_t_pengiriman');
		$data['list_opvsdo_n_permintaan']	= $this->lang->line('list_opvsdo_n_permintaan');
		$data['list_opvsdo_n_penjualan']	= $this->lang->line('list_opvsdo_n_penjualan');
		$data['list_opvsdo_selisih']	= $this->lang->line('list_opvsdo_selisih');
		$data['list_opvsdo_t_n_selisih']	= $this->lang->line('list_opvsdo_t_n_selisih');
		$data['list_opvsdo_stop_produk']	= $this->lang->line('list_opvsdo_stop_produk');
		$data['list_opvsdo_kd_brg']	= $this->lang->line('list_opvsdo_kd_brg');
		$data['list_opvsdo_nm_brg']	= $this->lang->line('list_opvsdo_nm_brg');
		$data['list_opvsdo_unit_price']	= $this->lang->line('list_opvsdo_unit_price');
		$data['list_opvsdo_op']	= $this->lang->line('list_opvsdo_op');
		$data['list_opvsdo_n_op']	= $this->lang->line('list_opvsdo_n_op');
		$data['list_opvsdo_do']	= $this->lang->line('list_opvsdo_do');
		$data['list_opvsdo_n_do']	= $this->lang->line('list_opvsdo_n_do');
		$data['list_opvsdo_selisih_opdo']	= $this->lang->line('list_opvsdo_selisih_opdo');
		$data['list_opvsdo_n_selisih']	= $this->lang->line('list_opvsdo_n_selisih');
		
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
		
		$i_product	= $this->input->post('i_product');
		$iproduct	= !empty($i_product)?$i_product:"kosong";
		$d_op_first	= $this->input->post('d_op_first');
		$d_op_last	= $this->input->post('d_op_last');
		$f_stop_produksi	= $this->input->post('f_stop_produksi');
		$stop	= $this->input->post('stop');
		$stopproduct	= $stop==1?'TRUE':'FALSE';
		$fdropforcast	= $this->input->post('fdropforcast');
		$is_grosir	= $this->input->post('is_grosir');
		
		$icustomer	= $this->input->post('customer');
		
		$data['tglopmulai']	= $d_op_first;
		$data['tglopakhir']	= $d_op_last;
		$data['kproduksi']	= $i_product;
		$data['sproduksi']	= $f_stop_produksi==1?' checked ':'';
		$data['stop']		= $stop==1?' checked ':'';
		$data['stop2']		= $stop==1?'1':'0';
		
		$e_d_do_first	= explode("/",$d_op_first,strlen($d_op_first));
		$e_d_do_last	= explode("/",$d_op_last,strlen($d_op_last));
		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:'0';
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:'0';
		
		$data['iproduct']	= !empty($i_product)?$i_product:"kosong";
		$data['tdofirst']	= $n_d_do_first;
		$data['tdolast']	= $n_d_do_last;
		$data['icustomer']	= $icustomer;
		$data['fdropforcast']	= $fdropforcast;
		$data['is_grosir']	= $is_grosir;
		
		$data['var_iproduct']	= $iproduct;
		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;
		$data['var_stopproduct']= $stop==1?'TRUE':'FALSE';
		
		$data['customer']	= $this->mclass->lcustomer();
		$data['query']	= $this->mclass->clistopvsdo_new($i_product,$n_d_do_first,$n_d_do_last,$stopproduct,$fdropforcast, $is_grosir);
		$data['isixx']	= $this->mclass->clistopvsdo_dokosong($i_product,$n_d_do_first,$n_d_do_last,$stopproduct,$fdropforcast, $is_grosir);
		
		$data['isi']	= 'expoopvsdogrosir/vlistform';	
		$this->load->view('template',$data);	
		
	}
	
	
	function listbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/expoopvsdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
	
		$this->load->view('expoopvsdo/vlistformbrgjadi',$data);	
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expoopvsdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('expoopvsdo/vlistformbrgjadi',$data);	
	}	
	
	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		if(!empty($key)) {
			$query	= $this->mclass->flbarangjadi($key);
			$jml	= $query->num_rows();
		}else{
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->iproduct."</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->imotif."</a></td>
				  <td><a href=\"javascript:settextfield('$row->imotif')\">".$row->motifname."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}
	
	function exportbrgjadi() {
				$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
	$db2=$this->load->database('db_external', TRUE);
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
		$tanggalnya = date("d-m-Y");
			
		$i_product	= $this->input->post('i_product');
		$allitem	= $this->input->post('allitem');
		$d_first	= $this->input->post('d_first');
		$d_last	= $this->input->post('d_last');
		
		$awalasli = $d_first;
		$akhirasli = $d_last;
		$pisah1 = explode("/", $d_first);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$d_first = $thn1."-".$bln1."-".$tgl1;
		
		$pisah1 = explode("/", $d_last);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$d_last = $thn1."-".$bln1."-".$tgl1;
		
		//$data['is_grosir']	= $is_grosir;
		
		$qcompany = $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ORDER BY i_initial DESC LIMIT 1 ");
		if($qcompany->num_rows()>0) {
			$rcompany = $qcompany->row();
			$e_initial_name	= $rcompany->e_initial_name;		
		}else{
			$e_initial_name	= "";
		}
						
		$ObjPHPExcel = new PHPExcel();
		
		$qexpobrgjadi	= $this->mclass->expbrgjadi($i_product, $allitem);
		//if($qexpobrgjadi->num_rows()>0) {
		
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("IT Dept 2012");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("IT Dept 2012");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Data Barang Jadi")
				->setSubject("Laporan Data Barang Jadi")
				->setDescription("Laporan Data Barang Jadi")
				->setKeywords("Laporan Data Barang Jadi")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();
			
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4); // no
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // kode brg
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30); // nama brg
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14); // hjp
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10); // STP
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14); // stok awal
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14); // nilai stok awal
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14); // stok akhir
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14); // nilai stok akhir
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:I2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN DATA BARANG JADI');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:I3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $awalasli." s.d ".$akhirasli);
								
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:I4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', $e_initial_name);
						
			$ObjPHPExcel->getActiveSheet()->setCellValue('A7', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B7', 'Kode Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C7', 'Nama Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D7', 'HJP');
			$ObjPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('E7', 'STP');
			$ObjPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('F7', 'Stok Awal');
			$ObjPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('G7', 'Nilai Stok Awal');
			$ObjPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('H7', 'Stok Akhir');
			$ObjPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('I7', 'Nilai Stok Akhir');
			$ObjPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				); // sampe sini
			
			// if($qexpopvsdo->num_rows()>0) {
				
				$j	= 8;
				$nomor	= 1;
				$totstokawal = 0;
				$totnilaistokawal = 0;
				$totstokakhir = 0;
				$totnilaistokakhir = 0;
				foreach($qexpobrgjadi as $row) {
					//=============15-10-2012===================
					$qsoawal	= $this->mclass->soawal($d_first,$d_last,$row->i_product_motif);
					if($qsoawal->num_rows()>0) {
						$row_soawal	= $qsoawal->row();
						$jumsoawal	= $row_soawal->n_quantity_awal;
					} else {
						$jumsoawal = 0;
					}
					$totstokawal+= $jumsoawal;
					
					$nilaisoawal = $jumsoawal*$row->v_unitprice;
					$totnilaistokawal+= $nilaisoawal;
					
					$qbonmkeluar	= $this->mclass->lbonkeluar($d_first,$d_last,$row->i_product_motif);
					if($qbonmkeluar->num_rows()>0) {
						$row_bmkeluar	= $qbonmkeluar->row();
						$bonmkeluar	= $row_bmkeluar->jbonkeluar;
					} else {
						$bonmkeluar = 0;
					}

					$qbonmmasuk	= $this->mclass->lbonmasuk($d_first,$d_last,$row->i_product_motif);
					if($qbonmmasuk->num_rows()>0) {
						$row_bmmasuk	= $qbonmmasuk->row();
						$bonmmasuk	= $row_bmmasuk->jbonmasuk; 
					} else {
						$bonmmasuk = 0;
					}

					$qbbk	= $this->mclass->lbbk($d_first,$d_last,$row->i_product_motif);
					if($qbbk->num_rows()>0) {
						$row_bbk	= $qbbk->row();
						$bbk	= $row_bbk->jbbk; 
					} else {
						$bbk = 0;
					}

					$qbbm	= $this->mclass->lbbm($d_first,$d_last,$row->i_product_motif);
					if($qbbm->num_rows()>0) {
						$row_bbm	= $qbbm->row();
						$bbm	= $row_bbm->jbbm; 
					} else {
						$bbm = 0;
					}

					$qdo	= $this->mclass->ldo($d_first,$d_last,$row->i_product_motif);
					if($qdo->num_rows()>0) {
						$row_do	= $qdo->row();
						$do	= $row_do->jdo; 
					} else {
						$do = 0;
					}

					$qsj	= $this->mclass->lsj($d_first,$d_last,$row->i_product_motif);
					if($qsj->num_rows()>0) {
						$row_sj	= $qsj->row();
						$sj	= $row_sj->jsj;
					} else {
						$sj = 0;
					}
					
					$x11	= $bonmmasuk+$bbm;
					$x22	= $do+$bonmkeluar+$bbk+$sj;
					$z11	= $jumsoawal+$x11;
					$saldoakhirnya	= $z11-$x22;
					$nilaisaldoakhir = $saldoakhirnya*$row->v_unitprice;
					
					$totstokakhir+= $saldoakhirnya;
					$totnilaistokakhir+= $nilaisaldoakhir;
					//===============end========================
					
					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->i_product_motif);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->e_product_motifname);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

					/* $ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->unitprice); */
					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->v_unitprice);
					
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					if ($row->f_stop_produksi == 't')
						$fstp = "Ya";
					else
						$fstp = "Tidak";
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $fstp);
					
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					// F
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $jumsoawal);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					// G
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $nilaisoawal);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					// H
					//$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $bonmmasuk." ".$bonmkeluar." ".$bbm." ".$bbk." ".$do." ".$sj." ". $saldoakhirnya);
					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $saldoakhirnya);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					// I
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $nilaisaldoakhir);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
										
					$j++;																																																							
					$nomor++;
				}
				//die();
				//-----------------------------------------------------------
								
				$jj=$j;
				// $ObjPHPExcel->getActiveSheet()->mergeCells('A4:I4');
				// total
				
				$ObjPHPExcel->getActiveSheet()->mergeCells('A'.$jj.':E'.$jj);
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$jj, "TOTAL");
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$jj, $totstokawal);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$jj, $totnilaistokawal);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					/*
				 * 
				 * $totstokawal = 0;
				$totnilaistokawal = 0;
				$totstokakhir = 0;
				$totnilaistokakhir = 0;
				 * */
					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$jj, $totstokakhir);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
					
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$jj, $totnilaistokakhir);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$jj)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
			
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');

			$files	= $this->session->userdata('gid')."laporan_barangjadi"."_".$tanggalnya.".xls";	
			$ObjWriter->save("files/".$files);
																							
		//} 

		//$efilename = substr($files,1,strlen($files));
		//$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
		print "<script>alert(\"Data barang jadi telah dieksport, terimakasih.\");
		window.open(\"index\", \"_self\");
		
		$('#gambarnya').hide(); $('#btndetail').attr(\"disabled\", false);</script>";
		
	}

	function cpopup() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
			
		$iproduct	= $this->uri->segment(4);
		$tdofirst	= $this->uri->segment(5);
		$tdolast	= $this->uri->segment(6);
		$sproduct	= $this->uri->segment(7);
		$stop		= $this->uri->segment(8);
		
		$extdofirst	= explode("-",$tdofirst,strlen($tdofirst));
		$extdolast	=  explode("-",$tdolast,strlen($tdolast));
		
		$nwdofirst	= $extdofirst[2]."/".$extdofirst[1]."/".$extdofirst[0];
		$nwdolast	= $extdolast[2]."/".$extdolast[1]."/".$extdolast[0];
		
		$periode	= $nwdofirst." s.d ".$nwdolast;
		
		$data['tglopmulai']	= $nwdofirst;
		$data['tglopakhir']	= $nwdolast;
		$data['kproduksi']	= $iproduct!='kosong'?$iproduct:"";
		$data['sproduksi']	= $sproduct=='TRUE'?" checked ":"";
		$data['stop']		= $stop==1?" checked ":"";
		
		$data['isi']	= $this->mclass->clistopvsdo($iproduct,$tdofirst,$tdolast,$sproduct);
		
		$this->load->view('expoopvsdo/header_excelfiles',$data);	
	}
	
}
?>
