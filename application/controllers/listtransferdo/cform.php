<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		if($this->session->userdata('ses_user_id') && 
			$this->session->userdata('ses_user_name') && 
			$this->session->userdata('isession')!=0 ) 
		{	
			$data['page_title_listtrasferdo']	= $this->lang->line('page_title_listtrasferdo');
			$data['form_title_detail_listtrasferdo']	= $this->lang->line('form_title_detail_listtrasferdo');
			$data['list_kodeop_listtrasferdo']	= $this->lang->line('list_kodeop_listtrasferdo');
			$data['list_tglop_listtrasferdo']	= $this->lang->line('list_tglop_listtrasferdo');
			$data['list_kodedo_listtrasferdo']	= $this->lang->line('list_kodedo_listtrasferdo');
			$data['list_tgldo_listtrasferdo']	= $this->lang->line('list_tgldo_listtrasferdo');
			$data['list_area_listtrasferdo']	= $this->lang->line('list_area_listtrasferdo');
			$data['list_kodebrg_listtrasferdo']	= $this->lang->line('list_kodebrg_listtrasferdo');
			$data['list_nmbrg_listtrasferdo']	= $this->lang->line('list_nmbrg_listtrasferdo');
			$data['list_jml_listtrasferdo']	= $this->lang->line('list_jml_listtrasferdo');
			$data['button_transfer_ulang']	= $this->lang->line('button_transfer_ulang');
			$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
			$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
			$data['list_no_do']	= $this->lang->line('list_no_do');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');		
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$this->load->model('listtransferdo/mclass');
			$this->load->view('listtransferdo/vmainform',$data);
		}	
	}
	
	function carilisttransferdo() {			
		$data['page_title_listtrasferdo']	= $this->lang->line('page_title_listtrasferdo');
		$data['form_title_detail_listtrasferdo']	= $this->lang->line('form_title_detail_listtrasferdo');
		$data['list_kodeop_listtrasferdo']	= $this->lang->line('list_kodeop_listtrasferdo');
		$data['list_tglop_listtrasferdo']	= $this->lang->line('list_tglop_listtrasferdo');
		$data['list_kodedo_listtrasferdo']	= $this->lang->line('list_kodedo_listtrasferdo');
		$data['list_tgldo_listtrasferdo']	= $this->lang->line('list_tgldo_listtrasferdo');
		$data['list_area_listtrasferdo']	= $this->lang->line('list_area_listtrasferdo');
		$data['list_kodebrg_listtrasferdo']	= $this->lang->line('list_kodebrg_listtrasferdo');
		$data['list_nmbrg_listtrasferdo']	= $this->lang->line('list_nmbrg_listtrasferdo');
		$data['list_jml_listtrasferdo']	= $this->lang->line('list_jml_listtrasferdo');
		$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
		$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
		$data['button_transfer_ulang']	= $this->lang->line('button_transfer_ulang');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['ltransferdobrg']	= "";

		$d_do_first	= $this->input->post('d_do_first');
		$d_do_last	= $this->input->post('d_do_last');
		$kddo		= $this->input->post('nomor_do');
		
		if($d_do_first!='' || $d_do_last!=''){
			$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first)); // dd/mm/YYYY
			$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last)); // dd/mm/YYYY		
			$n_d_do_first	= (!empty($e_d_do_first[2]) && strlen($e_d_do_first[2])==4)?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:'';
			$n_d_do_last	= (!empty($e_d_do_last[2]) && strlen($e_d_do_last[2])==4)?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:'';
			
			$data['tgldomulai']	= $d_do_first;
			$data['tgldoakhir']	= $d_do_last;
			$data['nomordo']	= '';
		}else{
			$tawal	= $this->uri->segment(4);
			$takhir	= $this->uri->segment(5);
			$nomer	= $this->uri->segment(6);
			if(($tawal!='empty' || $takhir!='empty') && ($tawal!='' || $takhir!='')){
				$n_d_do_first	= $tawal;
				$n_d_do_last	= $takhir;
				$e_d_do_first	= explode("-",$n_d_do_first,strlen($n_d_do_first)); // YYYY-mm-dd
				$e_d_do_last	= explode("-",$n_d_do_last,strlen($n_d_do_last)); // YYYY-mm-dd
				$data['tgldomulai']	= $e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0];
				$data['tgldoakhir']	= $e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0];
				$data['nomordo']	= '';
			}else{
				$n_d_do_first	= '';
				$n_d_do_last	= '';			
				$data['tgldomulai']	= '';
				$data['tgldoakhir']	= '';
				$data['nomordo']	= $kddo;
			}
		}

		$turi1	= ($n_d_do_first!='' && $n_d_do_first!='empty')?$n_d_do_first:'empty';
		$turi2	= ($n_d_do_last!='' && $n_d_do_last!='empty')?$n_d_do_last:'empty';
		$turi3	= $kddo==''?'empty':$kddo;
					
		$this->load->model('listtransferdo/mclass');
		
		$flag	= 	$turi1=='empty'?'f':'t';
		$data['flag']	= $flag;
		
		if($flag=='t'){
			$query	= $this->mclass->clistdobrg_t($n_d_do_first,$n_d_do_last,$kddo);
			$jml	= $query->num_rows();		
			$pagination['base_url'] 	= '/listtransferdo/cform/carilisttransferdoperpages/'.$turi1.'/'.$turi2.'/'.$turi3.'/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 10;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(7,0);
			$this->pagination->initialize($pagination);
			$data['create_link']		= $this->pagination->create_links();		
			$data['isi_t']	= $this->mclass->viewperpages_t($pagination['per_page'],$pagination['cur_page'],$n_d_do_first,$n_d_do_last,$kddo);
		}elseif($flag=='f'){			
			$data['isi_f']	= $this->mclass->clistdobrg_f($n_d_do_first,$n_d_do_last,$kddo);
		}
				
		$this->load->view('listtransferdo/vlistform',$data);
	}

	function carilisttransferdoperpages() {			
		$data['page_title_listtrasferdo']	= $this->lang->line('page_title_listtrasferdo');
		$data['form_title_detail_listtrasferdo']	= $this->lang->line('form_title_detail_listtrasferdo');
		$data['list_kodeop_listtrasferdo']	= $this->lang->line('list_kodeop_listtrasferdo');
		$data['list_tglop_listtrasferdo']	= $this->lang->line('list_tglop_listtrasferdo');
		$data['list_kodedo_listtrasferdo']	= $this->lang->line('list_kodedo_listtrasferdo');
		$data['list_tgldo_listtrasferdo']	= $this->lang->line('list_tgldo_listtrasferdo');
		$data['list_area_listtrasferdo']	= $this->lang->line('list_area_listtrasferdo');
		$data['list_kodebrg_listtrasferdo']	= $this->lang->line('list_kodebrg_listtrasferdo');
		$data['list_nmbrg_listtrasferdo']	= $this->lang->line('list_nmbrg_listtrasferdo');
		$data['list_jml_listtrasferdo']	= $this->lang->line('list_jml_listtrasferdo');
		$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
		$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
		$data['button_transfer_ulang']	= $this->lang->line('button_transfer_ulang');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopbrg']	= "";
		
		$tawal	= $this->uri->segment(4);
		$takhir	= $this->uri->segment(5);
		$nomer	= $this->uri->segment(6);

		$e_d_do_first	= explode("-",$tawal,strlen($tawal)); // YYYY-mm-dd
		$e_d_do_last	= explode("-",$takhir,strlen($takhir)); // YYYY-mm-dd
		$data['tgldomulai']	= $e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0];
		$data['tgldoakhir']	= $e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0];
		$data['nomordo']	= '';
					
		$this->load->model('listtransferdo/mclass');
		
		$data['flag']	= 't';
		
		$query	= $this->mclass->clistdobrg_t($tawal,$takhir,"empty");
		$jml	= $query->num_rows();		

		$pagination['base_url'] 	= '/listtransferdo/cform/carilisttransferdoperpages/'.$tawal.'/'.$takhir.'/empty/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(7,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();		
		$data['isi_t']	= $this->mclass->viewperpages_t($pagination['per_page'],$pagination['cur_page'],$tawal,$takhir,"empty");
				
		$this->load->view('listtransferdo/vlistform',$data);
	}
		
	function listbarangjadi() {
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listtransferdo/mclass');

		$query	= $this->mclass->ltransferdo();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listtransferdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ltransferdoperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('listtransferdo/vlistformbrgjadi',$data);
	}

	function listbarangjadinext() {
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listtransferdo/mclass');

		$query	= $this->mclass->ltransferdo();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listtransferdo/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ltransferdoperpages($pagination['per_page'],$pagination['cur_page']);

		$this->load->view('listtransferdo/vlistformbrgjadi',$data);
	}

	function flistbarangjadi() {
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "DELIVERY ORDER (DO)";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listtransferdo/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->fltransferdo($key);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			
			$cc	= 1;
			
			foreach($query->result() as $row){
				
				$expddo = explode("-",$row->d_do,strlen($row->d_do)); // YYYY-mm-dd
				$ddo	= $expddo[2]."/".$expddo[1]."/".$expddo[0];
				
				$list .= "
				 <tr>
					<td>".$cc."</td>
					<td><a href=\"javascript:settextfield('$row->i_do_code')\">".$row->i_do_code."</a></td>
					<td><a href=\"javascript:settextfield('$row->i_do_code')\">".$ddo."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
	
	function transferulang(){

		$data['page_title_listtrasferdo']	= $this->lang->line('page_title_listtrasferdo');
		$data['form_title_detail_listtrasferdo']	= $this->lang->line('form_title_detail_listtrasferdo');
		$data['list_kodeop_listtrasferdo']	= $this->lang->line('list_kodeop_listtrasferdo');
		$data['list_tglop_listtrasferdo']	= $this->lang->line('list_tglop_listtrasferdo');
		$data['list_kodedo_listtrasferdo']	= $this->lang->line('list_kodedo_listtrasferdo');
		$data['list_tgldo_listtrasferdo']	= $this->lang->line('list_tgldo_listtrasferdo');
		$data['list_area_listtrasferdo']	= $this->lang->line('list_area_listtrasferdo');
		$data['list_kodebrg_listtrasferdo']	= $this->lang->line('list_kodebrg_listtrasferdo');
		$data['list_nmbrg_listtrasferdo']	= $this->lang->line('list_nmbrg_listtrasferdo');
		$data['list_jml_listtrasferdo']	= $this->lang->line('list_jml_listtrasferdo');
		$data['list_tgl_mulai_do']	= $this->lang->line('list_tgl_mulai_do');
		$data['list_tgl_akhir_do']	= $this->lang->line('list_tgl_akhir_do');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_detail']	= $this->lang->line('button_detail');		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
				
		$nomordo = $this->uri->segment(4);

		$qdate	= $this->db->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
				
		$qdo	 = $this->db->query(" SELECT a.i_do, b.i_code, a.d_do, a.i_customer FROM tm_do a 
		INNER JOIN tr_branch b ON b.i_branch_code=a.i_branch WHERE a.i_do_code='$nomordo' AND a.f_do_cancel='f' GROUP BY a.i_do, b.i_code, a.d_do, a.i_customer "); 
		if($qdo->num_rows()>0){
			$rdo = $qdo->row();
			
			$qdoitem = $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$rdo->i_do' ORDER BY i_do_item ASC ");
			
			if($qdoitem->num_rows()>0) {
				
				$result = $qdoitem->result();
				
				foreach($result as $row) {
					
					$qop	= $this->db->query(" SELECT * FROM tm_op WHERE i_op='$row->i_op' AND f_op_cancel='f' ");
					if($qop->num_rows()>0){
						$rop = $qop->row();
					}
					
					$qcek	= $this->db->query(" SELECT i_do_code, i_op_code, i_branch, i_product FROM tm_trans_do WHERE i_do_code='$nomordo' AND  i_op_code='$rop->i_op_code' AND i_branch='$rdo->i_code' AND i_product='$row->i_product' ");
					
					if($qcek->num_rows()==0){
						
						$enote	= str_replace("'"," ",$row->e_note);
						
						$insert	= $this->db->query(" INSERT INTO tm_trans_do(i_do_code,i_op_code,d_do,d_op,i_customer,i_branch,i_product,e_product_name,n_deliver,v_do_gross,e_note,d_entry) 
							VALUES('$nomordo','$rop->i_op_code','$rdo->d_do','$rop->d_op','$rdo->i_customer','$rdo->i_code','$row->i_product','$row->e_product_name','$row->n_deliver','$row->v_do_gross','$enote','$dentry') ");
						
						$this->db->query(" UPDATE tm_do SET f_transfer='t' WHERE i_do='$rdo->i_do' AND f_do_cancel='f' ");
						
					}else{
						$this->db->query(" UPDATE tm_trans_do SET n_deliver='$row->n_deliver', v_do_gross='$row->v_do_gross' WHERE i_do_code='$nomordo' AND i_op_code='$rop->i_op_code' AND i_branch='$rdo->i_code' AND i_product='$row->i_product' ");
						$this->db->query(" UPDATE tm_do SET f_transfer='t' WHERE i_do='$rdo->i_do' AND f_do_cancel='f' ");
					}
									
				}
			}
		}
		
		$this->load->model('listtransferdo/mclass');
		$this->load->view('listtransferdo/vmainform',$data);					
	}
	
}
?>
