<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmmasuk/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================

	$id_gudang = $this->input->post('gudang', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	//echo $proses_submit; die();
	if ($proses_submit == "Proses") {
		$data['bonm_detail'] = $this->mmaster->get_detail_bonm($id_gudang);
		$data['go_proses'] = '1';
		$data['msg'] = '';
		$data['isi'] = 'bonmmasuk/vmainform';
		$this->load->view('template',$data);
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'bonmmasuk/vmainform';
		$this->load->view('template',$data);
	}
  }
  
  function edit(){
	$id_apply_stok 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_bonm($id_apply_stok);
	$data['msg'] = '';
	$data['id_apply_stok'] = $id_apply_stok;

	$data['isi'] = 'bonmmasuk/veditform';
	$this->load->view('template',$data);

  }
    
  function show_popup_sj(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	// sj pembelian
	
	$id_gudang 	= $this->uri->segment(4);
	$csupplier 	= $this->uri->segment(5);
	if ($csupplier == '')
		$csupplier 	= $this->input->post('csupplier', TRUE);  
	if ($id_gudang == '')
		$id_gudang 	= $this->input->post('id_gudang', TRUE);  
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jum_total = $this->mmaster->get_sjtanpalimit($csupplier, $id_gudang, $keywordcari);
							$config['base_url'] = base_url()."index.php/bonmmasuk/cform/show_popup_sj/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_sj($config['per_page'],$this->uri->segment(5), $csupplier, $id_gudang, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$data['csupplier'] = $csupplier;
	$data['id_gudang'] = $id_gudang;

	$this->load->view('bonmmasuk/vpopupsj',$data);

  }

  function submit(){			
	  $tgl = date("Y-m-d");
			$list_detail 	= $this->input->post('list_detail', TRUE);  //echo $list_detail; die();
			
			$id_detail = explode(",", $list_detail);
			foreach($id_detail as $row1) {
				if ($row1 != '') {
					// update statusnya!
					$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET status_stok = 't' WHERE id = '$row1' ");
					
					// ambil id headernya
					$query3	= $this->db->query(" SELECT id_apply_stok FROM tm_apply_stok_pembelian_detail 
							WHERE id = '$row1' ");
					$hasilrow = $query3->row();
					$id_apply_stok	= $hasilrow->id_apply_stok;
					
					// ambil field2 detailnya
					$query3	= $this->db->query(" SELECT kode_brg, qty, harga FROM tm_apply_stok_pembelian_detail
							WHERE id = '$row1' ");
					$hasilrow = $query3->row();
					$kode_brg	= $hasilrow->kode_brg;
					$qty	= $hasilrow->qty;
					$harga	= $hasilrow->harga;
					
					//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
					$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE status_stok = 'f' 
										AND id_apply_stok = '$id_apply_stok' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" UPDATE tm_apply_stok_pembelian SET status_stok = 't' WHERE id= '$id_apply_stok' ");
					}
					
					//$this->db->query(" UPDATE tm_apply_stok_pembelian SET status_stok = 't' WHERE id = '$id_apply_stok' ");
					
					// ambil kode_supplier dan no_sj
					$query3	= $this->db->query(" SELECT no_sj, kode_supplier, no_bonm FROM tm_apply_stok_pembelian 
							WHERE id = '$id_apply_stok' ");
					$hasilrow = $query3->row();
					$no_sj	= $hasilrow->no_sj;
					$kode_supplier	= $hasilrow->kode_supplier;
					$no_bonm	= $hasilrow->no_bonm;
					
					// ambil id pembelian
					$query3	= $this->db->query(" SELECT id FROM tm_pembelian 
							WHERE no_sj = '$no_sj' AND kode_supplier = '$kode_supplier' ");
					$hasilrow = $query3->row();
					$id_pembelian	= $hasilrow->id;
					
					// update lagi di tm_pembelian_detail
					$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 't' WHERE id_pembelian = '$id_pembelian' 
								AND kode_brg = '$kode_brg' ");
								
					//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
					$query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE status_stok = 'f' 
										AND id_pembelian = '$id_pembelian' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" UPDATE tm_pembelian SET status_stok = 't' WHERE id = '$id_pembelian' ");
					}
					
					//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg,
							'stok'=>$new_stok,
						//	'id_gudang'=>$id_gudang, //
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg' ");
					}
					
					// 14 juni 2011, ada tabel baru, yaitu tm_stok_harga (utk membedakan stok berdasarkan harga)
							//cek stok terakhir tm_stok_harga, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
														AND harga = '$harga' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty; // utk save ke tt_stok, $new_stok ini yg dipake karena per harga
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_harga, insert
								$data_stok = array(
									'kode_brg'=>$kode_brg,
									'stok'=>$new_stok,
									'harga'=>$harga,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_harga',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode_brg' AND harga = '$harga' ");
							}
					// #########################################################################################
					
					// ambil field stok_lain
					$query3	= $this->db->query(" SELECT stok_lain FROM tm_apply_stok_pembelian 
							WHERE id = '$id_apply_stok' ");
					$hasilrow = $query3->row();
					$stok_lain	= $hasilrow->stok_lain;
					
					if ($stok_lain == 'f')
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
										VALUES ('$kode_brg','$no_bonm', '$qty', '$new_stok', '$tgl', '$harga' ) ");
					else
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk_lain, saldo, tgl_input, harga) 
										VALUES ('$kode_brg','$no_bonm', '$qty', '$new_stok', '$tgl', '$harga' ) ");
				}
			} // end foreach row1
			
			redirect('bonmmasuk/cform/view');
			//=================================================================== END =============================================
		/*	
			// jika edit, var ini ada isinya
			$id_bonm 	= $this->input->post('id_apply_stok', TRUE);
			
			if ($id_bonm == '') {
				$cek_data = $this->mmaster->cek_data($no_bonm, $gudang);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'bonmmasuk/vmainform';
					$data['msg'] = "Data no Bon M ".$no_bonm." sudah ada..!";
					$data['id_sj'] = '';
					$data['go_proses'] = '';
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						if ($this->input->post('cek_'.$i, TRUE) == 'y') {
							$this->mmaster->save($no_bonm, $tgl_bonm, $id_sj, $no_sj, $kode_supplier, $gudang,
									$this->input->post('id_detail_'.$i, TRUE),
									$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
									$this->input->post('qty_'.$i, TRUE), $this->input->post('harga_'.$i, TRUE) );
						}
					}
					redirect('bonmmasuk/cform/view');
				}
			} // end if id_bonm == ''
			else { // update
				$tgl = date("Y-m-d");
				$this->db->query(" UPDATE tm_apply_stok_pembelian SET tgl_bonm = '$tgl_bonm', tgl_update = '$tgl'
								where id= '$id_bonm' "); //
				$this->db->query(" DELETE FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$id_bonm' ");
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						// jika cek = 'y' dan cek != cek_lama, update status_stok, dan update stoknya
						if (($this->input->post('cek_'.$i, TRUE) == 'y') && ($this->input->post('cek_lama_'.$i, TRUE) != $this->input->post('cek_'.$i, TRUE)) ) {
							$data_detail = array(
								'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
								'qty'=>$this->input->post('qty_'.$i, TRUE),
								'id_apply_stok'=>$id_bonm
							);
							$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
						 
						//insert data brg masuk ke tabel history stok, dan tambah stok di tabel tm_stok
							
							//cek stok terakhir tm_stok, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok 
							WHERE kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$this->input->post('kode_'.$i, TRUE),
									'stok'=>$new_stok,
								//	'id_gudang'=>$id_gudang, //
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."' ");
							}
							
							$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
							VALUES ('".$this->input->post('kode_'.$i, TRUE)."','$no_bonm', '".$this->input->post('qty_'.$i, TRUE)."', 
							'$new_stok', '$tgl', '".$this->input->post('harga_'.$i, TRUE)."' ) ");
							
							// update status_stok
							$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 't' 
							WHERE id= '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							
							// cek lagi status_stok di tabel detail, jika sudah 't' semua, maka di tabel tm_pembelian ubah juga jadi 't'
							$query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE status_stok = 'f' 
													AND id_pembelian = '$id_sj' ");
							if ($query3->num_rows() == 0){
								$this->db->query(" UPDATE tm_pembelian SET status_stok = 't' WHERE id= '$id_sj' ");
							}
						} // end if cek = 'y'
						
						// ======================
						// jika cek = 'y' dan cek != cek_lama, update status_stok, dan update stoknya
						if (($this->input->post('cek_'.$i, TRUE) == 't') && ($this->input->post('cek_lama_'.$i, TRUE) != $this->input->post('cek_'.$i, TRUE)) ) {
							// update status_stok = f
							$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 'f' 
							WHERE id= '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							
							
						// stoknya edit lagi
						// edit transaksi stoknya utk bon M
							//1. cek stok brg berdasarkan no_bukti di tt_stok
							$query3	= $this->db->query(" SELECT id, masuk FROM tt_stok WHERE no_bukti = '$no_bonm' 
											AND kode_brg='".$this->input->post('kode_'.$i, TRUE)."' ORDER BY id DESC LIMIT 1 ");
							$hasilrow = $query3->row();
							$masuk	= $hasilrow->masuk;
							
							//2. ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok 
											WHERE kode_brg='".$this->input->post('kode_'.$i, TRUE)."' ");
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$qty_baru = $this->input->post('qty_'.$i, TRUE);
							$stokreset = $stok_lama-$masuk;
							//$stokskrg = ($stok_lama-$masuk) + $qty_baru;
							
							//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
							$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga)
							VALUES ('".$this->input->post('kode_'.$i, TRUE)."', '$no_bonm', '$masuk', '$stokreset', '$tgl',
							'".$this->input->post('harga_'.$i, TRUE)."')  ");
							
								//5. update stok di tm_stok
							$this->db->query(" UPDATE tm_stok SET stok = '$stokreset', tgl_update_stok = '$tgl'
												where kode_brg= '".$this->input->post('kode_'.$i, TRUE)."'  ");
						}
						
						//=======================

					}
					redirect('bonmmasuk/cform/view');
			} */

  }
  
  function view(){
    $data['isi'] = 'bonmmasuk/vformview';
    $keywordcari = "all";
    $csupplier = '0';
	//$kode_bagian = '';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);				
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/bonmmasuk/cform/cari/'.$csupplier.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'bonmmasuk/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }


/*  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('retur-beli/cform/view');
  } */
  
}
