<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('keu-alokasi/mmaster');
  }

function index(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_area'] = $this->mmaster->getArea();
	$data['isi'] = 'keu-alokasi/vmainform';
	$this->load->view('template',$data);
	}
	
	
function view(){
	
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$id_area=$this->input->post('id_area');
	$date_from=$this->input->post('date_from');
	$date_to=$this->input->post('date_to');
	
	$query=$this->mmaster->getAll($id_area,$date_from,$date_to);
	$data['jum_data'] =count($query);
	
	$data['date_from']= $date_from;
	$data['date_to']= $date_to;
	$data['query']= $query;
	$data['isi'] = 'keu-alokasi/vformview';
	$this->load->view('template',$data);
	}
	
function viewalo(){
	
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$id_area=$this->input->post('id_area');
	$id_bank=$this->input->post('id_bank');
	$date_from=$this->input->post('date_from');
	$date_to=$this->input->post('date_to');
	$cari=$this->input->post('cari');
	
	$list_area=$this->mmaster->getArea();
	$list_bank=$this->mmaster->getBank();
	
	
	$query=$this->mmaster->getAllAlo($id_area,$date_from,$date_to);
	$jum_total=count($query);
	$data['jum_total'] =$jum_total;
	$config['cur_page']=$this->uri->segment(4);
	$config['base_url']= base_url().'index.php/keu-alokasi/cform/viewalo';
	$config['total_rows']=$jum_total;
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$this->pagination->initialize($config);
	$queryl=$this->mmaster->getAllAlolimit($id_area,$date_from,$date_to);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['list_area']= $list_area;
	$data['list_bank']= $list_bank;
	$data['date_from']= $date_from;
	$data['date_to']= $date_to;
	$data['id_area']= $id_area;
	$data['id_bank']= $id_bank;
	$data['cari']= $cari;
	$data['query']= $queryl;
	$data['isi'] = 'keu-alokasi/vformviewalo';
	$this->load->view('template',$data);
	}	
	
	function approve(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	
	$id=$this->uri->segment(4);
	$i_bank=$this->uri->segment(5);
	$i_area=$this->uri->segment(6);
	$date_from=$this->uri->segment(7);
	$date_to=$this->uri->segment(8);
	
	$query2=$this->mmaster->getAllitem_old($i_area,$date_from,$date_to,$id);
	
	
	
	$data['query']=$query2;
	$data['list_area'] = $this->mmaster->getArea();
	
	$data['i_bank']=$i_bank;
	$data['i_area']=$i_area;
	$data['date_from']=$date_from;
	$data['date_to']=$date_to;
	$data['id']=$id;
	
	
	$data['msg']='';

	$data['isi'] = 'keu-alokasi/vmainformapprove';
	$this->load->view('template',$data);
	}
	
	function edit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	
	$id=$this->uri->segment(8);
	$query2=$this->mmaster->getAllitem($id);
	
	$data['query']=$query2;
	$data['list_area'] = $this->mmaster->getArea();
	
	$data['id']=$id;
	$data['msg']='';
	$data['isi'] = 'keu-alokasi/veditformapprove';
	$this->load->view('template',$data);
	}
	
	
	function show_popup_debitur(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	
	$query=$this->mmaster->getdebittur();
	
	$config['base_url'] = base_url()."index.php/keu-alokasi/cform/show_popup_debitur/";
	$config['total_rows']=count($query);
	$config['per_page']=20;
	$config['first_link']='First';
	$config['last_link']='Last';
	$config['next_link']='Next';
	$config['prev_link']='Prev';
	$config['cur_page']=$this->uri->segment(4);
	$this->pagination->initialize($config);
	$data['query']=$this->mmaster->getdebitturlimit($config['per_page'],$config['cur_page']);
		
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['startnya']=$config['cur_page'];
	$data['jumdata']=count($query);
	$data['msg']='';
	$isi = 'keu-alokasi/vpopupapprove';
	$this->load->view($isi,$data);
	}
	
	function show_popup_nota(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$keywordcari 	= $this->input->post('cari', TRUE);  
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$posisi=$this->uri->segment(4);
	$date_from=$this->uri->segment(5);
	$query=$this->mmaster->getnota($date_from);
	
	$config['base_url'] = base_url()."index.php/keu-alokasi/cform/show_popup_nota/".$posisi."/".$date_from;
	$config['total_rows']=count($query);
	$config['per_page']=20;
	$config['first_link']='First';
	$config['last_link']='Last';
	$config['next_link']='Next';
	$config['prev_link']='Prev';
	$config['cur_page']=$this->uri->segment(6);
	$this->pagination->initialize($config);
	$data['query']=$this->mmaster->getnotalimit($config['per_page'],$config['cur_page'],$date_from);
		
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['posisi']=$posisi;
	$data['jumdata']=count($query);
	$data['msg']='';
	$isi = 'keu-alokasi/vpopupnota';
	$this->load->view($isi,$data);
	}	
	
	function updatealok(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$no_bank = $this->input->post('no_bank',TRUE);
	$jml = $this->input->post('no',TRUE);
	$tgl_bk = $this->input->post('tgl_bk',TRUE);	
	$id_area = $this->input->post('id_area',TRUE);
	$tgl_alo = $this->input->post('tgl_alo',TRUE);
	$alamat	= $this->input->post('alamat',TRUE);
	
	$bank = $this->input->post('bank',TRUE);
	$id_coa_bank = $this->input->post('id_coa_bank',TRUE);
	$keterangan = $this->input->post('keterangan',TRUE);	
	$vjumlahx = $this->input->post('jumlah',TRUE);
	$vlebihx     = $this->input->post('vlebih',TRUE);
    $vlebihx     = $this->input->post('vlebih',TRUE);
	$debitur = $this->input->post('debitur',TRUE);	
	$id_debitur = $this->input->post('id_debitur',TRUE);	
	
	$alamat = $this->input->post('alamat',TRUE);	
	$ibank = $this->input->post('i_bank',TRUE);	
	$i_kbank = $this->input->post('i_kbank',TRUE);	
	$i_area = $this->input->post('i_area',TRUE);
	$date_from = $this->input->post('date_from',TRUE);	
	$date_to = $this->input->post('date_to',TRUE);	
	
	$ialokasi= $this->mmaster->cekheader( $i_kbank,$i_area);
                                    
		$asal=0;
		$pengurang=$vjumlahx-$vlebihx;

        $fupdatebank=$this->mmaster->updatebank($i_kbank,$id_coa_bank,$i_area,$pengurang);
          
          for($i=1;$i<=$jml-1;$i++){
            $inota              = $this->input->post('id_nota_'.$i, TRUE);
            $kdnota              = $this->input->post('nota_'.$i, TRUE);
            $kategory              = $this->input->post('kategory_'.$i, TRUE);
            $dnota              = $this->input->post('tgl_nota_'.$i, TRUE);
          
            $vjumlah= $this->input->post('nilai_'.$i, TRUE);
            $vbayar  = $this->input->post('bayar_'.$i, TRUE);
            $vsisa  = $this->input->post('sisa_'.$i, TRUE);
            $vlebih  = $this->input->post('lebih_'.$i, TRUE);
            $vjumlah= str_replace(',','',$vjumlah);
            $vbayar = str_replace(',','',$vbayar);
            $vsisa = str_replace(',','',$vsisa);
            $kdnota =trim($kdnota);
    
            $eremark= $this->input->post('eremark'.$i,TRUE);
            $this->mmaster->updatedetail($ialokasi,$ibank,$i_area,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$id_coa_bank,$kdnota,$kategory,$vbayar,$i_kbank);
			$fupdatenota=$this->mmaster->updatenota($inota, $kdnota,$kategory,$dnota,$vsisa);
            if($fupdatenota==false) break;
          }
	redirect('keu-alokasi/cform/viewalo');
	}	
	function submitalok(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$no_bank = $this->input->post('no_bank',TRUE);
	$jml = $this->input->post('no',TRUE);
	$tgl_bk = $this->input->post('tgl_bk',TRUE);	
	$id_area = $this->input->post('id_area',TRUE);
	$tgl_alo = $this->input->post('tgl_alo',TRUE);
	$alamat	= $this->input->post('alamat',TRUE);
	
	$bank = $this->input->post('bank',TRUE);
	$id_coa_bank = $this->input->post('id_coa_bank',TRUE);
	$keterangan = $this->input->post('keterangan',TRUE);	
	$vjumlahx = $this->input->post('jumlah',TRUE);
	
    $vlebihx     = $this->input->post('vlebih',TRUE);
	$debitur = $this->input->post('debitur',TRUE);	
	$id_debitur = $this->input->post('id_debitur',TRUE);	
	
	$alamat = $this->input->post('alamat',TRUE);	
	$ibank = $this->input->post('i_bank',TRUE);	
	$i_kbank = $this->input->post('i_kbank',TRUE);	
	$i_area = $this->input->post('i_area',TRUE);
	$date_from = $this->input->post('date_from',TRUE);	
	$date_to = $this->input->post('date_to',TRUE);	
	
	$ialokasi= $this->mmaster->insertheader( $i_kbank,$i_area,$id_debitur,$tgl_bk,$tgl_alo,$bank,
                                          $vjumlahx,$vlebihx,$id_coa_bank,$debitur,$alamat,$no_bank);
                                          
		$asal=0;
	

        $fupdatebank=$this->mmaster->updatebank($i_kbank,$id_coa_bank,$i_area,$vlebihx);
          
          for($i=1;$i<=$jml-1;$i++){
            $inota              = $this->input->post('id_nota_'.$i, TRUE);
            $kdnota              = $this->input->post('nota_'.$i, TRUE);
            $kategory              = $this->input->post('kategory_'.$i, TRUE);
            $dnota              = $this->input->post('tgl_nota_'.$i, TRUE);
          
            $vjumlah= $this->input->post('nilai_'.$i, TRUE);
            $vbayar  = $this->input->post('bayar_'.$i, TRUE);
            $vsisa  = $this->input->post('sisa_'.$i, TRUE);
            $vlebih  = $this->input->post('lebih_'.$i, TRUE);
            $vjumlah= str_replace(',','',$vjumlah);
            $vbayar = str_replace(',','',$vbayar);
            $vsisa = str_replace(',','',$vsisa);
            $kdnota =trim($kdnota);
    
            $eremark= $this->input->post('eremark'.$i,TRUE);
            $this->mmaster->insertdetail($ialokasi,$ibank,$i_area,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$id_coa_bank,$kdnota,$kategory,$vbayar,$i_kbank);
			$fupdatenota=$this->mmaster->updatenota($inota, $kdnota,$kategory,$dnota,$vsisa);
            if($fupdatenota==false) break;
          }
	redirect('keu-alokasi/cform/viewalo');
	}	
	
	function delete(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	
	$cur_page = $this->uri->segment(4);	
	$i_alok = $this->uri->segment(5);	
	$date_from = $this->uri->segment(6);	
	$date_to = $this->uri->segment(7);	
	$i_kbank = $this->uri->segment(8);	
	
	$ialokasi= $this->mmaster->delheadalo($i_alok,$i_kbank);
                                    
		//~ $asal=0;
		//~ $pengurang=$vjumlahx-$vlebihx;
//~ 
        //~ $fupdatebank=$this->mmaster->updatebank($i_kbank,$id_coa_bank,$i_area,$pengurang);
          //~ 
          //~ for($i=1;$i<=$jml-1;$i++){
            //~ $inota              = $this->input->post('id_nota_'.$i, TRUE);
            //~ $kdnota              = $this->input->post('nota_'.$i, TRUE);
            //~ $kategory              = $this->input->post('kategory_'.$i, TRUE);
            //~ $dnota              = $this->input->post('tgl_nota_'.$i, TRUE);
          //~ 
            //~ $vjumlah= $this->input->post('nilai_'.$i, TRUE);
            //~ $vbayar  = $this->input->post('bayar_'.$i, TRUE);
            //~ $vsisa  = $this->input->post('sisa_'.$i, TRUE);
            //~ $vlebih  = $this->input->post('lebih_'.$i, TRUE);
            //~ $vjumlah= str_replace(',','',$vjumlah);
            //~ $vbayar = str_replace(',','',$vbayar);
            //~ $vsisa = str_replace(',','',$vsisa);
            //~ $kdnota =trim($kdnota);
    //~ 
            //~ $eremark= $this->input->post('eremark'.$i,TRUE);
            //~ $this->mmaster->updatedetail($ialokasi,$ibank,$i_area,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$id_coa_bank,$kdnota,$kategory,$vbayar,$i_kbank);
			//~ $fupdatenota=$this->mmaster->updatenota($inota, $kdnota,$kategory,$dnota,$vsisa);
            //~ if($fupdatenota==false) break;
          //~ }
	 redirect('keu-alokasi/cform/viewalo');
	}	
}
