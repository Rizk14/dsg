<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-bb-sup/mmaster');
  }
  
  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$data['msg'] = '';
	$data['go_proses'] = '';
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['supplier'] = $this->mmaster->get_supplier();
    $data['isi'] = 'mst-bb-sup/vmainform';
    
	$this->load->view('template',$data);

  }
  
  function edit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $idharga 	= $this->uri->segment(4);  
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $esupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $go_edit 	= $this->input->post('go_edit', TRUE);
    
    if ($idharga == '')
		$idharga 	= $this->input->post('idharga', TRUE);
    
    if ($go_edit == '') {
		$data['cur_page'] = $cur_page;
		$data['is_cari'] = $is_cari;
		$data['esupplier'] = $esupplier;
		$data['carinya'] = $carinya;
		
		$data['query'] = $this->mmaster->get_harga($idharga);
		$data['isi'] = 'mst-bb-sup/veditform';
		$data['idharga'] = $idharga;
		$this->load->view('template',$data);
	}
	else { // update data harga
		$harga 	= $this->input->post('harga', TRUE);
		$harga_lama 	= $this->input->post('harga_lama', TRUE);
		$id_brg 	= $this->input->post('id_brg', TRUE);
		$id_supplier 	= $this->input->post('id_supplier', TRUE);
		$id_satuan 	= $this->input->post('id_satuan', TRUE);
		
		$cur_page = $this->input->post('cur_page', TRUE);
		$is_cari = $this->input->post('is_cari', TRUE);
		$esupplier = $this->input->post('esupplier', TRUE);
		$carinya = $this->input->post('carinya', TRUE);
		
		$tgl = date("Y-m-d H:i:s");
		
		if ($harga != $harga_lama) {
			$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '$harga', tgl_update = '$tgl' WHERE id = '$idharga' ");    
			
		//	$cek_data_harga=$this->mmaster->cek_data_harga($id_supplier ,$id_brg,$harga);
			
			$this->db->query(" INSERT INTO tt_harga (id_brg, id_supplier, harga, tgl_input) 
											VALUES ('".$id_brg."','$id_supplier', '$harga', '$tgl') ");	
		}
		
		if ($carinya == '') $carinya = "all";
		if ($is_cari == 0)
			$url_redirectnya = "mst-bb-sup/cform/view/index/".$cur_page;
		else
			$url_redirectnya = "mst-bb-sup/cform/cari/index/".$esupplier."/".$carinya."/".$cur_page;
		
		redirect($url_redirectnya);
		//redirect('mst-bb-sup/cform/view');
	}
  }
  
  function proseslistbrg() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$supplier 	= $this->input->post('supplier', TRUE);
		$kel_brg 	= $this->input->post('kel_brg', TRUE);
		$kode_jenis 	= $this->input->post('kode_jenis', TRUE);
		$id_jenis 	= $this->input->post('id_jenis', TRUE);
		
		$data['list_brg'] = $this->mmaster->get_barang($kel_brg, $id_jenis, $supplier);
		$data['msg'] = '';	
		$data['jum'] = count($data['list_brg']);	
		//nm supplier
		$query	= $this->db->query(" SELECT pkp,kode_supplier, nama FROM tm_supplier WHERE id = '$supplier' ");    
		$hasilrow = $query->row();
		$nama_sup	= $hasilrow->kode_supplier." - ".$hasilrow->nama;
		$pkp = $hasilrow->pkp;
		//nm kel
		$query	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");    
		$hasilrow = $query->row();
		$nama_kel	= $hasilrow->nama;
		
		//nm jenis brg
		if ($id_jenis != '') {
			$query	= $this->db->query(" SELECT kode, nama FROM tm_jenis_barang WHERE id = '$id_jenis' ");    
			$hasilrow = $query->row();
			$nama_jenis	= $hasilrow->kode." - ".$hasilrow->nama;
		}
		else
			$nama_jenis = '';
		
		/*if ($kode_jenis == '')
			$nama_jenis = '';
		else
			$nama_jenis = $kode_jenis." - ".$nama_jenis; */
				
		$data['isi'] = 'mst-bb-sup/vmainform';
		$data['go_proses'] = '1';
		
		$data['id_supplier'] = $supplier;
		$data['pkp']= $pkp;
		$data['nama_supplier'] = $nama_sup;
		$data['kel_brg'] = $kel_brg;
		$data['nama_kel'] = $nama_kel;
		$data['nama_jenis'] = $nama_jenis;
		
		$this->load->view('template',$data);  
  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$jum = $this->input->post('jum', TRUE);
		$id_supplier = $this->input->post('id_supplier', TRUE);  
		$pkp = $this->input->post('pkp', TRUE);  
		 $tgl = date("Y-m-d H:i:s");
		for ($i=1;$i<=$jum;$i++)
		{
			if ($this->input->post('cek_'.$i, TRUE) == 'y') {
				// cek apakah datanya udh ada
				$cek_data = $this->mmaster->cek_data($id_supplier, $this->input->post('id_brg_'.$i, TRUE), $this->input->post('id_satuan_'.$i, TRUE));
				if (count($cek_data) == 0) { 
					$this->mmaster->save($id_supplier,$pkp, $this->input->post('id_brg_'.$i, TRUE), $this->input->post('id_satuan_'.$i, TRUE),
								$this->input->post('harga_'.$i, TRUE),$this->input->post('id_satuan_konversi_'.$i, TRUE) );
				}
				else {
					if ($this->input->post('harga_'.$i, TRUE) != '' || $this->input->post('harga_'.$i, TRUE) != 0)
						$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '".$this->input->post('harga_'.$i, TRUE)."'
								WHERE id_supplier = '$id_supplier' AND id_brg = '".$this->input->post('id_brg_'.$i, TRUE)."'
								AND id_satuan = '".$this->input->post('id_satuan_'.$i, TRUE)."' ");


/*
						$this->db->select("id from tm_stok_harga WHERE id_brg = '".$this->input->post('id_brg_'.$i, TRUE)."' 
						AND id_satuan = '".$this->input->post('id_satuan_'.$i, TRUE)."' AND is_harga_pkp = '$pkp'
						 AND harga = '".$this->input->post('harga_'.$i, TRUE)."'", false);
  
  
		$query = $this->db->get();
		if ($query->num_rows() == 0){
		
		$data_stok_harga = array(
		  'id_brg'=>$this->input->post('id_brg_'.$i, TRUE),
		  'id_supplier'=>$id_supplier,
		  'id_satuan'=>$this->input->post('id_satuan_'.$i, TRUE),
		  'id_satuan_konversi'=>$this->input->post('id_satuan_konversi'.$i, TRUE),
		  'harga'=>$this->input->post('harga'.$i, TRUE),
		 'tgl_input'=>$tgl,
		 'stok'=>0,
		  'tgl_update_stok'=>$tgl,
		  'is_harga_pkp'=>$pkp
		);
	
		$this->db->insert('tm_stok_harga',$data_stok_harga); 
	}
						
						$this->db->query(" UPDATE tm_stok_harga SET id_supplier= '".$id_supplier."' ,id_satuan_konversi= '".$this->input->post('id_satuan_konversi_'.$i, TRUE)."',
						tgl_update_stok='$tgl'
								WHERE  id_brg = '".$this->input->post('id_brg_'.$i, TRUE)."' AND harga = '".$this->input->post('harga_'.$i, TRUE)."'
								AND id_satuan = '".$this->input->post('id_satuan_'.$i, TRUE)."' AND is_harga_pkp = '".$pkp."'  ");		
			*/			
		
				}
			}
		}
		redirect('mst-bb-sup/cform');
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'mst-bb-sup/vformview';
    $keywordcari = "all";
    $supplier = '0';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $supplier);	
							$config['base_url'] = base_url().'index.php/mst-bb-sup/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $supplier);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['kel_brg'] = $this->mmaster->get_kel_brg();
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['esupplier'] = $supplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$supplier 	= $this->input->post('supplier', TRUE);  
	//echo $supplier; die();
	if ($keywordcari == '' && $supplier == '') {
		$supplier 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($supplier == '')
		$supplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $supplier);
							$config['base_url'] = base_url().'index.php/mst-bb-sup/cform/cari/index/'.$supplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $supplier);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'mst-bb-sup/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['esupplier'] = $supplier;
	$this->load->view('template',$data);
  }
  
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg 	= $this->uri->segment(4);
	if ($kel_brg == '')
		$kel_brg = $this->input->post('kel_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $kel_brg == '') {
		$kel_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barangtanpalimit($kel_brg, $keywordcari);
					$config['base_url'] = base_url()."index.php/mst-bb-sup/cform/show_popup_jenis/".$kel_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang($config['per_page'],$this->uri->segment(6), $kel_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['kel_brg'] = $kel_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('mst-bb-sup/vpopupjenis',$data);

  }
  
  function show_popup_jenis_bhn(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jns_brg 	= $this->uri->segment(4);
	if ($jns_brg == '')
		$jns_brg = $this->input->post('jns_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $jns_brg == '') {
		$jns_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_bahantanpalimit($jns_brg, $keywordcari);
							$config['base_url'] = base_url()."index.php/mst-bb-sup/cform/show_popup_jenis_bhn/".$jns_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_bahan($config['per_page'],$this->uri->segment(6), $jns_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['jns_brg'] = $jns_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		$hasilrow = $query3->row();
		$nama_jns_brg	= $hasilrow->nama;
	$data['nama_jns_brg'] = $nama_jns_brg;

	$this->load->view('mst-bb-sup/vpopupjenisbhn',$data);

  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
    $is_cari 	= $this->uri->segment(6);
    $esupplier 	= $this->uri->segment(7);
    $carinya 	= $this->uri->segment(8);
    
    $this->mmaster->delete($id);
    
    if ($carinya == '') $carinya = "all";
    if ($is_cari == 0)
		$url_redirectnya = "mst-bb-sup/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "mst-bb-sup/cform/cari/index/".$esupplier."/".$carinya."/".$cur_page;
	
	redirect($url_redirectnya);
    //redirect('mst-bb-sup/cform/view');
  }
  // extreme 21-05-2012
  function print_harga(){	  
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['isi'] = 'mst-bb-sup/vprintform';
	$data['msg'] = '';
	$this->load->view('template',$data);
  }
  
  function do_print_harga() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  $supplier 	= $this->uri->segment(4);
	
	  $data['query'] = $this->mmaster->get_harga_for_print($supplier); 
	  
	  // ambil data nama supplier
	  if ($supplier != '0') {
		$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$supplier' ");
		$hasilrow = $query3->row();
		$kode_supplier	= $hasilrow->kode_supplier;
		$nama_supplier	= $hasilrow->nama;
	  }
	  else {
		$kode_supplier = '';
		$nama_supplier = "All";
	  }
	  
	  $data['supplier'] = $supplier;
	  $data['kode_supplier'] = $kode_supplier;
	  $data['nama_supplier'] = $nama_supplier;
	  $this->load->view('mst-bb-sup/vprintharga',$data);
  }
  
  function do_exportexcel_harga() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	  }
	  $supplier 	= $this->uri->segment(4);
	  $query= $this->mmaster->get_harga_for_print($supplier); 
	  
	  // ambil data nama supplier
	  if ($supplier != '0') {
		$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$supplier' ");
		$hasilrow = $query3->row();
		$kode_supplier	= $hasilrow->kode_supplier;
		$nama_supplier	= $hasilrow->nama;
	  }
	  else {
		$kode_supplier = '';
		$nama_supplier = "All";
	  }
	  
	  $html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='6' align='center'>DATA HARGA PEMBELIAN BAHAN BAKU/PEMBANTU</th>
				  </tr>
				<tr>	
					<th colspan='6' align='center'>Supplier: ";
		
		if ($supplier != '0')
			$html_data.= "$kode_supplier - $nama_supplier";
		else
			$html_data.= "All";
		
		$html_data.="</th>
				 </tr>
				</table><br>";
				
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th>Supplier</th>
					 <th>Kelompok Brg</th>
					 <th>Nama Barang</th>
					 <th>Satuan</th>
					 <th>Harga (Rp.)</th>
					 <th>Harga Sebelum PPn (Rp.)</th>
				 </tr>
				</thead>
				<tbody>";
		
		if (is_array($query)) {
			$sup_temp = "";
			for($j=0;$j<count($query);$j++){
				$html_data.="<tr><td>";
				if ($sup_temp != $query[$j]['kode_supplier']) {
					$sup_temp = $query[$j]['kode_supplier'];
					$html_data.= $query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']; 
				}
				else
					$html_data.="&nbsp;";
				$html_data.="</td>
				<td>".$query[$j]['nama_kel_brg']."</td>
				<td>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>
				<td>".$query[$j]['satuan']."</td>
				<td align='right'>".$query[$j]['harga']."</td>
				<td>";
				if ($query[$j]['pkp'] == 't')
					$html_data.= $query[$j]['harga']/1.1;
				else
					$html_data.= "0";
				$html_data.="</td>
				</tr> ";
			}
		}
		$html_data.= "</tbody></table>";
		
		$nama_file = "data_harga_brg_supplier";
		$export_excel1 = '1';
		//if ($export_excel1 != '')
			$nama_file.= ".xls";
		//else
		//	$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
}
