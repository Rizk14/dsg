<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
			$data['page_title_adduser']	= $this->lang->line('page_title_adduser');
			$data['form_tbl_user_id']	= $this->lang->line('form_tbl_user_id');
			$data['form_tbl_user_name']	= $this->lang->line('form_tbl_user_name');
			$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
			$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
			$data['link_aksi']	= $this->lang->line('link_aksi');
			$data['form_user_name_usr']	= $this->lang->line('form_user_name_usr');
			$data['form_passwd_usr']	= $this->lang->line('form_passwd_usr');
			$data['form_re_passwd_usr']	= $this->lang->line('form_re_passwd_usr');
			$data['form_level_user_usr']	= $this->lang->line('form_level_user_usr');
			$data['form_status_usr']	= $this->lang->line('form_status_usr');
			$data['form_pilihan_level_usr']	= $this->lang->line('form_pilihan_level_usr');
			$data['form_pilihan_status_usr']	= $this->lang->line('form_pilihan_status_usr');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "";
			$data['limages']	= base_url();
			$data['llevel']	= "";
			$this->load->model('user/mclass');
			$data['query']	= $this->mclass->view();
			$data['opt_level']	= $this->mclass->LevelUser();
			$data['isi']		= 'user/vmainform';
			$this->load->view('template',$data);
		
	
	}
	function tambah() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}	
			$data['page_title_adduser']	= $this->lang->line('page_title_adduser');
			$data['form_tbl_user_id']	= $this->lang->line('form_tbl_user_id');
			$data['form_tbl_user_name']	= $this->lang->line('form_tbl_user_name');
			$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
			$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
			$data['link_aksi']	= $this->lang->line('link_aksi');
			$data['form_user_name_usr']	= $this->lang->line('form_user_name_usr');
			$data['form_passwd_usr']	= $this->lang->line('form_passwd_usr');
			$data['form_re_passwd_usr']	= $this->lang->line('form_re_passwd_usr');
			$data['form_level_user_usr']	= $this->lang->line('form_level_user_usr');
			$data['form_status_usr']	= $this->lang->line('form_status_usr');
			$data['form_pilihan_level_usr']	= $this->lang->line('form_pilihan_level_usr');
			$data['form_pilihan_status_usr']	= $this->lang->line('form_pilihan_status_usr');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "";
			$data['limages']	= base_url();
			$data['llevel']	= "";
			$this->load->model('user/mclass');
			$data['query']	= $this->mclass->view();
			$data['opt_level']	= $this->mclass->LevelUser();
			$data['isi']		= 'user/vformadd';
			$this->load->view('template',$data);
		
	
	}
	function simpan() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		$e_user_name	= $this->input->post('e_user_name')?trim($this->input->post('e_user_name')):trim($this->input->get_post('e_user_name'));
		$e_password	= $this->input->post('e_password')?$this->input->post('e_password'):$this->input->get_post('e_password');
		$re_e_password	= $this->input->post('re_e_password')?$this->input->post('re_e_password'):$this->input->get_post('re_e_password');
		$ilevel	= $this->input->post('ilevel')?$this->input->post('ilevel'):$this->input->get_post('ilevel');
		$iuseractive	= $this->input->post('iuseractive')?$this->input->post('iuseractive'):$this->input->get_post('iuseractive');
		
		if( (!empty($e_user_name) && !empty($e_password)) && 
			(trim($e_password)==trim($re_e_password)) ) {
			
			$this->load->model('user/mclass');
			$passwd	= md5(trim($e_password));
			
			$qry_cekuser = $this->mclass->CekUser($e_user_name);
			
			if($qry_cekuser->num_rows()>0) {
				$data['page_title_adduser']	= $this->lang->line('page_title_adduser');
				$data['form_tbl_user_id']	= $this->lang->line('form_tbl_user_id');
				$data['form_tbl_user_name']	= $this->lang->line('form_tbl_user_name');
				$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
				$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
				$data['link_aksi']	= $this->lang->line('link_aksi');
				$data['form_user_name_usr']	= $this->lang->line('form_user_name_usr');
				$data['form_passwd_usr']	= $this->lang->line('form_passwd_usr');
				$data['form_re_passwd_usr']	= $this->lang->line('form_re_passwd_usr');
				$data['form_level_user_usr']	= $this->lang->line('form_level_user_usr');
				$data['form_status_usr']	= $this->lang->line('form_status_usr');
				$data['form_pilihan_level_usr']	= $this->lang->line('form_pilihan_level_usr');
				$data['form_pilihan_status_usr']	= $this->lang->line('form_pilihan_status_usr');
				$data['button_simpan']	= $this->lang->line('button_simpan');
				$data['button_batal']	= $this->lang->line('button_batal');
				$data['detail']		= "";
				$data['list']		= "";
				$data['not_defined']	= "Maaf, Username yg anda masukan telah tersedia. Terimakasih.";
				$data['limages']	= base_url();
				$data['llevel']	= "";
				$this->load->model('user/mclass');
				$data['isi']	= $this->mclass->view();
				$data['opt_level']	= $this->mclass->LevelUser();
				$this->load->view('user/vmainform',$data);
			} else {
				$this->mclass->msimpan($e_user_name,$passwd,$ilevel,$iuseractive);			
			}
		} else {
			$data['page_title_adduser']	= $this->lang->line('page_title_adduser');
			$data['form_tbl_user_id']	= $this->lang->line('form_tbl_user_id');
			$data['form_tbl_user_name']	= $this->lang->line('form_tbl_user_name');
			$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
			$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
			$data['form_user_name_usr']	= $this->lang->line('form_user_name_usr');
			$data['form_passwd_usr']	= $this->lang->line('form_passwd_usr');
			$data['form_re_passwd_usr']	= $this->lang->line('form_re_passwd_usr');
			$data['form_level_usr']	= $this->lang->line('form_level_usr');
			$data['form_status_usr']	= $this->lang->line('form_status_usr');
			$data['form_pilihan_level_usr']	= $this->lang->line('form_pilihan_level_usr');
			$data['form_pilihan_status_usr']	= $this->lang->line('form_pilihan_status_usr');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "";
			$data['limages']	= base_url();
			$data['not_defined']	= "Form pengisian hrs diisi lengkap, terimakasih.";
			$data['llevel']	= "";
			$this->load->model('user/mclass');
			$data['isi']	= $this->mclass->view();
			$data['opt_level']	= $this->mclass->LevelUser();
			
			$this->load->view('user/vmainform',$data);			
		}
	}
	
	function actdelete() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('user/mclass');
		$this->mclass->delete($id);
	}
		
	function edit() {
		
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		$iuserid	= $this->uri->segment('4');

		$data['page_title_adduser']	= $this->lang->line('page_title_adduser');
		$data['form_tbl_user_id']	= $this->lang->line('form_tbl_user_id');
		$data['form_tbl_user_name']	= $this->lang->line('form_tbl_user_name');
		$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
		$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['form_user_name_usr']	= $this->lang->line('form_user_name_usr');
		$data['form_passwd_usr']	= $this->lang->line('form_passwd_usr');
		$data['form_re_passwd_usr']	= $this->lang->line('form_re_passwd_usr');
		$data['form_passwd_old_usr']= $this->lang->line('form_passwd_old_usr');
		$data['form_passwd_new_usr']= $this->lang->line('form_passwd_new_usr');
		$data['form_level_user_usr']	= $this->lang->line('form_level_user_usr');
		$data['form_status_usr']	= $this->lang->line('form_status_usr');
		$data['form_pilihan_level_usr']	= $this->lang->line('form_pilihan_level_usr');
		$data['form_pilihan_status_usr']	= $this->lang->line('form_pilihan_status_usr');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		$data['llevel']	= "";
		
		$this->load->model('user/mclass');
		$data['opt_level']	= $this->mclass->LevelUser();
		$query	= $this->mclass->get($iuserid);
		
		$data['i_user_id']  = $query['i_user_id'];   
		$data['e_user_name']= $query['e_user_name'];
		$data['e_password'] = $query['e_password'];
		$i_user_active		= $query['i_user_active'];
		$data['i_level']	= $query['i_level'];
		$data['aktif']		= $i_user_active==1?"selected":"";
		$data['nonaktif']	= $i_user_active==0?"selected":"";
		$data['isi']		= 'user/veditform';
			$this->load->view('template',$data);
		
	}
	
	function actedit() {
			
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
		$i_user_id	= $this->input->post('i_user_id')?$this->input->post('i_user_id'):$this->input->get_post('i_user_id');
		$e_user_name	= $this->input->post('e_user_name')?$this->input->post('e_user_name'):$this->input->get_post('e_user_name');
		$e_password_old	= $this->input->post('e_password_old')?$this->input->post('e_password_old'):$this->input->get_post('e_password_old');
		$e_password_new	= $this->input->post('e_password_new')?$this->input->post('e_password_new'):$this->input->get_post('e_password_new');
		$re_e_password	= $this->input->post('re_e_password')?$this->input->post('re_e_password'):$this->input->get_post('re_e_password');
		$ilevel	= $this->input->post('ilevel')?$this->input->post('ilevel'):$this->input->get_post('ilevel');
		$iuseractive	= $this->input->post('iuseractive')?$this->input->post('iuseractive'):$this->input->get_post('iuseractive');
		
		$e_password_old_enkri	= md5($e_password_old);
		$e_password_new_enkri	= md5($e_password_new);
		
		$this->load->model('user/mclass');
		if(($e_password_new==$re_e_password) &&
			!empty($re_e_password) ) {
			$getid	= $this->mclass->getid($i_user_id,$e_user_name,$e_password_old_enkri);
			if($getid->num_rows()>0) {
				$this->mclass->update($i_user_id,$e_user_name,$e_password_new_enkri);	
			} else {
				$data['page_title_adduser']	= $this->lang->line('page_title_adduser');
				$data['form_tbl_user_id']	= $this->lang->line('form_tbl_user_id');
				$data['form_tbl_user_name']	= $this->lang->line('form_tbl_user_name');
				$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
				$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
				$data['link_aksi']	= $this->lang->line('link_aksi');
				$data['form_user_name_usr']	= $this->lang->line('form_user_name_usr');
				$data['form_passwd_usr']	= $this->lang->line('form_passwd_usr');
				$data['form_re_passwd_usr']	= $this->lang->line('form_re_passwd_usr');
				$data['form_level_user_usr']	= $this->lang->line('form_level_user_usr');
				$data['form_status_usr']	= $this->lang->line('form_status_usr');
				$data['form_pilihan_level_usr']	= $this->lang->line('form_pilihan_level_usr');
				$data['form_pilihan_status_usr']	= $this->lang->line('form_pilihan_status_usr');
				$data['button_simpan']	= $this->lang->line('button_simpan');
				$data['button_batal']	= $this->lang->line('button_batal');
				$data['detail']		= "";
				$data['list']		= "";
				$data['not_defined']	= "Maaf, Password yg anda masukan salah, terimakasih";
				$data['limages']	= base_url();
				$data['llevel']	= "";
				$this->load->model('user/mclass');
				$data['isi']	= $this->mclass->view();
				$data['opt_level']	= $this->mclass->LevelUser();
				$this->load->view('user/vmainform',$data);
			}
		} else {
			$data['page_title_adduser']	= $this->lang->line('page_title_adduser');
			$data['form_tbl_user_id']	= $this->lang->line('form_tbl_user_id');
			$data['form_tbl_user_name']	= $this->lang->line('form_tbl_user_name');
			$data['form_tbl_level']	= $this->lang->line('form_tbl_level');
			$data['form_tbl_status']	= $this->lang->line('form_tbl_status');
			$data['link_aksi']	= $this->lang->line('link_aksi');
			$data['form_user_name_usr']	= $this->lang->line('form_user_name_usr');
			$data['form_passwd_usr']	= $this->lang->line('form_passwd_usr');
			$data['form_re_passwd_usr']	= $this->lang->line('form_re_passwd_usr');
			$data['form_level_user_usr']	= $this->lang->line('form_level_user_usr');
			$data['form_status_usr']	= $this->lang->line('form_status_usr');
			$data['form_pilihan_level_usr']	= $this->lang->line('form_pilihan_level_usr');
			$data['form_pilihan_status_usr']	= $this->lang->line('form_pilihan_status_usr');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']	= "Maaf, Password yg anda masukan tdk sama, terimakasih";
			$data['limages']	= base_url();
			$data['llevel']	= "";
			$this->load->model('user/mclass');
			$data['query']	= $this->mclass->view();
			$data['opt_level']	= $this->mclass->LevelUser();
			
			$data['isi']	= 'user/vmainform';
			$this->load->view('template',$data);
			
		}
	}
}
?>
