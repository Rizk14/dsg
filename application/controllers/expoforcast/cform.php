<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expoforcast/mclass');
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}		
			$data['form_title_detail_expo_forcast']	= $this->lang->line('form_title_detail_expo_forcast');
			$data['page_title_expo_forcast']	= $this->lang->line('page_title_expo_forcast');
			$data['list_no_expo_forcast']= $this->lang->line('list_no_expo_forcast');
			$data['list_no_forcast_expo_forcast']	= $this->lang->line('list_no_forcast_expo_forcast');
			$data['list_drop_forcast_expo_forcast']	= $this->lang->line('list_drop_forcast_expo_forcast');
			$data['list_tgl_forcast_expo_forcast']	= $this->lang->line('list_tgl_forcast_expo_forcast');
			$data['list_kd_brg_expo_forcast']	= $this->lang->line('list_kd_brg_expo_forcast');
			$data['list_nm_brg_expo_forcast']	= $this->lang->line('list_nm_brg_expo_forcast');
			$data['list_jml_forcast_expo_forcast']	= $this->lang->line('list_jml_forcast_expo_forcast');
			$data['list_no_sj_expo_forcast']	= $this->lang->line('list_no_sj_expo_forcast');
			$data['list_tgl_sj_expo_forcast']	= $this->lang->line('list_tgl_sj_expo_forcast');
			$data['list_jml_sj_expo_forcast']	= $this->lang->line('list_jml_sj_expo_forcast');
			$data['list_sisa_forcast_expo_forcast']	= $this->lang->line('list_sisa_forcast_expo_forcast');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			/*** $this->load->model('expoforcast/mclass'); ***/
				$data['isi']	= 'expoforcast/vmainform';	
			$this->load->view('template',$data);	
		
		
	}
	
	function carilistforcast() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['form_title_detail_expo_forcast']	= $this->lang->line('form_title_detail_expo_forcast');
		$data['page_title_expo_forcast']	= $this->lang->line('page_title_expo_forcast');
		$data['list_no_expo_forcast']= $this->lang->line('list_no_expo_forcast');
		$data['list_no_forcast_expo_forcast']	= $this->lang->line('list_no_forcast_expo_forcast');
		$data['list_drop_forcast_expo_forcast']	= $this->lang->line('list_drop_forcast_expo_forcast');
		$data['list_tgl_forcast_expo_forcast']	= $this->lang->line('list_tgl_forcast_expo_forcast');
		$data['list_kd_brg_expo_forcast']	= $this->lang->line('list_kd_brg_expo_forcast');
		$data['list_nm_brg_expo_forcast']	= $this->lang->line('list_nm_brg_expo_forcast');
		$data['list_jml_forcast_expo_forcast']	= $this->lang->line('list_jml_forcast_expo_forcast');
		$data['list_no_sj_expo_forcast']	= $this->lang->line('list_no_sj_expo_forcast');
		$data['list_tgl_sj_expo_forcast']	= $this->lang->line('list_tgl_sj_expo_forcast');
		$data['list_jml_sj_expo_forcast']	= $this->lang->line('list_jml_sj_expo_forcast');
		$data['list_sisa_forcast_expo_forcast']	= $this->lang->line('list_sisa_forcast_expo_forcast');
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
		
		$i_forcast	= $this->input->post('i_forcast');
		$iforcast	= !empty($i_forcast)?$i_forcast:"kosong";
		
		$i_drop_forcast	= $this->input->post('i_drop_forcast');
		$idropforcast	= !empty($i_drop_forcast)?$i_drop_forcast:"kosong";
		
		$d_forcast_first	= $this->input->post('d_forcast_first');
		$d_forcast_last	= $this->input->post('d_forcast_last');
		
		$data['tglforcastmulai']	= $d_forcast_first;
		$data['tglforcastakhir']	= $d_forcast_last;
		$data['i_forcast']			= $i_forcast;
		$data['i_drop_forcast']		= $i_drop_forcast;
		
		$e_d_forcast_first	= explode("/",$d_forcast_first,strlen($d_forcast_first));
		$e_d_forcast_last	= explode("/",$d_forcast_last,strlen($d_forcast_last));
		
		$n_d_forcast_first	= !empty($e_d_forcast_first[2])?$e_d_forcast_first[2].'-'.$e_d_forcast_first[1].'-'.$e_d_forcast_first[0]:'0';
		$n_d_forcast_last	= !empty($e_d_forcast_last[2])?$e_d_forcast_last[2].'-'.$e_d_forcast_last[1].'-'.$e_d_forcast_last[0]:'0';
		
		$data['iforcast']		= !empty($i_forcast)?$i_forcast:"kosong";
		$data['idropforcast']	= !empty($i_drop_forcast)?$i_drop_forcast:"kosong";
		$data['tforcastfirst']	= $n_d_forcast_first;
		$data['tforcastlast']	= $n_d_forcast_last;
		
		/*** $this->load->model('expoforcast/mclass'); ***/
		
		if($n_d_forcast_first!=0 && $n_d_forcast_last!=0 && $i_forcast=='' && $i_drop_forcast=='') {
			$data['query']	= $this->mclass->clistforcast2($iforcast,$idropforcast,$n_d_forcast_first,$n_d_forcast_last);
			$data['template']= 1;
		}else{
			$data['query']	= $this->mclass->clistforcast($iforcast,$idropforcast,$n_d_forcast_first,$n_d_forcast_last);
			$data['template']= 0;
		}
		$data['isi']	= 'expoforcast/vlistform';	
			$this->load->view('template',$data);	
		
	}
	
	function gexporforcast() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['form_title_detail_expo_forcast']	= $this->lang->line('form_title_detail_expo_forcast');
		$data['page_title_expo_forcast']	= $this->lang->line('page_title_expo_forcast');
		$data['list_no_expo_forcast']= $this->lang->line('list_no_expo_forcast');
		$data['list_no_forcast_expo_forcast']	= $this->lang->line('list_no_forcast_expo_forcast');
		$data['list_drop_forcast_expo_forcast']	= $this->lang->line('list_drop_forcast_expo_forcast');
		$data['list_tgl_forcast_expo_forcast']	= $this->lang->line('list_tgl_forcast_expo_forcast');
		$data['list_kd_brg_expo_forcast']	= $this->lang->line('list_kd_brg_expo_forcast');
		$data['list_nm_brg_expo_forcast']	= $this->lang->line('list_nm_brg_expo_forcast');
		$data['list_jml_forcast_expo_forcast']	= $this->lang->line('list_jml_forcast_expo_forcast');
		$data['list_no_sj_expo_forcast']	= $this->lang->line('list_no_sj_expo_forcast');
		$data['list_tgl_sj_expo_forcast']	= $this->lang->line('list_tgl_sj_expo_forcast');
		$data['list_jml_sj_expo_forcast']	= $this->lang->line('list_jml_sj_expo_forcast');
		$data['list_sisa_forcast_expo_forcast']	= $this->lang->line('list_sisa_forcast_expo_forcast');
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lfrcst']		= "";
			
		$iforcast		= $this->uri->segment(4);
		$idropforcast	= $this->uri->segment(5);
		$tforcastfirst	= $this->uri->segment(6);
		$tforcastlast	= $this->uri->segment(7);
		
		$extffirst	= explode("-",$tforcastfirst,strlen($tforcastfirst));
		$extflast	=  explode("-",$tforcastlast,strlen($tforcastlast));
		
		$nwffirst	= (!empty($tforcastfirst) || $tforcastfirst!=0)?$extffirst[2]."/".$extffirst[1]."/".$extffirst[0]:'0';
		$nwflast	= (!empty($tforcastlast) || $tforcastlast!=0)?$extflast[2]."/".$extflast[1]."/".$extflast[0]:'0';
		
		$periode	= $nwffirst." s.d ".$nwflast;
		
		$data['tforcastfirst']	= $tforcastfirst;
		$data['tforcastlast']	= $tforcastlast;
		
		$data['tglforcastmulai']	= $nwffirst==0?'':$nwffirst;
		$data['tglforcastakhir']	= $nwflast==0?'':$nwflast;
		
		/*** $this->load->model('expoforcast/mclass'); ***/
				
		$data['query']	= $this->mclass->clistforcast_excell1($iforcast,$idropforcast,$tforcastfirst,$tforcastlast);
		$data['template'] = 1;
		
		$ObjPHPExcel = new PHPExcel();
		
		$qexpoforcast	= $this->mclass->clistforcast_excell2($tforcastfirst,$tforcastlast);
		
		$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
		$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
		$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
		$ObjPHPExcel->getProperties()
			->setTitle("Laporan Forcast")
			->setSubject("Laporan Forcast")
			->setDescription("Laporan Forcast")
			->setKeywords("Laporan Bulanan")
			->setCategory("Laporan");

		$ObjPHPExcel->setActiveSheetIndex(0);
		
		$ObjPHPExcel->createSheet();
			
		$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

		$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10); // Kode Barang 
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Nama Barang
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12); // Forcast
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6); // SJ
		$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12); // Sisa Forcast
		$ObjPHPExcel->getActiveSheet()->mergeCells('A2:F2');
		$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN FORCAST');
		$ObjPHPExcel->getActiveSheet()->mergeCells('A3:F3');

		$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);
						
		$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
		
		$ObjPHPExcel->getActiveSheet()->mergeCells('A4:F4');

		$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);
						
		$ObjPHPExcel->getActiveSheet()->setCellValue('A4', 'CV. DUTA SETIA GARMEN');
			
		$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
		$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'KODE BARANG');
		$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'NAMA BARANG');
		$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'DROP FORCAST');
		$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'SJ');
		$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

		$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'SISA FORCAST');
		$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic'=> false,
						'size'  => 10
						),					
						'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom'=> array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				
		if($qexpoforcast->num_rows()>0) {
				
			$j	= 7;
			$nomor	= 1;
			
			$tforcast = 0;
			$tsisaforcast = 0;
			$tsj = 0;
								
			foreach($qexpoforcast->result() as $row) {
				
				$qtotalforcast = $this->mclass->totalforcast($row->i_product,$tforcastfirst,$tforcastlast);
				if($qtotalforcast->num_rows()>0){
					$rtotalforcast 		= $qtotalforcast->row();							
					$totalforcast 		= $rtotalforcast->totalforcast;
					$totalsisaforcast	= $rtotalforcast->totalsisaforcast;
					$totalsj			= $totalforcast-$totalsisaforcast;
				}else{
					$totalforcast 		= 0;
					$totalsisaforcast	= 0;
					$totalsj			= 0;
				}
			 	
				$tforcast 		= $tforcast+$totalforcast;
				$tsisaforcast 	= $tsisaforcast+$totalsisaforcast;
				$tsj 			= $tsj+$totalsj;
						 											
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->i_product);
				$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->e_product_name);
				$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $totalforcast);
				$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $totalsj);
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $totalsisaforcast);
				$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 9
							),
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)
						)
					);
				$j++;																																					
				$nomor++;
			}
			$jj=$j+1;

			$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$jj, $tforcast);
			$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$jj, $tsj);
			$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$jj, $tsisaforcast);
 						
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			$files	= $this->session->userdata('gid')."laporan_forcast"."_".$tforcastfirst."-".$tforcastlast.".xls";
			$ObjWriter->save("files/".$files);

			$efilename = substr($files,1,strlen($files));
	
			$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));		
		}
					$data['isi']	= 'expoforcast/vexpform';	
			$this->load->view('template',$data);	
		
	}

	function listforcast() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "DATA FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expoforcast/mclass'); ***/

		$query	= $this->mclass->lforcast();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/expoforcast/cform/listforcastnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lforcastperpages($pagination['per_page'],$pagination['cur_page']);
	
		$this->load->view('expoforcast/vlistformforcast',$data);	
	}

	function listforcastnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "DATA FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expoforcast/mclass'); ***/

		$query	= $this->mclass->lforcast();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expoforcast/cform/listforcastnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lforcastperpages($pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('expoforcast/vlistformforcast',$data);	
	}	
	
	function flistforcast() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$data['page_title']	= "DATA FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expoforcast/mclass'); ***/
		
		if(!empty($key)) {
			$query	= $this->mclass->flforcast($key);
			$jml	= $query->num_rows();
		} else {
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_forcast_code')\">".$row->i_forcast_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_forcast_code')\">".$row->d_forcast."</a></td>
				 </tr>";				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}

	function listdropforcast() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "DATA DROP FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expoforcast/mclass'); ***/

		$query	= $this->mclass->ldropforcast();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/expoforcast/cform/listdropforcastnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ldropforcastperpages($pagination['per_page'],$pagination['cur_page']);
	
		$this->load->view('expoforcast/vlistformdropforcast',$data);	
	}

	function listdropforcastnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "DATA DROP FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expoforcast/mclass'); ***/

		$query	= $this->mclass->ldropforcast();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expoforcast/cform/listdropforcastnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ldropforcastperpages($pagination['per_page'],$pagination['cur_page']);		
	
		$this->load->view('expoforcast/vlistformdropforcast',$data);	
	}	
		
	function flistdropforcast() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$data['page_title']	= "DATA DROP FORCAST";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		/*** $this->load->model('expoforcast/mclass'); ***/
		
		if(!empty($key)) {
			$query	= $this->mclass->fldropforcast($key);
			$jml	= $query->num_rows();
		} else {
			$jml	= 0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 
			
			foreach($query->result() as $row){

				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_forcast_code')\">".$row->i_forcast_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_forcast_code')\">".$row->d_forcast."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;	
	}
}

?>
