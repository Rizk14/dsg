<?php
#include_once("printipp_classes/PrintIPP.php"); 

class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
			$data['page_title_do']	= "Cetak Khusus - ".$this->lang->line('page_title_do');
			$data['form_title_detail_do']	= "Cetak Khusus - ".$this->lang->line('form_title_detail_do');
			$data['list_no_do']	= $this->lang->line('list_no_do');
			$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
			$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
			$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
			$data['list_qty_do']	= $this->lang->line('list_qty_do');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');		
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi']		='khususprntdo/vmainform';
			$this->load->model('khususprntdo/mclass');
			$this->load->view('template',$data);
	}
	
	function carilistdo() {		
		$data['page_title_do']	= "Cetak Khusus - ".$this->lang->line('page_title_do');
		$data['form_title_detail_do']	= "Cetak Khusus - ".$this->lang->line('form_title_detail_do');
		$data['list_no_do']	= $this->lang->line('list_no_do');
		$data['list_no_op_do']	= $this->lang->line('list_no_op_do');
		$data['list_kd_brg_do']	= $this->lang->line('list_kd_brg_do');
		$data['list_nm_brg_do']	= $this->lang->line('list_nm_brg_do');
		$data['list_qty_do']	= $this->lang->line('list_qty_do');
		$data['list_sisa_delivery_do']	= $this->lang->line('list_sisa_delivery_do');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['ldobrg']	= "";

		$kddo	= $this->input->post('nomor_do')? $this->input->post('nomor_do'): $this->input->get_post('nomor_do');
		
		$this->load->model('khususprntdo/mclass');
		
		$data['query']= $this->mclass->clistdobrg($kddo);
		$qtgl		= $this->mclass->ctgldobrg($kddo);
		$qcabang	= $this->mclass->getcabang($kddo);
		
		if($qcabang->num_rows() > 0 ) {
			$row_cabang	= $qcabang->row_array();
			$data['nmcabang']	= $row_cabang['cabangname'];
		} else {
			$data['nmcabang']	= "";
		}
		
		if($qtgl->num_rows() > 0 ) {

			$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli', 
						'08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
			
			$row_tgl	= $qtgl->row();
			$exp_tgl	= explode("-",$row_tgl->tgl,strlen($row_tgl->tgl)); // Y-m-d
			$f_tgl		= substr($exp_tgl[2],0,1)=='0'?substr($exp_tgl[2],1,1):$exp_tgl[2];
			$f_bln		= $bglobal[$exp_tgl[1]];
			$f_thn		= $exp_tgl[0];
			
			$data['nomor']	= $kddo;
			$data['sh_tgl']	= $f_tgl." ".$f_bln." ".$f_thn;
		} else {
			$data['nomor']	= "";
			$data['sh_tgl']	= "";		
		}
		$data['isi']	='khususprntdo/vlistform';
		$this->load->view('template',$data);
	}
	
	function cetakdo() {
		$kddo		= $this->uri->segment(4);
		
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;	
		
		$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli', 
							'08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
		$tgl	= date("d");
		$bln	= date("m");						
		$thn	= date("Y");
		$new_bln= $bglobal[$bln];
		$data['tglprint']	= $tgl."-".$new_bln."-".$thn;
		
		$this->load->model('khususprntdo/mclass');
		
		$data['isi']	= $this->mclass->clistdobrg($kddo);
		
		$qtgl		= $this->mclass->ctgldobrg($kddo);
		$qcabang	= $this->mclass->getcabang($kddo);
		$qnmrop		= $this->mclass->cnmrop($kddo);
		$qinitial	= $this->mclass->getinitial();

		if($qcabang->num_rows() > 0 ) {
			$row_cabang	= $qcabang->row_array();
			$data['nmcabang']	= $row_cabang['cabangname'];
			$data['addr']		= $row_cabang['addr'];
		} else {
			$data['nmcabang']	= '';
			$data['addr']	= '';
		}

		if($qinitial->num_rows()>0) {
			$row_initial	= $qinitial->row_array();
			$data['nminitial']	= $row_initial['e_initial_name'];
		} else {
			$data['nminitial']	= "";
		}
				
		if($qnmrop->num_rows() > 0) {
			$row_nmrop	= $qnmrop->row();
			$nomorop = $row_nmrop->iopcode;
		} else {
			$nomorop = "";
		}
		$data['nomorop']	= $nomorop;
		
		if($qtgl->num_rows() > 0 ) {
			$bglobal	= array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni',
								'07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember' );
			
			$row_tgl	= $qtgl->row();
			$exp_tgl	= explode("-",$row_tgl->tgl,strlen($row_tgl->tgl));
			$f_tgl		= substr($exp_tgl[2],0,1)=='0'?substr($exp_tgl[2],1,1):$exp_tgl[2];
			$f_bln		= $bglobal[$exp_tgl[1]];
			$f_thn		= $exp_tgl[0];
			
			$data['nomor']	= $kddo;
			$data['shtgl']	= (string) html_entity_decode($f_tgl.'#'.$f_bln.'#'.$f_thn);
			
			$data['dotgl']	= (string) html_entity_decode($f_tgl);
			$data['dobln']	= (string) html_entity_decode($f_bln);
			$data['dothn']	= (string) html_entity_decode($f_thn);

			#$this->mclass->fprinted($kddo);
		} else {
			$data['nomor']	= '';
			$data['shtgl']	= '';		
		}
		
		$this->load->view('khususprntdo/vprintdo',$data);
	}

	function updatecetak() {
		$kddo = $this->input->post('kddo');
		
		$this->load->model('khususprntdo/mclass');
		
		$this->db->trans_begin();
		
		$this->mclass->fprinted($kddo);

		if($this->db->trans_status()===FALSE){
			$this->db->trans_rollback();
			echo 'fail';
		} else {
			$this->db->trans_commit();
			echo $kddo;
			$this->logger->write("Cetak DO NO : ".$kddo);
		}
	}
}
?>
