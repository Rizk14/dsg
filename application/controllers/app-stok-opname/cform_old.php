<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('app-stok-opname/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'app-stok-opname/vmainform';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$gudang = $this->input->post('gudang', TRUE);  

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";

		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
		
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['gudang'] = $gudang;
		$data['kode_gudang'] = $kode_gudang;
		$data['nama_gudang'] = $nama_gudang;
		$data['nama_lokasi'] = $nama_lokasi;

	$cek_data = $this->mmaster->cek_so_bahanbaku($bulan, $tahun, $gudang); 
	//print_r($cek_data); die();
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." belum diinput..!";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'app-stok-opname/vmainform';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname/vmainform';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_bahan_baku yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname_bahanbaku($bulan, $tahun, $gudang);
	
			$data['jum_total'] = count($data['query']);
			$data['isi'] = 'app-stok-opname/vformview';
			$this->load->view('template',$data);
			
		}
	} // end else

  }
  
  function submit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $gudang = $this->input->post('gudang', TRUE);  
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  
	  $tgl = date("Y-m-d"); 
	  $ada = 0;
	  // cek di tiap2 bahan bakunya, apakah ada data brg yg blm ada harganya. jika blm ada harga, maka munculkan pesan error
	  // revisi 130212: utk data bhn baku yg blm ada stok harga, harganya diset 0 aja dulu
	  for ($i=1;$i<=$no;$i++) {
		  $kode_brg = $this->input->post('kode_'.$i, TRUE);
		  $id_satuan = $this->input->post('satuan_'.$i, TRUE);
		  $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
		  
		  //cek di tabel tm_harga_brg_supplier
		  //$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE kode_brg = '$kode_brg' ");
		  $query3	= $this->db->query(" SELECT harga FROM tm_stok_harga WHERE kode_brg = '$kode_brg' AND quilting = 'f' ");
			if ($query3->num_rows() == 0){
				//$ada = 0;
				//break;
				// save di tm_stok
				$data_detail = array(
						'kode_brg'=>$kode_brg, 
						'stok'=>$jum_stok,
						'tgl_update_stok'=>$tgl
					);
				$this->db->insert('tm_stok',$data_detail);
				
				// save di tm_stok_harga
				$data_detail = array(
						'kode_brg'=>$kode_brg, 
						'stok'=>$jum_stok,
						'harga'=>'0',
						'tgl_update_stok'=>$tgl
					);
				$this->db->insert('tm_stok_harga',$data_detail);
		   				
			}
			else {
				$ada = 1;
			}
	  }
	  
	  /*if ($ada == 0) {
		  $data['msg'] = "Input stok opname untuk bulan ".$nama_bln." ".$tahun." tidak dapat diproses karena ada data barang yang tidak ada harga..!";
		  $data['list_gudang'] = $this->mmaster->get_gudang();
		  $data['isi'] = 'app-stok-opname/vmainform';
		  $this->load->view('template',$data);
	  } */
	        
	      for ($i=1;$i<=$no;$i++) {
			 // cek ada selisih ga.
			 $kode_brg = $this->input->post('kode_'.$i, TRUE);
			 $id_satuan = $this->input->post('satuan_'.$i, TRUE);
			 $stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE); // ini sudah dlm hasil konversi (utk satuan awalnya Yard dan Lusin)
			 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
			 $idnya = $this->input->post('id_'.$i, TRUE);
			 			 
			 // balikin lagi ke satuan dasar (dari meter/pcs) jika yard atau lusin
			 if ($id_satuan == 2) { // yard
				$new_stok_reverse = $stok_fisik/0.91; // ini konversi dari meter ke yard
			 }
			 else if ($id_satuan == 7) { // lusin
				$new_stok_reverse = $stok_fisik/12; // ini konversi dari pcs ke lusin
			 }
			 else
				$new_stok_reverse = $stok_fisik;
			 
			 //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 18-02-2012 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			 // update ke tm_stok dan ke tm_stok_harga
			 //cek apakah data stoknya ada
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$stok_lama = 0;
				
					$data_stok = array(
						'kode_brg'=>$kode_brg,
						'stok'=>$new_stok_reverse,
						'tgl_update_stok'=>$tgl
					);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
					
					//update ke tm_stok dan history stoknya, 
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok_reverse', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode_brg' ");
				}
				
				// selisih
				$selisih_reverse = $new_stok_reverse-$stok_lama; // selisih = 11-12
				/*if ($kode_brg == 'BB050000000001') {
					echo $new_stok_reverse." ".$stok_lama." ".$selisih_reverse."<br>";
				} */
				
				// stok harga
				// 08-12-2012, cek dulu ada brp data stok harga yg tidak 0. trus query perulangan data2 stok harga yg ada berdasarkan kode brg tsb. 
				
				//$inya=1;
				$query2x	= $this->db->query(" SELECT count(id) as jum FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
									AND stok > 0 AND quilting='f' ");
				if ($query2x->num_rows() > 0){
					$hasil2x = $query2x->row();
					$jmldatastokharga = $hasil2x->jum;
				}
				else
					$jmldatastokharga = 0;
				
				$query2x	= $this->db->query(" SELECT sum(stok) as jumstok FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
									AND stok > 0 AND quilting='f' ");
				if ($query2x->num_rows() > 0){
					$hasil2x = $query2x->row();
					$jmlstokharga = $hasil2x->jumstok;
				}
				else
					$jmlstokharga = 0;
				
				$pengurangan = $new_stok_reverse-$jmlstokharga; 
				//if ($jmldatastokharga != 0 && $selisih_reverse !=0)
				if ($jmldatastokharga != 0 && $pengurangan !=0) {
					$bagirata = $new_stok_reverse/$jmldatastokharga;
					$statupdate=1;
				}
				else {
					$bagirata = 0;
					$statupdate=0;
				}

				$query2x	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
									AND stok > 0 AND quilting='f' ORDER BY id ASC ");
				$hasil2x = $query2x->result();
				foreach ($hasil2x as $row2x) {
					$stoknya = $row2x->stok;
					$harganya = $row2x->harga;
					$id_stok_harga = $row2x->id;
					
					if ($statupdate == 0) {
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$stoknya', tgl_update_stok = '$tgl' 
										where id= '$id_stok_harga' ");
						
						// save ke tt_so_bahan_baku_detail_harga
						// 06-12-12
						//cek apakah data stoknya ada
						$query3x	= $this->db->query(" SELECT id FROM tt_so_bahan_baku_detail_harga WHERE 
										id_stok_opname_bahan_baku_detail = '$idnya' AND harga = '$harganya' ");
						if ($query3x->num_rows() == 0){
							$data_stok = array(
								'id_stok_opname_bahan_baku_detail'=>$idnya,
								'harga'=>$harganya,
								'stok'=>$stoknya
							);
							$this->db->insert('tt_so_bahan_baku_detail_harga',$data_stok);
						}
						else {
							$hasil3x = $query3x->row();
							$idsodetailharga = $hasil3x->id; 
							$this->db->query(" UPDATE tt_so_bahan_baku_detail_harga SET stok = '$stoknya'
											where id= '$idsodetailharga' ");
						}
					}
					else {
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$bagirata', tgl_update_stok = '$tgl' 
										where id= '$id_stok_harga' ");
						
						// save ke tt_so_bahan_baku_detail_harga
						// 06-12-12
						//cek apakah data stoknya ada
						$query3x	= $this->db->query(" SELECT id FROM tt_so_bahan_baku_detail_harga WHERE 
										id_stok_opname_bahan_baku_detail = '$idnya' AND harga = '$harganya' ");
						if ($query3x->num_rows() == 0){
							$data_stok = array(
								'id_stok_opname_bahan_baku_detail'=>$idnya,
								'harga'=>$harganya,
								'stok'=>$bagirata
							);
							$this->db->insert('tt_so_bahan_baku_detail_harga',$data_stok);
						}
						else {
							$hasil3x = $query3x->row();
							$idsodetailharga = $hasil3x->id; 
							$this->db->query(" UPDATE tt_so_bahan_baku_detail_harga SET stok = '$bagirata'
											where id= '$idsodetailharga' ");
						}
					}
				}
				
				// --------------------------------------------------------------------------------------------------------
				// #################### 11-12-12 ga dipake lagi	####################
			/*	if ($query2->num_rows() > 0){
					$hasil2 = $query2->row();
					$stoknya = $hasil2->stok; // 
					$harganya = $hasil2->harga;
					
					//$stok_baru = $stoknya + $selisih_reverse;
					// new 06-12-12
					$stok_baru = $new_stok_reverse;
								
					$this->db->query(" UPDATE tm_stok_harga SET stok = '$stok_baru', tgl_update_stok = '$tgl' 
										where kode_brg= '$kode_brg' AND harga = '$harganya' ");
										
					// save ke tt_so_bahan_baku_detail_harga
					// 06-12-12
					//cek apakah data stoknya ada
					$query3x	= $this->db->query(" SELECT id FROM tt_so_bahan_baku_detail_harga WHERE 
									id_stok_opname_bahan_baku_detail = '$idnya' AND harga = '$harganya' ");
					if ($query3x->num_rows() == 0){
						$data_stok = array(
							'id_stok_opname_bahan_baku_detail'=>$idnya,
							'harga'=>$harganya,
							'stok'=>$stok_baru
						);
						$this->db->insert('tt_so_bahan_baku_detail_harga',$data_stok);
					}
					else {
						$hasil3x = $query3x->row();
						$idsodetailharga = $hasil3x->id; 
						$this->db->query(" UPDATE tt_so_bahan_baku_detail_harga SET stok = '$stok_baru'
										where id= '$idsodetailharga' ");
					}
					
				} */
				// #################### 11-12-12 end	####################
				
				//dan update statusnya di tt_stok_opname_bahan_baku detailnya
				$this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET status_approve = 't'
								where id = '$idnya' "); 
				//if ($kode_brg == 'BB050000000001') die();
			 //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end 18-02-2012 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			 
		  } // end for
		  $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  
		  redirect('app-stok-opname/cform');
  }

}
