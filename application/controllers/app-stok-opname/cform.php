<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('app-stok-opname/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['isi'] = 'app-stok-opname/vmainform';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$gudang = $this->input->post('gudang', TRUE);  

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";

		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$gudang' ");
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
		$nama_lokasi	= $hasilrow->nama_lokasi;
		
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['gudang'] = $gudang;
		$data['kode_gudang'] = $kode_gudang;
		$data['nama_gudang'] = $nama_gudang;
		$data['nama_lokasi'] = $nama_lokasi;

	$cek_data = $this->mmaster->cek_so_bahanbaku($bulan, $tahun, $gudang); 
	//print_r($cek_data); die();
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang ".$kode_gudang." - ".$nama_gudang." belum diinput..!";
		$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'app-stok-opname/vmainform';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang ".$kode_gudang." - ".$nama_gudang." sudah di-approve..!";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname/vmainform';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_bahan_baku yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname_bahanbaku($bulan, $tahun, $gudang);
	
			$data['jum_total'] = count($data['query']);
			
			$tgl_so = $cek_data['tgl_so'];
			$pisah1 = explode("-", $tgl_so);
			$thn1= $pisah1[0];
			$bln1= $pisah1[1];
			$tgl1= $pisah1[2];
			$tgl_so = $tgl1."-".$bln1."-".$thn1;
			
			$data['tgl_so'] = $tgl_so;
			$data['isi'] = 'app-stok-opname/vformview';
			$this->load->view('template',$data);
		}
	} // end else

  }
  
  function submit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $gudang = $this->input->post('gudang', TRUE);  
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  
	  // 13-05-2015
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  // 01-11-2014, 13-06-2015 GA DIPAKE LAGI
	/*  if ($bulan < 12) {
		$bulanplus1 = $bulan+1;
		$tahunplus1 = $tahun;
		if ($bulanplus1 < 10)
			$bulanplus1= '0'.$bulanplus1;
	  }
	  else {
		$bulanplus1 = '01';
		$tahunplus1 = $tahun+1;
	  }
	  
	  $tglawalplus = $tahunplus1."-".$bulanplus1."-01"; */
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	  
	  $query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
	  $hasilrow = $query3->row();
	  $kode_gudang	= $hasilrow->kode_gudang;
	  $nama_gudang	= $hasilrow->nama;
	  
	  // 01-11-2014, skrip baru (perhitungan transaksi barang di tgl setelahnya ================================================================
	  for ($i=1;$i<=$no;$i++)
		  {
			 $id_brg = $this->input->post('id_brg_'.$i, TRUE);
			 $harga = $this->input->post('harga_'.$i, TRUE);
			 $stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE);
			 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
			 
			 // 16-03-2015, 13-05-2015 dikomen
			 //$auto_saldo_akhir = $this->input->post('auto_saldo_akhir_'.$i, TRUE);
			 //$auto_saldo_akhir_harga = $this->input->post('auto_saldo_akhir_harga_'.$i, TRUE);
			 
			 $idnya = $this->input->post('id_'.$i, TRUE);
			 
			 // ============================================= 13-05-2015
			 $totalxx = $stok_fisik;
			 
			 // seting satuan konversi dipindah kesini 08-08-2015
			 $query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.kode_brg, a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$kode_brg = $hasilnya->kode_brg;
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
				}
				else {
					$kode_brg = '';
					$nama_brg = '';
					$id_satuan_konversi = 0;
					$rumus_konversi = 0;
					$angka_faktor_konversi = 0;
				}
			 
			 // 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/ 
			
			// 1.ambil brg masuk hasil pembelian
			$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							INNER JOIN tm_barang c ON c.id = b.id_brg
							WHERE b.tgl_bonm > '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND b.status_stok = 't' AND c.id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$jum_masuk = $hasilrow->jum_masuk;
				if ($jum_masuk == '')
					$jum_masuk = 0;
				
				// 08-08-2015
				if ($id_satuan_konversi != 0) {
					if ($rumus_konversi == '1') {
						$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '2') {
						$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '3') {
						$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '4') {
						$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
					}
					else
						$jum_masuk_konv = $jum_masuk;
				}
				else
					$jum_masuk_konv = $jum_masuk;
			}
			else
				$jum_masuk_konv = 0;
			
					// 08-08-2015
					// 1.2. ambil brg masuk hasil pembelian manual
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukmanual a 
									INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual
									INNER JOIN tm_barang c ON c.id = b.id_brg
									WHERE a.tgl_bonm > '$tgl_so' AND b.id_brg = '$id_brg'
									AND a.id_gudang = '$gudang' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masukmanual = $hasilrow->jum_masuk;
						if ($jum_masukmanual == '')
							$jum_masukmanual = 0;
						
						if ($id_satuan_konversi != 0) {
							if ($rumus_konversi == '1') {
								$jum_masukmanual_konv = $jum_masukmanual*$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '2') {
								$jum_masukmanual_konv = $jum_masukmanual/$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '3') {
								$jum_masukmanual_konv = $jum_masukmanual+$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '4') {
								$jum_masukmanual_konv = $jum_masukmanual-$angka_faktor_konversi;
							}
							else
								$jum_masukmanual_konv = $jum_masukmanual;
						}
						else
							$jum_masukmanual_konv = $jum_masukmanual;
					}
					else
						$jum_masukmanual_konv = 0;
			
			// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE  a.tgl_bonm > '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' AND ((a.tujuan < '3' OR a.tujuan > '5') AND a.keterangan <> 'DIBEBANKAN') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
								
				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE a.tgl_bonm > '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' AND ((a.tujuan >= '3' AND a.tujuan <= '5') OR a.keterangan = 'DIBEBANKAN' ) "); 
							// AND (a.tujuan > '2' OR a.keterangan = 'DIBEBANKAN')
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
					
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							WHERE a.tgl_bonm  > '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
				}
				else
					$jum_masuk_lain = 0;
				
				$totmasuk = $jum_masuk_konv + $jum_masukmanual_konv + $jum_masuk_lain;
				$totkeluar = $jum_keluar+$jum_keluar_lain;
												
				$jum_stok_akhir = $totmasuk-$totkeluar;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------

				// tambahkan stok_akhir ini ke qty SO
				$totalxx = $totalxx+$jum_stok_akhir; //echo $totalxx." ";
				
			 //cek stok terakhir tm_stok, dan update stoknya stoknya konversikan balik ke sat awal
				
				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.kode_brg, a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$kode_brg = $hasilnya->kode_brg;
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$totalxx_konvawal = $totalxx/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$totalxx_konvawal = $totalxx*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$totalxx_konvawal = $totalxx-$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$totalxx_konvawal = $totalxx+$angka_faktor_konversi;
						}
						else
							$totalxx_konvawal = $totalxx;
					}
					else
						$totalxx_konvawal = $totalxx;
				}
				else
					$totalxx_konvawal = $totalxx;
					
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok WHERE id_brg = '$id_brg' ");
				if ($query3->num_rows() == 0){						
					$data_stok = array(
							'id_brg'=>$id_brg,
							'stok'=>$totalxx_konvawal,
							'tgl_update_stok'=>$tgl
							);
					$this->db->insert('tm_stok', $data_stok);
				}
				else {
					$hasilrow = $query3->row();
					$id_stok	= $hasilrow->id;
					
					$this->db->query(" UPDATE tm_stok SET stok = '$totalxx_konvawal', tgl_update_stok = '$tgl' 
					where id_brg= '$id_brg' ");
				}
			 // end 13-05-2015 =============================================================
			 
			 // ============== new skrip 06-02-2014 ================================================
			 // skrip SO berdasarkan harga ini DIKOMEN. backupnya ada di web BLNPRODUKSI
			 //-------------- hitung total qty dari detail tiap2 harga -------------------
				
			/*	$qtytotalstokfisik = 0;
				$qtytotal_saldo_akhir = 0;
				for ($xx=0; $xx<count($harga); $xx++) {
					$harga[$xx] = trim($harga[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					// 16-03-2015
					$auto_saldo_akhir_harga[$xx] = trim($auto_saldo_akhir_harga[$xx]);
					$qtytotal_saldo_akhir+= $auto_saldo_akhir_harga[$xx];
					$qtytotalstokfisik+= $stok_fisik[$xx];
				} // end for
			// ---------------------------------------------------------------------
				// 24-03-2014
				$totalxx = $qtytotalstokfisik;
				//16-03-2015
				$totalxx_saldo_akhir = $qtytotal_saldo_akhir;
			
			// ======== update stoknya! =============
			// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// 1.ambil brg masuk hasil pembelian
			$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							WHERE b.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$jum_masuk = $hasilrow->jum_masuk;
				if ($jum_masuk == '')
					$jum_masuk = 0;
				
				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.kode_brg = '$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
						}
						else
							$jum_masuk_konv = $jum_masuk;
					}
					else
						$jum_masuk_konv = $jum_masuk;
				}
				else
					$jum_masuk_konv = $jum_masuk;
			}
			else
				$jum_masuk_konv = 0;
			
			// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE  a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' AND a.tujuan < '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
								
				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' AND a.tujuan > '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
					
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							WHERE a.tgl_bonm  >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
				}
				else
					$jum_masuk_lain = 0;
				
				$totmasuk = $jum_masuk_konv + $jum_masuk_lain;
				$totkeluar = $jum_keluar+$jum_keluar_lain;
												
				$jum_stok_akhir = $totmasuk-$totkeluar;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------

				// tambahkan stok_akhir ini ke qty SO
				//$totalbagusxx = $qtytotalstokfisikbgs + $stok_akhir_bgs; //echo $stok_akhir_bgs." ";
				//$totalperbaikanxx = $qtytotalstokfisikperbaikan + $stok_akhir_perbaikan; //echo $stok_akhir_perbaikan." ";
				$totalxx = $totalxx+$jum_stok_akhir; //echo $totalxx." ";
				
				// 16-03-2015
				$totalxx_saldo_akhir = $totalxx_saldo_akhir + $jum_stok_akhir;
			
				//cek stok terakhir tm_stok, dan update stoknya
				// 04-11-2014, stoknya konversikan balik ke sat awal
				
				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.kode_brg = '$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							//$totalxx_konvawal = $totalxx/$angka_faktor_konversi;
							$totalxx_konvawal = $totalxx_saldo_akhir/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							//$totalxx_konvawal = $totalxx*$angka_faktor_konversi;
							$totalxx_konvawal = $totalxx_saldo_akhir*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							//$totalxx_konvawal = $totalxx-$angka_faktor_konversi;
							$totalxx_konvawal = $totalxx_saldo_akhir-$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							//$totalxx_konvawal = $totalxx+$angka_faktor_konversi;
							$totalxx_konvawal = $totalxx_saldo_akhir+$angka_faktor_konversi;
						}
						else
							//$totalxx_konvawal = $totalxx;
							$totalxx_konvawal = $totalxx_saldo_akhir;
					}
					else
						//$totalxx_konvawal = $totalxx;
						$totalxx_konvawal = $totalxx_saldo_akhir;
				}
				else
					//$totalxx_konvawal = $totalxx;
					$totalxx_konvawal = $totalxx_saldo_akhir;
					
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
					if ($query3->num_rows() == 0){						
						$data_stok = array(
							//'id'=>$id_stok,
							'kode_brg'=>$kode_brg,
							'stok'=>$totalxx_konvawal,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok', $data_stok);
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						
						$this->db->query(" UPDATE tm_stok SET stok = '$totalxx_konvawal', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg' ");
					} // done 04-11-2014
					
				// stok_harga
				// ----------------------------------------------
				for ($xx=0; $xx<count($harga); $xx++) {
					$harga[$xx] = trim($harga[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					//16-03-2015
					$auto_saldo_akhir_harga[$xx] = trim($auto_saldo_akhir_harga[$xx]);
					
					// 24-03-2014
					$totalxx = $stok_fisik[$xx];
					$totalxx_saldo_akhir = $auto_saldo_akhir_harga[$xx];
						
					// ========================= stok per harga ===============================================
					
					// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
					// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
				// 1. ambil brg masuk hasil pembelian
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							WHERE b.tgl_bonm >= '$tglawalplus' AND b.kode_brg = '$kode_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$gudang'
							AND b.harga = '".$harga[$xx]."' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
					
					$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
								a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
								INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.kode_brg = '$kode_brg' ");
					if ($query3->num_rows() > 0){
						$hasilnya = $query3->row();
						$nama_brg = $hasilnya->nama_brg;
						$id_satuan_konversi = $hasilnya->id_satuan_konversi;
						$rumus_konversi = $hasilnya->rumus_konversi;
						$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
						
						if ($id_satuan_konversi != 0) {
							if ($rumus_konversi == '1') {
								$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '2') {
								$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '3') {
								$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '4') {
								$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
							}
							else
								$jum_masuk_konv = $jum_masuk;
						}
						else
							$jum_masuk_konv = $jum_masuk;
					}
					else
						$jum_masuk_konv = $jum_masuk;
				}
				else
					$jum_masuk_konv = 0;
			
				// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							WHERE  a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND a.id_gudang = '$gudang' AND c.harga = '".$harga[$xx]."'
							AND b.is_quilting = 'f' AND a.tujuan < '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
			
				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							WHERE a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND a.id_gudang = '$gudang' AND c.harga = '".$harga[$xx]."'
							AND b.is_quilting = 'f' AND a.tujuan > '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
				
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
							WHERE a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND a.id_gudang = '$gudang' AND c.harga = '".$harga[$xx]."'
							AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
				}
				else
					$jum_masuk_lain = 0;
			
				$totmasuk = $jum_masuk_konv + $jum_masuk_lain;
				$totkeluar = $jum_keluar+$jum_keluar_lain;
												
				$jum_stok_akhir = $totmasuk-$totkeluar;
				$totalxx = $totalxx + $jum_stok_akhir;
				
				// 16-03-2015
				$totalxx_saldo_akhir = $totalxx_saldo_akhir + $jum_stok_akhir;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
				// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
				//$totalbagusxx = $stok_fisik_bgs[$xx] + $stok_akhir_bgs;
				//$totalperbaikanxx = $stok_fisik_perbaikan[$xx] + $stok_akhir_perbaikan;
				//$totalxx = $totalxx + $jum_stok_akhir;
					
					// 04-11-2014
					//cek stok terakhir tm_stok_harga, dan update stoknya.
					// stoknya konversikan balik ke sat awal
				
				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.kode_brg = '$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							//$totalxx_konvawal = $totalxx/$angka_faktor_konversi;
							$totalxx_konvawal = $totalxx_saldo_akhir/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							//$totalxx_konvawal = $totalxx*$angka_faktor_konversi;
							$totalxx_konvawal = $totalxx_saldo_akhir*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							//$totalxx_konvawal = $totalxx-$angka_faktor_konversi;
							$totalxx_konvawal = $totalxx_saldo_akhir-$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							//$totalxx_konvawal = $totalxx+$angka_faktor_konversi;
							$totalxx_konvawal = $totalxx_saldo_akhir+$angka_faktor_konversi;
						}
						else
							//$totalxx_konvawal = $totalxx;
							$totalxx_konvawal = $totalxx_saldo_akhir;
					}
					else
						//$totalxx_konvawal = $totalxx;
						$totalxx_konvawal = $totalxx_saldo_akhir;
				}
				else
					//$totalxx_konvawal = $totalxx;
					$totalxx_konvawal = $totalxx_saldo_akhir;
					
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE harga = '".$harga[$xx]."'
							AND kode_brg='$kode_brg' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_unit_jahit_warna, insert						
						$data_stok = array(
							//'id'=>$id_stok_warna,
							'kode_brg'=>$kode_brg,
							'harga'=>$harga[$xx],
							'stok'=>$totalxx_konvawal,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_harga', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_harga SET stok = '".$totalxx_konvawal."', 
						tgl_update_stok = '$tgl' 
						where harga= '".$harga[$xx]."' AND kode_brg='$kode_brg' ");
					}
					
				} // end for
				*/
				// ----------------------------------------------
			 // ====================================================================================
			 
			 $this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET status_approve = 't'
								where id = '$idnya' ");

		  } // end for
		  
		  // 28-08-2015
		  $uid_update_by = $this->session->userdata('uid');
		  $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET tgl_update = '$tgl', uid_update_by = '$uid_update_by',
								status_approve = 't' where id = '$id_header' ");
		  
		    $data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." di gudang ".$kode_gudang." - ".$nama_gudang." sudah berhasil di-approve, dan otomatis mengupdate stok";
			$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname/vmainform';
			$this->load->view('template',$data);
	  // ============================================= END 01-11-2014 ==========================================
	  
	  
// 01-11-2014, INI DIKOMEN AJA	  
	/*  $ada = 0;
	  // cek di tiap2 bahan bakunya, apakah ada data brg yg blm ada harganya. jika blm ada harga, maka munculkan pesan error
	  // revisi 130212: utk data bhn baku yg blm ada stok harga, harganya diset 0 aja dulu
	  for ($i=1;$i<=$no;$i++) {
		  $kode_brg = $this->input->post('kode_'.$i, TRUE);
		  $id_satuan = $this->input->post('satuan_'.$i, TRUE);
		  $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
		  
		  //cek di tabel tm_harga_brg_supplier
		  //$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE kode_brg = '$kode_brg' ");
		  $query3	= $this->db->query(" SELECT harga FROM tm_stok_harga WHERE kode_brg = '$kode_brg' AND quilting = 'f' ");
			if ($query3->num_rows() == 0){
				//$ada = 0;
				//break;
				// save di tm_stok
				$data_detail = array(
						'kode_brg'=>$kode_brg, 
						'stok'=>$jum_stok,
						'tgl_update_stok'=>$tgl
					);
				$this->db->insert('tm_stok',$data_detail);
				
				// save di tm_stok_harga
				$data_detail = array(
						'kode_brg'=>$kode_brg, 
						'stok'=>$jum_stok,
						'harga'=>'0',
						'tgl_update_stok'=>$tgl
					);
				$this->db->insert('tm_stok_harga',$data_detail);
		   				
			}
			else {
				$ada = 1;
			}
	  }
	  	        
	      for ($i=1;$i<=$no;$i++) {
			 // cek ada selisih ga.
			 $kode_brg = $this->input->post('kode_'.$i, TRUE);
			 $id_satuan = $this->input->post('satuan_'.$i, TRUE);
			 $stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE); // ini sudah dlm hasil konversi (utk satuan awalnya Yard dan Lusin)
			 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
			 $idnya = $this->input->post('id_'.$i, TRUE);
			 			 
			 // balikin lagi ke satuan dasar (dari meter/pcs) jika yard atau lusin
			 if ($id_satuan == 2) { // yard
				$new_stok_reverse = $stok_fisik/0.91; // ini konversi dari meter ke yard
			 }
			 else if ($id_satuan == 7) { // lusin
				$new_stok_reverse = $stok_fisik/12; // ini konversi dari pcs ke lusin
			 }
			 else
				$new_stok_reverse = $stok_fisik;
			 
			 //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 18-02-2012 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			 // update ke tm_stok dan ke tm_stok_harga
			 //cek apakah data stoknya ada
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$stok_lama = 0;
				
					$data_stok = array(
						'kode_brg'=>$kode_brg,
						'stok'=>$new_stok_reverse,
						'tgl_update_stok'=>$tgl
					);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
					
					//update ke tm_stok dan history stoknya, 
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok_reverse', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode_brg' ");
				}
				
				// selisih
				$selisih_reverse = $new_stok_reverse-$stok_lama; // selisih = 11-12
				
				// stok harga
				// 08-12-2012, cek dulu ada brp data stok harga yg tidak 0. trus query perulangan data2 stok harga yg ada berdasarkan kode brg tsb. 
				
				//$inya=1;
				$query2x	= $this->db->query(" SELECT count(id) as jum FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
									AND stok > 0 AND quilting='f' ");
				if ($query2x->num_rows() > 0){
					$hasil2x = $query2x->row();
					$jmldatastokharga = $hasil2x->jum;
				}
				else
					$jmldatastokharga = 0;
				
				$query2x	= $this->db->query(" SELECT sum(stok) as jumstok FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
									AND stok > 0 AND quilting='f' ");
				if ($query2x->num_rows() > 0){
					$hasil2x = $query2x->row();
					$jmlstokharga = $hasil2x->jumstok;
				}
				else
					$jmlstokharga = 0;
				
				$pengurangan = $new_stok_reverse-$jmlstokharga; 
				//if ($jmldatastokharga != 0 && $selisih_reverse !=0)
				if ($jmldatastokharga != 0 && $pengurangan !=0) {
					$bagirata = $new_stok_reverse/$jmldatastokharga;
					$statupdate=1;
				}
				else {
					$bagirata = 0;
					$statupdate=0;
				}

				$query2x	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
									AND stok > 0 AND quilting='f' ORDER BY id ASC ");
				$hasil2x = $query2x->result();
				foreach ($hasil2x as $row2x) {
					$stoknya = $row2x->stok;
					$harganya = $row2x->harga;
					$id_stok_harga = $row2x->id;
					
					if ($statupdate == 0) {
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$stoknya', tgl_update_stok = '$tgl' 
										where id= '$id_stok_harga' ");
						
						// save ke tt_so_bahan_baku_detail_harga
						// 06-12-12
						//cek apakah data stoknya ada
						$query3x	= $this->db->query(" SELECT id FROM tt_so_bahan_baku_detail_harga WHERE 
										id_stok_opname_bahan_baku_detail = '$idnya' AND harga = '$harganya' ");
						if ($query3x->num_rows() == 0){
							$data_stok = array(
								'id_stok_opname_bahan_baku_detail'=>$idnya,
								'harga'=>$harganya,
								'stok'=>$stoknya
							);
							$this->db->insert('tt_so_bahan_baku_detail_harga',$data_stok);
						}
						else {
							$hasil3x = $query3x->row();
							$idsodetailharga = $hasil3x->id; 
							$this->db->query(" UPDATE tt_so_bahan_baku_detail_harga SET stok = '$stoknya'
											where id= '$idsodetailharga' ");
						}
					}
					else {
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$bagirata', tgl_update_stok = '$tgl' 
										where id= '$id_stok_harga' ");
						
						// save ke tt_so_bahan_baku_detail_harga
						// 06-12-12
						//cek apakah data stoknya ada
						$query3x	= $this->db->query(" SELECT id FROM tt_so_bahan_baku_detail_harga WHERE 
										id_stok_opname_bahan_baku_detail = '$idnya' AND harga = '$harganya' ");
						if ($query3x->num_rows() == 0){
							$data_stok = array(
								'id_stok_opname_bahan_baku_detail'=>$idnya,
								'harga'=>$harganya,
								'stok'=>$bagirata
							);
							$this->db->insert('tt_so_bahan_baku_detail_harga',$data_stok);
						}
						else {
							$hasil3x = $query3x->row();
							$idsodetailharga = $hasil3x->id; 
							$this->db->query(" UPDATE tt_so_bahan_baku_detail_harga SET stok = '$bagirata'
											where id= '$idsodetailharga' ");
						}
					}
				}
				
				
				//dan update statusnya di tt_stok_opname_bahan_baku detailnya
				$this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET status_approve = 't'
								where id = '$idnya' "); 
			 //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end 18-02-2012 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			 
		  } // end for
		  $this->db->query(" UPDATE tt_stok_opname_bahan_baku SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  
		  redirect('app-stok-opname/cform'); */
  }

}
