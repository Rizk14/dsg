<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
			$data['page_title_ssp']			= $this->lang->line('page_title_ssp');
			$data['form_nama_wp_ssp']		= $this->lang->line('form_nama_wp_ssp');
			$data['form_kd_akun_pajak_ssp']	= $this->lang->line('form_kd_akun_pajak_ssp');
			$data['form_kd_jns_setor_ssp']	= $this->lang->line('form_kd_jns_setor_ssp');
			$data['form_uraian_pembayaran_spp']	= $this->lang->line('form_uraian_pembayaran_spp');
			$data['form_masa_pajak_ssp']	= $this->lang->line('form_masa_pajak_ssp');
			$data['form_masa_pajak_thn_ssp']= $this->lang->line('form_masa_pajak_thn_ssp');
			$data['form_masa_pajak_bln_ssp']= $this->lang->line('form_masa_pajak_bln_ssp');
			$data['form_jml_pembayaran_ssp']= $this->lang->line('form_jml_pembayaran_ssp');
			$data['form_wjb_pajak_penyetor_ssp']		= $this->lang->line('form_wjb_pajak_penyetor_ssp');
			$data['form_wjb_pajak_penyetor_tempat_ssp']	= $this->lang->line('form_wjb_pajak_penyetor_tempat_ssp');
			$data['form_wjb_pajak_penyetor_tgl_ssp']	= $this->lang->line('form_wjb_pajak_penyetor_tgl_ssp');
			$data['form_wjb_pajak_penyetor_penyetor_ssp']	= $this->lang->line('form_wjb_pajak_penyetor_penyetor_ssp');
			$data['button_simpan']	= $this->lang->line('button_simpan');	
			$data['button_batal']	= $this->lang->line('button_batal');
	
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$this->load->model('ssp/mclass');
			
			$data['isi']	= 'ssp/vform';
			$this->load->view('template',$data);
			
	}

	function listpenyetor() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "PENYETOR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('ssp/mclass');

		$query	= $this->mclass->lpenyetor();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/ssp/cform/listpenyetornext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lpenyetorperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('ssp/vlistpenyetor',$data);
	}

	function listpenyetornext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "PENYETOR";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('ssp/mclass');

		$query	= $this->mclass->lpenyetor();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/ssp/cform/listpenyetornext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lpenyetorperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('ssp/vlistpenyetor',$data);
	}
	
	function listjenissetorpajak() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "JENIS SETOR PAJAK";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('ssp/mclass');

		$query	= $this->mclass->ljnssetorpajak();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/ssp/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ljnssetorpajakperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('ssp/vlistjnssetorpajak',$data);
	}

	function listjenissetorpajaknext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "JENIS SETOR PAJAK";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('ssp/mclass');

		$query	= $this->mclass->ljnssetorpajak();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/ssp/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->ljnssetorpajakperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('ssp/vlistjnssetorpajak',$data);
	}

	function flistjenissetorpajak() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$iterasi	= $this->uri->segment(4,0);
		$ibranch	= $this->uri->segment(5,0);
		$icust	= $this->uri->segment(6,0);
		
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "ORDER PEMBELIAN (OP)";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('do/mclass');

		$query	= $this->mclass->flop($key);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
			
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->op','$row->iproduct','$row->productname','$row->qty','$row->harga','$row->qtyproduk','$row->qty')\">".$row->op."</a></td>	 
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->op','$row->iproduct','$row->productname','$row->qty','$row->harga','$row->qtyproduk','$row->qty')\">".$row->iproduct."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->op','$row->iproduct','$row->productname','$row->qty','$row->harga','$row->qtyproduk','$row->qty')\">".$row->qtyproduk."</a></td>
				  <td><a href=\"javascript:settextfield('$row->op','$row->iproduct','$row->productname','$row->qty','$row->harga','$row->qtyproduk','$row->qty')\">".$row->productname."</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->op','$row->iproduct','$row->productname','$row->qty','$row->harga','$row->qtyproduk','$row->qty')\">".$row->qty."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
		
		echo $item;
	}

	function listinisial() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$data['page_title']	= "NAMA WAJIB PAJAK";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('ssp/mclass');

		$query	= $this->mclass->linisial();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/ssp/cform/listinisial/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->linisialperpages($pagination['per_page'],$pagination['cur_page']);		

		$this->load->view('ssp/vlistinisial',$data);
	}
		
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}	
		$wajibpajak1	= $this->input->post('wajibpajak')?$this->input->post('wajibpajak'):$this->input->get_post('wajibpajak');
		$i_initial1	= $this->input->post('i_initial')?$this->input->post('i_initial'):$this->input->get_post('i_initial');
		$kdakunpajak1	= $this->input->post('kdakunpajak')?$this->input->post('kdakunpajak'):$this->input->get_post('kdakunpajak');
		$kdjnssetor1	= $this->input->post('kdjnssetor')?$this->input->post('kdjnssetor'):$this->input->get_post('kdjnssetor');
		$uraipembayaran1	= $this->input->post('uraipembayaran')?$this->input->post('uraipembayaran'):$this->input->get_post('uraipembayaran');
		$bln1	= $this->input->post('bln')?$this->input->post('bln'):$this->input->get_post('bln');
		$thn1	= $this->input->post('thn')?$this->input->post('thn'):$this->input->get_post('thn');
		$jmlpebayaran1	= $this->input->post('jmlpebayaran')?$this->input->post('jmlpebayaran'):$this->input->get_post('jmlpebayaran');
		$tmptwp1	= $this->input->post('tmptwp')?$this->input->post('tmptwp'):$this->input->get_post('tmptwp');
		$tglwp1	= $this->input->post('tglwp')?$this->input->post('tglwp'):$this->input->get_post('tglwp');
		$penyetor1	= $this->input->post('penyetor')?$this->input->post('penyetor'):$this->input->get_post('penyetor');
		$ipenyetor1	= $this->input->post('ipenyetor')?$this->input->post('ipenyetor'):$this->input->get_post('ipenyetor');
		
		$exptglwp1	= explode("/",$tglwp1,strlen($tglwp1)); // d/m/Y
		$tglwpnew	= $exptglwp1[2].'-'.$exptglwp1[1].'-'.$exptglwp1[0];
		
		if( !empty($wajibpajak1) && 
		    !empty($kdakunpajak1) && 
		    !empty($penyetor1) ) {
			
			$this->load->model('ssp/mclass');
			$this->mclass->msimpan($wajibpajak1,$i_initial1,$kdakunpajak1,$kdjnssetor1,$uraipembayaran1,$bln1,$thn1,$jmlpebayaran1,$tmptwp1,$tglwpnew,$penyetor1,$ipenyetor1);
		} else {
			$data['page_title_ssp']			= $this->lang->line('page_title_ssp');
			$data['form_nama_wp_ssp']		= $this->lang->line('form_nama_wp_ssp');
			$data['form_kd_akun_pajak_ssp']	= $this->lang->line('form_kd_akun_pajak_ssp');
			$data['form_kd_jns_setor_ssp']	= $this->lang->line('form_kd_jns_setor_ssp');
			$data['form_uraian_pembayaran_spp']	= $this->lang->line('form_uraian_pembayaran_spp');
			$data['form_masa_pajak_ssp']	= $this->lang->line('form_masa_pajak_ssp');
			$data['form_masa_pajak_thn_ssp']= $this->lang->line('form_masa_pajak_thn_ssp');
			$data['form_masa_pajak_bln_ssp']= $this->lang->line('form_masa_pajak_bln_ssp');
			$data['form_jml_pembayaran_ssp']= $this->lang->line('form_jml_pembayaran_ssp');
			$data['form_wjb_pajak_penyetor_ssp']		= $this->lang->line('form_wjb_pajak_penyetor_ssp');
			$data['form_wjb_pajak_penyetor_tempat_ssp']	= $this->lang->line('form_wjb_pajak_penyetor_tempat_ssp');
			$data['form_wjb_pajak_penyetor_tgl_ssp']	= $this->lang->line('form_wjb_pajak_penyetor_tgl_ssp');
			$data['form_wjb_pajak_penyetor_penyetor_ssp']	= $this->lang->line('form_wjb_pajak_penyetor_penyetor_ssp');
			$data['button_simpan']	= $this->lang->line('button_simpan');	
			$data['button_batal']	= $this->lang->line('button_batal');
	
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			
			$this->load->model('ssp/mclass');
			
			print "<script>alert(\"Maaf, SSP gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			
			$this->load->view('ssp/vform',$data);
		}
	}
}
?>
