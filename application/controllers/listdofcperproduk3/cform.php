<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('listdofcperproduk/mclass');
	}
	
	function index() {
		
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['disabled']	= 'f';
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi']	= 'listdofcperproduk/vmainform';
			$this->load->view('template',$data);
			
		}	
	
	
	function viewdata() {
 	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$d_first	= $this->input->post('d_op_first');
		$d_last	= $this->input->post('d_op_last');
		
		$data['tglmulai']	= $d_first;
		$data['tglakhir']	= $d_last;
		
		$e_d_do_first	= explode("/",$d_first,strlen($d_first));
		$e_d_do_last	= explode("/",$d_last,strlen($d_last));
		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";

		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;
		
		$data['query']	= $this->mclass->getqtyperproduk($n_d_do_first,$n_d_do_last);
			$data['isi']	= 'listdofcperproduk/vlistform';
			$this->load->view('template',$data);
		
	}


	function viewdata2() {
 	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$d_first	= $this->input->post('d_op_first');
		$d_last		= $this->input->post('d_last');
		
		$data['tglmulai']	= $d_first;
		$data['tglakhir']	= $d_last;
		
		$e_d_do_first	= explode("/",$d_first,strlen($d_first));
		$e_d_do_last	= explode("/",$d_last,strlen($d_last));
		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";

		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;
		
		$data['query']	= $this->mclass->getqtyperproduk($n_d_do_first,$n_d_do_last);
			$data['isi']	= 'listdofcperproduk/vlistform2';
			$this->load->view('template',$data);
		
	}

	function update() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
			$jml				= $this->input->post('jml', TRUE);
			{
				$this->load->model('listdofcperproduk/mclass');
				
				for($j=0;$j<=$jml;$j++){
					  $periode				= $this->input->post('periode'.$j, TRUE);
					  $imotif				= $this->input->post('imotif'.$j, TRUE);
					  $icolor				= $this->input->post('icolor'.$j, TRUE);
					  $productmotif			= $this->input->post('productmotif'.$j, TRUE);
					  $saldo_awal			= $this->input->post('saldo_awal'.$j, TRUE);
					  $this->mclass->update_saldo_awalfc($periode, $imotif, $icolor, $saldo_awal);
					}	
		
			}  
			$data['isi']	= 'listdofcperproduk/vmainform';
			$this->load->view('template',$data);
			
}
}
?>
