<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('bonmmasuklain/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmmasuklain ORDER BY no_bonm DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_bonm;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==9) {
				$nobonm = substr($no_bonm, 0, 9);
				$n_bonm	= (substr($nobonm,4,5))+1;
				$th_bonm	= substr($nobonm,0,4);
				if($th_now==$th_bonm) {
						$jml_n_bonm	= $n_bonm;
						switch(strlen($jml_n_bonm)) {
							case "1": $kodebonm	= "0000".$jml_n_bonm;
							break;
							case "2": $kodebonm	= "000".$jml_n_bonm;
							break;	
							case "3": $kodebonm	= "00".$jml_n_bonm;
							break;
							case "4": $kodebonm	= "0".$jml_n_bonm;
							break;
							case "5": $kodebonm	= $jml_n_bonm;
							break;	
						}
						$nomorbonm = $th_now.$kodebonm;
				}
				else {
					$nomorbonm = $th_now."00001";
				}
			}
			else {
				$nomorbonm	= $th_now."00001";
			}

	$data['isi'] = 'bonmmasuklain/vmainform';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['no_bonm'] = $nomorbonm;
	$this->load->view('template',$data);

  }
  
  function edit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_bonm 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_bonm($id_bonm); 
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['msg'] = '';
	$data['isi'] = 'bonmmasuklain/veditform';
	$this->load->view('template',$data);

  }
  
  function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_bonm 	= $this->input->post('id_bonm', TRUE);
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$jenis 	= $this->input->post('jenis', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");

			//update headernya
			$this->db->query(" UPDATE tm_bonmmasuklain SET tgl_bonm = '$tgl_bonm', jenis_masuk = '$jenis', tgl_update='$tgl',
							no_manual='$no_bonm_manual', 
							keterangan = '$ket' where id= '$id_bonm' ");
							
				//reset stok, dan hapus dulu detailnya
				//============= 25-01-2013 ====================
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasuklain_detail WHERE id_bonmmasuklain = '$id_bonm' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					$tgl = date("Y-m-d");
					foreach ($hasil2 as $row2) {
						// cek dulu satuan brgnya. jika satuannya yard/lusin, maka qty di detail ini konversikan lagi ke yard/lusin
							$query3	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$satuan	= $hasilrow->satuan;
								}
								else {
									$satuan	= '';
								}
								
							/*	if ($satuan == '2') {
									$qty_sat_awal = $row2->qty / 0.90; //meter ke yard 07-03-2012, rubah dari 0.91 ke 0.90
								}
								else if ($satuan == '7') {
									$qty_sat_awal = $row2->qty / 12; // pcs ke lusin
								}
								else
									$qty_sat_awal = $row2->qty; */
						$qty_sat_awal = $row2->qty;
						// ============ update stok =====================
						
						//$nama_tabel_stok = "tm_stok";
						if ($row2->is_quilting == 't')
							$nama_tabel_stok = "tm_stok_hasil_makloon";
						else
							$nama_tabel_stok = "tm_stok";
				
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama-$qty_sat_awal; // berkurang stok karena reset dari bon M masuk lain
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert($nama_tabel_stok, $data_stok);
								}
								else {
									$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' ");
								}
								
								// ========== 25-01-2013 ==============
								$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
								if ($row2->is_quilting == 't')
									$sqlxx.= " kode_brg_quilting = '$row2->kode_brg' ";
								else
									$sqlxx.= " kode_brg = '$row2->kode_brg' ";
								$sqlxx.= " AND quilting = '$row2->is_quilting' ORDER BY id DESC LIMIT 1";
								//echo $sqlxx."<br>";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilrow = $queryxx->row();
									$stok_lama	= $hasilrow->stok;
									$harganya	= $hasilrow->harga;
								}
								else {
									$stok_lama = 0;
									$harganya = 0;
								}
								//echo $harganya." ".$stok_lama." ".$qty_sat_awal."<br><br>";
								$stokreset = $stok_lama-$qty_sat_awal;
								
								if ($row2->is_quilting == 'f')
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg= '$row2->kode_brg' AND harga = '$harganya' "); //
								else
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg_quilting= '$row2->kode_brg' AND harga = '$harganya' "); //
																
								// ========== end 25-01-2013 ===========					
						// ==============================================
					} // end foreach
				} // end reset stok
				//=============================================
				$this->db->delete('tm_bonmmasuklain_detail', array('id_bonmmasuklain' => $id_bonm));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$is_quilting = $this->input->post('is_quilting_'.$i, TRUE);		
						$kode = $this->input->post('kode_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);

						// ========= 26-12-2012 ==============================
						if ($is_quilting == '')
							$is_quilting = 'f';
						
						// cek satuan barangnya, jika yard (qty = meter) maka konversi balik ke yard
						$queryxx	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$kode' ");
						
						if ($queryxx->num_rows() > 0){
							$hasilrow = $queryxx->row();
							$id_satuan	= $hasilrow->satuan;
						}
						else {
							$id_satuan	= '';
						
						}
						//$hasilrow = $queryxx->row();
						//$id_satuan	= $hasilrow->satuan; 
						
					/*	if ($id_satuan == '2') { // jika satuan awalnya yard
							$qty_sat_awal = $qty / 0.90;
							$konv = 't';
						}
						else if ($id_satuan == '7') { // jika satuan awalnya lusin
							$qty_sat_awal = $qty / 12;
							$konv = 't';
						}
						else {
							$qty_sat_awal = $qty;
							$konv = 'f';
						} */
						
						$qty_sat_awal = $qty; 
						
						// ======== update stoknya! =============
						if ($is_quilting == 't')
							$nama_tabel_stok = "tm_stok_hasil_makloon";
						else
							$nama_tabel_stok = "tm_stok";
						
						//cek stok terakhir tm_stok, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$kode' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qty_sat_awal;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$kode,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert($nama_tabel_stok, $data_stok);
							}
							else {
								$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode' ");
							}
							
							// NEW
							//cek stok terakhir tm_stok_harga, ambil harga terbaru aja dan update stoknya
							$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
							if ($is_quilting == 't')
								$sqlxx.= " kode_brg_quilting = '$kode' ";
							else
								$sqlxx.= " kode_brg = '$kode' ";
							$sqlxx.= " AND quilting = '$is_quilting' ";
							$sqlxx.= " ORDER BY id DESC LIMIT 1";
							
							$queryxx	= $this->db->query($sqlxx);
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$id_stok_harga	= $hasilxx->id;
								$stokharganya	= $hasilxx->stok;
								$harga	= $hasilxx->harga;
							/*else {
								$id_stok_harga = 0;
								$stokharganya = 0;
								$harga = 0;
							} */
								
								$new_stok = $stokharganya+$qty_sat_awal;
								//if ($kode == 'QST0014') 
								//	echo $qty_sat_awal." ".$stokharganya."<br>";
								
								if ($is_quilting == 'f')
									$fieldkode = 'kode_brg';
								else
									$fieldkode = 'kode_brg_quilting';
									
								if ($queryxx->num_rows() == 0){ // jika blm ada data di tm_stok_harga, insert
									$data_stok = array(
										$fieldkode=>$kode,
										'stok'=>$new_stok,
										'harga'=>$harga,
										'tgl_update_stok'=>$tgl,
										'quilting'=>$is_quilting
										);
									$this->db->insert('tm_stok_harga',$data_stok);
								}
								else {
									if ($is_quilting == 'f')
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
											where kode_brg= '$kode' AND harga = '$harga' ");
									else
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
											where kode_brg_quilting= '$kode' AND harga = '$harga' ");
								}
							}
					
					// jika semua data tdk kosong, insert ke tm_bonmmasuklain_detail
					$data_detail = array(
						'id_bonmmasuklain'=>$id_bonm,
						'kode_brg'=>$kode,
						'qty'=>$qty,
						'keterangan'=>$ket_detail,
						//'konversi_satuan'=>$konv,
						'is_quilting'=>$is_quilting
					);
					$this->db->insert('tm_bonmmasuklain_detail',$data_detail);
							// ================ end insert item detail ===========
				} // end perulangan
					//die();
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "bonmmasuklain/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "bonmmasuklain/cform/cari/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
				
  }

  function submit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
		}
	  
			$no_bonm 	= $this->input->post('no_bonm', TRUE);
			$tgl_bonm 	= $this->input->post('tgl_bonm', TRUE);  
			$no_bonm_manual 	= $this->input->post('no_bonm_manual', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			$jenis 	= $this->input->post('jenis', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_bonm);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			$cek_data = $this->mmaster->cek_data($no_bonm_manual, $thn1);
			if (count($cek_data) > 0) {
				$data['isi'] = 'bonmmasuklain/vmainform';
				$data['msg'] = "Data no bon M ".$no_bonm_manual." utk tahun $thn1 sudah ada..!";
				
				$th_now	= date("Y");
				$query3	= $this->db->query(" SELECT no_bonm FROM tm_bonmmasuklain ORDER BY no_bonm DESC LIMIT 1 ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) 
					$no_bonm	= $hasilrow->no_bonm;
				else
					$no_bonm = '';
				if(strlen($no_bonm)==9) {
					$nobonm = substr($no_bonm, 0, 9);
					$n_bonm	= (substr($nobonm,4,5))+1;
					$th_bonm	= substr($nobonm,0,4);
					if($th_now==$th_bonm) {
							$jml_n_bonm	= $n_bonm;
							switch(strlen($jml_n_bonm)) {
								case "1": $kodebonm	= "0000".$jml_n_bonm;
								break;
								case "2": $kodebonm	= "000".$jml_n_bonm;
								break;	
								case "3": $kodebonm	= "00".$jml_n_bonm;
								break;
								case "4": $kodebonm	= "0".$jml_n_bonm;
								break;
								case "5": $kodebonm	= $jml_n_bonm;
								break;	
							}
							$nomorbonm = $th_now.$kodebonm;
					}
					else {
						$nomorbonm = $th_now."00001";
					}
				}
				else {
					$nomorbonm	= $th_now."00001";
				}
				$data['list_gudang'] = $this->mmaster->get_gudang();
				$data['no_bonm'] = $nomorbonm;
				$this->load->view('template',$data);
			}
			else {
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->save($no_bonm,$tgl_bonm, $no_bonm_manual, $id_gudang, $jenis, $ket, 
							$this->input->post('is_quilting_'.$i, TRUE),
							$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								$this->input->post('qty_'.$i, TRUE), $this->input->post('stok_'.$i, TRUE),  
								$this->input->post('satuan_'.$i, TRUE), $this->input->post('ket_detail_'.$i, TRUE) );						
				}
				redirect('bonmmasuklain/cform/view');
			}
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'bonmmasuklain/vformview';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/bonmmasuklain/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $date_from, $date_to);						
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
    
    /*if ($keywordcari == '' && ($date_from == '' || $date_to == '')) {
		$date_from 	= $this->uri->segment(4);
		$date_to 	= $this->uri->segment(5);
		$keywordcari = $this->uri->segment(6);
	} */
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(6);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	
	$jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $date_from, $date_to);
    
							$config['base_url'] = base_url().'index.php/bonmmasuklain/cform/cari/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(7), $keywordcari, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'bonmmasuklain/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }

  function show_popup_brg(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$id_gudang	= $this->uri->segment(4);
	$is_quilting 	= $this->uri->segment(5);
	$posisi 	= $this->uri->segment(6);
	
	if ($id_gudang == '' || $is_quilting == '' || $posisi == '') {
		$id_gudang = $this->input->post('id_gudang', TRUE);  
		$is_quilting 	= $this->input->post('is_quilting', TRUE);  
		$posisi 	= $this->input->post('posisi', TRUE);  
	}
	$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' || ($id_gudang == '' && $is_quilting == '' && $posisi == '') ) {
			$id_gudang	= $this->uri->segment(4);
			$is_quilting 	= $this->uri->segment(5);
			$posisi 	= $this->uri->segment(6);
			$keywordcari 	= $this->uri->segment(7);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		//echo $id_gudang." ".$is_quilting." ".$posisi." ".$keywordcari;			  
		$qjum_total = $this->mmaster->get_bahantanpalimit($keywordcari, $id_gudang, $is_quilting);
		
				$config['base_url'] = base_url()."index.php/bonmmasuklain/cform/show_popup_brg/".$id_gudang."/".
							$is_quilting."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(8);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_bahan($config['per_page'],$config['cur_page'], $keywordcari, $id_gudang, $is_quilting);
	$data['jum_total'] = count($qjum_total);
	$data['id_gudang'] = $id_gudang;
	$data['is_quilting'] = $is_quilting;
	$data['posisi'] = $posisi;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	
	$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$id_gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$nama_gudang	= '';
	}
	$data['nama_gudang'] = $nama_gudang;

	$this->load->view('bonmmasuklain/vpopupbrg',$data);
  }
  
  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$carinya 	= $this->uri->segment(9);
    
    $this->mmaster->delete($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "bonmmasuklain/cform/view/index/".$cur_page;
	else
		$url_redirectnya = "bonmmasuklain/cform/cari/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  
  function generate_nomor() {
		//$jenis_brg 	= $this->uri->segment(4);
		$rows = array();
		$rows = $this->mmaster->generate_nomor();
		echo json_encode($rows);

  }  

}
