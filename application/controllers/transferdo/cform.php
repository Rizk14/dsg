<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
			$data['page_title_transfer_do']	= $this->lang->line('page_title_transfer_do');
			$data['form_title_transfer_do']	= $this->lang->line('form_title_transfer_do');
			$data['list_tgl_mulai_do_transfer_do']	= $this->lang->line('list_tgl_mulai_do_transfer_do');
			$data['list_tgl_akhir_do_transfer_do']	= $this->lang->line('list_tgl_akhir_do_transfer_do');
			$data['list_transfer_do_stop_produk']	= $this->lang->line('list_transfer_do_stop_produk');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_transfer']	= $this->lang->line('button_transfer');
			
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['file']		= "";
			$data['tgldomulai']	= "";
			$data['tgldoakhir']	= "";
			$data['disabled']	= "";
			
			$this->load->model('transferdo/mclass');
			
			$data['customertransfer'] = $this->mclass->lcustomertransfer();
			$data['customer'] 		  = $this->mclass->lcustomer();
			
			$data['isi']	= 'transferdo/vmainform';	
			$this->load->view('template',$data);
			
		
	}

	function gexportopvsdo() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title_transfer_do']	= $this->lang->line('page_title_transfer_do');
		$data['form_title_transfer_do']	= $this->lang->line('form_title_transfer_do');
		$data['list_tgl_mulai_do_transfer_do']	= $this->lang->line('list_tgl_mulai_do_transfer_do');
		$data['list_tgl_akhir_do_transfer_do']	= $this->lang->line('list_tgl_akhir_do_transfer_do');
		$data['list_transfer_do_stop_produk']	= $this->lang->line('list_transfer_do_stop_produk');
		
		$data['button_batal']		= $this->lang->line('button_batal');
		$data['button_transfer']	= $this->lang->line('button_transfer');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
		
		$d_do_first	= $this->input->post('d_do_first');
		$d_do_last	= $this->input->post('d_do_last');
		$icustomertransfer	= $this->input->post('customertransfer');
		
		$data['tgldomulai']	= $d_do_first;
		$data['tgldoakhir']	= $d_do_last;
		
		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		
		$tdofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:'0';
		$tdolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:'0';
		
		$periode	= $d_do_first." s.d ".$d_do_last;
		
		$this->load->model('transferdo/mclass');

		$data['customertransfer'] = $this->mclass->lcustomertransfer();
		$data['customer'] 		  = $this->mclass->lcustomer();
		
		if($this->input->post('optformatberkas')=='2') {
		
			$ObjPHPExcel = new PHPExcel();
	
			$qexpopvsdo	= $this->mclass->exptransferdo_new($tdofirst,$tdolast,$icustomertransfer);
			
			if($qexpopvsdo->num_rows()>0) {
			
				$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
				$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
				$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
		
				$ObjPHPExcel->getProperties()
					->setTitle("Transfer DO")
					->setSubject("Transfer DO")
					->setDescription("Transfer DO")
					->setKeywords("Laporan Transfer DO")
					->setCategory("Laporan");
	
				$ObjPHPExcel->setActiveSheetIndex(0);
				
				$ObjPHPExcel->createSheet();
				
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
						
				$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'NODOK,C,7');
				
				$ObjPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'TGLDOK,D');
				$ObjPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'KODELANG,C,11');
				$ObjPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'KODEPROD,C,11');
				$ObjPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'JUMLAH,N,8,0');
				$ObjPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'HARGASAT,N,11,0');
				$ObjPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'NOOP,C,6');
				$ObjPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'WILA,C,7');
				$ObjPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'LANG,C,6');
				$ObjPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
					
				if($qexpopvsdo->num_rows()>0) {
					
					$j	= 2;
					$nomor	= 1;
									
					foreach($qexpopvsdo->result() as $row) {
						
						$ddo	= explode("-",$row->d_do,strlen($row->d_do));
						$tgl	= $ddo[2];
						$bln	= $ddo[1];
						$thn	= $ddo[0];
						$ftgl	= $bln."/".$tgl."/".substr($thn,2,2);
						
						$idokode	= '0'.$row->idocode;
						$iopkode	= '0'.$row->iopcode;
						$kodeproduksi	= substr($row->i_product,0,7);				
						
						$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$j, $idokode, Cell_DataType::TYPE_STRING); 
						$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
						
						$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $ftgl);
						$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->i_customer_transfer);
						$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
						
						$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$j, $row->i_product, Cell_DataType::TYPE_STRING); 
						$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->n_deliver);
						$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->vunitprice);
						$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$j, $iopkode, Cell_DataType::TYPE_STRING); 
						$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
						$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$j, $row->ibranch, Cell_DataType::TYPE_STRING); 
						$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, '');
						$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$j++;																																																							
						$nomor++;			
					}
				}
					
				$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
				
				$files	= "3transfer_do_".$tdofirst."-".$tdolast.".xls";
				$ObjWriter->save("files/".$files);
				
				$f	= substr($files,1,strlen($files)-1);
				$disabled = 'disabled';																		
			}else{
				$f	= '';
				$disabled = '';
			}
			
		}else{
			include_once("phpxbase/Column.class.php");
			include_once("phpxbase/Record.class.php");
			include_once("phpxbase/Table.class.php");
			include_once("phpxbase/WritableTable.class.php");
			
			$fields = array(
				array("NODOK", DBFFIELD_TYPE_CHAR, 7),
				array("TGLDOK", DBFFIELD_TYPE_DATE),
				array("KODELANG", DBFFIELD_TYPE_CHAR, 11),
				array("KODEPROD", DBFFIELD_TYPE_CHAR, 11),
				array("JUMLAH", DBFFIELD_TYPE_NUMERIC, 8, 0),
				array("HARGASAT", DBFFIELD_TYPE_NUMERIC, 11, 0),
				array("NOOP", DBFFIELD_TYPE_CHAR, 6),
				array("WILA", DBFFIELD_TYPE_CHAR, 7),
				array("LANG", DBFFIELD_TYPE_CHAR, 6));
			
			$filenya	=	"3transfer_do_".$tdofirst."-".$tdolast.".dbf";
			$tableNew = XBaseWritableTable::create("files/".$filenya,$fields,false);		

			$qexpopvsdo	= $this->mclass->exptransferdo_new($tdofirst,$tdolast,$icustomertransfer);
			
			if($qexpopvsdo->num_rows()>0) {

				$j	= 2;
				$nomor	= 1;
									
				foreach($qexpopvsdo->result() as $row) {
						
					$ddo	= explode("-",$row->d_do,strlen($row->d_do));
					$tgl	= $ddo[2];
					$bln	= $ddo[1];
					$thn	= $ddo[0];
					
					$mydate	= strtotime($row->d_do);
					
					$idokode	= '0'.$row->idocode;
					$iopkode	= '0'.$row->iopcode;
					$kodeproduksi	= substr($row->i_product,0,7);	
										
					$r =& $tableNew->appendRecord();
					$r->setObjectByName("NODOK",$idokode);
					$r->setObjectByName("TGLDOK",$mydate);
					$r->setObjectByName("KODELANG",$row->i_customer_transfer);
					$r->setObjectByName("KODEPROD",$row->i_product);
					$r->setObjectByName("JUMLAH",$row->n_deliver);
					$r->setObjectByName("HARGASAT",$row->vunitprice);
					$r->setObjectByName("NOOP",$iopkode);
					$r->setObjectByName("WILA",$row->ibranch);
					$r->setObjectByName("LANG",'');
					$tableNew->writeRecord();
				}

				$tableNew->close();
				$files	= $filenya;
				
				$f	= substr($files,1,strlen($files)-1);
				$disabled = 'disabled';							
			}else{
				$f	= '';
				$disabled = '';				
			}
			
		}
		
		$data['file']	= $f;
		$data['disabled']	= $disabled;
		
		$this->load->view('transferdo/vform',$data);
	}
	
	
	
	function gexportopvsdo_old() {
		
		$data['page_title_transfer_do']	= $this->lang->line('page_title_transfer_do');
		$data['form_title_transfer_do']	= $this->lang->line('form_title_transfer_do');
		$data['list_tgl_mulai_do_transfer_do']	= $this->lang->line('list_tgl_mulai_do_transfer_do');
		$data['list_tgl_akhir_do_transfer_do']	= $this->lang->line('list_tgl_akhir_do_transfer_do');
		$data['list_transfer_do_stop_produk']	= $this->lang->line('list_transfer_do_stop_produk');
		
		$data['button_batal']		= $this->lang->line('button_batal');
		$data['button_transfer']	= $this->lang->line('button_transfer');
		
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$data['tgl']	= date("d");
		$data['bln']	= date("m");
		$data['thn']	= date("Y");
		
		$d_do_first	= $this->input->post('d_do_first');
		$d_do_last	= $this->input->post('d_do_last');
		//$icustomer	= $this->input->post('customer');
		$icustomertransfer	= $this->input->post('customertransfer');
		
		/*
		$f_stop_produksi	= $this->input->post('f_stop_produksi')?$this->input->post('f_stop_produksi'):$this->input->get_post('f_stop_produksi');
		$stop	= $this->input->post('stop')?$this->input->post('stop'):$this->input->get_post('stop');
		$stopproduct	= $stop=='1'?"1":"0";
		*/
		
		$data['tgldomulai']	= $d_do_first;
		$data['tgldoakhir']	= $d_do_last;
		
		/*
		$data['sproduksi']	= $f_stop_produksi==1?" checked ":"";
		$data['stop']		= $stop==1?" checked ":"";
		$data['stop2']		= $stop==1?"1":"0";
		*/
		
		$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
		$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
		
		$tdofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:'0';
		$tdolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:'0';
		
		$periode	= $d_do_first." s.d ".$d_do_last;
		
		$this->load->model('transferdo/mclass');

		$data['customertransfer'] = $this->mclass->lcustomertransfer();
		$data['customer'] 		  = $this->mclass->lcustomer();
		
		if($this->input->post('optformatberkas')=='2') {
		
			$ObjPHPExcel = new PHPExcel();
	
			$qexpopvsdo	= $this->mclass->exptransferdo_new($tdofirst,$tdolast,$icustomertransfer);
			
			if($qexpopvsdo->num_rows()>0) {
			
				$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
				$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
				$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
		
				$ObjPHPExcel->getProperties()
					->setTitle("Transfer DO")
					->setSubject("Transfer DO")
					->setDescription("Transfer DO")
					->setKeywords("Laporan Transfer DO")
					->setCategory("Laporan");
	
				$ObjPHPExcel->setActiveSheetIndex(0);
				
				$ObjPHPExcel->createSheet();
				
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
				$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
				
				$ObjPHPExcel->getActiveSheet()->setCellValue('A1', 'NODOK,C,7');
				$ObjPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('B1', 'TGLDOK,D');
				$ObjPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('C1', 'KODELANG,C,11');
				$ObjPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('D1', 'KODEPROD,C,11');
				$ObjPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('E1', 'JUMLAH,N,8,0');
				$ObjPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('F1', 'HARGASAT,N,11,0');
				$ObjPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('G1', 'NOOP,C,6');
				$ObjPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('H1', 'WILA,C,7');
				$ObjPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
	
				$ObjPHPExcel->getActiveSheet()->setCellValue('I1', 'LANG,C,6');
				$ObjPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'bold'  => false,
							'italic'=> false,
							'size'  => 10
							)
						)
					);
					
				if($qexpopvsdo->num_rows()>0) {
					
					$j	= 2;
					$nomor	= 1;
									
					foreach($qexpopvsdo->result() as $row) {
						
						$ddo	= explode("-",$row->d_do,strlen($row->d_do)); // Y-m-d
						$tgl	= $ddo[2];
						$bln	= $ddo[1];
						$thn	= $ddo[0];
						$ftgl	= $bln."/".$tgl."/".substr($thn,2,2);
						
						/* 03012012
						$qharga0 = $this->mclass->detailhargaperpelanggan0($row->i_product,$icustomertransfer);
						$qharga1 = $this->mclass->detailhargaperpelanggan1($row->i_product);
						$qharga2 = $this->mclass->detailharga($row->i_product);
						
						if($qharga0->num_rows()>0) {
							$hjp	= $qharga0->row();
						}elseif($qharga1->num_rows()>0) {
							$hjp	= $qharga1->row();
						}else{
							$hjp	= $qharga2->row();
						}
						*/
						
						$idokode	= '0'.$row->idocode;
						$iopkode	= '0'.$row->iopcode;
						$kodeproduksi	= substr($row->i_product,0,7);				
						//$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $idokode);
						$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$j, $idokode, Cell_DataType::TYPE_STRING); 
						$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
						
						//$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$j, $ftgl, Cell_DataType::TYPE_STRING); 
						$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $ftgl);
						$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->i_customer_transfer);
						$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						//$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $kodeproduksi);
						$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$j, $row->i_product, Cell_DataType::TYPE_STRING); 
						$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->n_deliver);
						$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						//$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $hjp->unitprice);
						$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->vunitprice);
						$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						//$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $iopkode);
						$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$j, $iopkode, Cell_DataType::TYPE_STRING); 
						$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
						$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$j, $row->ibranch, Cell_DataType::TYPE_STRING); 
						//$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->ibranch);
						$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, '');
						$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
							array(
								'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 9
								)
							)
						);
	
						$j++;																																																							
						$nomor++;			
					}					
				}
					
				$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
				
				$files	= "3transfer_do_".$tdofirst."-".$tdolast.".xls";
				$ObjWriter->save("files/".$files);
				
				$f	= substr($files,1,strlen($files)-1);
				$disabled = 'disabled';																		
			} else {
				$f	= '';
				$disabled = '';
			}
		}else{
			require_once("phpxbase/Column.class.php");
			require_once("phpxbase/Record.class.php");
			require_once("phpxbase/Table.class.php");
			require_once("phpxbase/WritableTable.class.php");
			
			/*
			$data['page_title_transfer_do']	= $this->lang->line('page_title_transfer_do');
			$data['form_title_transfer_do']	= $this->lang->line('form_title_transfer_do');
			$data['list_tgl_mulai_do_transfer_do']	= $this->lang->line('list_tgl_mulai_do_transfer_do');
			$data['list_tgl_akhir_do_transfer_do']	= $this->lang->line('list_tgl_akhir_do_transfer_do');
			$data['list_transfer_do_stop_produk']	= $this->lang->line('list_transfer_do_stop_produk');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_transfer']	= $this->lang->line('button_transfer');
			
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['lopvsdo']	= "";
			
			$data['tgl']	= date("d");
			$data['bln']	= date("m");
			$data['thn']	= date("Y");
				
			$d_do_first	= $this->input->post('d_do_first');
			$d_do_last	= $this->input->post('d_do_last');
			$icustomertransfer	= $this->input->post('customertransfer');
		
			$data['tgldomulai']	= $d_do_first;
			$data['tgldoakhir']	= $d_do_last;

			$e_d_do_first	= explode("/",$d_do_first,strlen($d_do_first));
			$e_d_do_last	= explode("/",$d_do_last,strlen($d_do_last));
			
			$tdofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:'0';
			$tdolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:'0';
			
			$periode	= $d_do_first." s.d ".$d_do_last;
			
			$this->load->model('transferdo/mclass');

			$data['customertransfer'] = $this->mclass->lcustomertransfer();
			$data['customer'] 		  = $this->mclass->lcustomer();
			*/
								
			// definisikan field
			/*
			$fields = array(
				array("NODOK,C,7", DBFFIELD_TYPE_CHAR, 6),
				array("TGLDOK,D", DBFFIELD_TYPE_DATE),
				array("KODELANG,C,11", DBFFIELD_TYPE_CHAR, 5),
				array("KODEPROD,C,11", DBFFIELD_TYPE_CHAR, 9),
				array("JUMLAH,N,8,0", DBFFIELD_TYPE_NUMERIC, 5, 0),
				array("HARGASAT,N,11,0", DBFFIELD_TYPE_NUMERIC, 15, 0),
				array("NOOP,C,6", DBFFIELD_TYPE_CHAR, 6),
				array("WILA,C,7", DBFFIELD_TYPE_CHAR, 2),
				array("LANG,C,6", DBFFIELD_TYPE_CHAR, 10)
			);	
			*/
			$fields = array(
				array("NODOK", DBFFIELD_TYPE_CHAR, 7),
				array("TGLDOK", DBFFIELD_TYPE_DATE),
				array("KODELANG", DBFFIELD_TYPE_CHAR, 11),
				array("KODEPROD", DBFFIELD_TYPE_CHAR, 11),
				array("JUMLAH", DBFFIELD_TYPE_NUMERIC, 8, 0),
				array("HARGASAT", DBFFIELD_TYPE_NUMERIC, 11, 0),
				//array("HARGASAT", DBFFIELD_TYPE_CHAR, 11, 0),
				array("NOOP", DBFFIELD_TYPE_CHAR, 6),
				array("WILA", DBFFIELD_TYPE_CHAR, 7),
				array("LANG", DBFFIELD_TYPE_CHAR, 6)
			);
						
			// buat tabel baru 
			$filenya	=	"3transfer_do_".$tdofirst."-".$tdolast.".dbf";
			$tableNew = XBaseWritableTable::create("files/".$filenya,$fields,false);		

			$qexpopvsdo	= $this->mclass->exptransferdo_new($tdofirst,$tdolast,$icustomertransfer);
			if($qexpopvsdo->num_rows() > 0) {

				$j	= 2;
				$nomor	= 1;
									
				foreach($qexpopvsdo->result() as $row) {
						
					$ddo	= explode("-",$row->d_do,strlen($row->d_do)); // Y-m-d
					$tgl	= $ddo[2];
					$bln	= $ddo[1];
					$thn	= $ddo[0];
					//$ftgl	= $bln."/".$tgl."/".substr($thn,2,2);
					$mydate	= strtotime($row->d_do);
					
					/* 03012012
					$qharga0 = $this->mclass->detailhargaperpelanggan0($row->i_product,$icustomertransfer);
					$qharga1 = $this->mclass->detailhargaperpelanggan1($row->i_product);
					$qharga2 = $this->mclass->detailharga($row->i_product);
					
					if($qharga0->num_rows()>0) {
						$hjp	= $qharga0->row();
					}elseif($qharga1->num_rows()>0) {
						$hjp	= $qharga1->row();
					}else{
						$hjp	= $qharga2->row();
					}
					*/
					
					$idokode	= '0'.$row->idocode;
					$iopkode	= '0'.$row->iopcode;
					$kodeproduksi	= substr($row->i_product,0,7);	
					
					//$tostring	= (string) $hjp->unitprice;
										
					// masukkan data
					$r =& $tableNew->appendRecord();
					$r->setObjectByName("NODOK",$idokode);
					//$r->setObjectByName("TGLDOK",$ftgl);
					$r->setObjectByName("TGLDOK",$mydate);
					$r->setObjectByName("KODELANG",$row->i_customer_transfer);
					$r->setObjectByName("KODEPROD",$row->i_product);
					$r->setObjectByName("JUMLAH",$row->n_deliver);
					//$r->setObjectByName("HARGASAT",$hjp->unitprice);
					$r->setObjectByName("HARGASAT",$row->vunitprice);
					$r->setObjectByName("NOOP",$iopkode);
					$r->setObjectByName("WILA",$row->ibranch);
					$r->setObjectByName("LANG",'');
					$tableNew->writeRecord();
					
				} // End For Loop

				// tutup tabel
				$tableNew->close();
				$files	= $filenya;
				
				$f	= substr($files,1,strlen($files)-1);
				$disabled = 'disabled';							
			}else{
				$f	= '';
				$disabled = '';				
			}
		}
		
		$data['file']	= $f;
		$data['disabled']	= $disabled;
		
		$this->load->view('transferdo/vform',$data);
	}
}
?>
