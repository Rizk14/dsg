<?php
/*
v = variables
*/
class Cform extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title_bmm']	= $this->lang->line('page_title_bmm');
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined'] = "";
		$data['limages']	= base_url();
		$data['form_title_detail_bmm']	= $this->lang->line('form_title_detail_bmm');
		$data['form_nomor_bmm']		= $this->lang->line('form_nomor_bmm');
		$data['form_tanggal_bmm']	= $this->lang->line('form_tanggal_bmm');
		$data['form_sj_bmm']		= $this->lang->line('form_sj_bmm');
		$data['form_title_detail_bmm']	= $this->lang->line('form_title_detail_bmm');
		$data['form_kode_produk_bmm']	= $this->lang->line('form_kode_produk_bmm');
		$data['form_nm_produk_bmm']		= $this->lang->line('form_nm_produk_bmm');
		$data['form_jml_produk_bmm']	= $this->lang->line('form_jml_produk_bmm');
		$data['button_simpan']	= $this->lang->line('button_simpan');
		$data['button_batal']	= $this->lang->line('button_batal');
		$tahun	= date("Y");

		$tgl	= date("d");
		$bln	= date("m");
		$thn	= date("Y");

		$data['dateTime']	= date("m/d/Y", time());
		$data['tbonmmasuk']	= $tgl . "/" . $bln . "/" . $thn;
		$data['dbonmmasuk'] = $thn . "-" . $bln . "-" . $tgl;

		$this->load->model('bonmmasukgd/mclass');

		$qthn	= $this->mclass->get_thnbonmmasuk();
		$qbonmmasuk	= $this->mclass->get_nomorbonmmasuk();

		if ($qthn->num_rows() > 0) {
			$th		= $qthn->row_array();
			$thn	= $th['thn'];
		} else {
			$thn	= $tahun;
		}

		if ($thn == $tahun) {
			if ($qbonmmasuk->num_rows() > 0) {
				$row	= $qbonmmasuk->row_array();
				$ibonmmasuk		= $row['icode'] + 1;

				switch (strlen($ibonmmasuk)) {
					case "1":
						$nomorbonmmsuk	= "0000" . $ibonmmasuk;
						break;
					case "2":
						$nomorbonmmsuk	= "000" . $ibonmmasuk;
						break;
					case "3":
						$nomorbonmmsuk	= "00" . $ibonmmasuk;
						break;
					case "4":
						$nomorbonmmsuk	= "0" . $ibonmmasuk;
						break;
					case "5":
						$nomorbonmmsuk	= $ibonmmasuk;
						break;
				}
			} else {
				$nomorbonmmsuk		= "00001";
			}
			$nomor	= $tahun . $nomorbonmmsuk;
		} else {
			$nomor	= $tahun . "00001";
		}
		$data['no']	= $nomor;

		$data['isi']	= 'bonmmasukgd/vmainform';
		$this->load->view('template', $data);
	}


	function listbarangjadi()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iterasi = $this->uri->segment(4, 0);
		$dinbonm = $this->uri->segment(5, 0);

		$expdbonmmasuk	= explode("-", $dinbonm, strlen($dinbonm));

		$bulan	= $expdbonmmasuk[1];
		$tahun	= $expdbonmmasuk[0];

		$data['dinbonm']	= $dinbonm;
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('bonmmasukgd/mclass');

		$query	= $this->mclass->lbarangjadi($bulan, $tahun);
		$jml	= $query->num_rows();
		if ($jml == 0) {
			$query	= $this->mclass->lbarangjadiopsi($bulan, $tahun);
			$jml	= $query->num_rows();
		}
		$result	= $query->result();

		$pagination['base_url'] = base_url() . 'index.php/bonmmasukgd/cform/listbarangjadinext/' . $iterasi . '/' . $dinbonm . '/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6, 0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']		= $this->mclass->lbarangjadiperpages($pagination['per_page'], $pagination['cur_page'], $bulan, $tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpagesopsi($pagination['per_page'], $pagination['cur_page'], $bulan, $tahun);
		$this->load->view('bonmmasukgd/vlistformbrgjadi', $data);
	}

	function listbarangjadinext()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iterasi = $this->uri->segment(4, 0);
		$dinbonm = $this->uri->segment(5, 0);

		$expdbonmmasuk	= explode("-", $dinbonm, strlen($dinbonm));
		$bulan	= $expdbonmmasuk[1];
		$tahun	= $expdbonmmasuk[0];

		$data['dinbonm']	= $dinbonm;
		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('bonmmasukgd/mclass');

		$query	= $this->mclass->lbarangjadi($bulan, $tahun);
		$jml	= $query->num_rows();
		if ($jml == 0) {
			$query	= $this->mclass->lbarangjadiopsi($bulan, $tahun);
			$jml	= $query->num_rows();
		}
		$result	= $query->result();

		$pagination['base_url'] 	= base_url() . 'index.php/bonmmasukgd/cform/listbarangjadinext/' . $iterasi . '/' . $dinbonm . '/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6, 0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'], $pagination['cur_page'], $bulan, $tahun);
		$data['isiopsi']	= $this->mclass->lbarangjadiperpagesopsi($pagination['per_page'], $pagination['cur_page'], $bulan, $tahun);

		$this->load->view('bonmmasukgd/vlistformbrgjadi', $data);
	}

	function flistbarangjadi()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key') ? $this->input->post('key') : $this->input->get_post('key');
		$dinbonm = $this->input->post('dinbonm') ? $this->input->post('dinbonm') : $this->input->get_post('dinbonm');

		$expdbonmmasuk	= explode("-", $dinbonm, strlen($dinbonm));
		$bulan	= $expdbonmmasuk[1];
		$tahun	= $expdbonmmasuk[0];

		$iterasi = $this->uri->segment(4, 0);

		$data['iterasi']	= $iterasi;
		$data['page_title']	= "BARANG JADI + MOTIF";
		$data['isi']		= "";
		$data['lurl']		= base_url();

		$this->load->model('bonmmasukgd/mclass');

		$query	= $this->mclass->flbarangjadi($key, $bulan, $tahun);
		$jml	= $query->num_rows();
		if ($jml == 0) {
			$query	= $this->mclass->flbarangjadiopsi($key, $bulan, $tahun);
			$jml	= $query->num_rows();
		}

		$list	= "";
		if ($jml > 0) {
			$cc	= 1;
			$db2 = $this->load->database('db_external', TRUE);
			foreach ($query->result() as $row) {

				//--------------08-01-2014 ----------------------------
				// ambil data warna dari tr_product_color
				$sqlxx	= $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b 
										 WHERE a.i_color=b.i_color AND a.i_product_motif = '$row->imotif' ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0) {
					$hasilxx = $sqlxx->result();

					foreach ($hasilxx as $rownya) {
						$listwarna .= $rownya->e_color_name . "<br>";
					}
				}

				//-----------------------------------------------------

				$list .= "
				 <tr>
				  <td width=\"2px;\">" . $cc . "</td>
				  <td width=\"90px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->stp','$row->iso')\">" . $row->imotif . "</a></td>	 
				  <td><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->stp','$row->iso')\">" . $row->motifname . "</a></td>
				  <td width=\"40px;\"><a href=\"javascript:settextfield('$row->imotif','$row->motifname','$row->qty','$row->stp','$row->iso')\">" . $row->qty . "</a></td>
				  <td>" . $listwarna . "</td>
				 </tr>";
				$cc += 1;
			}
		}

		$item	=
			"<table class=\"listtable2\">
		<tbody>" .
			$list
			. "</tbody>
		</table>";

		echo $item;
	}

	function simpan()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iteration	= $this->input->post('iteration') ? $this->input->post('iteration') : $this->input->get_post('iteration');
		$iterasi	= $iteration;
		$i_inbonm	= $this->input->post('i_inbonm');
		$d_inbonm	= $this->input->post('d_inbonm');
		$i_sj		= $this->input->post('i_sj');

		$exp_d_inbonm	= explode("/", $d_inbonm, strlen($d_inbonm)); // dd/mm/YYYY
		$dinbonm		= $exp_d_inbonm[2] . "-" . $exp_d_inbonm[1] . "-" . $exp_d_inbonm[0];

		if ($exp_d_inbonm[2] == '' || $exp_d_inbonm[1] == '' || $exp_d_inbonm[0] == '')
			$dinbonm	= date("Y-m-d");

		$i_product	= array();
		$e_product_name	= array();
		//$n_count_product= array();
		$qty_product	= array();
		$f_stp	= array();
		$iso	= array();

		$i_product_0	= $this->input->post('i_product_' . 'tblItem' . '_' . '0');

		for ($cacah = 0; $cacah <= $iterasi; $cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_' . 'tblItem' . '_' . $cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_' . 'tblItem' . '_' . $cacah);
			//$n_count_product[$cacah]	= $this->input->post('n_count_product_'.'tblItem'.'_'.$cacah);

			$qty_warna[$cacah]	= $this->input->post('qty_warna_' . $cacah);
			$i_product_color[$cacah]	= $this->input->post('i_product_color_' . $cacah);
			$i_color[$cacah]	= $this->input->post('i_color_' . $cacah);

			$qty_product[$cacah]	= $this->input->post('qty_product_' . 'tblItem' . '_' . $cacah);
			$f_stp[$cacah]	= $this->input->post('f_stp_' . 'tblItem' . '_' . $cacah);
			$iso[$cacah]	= $this->input->post('iso_' . 'tblItem' . '_' . $cacah);
		}

		$this->load->model('bonmmasukgd/mclass');

		if (
			!empty($i_inbonm) &&
			!empty($d_inbonm)
		) {
			if (!empty($i_product_0)) {

				$qcekbonmmasuk	= $this->mclass->caribonmmasuk($i_inbonm);

				if (($qcekbonmmasuk->num_rows() < 0) || ($qcekbonmmasuk->num_rows() == 0)) {
					// $n_count_product
					$this->mclass->msimpan(
						$i_inbonm,
						$dinbonm,
						$i_product,
						$e_product_name,
						$qty_warna,
						$i_product_color,
						$i_color,
						$qty_product,
						$iterasi,
						$f_stp,
						$iso,
						$i_sj
					);
				} else {
					print "<script>alert(\"Maaf, Data Bon M Masuk gagal disimpan.\");
					window.open(\"index\", \"_self\");</script>";
					//show(\"bonmmasukgd/cform\",\"#content\");</script>";
				}
			} else {
				print "<script>alert(\"Maaf, item utk Bon M Masuk harus terisi. Terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
			}
		} else {
			$data['page_title_bmm']	= $this->lang->line('page_title_bmm');
			$data['detail']			= "";
			$data['list']			= "";
			$data['not_defined']	= "";
			$data['limages']		= base_url();
			$data['form_title_detail_bmm']	= $this->lang->line('form_title_detail_bmm');
			$data['form_nomor_bmm']	= $this->lang->line('form_nomor_bmm');
			$data['form_tanggal_bmm']	= $this->lang->line('form_tanggal_bmm');
			$data['form_sj_bmm']		= $this->lang->line('form_sj_bmm');
			$data['form_title_detail_bmm']	= $this->lang->line('form_title_detail_bmm');
			$data['form_kode_produk_bmm']	= $this->lang->line('form_kode_produk_bmm');
			$data['form_nm_produk_bmm']	= $this->lang->line('form_nm_produk_bmm');
			$data['form_jml_produk_bmm']	= $this->lang->line('form_jml_produk_bmm');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$tahun	= date("Y");

			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");

			$data['dateTime']	= date("m/d/Y", time());
			$data['tbonmmasuk']	= $tgl . "/" . $bln . "/" . $thn;

			$qthn	= $this->mclass->get_thnbonmmasuk();
			$qbonmmasuk	= $this->mclass->get_nomorbonmmasuk();

			if ($qthn->num_rows() > 0) {
				$th		= $qthn->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}

			if ($thn == $tahun) {

				if ($qbonmmasuk->num_rows() > 0) {

					$row	= $qbonmmasuk->row_array();
					$ibonmmasuk		= $row['icode'] + 1;

					switch (strlen($ibonmmasuk)) {
						case "1":
							$nomorbonmmsuk	= "0000" . $ibonmmasuk;
							break;
						case "2":
							$nomorbonmmsuk	= "000" . $ibonmmasuk;
							break;
						case "3":
							$nomorbonmmsuk	= "00" . $ibonmmasuk;
							break;
						case "4":
							$nomorbonmmsuk	= "0" . $ibonmmasuk;
							break;
						case "5":
							$nomorbonmmsuk	= $ibonmmasuk;
							break;
					}
				} else {
					$nomorbonmmsuk		= "00001";
				}
				$nomor	= $tahun . $nomorbonmmsuk;
			} else {
				$nomor	= $tahun . "00001";
			}
			$data['no']	= $nomor;

			print "<script>alert(\"Maaf, Data Bon M Masuk gagal disimpan. Terimakasih.\");
			window.open(\"index\", \"_self\");</script>";

			$this->load->view('bonmmasukgd/vmainform', $data);
		}
	}

	function simpan_old()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iteration	= $this->input->post('iteration') ? $this->input->post('iteration') : $this->input->get_post('iteration');
		$iterasi	= $iteration;
		$i_inbonm	= $this->input->post('i_inbonm') ? $this->input->post('i_inbonm') : $this->input->get_post('i_inbonm');
		$d_inbonm	= $this->input->post('d_inbonm') ? $this->input->post('d_inbonm') : $this->input->get_post('d_inbonm');
		$i_sj		= $this->input->post('i_sj') ? $this->input->post('i_sj') : $this->input->get_post('i_sj');

		$exp_d_inbonm	= explode("/", $d_inbonm, strlen($d_inbonm)); // dd/mm/YYYY
		$dinbonm		= $exp_d_inbonm[2] . "-" . $exp_d_inbonm[1] . "-" . $exp_d_inbonm[0];

		$i_product	= array();
		$e_product_name	= array();
		$n_count_product = array();
		$qty_product	= array();
		$f_stp	= array();
		$iso	= array();

		$i_product_0	= $this->input->post('i_product_' . 'tblItem' . '_' . '0');

		for ($cacah = 0; $cacah <= $iterasi; $cacah++) {
			$i_product[$cacah]	= $this->input->post('i_product_' . 'tblItem' . '_' . $cacah) ? $this->input->post('i_product_' . 'tblItem' . '_' . $cacah) : $this->input->get_post('i_product_' . 'tblItem' . '_' . $cacah);
			$e_product_name[$cacah]	= $this->input->post('e_product_name_' . 'tblItem' . '_' . $cacah) ? $this->input->post('e_product_name_' . 'tblItem' . '_' . $cacah) : $this->input->get_post('e_product_name_' . 'tblItem' . '_' . $cacah);
			$n_count_product[$cacah]	= $this->input->post('n_count_product_' . 'tblItem' . '_' . $cacah) ? $this->input->post('n_count_product_' . 'tblItem' . '_' . $cacah) : $this->input->get_post('n_count_product_' . 'tblItem' . '_' . $cacah);
			$qty_product[$cacah]	= $this->input->post('qty_product_' . 'tblItem' . '_' . $cacah) ? $this->input->post('qty_product_' . 'tblItem' . '_' . $cacah) : $this->input->get_post('qty_product_' . 'tblItem' . '_' . $cacah);
			$f_stp[$cacah]	= $this->input->post('f_stp_' . 'tblItem' . '_' . $cacah) ? $this->input->post('f_stp_' . 'tblItem' . '_' . $cacah) : $this->input->get_post('f_stp_' . 'tblItem' . '_' . $cacah);
			$iso[$cacah]	= $this->input->post('iso_' . 'tblItem' . '_' . $cacah) ? $this->input->post('iso_' . 'tblItem' . '_' . $cacah) : $this->input->get_post('iso_' . 'tblItem' . '_' . $cacah);
		}

		if (
			!empty($i_inbonm) &&
			!empty($d_inbonm)
		) {
			if (!empty($i_product_0)) {
				$this->load->model('bonmmasukgd/mclass');
				$qcekbonmmasuk	= $this->mclass->caribonmmasuk($i_inbonm);
				if (($qcekbonmmasuk->num_rows() < 0) || ($qcekbonmmasuk->num_rows() == 0)) {
					$this->mclass->msimpan($i_inbonm, $dinbonm, $i_product, $e_product_name, $n_count_product, $qty_product, $iterasi, $f_stp, $iso, $i_sj);
				} else {
					print "<script>alert(\"Maaf, Data Bon M Masuk gagal disimpan.\");
					window.open(\"index\", \"_self\");</script>";
				}
			} else {
				print "<script>alert(\"Maaf, item utk Bon M Masuk harus terisi. Terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
			}
		} else {
			$data['page_title_bmm']	= $this->lang->line('page_title_bmm');
			$data['detail']			= "";
			$data['list']			= "";
			$data['not_defined']	= "";
			$data['limages']		= base_url();
			$data['form_title_detail_bmm']	= $this->lang->line('form_title_detail_bmm');
			$data['form_nomor_bmm']	= $this->lang->line('form_nomor_bmm');
			$data['form_tanggal_bmm']	= $this->lang->line('form_tanggal_bmm');
			$data['form_sj_bmm']		= $this->lang->line('form_sj_bmm');
			$data['form_title_detail_bmm']	= $this->lang->line('form_title_detail_bmm');
			$data['form_kode_produk_bmm']	= $this->lang->line('form_kode_produk_bmm');
			$data['form_nm_produk_bmm']	= $this->lang->line('form_nm_produk_bmm');
			$data['form_jml_produk_bmm']	= $this->lang->line('form_jml_produk_bmm');
			$data['button_simpan']	= $this->lang->line('button_simpan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$tahun	= date("Y");

			$tgl	= date("d");
			$bln	= date("m");
			$thn	= date("Y");

			$data['dateTime']	= date("m/d/Y", time());
			$data['tbonmmasuk']	= $tgl . "/" . $bln . "/" . $thn;

			$this->load->model('bonmmasukgd/mclass');

			$qthn	= $this->mclass->get_thnbonmmasuk();
			$qbonmmasuk	= $this->mclass->get_nomorbonmmasuk();

			if ($qthn->num_rows() > 0) {
				$th		= $qthn->row_array();
				$thn	= $th['thn'];
			} else {
				$thn	= $tahun;
			}

			if ($thn == $tahun) {
				if ($qbonmmasuk->num_rows() > 0) {
					$row	= $qbonmmasuk->row_array();
					$ibonmmasuk		= $row['icode'] + 1;

					switch (strlen($ibonmmasuk)) {
						case "1":
							$nomorbonmmsuk	= "0000" . $ibonmmasuk;
							break;
						case "2":
							$nomorbonmmsuk	= "000" . $ibonmmasuk;
							break;
						case "3":
							$nomorbonmmsuk	= "00" . $ibonmmasuk;
							break;
						case "4":
							$nomorbonmmsuk	= "0" . $ibonmmasuk;
							break;
						case "5":
							$nomorbonmmsuk	= $ibonmmasuk;
							break;
					}
				} else {
					$nomorbonmmsuk		= "00001";
				}
				$nomor	= $tahun . $nomorbonmmsuk;
			} else {
				$nomor	= $tahun . "00001";
			}
			$data['no']	= $nomor;

			print "<script>alert(\"Maaf, Data Bon M Masuk gagal disimpan. Terimakasih.\");
			window.open(\"index\", \"_self\");</script>";

			$this->load->view('bonmmasukgd/vmainform', $data);
		}
	}

	function caribonmmasuk()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$nbonmmasuk	= $this->input->post('nbonmmasuk') ? $this->input->post('nbonmmasuk') : $this->input->get_post('nbonmmasuk');
		$this->load->model('bonmmasukgd/mclass');
		$qndo	= $this->mclass->caribonmmasuk($nbonmmasuk);
		if ($qndo->num_rows() > 0) {
			echo "Maaf, No. Bon M sdh ada!";
		}
	}

	// 07-01-2014
	function additemwarna()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$db2 = $this->load->database('db_external', TRUE);
		$iproductmotif 	= $this->input->post('iproductmotif', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);

		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
									WHERE a.i_color = b.i_color AND a.i_product_motif = '" . $iproductmotif . "' ");
		if ($queryxx->num_rows() > 0) {
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(
					'i_product_color' => $rowxx->i_product_color,
					'i_color' => $rowxx->i_color,
					'e_color_name' => $rowxx->e_color_name
				);
			}
		} else
			$detailwarna = '';

		$data['detailwarna'] = $detailwarna;
		$data['iproductmotif'] = $iproductmotif;
		$data['posisi'] = $posisi;
		$this->load->view('bonmmasukgd/vlistwarna', $data);
		//print_r($detailwarna); die();
		//echo $iproductmotif; die();
		return true;
	}

	// 06-10-2014. generate data2 SO per warna berdasarkan SO terakhir
	function generate_so_warna()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$db2 = $this->load->database('db_external', TRUE);
		$queryso = $db2->query(" SELECT a.i_so, b.i_so_item, b.i_product FROM tm_stokopname a INNER JOIN 
							tm_stokopname_item b ON a.i_so = b.i_so
							WHERE a.i_status_so = '0' ORDER BY a.i_so 
						 ");
		if ($queryso->num_rows() > 0) {
			$hasilso = $queryso->result();

			foreach ($hasilso as $rowso) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a INNER JOIN tr_color b
											on a.i_color = b.i_color WHERE a.i_product_motif = '" . $rowso->i_product . "' ");
				if ($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$data_sowarna = array(
							'i_so_item' => $rowso->i_so_item,
							'i_color' => $rowxx->i_color,
							'n_quantity_awal' => 0,
							'n_quantity_akhir' => 0,
							'e_note' => ''
						);
						$db2->insert('tm_stokopname_item_color', $data_sowarna);

						/*$detailwarna[] = array(	'i_product_color'=> $rowxx->i_product_color,
												'i_color'=> $rowxx->i_color,
												'e_color_name'=> $rowxx->e_color_name
											); */
					} // end foreach2
				} // end if2
			} // end foreach1
		}
	}
}
