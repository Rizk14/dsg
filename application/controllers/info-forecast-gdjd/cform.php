<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-forecast-gdjd/mmaster');
  }
  function wip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mmaster->get_gudang_jd();
	$data['isi'] = 'info-forecast-gdjd/vmainformwip';
	$this->load->view('template',$data);

  }
  
  function viewwip(){ // modif 12-11-2015
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$jenis_lap = $this->input->post('jenis_lap', TRUE);
	if($jenis_lap==1){
    $data['isi'] = 'info-forecast-gdjd/vformviewwip';
	}
	elseif($jenis_lap==2){
    $data['isi'] = 'info-forecast-gdjd/vformviewwip_warna';
	}
	$date_from = $this->input->post('date_from', TRUE);
	
	$date_to = $this->input->post('date_to', TRUE);  
	$gudang = $this->input->post('gudang', TRUE);  
	
	$data['query'] = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $gudang);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['gudang'] = $gudang;
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$this->load->view('template',$data);
  }
   function export_excel_wip() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='6' align='center'>Laporan Drop Forecast Gudang Jadi</th>
		 </tr>
		 <tr>
			<th colspan='6' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='6' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			<th width='3%' ><font size='3'>No</font></th>
		<th width='5%' ><font size='3'>Kode</font></th>
		<th width='30%'><font size='3'>Nama Barang WIP</font></th>
		<th width='8%' ><font size='3'>Drop Forecast</font></th>
		<th width='8%' ><font size='3'>Quantity Keluar</font></th>
		<th width='8%' ><font size='3'>Sisa</font></th>
		 		 </tr>
		</thead>
		<tbody>";

		
			if (is_array($query)) {
				 for($j=0;$j<count($query);$j++){
				
				$html_data.="<tr class=\"record\">
				<td align='center'>".($j+1)."</td>
				<td>".$query[$j]['kode_brg']."</td>
				<td>".$query[$j]['nama_brg']."</td>
				<td align='right'>".number_format($query[$j]['sa'],0,',','.')."</td>
				<td align='right'>".number_format($query[$j]['jum_keluar'],0,',','.')."</td>
				<td align='right'>".number_format($query[$j]['sisa'],0,',','.')."</td>
				</tr>";					
		 	}
		   }
	
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan Forecast gudang jadi";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  function export_excel_wip_warna() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$id_gudang = $this->input->post('id_gudang', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$kode_gudang = $this->input->post('kode_gudang', TRUE);
		$nama_gudang = $this->input->post('nama_gudang', TRUE);
		$nama_lokasi = $this->input->post('nama_lokasi', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_mutasi_stok_wip($date_from, $date_to, $id_gudang);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$html_data = "
		<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='6' align='center'>Laporan Drop Forecast Gudang Jadi</th>
		 </tr>
		 <tr>
			<th colspan='6' align='center'>Lokasi Gudang: [$nama_lokasi] $kode_gudang-$nama_gudang</th>
		 </tr>
		 <tr>
			<th colspan='6' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			<th width='3%' ><font size='3'>No</font></th>
		<th width='5%' ><font size='3'>Kode</font></th>
		<th width='30%'><font size='3'>Nama Barang WIP</font></th>
		<th width='8%' ><font size='3'>Drop Forecast</font></th>
		<th width='8%' ><font size='3'>Quantity Keluar</font></th>
		<th width='8%' ><font size='3'>Sisa</font></th>
		 		 </tr>
		</thead>
		<tbody>";

		
			if (is_array($query)) {
				 for($j=0;$j<count($query);$j++){
				//~ 
				$html_data.="<tr class=\"record\">
				<td align='center'>".($j+1)."</td>
				<td>".$query[$j]['kode_brg']."</td>
				<td>".$query[$j]['nama_brg']."</td>
				<td align='right' width='15%'>";
				 if (is_array($query[$j]['data_stok_warna'])) {
					 $var_detail = $query[$j]['data_stok_warna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['tm_warna'] ." : ". $var_detail[$k]['qty_fore_warna'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
						     
					}
					 $html_data.=  "<br> Total : " .number_format($query[$j]['sa'],0,',','.');
				 }
				 $html_data.= "</td>";
				 $html_data.= "<td align='right' width='15%'>";
				if (is_array($query[$j]['data_stok_warna'])) {
					 $var_detail = $query[$j]['data_stok_warna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['tm_warna'] ." : ". $var_detail[$k]['qty_sj_warna'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
						     
					}
					 $html_data.=  "<br> Total : " .number_format($query[$j]['jum_keluar'],0,',','.');
				 }
				 $html_data.= "</td>";
				 $html_data.= "<td align='right' width='15%'>";
				if (is_array($query[$j]['data_stok_warna'])) {
					 $var_detail = $query[$j]['data_stok_warna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  $html_data.= $var_detail[$k]['tm_warna'] ." : ". $var_detail[$k]['qty_sisa_warna'];
						  if ($k<$hitung-1)
						     $html_data.= "<br>";
						     
					}
					 $html_data.=  "<br> Total : " .number_format($query[$j]['sisa'],0,',','.');
				 }
				 $html_data.= "</td>
				</tr>";					
		 	}
		   }
	
		   
		 $html_data.= "</tbody>
		</table>";

		$nama_file = "laporan Forecast gudang jadi warna";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  }
  
  
