<?php
class Creport extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('trial-wip-packing/mreport');
	}

	function viewsounitpacking()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}

		$data['isi'] = 'trial-wip-packing/vformviewsounitpacking';
		$id_unit 	= $this->input->post('id_unit', TRUE);
		$bulan 	= $this->input->post('bulan', TRUE);
		$tahun 	= $this->input->post('tahun', TRUE);

		if ($id_unit == '' && $bulan == '' && $tahun == '') {
			$id_unit 	= $this->uri->segment(4);
			$bulan 	= $this->uri->segment(5);
			$tahun 	= $this->uri->segment(6);
		}

		if ($bulan == '')
			$bulan 	= "00";
		if ($tahun == '')
			$tahun 	= "0";
		if ($id_unit == '')
			$id_unit = '0';

		$jum_total = $this->mreport->get_sowipunittanpalimit($id_unit, $bulan, $tahun);

		$config['base_url'] = base_url() . 'index.php/trial-wip-packing/creport/viewsounitpacking/' . $id_unit . '/' . $bulan . '/' . $tahun . '/';
		//$config['total_rows'] = $query->num_rows(); 
		$config['total_rows'] = count($jum_total);
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(7);
		$this->pagination->initialize($config);

		$data['query'] = $this->mreport->get_sowipunit($config['per_page'], $this->uri->segment(7), $id_unit, $bulan, $tahun);
		$data['jum_total'] = count($jum_total);

		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;

		$data['list_unit'] = $this->mreport->get_unit_packing();
		$data['cunit'] = $id_unit;
		$data['cbulan'] = $bulan;

		if ($tahun == '0')
			$data['ctahun'] = '';
		else
			$data['ctahun'] = $tahun;
		$data['startnya'] = $config['cur_page'];

		$this->load->view('template', $data);
	}
	function editsounitpacking()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}

		// ------------------------

		$id_so 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$cunit 	= $this->uri->segment(6);
		$cbulan 	= $this->uri->segment(7);
		$ctahun 	= $this->uri->segment(8);

		$data['cur_page'] = $cur_page;
		$data['cunit'] = $cunit;
		$data['cbulan'] = $cbulan;
		$data['ctahun'] = $ctahun;
		$data['id_so'] = $id_so;

		$data['query'] = $this->mreport->get_sounitpacking($id_so, $cbulan, $ctahun);
		$data['querybrgbaru'] = $this->mreport->get_detail_stokbrgbaru_unitpacking($id_so, $cbulan, $ctahun);

		if (is_array($data['query']))
			$data['jum_total'] = count($data['query']);
		if (is_array($data['querybrgbaru']))
			$data['jum_total'] += count($data['querybrgbaru']);
		//echo $data['jum_total']; die();

		$data['isi'] = 'trial-wip-packing/veditsounitpacking';
		$this->load->view('template', $data);
		//-------------------------
	}
	function updatesounitpacking()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in != true) {
			redirect('loginform');
		}

		$id_so = $this->input->post('id_so', TRUE);
		$id_unit = $this->input->post('id_unit', TRUE);
		$jum_data = $this->input->post('jum_data', TRUE);

		$cur_page = $this->input->post('cur_page', TRUE);
		$cunit = $this->input->post('cunit', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);

		// 22-12-2014
		$tgl_so 	= $this->input->post('tgl_so', TRUE);
		$pisah1 = explode("-", $tgl_so);
		$tgl1 = $pisah1[0];
		$bln1 = $pisah1[1];
		$thn1 = $pisah1[2];
		$tgl_so = $thn1 . "-" . $bln1 . "-" . $tgl1;

		$jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
		$tgl = date("Y-m-d H:i:s");

		// update ke tabel tt_stok_opname_
		// ambil data terakhir di tabel tt_stok_opname_				 
		$this->db->query(" UPDATE tt_stok_opname_unit_packing SET tgl_so = '$tgl_so', 
						jenis_perhitungan_stok='$jenis_perhitungan_stok', tgl_update = '$tgl', status_approve='t' 
						where id = '$id_so' and bulan ='$cbulan' and tahun ='$ctahun' ");

		for ($i = 1; $i <= $jum_data; $i++) {
			$this->mreport->savesounitpacking(
				$id_so,
				$tgl_so,
				$id_unit,
				$jenis_perhitungan_stok,
				$this->input->post('id_' . $i, TRUE),
				$this->input->post('id_brg_wip_' . $i, TRUE),
				$this->input->post('id_warna_' . $i, TRUE),
				$this->input->post('stok_fisik_' . $i, TRUE)
			);
		}
		//die();
		if ($ctahun == '') $ctahun = "0";
		$url_redirectnya = "trial-wip-packing/creport/viewsounitpacking/" . $cunit . "/" . $cbulan . "/" . $ctahun . "/" . $cur_page;
		redirect($url_redirectnya);
	}
}
