<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->model('mst-motif/mmaster');
  }

  function index(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->kode;
			$enama = $row->nama;
		}
	}
	else {
			$ekode = '';
			$enama = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['enama'] = $enama;
	$data['edit'] = $edit;
	
	$data['query'] = $this->mmaster->getAll();
    $data['isi'] = 'mst-motif/vmainform';
    
	$this->load->view('template',$data);
  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		else {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('mst-motif/cform');
		}
		else
		{
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			
			if ($goedit == 1) {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) == 0) { 
						$this->mmaster->save($kode, $kodeedit, $nama, $goedit);
					}
				}
				else {
					$this->mmaster->save($kode, $kodeedit, $nama, $goedit);
				}
			}
			else {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) == 0) { 
					$this->mmaster->save($kode, $kodeedit, $nama, $goedit);
				}
			}
			
			redirect('mst-motif/cform');
		}
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-motif/cform');
  }
  
}
