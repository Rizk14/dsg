var stdChildWin;
function stdTestFocus() {
	if (stdChildWin) {
		stdChildWin.focus();
	}
}

function stdWinPopup(URL,wd,hg) {
	stdChildWin = centerPopup(URL, "winPop", wd, hg);
}

function centerPopup(url, win, w, h) {
	var features;
	var top;
	var left;
	var winOp;
	top = (screen.height - h)/2;
	left = (screen.width - w)/2; 
	if(top < 0) top = 0; 
	if(left < 0) left = 0; 
	features = 'top=' + top + ',left=' +left; 
	features += ',height=' + h + ',width=' + w + ',toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,copyhistory=no'; 
	winOp = window.open(url,win, features); 
	winOp.focus();
	return winOp;
}

function getList(u,w,h,Frm,St) {
	var val = prompt(St+": ","");
	if (val != null) {
		stdWinPopup(u+"&val="+val+"&form="+Frm,w,h);
	} else {
		return false;
	}
	return true;
}