<?php
function Terbilang($x) {
  $Penyebut = array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas','Duabelas','Tigabelas','Empatbelas','Limabelas','Enambelas','Tujuhbelas','Delapanbelas','Sembilanbelas');
  #$Mayor = array('','ribu','juta','bilyun','trilyun','kwatrilyun','kwintilyun','sekstilyun');  /* Mengikuti gaya Amerika Serikat */
  #$Mayor = array('ribu','juta','milyar','bilyun','ribu bilyun','trilyun','ribu trilyun');  /* Ikuti gaya Inggris */
  $Mayor = array('','Ribu','Juta','Milyar','Trilyun','Kwatrilyun','Kwintilyun','Sekstilyun');  /* Mengikuti gaya NKRI */

  $AngkaMasukan = $x;
  if ($x!=0) {
  $Terbilang = '';
  #$AngkaMasukan = number_format($AngkaMasukan,0,'','');
  $AngkaMasukan = "$AngkaMasukan";
  $Panjang = strlen($AngkaMasukan);

  # Jika panjangnya bukan kelipatan 3 maka tambahkan spasi di depannya agar menjadi kelipatan 3
  # Hal ini dimaksudkan agar fragmen yang tercipta sungguh-sungguh kelompok 3 angka (aksara)
  if (fmod($Panjang,3) > 0) {
    $Add = 3 - fmod($Panjang,3);
    for ($i=0; $i<$Add; $i++) { $AngkaMasukan = ' '.$AngkaMasukan; }
  }

  # Ayo belah menjadi fragmen utama
  $Angka = str_split($AngkaMasukan,3);    # Ciptakan fragmen yang setiap fragmen terdiri dari 3 aksara
  $Fragmen = count($Angka);
  $Angka[0] = trim($Angka[0]," ");    # Pastikan fragmen terdepan (pertama) terbebas dari karakter spasi bantuan

  for ($i=0; $i<$Fragmen; $i++) {
    $Tes = $Angka[$i] * 1;
    $Dijit = str_split($Tes);

    if ($Tes == 1 and ($Fragmen-$i-1) == 1) { $Terbilang .= 'se'; }
    if ($Tes < 20 and ($Fragmen-$i-1) != 1) { $Terbilang .= $Penyebut[$Tes] . " "; }
    if ($Tes >= 20 and $Tes < 1000) {
      if ($Tes < 100) { $Terbilang .= $Penyebut[$Dijit[0]] . ' Puluh ' . $Penyebut[$Dijit[1]] . ' '; }
      if ($Tes >= 100) {
        if ($Dijit[0] == '1') { $Terbilang .= 'Seratus '; } else { $Terbilang .= $Penyebut[$Dijit[0]] . ' Ratus '; }
        $Tes = fmod($Tes,100);    # Jika lebih dari 99 maka sisanya dihitung lagi
        $Dijit = str_split($Tes);
        if ($Tes < 20) { $Terbilang .= $Penyebut[$Tes]; }
        else { $Terbilang .= $Penyebut[$Dijit[0]] . ' Puluh ' . $Penyebut[$Dijit[1]] . ' '; }
      }
    }
    $Terbilang .= ' ' . $Mayor[$Fragmen-$i-1] . ' ';
  }
  $Terbilang = trim($Terbilang," ");    # Pastikan tidak ada karakter spasi yang ada di depan dan belakang hasil proses

  //return $Terbilang;
  return $Terbilang;
  }
}

function kekata($x) {
#
    $x = abs($x);
#
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
#
    "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
#
    $temp = "";
#
    if ($x <12) {
#
        $temp = " ". $angka[$x];
#
    } else if ($x <20) {
#
        $temp = kekata($x - 10). " belas";
#
    } else if ($x <100) {
#
        $temp = kekata($x/10)." puluh". kekata($x % 10);
#
    } else if ($x <200) {
#
        $temp = " seratus" . kekata($x - 100);
#
    } else if ($x <1000) {
#
        $temp = kekata($x/100) . " ratus" . kekata($x % 100);
#
    } else if ($x <2000) {
#
        $temp = " seribu" . kekata($x - 1000);
#
    } else if ($x <1000000) {
#
        $temp = kekata($x/1000) . " ribu" . kekata($x % 1000);
#
    } else if ($x <1000000000) {
#
        $temp = kekata($x/1000000) . " juta" . kekata($x % 1000000);
#
    } else if ($x <1000000000000) {
#
        $temp = kekata($x/1000000000) . " milyar" . kekata(fmod($x,1000000000));
#
    } else if ($x <1000000000000000) {
#
        $temp = kekata($x/1000000000000) . " trilyun" . kekata(fmod($x,1000000000000));
#
    }      
#
        return $temp;
#
}
#
function terbilangxx($x, $style=4) {
#
    if($x<0) {
#
        $hasil = "minus ". trim(kekata($x));
#
    } else {
#
        $hasil = trim(kekata($x));
#
    }      
#
    switch ($style) {
#
        case 1:
#
            $hasil = strtoupper($hasil);
#
            break;
#
        case 2:
#
            $hasil = strtolower($hasil);
#
            break;
#
        case 3:
#
            $hasil = ucwords($hasil);
#
            break;
#
        default:
#
            $hasil = ucfirst($hasil);
#
            break;
#
    }      
#
    return $hasil;
#
}

?>
