function displayPanel(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');

	for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel':'tab';
	 }
}

function displayPanelUser(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_user':'tab_user';
	 }
}

function displayPanelBrgMentah(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_brg_mentah':'tab_brg_mentah';
	 }
}

function displayPanelBrg(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_brg_jadi':'tab_brg_jadi';
	 }
}


function displayPanelBrgMotif(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_brg_motif':'tab_brg_motif';
	 }
}

function displayPanelPemasok(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_pemasok':'tab_pemasok';
	 }
}

function displayPanelHarga(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_harga':'tab_harga';
	 }
}

function displayPanelKls(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_kls':'tab_kls';
	 }
}

function displayPanelKategori(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_kategori':'tab_kategori';
	 }
}

function displayPanelPel(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_pel':'tab_pel';
	 }
}

function displayPanelGroupPel(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_group_pel':'tab_group_pel';
	 }
}

function displayPanelPelTransfer(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_peltransfer':'tab_peltransfer';
	 }
}

function displayPanelMerek(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_merek':'tab_merek';
	 }
}

function displayPanelLayout(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_layout':'tab_layout';
	 }
}

function displayPanelStatus(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_status':'tab_status';
	 }
}

function displayPanelWarna(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_warna':'tab_warna';
	 }
}

function displayPanelPrinter(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_printer':'tab_printer';
	 }
}

function displayPanelAkunPajak(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_AkunPajak':'tab_AkunPajak';
	 }
}

function displayJnsSetorPajak(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_jnssetorpajak':'tab_jnssetorpajak';
	 }
}

function displaypenyetor(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_penyetor':'tab_penyetor';
	 }
}

function displayPanelJenisVoucher(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_JenisVoucher':'tab_JenisVoucher';
	 }
}

function displayPanelSatuanBhnBaku(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_SatuanBhnBaku':'tab_SatuanBhnBaku';
	 }
}

function displayKodeVoucher(nval) {
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	
	 for(i = 0; i < panels.length; i++) {
	   document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
	   document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel_kodevoucher':'tab_kodevoucher';
	 }
}
