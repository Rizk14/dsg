$(function()
{
	$(window).resize(function()
	{
		jsResizeBlackOverlay("#fade");
	});
});

function jsResizeBlackOverlay(divBlackOverlay)
{
	if($(window).height() > $(document).height()) $(divBlackOverlay).height($(window).height()); else $(divBlackOverlay).height($(document).height());
	if($(window).width() > $(document).width()) $(divBlackOverlay).width($(window).width()); else $(divBlackOverlay).width($(document).width());
}

function jsDlgShow(divDisabled, divBlackOverlay, divLightBox)
{
	jsResizeBlackOverlay(divBlackOverlay);
	$(divBlackOverlay).fadeTo("fast", 0.3, function()
	{
		$(divDisabled).attr("disabled", "disabled");
		$(divBlackOverlay).fadeIn("fast", function()
		{
			$(divLightBox).slideDown("fast");
		});
	});
}

function jsDlgHide(divDisabled, divBlackOverlay, divLightBox)
{
	$(divLightBox).slideUp("fast", function()
	{
		$(divBlackOverlay).fadeOut("fast", function()
		{
			$(divDisabled).attr("disabled", "");
		});
	});
}
